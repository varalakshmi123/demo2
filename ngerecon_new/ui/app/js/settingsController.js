/**
 * Created by swami on 14/4/15.
 */
'use strict';
var app = angular.module("nextGen");
app.controller("settingsController", ["$scope", "$route", "$location", "$timeout", "$uibModal", "$rootScope", "$http", "ReconsService", "fileUploadService", "ReconProcessService",
    function ($scope, $route, $location, $timeout, $uibModal, $rootScope, $http, ReconsService, fileUploadService, ReconProcessService) {

        $scope.call = false
        $scope.recons = false
        $rootScope.enableReconSetting = true
        $scope.edit = true
        $scope.dropDown = false
        $scope.recon = {}
        $scope.image = {}
        $scope.srcs = false
        $scope.add = false
        $scope.nameErrorMsg = false
        $scope.dataBase = false
        $scope.src = ''
        $scope.listSource = false
        $scope.uploadRecon = function (index) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService, ReconProcessService) {
                    $scope.title = $rootScope.resources['reconImgUpload']

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/image/upload"; //Url of webservice/api/server
                        var filePath = "/files/reconImg"
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.showMessage($rootScope.resources['image'], $rootScope.resources['imageUploadMsg'], 'success');
                            $uibModalInstance.dismiss('cancel');
                            var dataR = {}
                            dataR['imagePath'] = filePath + '/' + data['msg']['fileName']
                            dataR['reconProcess'] = $scope.distinctReconProcess[index]
                            ReconProcessService.saveProcessDetail('dummy', dataR, function (data) {
                                $scope.image[$scope.distinctReconProcess[index]] = true
                            })
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });

                    }

                    $scope.submit = function () {
                        $scope.redirect(recon)
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        $scope.sources = {}
        $scope.struct = ''
        $scope.Structures = {}
        $scope.keyCols = []
        $scope.editStruc = ''
        $scope.srcDetails = {
            'sourceName': '',
            'struct': ''
        }

        // Add a new recon
        $scope.addNewRecon = function () {
            $scope.add = true;
            $scope.recon = {};
            $scope.reconId = undefined
            $scope.sources = {}
        }

        //Add source to recon
        $scope.addSource = function (source) {
            $scope.srcs = true
            $scope.add = false
            $scope.src = source
            $scope.sourceName = ''
            $scope.editingSource = source
            $scope.sources[source] = {}
            $scope.Structures = {}
            $scope.srcDetails['sourceName'] = source
        }

        //Edit existing source in recon
        $scope.editSource = function (source, index) {
            $scope.Structures = {}
            $scope.edit = false
            $scope.listSource = false
            $scope.editingSource = source
            $scope.add = false
            $scope.newSource = true
            $scope.src = source
            angular.forEach($scope.sources[source], function (k, v) {
                if (v != 'feedId') {
                    $scope.Structures[v] = k
                }
            })
            $scope.strctlen = $scope.Structures.length
            $scope.src = source
        }

        //Deleteing recon from process
        $scope.deleteRecon = function (val, index) {
            var index = index
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconsService) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteReconMsg']
                    $scope.values = $scope.processRecons[index]

                    $scope.submit = function () {
                        ReconsService.deleteData('dummy', $scope.values, function (data) {
                            $uibModalInstance.dismiss('cancel');
                            $scope.refreshReconView()
                            $scope.processRecons.splice(index, 1)
                            $rootScope.showMessage($rootScope.resources.delete, $rootScope.resources.deleteReconSucc, 'success')
                        }, function (err) {
                            console.log(err)
                        })
                    }

                    $scope.cancel = function () {
                        $scope.refreshReconView()
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            console.log($scope.reconDetails[index])

        }

        var strongRegex = new RegExp("^[a-zA-Z_][0-9]*$");
        $scope.checkReconName = function (name) {
            if (strongRegex.test(name) == true) {
                $scope.nameErrorMsg = false
            } else {
                $scope.nameErrorMsg = true
            }
        }

        $scope.refreshReconView = function () {
            $scope.distinctReconProcess = []
            $scope.recon = {}
            $scope.reconDetails = []
            $scope.sources = {}
            $scope.add = false
            $scope.listSource = false
            ReconsService.get(undefined, {'skip': 0, 'limit': 100}, function (data) {
                angular.forEach(data['data'], function (k, v) {
                    $scope.reconDetails.push({'reconProcess': k['reconProcess'], 'reconName': k['reconName']})
                    if ($scope.distinctReconProcess.indexOf(k['reconProcess']) == -1) {
                        $scope.distinctReconProcess.push(k['reconProcess'])
                        $scope.image[k['reconProcess']] = false
                    }
                })
            })
            ReconProcessService.get(undefined, undefined, function (data) {
                angular.forEach(data['data'], function (v, k) {
                    if (v['imagePath'] != undefined) {
                        $scope.image[v['reconProcess']] = true
                    }
                })
            })
        }


        //Refresh recon view
        $scope.refreshReconView()

        //get All recon Processs to view
        $scope.getProcesRecons = function (index) {
            $scope.processRecons = []
            $scope.process = $scope.distinctReconProcess[index]
            $scope.expanded = true
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v['reconProcess'] == $scope.distinctReconProcess[index]) {
                    $scope.processRecons.push(v)
                }
            })
        }


        $scope.removeProcess = function (index) {
            $scope.expanded = false
            $scope.processRecons = []
            $scope.process = ''
        }

        //Function to save Recon details
        $scope.saveRecon = function (data) {
            $scope.sourceName = ''
            data['sources'] = $scope.sources

            if (!('imagePath' in data)) {
                data['imagePath'] = '/files/reconImg/download.png'
            }
            var query = {'reconProcess': data['reconProcess']}
            ReconProcessService.saveProcessDetail('dummy', query, function (data) {
                console.log(data)
            }, function (err) {
                $rootScope.showMessage($rootScope.resources.error, err.msg, 'error')
            })

            if (angular.isDefined($scope.reconId)) {
                // ReconsService.put($scope.reconId, data, function (result) {
                //     $scope.refreshReconView()
                //     console.log(result)
                // })

                //     if (!('isLicensed' in data)) {
                //         data['isLicensed'] = false
                data['_id'] = $scope.reconId
            }

            ReconsService.saveRecon('dummy', data, function (data) {
                $scope.refreshReconView()
                $rootScope.showMessage('Recon', data, 'success')
            }, function (err) {
                $rootScope.showMessage('Recon', err.msg, 'error')
            })
            // }
            // else {
            //     if (!('isLicensed' in data)) {
            //         data['isLicensed'] = false
            //     }
            // ReconsService.post(data, function (result) {
            //     $scope.refreshReconView()
            // })
            // ReconsService.saveRecon('dummy', data, function (data) {
            //     $scope.refreshReconView()
            //     $rootScope.showMessage('Recon', 'Added successfully', 'success')
            // })
            //
            // }
            $scope.removeProcess()
        }

        //Function to edit recon
        $scope.editRecon = function (recon) {
            var perColConditions = {}
            perColConditions ['reconName'] = recon['reconName']

            ReconsService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                $scope.sources = data['data'][0]['sources']
                $scope.reconId = data['data'][0]["_id"]
            })
            $scope.recon = recon
            $scope.add = true
            $scope.listSource = true
        }

        //Function to clone whole recon details
        $scope.cloneRecon = function (recon) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['cloneMsg']
                    $scope.submit = function () {
                        $scope.cloneSrc = true
                        var perColConditions = {}
                        perColConditions ['reconName'] = recon['reconName']
                        perColConditions['cloneSrc'] = $scope.cloneSrc
                        $rootScope.openModalPopupOpen()
                        ReconsService.cloneSelectedRecon(undefined, perColConditions, function (data) {
                            $rootScope.openModalPopupClose()
                            $scope.refreshReconView()
                            $scope.removeProcess()
                            $rootScope.showMessage('Clone', data, 'success')
                        })
                        $rootScope.displayMessage('Clone', 'md', 'success');
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $scope.cloneSrc = false
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            })
        }


        //adding New structure to the structure
        $scope.saveStruct = function (srcDetails) {
            srcDetails.sourceName = $scope.src
            if (!angular.isDefined($scope.sources[srcDetails.sourceName])) {
                $scope.sources[srcDetails.sourceName] = {}
            }
            if (!angular.isDefined($scope.sources[srcDetails.sourceName][srcDetails.struct])) {
                $scope.sources[srcDetails.sourceName][srcDetails.struct] = {'keyColumns': []}
                $scope.Structures[srcDetails.struct] = {'keyColumns': []}
                $scope.sources[srcDetails.sourceName][srcDetails.struct]['keyColumns'].push({
                    "keyCols": '',
                    "DataType": ''
                })
                $scope.Structures[srcDetails.struct]['keyColumns'].push({
                    "keyCols": '',
                    "DataType": ''
                })
            }
            $scope.srcDetails.struct = ''
        }


        //Add new column to the structure of source
        $scope.addRow = function (src, struct) {
            if (angular.isDefined($scope.editingStruct)) {
                if ($scope.editingStruct != struct) {
                    $scope.Structures[struct] = {'keyColumns': []}
                    $scope.Structures[struct]['keyColumns'] = $scope.Structures[$scope.editingStruct]['keyColumns']
                    $scope.Structures[struct]['feedId'] = $scope.Structures[$scope.editingStruct]['feedId']
                    delete $scope.Structures[$scope.editingStruct]
                    $scope.editingStruct = struct
                }
            }
            $scope.struct = struct
            $scope.Structures[struct]['keyColumns'].push({
                "keyCols": '',
                "DataType": ''
            })
        }


        // $scope.addNewSource = function () {
        //     $scope.src = ''
        //     $scope.listSource = false
        // }

        $scope.saveStructure = function (struct, columns) {
            $scope.edit = false
            if ($scope.editingStruct != struct) {
                $scope.Structures[struct] = columns
                delete $scope.Structures[$scope.editingStruct]
            }
            else {
                $scope.Structures[$scope.editingStruct] = columns
            }
        }

        $scope.editStructure = function (struct) {
            $scope.edit = true
            $scope.editingStruct = struct
        }

        // Cancel saving the source
        $scope.cancelSource = function (src, index) {
            $scope.newSource = false
            $scope.add = true

            if (Object.keys($scope.sources[src]).length) {
                $scope.listSource = true
                $scope.editRecon($scope.recon)

                angular.forEach($scope.sources[src], function (k, v) {
                    if (v == []) {
                        delete $scope.sources[src][k]
                    }
                    if (v['keyCols'] == '') {
                        delete $scope.sources[src]
                    }
                })
            }
            else {
                delete $scope.sources[src]
            }
        }


        //Save Source For the recon
        $scope.saveSource = function (srcs) {
            $scope.edit = false
            $scope.newSource = false
            $scope.add = true
            $scope.src = ''
            console.log($scope.sources)
            if (srcs != $scope.editingSource) {
                $scope.sources[srcs] = $scope.Structures
                delete $scope.sources[$scope.editingSource]
            }
            else {
                $scope.sources[$scope.editingSource] = $scope.Structures
            }
            $scope.listSource = true
            $scope.strctlen = $scope.Structures.length
        }

        //Function to Deleting source of recon
        $scope.deleteSource = function (key) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteMsg']

                    $scope.submit = function () {
                        delete $scope.sources[key]
                        $rootScope.displayMessage($rootScope.resources.srcDelMsg, 'md', 'success');
                        $uibModalInstance.dismiss('cancel');

                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        //Function to remove structure from source
        $scope.removeStruct = function (struct) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteStructMsg']

                    $scope.submit = function () {
                        delete $scope.Structures[struct]
                        $rootScope.displayMessage($rootScope.resources.struDelMsg, 'md', 'success');
                        $uibModalInstance.dismiss('cancel');

                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }

        //Function to remove column from structure
        $scope.removeCol = function (index, src, struct) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteColMsg']

                    $scope.submit = function () {
                        $scope.Structures[struct]['keyColumns'].splice(index, 1)
                        $rootScope.displayMessage($rootScope.resources.deleteCol, 'md', 'success');
                        $uibModalInstance.dismiss('cancel');

                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }
    }]);
