'use strict';
var app = angular.module("nextGen");

app.controller('auditsController', ["$rootScope", "$scope","$filter","$interval","AuditsService", function ($rootScope, $scope,$filter,$interval,AuditsService) {


    $scope.getAuditDetails = function () {
        AuditsService.get(undefined, undefined, function (data) {
            $scope.auditDetails = data['data']
            $scope.totalItems = data['total']
            angular.forEach($scope.auditDetails, function (val, index) {
                var date = $filter('date')(val["logTime"], 'd-MM-yyyy HH:mm:ss');
                $scope.auditDetails[index]['logTime'] = date
            })
        }, function (err) {
            console.log(err.msg)
        });
    }
 $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields,
                                                  orderBy, orderByReverse) {
        if (Object.keys($rootScope).indexOf('genDataTableQuery') != -1) {
            AuditsService.get(undefined, $rootScope.genDataTableQuery(currentPage, pageItems,
                filterBy, filterByFields, orderBy, orderByReverse), function (data) {
                $scope.auditDetails = data['data']
                $scope.totalItems = data['total']
                angular.forEach($scope.auditDetails, function (val, index) {
                    var date = $filter('date')(val["logTime"], 'd-MM-yyyy HH:mm:ss');
                    $scope.auditDetails[index]['logTime'] = date
                })
            }, function (err) {
                console.log(err.msg)
            })
        }
    }

 $scope.autoFresh = function () {
        console.log('------- Auto Refresh --------')
         $scope.getAuditDetails()
    }

    $interval(function () {
        $scope.autoFresh();
    }, 30000);


}]);
