import pymongo
import pymongo.collection
import pymongo.son_manipulator
import utilities
import copy
import re
import time
import traceback
import functools
import bson
import json
from config import config
import flask

import logger
logr = logger.Logger.getInstance("framework").getLogger()

#If Mongo connection is down, keep retrying!!!
def exceptionHandler(f):
    @functools.wraps(f)
    def handler(*args, **kwargs):
        count = 0
        while True:
            count += 1
            try:
                return f(*args, **kwargs)
            except (pymongo.errors.AutoReconnect, pymongo.errors.ConnectionFailure), e:
                logr.error('Unable to connect to mongo, will retry: %s' % (traceback.format_exc()))
                time.sleep(5)
                if count == 5:
                    count = 0

    return handler


class HideFields(pymongo.son_manipulator.SONManipulator):
    def __init__(self, hiddenFields):
        self.hiddenFields = hiddenFields

    def transform_outgoing(self, son, collection):
        for field in self.hiddenFields:
            if field in son:
                del son[field]
        return son


class DataFormatter(pymongo.son_manipulator.SONManipulator):
    def transform_outgoing(self, son, collection):
        if "_id" in son:
            son["_id"] = str(son["_id"])  # Change the ObjectId to str, else json serialization will have issues
        return son


class MongoCollection(pymongo.collection.Collection):
    pymongoConnection = {}

    def __init__(self, collectionName, hideFields=True, restrictAccess=True, mongoUrl=config.mongoUrl,
                 mongoDBName=config.databaseName):
        self.mongoUrl = mongoUrl
        self.databaseName = mongoDBName
        self._initConnectionAndDatabase(hideFields)
        self.restrictAccess = restrictAccess
        self.forcePrimary = False
        self.partnerId = None

        super(MongoCollection, self).__init__(self.databaseConnection, collectionName)

        if restrictAccess and flask.has_request_context() and "sessionData" in flask.session:
            self._setUser(flask.session["sessionData"])

    def _getHiddenFields(self):
        return []

    def _getMongoCaseInsensitive(self,data):
        return re.compile("^" + re.escape(data) + "$", re.IGNORECASE)

    def _setUser(self, userData):
        self.partnerId = userData["partnerId"]

    @exceptionHandler
    def count(self, condition=None):
        if condition:
            return True, self.find(self._canAccessCondition(self._fixCondition(condition))).count()
        return False, "Require filter condition."

    @exceptionHandler
    def getAll(self, query={}, skip=0, limit=10, orderBy=[], fields=None):
        mongoquery = {}
        # Sample data
        # {"skip":0,"limit":10,"perColConditions":{"resource_list":"test","TicketNbr":"1234"}}
        # {"skip":0,"limit":10,"globalConditions":{"globalsearch":["resource_list","TicketNbr","status_description","Summary"]}}
        if "condition" in query:
            mongoquery["filter"] = self._canAccessCondition(self._fixCondition(query["condition"]))
        elif "perColConditions" in query:
            q = {}
            for key, value in query["perColConditions"].items():
                if isinstance(value, basestring):
                    q[key] = {'$regex': '.*' + value + '.*', '$options': 'i'}
                else:
                    q[key] = value
            mongoquery["filter"] = self._canAccessCondition(self._fixCondition(q))
        elif "globalConditions" in query:
            q = {}
            for key, value in query["globalConditions"].items():
                regexp = {}
                if isinstance(value, basestring):
                    regexp["$regex"] = '.*' + key + '.*'
                    regexp['$options'] = 'i'
                else:
                    regexp = value
                orExp = []
                for column in value:
                    orExp.append({column: regexp})
                q["$or"] = orExp
            mongoquery["filter"] = self._canAccessCondition(self._fixCondition(q))
        else:
            mongoquery["filter"] = self._canAccessCondition(self._fixCondition({}))

        if orderBy:
            mongoquery["sort"] = orderBy.items()

        if fields:
            mongoquery['projection'] = fields

        cursor = self.find(**mongoquery)
        resp = {}
        cursor.skip(skip)
        cursor.limit(limit)
        resp["total"] = cursor.count(False)
        resp["current"] = cursor.count(True)
        resp["data"] = list(cursor)
        return True, resp

    @exceptionHandler
    def get(self, condition):
        condition = self._canAccessCondition(self._fixCondition(condition))
        if condition is None or len(condition) == 0:
            return False, "Cannot get all entries of collection"
        found = self.find_one(condition)
        if found:
            return True, found
        else:
            return False, "No data found" + str(condition)

    @exceptionHandler
    def delete(self, condition):
        condition = self._canAccessCondition(self._fixCondition(condition))
        if condition is None or len(condition) == 0:
            return False, "Cannot remove all entries of collection"
        resp = self.remove(condition)
        if 'err' in resp and resp["err"]:
            return False, resp["err"]
        else:
            return True, "Deleted"

    @exceptionHandler
    def create(self, doc):
        if (not 'partnerId' in doc) and (not self.partnerId):
            raise ValueError("Login as a user before inserting data.")
        doc["created"] = utilities.getUtcTime()
        doc["updated"] = utilities.getUtcTime()
        if "partnerId" not in doc:
            doc["partnerId"] = self.partnerId

        id = str(self.save(doc))
        if id:
            return self.get(id)
        else:
            return False, "Creation failed"

    @exceptionHandler
    def modify(self, condition, doc, multi=False):
        cleanupFields = ["_id", "created", "updated", "partnerId"]
        for field in cleanupFields:
            if field in doc:
                del doc[field]
        doc["updated"] = utilities.getUtcTime()

        resp = super(MongoCollection, self).update(self._canAccessCondition(self._fixCondition(condition)),
                                                   {"$set": doc}, multi=multi)
        if 'err' in resp and resp["err"]:
            return False, resp["err"]
        else:
            self.forcePrimary = True
            return self.get(self._canAccessCondition(self._fixCondition(condition)))

    def _canAccessCondition(self, condition={}):
        # if user is logged in then only update the condition
        if self.partnerId and self.restrictAccess:
            # If the logged in user is in "accessors" then allow access
            accessCondition = [{"partnerId": self.partnerId}]

            if not "$or" in condition:
                condition["partnerId"] = self.partnerId
            else:
                # If existing condition has $or then move that to be $and, case this can happen is when user search in table view or so...
                condition["$and"] = [{"$or": condition["$or"]}, {"partnerId": self.partnerId}]
                del condition["$or"]
        return condition

    @exceptionHandler
    def exists(self, condition, isGlobal=False):
        condition = self._fixCondition(condition)
        if not isGlobal:
            condition = self._canAccessCondition(condition)
        return self.find_one(condition) is not None

    @exceptionHandler
    def hasAccess(self, id):
        return self.find_one(self._canAccessCondition(self._fixCondition(id))) is not None

    @exceptionHandler
    def _initConnectionAndDatabase(self, hideFields):
        self.connection = pymongo.MongoClient(self.mongoUrl, readPreference='secondaryPreferred', j=True)
        self.databaseConnection = self.connection[self.databaseName]
        if hideFields:
            self.databaseConnection.add_son_manipulator(HideFields(self._getHiddenFields()))
        self.databaseConnection.add_son_manipulator(DataFormatter())

    def _fixCondition(self, condition):
        # convert _id from string to ObjectId
        fixed = {}
        if condition is None:
            return fixed
        elif isinstance(condition, basestring) and bson.objectid.ObjectId.is_valid(condition):
            # If its simple string assuming it to be _id
            fixed["_id"] = bson.objectid.ObjectId(condition)
        elif isinstance(condition, dict):
            # If dict contains _id as string convert to ObjectId
            if "_id" in condition and isinstance(condition["_id"], basestring) and bson.objectid.ObjectId.is_valid(condition['_id']):
                condition["_id"] = bson.objectid.ObjectId(condition["_id"])
            fixed = condition
        return fixed
