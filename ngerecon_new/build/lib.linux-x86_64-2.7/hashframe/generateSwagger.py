#!/usr/bin/env python

import pyclbr
import inspect
import config
import json
import os
import re

swaggerSpec = {
    "swagger": "2.0",
    "info": {
        "version": "1.0.0",
        "title": "API",
        "description": "HashInclude API",
        "contact": {
            "name": "HashInclude",
            "email": "prashanth.gopinath@gmail.com"
        }
    },
    "host": "",
    "basePath": "/api",
    "schemes": [
        "https"
    ],
    "paths": {
    },
    "definitions": {
    },
    "parameters": {
    },
    "responses": {
    },
    "securityDefinitions": {
    }
}

if __name__ == "__main__":
    for module in config.applicationModules:
        moduleInstance = __import__(module)
        moduleClasses = pyclbr.readmodule(module)
        for klassName, klassSpec in moduleClasses.items():
            x = "".join(["_" + ch.lower() if ch.isupper() else ch for ch in klassName])
            if x.startswith('_'):
                x = x[1:]
            klassPath = os.path.join('/', x)
            swaggerSpec['paths'][klassPath] = {"get": {
                "description": "List",
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            },
                "post": {
                    "description": "Create",
                    "responses": {
                        "200": {
                            "description": "OK"
                        }
                    }
                }
            }
        klassPathId = os.path.join(klassPath, '{id}')
        swaggerSpec['paths'][klassPathId] = {"get": {
            "description": "List",
            "parameters": [
                {
                    "name": "id",
                    "type": "integer",
                    "format": "int32",
                    "in": "path",
                    "required": True
                }
            ],
            "responses": {
                "200": {
                    "description": "OK"
                }
            }
        },
            "put": {
                "description": "List",
                "parameters": [
                    {
                        "name": "id",
                        "type": "integer",
                        "format": "int32",
                        "in": "path",
                        "required": True
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            },
            "delete": {
                "description": "Create",
                "parameters": [
                    {
                        "name": "id",
                        "type": "integer",
                        "format": "int32",
                        "in": "path",
                        "required": True
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        }

        klassInstance = getattr(moduleInstance, klassName)
        klassMethods = klassSpec.methods.keys()
        for klassMethod in klassMethods:
            if not klassMethod.startswith("_"):
                args = inspect.getargspec(getattr(klassInstance, klassMethod))[0][2:]
                if len(args):
                    print args
                    actionPath = os.path.join(klassPathId, klassMethod)
                    print 'Path:', actionPath
                    swaggerSpec['paths'][actionPath] = {"post": {
                        "description": "Action",
                        "parameters": [
                            {
                                "name": "id",
                                "type": "integer",
                                "format": "int32",
                                "in": "path",
                                "required": True
                            }
                        ],
                        "responses": {
                            "200": {
                                "description": "OK"
                            }
                        }
                    }
                    }
f = open("Swagger.json", "w")
f.write(json.dumps(swaggerSpec, indent=4))
f.close()
