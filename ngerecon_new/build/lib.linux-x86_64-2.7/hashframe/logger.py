import os
import logging
import logging.handlers
from config import config

class Logger:
    _instances = {}

    @classmethod
    def getInstance(cls, logFile=None):
        if not logFile in Logger._instances:
            if logFile:
                Logger._instances[logFile] = Logger(logFile)
            else:
                raise Exception("Require log file name.")
        return Logger._instances[logFile]

    def __init__(self, logFile):
        if not os.path.exists(config.logBaseDir):
            os.makedirs(config.logBaseDir)

        logFilePath = os.path.join(config.logBaseDir, logFile+".log")

        # Set up a specific logger with our desired output level
        self.my_logger = logging.getLogger(logFile)
        self.my_logger.setLevel(logging.DEBUG)


        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(logFilePath, maxBytes=config.logMaxBytes, backupCount=config.logBackupCount)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(module)s %(funcName)s %(lineno)s %(message)s')
        handler.setFormatter(formatter)
        self.my_logger.addHandler(handler)

    def getLogger(self):
        return self.my_logger
