pymongo==3.5.1
redis==2.10.6
flask==0.12.2
itsdangerous==0.24
requests==2.18.4
xmldict==0.4.1

[gevent]
gevent==1.2.2
