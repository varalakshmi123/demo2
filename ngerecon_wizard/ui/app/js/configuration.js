var app = angular.module('nextGen');
app.controller('configurationController', ['$scope', '$route', '$location', '$timeout', '$uibModal', '$interval', "$window",
    '$rootScope', '$http', 'ReconsService', 'fileUploadService', 'ReconProcessService', 'SourceReferenceService', 'MatchRuleReferenceService', 'ReconDetailsService', 'FlowsService', 'MatchRuleSumColumnsService',
    function ($scope, $route, $location, $timeout, $uibModal, $interval, $window, $rootScope, $http, ReconsService, fileUploadService, ReconProcessService, SourceReferenceService, MatchRuleReferenceService, ReconDetailsService, FlowsService, MatchRuleSumColumnsService) {

        // To Be moved to the json file
        $scope.loadTypes = ['CSV', 'Excel']
        $scope.newDeriveColumnType = ['SET NEW COLUMN', 'CONDITIONAL NEW COLUMN', 'LOGICAL FILTER', 'UPLOAD FILTER']
        $scope.dataTypes = ['np.int64', 'np.float64', 'np.datetime64', 'str']
        $scope.delimiter = ['|', ',']
        $scope.groupbyFuncs = ['sum', 'avg', 'max', 'min', 'mean', 'size']
        $scope.configurationSteps =
            ['defineSource', 'selectMatchColumns', 'defineSumColumns', 'resultsOfProcess'];
        $scope.active = {};

        $rootScope.flowelements = []
        $rootScope.activeFlow = {}

        // Limit items to be dropped in list1
        $scope.optionsList1 = {
            accept: function (dragEl) {
                if ($scope.list1.length >= 2) {
                    return false;
                } else {
                    return true;
                }
            }
        };

        $scope.initiVaiables = function () {
            $scope.filterCondExpArray = [{condValue: ''}]
            $scope.present = 'defineSource';
            $scope.next = 'selectMatchColumns';
            $scope.create = false;
            $scope.filterCondArray = []
            $scope.addSource = false;
        }

        $scope.initProcess = function () {
            $scope.sampleFile=false
            $scope.expressions = [];
            $scope.sourceDetails = []
            $scope.derivedColumns = []
            $scope.ruleDefined = []
            $scope.feedDef = {'filters': []}
            $scope.sources = []
            $scope.matchColumns = {}
            $scope.sumColumns = {}
            $scope.sourceName = ''
            $scope.hideUpload=true
            $scope.properties = false
            $scope.showCustomFilterBox = false
            $scope.active = {};
            $scope.listSource = false;
            $scope.reconEdit = false;
            $scope.showCustomFilterBox = false;
            $scope.recon = {}
            $scope.editingSource = false;
            $rootScope.flowelements = []
            $rootScope.activeFlow = {}

            $scope.initiVaiables();
            // $scope.initGrid()
        }

        $scope.refreshReconView = function () {
            $scope.distinctReconProcess = []
            $scope.recon = {}
            $scope.reconDetails = []
            $scope.sources = []
            $scope.image = {}
            $scope.listSource = false
            ReconsService.get(undefined, {'skip': 0, 'limit': 100}, function (data) {
                $scope.reconDetails = data['data']
                angular.forEach(data['data'], function (k, v) {
                    if ($scope.distinctReconProcess.indexOf(k['reconProcess']) == -1) {
                        $scope.distinctReconProcess.push(k['reconProcess'])
                        $scope.image[k['reconProcess']] = false
                    }
                });
            })
            ReconProcessService.get(undefined, undefined, function (data) {
                angular.forEach(data['data'], function (v, k) {
                    if (v['imagePath'] != undefined) {
                        $scope.image[v['reconProcess']] = true;
                    }
                });
            });
        }

        $scope.refreshReconView();
        $scope.initProcess()

        $scope.getProcesRecons = function (index) {
            $scope.processRecons = []
            $scope.process = $scope.distinctReconProcess[index]
            $scope.expanded = true
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v['reconProcess'] == $scope.distinctReconProcess[index]) {
                    $scope.processRecons.push(v)
                }
            })
        }

        $scope.removeProcess = function (index) {
            $scope.expanded = false
            $scope.processRecons = []
            $scope.process = ''
        }


        $scope.uploadRecon = function (index) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService, ReconProcessService) {
                    $scope.title = $rootScope.resources['reconImgUpload']

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/image/upload"; //Url of webservice/api/server
                        var filePath = "/files/reconImg"
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.showMessage($rootScope.resources['image'], $rootScope.resources['imageUploadMsg'], 'success');
                            $uibModalInstance.dismiss('cancel');
                            var dataR = {}
                            dataR['imagePath'] = filePath + '/' + data['msg']['fileName']
                            dataR['reconProcess'] = $scope.distinctReconProcess[index]
                            ReconProcessService.saveProcessDetail('dummy', dataR, function (data) {
                                $scope.image[$scope.distinctReconProcess[index]] = true
                            })
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });

                    }

                    $scope.submit = function () {
                        $scope.redirect(recon)
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }


        $scope.uploadFilters = function () {
            if (!angular.isDefined($scope.feedDef.filters)) {
                $scope.feedDef['filters'] = []
            }
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/filter/upload"; //Url of webservice/api/server

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            $scope.plate = true
                            if (angular.isDefined(file)) {
                                var query = {
                                    'reconName': $scope.recon.reconName,
                                    'source': $scope.sourceName,
                                    'filter': true,
                                    'fileName': file.name
                                }
                                SourceReferenceService.loadSourceRef('dummy', query, function (data) {
                                    angular.forEach(data, function (k, v) {
                                        if ($scope.feedDef.filters.indexOf(k) == -1) {
                                            $scope.feedDef.filters.push(k)
                                        }
                                    })
                                    // $scope.feedDef.filters = data
                                    $scope.updateDerivedCols(data)
                                    $scope.updateSourceReference($scope.feedDef)
                                }, function (err) {
                                    console.log(err.msg)
                                })
                            }
                            $scope.showCustomFilterBox = !$scope.showCustomFilterBox
                            $scope.filterType = ''
                            $scope.properties = false

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }


                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        $scope.updateSourceReference = function (data) {
            if (Object.keys(data).indexOf('_id')) {
                SourceReferenceService.put(data._id, data, function (data) {
                    console.log(data)

                }, function (err) {
                    $rootScope.showMessage('Save', err.msg, 'error');
                });
            }
        }

        $scope.initGrid = function () {
            $scope.gridOptions = {
                columnDefs: [],
                rowData: null,
                enableSorting: true,
                checkboxSelection: true,
                enableColResize: true,
                suppressMenuHide: true,
                enableFilter: true,
                rowSelection: 'multiple',
                defaultColDef: {
                    width: 100,
                    filter: 'agTextColumFilter'
                },
                onCellDoubleClicked: cellFocused,
            };

        }


        $scope.createFlow = function () {
            query = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess
            }
            FlowsService.generateFlow('dummy', query, function (data) {
                $rootScope.showMessage('Flow', data, 'success');
                $scope.initProcess()
                $scope.refreshReconView()
            })
        }


        $scope.initGrid()
        $scope.createRecon = function (data) {
            $scope.create = true;
            $scope.present = 'defineSource';
            $scope.next = 'selectMatchColumns';
        };


        $scope.downloadFile = function () {
            query = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess

            }

            MatchRuleReferenceService.downloadReports('dummy', query, function (data) {
                var filePath = 'files/Outputs/'
                filePath = filePath + data
                $window.open(filePath)
            }, function (err) {
                $rootScope.showMessage('Download', err.msg, 'error');
            });

        }

        $scope.editRecon = function (recon) {
            $scope.reconEdit = true
            $scope.initiVaiables()
            $scope.recon.reconName = recon.reconName
            $scope.recon.reconProcess = recon.reconProcess
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v.reconName == recon.reconName) {
                    $scope.sources = v.sources
                }
            })
            $scope.sources = recon.sources
            $scope.createRecon()
            $scope.listSource = true
            $scope.properties = false
        }

        function getRandomColor() {
            var letters = 'BCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
        }

        $scope.marginLeft = 0

        $scope.getStyle = function (index) {

            if (index = 0) {
                $scope.marginLeft = $scope.marginLeft + 110
            }
            else {
                $scope.marginLeft = $scope.marginLeft + 600
            }
            return {
                'margin-left': $scope.marginLeft
            }
        }

        $scope.updateReconDetails = function () {
            var perColConditions = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess
                }
            }
            ReconsService.get(undefined, {'query': perColConditions}, function (data) {
                console.log(data)
                $scope.recon.sources = $scope.sources
                if (data['total'] == 1) {
                    data = data['data'][0]
                    ReconsService.put(data._id, $scope.recon, function (data) {
                        $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
                    }, function (err) {
                        $rootScope.showMessage('Save', err.msg, 'error');
                    });
                }
                else {
                    ReconsService.post($scope.recon, function (data) {
                        console.log(data);
                    });
                }
            })
        }

        $scope.nextStep = function (present) {
            $scope.prev = present;
            $scope.present = $scope.next;
            $scope.next = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) + 1];
            $scope.active[present] = true;
            if ($scope.prev == 'defineSource') {
                // $scope.recon.sourceCount = $scope.sourceDetails.length;
                $scope.updateReconDetails()
                var perColConditions = {
                    'perColConditions': {
                        'reconName': $scope.recon.reconName,
                    }
                }

                MatchRuleReferenceService.get(undefined, {'query': perColConditions}, function (data) {

                    if (data['total'] == 0) {
                        $scope.selectMatchType()
                    } else {
                        $rootScope.flowelements = data['data'][0]['Rules']
                        if ($rootScope.flowelements.length > 0) {
                            angular.forEach($rootScope.flowelements, function (a, b) {
                                a['active'] = false
                                a['connections'] = (angular.isDefined(a['connections'])) ? a['connections'] : []
                                $rootScope[a['Id']] = new flowchart.ChartViewModel({
                                    'nodes': a['nodes'],
                                    'connections': a['connections']
                                });
                            })
                            $rootScope.activeFlow = $rootScope.flowelements[0]
                            $rootScope.flowelements[0]['active'] = true

                        }
                        else {
                            $scope.selectMatchType()
                        }
                    }
                }, function (err) {
                    $rootScope.showErrorMsg(err.msg)
                })
            }

            if ($scope.prev == 'selectMatchColumns') {
                $rootScope.openModalPopupOpen()
                var completeRules = []
                var perColConditions = {
                    'perColConditions': {
                        'reconName': $scope.recon.reconName,
                        'reconProcess': $scope.recon.reconProcess

                    }
                }
                var flowdata = {}
                flowdata = {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess
                }
                angular.forEach($rootScope.flowelements, function (val, key) {
                        completeRules.push({
                            'Id': val['Id'],
                            'title': val['title'],
                            'flowSources': val['flowSources'],
                            'nodes': $rootScope[val['Id']]['data']['nodes'],
                            'connections': $rootScope[val['Id']]['data']['connections']
                        })
                    }
                )
                flowdata['Rules'] = completeRules
                MatchRuleReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                    console.log(data)
                    if (data['total'] == 1) {
                        data = data['data'][0]
                        MatchRuleReferenceService.put(data._id, flowdata, function (data) {
                            $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
                        }, function (err) {
                            $rootScope.showMessage('Save', err.msg, 'error');
                        });
                    }
                    else {
                        MatchRuleReferenceService.post(flowdata, function (data) {
                            console.log(data);
                        });
                    }
                    var dataR = {}
                    dataR.reconName = $scope.recon.reconName
                    dataR.reconProcess = $scope.recon.reconProcess
                    dataR.matchRules = $scope.ruleDefined
                    MatchRuleReferenceService.getSelectedColumns($scope.matchId, dataR, function (data) {
                        $scope.sourceData = data;
                        $scope.sorcesDataSrc = $scope.sourceData['souces']
                        delete($scope.sourceData['souces'])
                        $rootScope.openModalPopupClose()
                    }, function (err) {
                        $rootScope.openModalPopupClose()
                        $rootScope.showMessage('Job Error', err.msg, 'error');
                    });
                })
            }
            if ($scope.prev == 'defineSumColumns') {
                var data = {}
                data.reconName = $scope.recon.reconName
                data.reconProcess = $scope.recon.reconProcess
                data.matchRules = $scope.ruleDefined
                data.sumColumns = $scope.sourceData
                $scope.matchSummary = {}
                $rootScope.openModalPopupOpen()
                MatchRuleReferenceService.saveMatchRule($scope.matchId, data, function (data) {
                    $scope.matchSummary = data
                    $rootScope.openModalPopupClose()

                }, function (err) {
                    $rootScope.showMessage('Save', err.msg, 'error');
                })
            }
        };

        $scope.previousStep = function (present) {
            $scope.next = present;
            $scope.present = $scope.prev;
            $scope.active[present] = false;
            if ($scope.configurationSteps.indexOf($scope.prev) != 0) {
                $scope.prev = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) - 2];
            }
            if ($scope.present == 'selectMatchColumns') {
                $scope.resultGridOptions.columnDefs = []
                $scope.resultGridOptions.rowData = null;
                $scope.resultGridOptions.api.setRowData = $scope.gridOptions.rowData
                $scope.resultGridOptions.api.columnDefs = $scope.gridOptions.columnDefs;
                $scope.sourceData = {}
            }
        };

        $scope.skipRowFunction = function () {
            $scope.selectedRows = $scope.gridOptions.api.getSelectedNodes()
            $scope.skiprows = $scope.selectedRows[0].childIndex + 1
            if ($scope.selectedRows.length > 1) {
                $scope.skipfooter = $scope.gridOptions.api.getDisplayedRowCount() - $scope.selectedRows[1].childIndex

            }
            console.log($scope.skiprows)
            console.log($scope.skipfooter)
            var query = {}
            query = {
                'fileName': $scope.fileName,
                'skiprows': $scope.skiprows,
                'skipfooter': $scope.skipfooter,
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName
            }
            SourceReferenceService.skipRows('dummy', query, function (data) {
                if (data) {
                    $scope.updateDetails(data);
                }
                $rootScope.openModalPopupClose()
                $scope.fileUploaded = true;
                $rootScope.displayMessage('Rows Skipped Successfully', 'md', 'success');

            });
        }


        $scope.initializeCustomFilter = function () {
            $scope.showCustomFilterBox = true
        }


        $scope.viewProperties = function (sourceName) {

            $scope.hidePrev = false;

            var query = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess,
                    'source': sourceName
                }
            };
            $scope.feedDef = {}
            SourceReferenceService.get(undefined, {'query': query}, function (data) {
                    $scope.feedDef = data.data[0];
                    $scope.properties = true;
                    $scope.listSource = false;
                    $scope.showCustomFilterBox = false
                }, function (err) {
                    $rootScope.showMessage('Save', err.msg, 'error');
                }
            );
        }

        $scope.saveProperties = function (data) {
            if ($scope.addSource) {
                if ($scope.sources.indexOf($scope.sourceName) == -1 && $scope.sourceName != '') {
                    $scope.sources.push($scope.sourceName)
                }
            }
            $scope.properties = false
            SourceReferenceService.put(data._id, data, function (data) {
                $scope.updateDerivedCols($scope.feedDef.filters)
                $scope.feedDetails = $scope.feedDef.feedDetails
                $scope.oldSourceName = $scope.sourceName
                $scope.sourceName = $scope.feedDef.source
                // $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
            }, function (err) {
                $rootScope.showMessage('Save', err.msg, 'error');
            });
        };

        $scope.addSourceToRecon = function () {
            if ($scope.oldSourceName != $scope.sourceName && $scope.oldSourceName != undefined) {
                var index = $scope.sources.indexOf($scope.oldSourceName)
                $scope.sources[index] = $scope.sourceName
                $scope.updateReconDetails()
            }
            $scope.newStrbutton=true
            $scope.listSource = true
            $scope.customFilters = []
            $scope.saveProperties($scope.feedDef)
            $scope.addSource = false
            $scope.editingSource = false
            $scope.sourceName = ''
            $rootScope.showMessage('Save', $scope.resources.updateSuccess, 'success');
        }

        // $scope.cancelProperties = function () {
        //     if (!$scope.addSource) {
        //         $scope.listSource = true
        //     }
        //     else {
        //         $scope.listSource = false
        //     }
        //     $scope.properties = false
        // }

        $scope.addNewSource = function (source) {
            $scope.editingSource = false
            if (source == undefined) {
                $scope.sourceName = ''
                $scope.gridOptions.columnDefs = [];
                $scope.gridOptions.rowData = [];
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
            }
            else {
                $scope.editingSource = true
                $scope.sourceName = source
            }
            $scope.properties = false
            $scope.listSource = false;
            $scope.addSource = true;
        }


        // Uploading source Defination file
        $scope.uploadSourceFileVar = false;
        $scope.uploadSourceFile = function (source) {

            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload";
                    $scope.upload = function () {

                        var file = $scope.uploadFile;
                        var fileName = file.name
                        var uploadUrl = '/api/no_auth/source/upload';
                        $rootScope.openModalPopupOpen()
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            var structKey = $scope.secondStructure

                            $scope.updateSourceDetails(source, file)

                            $uibModalInstance.dismiss('cancel');
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.sampleFile = false
                    };
                }
            });
        }

        //Function load sample file in filter section
        $scope.loadFile = function (val) {
            $scope.closeData = false
            $scope.hidePrev = true
            if (true) {
                var query = {
                    'reconName': $scope.recon.reconName,
                    'source': val,
                    'saveSourceDetails': false
                }
                SourceReferenceService.loadSampleFile('dummy', query, function (data) {
                    if (data['fileLoaded']) {
                        $scope.updateDetails(data)
                    }
                    else {
                        $scope.updateDetails(data)
                        $rootScope.showMessage('File', data.msg, 'error')
                    }
                }, function (err) {
                    $rootScope.showMessage('File', err.msg, 'error')
                })
                $scope.addNewSource(val)
            }
            else {
                $scope.updateDetails($scope.sampleData)
            }
        }
        // Options For selecting excel sheet Name
        $scope.selectSheets = function (source, data) {
            $scope.sheetNames = data['sheetNames']
            $scope.doc = data
            $uibModal.open({
                templateUrl: 'sheetName.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = $rootScope.resources.selectSheet
                    $scope.submit = function () {
                        $scope.doc['sheetNames'] = $scope.sheetName
                        $scope.doc['checkFileProperty'] = true
                        $scope.doc['saveSourceDetails'] = data['saveSourceDetails']
                        $scope.doc['secondStructure'] = $scope.secondStructure

                        if ($scope.sheetNames == true) {
                            $scope.sheetNames == 0
                        }
                        SourceReferenceService.saveSourceDetails('dummy', $scope.doc, function (data) {
                            if (data) {
                                // $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $uibModalInstance.dismiss('cancel');
                        }, function (err) {
                            console.log(err.msg)
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.sampleFile = false

                    };
                }
            })
        }


        // confirmation while uploading sample file to upload source Feed Details
        $scope.updateSourceDetails = function (source, file) {
            $scope.sampleFile = true
            $uibModal.open({
                templateUrl: 'app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.deleteMsg = "Do you want to update sourceDetails?"
                    $scope.deleteTitle = "Source Details"

                    $scope.submit = function () {
                        $scope.saveDetails = true
                        var perColConditions = {
                            'perColConditions': {
                                'reconName': $scope.recon.reconName,
                                'reconProcess': $scope.recon.reconProcess
                            }
                        }

                        ReconsService.get(undefined, {'query': perColConditions}, function (data) {
                            if (!angular.isDefined($scope.recon.sources)) {
                                $scope.recon.sources = []
                            }
                            if ($scope.recon.sources.indexOf($scope.sourceName) == -1) {
                                $scope.recon.sources.push($scope.sourceName)
                            }
                            if (data['total'] == 1) {
                                data = data['data'][0]
                                ReconsService.put(data._id, $scope.recon, function (data) {
                                    $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
                                }, function (err) {
                                    $rootScope.showMessage('Save', err.msg, 'error');
                                });
                            }
                            else {
                                ReconsService.post($scope.recon, function (data) {
                                    console.log(data);
                                });
                            }
                        })

                        $uibModalInstance.dismiss('cancel');
                        var query = {}
                        query = {
                            'fileName': file.name,
                            'skipRows': 0,
                            'skipfooter': 0,
                            'reconName': $scope.recon.reconName,
                            'reconProcess': $scope.recon.reconProcess,
                            'source': source,
                            'saveSourceDetails': $scope.saveDetails,
                            'secondStructure': $scope.secondStructure
                        }

                        SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                            $rootScope.openModalPopupClose()
                            if (Object.keys(data).indexOf('sheetNames') != -1) {
                                data['saveSourceDetails'] = $scope.saveDetails
                                $scope.selectSheets(source, data)
                            }
                            else {
                                // $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $scope.fileUploaded = true;
                            $rootScope.showMessage("Save", $scope.resources.fileUpload, 'success');

                        }, function (err) {
                            $rootScope.showMessage('Error', err.msg, 'error')
                        });
                    }

                    $scope.cancel = function () {
                        $scope.saveDetails = false
                        var query = {}
                        query = {
                            'fileName': file.name,
                            'skipRows': 0,
                            'reconName': $scope.recon.reconName,
                            'reconProcess': $scope.recon.reconProcess,
                            'source': source,
                            'saveSourceDetails': $scope.saveDetails
                        }
                        SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                            $rootScope.openModalPopupClose()
                            if (Object.keys(data).indexOf('sheetNames') != -1) {
                                data['saveSourceDetails'] = $scope.saveDetails
                                $scope.selectSheets(source, data)
                            }
                            else {
                                $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $scope.fileUploaded = true;
                            $rootScope.showMessage("Save", $scope.resources.fileUpload, 'success');

                        }, function (err) {
                            $rootScope.showMessage('Save', err.msg, 'error')
                        });
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        // Function to update source data in to Grid
        $scope.updateDetails = function (data) {
            $scope.sampleData = data
            $scope.columnDefs = [{
                headerName: '',
                'width': 30,
                checkboxSelection: true,
                suppressAggregation: true,
                suppressSorting: true,
                suppressMenu: true,
                pinned: true
            }]
            $scope.defineSource = true;
            $scope.uploadSourceFileVar = true
            $scope.sourceDetails = data.sourceDetails;
            // $scope.expressions = data.filters
            $scope.derivedColumns = data.derivedColumns
            $scope.feedDetails = data.feedDetails
            angular.forEach(data.feedDetails, function (k, v) {
                if (k.dataType == 'str') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agTextColumnFilter',
                        'editable': true,
                        'filterParams': {'caseSensitive': true}
                    });
                }
                else if (k.dataType == 'np.int64' || k.dataType == 'np.float64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agNumberColumnFilter',
                        'editable': true
                    });
                }
                else if (k.dataType == 'np.datetime64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agDateColumnFilter',
                        'editable': true
                    });
                }
                else {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agSetColumnFilter',
                        'editable': true
                    });
                }
            })
            $scope.gridOptions.columnDefs = $scope.columnDefs;
            // $scope.gridOptions.rowData = data.data;
            if (Object.keys(data).indexOf('data') != -1) {
                $scope.gridOptions.rowData = JSON.parse(data['data']);
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
            }
            $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
        }


        function cellFocused(data) {
            $scope.value = data.value;
            $scope.column = data.colDef.field;
            // $scope.type = $scope.columnDefs[$scope.column];
        }

        $scope.$watch(window.getSelection(), function (oldValue, newValue) {
            $scope.selectedText = window.getSelection().toString()
        })

        function onCellClick() {
            console.log('cellClicked');
        }

        $scope.cellClick = 'cellClicked'


        function getText(data) {
            console.log(data);
            var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
            } else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }
            return text;
        }

        $scope.filterConditionsMasterArray = {
            'str': ['notEqual', 'equals', 'startWith', 'notStartWith', 'endsWith', 'contains', 'notContains', 'notEndsWith', 'isIn', 'notIsIn', 'subString'],
            'np.float64': ['>', '<', '>=', '<=', '!=', '==', '/', '-', '*'],
            'np.int64': ['>', '<', '>=', '<=', '!=', '==', '/', '-', '*'],
            'np.datetime64': ['changeFormat', 'formatToDate']
        }


        var stringExpressions = {
            'notEqual': "(df['$colName'].str.strip()!='$colValue')",
            'equals': "(df['$colName'].str.strip()=='$colValue')",
            'startWith': "(df['$colName'].str.strip().startswith('$colValue'))",
            'notStartWith': "(~df['$colName'].str.strip().str.startswith('$colValue'))",
            'endsWith': "(df['$colName'].str.strip().endswith('$colValue'))",
            'notEndsWith': "(~df['$colName'].str.strip().endswith('$colValue'))",
            'contains': "(df['$colName'].str.strip().str.contains('$colValue',case=False))",
            'notContains': "(~df['$colName'].str.strip().str.contains('$colValue'))",
            'isIn': "(df['$colName'].str.strip().isin('$colValue'))",
            'notIsIn': "(~df['$colName'].str.strip().isin('$colValue'))",
            'greaterThan':"(df['$colName']>$colValue)"
        }

        $scope.filters = [];
        $scope.customFilters = [];

        var numbericExpression = {
            'condition': "df['$colName'] $operator $colValue"
        };

        var dateExpression = {
            'formatToDate': "df['$colName']=pandas.to_datetime(df['$colName'],format='$colValue')",
            "changeFormat": "df['$colName']=df['$colName'].apply(lambda x: x.strftime('$colValue'))"
        }

        $scope.deleteFilter = function (index) {
            $scope.feedDef.filters.splice(index, 1)
            $scope.updateDerivedCols($scope.feedDef.filters)
        }


        $scope.saveFilterCond = function () {
            var fil = []
            if ($scope.filterCondExpArray.length > 0) {
                $scope.customFilters.push($scope.buildCustomExpression())
                $scope.filterCondExpArray = [{condValue: ''}]
            }

            angular.forEach($scope.customFilters, function (value, index) {
                if ($scope.customFilters.indexOf(value) != -1) {
                    fil.push(value)
                }
            })

            var query = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName,
                'filters': fil
            }
            SourceReferenceService.saveFilters('dummy', query, function (data) {
                console.log(data)
                $scope.updateFeedDefFilters()
                $scope.customFilters = []
            })
            $scope.showCustomFilterBox = false
            $scope.newcolumnName = ''
            $scope.valueOfColumn = ''
        }

        $scope.addFilterCond = function () {
            $scope.filterCondExpArray.push({condValue: ''})
            console.log($scope.filterCondExpArray)
        };

        $scope.reinitFilterParams = function () {
            $scope.newcolumnName = '';
            $scope.valueOfColumn = '';
            $scope.expressions = [];
            $scope.filterCondExpArray = [{condValue: ''}];
        }


        // $scope.addDeriveColumns = function () {
        //     $scope.newDerivedColumns = true
        //     $scope.addingColumn = {}
        // };

        $scope.setNewColumn = function () {
            var exp = "df['$colName'] = $colValue"
            if (Object.keys($scope.feedDef).indexOf('filters') == -1) {
                $scope.feedDef.filters = []
            }
            $scope.feedDef.filters.push(exp.replace('$colName', $scope.newcolumnName).replace('$colValue', $scope.valueOfColumn))
            if (Object.keys($scope.feedDef).indexOf('derivedColumns') == -1) {
                $scope.feedDef.derivedColumns = []
            }
            $scope.feedDef.derivedColumns.push($scope.newcolumnName)
            $scope.filterType = ''
            $scope.newcolumnName = ''
            $scope.valueOfColumn = ''
            $scope.updateSourceReference($scope.feedDef)
            $scope.showCustomFilterBox = !$scope.showCustomFilterBox
        }

        $scope.removeFilterCond = function (index) {
            $scope.filterCondExpArray.splice(index, 1)
            if ($scope.filterCondExpArray.length == 0) {
                $scope.showCustomFilterBox = false
            }
        }

        $scope.onChangeOfMdlField = function (val, index) {
            $scope.filterCondExpArray[index]['filterCondArray'] = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
            //$scope.filterCondArray = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
        }

        $scope.onChangeOfFilterCond = function (val) {
        }


        $scope.updateDerivedCols = function (filters) {
            if (filters != undefined & filters != []) {
                $scope.filterlength = filters.length
                if (!angular.isDefined(filters)) {
                    $scope.feedDef.derivedColumns = []
                }

                else {
                    $scope.feedDef.derivedColumns = []
                    for (var i = 0; i < filters.length; i++) {
                        var tmp = filters[i]
                        if (tmp.includes('=')) {
                            tmp = tmp.split('=')[0]
                            var tmp = tmp.replace(/(^.*\[|\].*$)/g, '').replace(/df|'|:/g, '').split(',');
                            angular.forEach(tmp, function (col, k) {
                                if (col.trim() != '') {
                                    if ($scope.feedDef.derivedColumns.indexOf(col) == -1) {
                                        $scope.feedDef.derivedColumns.push(col)
                                    }
                                    // if ($scope.fileColumns.indexOf(col) == -1) {
                                    //     $scope.fileColumns.push(col)
                                    // }
                                }
                            })
                        }
                    }
                }

            }
        }

        $scope.buildCustomExpression = function () {
            if ($scope.newcolumnName != undefined && $scope.newcolumnName != '') {
                var str = "df.loc["
            }
            else {
                var str = "df = df[";
            }

            for (i = 0; i < $scope.filterCondExpArray.length; i++) {

                if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'str') {
                    str += buildCustomStringCondition($scope.filterCondExpArray[i])
                }
                else if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.int64' || $scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.float64') {
                    if (['/', '-', '+', '*'].indexOf($scope.filterCondExpArray[i]['selectedCond']) !== -1) {
                        str = ""
                        str += "df" + "['" + $scope.filterCondExpArray[i]['selectedField']['fileColumn'] + "'] = "
                        str += buildCustomNumericCondition($scope.filterCondExpArray[i])
                        return str
                    }
                    else {

                        str += buildCustomNumericCondition($scope.filterCondExpArray[i])
                    }
                }

                else if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.datetime64') {
                    str = ""

                    str += buildCustomDateCondition($scope.filterCondExpArray[i])
                    return str
                }

                // append logical operator if any
                if (angular.isDefined($scope.filterCondExpArray[i]['logicalOperator'])) {

                    if ($scope.filterCondExpArray[i]['logicalOperator'] != ''
                        && $scope.filterCondExpArray.length - 1 > i) {
                        str += ' ' + $scope.filterCondExpArray[i]['logicalOperator'] + ' '
                    }
                }
            }
            if ($scope.newcolumnName != undefined && $scope.newcolumnName != '') {
                str += ",'" + $scope.newcolumnName + "'" + "] = "
                str += $scope.valueOfColumn == undefined ? '' : $scope.valueOfColumn
            }
            else {
                str += "]"
            }
            return str;
        }

        function buildCustomDateCondition(value) {
            if (value['selectedCond'] == 'changeFormat') {
                return dateExpression[value['selectedCond']].replace('$colName', value['selectedField']['fileColumn']).replace('$colValue', value['condValue']).replace('$colName', value['selectedField']['fileColumn'])
            }
        }


        function buildCustomStringCondition(value) {

            if (value['selectedCond'] == 'isIn' || value['selectedCond'] == 'notIsIn') {
                var condValue = ''
                var condValue = '[' + value['condValue'].replace("^", "'").replace('$', "'").replace(/,/g, "','") + ']'
                return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['fileColumn']).replace('$colValue', condValue)
            } else {
                return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['fileColumn']).replace('$colValue', value['condValue'] == undefined ? '' : value['condValue'])
            }
        }

        function buildCustomNumericCondition(value) {
            return numbericExpression['condition'].replace('$colName', value['selectedField']['fileColumn']).replace('$operator', value['selectedCond']).replace('$colValue', value['condValue'])
        }


        $scope.buildExpression = function (value) {
            if(value==undefined||value==''){
                return false

            }
            else {
                            var str = "df = df["

            angular.forEach(value, function (v, k) {
                if (Object.keys(v).indexOf('operator') != -1) {
                    var exp = ''
                    var lenValue = Object.keys(v).length - 1
                    for (var i = 1; i <= lenValue; i++) {
                        exp += buildStringCondition(v['condition' + i], k);
                        if (v['operator'] == 'AND' && i < lenValue) {
                            exp += '&'
                        }
                        if (v['operator'] == 'OR' && i < lenValue) {
                            exp += '|'
                        }
                    }
                    str += exp
                }
                else {
                    str += buildStringCondition(v, k);
                }
            })
            str += "]"
            $scope.gridOptions.api.setFilterModel(null)
            $scope.gridOptions.api.onFilterChanged()

            }
            return str;

        }

        function buildStringCondition(value, column) {
            return stringExpressions[value.type].replace('$colName', column).replace('$colValue', value.filter == undefined ? '' : value.filter);
        }

        $scope.buildDeriveColumns = function (columnName, selectedText) {
            $scope.selectedText = selectedText
            var filter = []
            var filter = $scope.gridOptions.api.getFilterModel()
            if (filter != undefined) {
                angular.forEach(filter, function (v, k) {
                    $scope.filterModule = buildStringCondition(v, k)
                    $scope.filterColumn = k
                })
            }
            var query = {}
            angular.forEach($scope.expressions, function (v, k) {
                $scope.filters.push(v)
            })

            query = {
                'completeValue': $scope.value,
                'selectedColumn': $scope.column,
                'columnType': $scope.type,
                'selectedText': $scope.selectedText,
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName,
                'filters': $scope.feedDef.filters,
                'derivedColumns': $scope.derivedColumns,
                'filterModule': $scope.filterModule,
                'filterColumn': $scope.filterColumn,
                'columnName': columnName
            }
            // query['sourceDetails'] = $scope.sourceDetails
            SourceReferenceService.deriveColumns('dummy', query, function (data) {
                if (data) {
                    $scope.columnDefs = []
                    $scope.updateDetails(data)
                    $scope.updateFeedDefFilters()
                }
            })
            $scope.filterModule = ''
            $scope.filterColumn = ''
        }


        $scope.addDeriveColumn = function () {
            $scope.selectedText = window.getSelection().toString()

            $uibModal.open({
                templateUrl: 'addDeriveColumn.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: 'md',
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                    $scope.getSourceDetails = function (columnName) {
                        var query = {
                            'perColConditions': {
                                'reconName': $scope.recon.reconName,
                                'reconProcess': $scope.recon.reconProcess,
                                'source': $scope.sourceName
                            }
                        };
                        SourceReferenceService.get(undefined, {'query': query}, function (data) {
                                $scope.feedDef = data.data[0];
                                $scope.buildDeriveColumns(columnName, $scope.selectedText)
                            }, function (err) {
                                $rootScope.showMessage('Save', err.msg, 'error');
                            }
                        );
                        $uibModalInstance.dismiss('cancel')

                    }

                }
            })


        };

        document.addEventListener('mouseup', function () {
            console.log(window.getSelection().toString());
        });

        $rootScope.chart = {}

        $scope.sources = []
        $scope.saveSource = function () {
            if ($scope.sources.indexOf($scope.sourceName) == -1 && $scope.sourceName != '') {
                $scope.sources.push($scope.sourceName)
            }
            $scope.addSource = false
            $scope.sourceName = ''
            $scope.listSource = true
        }

// Matching Rule Define section starts
        $scope.addNewMatchRule = function (mode, index) {
            $scope.mode = mode
            $scope.matchRules = {}
            if ($scope.mode == 'Add') {
                if (index == 'selfMatchSource') {
                    $scope.selfMatch = true
                }
                else {
                    $scope.selfMatch = false
                    angular.forEach($scope.sources, function (k, v) {
                        $scope.matchRules[k] = {'matchColumns': [], 'sumColumns': [], 'debitCreditColum': []};
                    });

                }
            }
            if ($scope.mode == 'Edit') {
                $scope.ruleName = $scope.ruleDefined[index]['ruleName']
                $scope.matchRules = $scope.ruleDefined[index]['columns']
            }
            $uibModal.open({
                templateUrl: 'ruleDefine.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: 'md',
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                    $scope.createRule = function (sourceName) {
                        $scope.matchRules[sourceName] = {'matchColumns': [], 'sumColumns': []};
                    }
                    $scope.saveRule = function (ruleName) {
                        if ($scope.mode == 'Edit') {
                            $scope.ruleDefined[index]['ruleName'] = ruleName;
                            $scope.ruleDefined[index]['columns'] = $scope.matchRules
                        }
                        else {
                            $scope.ruleDefined.push({'ruleName': ruleName, 'columns': $scope.matchRules})

                        }
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }

        $scope.refreshReconJob = function () {
            var query = {}
            query['reconName'] = $scope.recon.reconName

            ReconDetailsService.getReportDetails('dummy',
                query, function (data) {
                    $scope.reportDetails = data.details;

                    angular.forEach($scope.reportDetails, function (v, k) {
                        $scope.amounts.Matched[v.Source] = ''
                        $scope.amounts.Exception[v.Source] = ''
                        $scope.amounts.Matched[v.Source] = v['Matched Amount'];
                        $scope.amounts.Exception[v.Source] = v['UnMatched Amount'];
                    });

                    $rootScope.openModalPopupClose();
                }, function (err) {
                    $rootScope.openModalPopupClose();
                    $scope.initGrid()
                    $scope.show = false
                    $rootScope.showMessage
                    ($rootScope.resources.message, err.msg, 'warning');
                });
        }


        $scope.pos = {}


        $scope.deleteSource = function (val) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconsService) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteReconMsg']

                    $scope.submit = function () {
                        $scope.feedDef = {'filters': []}
                        var index = $scope.sources.indexOf(val)
                        $scope.sources.splice(index, 1)
                        query = {}
                        query['source'] = val
                        query['reconName'] = $scope.recon.reconName
                        query['reconProcess'] = $scope.recon.reconProcess
                        SourceReferenceService.deleteSource('dummy', query, function (data) {
                            $uibModalInstance.dismiss('cancel');
                            $rootScope.showMessage('Delete', data, 'success');

                        }, function (err) {
                            $rootScope.showMessage('Delete', err.msg, 'error');
                        })

                        var perColConditions = {
                            'perColConditions': {
                                'reconName': $scope.recon.reconName,
                                'reconProcess': $scope.recon.reconProcess
                            }
                        }

                        ReconsService.get(undefined, {'query': perColConditions}, function (data) {
                            console.log(data)
                            $scope.recon.sources = $scope.sources

                            if (data['total'] == 1) {
                                data = data['data'][0]
                                ReconsService.put(data._id, $scope.recon, function (data) {
                                    $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
                                }, function (err) {
                                    $rootScope.showMessage('Save', err.msg, 'error');
                                });
                            }
                            else {
                                ReconsService.post($scope.recon, function (data) {
                                    console.log(data);
                                });
                            }


                        })


                    }

                    $scope.cancel = function () {
                        $scope.refreshReconView()
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }


        $scope.deleteRecon = function (val, index) {
            var index = index
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconsService) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteReconMsg']
                    $scope.values = $scope.processRecons[index]

                    $scope.submit = function () {
                        ReconsService.deleteData('dummy', $scope.values, function (data) {
                            $uibModalInstance.dismiss('cancel');
                            $scope.refreshReconView()
                            $scope.processRecons.splice(index, 1)
                            $rootScope.showMessage($rootScope.resources.delete, $rootScope.resources.deleteReconSucc, 'success')
                        }, function (err) {
                            console.log(err)
                        })


                    }

                    $scope.cancel = function () {
                        $scope.refreshReconView()
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            console.log($scope.reconDetails[index])

        }


        $scope.deleteColumns = function (index) {
            $scope.feedDef.feedDetails.splice(index, 1)
        }


        function getID() {
            return (Math.floor(Math.random() * 1000000000)).toString()
        }


        $rootScope.saveFlow = function () {
            var completeRules = []
            var perColConditions = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess

                }
            }
            var flowdata = {}
            flowdata = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess
            }
            angular.forEach($rootScope.flowelements, function (val, key) {
                    completeRules.push({
                        'Id': val['Id'],
                        'title': val['title'],
                        'flowSources': val['flowSources'],
                        'nodes': $rootScope[val['Id']]['data']['nodes'],
                        'connections': $rootScope[val['Id']]['data']['connections']
                    })
                }
            )
            flowdata['Rules'] = completeRules
            MatchRuleReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                console.log(data)
                if (data['total'] == 1) {
                    data = data['data'][0]
                    MatchRuleReferenceService.put(data._id, flowdata, function (data) {
                        $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
                    }, function (err) {
                        $rootScope.showMessage('Save', err.msg, 'error');
                    });
                }
                else {
                    MatchRuleReferenceService.post(flowdata, function (data) {
                        console.log(data);
                    });
                }
            })
        }


        $scope.resultGridOptions = {

            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },

            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            enableColResize: true
        };

        $scope.expandSelectedSource = function (source) {
            $scope.columnDefs = []
            $scope.rowData = $scope.sourceData[source]
            angular.forEach($scope.rowData[0], function (v, k) {
                if (k == 'MATCHING_STATUS') {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302,
                        'rowGroupIndex': 0
                    })
                }
                else {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302
                    })
                }
            })
            $scope.resultGridOptions.api.setColumnDefs($scope.columnDefs)
            $scope.resultGridOptions.api.setRowData($scope.rowData)
        }


        $scope.removeRule = function (i) {
            console.log("Removing tab: " + i);

            var perColConditions = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess,
                }
            }
            // MatchRuleReferenceService.removeRule('dummy',query,function(data){
            //     console.log(data)
            // },function(err){
            //     console.log(err)
            // })
            $scope.flowelements.splice(i, 1);
            if ($scope.flowelements.length == 0) {
                MatchRuleReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                    if (data['total'] > 0) {
                        angular.forEach(data['data'], function (k, v) {
                            MatchRuleReferenceService.deleteData(k['_id'], function (data) {
                                console.log(data)
                            }, function (err) {
                                console.log(err)
                            }, function (err) {
                                console.log(err)
                            })
                        })
                    }
                })
            }
        };

        $scope.getChartVariable = function () {
            return $rootScope[$rootScope.activeFlow['Id']]
        }


        $rootScope.setActiveFlow = function (index) {
            angular.forEach($rootScope.flowelements, function (a, b) {
                a['active'] = false
            })
            $rootScope.activeFlow = $rootScope.flowelements[index]
            if (angular.isDefined($rootScope[$rootScope.activeFlow['Id']])) {
            }
            else {
                $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
            }
            $rootScope.flowelements[index]['active'] = true
        }


        $scope.addFilter = function () {
            var filterApplied = $scope.gridOptions.api.getFilterModel();

            $scope.filters.push();
            // $scope.buildExpression(filterApplied)

            var fil = []

            angular.forEach($scope.filters, function (value, index) {
                if ($scope.filters.indexOf(value) != -1) {
                    fil.push(value)
                }
            })

            var query = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName,
                'filters': fil
            }
            SourceReferenceService.saveFilters('dummy', query, function (data) {
                $scope.updateFeedDefFilters()
                $scope.filters = []
            })


        };

        $scope.saveFilter = function (index, template) {
            $scope.feedDef['filters'][index] = template
        }

        $scope.updateFeedDefFilters = function () {
            var perColConditions = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess,
                    'source': $scope.sourceName
                }
            }


            SourceReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                    $scope.feedDef.filters = data.data[0]['filters'];
                }, function (err) {
                    $rootScope.showMessage('Save', err.msg, 'error');
                }
            );
        }


        $scope.selectMatchType = function () {
            $uibModal.open({
                templateUrl: 'addflow.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, SourceReferenceService) {


                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                    $scope.addRule = function () {
                        angular.forEach($rootScope.flowelements, function (a, b) {
                            a['active'] = false
                        })
                        var flowSources = []
                        if ($scope.isSelfMatch) {
                            flowSources.push($scope.selfMatchSource)
                            flowSources.push($scope.selfMatchSource)
                        }
                        else {
                            flowSources = $scope.sources
                        }


                        var length = $rootScope.flowelements.length + 1
                        $rootScope.flowelements.push({
                            'reconName': $scope.recon.reconName,
                            'reconProcess': $scope.recon.reconProcess,
                            'active': true,
                            'Id': getID(),
                            'title': $scope.ruleName,
                            'flowSources': flowSources
                        })
                        length = 0

                        console.log($rootScope.flowelements)

                        var perColConditions = {
                            'perColConditions': {
                                'reconName': $scope.recon.reconName,
                                'reconProcess': $scope.recon.reconProcess
                            }
                        }
                        SourceReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                            $scope.sourceDict = {};
                            angular.forEach(data.data, function (key, val) {
                                if (Object.keys($scope.sourceDict).indexOf(key.source) == -1) {
                                    $scope.sourceDict[key.source] = []
                                }
                                angular.forEach(key.feedDetails, function (colkey) {
                                    if ($scope.sourceDict[key.source].indexOf(colkey.fileColumn) == -1) {
                                        $scope.sourceDict[key.source].push(colkey.fileColumn);
                                    }
                                })
                                angular.forEach(key.derivedColumns, function (v, k) {
                                    $scope.sourceDict[key.source].push(v)
                                })
                                $scope.sourceDict[key.source].push(key.struct)


                            })
                            $scope.sourceCols = {};
                            var _x = 0
                            var _y = 0
                            var newNodeDataModel = []
                            var sourceData = []
                            if ($scope.isSelfMatch) {
                                angular.forEach(data.data, function (v, k) {
                                    if ($scope.selfMatchSource == v['source']) {
                                        sourceData.push(v)
                                        sourceData.push(v)
                                    }
                                })
                            }
                            else {
                                angular.forEach(flowSources, function (source, index) {
                                    if (_x == 0) {
                                        _x = _x + 100
                                    }
                                    else {
                                        _x = _x + 400
                                        _y = 0
                                    }
                                    $scope.sourceCols[source] = []

                                    angular.forEach($scope.sourceDict[source], function (col, key) {
                                        _y = _y + 50
                                        var newModel = {
                                            "outputConnectors": [
                                                {
                                                    "name": "1"
                                                }
                                            ],
                                            "name": col,
                                            "color": getRandomColor(),
                                            "inputConnectors": [
                                                {
                                                    "name": "X"
                                                }
                                            ],
                                            "className": source,
                                            "width": 150,
                                            "y": _y,
                                            "x": _x,
                                            "type": "val.fileColumn",
                                            "id": getID()
                                        }

                                        newNodeDataModel.push(newModel)
                                        $scope.sourceCols[source].push(col)
                                        $scope.matchColumns[source] = [];
                                        $scope.sumColumns[source] = [];
                                        //
                                        // angular.forEach(v.derivedColumns, function (val, key) {
                                        //     if ($scope.sourceCols[source].indexOf(val) == -1) {
                                        //
                                        //
                                        //         _y = _y + 50
                                        //         var newModel = {
                                        //             "outputConnectors": [
                                        //                 {
                                        //                     "name": "1"
                                        //                 }
                                        //             ],
                                        //             "name": val,
                                        //             "color": getRandomColor(),
                                        //             "inputConnectors": [
                                        //                 {
                                        //                     "name": "X"
                                        //                 }
                                        //             ],
                                        //             "className": source,
                                        //             "width": 150,
                                        //             "y": _y,
                                        //             "x": _x,
                                        //             "type": "val.fileColumn",
                                        //             "id": getID()
                                        //         }
                                        //
                                        //         newNodeDataModel.push(newModel)
                                        //         $scope.sourceCols[source].push(val)
                                        //         // $scope.matchColumns[source] = [];
                                        //         // $scope.sumColumns[source] = [];
                                        //     }
                                        // })

                                        $rootScope.activeFlow = $rootScope.flowelements[$rootScope.flowelements.length - 1]
                                        $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
                                        angular.forEach(newNodeDataModel, function (v, k) {
                                            $rootScope[$rootScope.activeFlow['Id']].addNode(v);

                                        })

                                        $scope.getChartVariable = function () {
                                            return $rootScope[$rootScope.activeFlow['Id']]
                                        }


                                        // $scope.filters[v.source] = v.filters;

                                    })

                                })

                            }
                        });

                        $uibModalInstance.dismiss('cancel')

                    }
                }
            });
        }


    }]);
