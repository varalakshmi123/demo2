/**
 * Created by root on 31/5/18.
 */
var app = angular.module("nextGen");

app.controller("forceMatchController",['$scope','$rootScope','$window','$filter','$uibModal','ReconsService','ForceMatchService','ReconDetailsService',
        function($scope, $rootScope, $window, $filter, $uibModal,ReconsService, ForceMatchService,ReconDetailsService) {

    $scope.successFlag = false
    $scope.stmtDateSearch = 0
    $scope.transactionData = []
    $scope.filterValue = ""
    $scope.selectedprimaryCol=[]
    $scope.selectedmatchCol=[]
    $rootScope.forceMatchLog = false

            $scope.primaryGridOptions = {

                defaultColDef: {
                    // allow every column to be aggregated
                    enableValue: true,
                    // allow every column to be grouped
                    enableRowGroup: true,
                    // allow every column to be pivoted
                    enablePivot: true,

                },
                columnDefs: [],
                enableSorting: true,
                showToolPanel: true,
                rowMultiSelectWithClick: true,
                enableFilter: true,
                rowSelection: "multiple",
                checkboxSelection: true,
                rowGroupPanelShow: 'always',
                pagination: true,
                onSelectionChanged: selectionChangedFunc
            }

            $scope.matchGridOptions = {

                defaultColDef: {
                    // allow every column to be aggregated
                    enableValue: true,
                    // allow every column to be grouped
                    enableRowGroup: true,
                    // allow every column to be pivoted
                    enablePivot: true,

                },
                columnDefs: [],
                enableSorting: true,
                showToolPanel: true,
                rowMultiSelectWithClick: true,
                enableFilter: true,
                rowSelection: "multiple",
                checkboxSelection: true,
                rowGroupPanelShow: 'always',
                pagination: true,
                onSelectionChanged: selectionChangedFunc
            }


    function selectionChangedFunc() {

    }

    $scope.inlineOptions = {
        maxDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.maxDate = $scope.inlineOptions.maxDate ? null : new Date();
        $scope.dateOptions.maxDate = $scope.inlineOptions.maxDate;
    };
    $scope.selectColumns = []
    $scope.toggleMin();

    $scope.changeReconProcess = function () {
        $scope.filteredRecons = []
        $scope.filteredRecons = $rootScope.recons[$scope.selReconProcess]
    }

    // $scope.resetForceVariable = function () {
    //     $scope.primarySrc = ''
    //     $scope.matchSrc = ''
    //     $scope.primaryColumns = []
    //     $scope.matchColumns = []
    //     $scope.selectedprimaryCol = []
    //     $scope.selectedmatchCol = []
    //     $scope.transactionData = []
    //     $scope.primaryGridOptions.columnDefs = []
    //     $scope.primaryGridOptions.rowData = []
    //     $scope.primaryGridOptions.api.setColumnDefs([])
    //     $scope.primaryGridOptions.api.setRowData([])
    //
    //
    // }
    $scope.deleteExpression = function (index, key) {
        $scope[key].splice(index, 1)
    }
    $scope.addColumns = function (prsrc, masrc) {
        $uibModal.open({
            templateUrl: 'primary.html',
            windowClass: 'modal show',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $uibModalInstance) {
                $scope.addColumn = function (prColum, matchCol) {
                    $scope.subprocess(prColum, matchCol)
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        })
    }

    $scope.getPrimaryData = function (prsrc, masrc, index,isForceMatch) {
        data = {}
        $scope.selectColumns.push(index)
        data['primaryName'] = prsrc
        data['matchName'] = masrc
        data['reconName'] = $scope.selRecon

        if ($scope.transactionData.length == 0) {
            // $rootScope.openModalPopupOpen()
            ForceMatchService.readReportData('dummy', data, function (data) {
                // if (!angular.isDefined(isForceMatch)) {
                //     $scope.addColumns($scope.primarySrc, $scope.matchSrc)
                // }

                console.log(data)
                console.log('*****************')
                $scope.primaryColumns = data['primaryCol']
                $scope.matchColumns = data['matchCol']

                if (Object.keys(data).length == 0) {

                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        $rootScope.showAlertMsg("No Records Found in DB.")
                    }


                }
                else if (data['total'] == 0) {
                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        $rootScope.showAlertMsg($rootScope.resources['noDataFound'])
                    }
                }

                else {
                    //load data
                    $scope.columnDefs = [{
                        headerName: '',
                        'width': 30,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,
                        pinned: true
                    }]

                    $scope.transactionData = data['primaryData']
                    hideFields = []
                    // check if model fields are configured, if not show all columns
                    angular.forEach($scope.transactionData[0], function (v, k) {
                        $scope.columnDefs.push({
                            'headerName': k,
                            'field': k,
                            'width': 302,
                        })
                    })

                    $scope.primaryGridOptions.columnDefs = $scope.columnDefs;
                    $scope.primaryGridOptions.rowData = $scope.transactionData;
                    $scope.primaryGridOptions.api.setColumnDefs($scope.columnDefs)
                    $scope.primaryGridOptions.api.setRowData($scope.transactionData)
                }
                $rootScope.openModalPopupClose()
                if (angular.isDefined(isForceMatch)) {
                    $rootScope.showSuccessMsg('Force Match Done Successfully');
                }
            })
        }
        else {
            // $scope.addColumns($scope.primarySrc, $scope.matchSrc)
            // $rootScope.openModalPopupClose()

        }

    }

    var filterCount = 0;
    $scope.onFilterChanged = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.primaryGridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }



    $scope.searchSelected = function () {
        $scope.showdiscovery = true
        console.log($scope.primaryGridOptions.api.getSelectedRows())
        data = {}
        if ($scope.primaryGridOptions.api.getSelectedRows().length == 0) {
            $scope.showdiscovery = false
            $rootScope.displayMessage("Please select the Records.",'md','warning')
        }
        else {
            data['selectedRows'] = $scope.primaryGridOptions.api.getSelectedRows()
            data['matchColumns'] = $scope.selectedmatchCol
            data['primaryColumns'] = $scope.selectedprimaryCol
            data['matchSource'] = $scope.matchSrc
            data['reconName'] = $scope.selRecon
            ForceMatchService.readMatchData('dummy', data, function (data) {
                console.log(data)
                console.log('*****************')

                if (data['total'] == 0) {
                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        $rootScope.showAlertMsg($rootScope.resources['noDataFound'])
                    }
                } else {
                    //load data
                    $scope.columnDefs = [{
                        headerName: '',
                        'width': 30,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,
                        pinned: true
                    }]

                    $scope.transactionData = data['matchSourceData']
                    hideFields = []
                    // check if model fields are configured, if not show all columns
                    angular.forEach($scope.transactionData[0], function (v, k) {
                        $scope.columnDefs.push({
                            'headerName': k,
                            'field': k,
                            'width': 302,
                        })
                    })

                    $scope.matchGridOptions.columnDefs = $scope.columnDefs;
                    $scope.matchGridOptions.rowData = $scope.transactionData;
                    $scope.matchGridOptions.api.setColumnDefs($scope.columnDefs)
                    $scope.matchGridOptions.api.setRowData($scope.transactionData)
                }


                $scope.showdiscovery = false

            })
        }


    }

    $scope.addPrimayCol = function(primary,index){
        if (!angular.isDefined($scope.selectedprimaryCol)) {
            $scope.selectedprimaryCol = []
        }
        if($scope.selectedprimaryCol.indexOf(primary)==-1){
            $scope.selectedprimaryCol[index] = primary
        }
        else{
            $rootScope.displayMessage('Column Already Selected','md','warning')
        }
    }

    $scope.addMatchCol = function(match,index){
        if (!angular.isDefined($scope.selectedmatchCol)) {
            $scope.selectedmatchCol = []
        }
        if($scope.selectedmatchCol.indexOf(match)==-1){
            $scope.selectedmatchCol[index] = match
        }
        else{
            $rootScope.displayMessage('Column Already Selected','md','warning')
        }
    }

    $scope.addSourceColumns = function () {
        $$uibModal.open({
            templateUrl: 'columns.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $uibModalInstance) {
                console.log('hi')

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }

        })
    }

    $scope.loadTxnData = function () {
        if (!angular.isDefined($scope.selRecon) && !angular.isDefined($scope.selReportDetail['source'])) {
            return ''
        }

        var hideFields = ['#id']
        var dataR = {}
        dataR['reconName'] = $scope.selRecon
        dataR['matchStatus'] = $scope.view
        dataR['source'] = $scope.selReportDetail['source']

        if ($scope.stmtDateSearch != 0) {
            dataR['statementDate'] = $filter('date')(new Date($scope.toEpoch($scope.stmtDateSearch) * 1000), 'dd/MM/yy')
        } else {
            dataR['statementDate'] = ''
        }

        $scope.primaryGridOptions.columnDefs = [];
        $scope.primaryGridOptions.rowData = [];
        $scope.primaryGridOptions.api.setColumnDefs([])
        $scope.primaryGridOptions.api.setRowData([])

        $rootScope.openModalPopupOpen()
        ReconAssignmentService.loadManualReconData('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                $scope.transactionData = [];
                $scope.exportCsvFile = data['export_file']
                $scope.txnCount = data['total']
                $scope.objectId = data['objectId']
                if (data['total'] == 0) {
                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        $rootScope.showAlertMsg($rootScope.resources['noDataFound'])
                    }
                } else {
                    //load data
                    $scope.columnDefs = [{
                        headerName: '',
                        'width': 30,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,
                        pinned: true
                    }]

                    $scope.transactionData = JSON.parse(data['data'])

                    // check if model fields are configured, if not show all columns
                    if (angular.isDefined(data['sortColumns'])) {
                        $scope.orderColumns = []
                        data['sortColumns'] = JSON.parse(data['sortColumns'])
                        angular.forEach(data['sortColumns'], function (v, k) {
                            $scope.columnDefs.push(
                                {
                                    'headerName': v['UI_DISPLAY_NAME'],
                                    'field': v['MDL_FIELD_ID'],
                                    'width': 302,
                                })
                        })
                    } else {
                        angular.forEach($scope.transactionData[0], function (v, k) {
                            if (hideFields.indexOf(k) == -1) {
                                $scope.columnDefs.push(
                                    {
                                        'headerName': k,
                                        'field': k,
                                        'width': 302,
                                    })
                            }
                        })
                    }

                    $scope.primaryGridOptions.columnDefs = $scope.columnDefs;
                    $scope.primaryGridOptions.rowData = $scope.transactionData;
                    $scope.primaryGridOptions.api.setColumnDefs($scope.columnDefs)
                    $scope.primaryGridOptions.api.setRowData($scope.transactionData)
                }
            },
            function (err) {
                $rootScope.openModalPopupClose()
                console.log(err.msg)
                $rootScope.showAlertMsg(err.msg)
            })
    }

    $scope.forceMatch = function (comments) {
        var dataR = {}
        dataR['primaryData'] = $scope.primaryGridOptions.api.getSelectedRows()
        dataR['matchData'] = $scope.matchGridOptions.api.getSelectedRows()
        dataR['comments'] = comments
        // dataR['objectId'] = $scope.objectId
        dataR['matchSource'] = $scope.matchSrc
        dataR['primarySource'] = $scope.primarySrc
        dataR['reconName'] = $scope.selRecon
        dataR['reconProcess'] = $scope.selReconProcess
        $rootScope.openModalPopupOpen()
        ForceMatchService.forceMatchReconData('dummy', dataR, function (data) {

            $scope.transactionData = []
            $scope.getPrimaryData($scope.primarySrc, $scope.matchSrc, 'ForceMatch')
            $scope.successFlag = true
            $scope.matchGridOptions.columnDefs = [];
            $scope.matchGridOptions.rowData = [];
            $scope.matchGridOptions.api.setColumnDefs([])
            $scope.matchGridOptions.api.setRowData([])
            $scope.comments = ''
            // $rootScope.openModalPopupClose()
        }, function (err) {
            $rootScope.openModalPopupClose()
            $rootScope.displayMessage(err.msg,'md','warning')
        })
        console.log($scope.selectedRecords)
    }

    $scope.selectAllRows = function (grid) {
        if ($scope[grid].api.isAnyFilterPresent()) {
            $scope[grid].api.forEachNodeAfterFilter(function (node) {
                node.setSelected(true);
            });
        } else {
            $scope[grid].api.selectAll();
        }
    }

    $scope.clearAllRowSelection = function (grid) {
        $scope[grid].api.deselectAll();
    }

    $scope.onBtExport = function () {
        var filePath = window.location.origin + '/app/files/recon_reports/'
        $window.open(filePath + $scope.exportCsvFile)
    }

    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };

    $scope.clearFilter = function () {
        $scope.stmtDateSearch = 0
        // $scope.loadTxnData()
    }

    $scope.loadSources = function () {
        $scope.primarySrcList = []
        ReconDetailsService.getReports('dummy',{'reconName':$scope.selRecon},function (data) {
            $scope.report = data
            angular.forEach(data['reportDetails'], function (v, k) {
                if (v['reportPath'].indexOf('UnMatched') != -1) {
                    $scope.primarySrcList.push(v['source'])
                }
            })
        },function (err) {
          console.log(data)
        })

    }

    $scope.loadMatchSources = function (primarySrc) {
        $scope.selectColumns = []
        $scope.matchSrcList = []
        $scope.matchSrc = ''
        angular.forEach($scope.primarySrcList, function (v, k) {
            if (angular.isDefined(primarySrc) && v != primarySrc) {
                $scope.matchSrcList.push(v)
            }
        })
        $scope.transactionData = []
        $scope.selectedprimaryCol = []
        $scope.selectedmatchCol = []
        $scope.primaryGridOptions.columnDefs = []
        $scope.primaryGridOptions.rowData = []
        $scope.primaryGridOptions.api.setColumnDefs([])
        $scope.primaryGridOptions.api.setRowData([])


    }

    $scope.removeCol = function(index){
        $scope.selectColumns.splice(index,1)
        $scope.selectedprimaryCol.splice(index,1)
        $scope.selectedmatchCol.splice(index,1)
    }

}]);