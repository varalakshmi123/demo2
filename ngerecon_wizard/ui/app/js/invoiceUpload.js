var app = angular.module('nextGen');
app.controller('invoiceUploadController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'ReconDetailsService',
    '$http', 'ReconExecDetailsLogService',
    'ForceMatchService', 'ReconsService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, ReconDetailsService, $http, ReconExecDetailsLogService, ReconsService, SourceReferenceService) {
        var inVoiceColumns = ['NEFT Number', 'NEFT Date', 'Remarks']
        $scope.gridOptions = {
            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },
            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            enableColResize: true,
            groupSelectsChildren: true,
            groupSelectsFiltered: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            onSelectionChanged: $scope.getPrimaryLength
        };


        $scope.updateDetails = function (data) {
            $scope.sampleData = data
            $scope.columnDefs = [{
                headerName: '',
                'width': 30,
                checkboxSelection: true,
                suppressAggregation: true,
                suppressSorting: true,
                suppressMenu: true,
                pinned: true
            }]
            angular.forEach(data.feedDetails, function (k, v) {
                if (k.dataType == 'str') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agTextColumnFilter',
                    });
                }
                else if (k.dataType == 'np.int64' || k.dataType == 'np.float64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agNumberColumnFilter',
                    });
                }
                else if (k.dataType == 'np.datetime64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agDateColumnFilter',
                    });
                }
                else {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agSetColumnFilter'
                    });
                }
            })
            for (var i = 0; i < inVoiceColumns.length; i++) {
                $scope.columnDefs.push({
                    'field': inVoiceColumns[i],
                    'editable': true
                })
            }
            $scope.gridOptions.columnDefs = $scope.columnDefs;
            $scope.gridOptions.rowData = data['data'];
            $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
            $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
            $rootScope.openModalPopupClose()

        }

        $scope.activeRecon = 'Abc'
        $scope.activeRecon = 'Abc'
        $scope.uploadInvoiceFile = function () {
            $uibModal.open({
                templateUrl: '/app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService, SourceReferenceService) {
                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/source/upload"; //Url of webservice/api/server
                        var filePath = "/files/reconImg"
                        $rootScope.openModalPopupOpen()
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.fileName = data['msg'].fileName;
                            $uibModalInstance.dismiss('cancel');
                            var query = {}
                            query['fileName'] = $scope.fileName
                            query['reconName'] = $scope.activeRecon
                            query['checkFileProperty'] = false
                            query['source'] = 'Invoice'
                            query['save'] = false
                            query['sheetNames'] = 0
                            SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                                $scope.updateDetails(data)
                            })

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }
            })
        }
    }]);
