from numpy.distutils.command.config import config
from pandas.computation import expressions

import flask
import hashframe
import pandas
import numpy as np
import hashlib
from pandas.api.types import *
import AesEncrypt
import shutil
import json
import zipfile
from bson.json_util import dumps
from bson import json_util
from copy import copy
import traceback
import webbrowser

logr = hashframe.Logger.getInstance("application").getLogger()
import datetime
import subprocess
import os
import csv
import datetime
import re
from flask import request
from shutil import copyfile
import smtplib
from bson import ObjectId
import rstr
from email.mime.multipart import MIMEMultipart
# from email.MIMEText import MIMEText
import uuid
import requests
import copy


# Login
class Login(hashframe.noAuthHandler):
    def create(self, doc):
        status, data = Users().get({'userName': doc['userName']})
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, doc['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        password = hashlib.md5(decrypt).hexdigest()
        resp = "User not found"

        # check is password is correct
        if status:
            # Checking Approved status
            if data['ApprovalStatus'] == "NotApproved":
                return False, 'Account to be approved'

            # Passsword check
            if password == data['password']:
                if data["role"] not in ["admin", "Configurer"]:
                    if (datetime.datetime.now() - datetime.datetime.fromtimestamp(
                            data['lastPwdUpdt'] / 1000)).days > 45:
                        return False, 'Password Expired'

                    if data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                flask.session['sessionData'] = {}
                flask.session['sessionData']['userName'] = data['userName']
                flask.session['sessionData']['email'] = data['email']
                flask.session['sessionData']['partnerId'] = data['partnerId']
                flask.session["sessionData"]["userId"] = data['_id']
                # flask.session['sessionData']['preveliges'] = data['preveliges']
                data['accountLocked'] = False
                data['loginCount'] = 0
                Users().modify(data['_id'], data)
                if data.get('role'):
                    flask.session['sessionData']['role'] = data['role']
            else:
                # if Password is Wrong
                if data['role'] != 'admin':
                    if 'accountLocked' in data and data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                    if 'loginCount' in data and data['loginCount'] != hashframe.config.loginCount:
                        data['loginCount'] = data['loginCount'] + 1
                        data['accountLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'Password is Incorrect You have ' + str(
                            hashframe.config.loginCount - data['loginCount']) + ' attempts left'

                    elif 'loginCount' in data and data['loginCount'] == hashframe.config.loginCount:
                        data['accountLocked'] = True
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'accountLocked Please Contact Admin'

                    else:
                        data['loginCount'] = 1
                        data['accoungLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                    return False, 'Password Incorrect'
                else:
                    return False, 'Password Incorrect'

            # get all the assigned recons to session
            _, assigned_recons = self.get_assigned_recons(data['userName'])
            flask.session['sessionData'].update(assigned_recons)

            return True, flask.session['sessionData']
        return False, resp

    def getAll(self, query={}):
        if 'sessionData' in flask.session:
            return True, flask.session['sessionData']
        return False, "Logged out."

    def get_assigned_recons(self, user_name):
        users = []
        users.append(user_name)
        data = list(ReconAssignment().find({'users': {"$in": users}}))
        # status,data = ReconAssignment().getAll({'condition': {'userName': user_name}})
        assigned_recons = []
        recon_process = {}
        actions = {}
        if data:
            for r in data:
                assigned_recons.extend(r.get('reconNames', []))
                actions = {recon: r['groupRole'] for recon in assigned_recons}
                recon_details = list(Recons().find({"reconName": {"$in": assigned_recons}}))
                # status,recon_details = list(Recons().getAll({'condition': {"reconName": {"$in": assigned_recons}}}))
            for r in recon_details:
                if recon_process.get(r['reconProcess']) is None:
                    recon_process[r['reconProcess']] = []
                recon_process[r['reconProcess']].append({'name': r['reconName'], 'actions': actions[r['reconName']]})
                # recon_process[r['reconProcess']].append(r['reconName'])
        else:
            pass
        return True, {"reconProcess": recon_process, 'assignedRecons': assigned_recons}


#  signup User/Creating User


class ReconLicense(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconLicense, self).__init__("reconlicense", hideFields)

    def getReconLicense(self, reconName):
        doc = ReconLicense().find_one({'reconName': reconName})
        return True, doc


class NoAuth(hashframe.noAuthHandler):
    def createUser(self, id, query):
        if not re.match(r'[\w.?\d]*@suryodaybank.com', query['email']):
            return False, 'Mail Id is Not Valid'

        if Users().find_one({'userName': query['userName']}):
            return False, 'UserName Already Exist'

        if Users().find_one({'email': query['email']}):
            return False, "Mail Id Is Already Exist"

        query['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        query['password'] = hashlib.md5(decrypt).hexdigest()
        query['ApprovalStatus'] = 'Not Approved'
        query['loginCount'] = 0
        query['accountLocked'] = False
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        Users().insert_one(query)
        return True, 'Success'

    def upload(self, id):
        mftPath = os.path.join(hashframe.config.uploadDir.get(id, hashframe.config.uploadDir['file']))
        fname = flask.request.files['file'].filename

        if os.path.exists(mftPath + '/' + fname):
            os.remove(mftPath + '/' + fname)

        if not os.path.exists(mftPath):
            os.makedirs(mftPath)
        flask.request.files['file'].save(os.path.join(mftPath, fname))
        status = {'status': 'File uploaded.', 'fileName': fname}
        return True, status

    def sendPasswordToMail(self, id, query):
        userDetails = Users().find_one({'userName': query['userName']})
        if userDetails:
            if userDetails['email'] == query['associatedMail']:
                password = rstr.xeger(r'[A-Z][a-z]{2}[@#$][0-9]{4}')
                email = userDetails['email']

                try:
                    smtp = smtplib.SMTP(hashframe.config.smtpServer, hashframe.config.smtpPort)
                    sender = hashframe.config.smtpMail

                    smtp.ehlo()
                    smtp.starttls()

                    smtp.login(hashframe.config.smtpMail, hashframe.config.smtpPassword)

                    recipients = userDetails['email']

                    msg = MIMEMultipart()
                    msg["From"] = sender
                    msg['To'] = recipients
                    msg["Subject"] = 'PASSWORD CHANGE'

                    body = "UserName : " + userDetails[
                        'userName'] + "Kindly Use the following PASSWORD to login" + " : " + str(password)

                    msg.attach(MIMEText(body, 'Plain'))

                    smtp.sendmail(sender, recipients, msg.as_string())
                    smtp.close()

                    logr.info('Password Change Email sent')
                except Exception as e:
                    logr.info(e)
                    logr.info('Error while sending Email')
                    return False, 'Error while sending Mail'

                del userDetails['password']
                userDetails['password'] = hashlib.md5(str(password)).hexdigest()
                Users().modify(userDetails['_id'], userDetails)
                return True, 'Password Sent To your Mail Id'
            else:
                return False, 'Please Enter Associated mail Id'
        else:
            return False, 'User Not Found'

    def updateExpirePassword(self, id, query):
        status, doc = Users().get({'userName': query['userName']})

        if 'newPassword' in query and 'oldPassword' in query:
            decryptnewPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['newPassword'])
            decryptnewPassword = decryptnewPassword.replace('/<=%+,?->/', '')

            decryptoldPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['oldPassword'])
            decryptoldPassword = decryptoldPassword.replace('/<=%+,?->/', '')

            newPassowrd = hashlib.md5(decryptnewPassword).hexdigest()
            oldPassword = hashlib.md5(decryptoldPassword).hexdigest()

            if (doc['password'] != oldPassword):
                return False, 'Old Password is incorrect'
            doc['password'] = newPassowrd
            doc['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000

        Users().modify({'userName': query['userName']}, doc)
        return True, 'User Details Updated SuccessFully'


#  Logout
class Logout(hashframe.noAuthHandler):
    def create(self, doc):
        flask.session.clear()
        flask.abort(401)

    def getAll(self, query={}):
        flask.session.clear()
        flask.abort(401)


class ReconExecDetailsLog(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconExecDetailsLog, self).__init__("recon_execution_details_log", hideFields)

    def getLatestExeDetails(self, reconId):
        doc = ReconExecDetailsLog().find_one({'reconName': reconId, 'jobStatus': 'Success'},
                                             sort=[('reconExecutionId', -1)])
        return True, doc

    def getMaxReconExecutionDoc(self, recon_names=None):
        from bson.son import SON
        pipeline = [{'$match': {'reconName': {'$in': recon_names or []}}},
                    {'$group': {'_id': '$reconName', 'executionId': {'$max': '$reconExecutionId'}}},
                    {"$sort": SON([("_id", -1)])}]

        agg_results = ReconExecDetailsLog().aggregate(pipeline=pipeline)
        filters = []
        if agg_results is not None:
            filters = [x['executionId'] for x in agg_results]

        return ReconExecDetailsLog().find({'reconExecutionId': {'$in': filters}})

    def rollbackRecon(self, id, query):
        date = datetime.datetime.strptime(query['statementDate'], '%d-%m-%Y')

        doc = ReconExecDetailsLog().find(
            {'reconName': query['reconName'], 'jobStatus': 'Success', 'statementDate': {'$gte': date}})

        if doc.count() == 0:
            return False, "No Previous data found."

        li = [d['statementDate'].strftime('%d%m%Y') for d in doc]
        mft_path = hashframe.config.mftPath + '/%s/' % query['reconName']
        for i in li:
            path = mft_path + i
            shutil.rmtree(path, ignore_errors=True)

        logr.info("Rollback Records for recon %s " % query['reconName'])
        ReconExecDetailsLog().remove(
            {'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        ReconSummary().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        CustomReport().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        MasterDumps().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        return True, 'Rollback successful.'

    def get_latest_execution_details(self, id):
        assigned_recons = flask.session['sessionData'].get('assignedRecons', [])
        _, recons = Recons().getAll({"condition": {'reconName': {'$in': assigned_recons}}}, limit=100)

        if recons['total'] == 0:
            return False, 'No Recon Allocated.'

        recon_exec = list(self.getMaxReconExecutionDoc(assigned_recons))
        if recon_exec:
            for _exec in recon_exec:
                _exec['statementDate'] = _exec['statementDate'].strftime('%d-%m-%Y')

            recon_exec = {x['reconName']: x for x in recon_exec}
            for x in recons['data']:
                x.update(recon_exec.get(x['reconName'], {}))

        return True, {'data': recons['data'], 'total': len(recons['data'])}


class MasterDumps(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(MasterDumps, self).__init__("master_dumps", hideFields)


#  store Column DataTypes of Each Sources of all Recons
class ColumnDataTypes(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ColumnDataTypes, self).__init__("columnDtypes", hideFields)

    def getColumnDataTypes(self, reconId, source):
        doc = ColumnDataTypes().find_one({'reconName': reconId, 'source': source})
        return doc


# Store Audit logs
class ReconAudit(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAudit, self).__init__("reconaudit", hideFields)


#  store users details
class Users(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(Users, self).__init__("users", hideFields)

    def create(self, doc):
        (status, userDoc) = super(Users, self).create(doc)
        return status, userDoc

    def deleteUser(self, id, query):
        status, data = Users().delete(id)
        status, data = ReconAssignment().delete({"userName": query['userName']})
        if status:
            return True, 'DELETED'
        else:
            return False, 'error'


#  store Recons Details from settings tab
class CustomReport(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(CustomReport, self).__init__("custom_reports", hideFields)

    def create(self, doc):
        (status, custom_report) = super(CustomReport, self).create(doc)
        return status, custom_report

    def getDailySettlementRpt(self, id, query):
        try:
            query['statementDate'] = datetime.datetime.strptime(query['statementDate'], '%Y-%m-%d')
            print {'statementDate': query['statementDate'], "reportName": query["reportType"]}
            aqqData = pandas.DataFrame()
            issData = pandas.DataFrame()
            # Get Aqquirer Data

            status, data = CustomReport().get(
                {'statementDate': query['statementDate'], "reportName": 'remmiter', 'reconName': 'IMPS_OUTWARD'})

            if status:
                aqqData = pandas.DataFrame(data['data'])
                del aqqData['Remmiter P2A-08#Cycle']
                col = {}
                for i in aqqData.columns:
                    if '#' in i:
                        if i.split('#')[1] not in ['Debit', 'Credit', 'Cycle']:
                            col[i] = i.split('#')[1] + 'aqqCol'
                        else:
                            col[i] = i.split('#')[1]
                aqqData.rename(columns=col, inplace=True)
                # aqqData['CYCLE'] = aqqData['FEED_FILE_NAME'].str.split('_').str[1].str[:2]
                # del aqqData['FEED_FILE_NAME']

            # Get Issuer Data
            status, data = CustomReport().get(
                {'statementDate': query['statementDate'], "reportName": 'beneficiary', 'reconName': 'IMPS_INWARD'})

            if status:
                issData = pandas.DataFrame(data['data'])
                del issData["Beneficiary P2A-08#Cycle"]
                col1 = {}
                for i in issData.columns:
                    if '#' in i:
                        if i.split('#')[1] not in ['Debit', 'Credit', 'Cycle']:
                            col1[i] = i.split('#')[1] + 'issCol'
                        else:
                            col1[i] = i.split('#')[1]
                issData.rename(columns=col1, inplace=True)
                # del issData['FEED_FILE_NAME']

            if len(aqqData) or len(issData):
                df = pandas.concat([aqqData, issData], axis=1)
                df['REMITTER'] = False
                df['BENEFICIARY'] = False
                df.loc[df['Debit'] == df['remitter_sub_totalaqqCol'], 'REMITTER'] = True
                df.loc[df['Credit'] == df['beneficiary_sub_totalissCol'], 'BENEFICIARY'] = True

                doc = df.to_dict(orient='records')
                return True, df.to_dict(orient='records')
            else:
                return True, {}
        except Exception as e:
            print e
            print traceback.format_exc()


#  store Recons Details from settings tab
class Recons(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(Recons, self).__init__("recons", hideFields)

    def create(self, doc):
        (status, reconsDoc) = super(Recons, self).create(doc)
        return status, reconsDoc

    def deleteData(self, condition, query):
        try:
            status, result = Recons().delete({'reconName': query['reconName'], 'reconProcess': query['reconProcess']})
            if len(list(Recons().find({'reconProcess': query['reconProcess']}))) == 0:
                ReconProcess().delete({'reconProcess': query['reconProcess']})
            if status:
                for coll in [SourceReference, MatchRuleReference, MatchRuleSumColumns, Flows]:
                    status, result = coll().delete({'reconName': query['reconName']})
                return True, 'Success'
            else:
                return False, 'Failed'
        except Exception as e:
            logr.info(traceback.format_exc())

    def getReconDetails(self, id):
        status, reconDetail = Recons().getAll({})

    def getProcessRecons(self, id, query):
        status, recons = Recons().getAll({"condition": {"reconProcess": query['reconProcess']}})
        allRecons = []
        if status and len(recons['data']) > 0:
            for recon in recons['data']:
                doc = {}
                doc['reconName'] = recon['reconName']
                doc['reconProcess'] = recon['reconProcess']
                if isinstance(recon['sources'], list):
                    doc['sourcesCount'] = len(recon['sources'])
                elif isinstance(recon['sources'], dict):
                    doc['sourcesCount'] = len(recon['sources'].keys())
                doc['sources'] = recon['sources']
                doc['_id'] = recon['_id']
                allRecons.append(doc)
        return True, allRecons

    def cloneSelectedRecon(self, id, query):
        recon = Recons().find_one({'reconName': query['reconName']})
        feedId = {}
        if recon:
            for source in recon['sources']:
                for struct, values in recon['sources'][source].iteritems():
                    sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime('%d%m%Y%H%M%S')
                    values['feedId'] = sourceId
                    feedId[source] = sourceId

            recon['reconName'] = recon['reconName'] + '_Cloned'
            doc = Recons().find_one({'reconName': recon['reconName']})
            if doc:
                recon['reconName'] = recon['reconName'] + '_Cloned'
            del recon['_id']
            Recons().save(recon)

        else:
            return False, 'No data found for cloning'
        if query['cloneSrc']:
            srcReferences = list(SourceReference().find({'reconName': query['reconName']}))
            if len(srcReferences) > 0:
                for source in srcReferences:
                    del source['_id']
                    source['reconName'] = recon['reconName']
                    source['feedId'] = feedId[source['source']]
                    SourceReference().save(source)
            else:
                return True, 'Source references not found'

        else:
            return False, 'No data found for cloning'
        return True, 'Recon cloned successfully'

    def saveRecon(self, id, query):
        try:
            if '_id' in query.keys():
                recon_details = Recons().find_one({'_id': ObjectId(query['_id'])})
                if recon_details:
                    for source in query['sources']:
                        for struct, values in query['sources'][source].iteritems():
                            sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime(
                                '%d%m%Y%H%M%S')
                            if 'feedId' not in values.keys():
                                values['feedId'] = sourceId
                    Recons().modify(query['_id'], query)

                for source in query['sources']:
                    for struct, values in query['sources'][source].iteritems():
                        reference = SourceReference().find_one({'feedId': values['feedId']})
                        if reference:
                            reference['reconName'] = query['reconName']
                            reference['source'] = source
                            reference['struct'] = struct
                            SourceReference().modify(reference['_id'], reference)
                return True, 'Updated successfully'
            else:
                query['isLicensed'] = False
                query['partnerId'] = "54619c820b1c8b1ff0166dfc"
                for source in query['sources']:
                    for struct in query['sources'][source]:
                        sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime(
                            '%d%m%Y%H%M%S')
                        query['sources'][source][struct]['feedId'] = sourceId
                Recons().save(query)
                return True, 'Added successfully'
        except Exception as e:
            print traceback.format_exc()
            logr.info(e)
            return False, e

    def getReports(self, id, query):
        if query['stmtDate']:
            stmtDate = datetime.datetime.strptime(query['stmtDate'], '%d/%m/%Y')
        else:
            return False, 'Please Select StatementDate'

        doc = list(ReconExecDetailsLog().find(
            {'reconName': query['reconName'], 'jobStatus': 'Success', 'statementDate': stmtDate}))

        if doc:
            path = hashframe.config.mftPath + os.sep + query['reconName'] + os.sep + datetime.datetime.strftime(
                stmtDate, '%d%m%Y') + '/OUTPUT/'
            filesList = os.listdir(path)
            downloadPathList = []
            for file in filesList:
                fileDict = {'fileName': '', 'downloadPath': ''}
                shutil.copy(path + file, hashframe.config.contentDir)
                key = os.path.basename(path + file).split('.')[0]
                fileDict['fileName'] = key
                fileDict['downloadPath'] = file
                downloadPathList.append(fileDict)
            return True, downloadPathList
        return False, 'No Execution Found For This Date'


#  Structure Details From MarketPlace Tab For each Structure
class SourceReference(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(SourceReference, self).__init__("sourceReference", hideFields)

    def create(self, doc):
        (status, sourceRefDoc) = super(SourceReference, self).create(doc)
        return status, sourceRefDoc

    def downloadSource(self, id, query):
        refernce = SourceReference().find_one({'source': query['source'], 'struct': query['struct']})
        if refernce is None:
            df = pandas.DataFrame(refernce['feedDetails'])
        else:
            df = pandas.DataFrame(query['feedDetails'])

        fileName = hashframe.config.uploadDir.get(id, 'file') + os.sep + query['struct'] + 'Reference.csv'
        df = df[['fileColumn', 'dataType', 'datePattern', 'position']]
        if refernce is not None:
            # open a file for writing

            df.to_csv(fileName, index=False)
        else:
            return False, 'No Data Found'
        return True, query['struct'] + 'Reference.csv'

    def loadSourceRef(self, id, query={}):
        if query.get('filter'):
            _file = hashframe.config.uploadDir.get('filter') + os.sep + query.get('fileName')
            with open(_file) as f:
                lines = [x for x in f.readlines() if x.strip().replace('\n', '')]
                feed = SourceReference().find_one(
                    {'reconName': query['reconName'], 'source': query['source']})

                if feed:
                    if 'filters' not in feed.keys():
                        feed['filters'] = lines
                    else:
                        for filter in lines:
                            if filter not in feed['filters']:
                                feed['filters'].append(filter)
                    SourceReference().modify(feed['_id'], feed)
                return True, lines
        else:
            ref_file = pandas.read_csv(hashframe.config.uploadDir['file'] + os.sep + query.get('fileName'),
                                       engine='python')
            ref_file.replace('nan', '', inplace=True)
            ref_file.rename(columns=hashframe.config.sourceRefMapper, inplace=True)
            ref_file['position'] = ref_file.index + 1
            ref_file.fillna('', inplace=True)
            return True, ref_file.to_dict(orient="records")

    def getSourceForRecon(self, id, query):
        status, sources = SourceReference().getAll({'condition': {'reconName': query['reconName']}})
        reconDetails = []
        if status:
            for source in sources['data']:
                doc = {}
                doc['reconName'] = source['reconName']
                doc['struct'] = source['struct']
                doc['source'] = source['source']
                doc['referenceSource'] = source['referenceSource']

                if 'position' in source.keys():
                    doc['position'] = source['position']
                else:
                    doc['position'] = sources['data'].index(source) + 1
                reconDetails.append(doc)
            reconDetails = sorted(reconDetails, key=lambda k: k['position'])
        return True, reconDetails

    def storeOrderOfSource(self, id, query):
        if query is not None:
            for i in query:
                SourceReference().modify(
                    {'source': i['source'], 'struct': i['struct'], 'reconName': i['reconName']},
                    {'position': i['position']})
        return True, 'Updated SuccessFully'

    def addSourceRef(self, id, query):
        recon_details = Recons().find_one({'_id': ObjectId(query['_id'])})
        for ref in query['sourceRef']:
            reconPath = hashframe.config.mftPath + ref['reconName']
            if not os.path.exists(reconPath):
                os.mkdir(reconPath)
            status, doc = SourceReference().get(
                {'reconName': ref['reconName'], 'struct': ref['struct'], 'source': ref['source']})
            if status:
                for source in recon_details['sources']:
                    for struct, values in recon_details['sources'][source].iteritems():
                        if 'feedId' not in doc.keys():
                            if ref['struct'] == struct:
                                doc['feedId'] = values['feedId']
                SourceReference().modify(doc['_id'], doc)
            else:
                for source in recon_details['sources']:
                    for struct, values in recon_details['sources'][source].iteritems():
                        if 'feedId' not in ref.keys():
                            if ref['struct'] == struct:
                                ref['feedId'] = values['feedId']
                status, _ = SourceReference().create(ref)

        return True, ''

    def saveSourceDetails(self, id, query, checkFileProperty=True, deriveColumn=False):
        try:
            columns = []
            # Find Recon Details
            if not ReconProcess().find_one({'reconProcess': query['reconProcess']}):
                reconProcess = {
                    "reconProcess": query['reconProcess'],
                    "partnerId": "54619c820b1c8b1ff0166dfc",
                    "isLicensed": True
                }
                ReconProcess().save(reconProcess)

            recon = Recons().find_one({'reconName': query['reconName'], 'reconProcess': query['reconProcess']})
            query['partnerId'] = "54619c820b1c8b1ff0166dfc"

            feedCount = list(SourceReference().find({'reconName': query['reconName'], 'source': query['source']}))
            # Read Properties Of sourceFile if required
            if checkFileProperty:
                status, doc = self.getSourceProperties(
                    hashframe.config.uploadDir['source'] + '/' + query.get('fileName', ''))
                try:
                    data = {'delimiter': doc['delimiter'], 'fileName': query['fileName'],
                            'loadType': doc['loadType'], 'skiprows': query.get('skipRows', 0),
                            'skipfooter': query.get('skipRows', 0),
                            'filePattern': doc['filePattern'], 'fileExtension': doc['fileExtension']}
                    data['filters'] = []
                    data['derivedColumns'] = []
                    data['reconName'] = query.get('reconName', '')
                    data['reconProcess'] = query.get('reconProcess', '')
                    data['source'] = query.get('source', '')
                    data['struct'] = query.get('source', '') + '_' + str(len(feedCount) + 1)
                    data['fileName'] = query['fileName']
                    data['postion'] = len(feedCount) + 1
                    print "==================building data==============="

                except Exception as e:
                    print e


            else:
                # Properties of file will be in Query
                doc = copy.deepcopy(query)
                data = copy.deepcopy(query)

            # Read Sample File of Source
            if query.get('fileName'):
                sep = str(doc['delimiter'])

                if os.path.exists(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName')):

                    # Read CSV Files
                    if doc['loadType'] == 'CSV':
                        skiprows = str(query.get('skiprows', 0)).replace('.0', '')
                        skiprows = int(skiprows)
                        df = pandas.read_csv(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'),
                                             skiprows=skiprows, skipfooter=query.get('skipfooter', 0),
                                             sep=str(doc['delimiter']), engine='python')
                        df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

                    # Read Excel Files
                    if doc['loadType'] == 'Excel':
                        if 'sheetNames' not in query:
                            xl = pandas.ExcelFile(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'))
                            data['sheetNames'] = xl.sheet_names
                            return True, data
                        data['sheetNames'] = query['sheetNames']

                        if query.get('skiprows', 0) != 0:
                            skiprows = query.get('skiprows', 0) - 1
                        else:
                            skiprows = str(query.get('skiprows', 0)).replace('.0', '')

                        df = pandas.read_excel(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'),
                                               sheet_name=query.get('sheetNames', 0),
                                               skiprows=skiprows, skipfooter=query.get('skipfooter', 0))
                        df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

                    if doc['loadType'] == 'FixedFormat':
                        print "==========the file is fixed format============="
                        skiprows = str(query.get('skiprows', 0)).replace('.0', '')
                        skiprows = int(skiprows)
                        self.loadFixedType = doc['fileExtension']
                        try:
                            print "=============printing query========="

                            pos, names, typesdic = FixedStructure().buildColPostionsandNames(data)
                            data_types_date = {}
                            data_types = {}
                            for i in typesdic:
                                if typesdic[i] == 'np.datetime64':
                                    data_types_date[i] = typesdic[i]
                                    data_types[i] = eval('str')

                                else:
                                    data_types[i] = eval(typesdic[i])

                            df = pandas.read_fwf(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'),
                                                 header=None, colspecs=pos, names=names,
                                                 skiprows=skiprows,
                                                 converters=data_types,
                                                 skipfooter=query.get('skipfooter', 0))
                        except Exception as e:
                            print e

                    # Generate Column Dtypes For sourceReference
                    for col in df.columns:
                        columns.append({'fileColumn': col, 'dataType': 'str', 'required': True, 'datePattern': '',
                                        'position': df.columns.get_loc(col) + 1})
                    # print columns
                    # Call Function For columnType
                    columns = self.getColumnType(df)
                    if query.get('feedDetails'):
                        data['feedDetails'] = query['feedDetails']
                    else:
                        data['feedDetails'] = columns

                else:
                    return False, 'File is not found. Please Upload File Once'

                # Condition to save Source Details or not
                data['filters'] = query.get('filters', [])
                data['derivedColumns'] = query.get('derivedColumns', [])
                data['saveSourceDetails'] = query.get('saveSourceDetails', True)
                data['secondStructure'] = query['secondStructure']
                print "========before gng to save feed details========="

                # Call Function to save Feed Details
                self.saveFeedDetails(data, deriveColumn)

                # Deletd _id in data to return
                if '_id' in data:
                    del data['_id']
                data['data'] = df.to_dict(orient='records')
                data['data'] = json.dumps(data['data'], default=self.myConverter)

            return True, data
        except Exception as e:
            print e
            return False, 'Error Occured'

    def myConverter(self, data):
        if isinstance(data, datetime.datetime):
            return data.__str__()

    def skipRows(self, id, query):
        feed = SourceReference().find_one({'reconName': query['reconName'], 'source': query['source']})
        feed['skiprows'] = feed.get('skiprows', 0) + query['skiprows']
        feed['skipfooter'] = feed.get('skipfooter', 0) + query.get('skipfooter', 0)
        SourceReference().modify(feed['_id'], feed)
        status, data = self.saveSourceDetails('dummy', feed, checkFileProperty=False)
        return True, data

    def saveFeedDetails(self, query, derivedColumn=False):
        # Copy of sourceDetails
        feedBackUp = query.copy()
        print "================came into save feed details================"

        # Find existing Feed For source
        feed = SourceReference().find_one(
            {'reconName': feedBackUp['reconName'], 'source': feedBackUp.get('source', ''),
             'struct': feedBackUp.get('struct', '')})
        print "=chehcking whether the same struct of source is present or not==========="

        if feed:
            # Fucntion called From derivedColumns Function
            if derivedColumn:
                feedBackUp['feedDetails'] = feed['feedDetails']
                feedBackUp['filters'] = query['filters']
                feed['filters'] = query['filters']
                feedBackUp['derivedColumns'] = query['derivedColumns']
                feed['derivedColumns'] = query['derivedColumns']

            # Function will be called from loadSampleFile or while Uploading sampleFile
            if query['saveSourceDetails'] == False:
                if 'saveSourceDetails' in feed.keys():
                    del feedBackUp['saveSourceDetails']
                if 'fileName' in query:
                    feed['fileName'] = query['fileName']
                    status, doc = SourceReference().modify(feed['_id'], feed)
                return True, ''

            if 'saveSourceDetails' in feedBackUp.keys():
                del feedBackUp['saveSourceDetails']

            # Modify the existing FeedDef with updated Details
            status, doc = SourceReference().modify(feed['_id'], feedBackUp)

        # If no Feed found for source saveDetails directly to mongo
        else:

            # not to store saveSourceDetails varibale in sourceEntry
            if 'saveSourceDetails' in feedBackUp.keys():
                del feedBackUp['saveSourceDetails']

            # Add Partner Id into Feed and Save
            feedBackUp['partnerId'] = "54619c820b1c8b1ff0166dfc"
            SourceReference().save(feedBackUp)
            return True, ''

    def getColumnType(self, df):
        columns = []
        for col in df.columns:
            df[col] = df[col].fillna(ReconDetails().infer_dtypes(df[col]))
            a = ReconDetails().infer_dtypes(df[col])
            if a == '' or 0:
                dtype = 'str'
            elif a == 0.0:
                dtype = 'np.float64'
            else:
                df[col] = df[col].astype(str)
                dtype = 'np.datetime64'
            columns.append({'fileColumn': col, 'dataType': 'str', 'required': True, 'datePattern': '',
                            'position': df.columns.get_loc(col) + 1})
        return columns

    def loadSampleFile(self, id, query):
        doc = SourceReference().find_one(
            {'reconName': query['reconName'], 'source': query['source']})

        doc['saveSourceDetails'] = False

        if doc and 'fileName' in doc:
            status, data = self.saveSourceDetails('dummy', doc, checkFileProperty=False)
            if not status:
                doc['fileLoaded'] = False
                doc['msg'] = data
                return True, doc
            data['feedDetails'] = doc['feedDetails']
            data['fileLoaded'] = True
            return status, data
        else:
            doc['msg'] = 'Please Upload Sample File'
            doc['fileLoaded'] = False
            return True, doc

    def saveFilters(self, id, query):
        sourceRefrences = list(
            SourceReference().find(
                {'reconName': query['reconName'], 'reconProcess': query['reconProcess'], 'source': query['source']}))
        for i in sourceRefrences:
            for filter in query['filters']:
                if filter not in i['filters']:
                    i['filters'].append(filter)
            SourceReference().modify(i['_id'], i)
        return True, ''

    def deleteSource(self, id, query):
        status, data = SourceReference().delete(query)
        if status:
            for coll in [MatchRuleReference]:
                status, result = coll().delete({'reconName': query['reconName'], 'reconProcess': query['reconProcess']})
                return True, 'Source Successfully Deleted'
        else:
            return False, 'Failed'

    # def deriveColumns(self, id, query):
    #     # if query['columnType'] == 'str':
    #     completeString = str(query['completeValue'])
    #     subString = query['selectedText']
    #     specialChar = ['/', ':', '-', '~', '_', '%']
    #     doc = {}
    #     for i in specialChar:
    #         count = completeString.count(i)
    #         if (count != 0):
    #             list = completeString.split(i)
    #             index = list.index(subString) if subString in list else ''
    #             if index is not '':
    #                 doc = {}
    #                 doc['type'] = 'derivedColStrExp'
    #                 doc['filter_type'] = 'split'
    #                 doc['value'] = i
    #                 doc['index'] = index
    #                 doc['columnName'] = query['selectedColumn']

    # if doc == {}:
    #     starValue = completeString.index(subString)
    #     if len(completeString) == len(subString) and completeString == subString:
    #         endValue = ''
    #         starValue = ''
    #     elif len(subString) + starValue == len(completeString):
    #         endValue = ''
    #         starValue = completeString.index(subString)
    #     elif starValue == 0:
    #         starValue = 0
    #         endValue = len(subString)
    #     else:
    #         endValue = completeString.rindex(subString[-1:]) + 1
    #
    #     doc = {}
    #     doc['type'] = 'derivedColStrExp'
    #     doc['filter_type'] = 'sub string'
    #     doc['endValue'] = endValue
    #     doc['columnName'] = query['selectedColumn']
    #     doc['startValue'] = starValue
    #     doc['value'] = str(starValue) + ':' + str(endValue)
    #     logr.infoquery
    #     logr.info'*' * 100
    #     status, exp = self.checkFilePropertyExpression(doc, query)
    #     data = {}
    #     query['filters'] = exp['filters']
    #     columns = self.getColumnType(exp['data'])
    #     query['feedDetails'] = columns
    #     query['derivedColumns'] = exp['derivedColumns']
    #
    #     self.saveSourceDetails(query, derivedColumn=True)
    #     if '_id' in query:
    #         del query['_id']
    #     query['data'] = exp['data'].to_dict(orient='records')
    #     return True, query

    def checkFilePropertyExpression(self, doc, data):
        with open('/usr/share/nginx/www/ngerecon/flask/JSON/expression.json', "r") as f:
            expressions = json.load(f)
        reqExpe = expressions[doc['type']][doc['filter_type']]
        reqExpe = reqExpe.replace('$colName', doc['columnName'])
        reqExpe = reqExpe.replace('$colValue', doc['value'])
        exp = "df['" + doc['columnName'].replace(' ', '') + "Derived'] = " + reqExpe
        if 'index' in doc and doc['index'] is not None:
            exp = exp.replace('$index', str(doc['index']))
        else:
            exp = exp.replace('$index', '')

        feed = SourceReference().find_one({'reconName': data['reconName'], 'source': data['source']})
        data['filters'].append(exp)
        data['derivedColumns'].append(doc['columnName'].replace(' ', '') + "Derived")
        if feed:
            status, sourceData = self.saveSourceDetails('dummy', feed, checkFileProperty=True)
        df = pandas.DataFrame(sourceData['data'])
        if df is not None:
            for exp in data['filters']:
                logr.infoexp
                exec (exp)
        doc = {}
        doc['filters'] = data['filters']
        doc['derivedColumns'] = data['derivedColumns']
        doc['data'] = df
        return True, doc

    def getDelimiter(self, sourcefile, delimiterlist):
        try:
            print('Checking For Delimeter')
            print('======================')
            file = open(sourcefile, 'r')
            data = " ".join(file.readlines()[0:25])
            countlist = []

            for delimiter in delimiterlist:
                countlist.append(data.count(str(delimiter)))
            print 'here not att all comes'
            print('Delimeters Counts in File %s' % (countlist))

            max_value = max(countlist)
            print("Max Value in CountList %s" % (max_value))

            max_index = countlist.index(max_value)
            logr.info("Delimeter in file is %s" % (delimiterlist[max_index]))
            print('======================')

            return delimiterlist[max_index]
        except Exception as e:
            logr.info("Below Error Occured while Geting Delimiter")
            logr.info(e)

    def getSourceProperties(self, sourcefile):
        print "===============came to sourceproperties==================="

        delimiter = None
        fileName = os.path.basename(sourcefile)
        fileExtension = fileName.split('.')
        if len(fileExtension) > 2:
            for ext in fileExtension:
                if ext in hashframe.config.extensionCheck:
                    fileExtension = ext
        else:
            fileExtension = fileName.split('.')[1]
        filePattern = self.getfilePattern(fileName.split('.')[0]) + '.' + fileExtension
        loadType, delimiter = self.fileTypeMapper(sourcefile, fileExtension)
        doc = {}
        doc['loadType'] = loadType
        doc['delimiter'] = delimiter
        doc['filePattern'] = filePattern
        doc['fileExtension'] = fileExtension
        return True, doc

    def fileTypeMapper(self, sourcefile, fileExtension):
        delimiter = None
        delimiterList = self.getDelimiterList()
        fileExtension = fileExtension.lower()
        if fileExtension not in hashframe.config.checkforDelimiter['listOfNonDelimitedTypes']:
            delimiter = self.getDelimiter(sourcefile, delimiterList)
        if fileExtension in hashframe.config.checkforDelimiter['listOfNonDelimitedTypes'] and fileExtension == 'mcrb':
            delimiter = 'NA'
            loadType = 'FixedFormat'
        if fileExtension != 'mcrb':
            loadType = self.getLoadType(fileExtension, delimiter)

        return loadType, delimiter

    def getfilePattern(self, filename):
        ext = "".join(['d' if x.isdigit() else x for x in filename])
        # ext = filename[0:5] + '*.*'
        prev = ext[0]
        count = 1
        for c in ext[1:]:
            if prev != c:
                if prev == 'd':
                    ext = ext.replace(prev * count, '[0-9]{' + str(count) + '}', 1)
                    count = 1
                else:
                    count = 1
            else:
                count += 1
            prev = c
        else:
            if prev == 'd':
                ext = ext.replace(prev * count, '[0-9]{' + str(count) + '}', 1)
        return ext

    def getDelimiterList(self):
        delimiterList = []
        for key, property in hashframe.config.fileproperties.iteritems():
            if 'delimiter' in property.keys():
                delimiterList.extend(property['delimiter'])
        return delimiterList

    def getLoadType(self, fileExtension, delimiter):
        loadType = ''
        fileExtension = fileExtension.lower()
        if delimiter is None:
            for key, property in hashframe.config.fileproperties.iteritems():
                if fileExtension in property['types']:
                    loadType = key
        else:
            for key, property in hashframe.config.fileproperties.iteritems():
                if fileExtension in property['types'] and delimiter in property.get('delimiter', ''):
                    loadType = key
        return loadType

    def generateFilter(self, column, fullVal, partVal, regX, frontChar, rearChar, filterModule, filterColumn,
                       filters=[]):
        regexVal = frontChar + partVal + rearChar
        nopartVal = False
        if not filters:
            string = "df['" + column + "_Derived'" + ']='

        for fil in filters:
            if column in fil.split('=')[0]:
                string = 'df.loc[df["' + column + '_Derived"]=="",' + '"' + column + '_Derived"]='
            else:
                string = 'df["' + column + '_Derived"' + ']='
        groups = re.search(regX, fullVal).groups() if re.search(regX, fullVal) else []

        if regexVal not in groups:
            nopartVal = True
            regX = regX.replace(frontChar, '').replace(rearChar, '').replace('\\', '')
            groups = re.findall(regX, fullVal) if re.findall(regX, fullVal) else []

        for index, group in enumerate(groups):
            if partVal in group:
                idx = str(index)
                break
        if not nopartVal:
            exp = 'df["' + column + '"].apply(lambda x:' + 're.search("' + regX + '",str(x)).groups()[' + str(
                idx) + '].replace("' + frontChar + '","").replace("' + rearChar + '","") if re.search("' + regX + '",str(x)) else "" )'
        else:
            exp = 'df["' + column + '"].apply(lambda x:' + 're.findall("' + regX + '",str(x))[' + str(
                idx) + '].replace("' + frontChar + '","").replace("' + rearChar + '","") if re.findall("' + regX + '",str(x)) else "" )'

        if len(filterModule) > 0:
            string = "df.loc[" + filterModule + ',"' + filterColumn + '"]=' + exp

        else:
            string += exp

        return True, string

    def deriveColumns(self, id, query):
        try:
            partVal = str(query['selectedText'])
            fullVal = str(query['completeValue'])
            column = str(query['selectedColumn'])
            filterModule = query.get('filterModule', '')
            filterColumn = query.get('filterColumn', '')

            expression = self.checkforSubstring(partVal, fullVal, column, filterModule, filterColumn)
            structure = SourceReference().find_one({'reconName': query['reconName'], 'source': query['source']})
            if expression:
                expression = expression.replace(column + '_Derived', query['columnName'])
                if len(query['filters']) > 0:
                    try:
                        query['filters'].index(expression)
                        index = 0
                    except:
                        index = -1
                    if index == -1:
                        query['filters'].append(expression)
                else:
                    query['filters'].append(expression)

            else:
                ext = "".join(['w' if x.isalpha() else '' 'd' if x.isdigit() else x for x in partVal])

                specialChar = ['/', ':', '-', '~', '_', '%']
                charList = [char for char in specialChar if char in partVal]

                wcount = ext.count('w')
                lcount = ext.count('d')
                scount = len("".join([x for x in ext if x not in ['w', 'd']]))

                if lcount > 0 and wcount > 0:
                    regX = '([A-Za-z0-9' + ''.join(charList) + ']+)'
                elif lcount + scount == len(partVal) or wcount + scount == len(partVal):
                    if lcount > 0:
                        regX = '([0-9' + ''.join(charList) + ']+)'
                    else:
                        regX = '([A-Za-z ' + ''.join(charList) + ']+)'

                frontVal, rearVal, frontChar, rearChar = self.checkforSurrounding(fullVal, partVal, regX)
                if frontVal:
                    regX = regX.replace('(', frontVal)
                if rearVal:
                    regX = regX.replace(')', rearVal)

                if '/' in regX:
                    regX = regX.replace('/', '\/')

                status, expression = self.generateFilter(column, fullVal, partVal, regX, frontChar, rearChar,
                                                         filterModule,
                                                         filterColumn)
                expression = expression.replace(column + '_Derived', query['columnName'])

                if len(query['filters']) > 0:
                    try:
                        query['filters'].index(expression)
                        index = 0
                    except:
                        index = -1
                    if index == -1:
                        query['filters'].append(expression)
                else:
                    query['filters'].append(expression)

            query['derivedColumns'].append(query['columnName'])

            structure['saveSourceDetails'] = False
            structure['filters'] = query['filters']
            if 'derivedColumns' in structure.keys():
                structure['derivedColumns'].extend(query['derivedColumns'])
            else:
                structure['derivedColumns'] = query['derivedColumns']
            if structure:
                status, sourceData = self.saveSourceDetails('dummy', structure, checkFileProperty=False,
                                                            deriveColumn=True)
            df = pandas.DataFrame(eval(sourceData['data']))
            df = df.fillna('')
            if df is not None:
                exec (expression)

            columns = self.getColumnType(df)
            query['saveSourceDetails'] = True

            query['feedDetails'] = columns
            query['data'] = df
            if '_id' in query:
                del query['_id']
            query['data'] = query['data'].to_dict(orient='records')
            query['data'] = json.dumps(query['data'], default=self.myConverter)

            return True, query
        except Exception as e:
            print traceback.format_exc()
            return False, 'Internal Error'

    def checkforSubstring(self, partVal, fullVal, column, filterModule, filterColumn):
        expression = None
        if fullVal == partVal:
            expression = "df['" + column + "_Derived']=df['" + column + "']"
            return expression
        fullVal = fullVal.replace(partVal, '$' * len(partVal))
        derived = fullVal.replace(' ', '')
        occursatFirstPosition = False
        occursatLastPosition = False
        if filterModule and filterColumn:
            initialPart = "df.loc[" + filterModule + ",'" + column + "_Derived']"
        else:
            initialPart = "df['" + column + "_Derived']"

        if derived.find('$') == 0:
            occursatFirstPosition = True
            if derived[derived.find('$') + len(partVal)].isalnum():
                expression = initialPart + " = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if derived.find('$') + len(partVal) == len(derived):
            occursatLastPosition = True
            if derived[len(derived) - len(partVal) - 1].isalnum():
                expression = initialPart + " = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if not occursatFirstPosition:
            if derived[derived.find('$') - 1].isalnum():
                expression = initialPart + " = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if not occursatLastPosition:
            if derived[derived.find('$') + len(partVal)].isalnum():
                expression = initialPart + " = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        return expression

    def checkforSurrounding(self, fullVal, partVal, regX):
        substring1 = fullVal[:fullVal.find(partVal)]
        substring2 = fullVal[fullVal.find(partVal) + len(partVal):]
        index1, index2 = self.getNearestSplIdx(fullVal, partVal, substring1, substring2)
        frontVal = None
        rearVal = None
        frontChar = ''
        rearChar = ''

        if index1 is not None:
            frontspaces = fullVal[index1 + 1:fullVal.find(partVal)]
            frontVal = '(' + substring1[index1] + frontspaces
            frontChar = substring1[index1]

        if index2 is not None:
            backspaces = fullVal[fullVal.find(partVal) + len(partVal):fullVal.find(partVal) + len(partVal) + index2]
            rearVal = backspaces + substring2[index2] + ')'
            rearChar = substring2[index2]
        return frontVal, rearVal, frontChar, rearChar

    def getNearestSplIdx(self, fullVal, partVal, substring1, substring2):
        specialChar = {'/', ':', '-', '~', '_', '%'}
        indexList1 = []
        indexList2 = []
        for char in specialChar:
            if char in substring1:
                indexList1.append(max(self.findOccurrences(substring1, char)))
            if char in substring2:
                indexList2.append(min(self.findOccurrences(substring2, char)))

        # logr.infoindexList1, indexList2

        if indexList1 and indexList2:
            return max(indexList1), min(indexList2)

        elif indexList1 or indexList2:
            if indexList1:
                return max(indexList1), None
            else:
                return None, min(indexList2)
        else:
            return None, None

    def findOccurrences(self, s, ch):
        return [i for i, letter in enumerate(s) if letter == ch]


#  Recon Meta Info
class ReconMetaInfo(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconMetaInfo, self).__init__("reconMetaInfo", hideFields)

    def create(self, doc):
        (status, reconMeta) = super(ReconMetaInfo, self).create(doc)
        return status, reconMeta


# FixedStructure
class FixedStructure(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(FixedStructure, self).__init__("fixedStructure", hideFields)
        self.positions = []
        self.names = []
        self.typedic = {}

    def create(self, doc):
        (status, fixedstruct) = super(FixedStructure, self).create(doc)
        return status, fixedstruct

    def getDatePatterns(self, data):
        doc = list(FixedStructure().find({'filePattern': data}))
        print "==indside the datepatterns function=========="
        return doc[0]['feedDetails']

    def buildColPostionsandNames(self, data):
        fs = FixedStructure()
        print "===building the cols==============="

        try:

            reqDoc = FixedStructure().find_one({'filePattern': data['filePattern']})

            print "============this is the reqDoc======="
            typesdic = {}
            for eachDict in reqDoc['feedDetails']:
                self.positions.append((int(eachDict['From']), int(eachDict['To'])))
                self.names.append(eachDict['fileColumn'])
                typesdic[str(eachDict['fileColumn'])] = str(eachDict['DataType'])
                pos = self.positions
                colnames = self.names
            print "======in build cols============"

            return pos, colnames, typesdic
        except Exception as e:
            print e


# Recon Processes information collection
class ReconProcess(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconProcess, self).__init__("reconProcessDetails", hideFields)

    def create(self, doc):
        (status, reconProcessDetails) = super(ReconProcess, self).create(doc)
        return status, reconProcessDetails

    def saveProcessDetail(self, id, query):
        doc = ReconProcess().find_one({'reconProcess': query['reconProcess']})
        if doc:
            doc['reconProcess'] = query['reconProcess']
            if 'imagePath' in query.keys():
                doc['imagePath'] = query['imagePath']
            if 'isLicensed' in query.keys():
                doc['isLicensed'] = query['isLicensed']
            status, data = ReconProcess().modify(doc['_id'], doc)
            return True, 'Process updated successfully'
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        query['isLicensed'] = False
        doc = ReconProcess().save(query)
        return True, 'Uploaded'

    def getJobProcess(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            doc['statementDate'] = doc['statementDate'].strftime("%d%m%Y")
        return True, doc


#  Recon Assigned Collection
class ReconAssignment(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def create(self, doc):
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def deleteData(self, condition):
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        return super(ReconAssignment, self).delete(condition)

    # Get Assigned Recons For logged In User
    def assignedRecons(self, id, query):
        status, assignData = ReconAssignment().getAll({'condition': {'userName': query['userName']}})
        if status and assignData['total'] == 0:
            return False, 'No Recon Assignment For this User'
        status, reconsData = Recons().getAll({})
        reconsList = {}
        assignedRecons = []
        for doc in assignData['data']:
            for recon in reconsData['data']:
                if recon['reconProcess'] in doc['reconProcesses']:
                    reconsList[recon['reconProcess']] = [] if recon['reconProcess'] not in reconsList.keys() else \
                        reconsList[recon['reconProcess']]
                    if recon['reconName'] in doc['reconNames']:
                        reconsList[recon['reconProcess']].append(recon['reconName'])
                        assignedRecons.append(recon['reconName'])
        detailsLog = []
        for i in assignedRecons:
            stat, logDetails = ReconExecDetailsLog().getLatestExeDetails(i)
            if logDetails:
                for process in reconsList:
                    if i in reconsList[process]:
                        logDetails['reconProcess'] = process
                        logDetails['statementDate'] = datetime.datetime.strftime(logDetails['statementDate'],
                                                                                 '%Y-%m-%d')
                detailsLog.append(logDetails)
            else:
                doc = {'reconName': i}
                for process in reconsList:
                    if i in reconsList[process]:
                        doc['reconProcess'] = process
                detailsLog.append(doc)
        data = {'total': len(detailsLog), 'reconsLog': detailsLog, 'data': reconsList}
        return True, data

    def getReconDetails(self):
        recondata = list(Recons().find({}))
        recondetails = []
        if len(recondata):
            reconAssign = ReconAssignment()
            users = []
            users.append(flask.session["sessionData"]["userName"])
            assignData = list(reconAssign.find({'users': {"$in": users}}))

            if assignData:
                for data in recondata:
                    for recon in assignData:
                        if data['reconName'] in recon['reconNames']:
                            recondetails.append(data)
        return True, recondetails

    def getUsers(self, id, query):
        usernames = []
        roles = hashframe.config.roles
        reconAssignments = list(ReconAssignment().find(
            {'groupRole': roles[query['role']], 'reconNames': {'$in': query['recons']}}))

        for i in reconAssignments:
            usernames.extend(i['users'])
        users = list(Users().find({"role": {'$ne': 'admin'}, 'ApprovalStatus': 'Approved'}))
        allUsers = [user['userName'] for user in users]
        availableUsers = list(set(allUsers) - set(usernames))
        return True, availableUsers


# reorder Column Collection
class ColumnConfiguration(hashframe.MongoCollection):
    def __init__(self):
        super(ColumnConfiguration, self).__init__('columnConfiguration')

    def create(self, doc):
        (status, columnConfiguration) = super(ColumnConfiguration, self).create(doc)
        return status, columnConfiguration


# recon Summary Collection
class ReconSummary(hashframe.MongoCollection):
    def __init__(self):
        super(ReconSummary, self).__init__('recon_summary')

    def create(self, doc):
        (status, reconSummary) = super(ReconSummary, self).create(doc)
        return status, reconSummary

    def getSummary(self, query):
        doc = ReconSummary().find_one(query)
        return True, doc


# Recon Details Collections
class ReconDetails(hashframe.MongoCollection):
    def __init__(self):
        super(ReconDetails, self).__init__("recon_details")

    def create(self, doc):
        (status, recon_details) = super(ReconDetails, self).create(doc)
        return status, recon_details

    def getDashboardSummary(self, id):
        status, doc = ReconAssignment().getReconDetails()
        if status:
            total = len(doc)
            ExecLog = []
            data = {'reconsExceuted': 0, 'successCount': 0, 'failureCount': 0}
            for recon in doc:
                if recon['reconProcess'] not in data.keys():
                    # data[recon['reconProcess']] = []
                    data['executedReconsList'] = []
                    data[recon['reconProcess']] = []
                data[recon['reconProcess']].append(recon['reconName'])
                execlog = list(ReconExecDetailsLog().getMaxReconExecutionDoc([recon['reconName']]))

                # execlog = ReconExecDetailsLog().find_one(
                #     {"reconName": recon['reconName']},
                #     sort=[('RECON_EXECUTION_ID', -1)])
                if execlog:
                    execlog = execlog[0]
                    data['reconsExceuted'] += 1
                    data['executedReconsList'].append(recon['reconName'])
                    if execlog['jobStatus'] == 'Success':
                        data['successCount'] += 1
                    else:
                        data['failureCount'] += 1
                det = list(ReconExecDetailsLog().getMaxReconExecutionDoc([recon['reconName']]))

                if det is not None and len(det) > 0:
                    det = det[0]
                    det['reconProcess'] = recon['reconProcess']
                    ExecLog.append(det)
            if len(ExecLog):
                data['msg'] = 'reconExecution details'
                data['total'] = len(ExecLog)
                for i in ExecLog:
                    i['statementDate'] = datetime.datetime.strftime(i['statementDate'], '%Y-%m-%d')
            # else:
            #     return True, "There is no execution details"
            data['totalAssignedRecons'] = total
            data['reconDeatails'] = doc
            data['reconExecutionDetailsLog'] = ExecLog
            return True, data
        else:
            return False, 'No Recons assigned for user'

    def exportAllCsv(self, id, query):
        data = {}
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            fileName = filePath + '/' + query['reportPath']
            reportPath = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/' + query['reportPath']
            copyfile(reportPath, fileName)
            fileName = flask.session["sessionData"]["userId"] + '/' + query['reportPath']
            return True, fileName
        return False, 'No Execution Details Available'

    def getOlfFormatReport(self, id, query):
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            path = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/'
            outfiles = os.listdir(path)
            columnList = []
            alldf = pandas.DataFrame()
            recon = Recons().find({'reconName': query['reconName']})
            if status:
                for source in recon['sources'].keys():
                    if query['type'] == 'Matched':
                        files = [source + '.csv', source + '_Self_Matched.csv']
                    elif query['type'] == 'UnMatched':
                        files = [source + '_UnMatched.csv']

                    for file in files:
                        if file in os.listdir(path):
                            status, converters = ReconDetails().getConverters(query['reconName'], source)
                            # converters['System_Idx'] = str
                            logr.info(path + file)
                            df = pandas.read_csv(path + file)
                            for key, val in converters.iteritems():
                                df[key] = df[key].astype(val)
                            if df.empty:
                                continue
                            status, doc = ColumnConfiguration().get({"reconName": query['reconName'], "source": source})
                            if status:
                                columnList.extend(doc['columns'])
                                seen = set()
                                columnList = [x for x in columnList if not (x in seen or seen.add(x))]

                            # matchfil = ['(df["%s"].fillna("") == "MATCHED")' % col for col in df.columns if
                            #             "Match" in col and 'Self Matched' not in col]
                            # matchfil = "&".join(matchfil)
                            # if 'Self Matched' in df.columns:
                            #     matchfil = matchfil + '|(df["Self Matched"].fillna("")=="MATCHED")'
                            # matchfil = eval(matchfil)

                            if len(df) > 0:
                                # logr.info(len(df))
                                if query['type'] == 'Matched':
                                    df['MATCHING_STATUS'] = 'MATCHED'
                                elif query['type'] == 'UnMatched':
                                    df['MATCHING_STATUS'] = 'UNMATCHED'

                                # df.loc[matchfil, 'MATCHING_STATUS'] = 'MATCHED'
                                # df.loc[df['MATCHING_STATUS'].fillna('') == '', 'MATCHING_STATUS'] = 'UNMATCHED'
                                # df=df[keyColumns+['MATCHED']]
                                # if alldf.empty:
                                #     alldf = pandas.concat([df, alldf], join_axes=[df.columns])
                                # else:
                                #     alldf = pandas.concat([df, alldf], join_axes=[alldf.columns])
                                alldf = alldf.append(df)
                                logr.info(alldf.head())
                                if len(columnList) > 0:
                                    alldf = alldf[columnList + ['MATCHING_STATUS']]
                                alldf = alldf.reset_index(drop=True)
                # logr.info(alldf['MATCHING_STATUS'].unique())
                # if query['type']=='Matched':
                #     alldf=alldf[alldf['MATCHING_STATUS']=='MATCHED']
                # else:
                #     alldf=alldf[alldf['MATCHING_STATUS']=='UNMATCHED']

            for col in alldf.columns.tolist():
                alldf[col] = alldf[col].fillna(ReconDetails().infer_dtypes(alldf[col]))

            alldf = alldf.replace('nan', '')
            alldf.rename(columns={'SOURCE': "SOURCE_NAME"}, inplace=True)

            # if query['reconName'] in hashframe.config.reconMapper.keys():
            #     infoJson=hashframe.config.reconMapper[query['reconName']]
            #     if 'expressions' in infoJson:
            #         for exp in infoJson['expressions']:
            #             exec(exp)
            #
            #     if 'sortOn' in infoJson:
            #         alldf=alldf.sort_values(by=infoJson['sortOn'])
            #     if 'apostrophe' in infoJson:
            #         for col in infoJson['apostrophe']:
            #             alldf[col]="'"+alldf[col].astype(str)

            fileName = query['reconName'] + '_' + query['type'] + '_' + str(
                datetime.datetime.now()) + '.csv'
            alldf.to_csv(filePath + os.sep + fileName, index=False)
            return True, flask.session["sessionData"]["userId"] + '/' + fileName
        else:
            return False, 'No Execution Details Available'

    def getReportDetails(self, id, query):
        details = ReconDetails().find_one({'reconName': query['reconName']})
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
            summaryQuery = {'reconName': query['reconName'], 'statementDate': doc['statementDate'],
                            'reconExecutionId': doc['reconExecutionId']}
            status, summary = ReconSummary().getSummary(summaryQuery)
            if not summary:
                summary = {'details': []}

            summary['sources'] = []
            summary['matchedCount'] = []
            summary['unMatchedCount'] = []
            sourceGroup = {}
            sourceGroup['defaultReport'] = {}
            for report in details['reportDetails']:
                # report['source'] = str(report['source']).replace(' ', '_')
                # if report['source'] not in summary['sources']:
                #     summary['sources'].append(report['source'])
                #     summary['matchedCount'].append()
                for i in summary['details']:
                    if i['Source'] == report['source']:
                        if report['source'] not in summary['sources']:
                            summary['sources'].append(report['source'])
                            summary['matchedCount'].append(i['Matched Count'])
                            summary['unMatchedCount'].append(i['UnMatched Count'])
                        # i['Execution Date Time'] = datetime.datetime.strptime(i['Execution Date Time'],"%d-%m-%Y")
                        #           if type(i['statementDate']) == datetime.datetime:
                        #               i['statementDate'] = datetime.datetime.strftime(i['Statement Date'], "%d-%m-%Y")
                        #               i['Execution Date Time'] = i['Execution Date Time'].strftime("%d-%m-%Y")
                        if report['reportName'] == 'Unmatched':
                            i['unMatchedReport'] = report['reportPath']
                        elif report['reportName'] == 'Matched':
                            i['matchedReport'] = report['reportPath']
                        elif report['reportName'] == report['source'] + ' ' + 'Self Match':
                            i['reversalReport'] = report['reportPath']

                if report['source'] == 'Reports':
                    if 'customReports' not in summary:
                        summary['customReports'] = []
                    summary['customReports'].append(
                        {"name": report['reportName'], 'reportPath': report['reportPath']})

                summary['statementDate'] = doc['statementDate'].strftime("%d-%m-%Y")

            # else:
            #     return False, 'No summary Found'

        else:
            return False, 'No execution found for this recon'
        return True, summary

    #  Filtered Data with advanced Search
    def getFilteredData(self, id, query):
        records = {}
        df = pandas.DataFrame()
        if 'stmtDate' in query.keys() and query['stmtDate']:
            stmtDate = datetime.datetime.strptime(str(query['stmtDate']), '%d/%m/%y').strftime("%d%m%Y")

        else:
            status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], query['source'])

        path = hashframe.config.mftPath + str(query['reconName']) + '/' + stmtDate + '/' + 'OUTPUT/' + str(
            query['reportPath'])
        if not os.path.exists(path):
            return False, "File Path Doesn't Exist"
        df = pandas.read_csv(path)

        if 'filters' in query.keys() and query['filters']:
            for expression in query['filters']:
                exec expression

        index = query['index']
        pageSize = query['pageSize']
        length = len(df)

        maxPages = ((length / pageSize) + 1)
        skip = (index * pageSize) - pageSize

        limit = (index * pageSize)
        if limit < 0:
            limit = len(df) + 1

        df = df[skip:limit]

        for col in df.columns:
            df[col] = df[col].fillna(self.infer_dtypes(df[col]))
        columnGrouper = self.columnDTypeGrouper(converters)

        records['data'] = df.to_dict(orient='records')
        records['maxPages'] = maxPages
        records['columns'] = [col for col in df.columns]
        records['reconName'] = query['reconName']
        records['report'] = query['reportPath']
        records['columnGrouper'] = columnGrouper
        return True, records

    # get Authorize Recoords
    def getAuthourizedRecords(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/'
        details = Recons().find_one({'reconName': query['reconName']})
        sources = details['sources']
        totalDf = pandas.DataFrame()
        for source in sources:
            sourcePath = path + source + '_Authorization.csv'
            if (os.path.exists(sourcePath)):
                df = pandas.read_csv(sourcePath)
                totalDf.append(df)
        if len(totalDf) > 0:
            return True, totalDf
        else:
            return False, 'No Data Found'

    #  get Reports Data With Pagination
    def getReportData(self, id, query):
        records = {}
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], query['source'])
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query['reportPath']
        print(path)
        if (os.path.exists(path)):
            index = query['index']
            pageSize = query['pageSize']
            length = int(subprocess.check_output("wc -l " + path.replace(' ', '\ '), shell=True).split()[0])
            maxPages = ((length / pageSize) + 1)

            skip = (index * pageSize) - pageSize
            limit = length - (index * pageSize)
            if maxPages:
                records['maxPages'] = maxPages
            # initialize headers
            with open(hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                'reportPath'], 'r') as file:
                reader = csv.reader(file)
                header = reader.next()
                df = pandas.read_csv(
                    hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                        'reportPath'],
                    skiprows=skip + 1, skipfooter=0 if limit < 0 else limit, names=header,
                    header=None,
                    engine='python')

            for col in df.columns:
                df[col] = df[col].fillna(self.infer_dtypes(df[col]))
            columnGrouper = self.columnDTypeGrouper(converters)

            if query['carryForward']:
                df['CARRY_FORWARD'] = df['CARRY_FORWARD'].astype(str)
                df = df[df['CARRY_FORWARD'] == 'Y']
            else:
                if 'CARRY_FORWARD' in df.columns and (
                        'UnMatched' in query['reportPath'] or 'Matched' in query['reportPath']):
                    df['CARRY_FORWARD'] = df['CARRY_FORWARD'].astype(str)
                    df = df[df['CARRY_FORWARD'] != 'Y']

            status, columnsDoc = ColumnConfiguration().get({'reconName': query['reconName'], 'source': query['source']})
            if status:
                df = df[columnsDoc['columns']]
            records['data'] = df.to_dict(orient='records')
            records['columns'] = [col for col in df.columns]
            records['reconName'] = query['reconName']
            records['report'] = query['reportPath']
            records['columnGrouper'] = columnGrouper
        else:
            return False, "No Files are there"
        return True, records

    def getConverters(self, reconName, source):
        doc = ColumnDataTypes().getColumnDataTypes(reconName, source)
        converters = {}
        if doc:
            for val in doc['dtypes']:
                if str(val['dtype']) != 'np.datetime64':
                    converters[val['column']] = self.typeMapper(val['dtype'])
                else:
                    converters[val['column']] = str
        else:
            logr.info('-------------------------')
            logr.info('No Column Dtypes Found')
            return False, 'No Column Dtypes Found'
        return True, converters

    # Download CustomReports
    def downloadCustomReport(self, id, query):
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if status:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            filepath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filepath):
                os.mkdir(filepath)
            if not query.get('writeToMongo'):
                path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                       query['report'][
                           'reportPath']
                if not os.path.exists(path):
                    return True, 'Report file not found'
                copyfile(path, filepath + '/' + query['report']['reportPath'])
            else:
                pass
            return True, flask.session["sessionData"]["userId"] + '/' + query['report']['reportPath']

    def columnDTypeGrouper(self, converters):
        columnGrouper = {"string": [], "numeric": []}
        for key, val in converters.iteritems():
            if val == str:
                columnGrouper["string"].append(key)
            elif val == (np.float64 or np.int64):
                columnGrouper["numeric"].append(key)
        return columnGrouper

    def typeMapper(self, dtype):
        if dtype == 'str':
            return str
        elif dtype == 'np.float64':
            return np.float64

        elif dtype == 'np.int64':
            return np.int64
        else:
            return np.datetime64

    def infer_dtypes(self, series):
        if is_float_dtype(series):
            return 0.0
        elif is_integer_dtype(series):
            return 0
        elif is_string_dtype(series):
            return ''
        elif is_datetime64_dtype(series):
            return pandas.NaT
        else:
            np.nan

    def updateReportName(self, id, query):
        report = query['report']
        data = ReconDetails().update(
            {'reconName': query['selectedRecon'], 'reportDetails.reportPath': report['reportPath']},
            {'$set': {'reportDetails.$.reportName': report['reportName']}})
        return True, data

    def getReports(self, id, query):
        reportData = ReconDetails().find_one({'reconName': query['reconName']})
        return True, reportData

    def interFaceHelical(self, id):
        cmd = "java -jar /home/gumma/Downloads/Helical-Insight-Custom-Authentication-SSO/Encryption-Decryption.jar 'tenant=helical|username={usr}|role=reconciler'".format(
            usr=flask.session["sessionData"]["userName"])
        encryptText = subprocess.check_output(cmd, shell=True)
        text = encryptText.split('\n')[0].split('::')[1]
        url = "http://12`3.201.255.101:8085/hi-ee/welcome.html?authToken="
        url = url + text

        webbrowser.open_new(url)
        return True, ''

    def getHistoricalData(self, id, query):
        user = flask.session['sessionData']['userName']
        password = flask.session['sessionData']['password']
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, password)
        decrypt = decrypt.replace('/<=%+,?->/', '')
        s = requests.Session()
        a = s.post('https://erecon.com/api/login',
                   data=json.dumps({"userName": user,
                                    "password": decrypt}),
                   headers={"Content-Type": "application/json"}, verify=False)
        reconMapper = hashframe.config.reconMapper
        if a.status_code == 200:
            doc = {"accountNumber": "",
                   "executionEndDate": 0,
                   "executionStartDate": 0,
                   "exportData": True,
                   "recon_id": reconMapper[query['recon_name']]['reconId'],
                   "recon_name": reconMapper[query['recon_name']]['reconName'],
                   "statementEndDate": query['statementEndDate'],
                   "statementStartDate": query['statementStartDate'],
                   }
            headers = {'Content-Type': 'application/json'}
            data = {'query': doc}
            result = s.post('https://erecon.com/api/recon_assignment/dummy/getReconMatchedStatementDate',
                            headers=headers, data=json.dumps(data))
            result = json.loads(str(result.text))
            fileName = ''
            if result['status'] == 'ok' and result['msg']['txn_count'] > 0:
                fileName = result['msg']['fileName']
                oid = fileName.split('/')[0]
                downloadedfileName = '\ '.join(fileName.split('/')[1].split(' '))
                completeFileName = '/usr/share/nginx/www/erecon/ui/app/files/recon_reports/' + oid + '/' + downloadedfileName
                filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
                if not os.path.exists(filePath):
                    os.mkdir(filePath)
                cmd = 'mv ' + completeFileName + ' ' + filePath
                os.system(cmd)
                fileName = flask.session["sessionData"]["userId"] + '/' + fileName.split('/')[-1]

        return True, fileName
    # Download CustomReports
    # def downloadCustomReport(self, id, query):
    #     status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
    #     if status:
    #         stmtDate = execLog['statementDate'].strftime("%d%m%Y")
    #         filepath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
    #         if not os.path.exists(filepath):
    #             print 'hi'
    #             os.mkdir(filepath)
    #         path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
    #                query['report']['path']
    #         if not os.path.exists(path):
    #             return True, 'Report file not found'
    #         copyfile(path, filepath + '/' + query['report']['path'])
    #         return True, flask.session["sessionData"]["userId"] + '/' + query['report']['path']


class MatchRuleSumColumns(hashframe.MongoCollection):
    def __init__(self):
        super(MatchRuleSumColumns, self).__init__("matchRuleSumColumns")

    def create(self, doc):
        (status, matchRuleSumColumns) = super(MatchRuleSumColumns, self).create(doc)
        return status, matchRuleSumColumns


# Match Rule for Corresponding Recons
class MatchRuleReference(hashframe.MongoCollection):
    def __init__(self):
        super(MatchRuleReference, self).__init__("matchRuleReference")

    def create(self, doc):
        (status, matchRuleReference) = super(MatchRuleReference, self).create(doc)
        return status, matchRuleReference

    def saveMatchRule(self, id, query):

        logr.info("Saving Match Rule and Processing Rules Started")

        sumColumnsDict = {}
        sumColumnsDict['reconName'] = query['reconName']
        sumColumnsDict['reconProcess'] = query['reconProcess']
        sumColumnsDict['sumColumns'] = [] if isinstance(query.get('sumColumns', []), str) else query.get('sumColumns',
                                                                                                         [])
        sumColumnsDict['subsetFilter'] = ''
        sumColumnsDict['partnerId'] = "54619c820b1c8b1ff0166dfc"

        # check if match rules sum coulmns already exists in the  MatchRuleSumColumns collection and update
        status, sumMatch = MatchRuleSumColumns().get(
            {'reconName': query['reconName'], 'reconProcess': query['reconProcess']})

        if status:
            MatchRuleSumColumns().modify(sumMatch['_id'], sumColumnsDict)
        else:
            status = MatchRuleSumColumns().save(sumColumnsDict)
        doc = MatchRuleReference().find_one({'reconName': query['reconName'], 'reconProcess': query['reconProcess']})

        # this section is to build the matchColumns, sumColumns. debitCredit Column from the connections made in the flowchart
        if doc and 'Rules' in doc.keys():
            columns = {}
            for rule in doc['Rules']:
                connections = rule['connections']
                nodes = rule['nodes']
                connections = self.getAllSourcePoints(connections)
                self.buildMatchAndSumCols(query, connections, columns, nodes, rule)

            # formatColumns func is to bring the rules to Nway Match input format
            columns['Rules'] = self.formatColumns(columns)
            columns['reconName'] = query['reconName']
            columns['reconProcess'] = query['reconProcess']

            # runJob func will load the files execute the filters and run the recon.
            status, data = self.runJob(columns)

            # dumpDataToFiles will dump the data to files
            self.dumpDataToFiles(data, query)

            # the below section is to calculate the count of matched,unmatched and self matched records.
            summary_source = []
            for key, val in data['results'].iteritems():
                if len(val)>0:
                    source_summary = self.compute_stats(key, val)
                    for key in ['Count#Matched', 'Count#UnMatched', 'Count#Reversal']:
                        if key not in source_summary[0]:
                            source_summary[0][key] = 0
                    summary_source.extend(source_summary)
            source_df = pandas.DataFrame(summary_source)
            # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
            rename_mapper = {}
            for col in source_df.columns:
                if 'Count#' in col:
                    aggr, match_status = tuple(col.split('#'))
                    rename_mapper[col] = match_status + ' ' + aggr
                else:
                    rename_mapper[col] = col.rstrip('#')
            source_df.rename(columns=rename_mapper, inplace=True)
            source_dict = source_df.to_dict(orient='records')
            return True, source_dict
        else:
            return False, 'Rules Not Defined'

    def buildMatchAndSumCols(self, query, connections, columns, nodes, rule):
        connectionList = []
        columns[rule['title']] = {}
        for connection in connections:
            if connection['source'].get('isBegining', False):
                connectionList.append(connection['source']['nodeID'])
                connectionList.append(connection['dest']['nodeID'])
                connectionList = self.traverse(connections, connectionList, connection['dest']['nodeID'], dest=True)
                for node in nodes:
                    for i in connectionList:
                        if node['id'] == i:
                            if node['className'] not in columns[rule['title']]:
                                columns[rule['title']][node['className']] = {'matchColumns': [], 'sumColumns': [],
                                                                             'debitCreditColumn': []}
                                columns[rule['title']][node['className']]['groupByFunc'] = \
                                    query['sumColumns'][rule['title']][node['className']].get('groupByFunc', None)

                            if node['name'] not in columns[rule['title']][node['className']]['matchColumns']:
                                columns[rule['title']][node['className']]['matchColumns'].append(node['name'])

                            for key in ['sumColumns', 'debitCreditColumn']:
                                columns[rule['title']][node['className']][key] = self.getSumColumns(rule['title'],
                                                                                                    node['className'],
                                                                                                    query, key)
                connectionList = []

    def formatColumns(self, columns):
        rules = {}
        sourceList = []
        for key, val in columns.iteritems():
            rules[key] = []
            for k, v in val.iteritems():
                source = {}
                if k not in sourceList:
                    source['source'] = k
                    sourceList.append(k)
                else:
                    source['source'] = 'results.' + k

                source['sourceTag'] = k
                if 'subsetFilter' in v:
                    source['subsetFilter'] = v['subsetFilter']
                source['columns'] = {}
                source['columns']['sumColumns'] = v['sumColumns']
                source['columns']['debitCreditColumn'] = v['debitCreditColumn']
                source['columns']['matchColumns'] = v['matchColumns']
                source['columns']['fuzzyColumns'] = v['fuzzyColumns']
                rules[key].append(source)
        print rules

        return rules

    def traverse(self, connections, connectionList, conid, dest):
        for connection in connections:
            if dest and conid in connection['source']['nodeID']:
                if not connection['dest'].get('isEnd', False):
                    connectionList.append(connection['dest']['nodeID'])
                    self.traverse(connections, connectionList, connection['dest']['nodeID'], dest=False)
                else:
                    connectionList.append(connection['dest']['nodeID'])
                    return connectionList
            else:
                if conid in connection['source']['nodeID']:
                    self.traverse(connections, connectionList, connection['source']['nodeID'], dest=True)
        return connectionList

    def getAllSourcePoints(self, connections):
        for index, Sconnection in enumerate(connections):
            isBegining = True
            isEnd = True
            for connection in connections:
                if Sconnection['source']['nodeID'] == connection['source']['nodeID']:
                    continue
                if Sconnection['source']['nodeID'] == connection['dest']['nodeID']:
                    isBegining = False
                if Sconnection['dest']['nodeID'] == connection['source']['nodeID']:
                    isEnd = False
            if isBegining:
                connections[index]['source']['isBegining'] = True
            if isEnd:
                connections[index]['dest']['isEnd'] = True
        return connections

    def getSumColumns(self, ruleName, source, query, key):
        sumColumns = MatchRuleSumColumns().find_one(
            {'reconName': query['reconName'], 'reconProcess': query['reconProcess']})
        doc = {'sumColumns': 'sumColumns', 'debitCreditColumn': 'debitColumns','subsetFilter':'subsetFilter','fuzzyColumns':'fuzzyColumns'}
        print(sumColumns)
        # exit()
        if doc[key] in sumColumns['sumColumns'][ruleName][source].keys():
            return sumColumns['sumColumns'][ruleName][source][doc[key]]
        else:
            return []

    def runJob(self, query):
        # logr.info('=========Job Running Started==========')
        sources = list(SourceReference().find({'reconName': query['reconName']}))
        # logr.info('Number of sources found -->' + str(len(sources)))
        self.sourceData = {}
        for source in sources:
            feed_def = pandas.DataFrame(source['feedDetails']).fillna('')
            required_fields = feed_def[feed_def['required'].replace('', False)]
            required_fields['position'] = required_fields['position'].astype(np.int64) - 1
            columnNames = required_fields['fileColumn'].tolist()

            filters = source['filters']
            converters = {
                feed['fileColumn']: eval(feed['dataType']) if feed['dataType'] != 'np.datetime64' else eval('str') for
                feed in source['feedDetails']}

            dateColumns = [feed for feed in source['feedDetails'] if feed['dataType'] == 'np.datetime64']
            sourcePath = '/usr/share/nginx/www/ngerecon/ui/files/source' + os.sep + source['fileName']
            # logr.info('Finding Feed File in path -->' + sourcePath)
            if not os.path.exists(sourcePath):
                # logr.info('Feed File Not found For source' + source['source'])
                return False, 'Feed File Not found For source' + source['source']
            if source['loadType'] == 'CSV':

                df = pandas.read_csv(sourcePath, skiprows=int(source.get('skiprows', 0)) + 1,
                                     skipfooter=int(source.get('skipfooter', 0)), delimiter=str(source['delimiter']),
                                     converters=converters, engine='python',
                                     usecols=list(required_fields.get('position')),
                                     header=None, names=columnNames
                                     )
                print(df.head())
                print(sourcePath)

            for dateCols in dateColumns:
                dtpattern = dateCols["datePattern"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                    df['tdlen'] = pandas.Series(df[dateCols['fileColumn']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['fileColumn']] = df.apply(
                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                                dateCols['fileColumn']], axis=1)
                    del df['tdlen']
                df[dateCols['fileColumn']] = pandas.to_datetime(df[dateCols['fileColumn']], format=dtpattern,

                                                                errors='raise')

            # loadin opf the fixed format file starts
            if source['loadType'] == 'FixedFormat':
                patternList = []
                remainingList = []
                print "came ============inside==== for reading the fixeddddddddddddd==============================="
                pos, colnames, typesdic = FixedStructure().buildColPostionsandNames(source)
                datepatternsList = FixedStructure().getDatePatterns(source['filePattern'])

                data_types_date = {}
                data_types = {}
                for i in typesdic:
                    if typesdic[i] == 'np.datetime64':
                        data_types_date[i] = typesdic[i]
                        data_types[i] = eval('str')
                    else:
                        data_types[i] = eval(typesdic[i])
                print 'this is the first file==========================='
                df = pandas.read_fwf(sourcePath,
                                     header=None, colspecs=pos, names=colnames,
                                     converters=data_types, skiprows=int(source.get('skiprows', 0)),
                                     skipfooter=int(source.get('skipfooter', 0)))
                print datepatternsList
                print data_types_date.keys()
                for (dateCols, key) in zip(datepatternsList, data_types_date.keys()):

                    if dateCols['fileColumn'].strip() == key.strip():
                        print 'key and pattern are equal=================='
                        print dateCols['DatePattern']

                        dtpattern = dateCols["DatePattern"]
                        print dtpattern
                        if dtpattern == '%m%d%y':

                            df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                            df['tdlen'] = pandas.Series(df[dateCols['fileColumn']]).str.len()

                            if list(df['tdlen'].unique()) != [8]:
                                df[dateCols['fileColumn']] = df.apply(
                                    lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                                        dateCols['fileColumn']], axis=1)
                                del df['tdlen']
                df[dateCols['fileColumn']] = pandas.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                                errors='raise')  # , errors='coerce'
                df.columns = df.columns.str.strip()

                refList = list(SourceReference().find({'reconName': query['reconName'], 'source': source['source']}))

                if len(refList) > 1:

                    for eachSource in refList:
                        patternList.append(eachSource['filePattern'])
                    for pattern in patternList:

                        if source['filePattern'] != pattern:
                            remainingList.append(pattern)
                    for i in range(0, len(remainingList)):

                        source_i = list(
                            SourceReference().find({'reconName': query['reconName'], 'filePattern': remainingList[i]}))

                        pos_i, colnames_i, typesdic_i = FixedStructure().buildColPostionsandNames(source_i[0])

                        nxtsourcePath = '/usr/share/nginx/www/ngerecon/ui/files/source' + os.sep + source_i[0][
                            'fileName']

                        data_types_date1 = {}
                        data_types1 = {}
                        for i in typesdic:
                            if typesdic[i] == 'np.datetime64':
                                data_types_date1[i] = typesdic[i]
                                data_types1[i] = eval('str')

                            else:
                                data_types1[i] = eval(typesdic[i])

                        datepatternsList1 = FixedStructure().getDatePatterns(source_i[0]['filePattern'])

                        df_i = pandas.read_fwf(nxtsourcePath,
                                               header=None, colspecs=pos_i, names=colnames_i,
                                               converters=data_types, skiprows=int(source_i[0].get('skiprows', 0)),
                                               skipfooter=int(source_i[0].get('skipfooter', 0)))

                        for (dateCols, key) in zip(datepatternsList1, data_types_date1.keys()):

                            if dateCols['fileColumn'].strip() == key.strip():
                                print 'key and pattern are equal=================='

                                dtpattern = dateCols["DatePattern"]

                                if dtpattern == '%m%d%y':

                                    df_i[dateCols['fileColumn']] = df_i[dateCols['fileColumn']].astype(str)
                                    df_i['tdlen'] = pandas.Series(df_i[dateCols['fileColumn']]).str.len()

                                    if list(df_i['tdlen'].unique()) != [8]:
                                        df_i[dateCols['fileColumn']] = df_i.apply(
                                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                                                dateCols['fileColumn']], axis=1)
                                        del df_i['tdlen']
                        df_i[dateCols['fileColumn']] = pandas.to_datetime(df_i[dateCols['fileColumn']],
                                                                          format=dtpattern,
                                                                          errors='raise')  # , errors='coerce'
                        df_i.columns = df_i.columns.str.strip()
                        df = df.append(df_i)

            if len(filters) > 0:
                df = self.executeFilters(df, filters)
            self.sourceData[source['source']] = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

        # logr.info('=========Loading Files Completed=========')

        # run func is where the records are sent for matching process.
        self.sourceData = self.run(query['Rules'])

        return True, self.sourceData

    def executeFilters(self, df, filters):
        for exp in filters:
            exec exp

        return df

    def getValues(self, obj={}, ref='', sep='.', default=None):
        for key in ref.split(sep):
            if obj is not None and key in obj.keys():
                obj = obj[key]
            elif default is not None:
                return default
            else:
                raise KeyError('Key "%s" not found in input object' % key)
        return obj

    def setValues(self, obj={}, ref='', set_value='', sep='.'):
        keys = ref.split(sep)
        count = 1
        for x in keys:
            if count <= len(keys) - 1:
                if x in obj:
                    continue
                else:
                    obj[x] = {}
                count += 1
        if keys:
            exec "obj['" + "']['".join(keys) + "'] = set_value"
        else:
            raise KeyError('Ref keys cannot be empty %s' % ref)

    def run(self, rules):
        payload = self.sourceData
        for ruleName, property in rules.iteritems():
            result = {}
            remainder = []
            for i in range(0, len(property)):
                source = property[i].copy()

                data = self.getValues(payload, source['source'], sep='.').reset_index(drop=True)

                # Do not allow matched records & records with null values
                sub_filters = ["df = df[df['%s'] != 'MATCHED']" % col for col in data.columns if ' Match' in col]

                match_cols = source['columns'].get('matchColumns', []) + source['columns'].get('sumColumns', [])
                # match_cols.append(source['columns'].get('sumColumns', []))

                match_cols = [x.rstrip() for x in match_cols]

                # match_cols = match_cols.str.stip()
                for col in match_cols:
                    if data[col].dtype == object:
                        sub_filters.append("df = df[df['{col}'].notnull() & (df['{col}']!='')]".format(col=col))
                    else:
                        sub_filters.append("df = df[df['{col}'].notnull()]".format(col=col))
                filtered_data = self.filter(data, sub_filters, payload, "Filter" + str(i))
                self.setValues(payload, source['source'], set_value=filtered_data)
                remaining_data = data[~data.index.isin(filtered_data.index)]
                remainder.append(remaining_data)

            if len(property) == 1:
                self.singlesidematch(payload, result, remainder, property)
                continue

            for i in range(0, len(property) - 1):
                leftsource = property[i]
                leftcolumns = leftsource["columns"]
                leftcolumns['matchColumns'] = [x.strip() for x in leftcolumns['matchColumns']]

                for j in range(i + 1, len(property)):
                    rightsource = property[j]
                    rightdata = self.getValues(payload, rightsource['source'], sep='.')
                    rightcolumns = rightsource["columns"]
                    rightcolumns['matchColumns'] = [x.strip() for x in rightcolumns['matchColumns']]

                    leftdata = self.getValues(payload, leftsource['source'], sep='.')

                    left, right = self.match(leftdata, rightdata,
                                             leftsumColumns=leftcolumns.get('sumColumns', []),
                                             rightsumColumns=rightcolumns.get('sumColumns', []),
                                             leftmatchColumns=leftcolumns.get('matchColumns', []),
                                             rightmatchColumns=rightcolumns.get('matchColumns', []),
                                             leftGroupByFunc=leftcolumns.get('groupByFunc', 'sum'),
                                             rightGroupByFunc=rightcolumns.get('groupByFunc', 'sum')

                                             )

                    if rightsource["sourceTag"] + " Match" in left.columns:
                        del left[rightsource["sourceTag"] + " Match"]

                    if leftsource["sourceTag"] + " Match" in right.columns:
                        del right[leftsource["sourceTag"] + " Match"]

                    left = left.rename(columns={'Matched': rightsource["sourceTag"] + " Match"})
                    right = right.rename(columns={'Matched': leftsource["sourceTag"] + " Match"})

                    self.setValues(payload, ref=property[i]['source'], set_value=left)
                    self.setValues(payload, ref=property[j]['source'], set_value=right)

            for i in range(0, len(property)):
                result_df = self.getValues(payload, property[i]["source"])
                result_df = pandas.concat([result_df, remainder[i]], join_axes=[result_df.columns]).reset_index(
                    drop=True)

                # force all blank status columns to unmatched
                match_cols = [col for col in result_df.columns if ' Match' in col]
                if match_cols:
                    result_df[match_cols] = result_df[match_cols].fillna('UNMATCHED')

                self.setValues(result, property[i]["sourceTag"], result_df)

            # assign will over-write the existing data
            for srcTag, value in result.iteritems():
                self.setValues(payload, 'results' + '.' + srcTag, set_value=value)

        return payload

    def singlesidematch(self, payload, result, remainder, property):
        source = property[0]
        columns = source["columns"]
        data = self.getValues(payload, source['source'], sep='.').reset_index(drop=True)

        if columns.get('debitCreditColumn'):
            cr_dr_indicator = columns.get('debitCreditColumn')[0]
        else:
            raise KeyError('Dr/Cr mandatory for single side match, not defined.')

        data[cr_dr_indicator] = data[cr_dr_indicator].apply(lambda x: re.match(x[0], x, re.I).group().upper())
        credit = data[data[cr_dr_indicator] == source.get('crSign', 'C')]
        debit = data[data[cr_dr_indicator] == source.get('drSign', 'D')]
        left, right = self.match(credit, debit,
                                 leftmatchColumns=columns.get('matchColumns', []) + columns.get(
                                     'sumColumns', []),
                                 rightmatchColumns=columns.get('matchColumns', []) + columns.get(
                                     'sumColumns', []))
        selfmatched = pandas.concat([left, right])

        selfmatched = selfmatched.rename(columns={'Matched': "Self Matched"})
        selfmatched.loc[selfmatched["Self Matched"] == 'UNMATCHED', "Self Matched"] = ''

        payload[property[0]["source"]] = selfmatched
        result[property[0]["sourceTag"]] = payload[property[0]["source"]].append(
            remainder[0])
        data = payload[property[0]["source"]].append(remainder[0])
        self.setValues(payload, 'results.' + property[0]['source'], data)

    def match(self, left=None, right=None, leftsumColumns=[], rightsumColumns=[], leftmatchColumns=[],
              rightmatchColumns=[], leftGroupByFunc='', rightGroupByFunc=''):

        # No data found in either of frame return others as un-matched
        if left.empty or right.empty:
            left['Matched'], right['Matched'] = 'UNMATCHED', 'UNMATCHED'
            return left, right
        leftcols = []
        rightcols = []
        rightsumColumns = [x.strip() for x in rightsumColumns]
        leftsumColumns = [x.strip() for x in leftsumColumns]
        rightmatchColumns = [x.strip() for x in rightmatchColumns]
        leftmatchColumns = [x.strip() for x in leftmatchColumns]
        for col in leftmatchColumns:
            if col not in leftsumColumns:
                leftcols.append(col)

        left_columns = leftcols + leftsumColumns
        for col in rightmatchColumns:
            if col not in rightsumColumns:
                rightcols.append(col)

        right_columns = rightcols + rightsumColumns
        right.drop(columns=['LINK_ID'], errors='ignore', inplace=True)
        left.drop(columns=['LINK_ID'], errors='ignore', inplace=True)

        if len(leftsumColumns) > 0:
            leftworking = eval('left[left_columns].groupby(leftcols).' + leftGroupByFunc + '().reset_index()')
        else:
            leftworking = left[left_columns]

        if len(rightsumColumns) > 0:
            rightworking = eval('right[right_columns].groupby(rightcols).' + rightGroupByFunc + '().reset_index()')
        else:
            rightworking = right[right_columns]

        # Incase of carry forward disable do not add carry forward indicator columns
        if 'CARRY_FORWARD' in left.columns:
            leftworking['left_carryforward'] = left['CARRY_FORWARD']
            left_cf_column = ['left_carryforward']
        else:
            left_cf_column = []

        if 'CARRY_FORWARD' in right.columns:
            rightworking['right_carryforward'] = right['CARRY_FORWARD']
            right_cf_column = ['right_carryforward']
        else:
            right_cf_column = []
        finalworking = leftworking.merge(rightworking, how="outer", left_on=left_columns,
                                         right_on=right_columns, indicator=True, suffixes=('', '_y'))

        for col in finalworking.columns.values:
            if col.endswith("_y"):
                del finalworking[col]

        matched = finalworking[finalworking["_merge"] == "both"]
        matched['LINK_ID'] = matched[matched.columns[0]].apply(
            lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

        if '_merge' in matched.columns:
            del matched['_merge']

        if len(leftsumColumns) > 0:
            leftmatchColumns = list(set(leftmatchColumns) - set(leftsumColumns))

        if len(rightsumColumns) > 0:
            rightmatchColumns = list(set(rightmatchColumns) - set(rightsumColumns))

        left = left.merge(matched[leftmatchColumns + right_cf_column + ['LINK_ID']], how="left",
                          on=leftmatchColumns,
                          indicator=True)

        right = right.merge(matched[rightmatchColumns + left_cf_column + ['LINK_ID']], how="left",
                            on=rightmatchColumns,
                            indicator=True)

        left['Matched'] = 'UNMATCHED'

        left.loc[left['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del left["_merge"]

        right['Matched'] = 'UNMATCHED'
        right.loc[right['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del right['_merge']

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

        return left, right

    def filter(self, df, expressions, payload={}, resultkey=''):
        if payload:
            payload[resultkey + "@BeforeFilter"] = len(df)
        for expression in expressions:
            try:
                payload[resultkey + "@BeforeFilter " + expression] = len(df)

                # check to avoid "ValueError: cannot set a frame with no defined index and a scalar"
                if isinstance(df, pandas.DataFrame) and df.empty:
                    continue

                exec expression
                payload[resultkey + "@AfterFilter " + expression] = len(df)
            except Exception, e:
                print e
                raise ValueError('Error Evaluting expression %s' % expression)

        return df

    def compute_stats(self, source, data=None, matchType='all'):
        data['matched_status'] = ''
        data['Count'] = 1
        data['Source'] = source
        print source
        print data.head()

        # segregate matched and un-matched transactions
        match_cond = []
        un_match_cond = []
        for col in data.columns:
            if ' Match' in col and 'Self Matched' != col:
                match_cond.append("(data['%s'] == 'MATCHED')" % col)
                un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
            else:
                pass

        # if matched == 'any', if either of the source is matched assume the records to be matched.
        if matchType == 'any':
            data.loc[eval(" | ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" & ".join(un_match_cond)), "matched_status"] = 'UnMatched'
        else:
            data.loc[eval(" & ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" | ".join(un_match_cond)), "matched_status"] = 'UnMatched'

        if 'Self Matched' in data.columns:
            data['Self Matched'] = data['Self Matched'].fillna('')
            data.loc[data['Self Matched'] == 'MATCHED', 'matched_status'] = 'Reversal'
        else:
            pass

        pivot_source = pandas.pivot_table(data, index=['Source'], columns=['matched_status'], values=['Count'],
                                          aggfunc=np.sum).reset_index()

        pivot_source.columns = pivot_source.columns.to_series().str.join('#')

        source_summary = pivot_source.to_dict(orient='records')

        return source_summary

    def dumpDataToFiles(self, data, query):

        outputPath = hashframe.config.uploadDir['download'] + '/' + query['reconName'] + '/'

        if os.path.exists(os.path.dirname(outputPath)):
            shutil.rmtree(outputPath)
        # else:
        #     if os.listdir(outputPath) != []:
        #         shutil.rmtree(outputPath)

        os.makedirs(os.path.dirname(outputPath))
        if data.get('recon_summary', []):
            summaryDf = pandas.DataFrame()
            for eachDict in data['recon_summary']:
                summaryDf = summaryDf.append(eachDict, ignore_index=True)
            summaryDf = summaryDf[['Source', 'Matched Count', 'UnMatched Count', 'Reversal Count']]
            summaryDf.to_csv(outputPath + "recon_summary.csv", index=False, header=True,
                             date_format='%d-%m-%Y %H:%M:%S', encoding='utf-8')
            # exit()

        for key, data in data.get('results', {}).iteritems():
            matchkey_cols = []
            match_cond = []
            un_match_cond = []

            # replace all non-ascii characters with spaces
            data.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True, inplace=True)

            for col in data.columns:
                if col.endswith(' Match') and 'Self Matched' != col:
                    matchkey_cols.append(col)
                    match_cond.append("(data['%s'] == 'MATCHED')" % col)
                    un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
                else:
                    pass

            if 'Self Matched' in data.columns:
                reversal_data = pandas.DataFrame()
                data['Self Matched'] = data['Self Matched'].fillna('')
                reversal_data = data[data['Self Matched'] == 'MATCHED']
                data = data[data['Self Matched'] != 'MATCHED']

                if not reversal_data.empty:
                    reversal_data.to_csv(outputPath + "%s_Self_Matched.csv" % key, index=False,
                                         header=True, date_format='%d-%m-%Y %H:%M:%S',
                                         encoding='utf-8')

            if query.get('matched', '') == 'any':
                matched_df = data[eval(" | ".join(match_cond))]
                un_matched_df = data[eval(" & ".join(un_match_cond))]
            else:
                matched_df = data[eval(" & ".join(match_cond))]
                un_matched_df = data[eval(" | ".join(un_match_cond))]

            matched_df.to_csv(outputPath + "%s_Matched.csv" % key, index=False, header=True,
                              date_format='%d-%m-%Y %H:%M:%S', encoding='utf-8')
            un_matched_df.to_csv(outputPath + "%s_UnMatched.csv" % key, index=False, header=True,
                                 date_format='%d-%m-%Y %H:%M:%S', encoding='utf-8')

    def downloadReports(self, id, query):
        outPutPath = hashframe.config.uploadDir['download'] + '/' + query['reconName']
        if os.path.exists(outPutPath):
            zipf = zipfile.ZipFile(outPutPath + '.zip', 'w', zipfile.ZIP_DEFLATED)
            files = os.listdir(outPutPath)
            for file in files:
                zipf.write(outPutPath + os.sep + file,
                           os.path.basename(outPutPath + os.sep + file))

        return True, query['reconName'] + '.zip'

    def getSelectedColumns(self, id, query):
        status, flows = MatchRuleReference().get({'reconName': query['reconName']})
        nodeIds = []
        allData = {'previous': []}
        status1, doc = MatchRuleSumColumns().get({'reconName': query['reconName']})
        sourceSelectedColumns = {}
        for rule in flows['Rules']:
            sourceSelectedColumns[rule['title']] = {}
            for conn in rule['connections']:
                nodeIds.append(conn['source']['nodeID'])
                nodeIds.append(conn['dest']['nodeID'])
            for node in rule['nodes']:
                if node['id'] in nodeIds:
                    if node['className'] not in sourceSelectedColumns[rule['title']]:
                        sourceSelectedColumns[rule['title']][node['className']] = {'columns': [],
                                                                                   'source': node['className']}
                    if node['name'] not in sourceSelectedColumns[rule['title']][node['className']]['columns']:
                        sourceSelectedColumns[rule['title']][node['className']]['columns'].append(node['name'])

                    if status1:
                        sourceSelectedColumns[rule['title']][node['className']]['sumColumns'] = doc['sumColumns'].get(
                            rule['title'], {}).get(
                            node['className'], {}).get('sumColumns', [])

                        sourceSelectedColumns[rule['title']][node['className']]['debitColumns'] = doc['sumColumns'].get(
                            rule['title'], {}).get(
                            node['className'], {}).get('debitColumns', [])

                        sourceSelectedColumns[rule['title']][node['className']]['subsetFilter'] = doc['sumColumns'].get(
                            rule['title'], {}).get(
                            node['className'], {}).get('subsetFilter', [])

                        sourceSelectedColumns[rule['title']][node['className']]['fuzzyColumns'] = doc['sumColumns'].get(
                            rule['title'], {}).get(
                            node['className'], {}).get('fuzzyColumns', [])

            sourceSelectedColumns[rule['title']]['souces'] = sourceSelectedColumns[rule['title']].keys()

        return True, sourceSelectedColumns


# create A flow of a recon
class Flows(hashframe.MongoCollection):
    def __init__(self):
        super(Flows, self).__init__("flows")

    def create(self, doc):
        (status, flows) = super(Flows, self).create(doc)
        return status, flows

    def generateFlow(self, id, query):
        reconName = query['reconName']
        reconProcess = query['reconProcess']

        with open('/usr/share/nginx/www/ngerecon/flask/nodestemplate.json') as fp:
            self.obj = json.load(fp)

        # check if default nodes exist write
        doc = Flows().find_one({"reconName": reconName, "reconProcess": reconProcess})
        if doc:
            Flows().remove({"reconName": reconName, "reconProcess": reconProcess})

        flow = self.getDefaultNodes(query)

        rules = self.getRules(query)

        for rule, sources in rules['Rules'].iteritems():
            flow = self.getNodeForNway(flow, sources)

        flow = self.getPostNodes(flow)

        Flows().save(flow)
        return True, 'Flow generated Successfully'

    def getDefaultNodes(self, query):
        flow = self.generateConnection(query)
        self.obj['Initializer']['properties']['reconName'] = query['reconName']
        self.obj['Initializer']['properties']['reconProcess'] = query['reconProcess']
        flow['nodes'] = []
        flow['nodes'].append(self.obj['Initializer'])
        flow = self.generateConnection(flow)
        flow['nodes'].append(self.obj['PreLoader'])
        return flow

    def getNodeForNway(self, flow, sources):
        with open('/usr/share/nginx/www/ngerecon/flask/nodestemplate.json') as fp:
            obj = json.load(fp)
        flow = self.generateConnection(flow)
        nwayList = []
        nway = False
        x, y = 0, 0
        for node in flow['nodes']:
            if node['name'].strip() == 'NWayMatch':
                nwayList.append(node)
                nway = True
        if nway:
            node = nwayList[-1]
            obj['NWayMatch']['x'] = node['x'] + 50
            obj['NWayMatch']['y'] = node['y'] + 50
        print(sources)
        obj['NWayMatch']['properties']['sources'] = sources
        flow['nodes'].append(obj['NWayMatch'])
        return flow

    def generateConnection(self, flow):
        connection = {"dest": {"nodeID": "", "connectorIndex": 0},
                      "source": {"nodeID": "", "connectorIndex": 0}}
        if not 'connections' in flow:
            flow['connections'] = []
            connection['source']['nodeID'] = 1000
            connection['dest']['nodeID'] = connection['source']['nodeID'] + 1
        else:
            connectionId = flow['connections'][len(flow['connections']) - 1]['dest']['nodeID']
            connection['source']['nodeID'] = connectionId
            connection['dest']['nodeID'] = connection['source']['nodeID'] + 1

        flow['connections'].append(connection)
        return flow

    def getPostNodes(self, flow):
        flow['nodes'].append(self.obj['DumpData'])
        flow = self.assignIdToNodes(flow)
        return flow

    def assignIdToNodes(self, flow):
        idList = []
        nodeList = []
        for connection in flow['connections']:
            idList.extend([connection['source']['nodeID'], connection['dest']['nodeID']])
        idList = list(set(idList))

        for id, node in zip(idList, flow['nodes']):
            node['id'] = str(id)
            nodeList.append(node)
        flow['nodes'] = nodeList
        return flow

    def getRules(self, query):
        doc = MatchRuleReference().find_one({'reconName': query['reconName'], 'reconProcess': query['reconProcess']})
        if doc and 'Rules' in doc.keys():
            columns = {}

            for rule in doc['Rules']:
                connections = rule['connections']
                nodes = rule['nodes']
                connections = MatchRuleReference().getAllSourcePoints(connections)
                connectionList = []
                columns[rule['title']] = {}
                for connection in connections:
                    if connection['source'].get('isBegining', False):
                        connectionList.append(connection['source']['nodeID'])
                        connectionList.append(connection['dest']['nodeID'])
                        connectionList = MatchRuleReference().traverse(connections, connectionList,
                                                                       connection['dest']['nodeID'],
                                                                       dest=True)
                        for node in nodes:
                            for i in connectionList:
                                if node['id'] == i:
                                    if node['className'] not in columns[rule['title']]:
                                        columns[rule['title']][node['className']] = {'matchColumns': [],
                                                                                     'sumColumns': [],
                                                                                     'fuzzyColumns':[],
                                                                                     'debitCreditColumn': []}
                                    columns[rule['title']][node['className']]['matchColumns'].append(node['name'])
                                    for key in ['sumColumns', 'debitCreditColumn','subsetFilter','fuzzyColumns']:
                                        print(MatchRuleReference().getSumColumns(
                                            rule['title'], node['className'], query, key))
                                        columns[rule['title']][node['className']][
                                            key] = MatchRuleReference().getSumColumns(
                                            rule['title'], node['className'], query, key)

                                    # Removing sumColumns from Match Columns
                                    for col in columns[rule['title']][node['className']]['sumColumns']:
                                        if col in columns[rule['title']][node['className']]['matchColumns']:
                                            columns[rule['title']][node['className']]['matchColumns'].remove(col)

                                    for col in columns[rule['title']][node['className']]['fuzzyColumns']:
                                        if col in columns[rule['title']][node['className']]['matchColumns']:
                                            columns[rule['title']][node['className']]['matchColumns'].remove(col)
                        connectionList = []

            columns['Rules'] = MatchRuleReference().formatColumns(columns)
            return columns


#  Force Match Operations
class ForceMatch(hashframe.MongoCollection):
    def __init__(self):
        super(ForceMatch, self).__init__("forceMatch_default_config")

    def create(self, doc):
        (status, forceMatch_default_config) = super(ForceMatch, self).create(doc)
        return status, forceMatch_default_config

    #  Pimary source Data while force Match
    def readReportData(self, id, query):
        data = {}
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if execLog:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            if query['source']:
                columns = []
                columns = [str(i['column']) for i in query['dtypes']]
                converter = {i['column']: eval(i['dtype']) for i in query['dtypes']}
                converter['System_Idx'] = str
                file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                    'source'] + '_UnMatched.csv'

                df = pandas.read_csv(file_name, sep=',', usecols=columns, converters=converter).fillna('')
                data['primaryCol'] = df.columns.tolist()
                data['primaryData'] = df.to_dict(orient='records')
                data['total'] = len(df)
        return True, data

    def readSourceData(self, id, query):
        logr.info("Reading All Source Data For Force Match")
        allSourceData = pandas.DataFrame()
        matchingColumns = set()
        orderOfCols = []
        reconDetails = Recons().find_one({'reconName': query['reconName']})

        try:
            status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
            if execLog:
                logr.info("Execution Details Foucd For the statement Data:%s" % execLog['statementDate'])
                stmtDate = execLog['statementDate'].strftime("%d%m%Y")
                for source in query['sources']:
                    for struct, keyCols in reconDetails['sources'][source].items():
                        cols = [col['keyCols'] for col in keyCols['keyColumns']]
                        matchingColumns.update(cols)
                    status, converters = ReconDetails().getConverters(query['reconName'], source)
                    converters['System_Idx'] = str
                    logr.info(converters)
                    if query['operation'] == 'rollBack':
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_ForceMatched.csv'
                    elif query['operation'] == 'forceMatch':
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_UnMatched.csv'

                    if not os.path.exists(file_name):
                        logr.info("%s file Not Found" % file_name)
                        return False, 'File Not Found'
                    df = pandas.read_csv(file_name, converters={'System_Idx': str}, sep=',', index_col=None).fillna('')
                    allSourceData = pandas.concat([allSourceData, df], ignore_index=True)

                # Getting Key column first in dataframe to view in UI
                allSourceData.fillna('', inplace=True)
                columns = allSourceData.columns.tolist()
                commonCols = list(set(columns) & (matchingColumns))
                nonCommonCols = list(set(columns) - (matchingColumns))
                commonCols.extend(nonCommonCols)
                allSourceData = allSourceData[commonCols]

                data = {}
                data['columns'] = allSourceData.columns.tolist()
                data['allSourceData'] = allSourceData.to_dict(orient='records')
                data['total'] = len(allSourceData)
                logr.info("Reading Data is Done")
                return True, data
            else:
                return False, "No Execution Details Present"
        except Exception as e:
            logr.info(traceback.format_exc())
            return False, "Internal Error"

    #  Matched Data of secondary source while force Match
    def readMatchData(self, id, query):
        data = {}
        if 'matchSource' in query.keys():
            matchSource = query['matchSource']
        primaryColList = query['primaryMatchColumns']
        matchColList = [i['column'] for i in query['secondaryMatchColumns']]
        converter = {i['column']: eval(i['dtype']) for i in query['secondaryMatchColumns']}
        converter['System_Idx'] = str
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if execLog:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            primaryDf = pandas.DataFrame(query['selectedRows'])

            if matchSource:
                file_name = hashframe.config.mftPath + query[
                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + matchSource + '_UnMatched.csv'

            matchdf = pandas.read_csv(file_name, usecols=matchColList, converters=converter).fillna('')

            for col in matchdf.columns:
                matchdf[col] = matchdf[col].fillna(ReconDetails().infer_dtypes(matchdf[col]))

            matched = pandas.DataFrame()
            for col in primaryColList:
                # matched = matchdf.loc[:, matchColList].isin(primaryDf[col].tolist())
                for col1 in matchColList:
                    for i in primaryDf[col].tolist():
                        if matchdf[col1].dtype == str:
                            matched.append(matchdf[matchdf[col1].apply(lambda x: x.str.contains(i))])

                # matchdf = matchdf[matched.values == True]

            data['total'] = len(matchdf)
            data['secondary'] = matchSource
            data['matchCols'] = matchdf.columns.tolist()
            data['matchSourceData'] = matchdf.to_dict(orient='records')
            return True, data

    # Function To Get Recon_details
    def getForceMatchReconDetails(self, reconName):
        details = Recons().find_one({'reconName': reconName})
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(reconName)
        if status:
            return True, details, execLog
        else:
            return False, details, 'No Data Found'

    # Function To Re Compute Recon Summary After Doing Force Match
    def recomputeReconSummary(self, reconName, execLog, forceMatchSources, authorization=False, sendToAuth=False,
                              rollBack=False):
        try:
            logr.info('===============Recompute Recon Summary is started===============')
            status, reconDetails, execLog = self.getForceMatchReconDetails(reconName)
            details = ReconDetails().find_one({'reconName': reconName})
            if status:
                stmtDate = execLog['statementDate'].strftime("%d%m%Y")
                logr.info('Statement Date in RecomputeSummary' + stmtDate)
                summary = ReconSummary().find_one({'statementDate': execLog['statementDate'], 'reconName': reconName,
                                                   'reconExecutionId': execLog['reconExecutionId']})
                logr.info('Summary Found For' + summary['reconName'])
                logr.info('source participated in ForceMatch ' + ",".join(forceMatchSources))
                for report in details['reportDetails']:
                    if report['reportName'] == 'Unmatched' and report['source'] in forceMatchSources:
                        logr.info('Looping Source For Recomput of summary ' + report['source'])
                        for struct, cols in reconDetails['sources'][report['source']].iteritems():
                            aggrCols = [col['keyCols'] for col in cols['keyColumns'] if col['DataType'] == 'float']
                            logr.info('Looping source aggregation Columns ' + ",".join(aggrCols))
                            converter = {col: eval('np.float64') for col in aggrCols}
                            authName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + report[
                                'source'] + '_Authorization.csv'
                            forceName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                        report[
                                            'source'] + '_ForceMatched.csv'

                            unMatchName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                          report[
                                              'source'] + '_UnMatched.csv'

                            logr.info('Authorization Path for source ' + authName)
                            logr.info('ForceMatchFile Path for source ' + forceName)
                            authLength = 0
                            forceLength = 0
                            if os.path.exists(authName):
                                logr.info('Authorization File exist')
                                auth = pandas.read_csv(authName, converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                authLength = len(auth)
                                cfd = 0
                            else:
                                auth = pandas.DataFrame()

                            if os.path.exists(forceName):
                                logr.info('Authorization is on progress and ForceMatch File Exits')
                                force = pandas.read_csv(forceName, converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                forceLength = len(force)

                            unMatchLen = 0
                            if os.path.exists(unMatchName):
                                unMatch = pandas.read_csv(unMatchName,
                                                          converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                unMatchLen = len(unMatch)

                            for det in summary['details']:
                                if det['Source'] == report['source']:
                                    det['Authorization_Count'] = authLength
                                    # det['Authorization_Amount'] = auth[aggrCols].sum()[0] if authLength > 0 else 0
                                    # logr.info('Authorization Records Count '+authLength)
                                    if sendToAuth and not rollBack:
                                        if 'CARRY_FORWARD' in auth.columns:
                                            logr.info(auth['CARRY_FORWARD'])
                                            cfd = len(auth[auth['CARRY_FORWARD'] == 'Y'])
                                            det['Carry-Forward UnMatched Count'] = len(
                                                unMatch[unMatch['CARRY_FORWARD'] == 'Y'])
                                            # det['Carry-Forward UnMatched Amount'] = det[
                                            #                                             'Carry-Forward UnMatched Amount'] - (
                                            #                                             auth[auth[
                                            #                                                      'CARRY_FORWARD'] == 'Y'][
                                            #                                                 aggrCols].sum()[0])

                                        det['UnMatched Count'] = unMatchLen
                                        # det['UnMatched Amount'] = det['UnMatched Amount'] - (auth[aggrCols].sum()[0])

                                    det['Force_Matched_count'] = forceLength
                                    # det['Force_Matched_Amount'] = force[aggrCols].sum()[0] if forceLength > 0 else 0
                                    logr.info('Force Match Records Count ' + str(forceLength))

                                    if rollBack:
                                        if 'CARRY_FORWARD' in auth.columns:
                                            cfd = len(unMatch[unMatch['CARRY_FORWARD'] == 'Y'])
                                            det['Carry-Forward UnMatched Count'] = cfd
                                            # det['Carry-Forward UnMatched Amount'] = \
                                            #     unMatch[unMatch['CARRY_FORWARD'] == 'Y'][aggrCols].sum()[
                                            #         0] if unMatchLen > 0 else det['Carry-Forward UnMatched Amount']
                                        #
                                        det['UnMatched Count'] = unMatchLen
                                        # det['UnMatched Amount'] = (unMatch[aggrCols].sum()[0]) if unMatchLen > 0 else \
                                        #     det['UnMatched Amount']

                from bson import ObjectId
                a = ReconSummary().update({'_id': ObjectId(summary['_id'])}, {'$set': {'details': summary['details']}})
                logr.info('===============Reconmpute Recon summary SuccessFull===============')
                return True, ''
            else:
                logr.info('No execution Found for recon')
                return status, execLog
        except Exception as e:
            return False, ''
            logr.info('===============Recompute Recon Summary Failed with error===============')
            logr.info(traceback.format_exc())

    #  ForceMatch of selected Transaction
    def forceMatchReconData(self, id, query):
        logr.info("================" + query['operation'] + " Record Started==============")
        data = {}
        orginalSourceData = {}
        orginalAuthData = {}
        totalDF = pandas.DataFrame(query['forceMatchData'])
        sources = totalDF['SOURCE'].unique().tolist()

        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        link_id = str(uuid.uuid4().int & (1 << 64) - 1)
        for source in sources:
            sourceDF = totalDF.loc[totalDF['SOURCE'] == source]
            if not sourceDF.empty:
                try:
                    if query['operation'] == 'rollBack':
                        rollBack = True
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_ForceMatched.csv'

                    elif query['operation'] == 'forceMatch':
                        rollBack = False
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_UnMatched.csv'

                    dtypes = ColumnDataTypes().find_one({'reconName': query['reconName'], 'source': source})
                    columnsDtypes = {
                        col['column']: eval(col['dtype']) if col['dtype'] != 'np.datetime64' else col[
                                                                                                      'dtype'] != 'np.datetime64'
                        for col in dtypes['dtypes']}
                    columnsDtypes['System_Idx'] = str

                    for col, datatype in columnsDtypes.iteritems():
                        if 'CarryForward' in col or col == 'LINK_ID':
                            columnsDtypes[col] = str

                    # logr.info(columnsDtypes)
                    logr.info(file_name)

                    sourceAllData = pandas.read_csv(file_name, converters=columnsDtypes)

                    # Preserve data if any exception comes
                    orginalSourceData[source] = {'data': sourceAllData, 'fileName': file_name}

                    primarDFSysInd = sourceDF['System_Idx'].tolist()

                    sourceDF['System_Idx'] = sourceDF['System_Idx'].astype(str)
                    logr.info(sourceDF['System_Idx'])
                    concatmatchdf = pandas.concat([sourceAllData, sourceDF], join_axes=[sourceAllData.columns])
                    for col in concatmatchdf.columns:
                        concatmatchdf[col] = concatmatchdf[col].fillna(ReconDetails().infer_dtypes(concatmatchdf[col]))
                    nonduplicated = concatmatchdf[~concatmatchdf.duplicated(subset=['System_Idx'], keep=False)]
                    nonduplicated.to_csv(file_name, index=False)

                    Authorization = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                    source + '_Authorization.csv'

                    logr.info('%s Authorization File Path %s' % (source, Authorization))

                    authorizedRecords = sourceAllData.loc[sourceAllData['System_Idx'].isin(primarDFSysInd)]
                    logr.info("Number of Records Found in %s :%s " % (source, str(len(authorizedRecords))))

                    authorizedRecords['ForceMatchRemarks'] = query['comments']

                    if query['operation'] == 'forceMatch':
                        authorizedRecords['AUTHORIZATION_STATUS'] = 'FORCE MATCH AUTHOURIZATION'
                    elif query['operation'] == 'rollBack':
                        authorizedRecords['AUTHORIZATION_STATUS'] = 'ROLL BACK AUTHOURIZATION'

                    authorizedRecords['LINK_ID'] = link_id
                    authorizedRecords['UserID'] = flask.session["sessionData"]["userName"]
                    if os.path.exists(Authorization):
                        authorizationDf = pandas.read_csv(Authorization)
                        orginalAuthData[source] = {'data': authorizationDf, 'fileName': Authorization}
                        logr.info('Number of Records Exits in authorization ' + str(len(authorizationDf)))
                        authorizedRecords = pandas.concat([authorizationDf, authorizedRecords],
                                                          join_axes=[authorizationDf.columns])
                    authorizedRecords.to_csv(
                        hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_Authorization.csv',
                        index=False)
                except Exception as e:
                    for key, val in orginalSourceData.iteritems():
                        val['data'].to_csv(val['fileName'], index=False)

                    for key, val in orginalAuthData.iteritems():
                        val['data'].to_csv(val['fileName'], index=False)

                    logr.info("Exception Occured During ForceMatch")
                    logr.info(traceback.format_exc())
                    return False, 'Exception Occured'

            doc = {}
            doc['reconName'] = query['reconName']
            # doc['reconProcess'] = query['reconProcess']
            doc['user'] = flask.session['sessionData']['userName']
            doc['dateTime'] = int(datetime.datetime.now().strftime("%s")) * 1000
            doc['operation'] = 'ForceMatch Records'
            # forceSummary = {query['primarySource']: len(primaryDF), query['matchSource']: len(matchDF)}
        status, msg = self.recomputeReconSummary(query['reconName'], execLog,
                                                 sources,
                                                 sendToAuth=True, rollBack=rollBack)
        if not status:
            for key, val in orginalSourceData.iteritems():
                val['data'].to_csv(val['fileName'], index=False)

            for key, val in orginalAuthData.iteritems():
                val['data'].to_csv(val['fileName'], index=False)

        # ForceMatch().insert(doc)
        return True, data

    # Getting wiating For Authorization Data
    def getWaitingForAuthorization(self, id, query):
        logr.info('===============Getting waiting for authorization Records===============')
        status, details, execLog = self.getForceMatchReconDetails(query['reconName'])

        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        sources = details['sources'].keys()
        authorizeRecords = pandas.DataFrame()
        for source in sources:
            file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                        source + '_Authorization.csv'
            if os.path.exists(file_name):
                status, converters = ReconDetails().getConverters(query['reconName'], source)
                converters['System_Idx'] = str
                df = pandas.read_csv(file_name, converters={'System_Idx': str})
                authorizeRecords = pandas.concat([authorizeRecords, df], ignore_index=True).fillna('')
        data = {}
        if len(authorizeRecords) <= 0:
            data['total'] = 0
            data['msg'] = 'No Data Found For Authorization'
            return True, data
        data['total'] = len(authorizeRecords)
        data['data'] = authorizeRecords.to_dict(orient='records')
        return True, data

    #  Authorize Records
    def authorizeRecord(self, id, query):
        logr.info('===================Authorization Process Started===================')
        selectedRows = pandas.DataFrame(query['selectedRows'])
        status, details, execLog = self.getForceMatchReconDetails(query['reconName'])
        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        # sources = details['sources'].keys()
        orginalSelectedData = {}
        for authStatus in ['FORCE MATCH AUTHOURIZATION', 'ROLL BACK AUTHOURIZATION']:
            authData = selectedRows[selectedRows['AUTHORIZATION_STATUS'] == authStatus]
            if not authData.empty:
                logr.info('Number of Rows selected for authorization ' + str(len(authData)))
                rowsSystemInx = authData['System_Idx'].tolist()
                sources = authData['SOURCE'].unique().tolist()
                logr.info(rowsSystemInx)
                # logr.info('Row System Index of selected Rows ' + ",".join(rowsSystemInx))
                matched = pandas.DataFrame()
                authorizatinSource = []
                for source in sources:
                    try:
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                    source + '_Authorization.csv'
                        converters = {}
                        if (os.path.exists(file_name)):
                            logr.info('Authourization File exits ' + file_name)
                            # status,converters = ReconDetails().getConverters(query['reconName'], source)
                            converters['System_Idx'] = str
                            df = pandas.read_csv(file_name, converters=converters)

                            if file_name not in orginalSelectedData:
                                orginalSelectedData[source] = {'data': df, 'fileName': file_name}

                            matched = df[df['System_Idx'].isin(rowsSystemInx)]
                            if (len(matched) > 0):
                                logr.info("Number Of Records matched to authorize " + str(len(matched)))
                                matched['AUTHOURIZED_BY'] = flask.session["sessionData"]["userName"]
                                authorizatinSource.append(source)
                            df = df[~df['System_Idx'].isin(rowsSystemInx)]

                            if len(df) > 0:
                                df.to_csv(file_name, index=False)
                            else:
                                logr.info('All authorization pending records authorized')
                                os.remove(file_name)

                            # File Name after authorizing forceMatched records
                            if authStatus == 'FORCE MATCH AUTHOURIZATION' and query['operation'] != 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'FORCE_MATCHED'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_ForceMatched.csv'

                            # File Name after authorizing rollBack records which are force matched
                            elif authStatus == 'ROLL BACK AUTHOURIZATION' and query['operation'] != 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'ROLL BACK'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_UnMatched.csv'

                            # File Name after rejecting force Matched records
                            elif authStatus == 'FORCE MATCH AUTHOURIZATION' and query['operation'] == 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'FORCE_MATCHED_REJECT'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_UnMatched.csv'

                            # File Name after rejecting roll Back records which are forceMatched
                            elif authStatus == 'ROLL BACK AUTHOURIZATION' and query['operation'] == 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'ROLL_BACK_REJECT'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_ForceMatched.csv'

                            if os.path.exists(File_name):
                                logr.info('Match File exits ' + File_name)
                                forced = pandas.read_csv(File_name, index_col=None, converters=converters)
                                if File_name not in orginalSelectedData:
                                    orginalSelectedData[File_name] = {'data': forced}
                                matched = pandas.concat([forced, matched], join_axes=[matched.columns],
                                                        ignore_index=True)
                            matched.to_csv(File_name, index=False)
                    except Exception as e:
                        logr.info(traceback.format_exc())
                        for key, val in orginalSelectedData.iteritems():
                            val['data'].to_csv(key, index=False)

                logr.info('Source which are participate in authorization is ' + ",".join(authorizatinSource))
        status, msg = self.recomputeReconSummary(query['reconName'], execLog, authorizatinSource, authorization=True,
                                                 rollBack=True)
        if not status:
            for key, val in orginalSelectedData.iteritems():
                val['data'].to_csv(key, index=False)

        return True, 'Authorize Done SuccessFully'

    # Roll Back Records
    # def authRollBackRecords(self, id, query):
    #     try:
    #         selectedRows = pandas.DataFrame(query['selectedRows'])
    #         status, details, execLog = self.getForceMatchReconDetails(query['reconName'])
    #         stmtDate = execLog['statementDate'].strftime("%d%m%Y")
    #         sources = details['sources'].keys()
    #         authorizeRecords = pandas.DataFrame()
    #         rowsSystemInx = selectedRows['System_Idx'].tolist()
    #         selectedSources = selectedRows['SOURCE'].unique().tolist()
    #         selectedRows['ROLLBACK_AUTHORIZED_BY'] = flask.session['sessionData']['userName']
    #
    #         for source in sources:
    #
    #             file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
    #                         source + '_UnMatched.csv'
    #
    #             authName = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
    #                        source + '_Authorization.csv'
    #
    #             if (os.path.exists(authName)):
    #                 status, converters = ReconDetails().getConverters(query['reconName'], source)
    #                 converters['System_Idx'] = str
    #                 df = pandas.read_csv(authName, converters=converters)
    #                 df = df[~df['System_Idx'].isin(rowsSystemInx)]
    #                 if len(df) > 0:
    #                     df.to_csv(authName, index=False)
    #                 else:
    #                     os.remove(authName)
    #
    #             if (os.path.exists(file_name)) and source in selectedSources:
    #                 status, converters = ReconDetails().getConverters(query['reconName'], source)
    #                 converters['System_Idx'] = str
    #                 df = pandas.read_csv(file_name, converters=converters)
    #                 df = pandas.concat([df, selectedRows[selectedRows['SOURCE'] == source]],
    #                                    join_axes=[selectedRows.columns])
    #                 df.to_csv(file_name, index=False)
    #
    #         self.recomputeReconSummary(query['reconName'], execLog, sources, rollBack=True)
    #         return True, ''
    #     except Exception as e:
    #         return False, e


#  Recon Job Operations
class Utilities(object):
    def __init__(self):
        self.sourceData = {}

    def upload(self, id):
        data = dict()
        mftPath = os.path.join(hashframe.config.mftPath)
        if not os.path.exists(mftPath):
            os.makedirs(mftPath)

        filesDir = mftPath + "/" + data['date'][0]
        if not os.path.exists(filesDir):
            os.mkdir(filesDir)

        if data['fileName'][0]:
            # request.files['file'].save(os.path.join(filesDir, data['fileName'][0]))
            if not os.path.exists(os.path.join(hashframe.config.mftPath, data['date'][0])):
                os.mkdir(hashframe.config.mftPath + data['date'][0])
            else:
                return False, 'Files already uploaded with this date'

            os.system('mv %s %s' % (
                os.path.join(filesDir, data['fileName'][0]), hashframe.config.mftPath + data['date'][0] + os.sep))
            # request.files['sendfile'].save('/tmp/'+dummy)
            return True, id
        else:
            return False, data

    def upload_recon_files(self, id):
        try:
            params = json.loads(request.form['data'])
            stmt_date = params['statementDate']

            contentDir = os.path.join(hashframe.config.contentDir)
            if not os.path.exists(contentDir):
                os.makedirs(contentDir)

            request.files['file'].save(os.path.join(contentDir, id))
            logr.info("File Uploaded : %s" % os.path.join(contentDir, id))
            logr.info(hashframe.config.mftPath + '/' + params['reconName'])

            shutil.copy(os.path.join(contentDir, id), hashframe.config.mftPath + '/' + params['reconName'])

            statementDate = datetime.datetime.strptime(stmt_date, '%d/%m/%Y')

            # create upstart script & init
            # init_service = '_'.join(params['reconName'].split())
            # fname = "/etc/init/%s" % init_service + '_RJob.conf'
            #
            # if os.path.exists(fname):
            #     return False, "Previous Job in progress, please wait until it completes."
            #
            # with open(fname, "w") as f:
            #     lines = list("script\n")
            #     lines.append("truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
            #                                                                               logFile=params['reconName']))
            #
            #     cmd = "{envPath} {flowRunner} {reconName} {stmtDate} {reportFile} > {dir}/error_logs/'{logFile}_Log.log' \n".format(
            #         envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
            #         stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
            #         reportFile=request.files['file'].filename , dir=hashframe.config.contentDir,
            #         logFile=params['reconName'])
            #     lines.append(cmd)
            #     lines.append("rm -f %s\n" % fname)
            #     lines.append("end script\n")
            #     f.writelines(lines)
            #
            # logr.info('=================================== UP-Start Job ===================================')
            # logr.info('Execution Command')
            # logr.info(cmd)
            # (status, out, err) = self.execution('initctl start "%s_RJob"' % init_service)
            # logr.info("====================================================================================")
            # os.remove(contentDir + '/' + id)
            #
            # return True, "Recon Job Initiated, please wait."

            # create upstart script & init
            init_service = '_'.join(params['reconName'].split())
            fname = "/etc/systemd/system/%s" % init_service + '_RJob.service'
            if os.path.exists(fname):
                return False, "Previous Job in progress, please wait until it completes."

            with open(fname, "w") as f:
                lines = list()
                lines.append('[Unit]\n')
                lines.append("Description = Recon Job\n")
                lines.append("After = network.target \n")
                lines.append("[Service] \n")
                lines.append("User = root \n")
                pre = "truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
                                                                                   logFile=params['reconName'])

                cmd = "{envPath} {flowRunner} {reconName} {stmtDate} {reportFile} > {dir}/error_logs/'{logFile}_Log.log' \n".format(
                    envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
                    stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
                    reportFile=request.files['file'].filename, dir=hashframe.config.contentDir,
                    logFile=params['reconName'])
                lines.append("ExecStartPre=%s" % pre)
                lines.append("ExecStart=%s" % cmd)
                lines.append("ExecStop=rm -f %s\n" % fname)
                lines.append("end script\n")
                f.writelines(lines)

            logr.info('=================================== UP-Start Job ===================================')
            logr.info('Execution Command')
            logr.info(cmd)

            os.system('systemctl daemon-reload')
            os.system('systemctl start "%s_RJob.service"' % init_service)
            logr.info("====================================================================================")
            os.remove(contentDir + '/' + id)

            return True, "Recon Job Initiated, please wait."
        except Exception as e:
            print traceback.format_exc()
            return False, 'Internal Error, Contact Admin.'

    def execution(self, command):
        childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        close_fds=True)
        out, err = childProcess.communicate()
        status = childProcess.returncode
        return status

    def match(self, left=None, right=None, leftsumColumns=[], rightsumColumns=[], leftmatchColumns=[],
              rightmatchColumns=[]):

        try:
            print("Matching...")

            if left.empty or right.empty:
                left['Matched'], right['Matched'] = 'UNMATCHED', 'UNMATCHED'

            left_columns = leftmatchColumns + leftsumColumns
            right_columns = rightmatchColumns + rightsumColumns

            right.drop(columns=['LINK_ID'], errors='ignore', inplace=True)
            left.drop(columns=['LINK_ID'], errors='ignore', inplace=True)

            if len(leftsumColumns) > 0:
                leftworking = left[left_columns].groupby(leftmatchColumns).sum().reset_index()
            else:
                leftworking = left[left_columns]

            if len(rightsumColumns) > 0:
                rightworking = right[right_columns].groupby(rightmatchColumns).sum().reset_index()
            else:
                rightworking = right[right_columns]

            finalworking = leftworking.merge(rightworking, how="outer", left_on=left_columns,
                                             right_on=right_columns, indicator=True, suffixes=('', '_y'))

            for col in finalworking.columns.values:
                if col.endswith("_y"):
                    del finalworking[col]

            matched = finalworking[finalworking["_merge"] == "both"]
            matched['LINK_ID'] = matched[matched.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

            if '_merge' in matched.columns:
                del matched['_merge']

            if len(leftsumColumns) > 0:
                leftmatchColumns = list(set(leftmatchColumns) - set(leftsumColumns))

            if len(rightsumColumns) > 0:
                rightmatchColumns = list(set(rightmatchColumns) - set(rightsumColumns))

            left = left.merge(matched[leftmatchColumns], how="left", on=leftmatchColumns,
                              indicator=True)

            right = right.merge(matched[rightmatchColumns], how="left", on=rightmatchColumns,
                                indicator=True)

            left['Matched'] = 'UNMATCHED'

            left.loc[left['_merge'] == 'both', 'Matched'] = 'MATCHED'
            del left["_merge"]

            right['Matched'] = 'UNMATCHED'
            right.loc[right['_merge'] == 'both', 'Matched'] = 'MATCHED'
            del right['_merge']

            for col in left.columns.values:
                if col.endswith("_y") or col.endswith("_x"):
                    del left[col]

            for col in right.columns.values:
                if col.endswith("_y") or col.endswith("_x"):
                    del right[col]
            return left, right
        except Exception as e:
            print e
