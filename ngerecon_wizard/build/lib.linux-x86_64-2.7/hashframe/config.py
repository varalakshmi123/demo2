import os
import sys
import json

class Config(object):
    def __init__(self):
        #Default config values
        appConfig = {
            'flaskBaseUrl': '/api',
            'secretKey': '58e57c3c-76cb-4c5c-8416-0f3226bc5e88',
            'sessionSalt' : 'f80423b7-33f2-4b57-ae97-44ef3f8a0c32',
            'cookieName' : 'hashframe',
            'cookieDomain':'',
            'sessionTimeoutSecs' : 86400,
            'logBaseDir': '/tmp/log/hashframe/',
            'logMaxBytes': 50000000,
            'logBackupCount': 100,
            'allowedDomains': [],
            'allowHeaders': 'x-hash-credentials, content-type'
        }

        if not os.environ.get('HASH_MODE'):
            print "Specify environment HASH_MODE to be one of 'dev' or 'test' or 'prod'"
            sys.exit(1)

        modeConfig = os.environ.get('HASH_MODE','') + '_config.json'
        if not os.path.exists(modeConfig):
            #As logger needs config, using print...
            print 'Config file %s not found, exiting...' % (modeConfig)
            sys.exit(1)
        else:
            print 'Loading Config file: %s.' % (modeConfig)
            #Load the application config, overwriting the defaults
            appConfig.update(json.loads(open(modeConfig).read()))
        for k, v in appConfig.items():
            object.__setattr__(self, k, v)

config = Config()
