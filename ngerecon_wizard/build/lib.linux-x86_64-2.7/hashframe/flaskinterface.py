
#Try to import gevent if available, this shouldbe the first activity!!!
try:
    import gevent.monkey
    gevent.monkey.patch_all()
    import gevent.pywsgi
    print 'Gevent enabled'
except:
    pass

import flask
import flask.views
import sessionhandler
import functools
import json
import inspect
from config import config
import socket
import noauthhandler
import traceback
import pprint
import utilities
import sys
import argparse
import glob
import os
import logger
logr = logger.Logger.getInstance("framework").getLogger()

appPath = os.getcwd()
def makeResponse(status, msg, headers={}, statusCode=200):
    if status == 'dataOnly':
        content = msg
    else:
        content = {"status": "ok" if status else "error", "msg": msg if msg else ""}

    standardHeaders = {
        'Content-Type': 'application/json', 
        'Cache-Control': 'private, max-age=0, no-cache, no-store', 
    }
    #If request is not for the same domain & the Origin is allowed, add the CORS headers
    if flask.request.headers.get('host') not in flask.request.headers.get('origin','') and flask.request.headers.get('origin') in config.allowedDomains:
        standardHeaders['Access-Control-Allow-Origin'] = flask.request.headers.get('origin') 
        standardHeaders['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
        standardHeaders['Access-Control-Allow-Credentials'] = 'true'
        #TODO: Make these list of headers configurable...
        standardHeaders['Access-Control-Allow-Headers'] = config.allowHeaders
    headers.update(standardHeaders)
    #TODO: Support response as XML?? based on query param type=json/xml/csv/excel with default: json
    #headers["Content-Disposition"] = "attachment; filename=export.csv"
    #headers["Content-Type"] = "text/csv"
    
    if not isinstance(content, basestring):
        content = json.dumps(content)
    return flask.Response(content, statusCode, headers.items())

def getInstance(collectionName):
    # If the application code, for some reason uses chdir, this will reset it!!!
    # This is insane fix, as the application code might have side effect ;)
    if os.getcwd() != appPath:
        os.chdir(appPath)
    modules = map(__import__, [f[:-3] for f in glob.glob('*.py') if os.path.isfile(f)])
    for module in modules:
        if hasattr(module, collectionName):
            klass = getattr(module, collectionName)
            if inspect.isclass(klass):
                instance = klass()
                return instance
    return None

def has_permission(action):
    #If the class implements "noauthhandler" then all functions inside that class will be open to public
    #There is no authentication or authorization required to make api calls to them. 
    if isinstance(flask.g.handler, noauthhandler.noAuthHandler):
        return True

    #If the action is not implemented in the class then return error
    if not hasattr(flask.g.handler, action):
        return False
    #TODO: 
    # Based on the OAuth2 scope based authorization model allow or deny access to the functions
    return True

def requestValidator(f):
    @functools.wraps(f)
    def setup_env(*args, **kwargs):
        #Convert the collection name from Snake case to Title case to match the class names.
        #We do this because HTTP is case insensitive, whereas python is sensitive
        #Example:
        #    URL           Python ClassName
        # /classname  -->     Classname
        # /class_name -->     ClassName
        # /a_w_s      -->     AWS 
        collectionName = kwargs["collection"].title().replace("_", "")

        #Get an instance of the python class that was requested in the url.
        #If the clas is not found, return an error back.
        flask.g.handler = getInstance(collectionName)
        if not flask.g.handler:
            return makeResponse(False, "Unknown Collection: %s" % (collectionName), statusCode=404)

        #If there is no valid session and the class requires authentication, return 401. 
        if (not isinstance(flask.g.handler, noauthhandler.noAuthHandler)) and (not flask.request.method == 'OPTIONS'):
            if not "sessionData" in flask.session:
                #TODO: 
                # Before returning a blind 401, check if the request has "Authorization" header and handle it.
                # For Dyno products, instead of calling /api/login getting the "Set-Cookie" header and sending that in "Cookie" header 
                # of each api request... this can be useful and less painful for API calls and integration by 3rd parties. 
                # Once we decide to go 100% with SSO we can use the "Bearer" token which can make things easier..
                return "dataOnly", "", {}, 401

        #If the url has "id" and the class has "exists" function then check if that "id" is
        #valid by clling the "exists", before calling the actual function.
        id = kwargs.get("id") 
        if id and id != "dummy":
            if hasattr(flask.g.handler, "exists"):
                if not flask.g.handler.exists(id):
                    return makeResponse(False, "Collection %s does not have %s" % (collectionName, kwargs["id"]), statusCode=404)

            #If the class has "hasAccess" method, call it to make sure that the request is allowed to 
            #call the function 
            if hasattr(flask.g.handler, "hasAccess") and not flask.g.handler.hasAccess(id):
                return makeResponse(False, "UnAuthorized for Collection %s / %s" % (collectionName, id), statusCode=403)
        #Make actual call to the function in "RequestHandler", flask MethodView class, based on the HTTP method get, put, post, delete, options...
        return f(*args, **kwargs)

    return setup_env


class RequestHandler(flask.views.MethodView):
    decorators = [requestValidator]

    def head(self, collection):
        if hasattr(flask.g.handler, "count"):
            return makeResponse(True, "", headers=[("X-Items-Count", flask.g.handler.count())])
        return makeResponse(False, "Method not supported.")

    def options(self, collection, id=None, action=None):
        return makeResponse('dataOnly', "")

    def get(self, collection, id=None, action=None):
        #NOTE: Don't ask me why but if I don't do this here, the files will not be available later....
        flask.request.files
        kwargs = {}
        if flask.request.args:
            for k, v in flask.request.args.items():
                #If the value is just a "str", decode will fail, 
                # so handling that case and keeping the value as such in the kwargs...
                try:
                    kwargs[k] = json.loads(v)
                except:
                    kwargs[k] = v
        #TODO:
        # Think about the case where query paramaters are passed for "get" & "getAll" scenario!!!
        # For now we are not handling it, we are only handling for the "action" cases if some one uses
        # GET instead of the POST which is our default.... 
        if action and has_permission(action):
            actionHandler = getattr(flask.g.handler, action, None)
                #NOTE:
                # This is not a good thing to do, what if someone actually wants to send partnerId in query??,
                # commented for now, earlier due to login session issue in the angularjs $http interceptor 
                # we used to send the partnerId from the browser and validate it against the 'sessionData' 
                # partnerId to make sure we are sane.... 
                # From now on we should not be requiring it and the interceptor should be removed in ui.
                #
                #if "partnerId" in kwargs:
                #    del kwargs['partnerId']

            resp = actionHandler(id, **kwargs)
        elif (id is not None) and has_permission("get"):
            resp = flask.g.handler.get(id)
        elif has_permission("getAll"):
            resp = flask.g.handler.getAll(**kwargs)
        else:
            resp = (False, "Not Authorized to get data from collection: %s" % (collection))
        return makeResponse(*resp)

    def _getBodyData(self):
        #NOTE: Don't ask me why but if I don't do this here, the files will not be available later....
        flask.request.files

        bodyData = {}
        if flask.request.data:
            #TODO:
            # Handle XML data
            logr.info(flask.request.data)
            if 'json' in flask.request.headers.get('content-type'):
                bodyData = flask.request.json
            elif 'x-www-form-urlencoded' in flask.request.headers.get('content-type'):
                #TODO: Check if "form" is the correct variable....
                bodyData = flask.request.form
            else:
                try:
                    bodyData = json.loads(flask.request.data)
                except:
                    logr.info('Crude Json parsing of data failed: %s' % (flask.request.data))

        return bodyData
    def post(self, collection, id=None, action=None):
        #TODO:
        # Handle case for multipart mime message, which has both files and json body
        kwargs = self._getBodyData()
        if action and has_permission(action):
            actionHandler = getattr(flask.g.handler, action, None)
            resp = actionHandler(id, **kwargs)
        elif id is None and has_permission("create"):
            resp = flask.g.handler.create(kwargs)
        else:
            resp = (False, "Invalid or unauthorized access %s / %s" % (collection, action))
        return makeResponse(*resp)

    #PUT will be handled only for an instance in a collection, i.e. to update an instance
    def put(self, collection, id):
        kwargs = self._getBodyData()
        if id and has_permission("modify"):
            resp = flask.g.handler.modify(id, kwargs)
        else:
            resp = (False, "Invalid or unauthorized access %s / %s" % (collection, id))
        return makeResponse(*resp)

    #PATCH will be handled only for an instance in a collection, i.e. to update an instance
    def patch(self, collection, id):
        kwargs = self._getBodyData()
        if id and has_permission("modify"):
            resp = flask.g.handler.modify(id, kwargs)
        else:
            resp = (False, "Invalid or unauthorized access %s / %s" % (collection, id))
        return makeResponse(*resp)

    #DELETE will be handled only for an instance in a collection, i.e. to delete an instance
    def delete(self, collection, id):
        if id and has_permission("delete"):
            resp = flask.g.handler.delete(id)
        else:
            resp = (False, "Invalid or unauthorized access %s / %s" % (collection, id))
        return makeResponse(*resp)

def getAddrInfoWrapper(host, port, family=0, socktype=0, proto=0, flags=0):
    return origGetAddrInfo(host, port, socket.AF_INET, socktype, proto, flags)

#Changing the getAddr, else it takes long time to resolve domains...
try:
    gevent #Check for presence of gevent
    origGetAddrInfo = gevent.socket.getaddrinfo
    gevent.socket.getaddrinfo = getAddrInfoWrapper
except:
    origGetAddrInfo = socket.getaddrinfo

socket.getaddrinfo = getAddrInfoWrapper

def create_app():
    #Add current directory to search for application modules.....
    sys.path.append(os.getcwd())

    app = flask.Flask(__name__)
    #app.config["PROPAGATE_EXCEPTIONS"] = True
    if len(config.cookieDomain):
        app.config["SESSION_COOKIE_DOMAIN"] = config.cookieDomain
    app.secret_key = config.secretKey
    app.session_interface = sessionhandler.ItsdangerousSessionInterface()
    app.session_cookie_name = config.cookieName

    app.add_url_rule(config.flaskBaseUrl+"/<collection>", view_func=RequestHandler.as_view("collection"), methods=["HEAD", "GET", "POST", "OPTIONS"])
    app.add_url_rule(config.flaskBaseUrl+"/<collection>/<id>", view_func=RequestHandler.as_view("instance"), methods=["GET", "PUT", "DELETE", "PATCH","OPTIONS"])
    app.add_url_rule(config.flaskBaseUrl+"/<collection>/<id>/<action>", view_func=RequestHandler.as_view("action"), methods=["POST", "GET", "OPTIONS"])

    return app

app = create_app()


@app.errorhandler(500)
def exceptionHandler(error):
    errFormat = '''Error:
Stack Trace:
%s
Environment:
%s
Payload Data:
%s
    ''' % (traceback.format_exc(), pprint.pformat(flask.request.environ), flask.request.data)
    logr.error(errFormat)
    return makeResponse('dataOnly', errFormat, statusCode=500)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="Listen on port.", default=8000, type=int)
    parser.add_argument('-d', "--debug",help='Run in debug mode', default=False, action='store_true', required=False)
    args = parser.parse_args()

    http_server =  gevent.pywsgi.WSGIServer(('', args.port), app)
    http_server.serve_forever()


if __name__ == "__main__":
    main()
