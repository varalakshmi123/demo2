import subprocess
import calendar
import datetime
import time

import xml.sax.saxutils

import logger
logr = logger.Logger.getInstance("framework").getLogger()


def execute(command, waitForCompletion=True):
    child = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    if waitForCompletion:
        out, err = child.communicate()
        status = child.returncode
        return status, out, err
    else:
        return child

def getUtcTime():
    return calendar.timegm(time.gmtime())

def fixXMLData(data):
    fixed = data
    #fixed = escape(fixed, {"'": '&#39;', '"': '&quot;'})
    fixed = xml.sax.saxutils.escape(fixed, {"'": '\\\'', '"': '&quot;'})
    return fixed
