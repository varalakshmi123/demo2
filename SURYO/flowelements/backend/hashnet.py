#*************************************************************************
# 
# HashInclude CONFIDENTIAL
# ________________________
# 
#  [2010] - [2016] HashInclude Computech Private Limited
#  All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of HashInclude Computech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to HashInclude Computech
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from HashInclude Computech Private Limited.
#*************************************************************************
#!/usr/share/nginx/www/env/bin/python

import sys
import network as networkManager
from requestManager import requestManager

def usage():
    execName = "hashnnet"
    print("Usage: ")
    print(" %s help" % execName)
    print(" %s show all" % execName)
    print(" %s show <interface>" % execName)
    print(" %s enable <interface>" % execName)
    print(" %s disable <interface>" % execName)
    print(" %s dhcp <interface>" % execName)
    print(" %s static <interface> <ip> <netMask> <gateway> <dnsServer>" % execName)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        usage()
    elif sys.argv[1] == "getIf" and len(sys.argv) == 2:
        iface_list = networkManager.getInterfaces()
        print(", ".join(iface_list))
    elif sys.argv[1] == "connAllIf" and len(sys.argv) == 2:
        networkManager.connectAllInterfaces()
    elif sys.argv[1] == "show" and len(sys.argv) == 3:
        networkManager.show(sys.argv[2])
    elif sys.argv[1] == "enable" and len(sys.argv) == 3:
        networkManager.setEnable(sys.argv[2],True)
    elif sys.argv[1] == "disable" and len(sys.argv) == 3:
        networkManager.setEnable(sys.argv[2],False)
    elif sys.argv[1] == "dhcp" and len(sys.argv) == 3:
        #if not configureMongo.isMongoConfigured():
        if True:
            networkManager.setDhcp(sys.argv[2])
        else:
            resp = requestManager.action("utilities", "abc", "setNetworkDetails", {"interface": sys.argv[2], "isDhcp": True})
            if resp["status"] == "error":
               print("Error configuring interface: " + resp["msg"])
    elif sys.argv[1] == "static" and len(sys.argv) == 7:
        #if not configureMongo.isMongoConfigured():
        if True:
            config = {
                "IP": sys.argv[3],
                "Netmask": sys.argv[4],
                "Gateway": sys.argv[5],
                "DnsServer": sys.argv[6],
                "Method":"Manual"
            }
            networkManager.setStatic(sys.argv[2],config)
        else:
            config = {
                "interface":sys.argv[2],
                "isDhcp":False,
                "IP": sys.argv[3],
                "Netmask": sys.argv[4],
                "Gateway": sys.argv[5],
                "DnsServer": sys.argv[6]
            }
            resp = requestManager.action("utilities", "abc", "setNetworkDetails", config)
            if resp["status"] == "error":
                print("Error configuring interface: " + resp["msg"])
    else:
        usage()
