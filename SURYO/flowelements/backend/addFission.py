import subprocess


def execution(command, waitForCompletion=True):
    child = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    if waitForCompletion:
        out, err = child.communicate()
        status = child.returncode
        return status, out, err
    else:
        return child


class Fission(object):
    # Function
    def addFunction(self, server, functionName, envName, funcCode, routeName=None, method=None):
        cmd = "fission --server %s fn create --name %s --env %s --code %s" % (server, functionName, envName, funcCode)
        print(cmd)
        if routeName is not None and method is not None:
            cmd += " --url %s --method %s" % (routeName, method)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Function created" % functionName

    def updateFunction(self, server, functionName, envName, funcCode):
        cmd = "fission --server %s fn update --name %s --env %s --code %s" % (server, functionName, envName, funcCode)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Function updated" % functionName

    def deleteFunction(self, server, functionName):
        cmd = "fission --server %s fn delete --name %s" % (server, functionName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Function deleted" % functionName

    def listFunction(self, server):
        ret = dict()
        cmd = "fission --server %s fn list" % server
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            out = out.decode("utf-8")
            if len(out):
                out = out.split('\n')[1:]
                for line in out:
                    if len(line) == 0:
                        continue
                    l = line.split(' ')
                    l = ' '.join(l).split()
                    ret[l[0]] = l[2]
            return True, ret

    # Environment
    def addEnvironment(self, server, envName, imageName):
        cmd = "fission --server %s env create --name %s --image %s" % (server, envName, imageName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Environment added" % envName

    def updateEnvironment(self, server, envName, imageName):
        cmd = "fission --server %s env update --name %s --image %s" % (server, envName, imageName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Environment updated" % envName

    def deleteEnvironment(self, server, envName):
        cmd = "fission --server %s env delete --name %s" % (server, envName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Environment deleted" % envName

    def listEnvironment(self, server):
        ret = dict()
        cmd = "fission --server %s env list" % server
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            out = out.decode("utf-8")
            if len(out):
                out = out.split('\n')[1:]
                for line in out:
                    if len(line) == 0:
                        continue
                    l = line.split(' ')
                    l = ' '.join(l).split()
                    ret[l[0]] = l[2]
            return True, ret

    # http Trigger
    def createTrigger(self, server, method, url, functionName):
        cmd = "fission --server %s route create --method %s --url %s --function %s" % (server, method, url, functionName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "Trigger created"

    def updateTrigger(self, server, triggerName, funcName):
        cmd = "fission --server %s route update --name %s --function %s" % (server, triggerName, funcName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Trigger updated" % triggerName

    def deleteTrigger(self, server, triggerName):
        cmd = "fission --server %s route delete --name %s" % (server, triggerName)
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            return True, "%s Trigger deleted" % triggerName

    def listTrigger(self, server):
        ret = {}
        cmd = "fission --server %s route list" % server
        (status, out, err) = execution(cmd)
        if status is 1:
            return False, err
        else:
            out = out.decode("utf-8")
            if len(out):
                out = out.split('\n')[1:]
                for line in out:
                    if len(line) == 0:
                        continue
                    l = line.split(' ')
                    l = ' '.join(l).split()
                    ret[l[0]] = [l[1], l[2], l[3]]
            return True, ret


def createImage(imagetype):
    pass


