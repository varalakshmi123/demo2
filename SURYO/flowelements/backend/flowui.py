import requests
import dbinterface
from FlowElements import *
from addFission import *


def deploy_flow(flowId):
    flowdb = dbinterface.MongoCollection("flows")
    flowData = flowdb.find_one({"_id": ObjectId(flowId)})
    if flowData is None:
        return False, "Invalid flow id"

    # Checking if the function is already created and routed
    f = Fission()
    server = "192.168.2.38:31313"
    envname = ""
    status, funcdict = f.listFunction(server)
    funclist = funcdict.keys()
    if flowId in funclist:
        print("Function already available for %s" % flowId)
    else:
        print("Creating function for %s" % flowId)

        # CREATING THE CODE
        conn = flowData['connections']
        nodes = flowData['nodes']

        finallist = list()
        for index, ndata in enumerate(nodes):
            nodedata = {"id": index + 1, "type": ndata['className'].strip()}
            if index != len(nodes) - 1:
                nodedata['next'] = index + 2
            prop = ndata['properties']
            if 'dfCols' in prop:
                del prop['dfCols']
            if 'filterStored' in prop:
                del prop['filterStored']
            if 'sourceFile' in prop:
                prop['sourceFile'] = "/ereconfiles/"+prop['sourceFile']
            if 'ouputFile' in prop:
                prop['ouputFile'] = 'ereconfiles/'+prop['ouputFile']
            nodedata['properties'] = prop
            finallist.append(nodedata)

        f = open("FlowElements.py")
        flowcode = f.read()
        f.close()
        #print(flowcode)

        code = "\ndef main():\n    flowdata=%s\n    f = FlowRunner(flowdata)\n    f.run()\n    return \"Success\"" % finallist
        flowcode += code
        f = open("%s.py" % flowId, 'w')
        f.write(flowcode)
        f.close()
        #print(code)
        return ""

deploy_flow("596f3a2b7d488f5d85632cfa")
