import dbinterface
import hashlib
import base64
import flask
import utilities
import json
import config
import inspect
import pyclbr
import bson
import re
import os
import logger
import httplib2
import random
import time
#from urlparse import urlparse
#import urllib.parse
# from ActiveDirectory import *
logger = logger.Logger.getInstance("application").getLogger()

#Key is the name of the role, value is the list of roles that can create the role
roles = {}
#Key is the name of the role, value is an object with collection name as key and list of functions as value
acls = {}

def apiKey(userId):
    return base64.b64encode(hashlib.sha256(userId).hexdigest())

def getMongoCaseInsensitive(data):
    return re.compile("^"+data+"$", re.IGNORECASE)

def login():
    if flask.request.method == 'POST' and "sessionData" not in flask.session:
        userQuery = None
        logger.info(flask.request.json)
        # Validate user/password
        flask.request.json["userName"] = flask.request.json["userName"].lower()
        if flask.request.remote_addr == "127.0.0.1":
            userQuery = {"userName": flask.request.json["userName"]}
        elif "userName" in flask.request.json and "password" in flask.request.json:
            if flask.request.json["userName"] == 'admin':
                userQuery = {"userName": flask.request.json["userName"],
                         "password": flask.request.json['password']}
            else:
                userQuery = {"userName": flask.request.json["userName"],
                             "password": hashlib.md5(flask.request.json['password']).hexdigest()}
        elif "userName" in flask.request.json and "apiKey" in flask.request.json:
            (status, found) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})
            if status and found and len(found):
                if flask.request.json["apiKey"] == apiKey(found["_id"]):
                    userQuery = {"userName": flask.request.json["userName"]}

        if userQuery:
            logger.info("userquery-----" + str(userQuery))
            (status, found) = Users(hideFields=False).get(userQuery)
            if status and found and len(found):
                # Checking if user is active and allowed to login
                if 'isActive' in found and found['isActive'] is False:
                    return flask.Response(json.dumps({"status": "error", "msg": "User is disabled for login,please contact Administrator."}), 200, [("Content-Type", "application/json")])
                del found["password"]
                flask.session["sessionData"] = found
            else:
                # Checking if the USER is in AD
                try:
                    if not validateADUser(flask.request.json["userName"], flask.request.json['password']):
                        return flask.Response(json.dumps({"status": "error", "msg": "Username/Password is incorrect."}), 200, [("Content-Type", "application/json")])
                    else:
                        (status, found) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})
                        if status is False:
                            newUser = dict(userName=flask.request.json["userName"], password="", role='customer', adUser=True)
                            adminRi = requestsInterface()
                            adminRi.post("user_management", newUser)
                            (status, found) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})
                        del found["password"]
                        flask.session["sessionData"] = found
                except Exception as e:
                    logger.info("Exception in login:%s" % e)
                    return flask.Response(json.dumps({"status": "error", "msg": "Username/Password is incorrect."}), 200, [("Content-Type", "application/json")])

    if "sessionData" in flask.session:
        #flask.session["sessionData"]["role"] = "customer"
        return flask.Response(json.dumps(
            {"status": "ok", "msg": {"_id": flask.session["sessionData"]["_id"],"partnerId": flask.session["sessionData"]["partnerId"], "apiKey": flask.session["sessionData"]["apiKey"], "role": flask.session["sessionData"]["role"], "userName": flask.session["sessionData"]["userName"],
             "machineId": utilities.getMachineId()}}), 200, [("Content-Type", "application/json")])
    else:
       flask.abort(401)

def logout():
    #if "sessionData" in flask.session:
    flask.session.clear()
    # May be update user for LastLogOut
    flask.abort(401)

def has_permission(role, collection, operation):
    global acls
    if len(acls) > 0:
        if role in acls:
            if collection in acls[role]:
                if operation in acls[role][collection]:
                    return True
                elif "*" in acls[role][collection]:
                    return True
            elif "*" in acls[role]:
                if operation in acls[role]["*"]:
                    return True
                elif "*" in acls[role]["*"]:
                    return True
        return False

    return True

# Called from flaskinterface.py
def initRoleAndAcl():
    global acls, roles
    acls = {}
    roles = {}
    (status, data) = Acls().getAll()
    for acl in data["data"]:
        acls[acl["role"]] = acl["access"]
        roles[acl["role"]] = acl["creators"]


class Users(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Users, self).__init__("users", hideFields, mongoHost=config.usersMongoHost, mongoDBName=config.usersDatabaseName)

    def getHiddenFields(self):
        return []

    def create(self, doc):
        if not all(prop in doc for prop in ["userName", "password", "role"]):
            return False, "Invalid input, Missing mandatory fields"

        if not len(roles):
            initRoleAndAcl()

        # If reseller, adding customer into roles for reseller so that he can create customer
        # if flask.session["sessionData"]["role"] != "reseller":
        #     if doc["role"] not in roles or flask.session["sessionData"]["role"] not in roles[doc["role"]]:
        #         logger.info("Error creating user")
        #         logger.info("SessionData: %s" % flask.session["sessionData"]["role"])
        #         logger.info("RolesData: %s" % doc['role'])
        #         logger.info("Roles: %s" % roles)
        #         return False, "Unauthorized to create this role"

        if self.exists({"userName": doc["userName"]}):
            return False, "User already exists"

        if ("password" in doc) and (len(doc['password'])):
            doc["password"] = hashlib.md5(doc['password']).hexdigest()

        doc["_id"] = bson.objectid.ObjectId()
        # doc["partnerId"] = str(doc["_id"])
        doc["apiKey"] = base64.b64encode(hashlib.sha256(str(doc["_id"])).hexdigest())

        return super(Users, self).create(doc)

    def modify(self, condition, doc, multi=False):
        if ("role" in doc) and ("admin" not in doc["role"]) and (flask.session["sessionData"]["role"] not in roles[doc["role"]]):
            return False, "Unauthorized to update this role"
        if "password" in doc:
            doc["password"] = hashlib.md5(doc['password']).hexdigest()
        logger.info(doc)
        return super(Users, self).modify(condition, doc)

    def deleteData(self, condition):
        return super(Users, self).delete(condition)

    def getUser(sefl,userName):
        userQuery = {'partnerId': flask.session["sessionData"]['partnerId'], "userName": getMongoCaseInsensitive(userName)}
        return Users(hideFields=False).get(userQuery)

    def getAllRoles(self, dummy):
        global roles
        rolesList=[]
        for role in roles:
            if role not in ["integrator"]:
                rolesList.append(role)
        resp={}
        resp["roles"]=rolesList
        return True,resp

    def changePassword(self, userId, password):
        doc = dict()
        doc['password'] = hashlib.md5(password).hexdigest()
        if '#' in doc['password']:
            return False, "The # character cannot be used in the password ...."
        return super(Users, self).modify(userId, doc)

    def setEmail(self, userId, email):
        doc = dict()
        doc['email'] = email
        return super(Users, self).modify(userId, doc)


class Acls(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        #super(Acls, self).__init__("acls", hideFields, mongoHost=config.usersMongoHost, mongoDBName=config.usersDatabaseName)
        super(Acls, self).__init__("acls", hideFields)

    def getAll(self, query={}):
        (status, resp) = super(Acls, self).getAll()
        try:
            if 'sessionData' in flask.session and flask.session["sessionData"]["role"] == "reseller":
                # Adding the customer acl to reseller so the reseller can create the user
                data = Acls().find_one({"role": "customer"})
                resp['total'] += 1
                resp['current'] += 1
                resp['data'].append(data)
        except Exception as e:
            pass
        return status, resp

    def create(self, doc):
        if flask.session["sessionData"]["role"] == "admin":
            resp = super(Acls, self).create(doc)
            initRoleAndAcl()
            return resp
        return False, "Unauthorized to create new acls"

    def modify(self, condition, doc, multi=False):
        resp = super(Acls, self).update(condition, doc, multi)
        initRoleAndAcl()
        return resp

    def delete(self, condition):
        resp = super(Acls, self).delete(condition)
        initRoleAndAcl()
        return resp

    def getmodules(self, id):
        modules = {}
        for module in config.applicationModules:
            moduleInstance = __import__(module)
            moduleClasses = pyclbr.readmodule(module)
            for klassName, klassSpec in moduleClasses.items():
                modules[klassName] = []
                klassInstance = getattr(moduleInstance, klassName)
                for klassMethod, na in inspect.getmembers(klassInstance):
                    if not klassMethod.startswith("_") and not klassMethod in ['changeOwner', 'count', 'exists', 'getHiddenFields', 'hasAccess', 'setUser']:
                        modules[klassName].append(klassMethod)
        return True, modules
