import dbinterface
from FlowElements import *
from addFission import *


def deploy_flow(flowId):
    flowdb = dbinterface.MongoCollection("flows")
    flowData = flowdb.find_one({"_id": ObjectId(flowId)})
    if flowData is None:
        return False, "Invalid flow id"

    # Checking if the function is already created and routed
    f = Fission()
    server = "192.168.2.38:31313"
    flist = f.listFunction(server)
    print(flist)

    return ""


    conn = flowData['connections']
    nodes = flowData['nodes']

    finallist = list()
    for index, ndata in enumerate(nodes):
        nodedata = {"id": index+1, "type": ndata['className'].strip()}
        if index != len(nodes) - 1:
            nodedata['next'] = index+2
        prop = ndata['properties']
        if 'dfCols' in prop:
            del prop['dfCols']
        if 'filterStored' in prop:
            del prop['filterStored']
        nodedata['properties'] = prop
        finallist.append(nodedata)

    f = FlowRunner(finallist)
    f.run()


deploy_flow("596f3a2b7d488f5d85632cfa")
