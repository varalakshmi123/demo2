#*************************************************************************
#
# HashInclude CONFIDENTIAL
# ________________________
#
#  [2010] - [2016] HashInclude Computech Private Limited
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of HashInclude Computech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to HashInclude Computech
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from HashInclude Computech Private Limited.
#*************************************************************************
#!/usr/bin/python

import os
import sys
import dbus
import copy
import time
import socket
from uuid import uuid4
import threading

nwGlobalLock = threading.Lock()

ServiceName = "org.freedesktop.NetworkManager"
NetworkManagerObjectName = "/org/freedesktop/NetworkManager"
NetworkManagerInterface  = "org.freedesktop.NetworkManager"
DeviceWiredInterface     = "org.freedesktop.NetworkManager.Device.Wired"
SettingsObjectName  = "/org/freedesktop/NetworkManager/Settings"
SettingsInterface   = "org.freedesktop.NetworkManager.Settings"
ConnectionInterface = "org.freedesktop.NetworkManager.Settings.Connection"
PropertiesInterface = "org.freedesktop.DBus.Properties"
ActiveInterface     = "org.freedesktop.NetworkManager.Connection.Active"
DeviceInterface     = "org.freedesktop.NetworkManager.Device"
AutoIpInterface     = "org.freedesktop.NetworkManager.IP4Config"

class Interface:
    def __init__(self, interface): #interface is eth0, eth1, etc.
        self.systemBus = dbus.SystemBus()
        self.nm = self.systemBus.get_object(ServiceName, NetworkManagerObjectName)
        self.nmIf = dbus.Interface(self.nm, NetworkManagerInterface)
        devices = self.nmIf.GetDevices()
        for d in devices:
            self.device = d
            self.deviceproxy = self.systemBus.get_object(ServiceName, d)
            self.deviceinterface  = dbus.Interface(self.deviceproxy,PropertiesInterface)
            self.activeConnection = self.deviceinterface.Get(DeviceInterface, "ActiveConnection")
            self.ip4config = self.deviceinterface.Get(DeviceInterface, "Ip4Config")

            #Get the Settings Connection
            if self.activeConnection != "/":
                conProxy = self.systemBus.get_object(ServiceName, self.activeConnection)
                conn = dbus.Interface(conProxy, PropertiesInterface)
                self.connection = conn.Get(ActiveInterface, "Connection")
            else:
                connName = self.deviceinterface.Get(DeviceInterface,'Interface')
                connectionPaths = self._listConnectionPaths()
                for path in connectionPaths:
                    conProxy = self.systemBus.get_object(ServiceName, path)
                    settingsConnection = dbus.Interface(conProxy, ConnectionInterface)
                    config = settingsConnection.GetSettings()
                    if connName == config['connection']['id']:
                        self.connection = path

            if self.deviceinterface.Get(DeviceInterface,'Interface') != interface:
                self.device = None
                self.deviceproxy = None
                self.deviceinterface = None
                self.activeConnection = None
                self.ip4config = None
                self.connection = None
                continue
            if int(self.deviceinterface.Get(DeviceInterface,'DeviceType')) != 1:
                raise TypeError("Only wired devices are supported")
            break
        if self.deviceproxy == None or self.deviceinterface == None:
            raise TypeError("Device not found, %s" % interface)

    def setEnable(self, enable):
        if enable == False and self.deviceinterface.Get(DeviceInterface,'ActiveConnection') != "/":
            deviceConn = dbus.Interface(self.deviceproxy, DeviceInterface)
            deviceConn.Disconnect()
            interfaces = os.listdir("/etc/NetworkManager/system-connections/")
            for interface in interfaces:
                if interface.startswith(DeviceInterface):
                    os.system("rm -rf /etc/NetworkManager/system-connections/%s" % DeviceInterface)
            '''
            if os.path.exists("/etc/NetworkManager/system-connections/%s" % DeviceInterface):
                os.system("rm -rf /etc/NetworkManager/system-connections/%s" % DeviceInterface)
            '''
        elif enable == True and self.deviceinterface.Get(DeviceInterface,'ActiveConnection') == "/":
            self.nmIf.ActivateConnection(self.connection, self.device, "/")
        else:
            print("Nothing to do")

    def _NetmaskToPrefixlen(self,netmask):
        prefixlen=32
        ip_int = socket.ntohl(self._IpToNum(netmask))
        while prefixlen:
            if ip_int & 1 == 1:
                break
            ip_int >>= 1
            prefixlen -= 1

        return prefixlen

    def _IpToNum(self,ip):
        dot = ip.split('.')
        num = int(dot[0]) << 24 | int(dot[1]) << 16 | int(dot[2]) << 8 | int(dot[3])
        num = socket.htonl(num)
        return num

    def _numToIp(self, num):
        return "%d.%d.%d.%d" % ((num & 0xff000000) >> 24, (num & 0x00ff0000) >> 16, (num&0x0000ff00) >> 8, (num & 0x000000ff))

    def _getAutoConnDetails(self,objectPath):
        autoData = {}

        autoproxy = self.systemBus.get_object(ServiceName, objectPath)
        autointerface = dbus.Interface(autoproxy,PropertiesInterface)
        addr = autointerface.Get(AutoIpInterface,'Addresses')

        autoData['address'] = self._numToIp(socket.ntohl(addr[0][0]))
        autoData['netmask'] = self._numToIp(0xffffffff ^ (0xffffffff >> addr[0][1]))
        autoData['gateway'] = self._numToIp(socket.ntohl(addr[0][2]))

        nameServer = autointerface.Get(AutoIpInterface,'Nameservers')
        autoData['dns'] = self._numToIp(socket.ntohl(nameServer[0]))
        return autoData

    def getConfig(self):
        connData = {"interface":"","type":"","address":"","netmask":"","gateway":"","dns":"","physical":False}
        connData['interface'] = self.deviceinterface.Get(DeviceInterface,'Interface')
        if self.deviceinterface.Get(DeviceWiredInterface,'Carrier') == 1:
            connData['physical'] = True
        connData['cableConnected'] = False
        try:
            if os.path.exists("/sys/class/net/%s/carrier" % connData['interface']):
                try:
                    f = open("/sys/class/net/%s/carrier" % connData['interface'])
                    data = f.read().strip()
                    if int(data) == 0:
                        connData['cableConnected'] = False
                    else:
                        connData['cableConnected'] = True
                    f.close()
                except Exception as e:
                    pass

        except Exception as e:
            pass
        if self.activeConnection == "/":
            return connData
        #Get the Settings of the Connection
        settingsProxy = self.systemBus.get_object(ServiceName, self.connection)
        settingsConn  = dbus.Interface(settingsProxy, ConnectionInterface)
        connSettings  = settingsConn.GetSettings()

        if connSettings['ipv4']['method'] == "auto":
            connData.update(self._getAutoConnDetails(self.ip4config))
            connData['isDhcp'] = True
            connData['type'] = 'DHCP'
        else:
            addr = connSettings['ipv4']['addresses']
            connData['address'] = self._numToIp(socket.ntohl(addr[0][0]))
            connData['netmask'] = self._numToIp(0xffffffff ^ (0xffffffff >> addr[0][1]))
            connData['gateway'] = self._numToIp(socket.ntohl(addr[0][2]))
            connData['dns'] = ",".join([self._numToIp(socket.ntohl(x)) for x in connSettings['ipv4']['dns']])
            connData['isDhcp'] = False
            connData['type'] = 'Static'
        return connData

    def _listConnectionPaths(self):
        proxy = self.systemBus.get_object(ServiceName, SettingsObjectName)
        settings = dbus.Interface(proxy, SettingsInterface)
        connectionPaths = settings.ListConnections()
        return connectionPaths

    def _activateConnection(self, connectionUUID):
        connectionPaths = self._listConnectionPaths()
        for path in connectionPaths:
            conProxy = self.systemBus.get_object(ServiceName, path)
            settingsConnection = dbus.Interface(conProxy, ConnectionInterface)
            config = settingsConnection.GetSettings()
            if connectionUUID == config['connection']['uuid']:
                nmProxy = self.systemBus.get_object(ServiceName, NetworkManagerObjectName)
                nm = dbus.Interface(nmProxy, NetworkManagerInterface)
                self._deActivateConnection()
                try:
                    nm.ActivateConnection(path,self.device,"/")
                except:
                    print('Exception in activating')
                    return
                time.sleep(2)

    def _deActivateConnection(self):
        proxy = self.systemBus.get_object(ServiceName, NetworkManagerObjectName)
        settings = dbus.Interface(proxy, PropertiesInterface)
        activeConPath = settings.Get(ServiceName,'ActiveConnections')
        for path in activeConPath:
            nm = dbus.Interface(proxy, NetworkManagerInterface)
            nm.DeactivateConnection(path)
            break

    def _addConnection(self,connectionDetails):
        #print 'adding a new connection with %s' % connectionDetails
        uuid = str(uuid4())

        #mac-address needs byte-array
        mac  = connectionDetails['Mac']
        macs = mac.split(":")
        m = [chr(int(x,16)) for x in macs]
        macba = dbus.ByteArray("".join(m))

        s_wired = dbus.Dictionary({
            'duplex': 'full',
            'mac-address': macba})
        s_con = dbus.Dictionary({
            'type': '802-3-ethernet',
            'uuid': uuid,
            'id': connectionDetails['Name']})
        if connectionDetails['Method'] == 'Auto':
            s_ip4 = dbus.Dictionary({'method': 'auto'})
        elif connectionDetails['Method'] == 'Manual':
            dnsservers = None
            if 'DnsServer' in connectionDetails:
                dnsservers = [dbus.UInt32(self._IpToNum(dnsSrv.strip())) for dnsSrv in connectionDetails['DnsServer'].split(",") if len(dnsSrv)]
            manualaddr = dbus.Array([dbus.UInt32(self._IpToNum(connectionDetails['IP'])), dbus.UInt32(self._NetmaskToPrefixlen(connectionDetails['Netmask'])), dbus.UInt32(self._IpToNum(connectionDetails['Gateway']))], signature=dbus.Signature('u'))

            s_ip4 = {'addresses': dbus.Array([manualaddr], signature=dbus.Signature('au')), 'method': 'manual'}
            if dnsservers:
                s_ip4['dns'] = dbus.Array(dnsservers, signature=dbus.Signature('u'))

        conn = dbus.Dictionary({
            '802-3-ethernet': s_wired,
            'connection': s_con,
            'ipv4': dbus.Dictionary(s_ip4)})

        proxy = self.systemBus.get_object(ServiceName, SettingsObjectName)
        settings = dbus.Interface(proxy, SettingsInterface)
        addedPath = settings.AddConnection(conn)
        self._activateConnection(uuid)

    def _delConnection(self,connectionUUID):
        connectionPaths = self._listConnectionPaths()
        for path in connectionPaths:
            conProxy = self.systemBus.get_object(ServiceName, path)
            settingsConnection = dbus.Interface(conProxy, ConnectionInterface)
            connSettings = settingsConnection.GetSettings()
            if connSettings['connection']['uuid'] == connectionUUID:
                settingsConnection.Delete()
        os.system("rm -rf /etc/NetworkManager/system-connections/%s" % connectionUUID)

    def setDhcp(self):
        connName = self.deviceinterface.Get(DeviceInterface,'Interface')
        macAddr  = self.deviceinterface.Get(DeviceWiredInterface,'HwAddress')
        config   = {'Name':connName,'Method':'Auto','Mac':macAddr}
        self._delConnection(connName)
        self._addConnection(config)

    def setStatic(self, config):
        connName = self.deviceinterface.Get(DeviceInterface,'Interface')
        macAddr  = self.deviceinterface.Get(DeviceWiredInterface,'HwAddress')
        config['Name'] = connName
        config['Mac']  = macAddr
        self._delConnection(connName)
        self._addConnection(config)

def connectAllInterfaces():
    iface_list = getInterfaces()
    for iface in iface_list:
        setEnable(iface, False)
        time.sleep(2)
        setEnable(iface, True)
        time.sleep(5)
        data = show(iface)
        if len(data) == 0:
            setDhcp(iface)
        # if str(data[0]['interface']) == iface:
        #    pass

def getInterfaces():
    IfaceList = []
    systemBus = dbus.SystemBus()
    nm = systemBus.get_object(ServiceName, NetworkManagerObjectName)
    nmIf = dbus.Interface(nm, NetworkManagerInterface)
    devices = nmIf.GetDevices()
    for d in devices:
        deviceproxy = systemBus.get_object(ServiceName, d)
        deviceinterface = dbus.Interface(deviceproxy,PropertiesInterface)
        if int(deviceinterface.Get(DeviceInterface,'DeviceType')) == 1:
            iface = str(deviceinterface.Get(DeviceInterface,'Interface'))
            if iface.lower().startswith('usb'):
                continue
            if not os.path.exists("/sys/class/net/%s/carrier" % iface):
                continue
            f = open("/sys/class/net/%s/carrier" % iface)
            data = f.read()
            f.close()
            if data.strip() == '0':
                continue
            IfaceList.append(iface)
    return IfaceList

def show(interface):
    global nwGlobalLock
    configs = []
    nwGlobalLock.acquire()
    try:
        tmp = []
        if interface == "all":
            systemBus = dbus.SystemBus()
            nm = systemBus.get_object(ServiceName, NetworkManagerObjectName)
            nmIf = dbus.Interface(nm, NetworkManagerInterface)
            devices = nmIf.GetDevices()
            for d in devices:
                deviceproxy = systemBus.get_object(ServiceName, d)
                deviceinterface = dbus.Interface(deviceproxy,PropertiesInterface)
                if int(deviceinterface.Get(DeviceInterface,'DeviceType')) == 1:
                    try:
                        iface = Interface(deviceinterface.Get(DeviceInterface,'Interface'))
                        configs.append(iface.getConfig())
                        tmp.append(iface)
                    except Exception as e:
                        pass
        else:
            configs.append(Interface(interface).getConfig())

        template = "{interface:15} {cableConnected:15} {physical:15} {type:15} {address:15} {netmask:15} {gateway:15} {dns:15}"
        print(template.format(interface="interface", cableConnected="cableConnected", physical="physical", type="type", address="address", netmask="netmask", gateway="gateway", dns="dns"))
        tmpCopy = copy.deepcopy(configs)
        for c in tmpCopy:
            c["physical"] = str(c["physical"])
            c["cableConnected"] = str(c["cableConnected"])
            print(template.format(**c))
    except:
        pass
    nwGlobalLock.release()
    return configs

def setDhcp(interface):
    global nwGlobalLock
    nwGlobalLock.acquire()
    try:
        Interface(interface).setDhcp()
    except:
        pass
    nwGlobalLock.release()

def setStatic(interface, config):
    global nwGlobalLock
    nwGlobalLock.acquire()
    try:
        Interface(interface).setStatic(config)
    except:
        pass
    nwGlobalLock.release()

def setEnable(interface, enable):
    global nwGlobalLock
    nwGlobalLock.acquire()
    try:
        Interface(interface).setEnable(enable)
    except:
        pass
    nwGlobalLock.release()

def usage():
    print("Usage: ")
    print(" %s help" % (sys.argv[0]))
    print(" %s show all" % (sys.argv[0]))
    print(" %s show <interface>" % (sys.argv[0]))
    print(" %s enable <interface>" % (sys.argv[0]))
    print(" %s disable <interface>" % (sys.argv[0]))
    print(" %s dhcp <interface>" % (sys.argv[0]))
    print(" %s static <interface> <ip> <netMask> <gateway> <dnsServer>" % (sys.argv[0]))

if __name__ == "__main__":
    if len(sys.argv) == 1:
        usage()
    elif sys.argv[1] == "show" and len(sys.argv) == 3:
        show(sys.argv[2])
    elif sys.argv[1] == "enable" and len(sys.argv) == 3:
        setEnable(sys.argv[2],True)
    elif sys.argv[1] == "disable" and len(sys.argv) == 3:
        setEnable(sys.argv[2],False)
    elif sys.argv[1] == "dhcp" and len(sys.argv) == 3:
        setDhcp(sys.argv[2])
    elif sys.argv[1] == "static" and len(sys.argv) == 7:
        config = {
            "IP": sys.argv[3],
            "Netmask": sys.argv[4],
            "Gateway": sys.argv[5],
            "DnsServer": sys.argv[6],
            "Method":"Manual"
        }
        setStatic(sys.argv[2],config)
    else:
        usage()
