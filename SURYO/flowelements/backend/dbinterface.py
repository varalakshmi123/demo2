import pymongo
import pymongo.collection
import pymongo.son_manipulator
import utilities
import copy
import time
import traceback
import functools
import bson
import json
import config
import flask
import logger
logger = logger.Logger.getInstance("application").getLogger()

def exceptionHandler(f):
    @functools.wraps(f)
    def handler(*args, **kwargs):
        count = 0
        while True:
            count += 1
            try:
                return f(*args, **kwargs)
            except (pymongo.errors.AutoReconnect, pymongo.errors.ConnectionFailure) as e:
                    time.sleep(5)
                    if count == 5:
                        traceback.print_exc()
                        count = 0
    return handler

class HideFields(pymongo.son_manipulator.SONManipulator):
    def __init__(self, hiddenFields):
        self.hiddenFields = hiddenFields
        self.hiddenFields.extend(["accessors"])

    def transform_outgoing(self, son, collection):
        for field in self.hiddenFields:
            if field in son:
                del son[field]
        return son


class DataFormatter(pymongo.son_manipulator.SONManipulator):
    def transform_outgoing(self, son, collection):
        if "_id" in son:
            son["_id"] = str(son["_id"]) #Change the ObjectId to str
        return son


class MongoCollection(pymongo.collection.Collection):
    Hierarchy = "hierarchy"
    def __init__(self, collectionName, hideFields=True, restrictAccess=True, mongoHost=config.mongoHost, mongoDBName=config.databaseName, mode=''):
        self.mongoHost = mongoHost
        self.databaseName = mongoDBName
        self._initConnectionAndDatabase(hideFields)
        self.restrictAccess = restrictAccess
        self.forcePrimary = False
        self.mode = mode 
        if collectionName not in self.databaseConnection.collection_names():
            super(MongoCollection, self).__init__(self.databaseConnection, collectionName)
            self.ensure_index([("accessors", pymongo.ASCENDING)]) #Mandatory to search using multi index
        else:
            super(MongoCollection, self).__init__(self.databaseConnection, collectionName)

        self.partnerId = None

        if restrictAccess and flask.has_request_context() and "sessionData" in flask.session:
            self._setUser(flask.session["sessionData"])

    def getHiddenFields(self):
        return []

    def _setUser(self, userData):
        self.partnerId = userData["partnerId"]
        
        
        
    @exceptionHandler
    def count(self, condition=None):
        if condition:
            return self.find(self._canAccessCondition(self._fixCondition(condition))).count()

    @exceptionHandler
    def groupedCounts(self,field,condition={}):
        results = self.group(key={field:1}, condition=self._canAccessCondition(self._fixCondition(condition)), initial={"count": 0}, reduce=self.reducer)
        return True,results

    @exceptionHandler

    def getAll(self, query={}):
        start = 0
        limit = 0
        mainKey = "executeMemberName"
        processed=False
        #Sample data
        #{"skip":0,"limit":10,"perColConditions":{"resource_list":"test","TicketNbr":"1234"}}
        #{"skip":0,"limit":10,"globalConditions":{"globalsearch":["resource_list","TicketNbr","status_description","Summary"]}}
        if "condition" in query:
            if type(query["condition"])==dict:
                q={}
                for key, value in query["condition"].items():
                    if type(value) is str or type(value) is unicode:
                        regexp=dict()
                      #if key != mainKey:
                        regexp["$regex"]='.*'+value+'.*'
                        regexp['$options']='i'
                        q[key]=regexp
                    else:
                        q[key]=value
                condition=q
            else:
                condition=query["condition"]
            query["spec"] = self._canAccessCondition(self._fixCondition(condition))
            processed=True
            #query["spec"] = self._canAccessCondition(self._fixCondition(query["condition"]))
            del query["condition"]
        if "perColConditions" in query:
            q = {}
            for key, value in query["perColConditions"].items():
                regexp=dict()
                if type(value) is not str and type(value) is not unicode:
                    q[key]=value
                else:
                  #if key != mainKey:
                    regexp["$regex"]='.*'+value+'.*'
                    regexp['$options']='i'
                    q[key]=regexp
            if  query["perColConditions"].has_key(mainKey):
                if "spec" in query and len(query["spec"].keys())>0:
                    query["spec"]={"$and":[query["spec"],self._canAccessCondition(self._fixCondition(q), query["perColConditions"][mainKey] )]}
                else:
                    query["spec"] = self._canAccessCondition(self._fixCondition(q), query["perColConditions"][mainKey] )
            else:
                if "spec" in query and len(query["spec"].keys())>0:
                    query["spec"]={"$and":[query["spec"],self._canAccessCondition(self._fixCondition(q))]}
                else:
                    query["spec"] = self._canAccessCondition(self._fixCondition(q))
            del query["perColConditions"]
            processed=True
        if "globalConditions" in query:
            q = {}
            for key, value in query["globalConditions"].items():
                regexp = {}
                regexp["$regex"]='.*'+key+'.*'
                regexp['$options']='i'
                orExp=[]
                for column in value:
                    orExp.append({column:regexp})
                q["$or"]=orExp
            query["spec"] = self._canAccessCondition(self._fixCondition(q))
            del query["globalConditions"]
            processed=True
        if "timeConditions" in query:
            q = {}
            for key, value in query["timeConditions"].items():
                exp = {"$gte": value[0], "$lte": value[1]}
                q[key] = exp
            if "spec" in query and len(query["spec"].keys())>0:
                query["spec"]={"$and":[query["spec"],self._canAccessCondition(self._fixCondition(q))]}
            else:
                query["spec"] = self._canAccessCondition(self._fixCondition(q))
            # query["spec"] = {"$and":[query["spec"],self._canAccessCondition(self._fixCondition(q))]}
            del query["timeConditions"]
            processed = True
        logger.info(query)
        if not processed:
            query["spec"] = self._canAccessCondition(self._fixCondition({}))

        if "orderBy" in query:
            query["sort"] = []
            for order in query["orderBy"]:
                for k, v in order.items():
                    query["sort"].append((k,v))
            del query["orderBy"]
        #print "Final query: ", query["spec"]

        if "skip" in query:
            start = int(query["skip"])
            del query["skip"]
        if "limit" in query:
            limit = int(query["limit"])
            del query["limit"]

        #if self.forcePrimary:
        #    query['read_preference'] =pymongo.ReadPreference.PRIMARY

        logger.info( "Query: "+ str(query))
        cursor = self.find(**query)
        resp = {}
        #print "cursor.count() ", cursor.count(), search
        cursor.skip(start)
        cursor.limit(limit)
        resp["total"] = cursor.count(False)
        resp["current"] = cursor.count(True)
        resp["data"] = list(cursor)
        return True, resp


    @exceptionHandler
    def get(self, condition):
        #print condition
        #print self._fixCondition(condition)
        #print self._canAccessCondition(self._fixCondition(condition))
        if self.forcePrimary:
            found = self.find_one(self._canAccessCondition(self._fixCondition(condition)), read_preference=pymongo.ReadPreference.PRIMARY)
        else:
            found = self.find_one(self._canAccessCondition(self._fixCondition(condition)))
        if found:
            return True, found
        else:
            return False, "No data found"

    @exceptionHandler
    def delete(self, condition):
        if condition is None or len(condition) == 0:
            return False, "Cannot remove all entries of collection"
        resp = self.remove(self._canAccessCondition(self._fixCondition(condition)))
        if 'err' in resp and resp["err"]:
            return False, resp["err"]
        else:
            return True, "Deleted"

    @exceptionHandler
    def create(self, doc):
        logger.info(type(doc))
        doc["created"] = utilities.getUtcTime()
        doc["updated"] = utilities.getUtcTime()
        doc["machineId"] = utilities.getMachineId()
    
        if "partnerId" not in doc:
            doc["partnerId"] = self.partnerId

        id = str(self.save(doc))
        if id:
            self.forcePrimary = True
            return self.get(id)
        else:
            return False, "Creation failed"

    @exceptionHandler
    def modify(self, condition, doc, multi=False):
        cleanupFields = ["_id", "created", "updated", "partnerId"]
        for field in cleanupFields:
            if field in doc:
                del doc[field]
        doc["updated"] = utilities.getUtcTime()
        logger.info(doc)
        resp = super(MongoCollection, self).update(self._canAccessCondition(self._fixCondition(condition)), {"$set": doc}, multi=multi)
        if 'err' in resp and resp["err"]:
            return False, resp["err"]
        else:
            self.forcePrimary = True
            return self.get(self._canAccessCondition(self._fixCondition(condition)))

    def _canAccessCondition(self, condition={},impKey = None):
        #if user is logged in then only update the condition
        logger.info("imp key")
        logger.info(impKey)
        if self.mode == MongoCollection.Hierarchy:
            #if user is logged in then only update the condition
            if self.userId and self.restrictAccess:
                #If the logged in user is in any of ancestors, ownedBy, createdBy then allow access
                accessCondition = []
                logger.info("inside")
                logger.info(condition)
                if impKey != None:
                     accessCondition.append({"executeMemberName": impKey})
                if flask.session['sessionData']['role'] != 'admin':
                    accessCondition.append({"partnerId": self.partnerId})
                if not "$or" in condition:
                    condition["$or"] = accessCondition
                else:
                    #If existing condition has $or then move that to be $and, case this can happen is when user search in table view or so...
                    condition["$and"] = [{"$or": condition["$or"]}, {"$or": accessCondition}] 
                    del condition["$or"]
            logger.info(condition)  
            return condition
        else:
            if self.partnerId and self.restrictAccess:
               #If the logged in user is in "accessors" then allow access
               if flask.session['sessionData']['role'] != 'admin':
                   accessCondition = [{"partnerId": self.partnerId}]
   
               if not "$or" in condition:
                   if flask.session['sessionData']['role'] != 'admin':
                       condition["partnerId"] = self.partnerId
               else:
                   #If existing condition has $or then move that to be $and, case this can happen is when user search in table view or so...
                   if flask.session['sessionData']['role'] != 'admin':
                       condition["$and"] = [{"$or": condition["$or"]}, {"partnerId": self.partnerId}]
                   del condition["$or"]
            return condition

    @exceptionHandler
    def exists(self, condition):
        return self.find_one(self._fixCondition(condition), read_preference=pymongo.ReadPreference.PRIMARY) is not None

    @exceptionHandler
    def hasAccess(self, id):
        return self.find_one(self._canAccessCondition(self._fixCondition(id))) is not None

    @exceptionHandler
    def _initConnectionAndDatabase(self, hideFields):
        if config.isReplicaSet:
            self.connection = pymongo.MongoReplicaSetClient(self.mongoHost, replicaSet=config.replicaSetName)
        else:
            self.connection = pymongo.MongoClient(self.mongoHost)

        # j: If True block until write operations have been committed to the journal. Ignored if the server is running without journaling.
        self.connection.write_concern = {'j': True}

        self.databaseConnection = self.connection[self.databaseName]
        if hideFields:
            self.databaseConnection.add_son_manipulator(HideFields(self.getHiddenFields()))
        self.databaseConnection.add_son_manipulator(DataFormatter())
        if config.isReplicaSet:
            self.databaseConnection.read_preference = pymongo.ReadPreference.SECONDARY_PREFERRED

    def _fixCondition(self, condition):
            #convert _id from string to ObjectId



        fixed = {}

        if condition is None:
            return fixed
        elif isinstance(condition, str) and bson.objectid.ObjectId.is_valid(condition):

            #If its simple string assuming it to be _id
            fixed["_id"] = bson.objectid.ObjectId(condition)
        elif isinstance(condition, dict):
            #If dict contains _id as string convert to ObjectId
            if "_id" in condition and isinstance(condition["_id"], str) and bson.objectid.ObjectId.is_valid(condition['_id']):
                condition["_id"] = bson.objectid.ObjectId(condition["_id"])
            fixed = condition

        return fixed
