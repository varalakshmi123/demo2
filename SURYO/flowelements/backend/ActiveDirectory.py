import ldap
from api_interface import requestsInterface


def getADSettings():
    ri = requestsInterface()
    data = ri.action("settings", "ad_settings", "get_settings")
    if data['status'] != 'ok':
        return {}
    if 'data' not in data['msg']:
        return {}
    adSettings = data['msg']['data']
    return adSettings


def validateADUser(userName, password):
    adSettings = getADSettings()
    if not bool(adSettings):
        return False

    LDAP_SERVER = "ldap://"+adSettings['activeDirectoryServer']
    LDAP_USERNAME = '%s@%s' % (userName, adSettings['activeDirectoryServer'])
    LDAP_PASSWORD = password
    # base_dn = adSettings['baseDn']
    ldap_filter = 'userPrincipalName=%s@%s' % (userName, adSettings['activeDirectoryServer'])
    attrs = ['memberOf']
    try:
        # build a client
        ldap_client = ldap.initialize(LDAP_SERVER)

        # perform a synchronous bind
        ldap_client.set_option(ldap.OPT_REFERRALS,0)
        ldap_client.simple_bind_s(LDAP_USERNAME, LDAP_PASSWORD)
    except ldap.INVALID_CREDENTIALS:
        ldap_client.unbind()
        return False
    except ldap.SERVER_DOWN:
        return False
    except:
        return False
    ldap_client.unbind()
    return True
