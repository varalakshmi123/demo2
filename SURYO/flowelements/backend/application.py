import json
import os
import pandas
import re
import requests
import sys
import time
import uuid
from datetime import datetime
from flask import request

import config
import dbinterface
import logger
import noauthhandler
from bson.objectid import ObjectId

logger = logger.Logger.getInstance("application").getLogger()


def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)


def getPartnerFilePath():
    return os.path.join(config.commonFilesPath, "theme_templates") + "/"


class Flows(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Flows, self).__init__("flows", hideFields)


class NoAuth(noauthhandler.noAuthHandler):
    def upload(self, id):
        contentDir = os.path.join(config.contentDir, id)
        fname = request.files['file'].filename
        if not os.path.exists(contentDir):
            os.makedirs(contentDir)
        request.files['file'].save(os.path.join(contentDir, fname))
        df = pandas.read_csv(contentDir + '/' + fname)

        jsonname = os.path.splitext(fname)[0]
        jsonname = jsonname + '.json'
        df.to_json(os.path.join(contentDir, jsonname), orient='records')
        return True, df.to_json(orient='records')

    def createFlow(self, id, data):
        data['partnerId'] = "54619c820b1c8b1ff0166dfc"
	status, resp = self.orderFlow(data)
        if status:
            status, data = Flows().create(resp)
            return status, data
        else:
            return status, resp

    def getFlows(self, id):
        status, data = Flows().getAll()
        return status, data

    def modifyFlow(self, id, doc):
        status, msg = self.orderFlow(doc)
        if status:
            status, data = Flows().modify(str(id), doc)
        else:
            return False, msg
        return status, data

    def orderFlow(self, d):
        initialId = None
        reconProcess = None
        for doc in d:
            if doc == 'nodes':
                for node in d['nodes']:
                    if node['name'] == 'Initializer':
                        initialId = node['id']
                        reconProcess = node.get('properties', {}).get('reconProcess')
                        break

        if initialId is None:
            return False, 'Cannot save flows without "Initializer"'
        connections = []
        node = []
        for i, j in enumerate(d['nodes']):
            j['type'] = re.sub(r'[\n]', '', j['type'])
            for index, doc in enumerate(d['connections']):
                if doc['source']['nodeID'] == initialId:
                    connections.append(doc)
                    for nodex, nodedata in enumerate(d['nodes']):
                        if nodedata['id'] == initialId:
                            node.append(nodedata)
                    initialId = doc['dest']['nodeID']
        for nodex, nodedata in enumerate(d['nodes']):
            if nodedata['id'] == initialId:
                node.append(nodedata)

        if reconProcess is None:
            return False, 'Cannot save flows without recon process.'

        d['reconProcess'] = reconProcess
        d['connections'] = connections
        d['nodes'] = node
        return True, d

    def deleteFlow(self, id):
        status, data = Flows().delete({"_id": ObjectId(str(id))})
        return status, data

    def individualNodes(self, id):
        file = open("nodes.html").read()
        return True, file

    def postCheck(self, id, data):
        logger.info(data)
        return True, ""

    def readProgress(self, id):
        f = open(config.progrssFiles + 'progress.json').read()
        logger.info(f)
        return True, f

    def deployFlow(self, flowId, debug=False):
        flowdb = dbinterface.MongoCollection("flows")
        flowData = flowdb.find_one({"_id": ObjectId(flowId)})
        if flowData is None:
            return False, "Invalid flow id"

        for node in flowData['nodes']:
            node['type'] = re.sub(r'[\n]', '', node['type'])

        f = FlowElements.FlowRunner(flowData['nodes'])
        f.run()
        return True, "Manual Test"

    def getRecons(self, id):
        status, data = Recons().getAll()
        return status, data

    def getReconProcess(self, id):
        status, data = ReconProcess().getAll()
        return status, data

    def loadReconFlows(self, id):
        data = list(Flows().find({'reconProcess': id}))
        return True, data


class Recons(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Recons, self).__init__("recons", hideFields)


class ReconProcess(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconProcess, self).__init__("reconProcessDetails", hideFields)


if __name__ == '__main__':
    print NoAuth().loadSources('IMPS Inward')
