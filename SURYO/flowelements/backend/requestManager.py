#*************************************************************************
#
# HashInclude CONFIDENTIAL
# ________________________
#
#  [2010] - [2016] HashInclude Computech Private Limited
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of HashInclude Computech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to HashInclude Computech
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from HashInclude Computech Private Limited.
#*************************************************************************
import time
import json
import requests

try:
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
except Exception as e:
    pass


# requests.packages.urllib3.disable_warnings()
data_headers = {'content-type': 'application/json'}


class requestManager(object):
    def __init__(self, ip="127.0.0.1", userName="admin", password=None, apiKey=None, licenseChecker=None, proxy=None):
        self.ip = ip
        self.userName = userName
        self.password = password
        self.apiKey = apiKey
        self.response = {}
        self.licenseChecker = licenseChecker
        if (self.password is None and self.apiKey is None) and not (ip == "127.0.0.1"):
            raise ValueError("Invalid credentials")

        self.baseurl = "https://%s/api" % self.ip

        self.rs = requests.Session()
        if proxy and len(proxy):
            self.rs.proxies.update(proxy)
        # TODO have to add proxy settings here
        # http://docs.python-requests.org/en/latest/user/advanced/#proxies
        # proxies = {}
        # proxies = {'http': 'http://user:pass@10.10.1.10:3128/'}
        # proxies = {'http://10.20.1.128': 'http://10.10.1.10:5323'}
        self.login()

    def __del__(self):
        try:
            self.logout()
        except Exception as e:
            pass

    def login(self):
        credentials = {}
        credentials["userName"] = self.userName
        if self.password:
            credentials["password"] = self.password
        elif self.apiKey:
            credentials["apiKey"] = self.apiKey
        # Need for licensing server for customer login
        if self.licenseChecker:
            credentials["licenseChecker"] = self.licenseChecker
        self.response = {}
        resp = self.rs.post(self.baseurl, data=json.dumps(credentials), headers=data_headers, verify=False)
        if resp.status_code != 200:
            raise ValueError("Login Failed")
        else:
            self.response = resp.json()

    def logout(self):
        self.rs.post(self.baseurl + "/logout", headers=data_headers, verify=False, timeout=0.1)

    def fissionRequest(self, method, url, data, params=None, timeout=None):
        # Check if session is valid
        resp = self.rs.get(self.baseurl, verify=False)
        if not resp.status_code == 200:
            self.login()

        meth = getattr(self.rs, method, None)
        if meth:
            if data is None:
                if timeout is not None:
                    resp = meth(url, verify=False, params=params, timeout=timeout)
                else:
                    resp = meth(url, verify=False, params=params)
            elif type(data) is not str:
                resp = meth(url, data=json.dumps(data), params=params, headers=data_headers, verify=False)
            else:
                resp = meth(url, data=data, params=params, headers=data_headers, verify=False)

            if resp.status_code == 200:
                return resp.json()
            else:
                return "Error: %s" % (str(resp.status_code))
        else:
            print("Unknown method: %s" % (method))

    def get(self, collection, id=None, params=None):
        url = self.baseurl + "/" + collection
        if id is not None:
            url = url + "/" + id
        return self.fissionRequest("get", url, None, params=params)

    def post(self, collection, data=None):
        url = self.baseurl + "/" + collection
        return self.fissionRequest("post", url, data)

    def put(self, collection, id, data=None):
        url = self.baseurl + "/" + collection + "/" + id
        return self.fissionRequest("put", url, data)

    def delete(self, collection, id, data=None):
        url = self.baseurl + "/" + collection + "/" + id
        return self.fissionRequest("delete", url, data)

    def action(self, collection, id, actionName, data=None, timeout=None):
        url = self.baseurl + "/" + collection + "/" + id + "/" + actionName
        return self.fissionRequest("post", url, data, timeout=timeout)
