import os
import sys
import json
import pyclbr
import config
import glob
# from bs4 import BeautifulSoup
import re
import copy

sys.path.append('/usr/share/nginx/www/flowelements/backend/smartrecon')

class ListAndDictCheck:
    def __init__(self,htmlheader):
        self.htmlheader=htmlheader

    def list_check(self,properties,model):
        s=model

        for index, v in enumerate(properties):
            s = s + '[' + str(index) + ']'
            if type(v) is dict:

                self.dict_check(v, s)
            elif type(v) is list:
                self.list_check(v,s)

            else:
                data = {'property': s, 'propPattern': s.capitalize()}

                self.htmlheader += formloop.format(**data)
                # print 'ffdd'

        # else:
        #     data = {'property': model, 'propPattern': model.capitalize()}
        #     print dir(self)
        #     # self.htmlheader.format(**data)
        #
        #     self.htmlheader += formloop.format(**data)
        #     print 'ffdd'


    def dict_check(self,properties,model):
        # if len(properties)>0:
        for k, v in properties.items():

            s = model
            s = s + '.' + k

            if type(v) is dict:
                self.dict_check(v,s)
            elif type(v) is list:
                self.list_check(v,s)

            if v:
                continue

            data = {'property': s, 'propPattern': k.capitalize()}

            self.htmlheader += formloop.format(**data)
            # print 'ffdd'
        # else:
        #     data = {'property': model + '.' + properties, 'propPattern': properties.capitalize()}
        #     self.htmlheader += formloop.format(**data)
        #     print 'ffdd'

        # data = {'className': className, 'property': k, 'propPattern': k.capitalize()}
        # print self.htmlheader
        return self.htmlheader

    def split_uppercase(s):
            r = []
            l = False
            data = []
            for c in s:
                # l being: last character was not uppercase
                if l and c.isupper():
                    data.append(''.join(r).title())
                    r = []
                l = not c.isupper()
                r.append(c)
            return ' '.join(data)


NodesSpec = []
header = """<script type="text/ng-template" id="{className}.html">\n <div ng-app="myapp" ng-controller="{ctrlName}">\n \
        <div class="editor-tray ui-draggable" style="width: 500px; right: 0px; transition: right 0.25s ease;">\n\
            <div class="editor-tray-header">\n\
                <div class="editor-tray-titlebar">\n\
                    <ul class="editor-tray-breadcrumbs">\n\
                        <li>Edit {ctrlName}</li>\n\
                    </ul>\n\
                </div>\n\
                <div class="editor-tray-toolbar">\n\
                    <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only leftButton" role="button" aria-disabled="false" id="node-dialog-delete" ng-click="close('delete')"> Delete </button>\n\
                    <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" ng-click="close('cancel')"aria-disabled="false">Cancel</button>\n\
                    <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only primary" role="button" aria-disabled="false" ng-click="close('done');save({className});">Done</button>\n\
                </div>\n\
            </div>\n"""
footer = """<div class="editor-tray-footer"></div> <div class="editor-tray-resize-handle"></div>\n </div>\n </div>\n </script>"""
formBody = """<div class="editor-tray-body-wrapper" style="min-width: 460px; height: 100%;">\n 
                          <div class="editor-tray-body">\n
                              <form class="form-horizontal" autocomplete="off">\n"""
formloop = """<div class="form-row">\n
               <label><i class="fa fa-tag"></i> <span>{propPattern}</span></label>\n
               <input type="text" ng-model="{property}"dir="" placeholder="{propPattern}">\n
           </div>\n"""

formfooter = """</form></div></div>\n"""

if __name__ == "__main__":
    nodesModules = ['FlowElements']
    cwd = os.getcwd()
    sysPath = sys.path

    if cwd in sysPath:
        sysPath.remove(cwd)
    sys.path = sysPath
    os.chdir('/usr/share/nginx/www/flowelements/backend/smartrecon')
    if True:
        module = 'FlowElements'
        moduleInstance = __import__(module)
        moduleClasses = pyclbr.readmodule(module)
        for klassName, klassSpec in moduleClasses.items():
            classInstance = getattr(moduleInstance, klassName)()
            properties = dict()

            try:

                if 'properties' in dir(classInstance):
                    # properties = classInstance.properties.keys()
                    for key in classInstance.properties.keys():
                        # properties[key]=mapper(classInstance.properties[key])
                        # if type(classInstance.properties[key]) is dict :
                        # 	properties[key]=rec(classInstance.properties[key],properties,key)
                        properties = classInstance.properties

                else:
                    properties = {}

                if issubclass(getattr(moduleInstance, klassName), getattr(moduleInstance, 'FlowElement')):
                    NodesData = {'className': klassName, 'name': klassName, 'properties': properties}
                    NodesSpec.append(NodesData)

            except Exception as e:
                print e
                pass

    f = open("%s/Nodes.json" % cwd, "w")
    f.write(json.dumps(NodesSpec, indent=4))
    # /usr/share/nginx/www/flowelements/ui/index_bkp.html
    file = open('/usr/share/nginx/www/flowelements/ui/index_bkp.html', 'r+')
    indexData = file.readlines()
    indexData = indexData[:-1]
    # print indexData
    f.close()
    check = ListAndDictCheck(formloop)
    for nodes in NodesSpec:
        htmldata = []
        jsonData = []
        htmlHeader=''
        # print nodes['className']
        data = {'className': nodes['className'], 'ctrlName': nodes['className'] + 'Ctrl'}
        htmlHeader = header.format(**data)

        form = copy.deepcopy(formBody)
        htmlHeader += '\n'
        htmlHeader += formBody

        #     print property
        htmlHeader+= check.dict_check(nodes['properties'],nodes['className'])
        # print nodes['className']
        # print check.dict_check(nodes['properties'],nodes['className'])
        check.htmlheader = ''
        # print 'check'

        htmlHeader += formfooter
        htmlHeader += footer
        htmldata.append(htmlHeader)
        f = open('/usr/share/nginx/www/flowelements/backend/smartrecon/scriptTemplate.js')
        data = f.read()
        f.close()
        JsData = data.replace('{className}', nodes['className'])
        JsData = JsData.replace('{ctrlName}', nodes['className'] + 'Ctrl')
        jsonData.append(JsData)
        indexData.extend(htmldata)
        indexData.extend(jsonData)

    indexData.extend('</body>')
    file.truncate(0)
    file.writelines(indexData)
    file.close()
