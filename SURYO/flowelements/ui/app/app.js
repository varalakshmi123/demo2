var app = angular.module('flowelementsapp', ['flowChart', 'ui.multiselect', 'isteven-multi-select', 'ui.bootstrap', 'schemaForm', 'ui.select']);
app.controller('mainCtrl', function ($scope, $rootScope, $sce, $http, $log, $timeout, $uibModal, NoAuthService) {
    $rootScope.infoTabs = 'info'
    $rootScope.openEditor = false
    $rootScope.showProperties = false
    $rootScope.availableColSets = {}
    $rootScope.availableFiltersSets = {}
    $rootScope.availableNewColumns = {}

    function getID() {
        return (Math.floor(Math.random() * 1000000000)).toString()
    }

    $scope.shadeCheck = function () {
        if ($rootScope.openEditor) {
            return {'display': 'block'}
        }
        else {
            return {'display': 'none'}
        }
    }
    function getRandomColor() {
        var letters = 'BCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * letters.length)];
        }
        return color;
    }

    $rootScope.showSuccessMsg = function (msg) {
        toastr.success(msg)
    }
    $rootScope.showErrorMsg = function (msg) {
        toastr.error(msg)
    }
    $rootScope.showWarningmsg = function (msg) {
        toastr.warning(msg)
    }

    $rootScope.flowelements = [{'reconName': 'flow1', 'active': true, 'Id': getID()}]
    $rootScope.activeFlow = $rootScope.flowelements[0]
    $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
    $scope.getChartVariable = function () {
        return $rootScope[$rootScope.activeFlow['Id']]
    }


    var chart = $("#chartSvg");
    var chartOffset = chart.offset();
    var chartSVG = $("#chartSvg>svg").get(0);
    var activeSpliceLink;
    var mouseX;
    var mouseY;
    var spliceTimer;
    $http.get('/app/json/nodes.json').success(function (data, status, headers, config) {
        angular.forEach(data, function (a, b) {
            a['color'] = getRandomColor()
        })
        $rootScope.nodesData = data
    })
    $rootScope.getProperties = function (name) {
        var ret = []
        angular.forEach($rootScope.nodesData, function (a, b) {
            if (a['name'] === name) {
                ret = a['properties']
            }
        })
        return angular.copy(ret)
    }
    $timeout(function () {
        angular.forEach($rootScope.nodesData, function (a, b) {
            $('#palette_node_' + a['name']).draggable({
                helper: 'clone',
                appendTo: 'body',
                revert: true,
                revertDuration: 50,
                containment: '#main-container',
                start: function () {
                    $("#chartSvg").focus();
                },
                stop: function () {
                    // d3.select('.link_splice').classed('link_splice', false);
                    if (spliceTimer) {
                        clearTimeout(spliceTimer);
                        spliceTimer = null;
                    }
                },
                drag: function (e, ui) {
                }
            });
        })
    }, 1500)
    var space_width = 5000,
        space_height = 5000
    $rootScope.scaleFactor = 1
    $rootScope.svgWidth = space_width
    $rootScope.svgHeight = space_height
    $rootScope.zoomIn = function () {
        if ($rootScope.scaleFactor < 2) {
            $rootScope.scaleFactor += 0.1;
            $rootScope.svgWidth = space_width * $rootScope.scaleFactor
            $rootScope.svgHeight = space_height * $rootScope.scaleFactor
        }
    }
    $rootScope.zoomOut = function () {
        if ($rootScope.scaleFactor > 0.3) {
            $rootScope.scaleFactor -= 0.1;
            $rootScope.svgWidth = space_width * $rootScope.scaleFactor
            $rootScope.svgHeight = space_height * $rootScope.scaleFactor
        }
    }
    $rootScope.zoomZero = function () {
        $rootScope.scaleFactor = 1;
        $rootScope.svgWidth = space_width * $rootScope.scaleFactor
        $rootScope.svgHeight = space_height * $rootScope.scaleFactor
    }
    function rgbToHex(a) {
        a = a.replace(/[^\d,]/g, "").split(",");
        return "#" + ((1 << 24) + (+a[0] << 16) + (+a[1] << 8) + +a[2]).toString(16).slice(1)
    }

    $("#chartSvg").droppable({
        accept: ".palette_node",
        drop: function (event, ui) {
            var selected_tool = ui.draggable[0].innerText;
            var selected_color = $("#palette_node_" + selected_tool).css("background-color");
            var dropPositionX = event.pageX - $(this).offset().left;
            var dropPositionY = event.pageY - $(this).offset().top;
            // Get mouse offset relative to dragged item:
            var dragItemOffsetX = event.offsetX;
            var dragItemOffsetY = event.offsetY;
            // Get position of dragged item relative to drop target:
            var dragItemPositionX = dropPositionX - dragItemOffsetX;
            var dragItemPositionY = dropPositionY - dragItemOffsetY;
            var newNodeDataModel = {
                name: selected_tool,
                type: selected_tool,
                id: getID(),
                color: rgbToHex(selected_color),
                x: dragItemPositionX,
                y: dragItemPositionY,
                inputConnectors: [
                    {
                        name: "X"
                    }
                ],
                outputConnectors: [
                    {
                        name: "1"
                    }
                ]
            };
            $rootScope[$rootScope.activeFlow['Id']].addNode(newNodeDataModel);
        }
    })

    $rootScope.editFlow = function (index) {
        $rootScope.openEditor = true
        $scope.selectedFlow = $rootScope.flowelements[index]
        $rootScope.dynamicTemplate = 'editFlow.html'
        $scope.saveFlowName = function (name) {
            $rootScope.flowelements[index]['reconName'] = name
            $rootScope.openEditor = false
        }
        $scope.cancel = function () {
            $rootScope.openEditor = false
        }
        $scope.deleteFlow = function () {
            $rootScope.removeFlow(index)
            $rootScope.openEditor = false
        }
    }
    $rootScope.addFlow = function () {
        if ($rootScope.flowelements.length != 0) {
            $rootScope.flowelements.push({
                'reconName': 'flow' + ($rootScope.flowelements.length + 1),
                'active': false,
                'Id': getID()
            })
            console.log($rootScope.flowelements)
        }
    }

    $rootScope.setActiveFlow = function (index) {
        angular.forEach($rootScope.flowelements, function (a, b) {
            a['active'] = false
        })
        $rootScope.activeFlow = $rootScope.flowelements[index]
        if (angular.isDefined($rootScope[$rootScope.activeFlow['Id']])) {
        }
        else {
            $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
        }
        $rootScope.flowelements[index]['active'] = true
    }

    $rootScope.removeFlow = function (index) {
        $uibModal.open({
            templateUrl: 'deleteFlow.html',
            windowClass: 'modal',
            backdrop: 'static',
            controller: function ($rootScope, $uibModalInstance) {
                $rootScope.submit = function () {
                    if ($rootScope.flowelements.length > 1) {
                        if (angular.isDefined($rootScope.flowelements[index]['_id'])) {
                            NoAuthService.deleteFlow($rootScope.flowelements[index]['_id'], function (data) {
                                $rootScope.showSuccessMsg("Flow removed succesfully")
                                $rootScope.getAllFlows()
                                $rootScope.cancel()
                            })
                        } else {
                            if ($rootScope.flowelements[index]['active'] === true) {
                                $rootScope.flowelements[index - 1]['active'] = true
                                $rootScope.activeFlow = $rootScope.flowelements[index - 1]
                            }
                            $rootScope.flowelements.splice(index, 1)
                            $rootScope.cancel()
                        }
                    }
                    else {
                        $rootScope.showWarningmsg("Permission Denied")
                        $rootScope.cancel()
                    }
                };
                $rootScope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
    }

    $rootScope.closeModal = function (type) {
        if (type === 'delete') {
            $rootScope[$rootScope.activeFlow['Id']].deleteSelected();
            $rootScope.openEditor = false
        }
        else if (type === 'cancel') {
            $rootScope.openEditor = false
        }
        else if (type === 'done') {
            $rootScope.openEditor = false
        }
    }
    $scope.getNodeIndex = function (id) {
        var ret = ''
        angular.forEach($rootScope[$rootScope.activeFlow['Id']].data['nodes'], function (a, b) {
            if (a['id'] === id) {
                ret = b
            }
        })
        return ret
    }
    $rootScope.saveNodeProperties = function (data, type) {
        var index = $scope.getNodeIndex(type)
        data = data || {}
        $rootScope[$rootScope.activeFlow['Id']]['data']['nodes'][index]['properties'] = data

        // add result keys to be used by other elements.
        if (angular.isDefined(data['resultkey']) && $rootScope.resultKeys.indexOf(data['resultkey']) == -1) {
            $rootScope.resultKeys.push(data['resultkey'])
        }
    }

    $rootScope.openModal = function (node) {
        $rootScope.selectedNode = node
        node['name'] = node['name'].replace(/(\r\n|\n|\r)/gm, "");
        $rootScope.dynamicTemplate = node['name'] + '.html'
        console.log($rootScope.dynamicTemplate)
        $rootScope.openEditor = true
        $rootScope.$broadcast(node['name'], getID());
        // $rootScope.$emit(node['name'], getID());
    }
    $scope.getClassName = function (name) {
        name = name.replace(/(\r\n|\n|\r)/gm, "");
        var ret = ''
        angular.forEach($rootScope.nodesData, function (a, b) {
            if (a['name'] === name) {
                ret = a['className']
            }
        })
        return ret
    }

    $rootScope.saveFlow = function () {
        var data = {
            'reconName': $rootScope.activeFlow['reconName'],
            'Id': $rootScope.activeFlow['Id'],
            'nodes': $rootScope[$rootScope.activeFlow['Id']]['data']['nodes'],
            'connections': $rootScope[$rootScope.activeFlow['Id']]['data']['connections']
        }
        angular.forEach(data['nodes'], function (a, b) {
            a['className'] = $scope.getClassName(a['name'])
        })

        if (angular.isDefined($rootScope.activeFlow['_id'])) {
            NoAuthService.modifyFlow($rootScope.activeFlow['_id'], data, function (data) {
                $rootScope.showSuccessMsg("Flow updated Successfully")
            })
        }
        else {
            NoAuthService.createFlow('dummy', data, function (dataR) {
                $rootScope.showSuccessMsg("Flow created Successfully")
            }, function (err) {
                $rootScope.showWarningmsg(err.msg)
            })
        }
        $rootScope.loadRecons()
    }
    $rootScope.activeNext = function (index) {
        console.log($rootScope.activeFlow)
        angular.forEach(flow in flowelements)
        if ($rootScope.activeFlow == flow)
            $rootScope.flowelements($index)

    }
    $rootScope.activePrevious = function (index) {
        console.log(index)
    }


    $rootScope.deployStatus = ''
    $rootScope.deployFlow = function () {
        $rootScope.deployStatus = 'started'
        NoAuthService.deployFlow($rootScope.activeFlow['_id'], function (data) {
            console.log(data)
            $rootScope.deployStatus = ''
            $rootScope.showSuccessMsg("Flow Executed Succesfully")
            $rootScope.getProgress()
        }, function (err) {
            $rootScope.deployStatus = ''
            $rootScope.showErrorMsg(err.msg)
            $rootScope.getProgress()
        })
    }

    $rootScope.getNodesHtml = function () {
        NoAuthService.individualNodes('dummy', function (data) {
            $rootScope.nodesHtml = data
        })
    }


    // watch for current active flow & initialize source data for existing process
    $rootScope.$watch('activeFlow', function (oldval, newval) {
        $rootScope.resultKeys = []
        if (angular.isDefined($rootScope.activeFlow['nodes'])) {

            angular.forEach($rootScope.activeFlow['nodes'], function (node, k) {
                if (node['type'] == 'Initializer' && angular.isDefined(node['properties']['reconName'])) {
                    $rootScope.loadLicensedRecons(node['properties']['reconName'])
                }
                if (angular.isDefined(node['properties']) && angular.isDefined(node['properties']['resultkey'])) {
                    $rootScope.resultKeys.push(node['properties']['resultkey'])
                }
            })
        } else {
            $rootScope.loadLicensedRecons()
        }
    })

    $rootScope.loadRecons = function (reconProcess) {

        // retains recon process on refresh view
        if (angular.isDefined(reconProcess)) {
            if (angular.isUndefined($rootScope.reconProcess) || ($rootScope.reconProcess != reconProcess)) {
                $rootScope.reconProcess = reconProcess
            }
        }

        NoAuthService.loadReconFlows($rootScope.reconProcess['reconProcess'], function (data) {
            $rootScope.flowelements = []
            $rootScope.activeFlow = {}
            if (data['total'] == 0) {
                $rootScope.showWarningmsg('No data found.')
            } else {
                $rootScope.flowelements = data
                angular.forEach($rootScope.flowelements, function (a, b) {
                    a['active'] = false
                    a['connections'] = (angular.isDefined(a['connections'])) ? a['connections'] : []
                    $rootScope[a['Id']] = new flowchart.ChartViewModel({
                        'nodes': a['nodes'],
                        'connections': a['connections']
                    });
                })
                $rootScope.activeFlow = $rootScope.flowelements[0]
                $rootScope.flowelements[0]['active'] = true
            }
        }, function (err) {
            $rootScope.showErrorMsg(err.msg)
        })
    }

    // initialize recon & recon process details.
    $rootScope.loadLicensedRecons = function (defaultRecon) {
        NoAuthService.getRecons(undefined, function (data) {
            $rootScope.reconLookup = {}
            if (data['total'] > 0) {
                $rootScope.reconsData = angular.copy(data['data']) // use this data for getting source ref instead of API
                $rootScope.reconIdx = {} // use to store position of recon  by recon name in masterData for fast access later
                angular.forEach(data['data'], function (v, k) {
                    if (!angular.isDefined($rootScope.reconLookup[v['reconProcess']])) {
                        $rootScope.reconLookup[v['reconProcess']] = []
                    }
                    $rootScope.reconLookup[v['reconProcess']].push(v['reconName'])
                    $rootScope.reconIdx[v['reconName']] = k
                })
            } else {
                //$rootScope.showErrorMsg('No data found.')
            }

            if (angular.isDefined(defaultRecon)) {
                $rootScope.initSourceRef(defaultRecon)
            }
        }, function (err) {
            console.log(err.msg)
            $rootScope.showErrorMsg(err.msg)
        })
    }

    $rootScope.initSourceRef = function (reconName) {
        var sources = $rootScope.reconsData[$rootScope.reconIdx[reconName]]['sources']
        $rootScope.sourceRefMapping = {}
        $rootScope.resultKeys = []
        var tmp = {}

        sourceList=Object.keys(sources)

        angular.forEach(sourceList,function(val,index)
        {
            angular.forEach(sources[val], function (struct, src) {

                tmp[val] = struct['keyColumns']

            // add sources to result keys
            if ($rootScope.resultKeys.indexOf(val) == -1) {
                $rootScope.resultKeys.push(val)
                $rootScope.resultKeys.push('results.' + val)
            }
        })

        })



        angular.forEach(tmp, function (def, src) {
            $rootScope.sourceRefMapping[src] = {"matchColumns": [], "sumColumns": []}
            angular.forEach(def, function (v, k) {
                if (v['DataType'] == 'float') {
                    $rootScope.sourceRefMapping[src]['sumColumns'].push(v['keyCols'])
                }
                $rootScope.sourceRefMapping[src]['matchColumns'].push(v['keyCols'])
            })
        })
    }

    $rootScope.loadReconProcess = function () {
        NoAuthService.getReconProcess('dummy', function (data) {
            if (data['total'] == 0) {
                $rootScope.showWarningmsg('Recon Process not defined.')
            }

            $rootScope.reconProcessList = data['data']
        }, function (err) {
            $rootScope.showErrorMsg(err.msg)
        })
    }

    $rootScope.loadReconProcess()

});