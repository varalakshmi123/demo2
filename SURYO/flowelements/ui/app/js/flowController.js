/**
 * Created by dinesh on 21/7/17.
 */
app.controller('flowController', function ($scope, $rootScope, $http, $log, $timeout, $uibModal, NoAuthService) {
    $log.info('In MainCtrl');
    $rootScope.infoTabs = 'info'
    $rootScope.openEditor = false
    $rootScope.showProperties = false
    $rootScope.availableColSets = {}
    function getID() {
        return (Math.floor(Math.random() * 1000000000)).toString()
    }

    $scope.shadeCheck = function () {
        if ($rootScope.openEditor) {
            return {'display': 'block'}
        }
        else {
            return {'display': 'none'}
        }
    }
    function getRandomColor() {
        var letters = 'BCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * letters.length)];
        }
        return color;
    }

    $rootScope.showSuccessMsg = function (msg) {
        toastr.success(msg)
    }
    $rootScope.showErrorMsg = function (msg) {
        toastr.error(msg)
    }
    $rootScope.showWarningmsg = function (msg) {
        toastr.warning(msg)
    }

    $rootScope.flowelements = [{'flowName': 'flow1', 'active': true, 'Id': getID()}]
    $rootScope.activeFlow = $rootScope.flowelements[0]
    $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
    $scope.getChartVariable = function () {
        return $rootScope[$rootScope.activeFlow['Id']]
    }
    var chart = $("#chartSvg");
    var chartOffset = chart.offset();
    var chartSVG = $("#chartSvg>svg").get(0);
    var activeSpliceLink;
    var mouseX;
    var mouseY;
    var spliceTimer;
    $http.get('/app/json/nodes.json').success(function (data, status, headers, config) {
        angular.forEach(data, function (a, b) {
            a['color'] = getRandomColor()
        })
        $rootScope.nodesData = data
    })
    $rootScope.getProperties = function (name) {
        var ret = []
        angular.forEach($rootScope.nodesData,function (a,b) {
            if (a['name'] === name){
                ret = a['properties']
            }
        })
        return ret
    }
    $timeout(function () {
        angular.forEach($rootScope.nodesData, function (a, b) {
            $('#palette_node_' + a['name']).draggable({
                helper: 'clone',
                appendTo: 'body',
                revert: true,
                revertDuration: 50,
                containment: '#main-container',
                start: function () {
                    $("#chartSvg").focus();
                },
                stop: function () {
                    // d3.select('.link_splice').classed('link_splice', false);
                    if (spliceTimer) {
                        clearTimeout(spliceTimer);
                        spliceTimer = null;
                    }
                },
                drag: function (e, ui) {
                }
            });
        })
    }, 1500)
    var space_width = 5000,
        space_height = 5000
    $rootScope.scaleFactor = 1
    $rootScope.svgWidth = space_width
    $rootScope.svgHeight = space_height
    $rootScope.zoomIn = function () {
        if ($rootScope.scaleFactor < 2) {
            $rootScope.scaleFactor += 0.1;
            $rootScope.svgWidth = space_width * $rootScope.scaleFactor
            $rootScope.svgHeight = space_height * $rootScope.scaleFactor
        }
    }
    $rootScope.zoomOut = function () {
        if ($rootScope.scaleFactor > 0.3) {
            $rootScope.scaleFactor -= 0.1;
            $rootScope.svgWidth = space_width * $rootScope.scaleFactor
            $rootScope.svgHeight = space_height * $rootScope.scaleFactor
        }
    }
    $rootScope.zoomZero = function () {
        $rootScope.scaleFactor = 1;
        $rootScope.svgWidth = space_width * $rootScope.scaleFactor
        $rootScope.svgHeight = space_height * $rootScope.scaleFactor
    }
    function rgbToHex(a) {
        a = a.replace(/[^\d,]/g, "").split(",");
        return "#" + ((1 << 24) + (+a[0] << 16) + (+a[1] << 8) + +a[2]).toString(16).slice(1)
    }

    $("#chartSvg").droppable({
        accept: ".palette_node",
        drop: function (event, ui) {
            var selected_tool = ui.draggable[0].innerText;
            var selected_color = $("#palette_node_" + selected_tool).css("background-color");
            var dropPositionX = event.pageX - $(this).offset().left;
            var dropPositionY = event.pageY - $(this).offset().top;
            // Get mouse offset relative to dragged item:
            var dragItemOffsetX = event.offsetX;
            var dragItemOffsetY = event.offsetY;
            // Get position of dragged item relative to drop target:
            var dragItemPositionX = dropPositionX - dragItemOffsetX;
            var dragItemPositionY = dropPositionY - dragItemOffsetY;
            var newNodeDataModel = {
                name: selected_tool,
                type: selected_tool,
                id: getID(),
                color: rgbToHex(selected_color),
                x: dragItemPositionX,
                y: dragItemPositionY,
                inputConnectors: [
                    {
                        name: "X"
                    }
                ],
                outputConnectors: [
                    {
                        name: "1"
                    }
                ]
            };
            $rootScope[$rootScope.activeFlow['Id']].addNode(newNodeDataModel);
            // alert('DROPPED IT AT ' + dragItemPositionX + ', ' + dragItemPositionY);
        }
    })

    $rootScope.editFlow = function (index) {
        $rootScope.openEditor = true
        $scope.selectedFlow = $rootScope.flowelements[index]
        $rootScope.dynamicTemplate = 'editFlow.html'
        $scope.saveFlowName = function (name) {
            $rootScope.flowelements[index]['flowName'] = name
            $rootScope.openEditor = false
        }
        $scope.cancel = function () {
            $rootScope.openEditor = false
        }
        $scope.deleteFlow = function () {
            $rootScope.removeFlow(index)
            $rootScope.openEditor = false
        }
    }
    $rootScope.addFlow = function () {
        if ($rootScope.flowelements.length <= 2) {
            $rootScope.flowelements.push({
                'flowName': 'flow' + ($rootScope.flowelements.length + 1),
                'active': false,
                'Id': getID()
            })
        }
    }

    $rootScope.setActiveFlow = function (index) {
        angular.forEach($rootScope.flowelements, function (a, b) {
            a['active'] = false
        })
        $rootScope.activeFlow = $rootScope.flowelements[index]
        if (angular.isDefined($rootScope[$rootScope.activeFlow['Id']])) {
        }
        else {
            $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
        }
        $rootScope.flowelements[index]['active'] = true
    }
    $rootScope.removeFlow = function (index) {
        $uibModal.open({
            templateUrl: 'deleteFlow.html',
            windowClass: 'modal',
            backdrop: 'static',
            controller: function ($rootScope, $uibModalInstance) {
                $rootScope.submit = function () {
                    if ($rootScope.flowelements.length > 1) {
                        if (angular.isDefined($rootScope.flowelements[index]['_id'])) {
                            NoAuthService.deleteFlow($rootScope.flowelements[index]['_id'], function (data) {
                                $rootScope.showSuccessMsg("Flow removed succesfully")
                                $rootScope.getAllFlows()
                                $rootScope.cancel()
                            })
                        } else {
                            if ($rootScope.flowelements[index]['active'] === true) {
                                $rootScope.flowelements[index - 1]['active'] = true
                                $rootScope.activeFlow = $rootScope.flowelements[index - 1]
                            }
                            $rootScope.flowelements.splice(index, 1)
                            $rootScope.cancel()
                        }
                    }
                    else {
                        $rootScope.showWarningmsg("Permission Denied")
                        $rootScope.cancel()
                    }
                };
                $rootScope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
    }


    $rootScope.closeModal = function (type) {
        if (type === 'delete') {
            $rootScope[$rootScope.activeFlow['Id']].deleteSelected();
            $rootScope.openEditor = false
        }
        else if (type === 'cancel') {
            $rootScope.openEditor = false
        }
        else if (type === 'done') {
            $rootScope.openEditor = false
        }
    }
    $scope.getNodeIndex = function (type) {
        var ret = ''
        angular.forEach($rootScope[$rootScope.activeFlow['Id']].data['nodes'], function (a, b) {
            if (a['name'] === type) {
                ret = b
            }
        })
        return ret
    }

    $rootScope.saveNodeProperties = function (data, type) {
        var index = $scope.getNodeIndex(type)
        $rootScope[$rootScope.activeFlow['Id']]['data']['nodes'][index]['properties'] = data
        console.log($rootScope[$rootScope.activeFlow['Id']])
    }

    $rootScope.openModal = function (node) {
        $rootScope.selectedNode = node
        node['name'] = node['name'].replace(/(\r\n|\n|\r)/gm, "");
        $rootScope.dynamicTemplate = node['name'] + '.html'
        console.log($rootScope.dynamicTemplate)
        $rootScope.openEditor = true
        $rootScope.$broadcast(node['name'], getID());
        // $rootScope.$emit(node['name'], getID());
    }
    $scope.getClassName = function (name) {
        name = name.replace(/(\r\n|\n|\r)/gm, "");
        var ret = ''
        angular.forEach($rootScope.nodesData, function (a, b) {
            if (a['name'] === name) {
                ret = a['className']
            }
        })
        return ret
    }

    $rootScope.saveFlow = function () {
        var data = {
            'flowName': $rootScope.activeFlow['flowName'],
            'Id': $rootScope.activeFlow['Id'],
            'nodes': $rootScope[$rootScope.activeFlow['Id']]['data']['nodes'],
            'connections': $rootScope[$rootScope.activeFlow['Id']]['data']['connections']
        }
        angular.forEach(data['nodes'], function (a, b) {
            a['className'] = $scope.getClassName(a['name'])
        })
        if (angular.isDefined($rootScope.activeFlow['_id'])) {
            NoAuthService.modifyFlow($rootScope.activeFlow['_id'], $rootScope.activeFlow, function (data) {
                console.log(data)
                $rootScope.showSuccessMsg("Flow Updated Succesfully")
            })
        }
        else {
            NoAuthService.createFlow('dummy', data, function (data) {
                console.log(data)
                $rootScope.showSuccessMsg("Flow Created Succesfully")
            })
        }
    }
    $rootScope.deployStatus = ''
    $rootScope.deployFlow = function () {
        $rootScope.deployStatus = 'started'
        NoAuthService.deployFlow($rootScope.activeFlow['_id'], function (data) {
            console.log(data)
            $rootScope.deployStatus = ''
            $rootScope.showSuccessMsg("Flow Executed Succesfully")
        },function (err) {
            $rootScope.deployStatus = ''
            $rootScope.showErrorMsg(err.msg)
        })
    }
    $rootScope.getAllFlows = function () {
        $http.get("api/no_auth/dummy/getFlows", {
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function (data, status) {
            console.log(data)
            if (data.msg['data'].length > 0) {
                $rootScope.flowelements = data['msg']['data']
                angular.forEach($rootScope.flowelements, function (a, b) {
                    a['active'] = false
                    a['connections'] = (angular.isDefined(a['connections'])) ? a['connections'] : []
                    $rootScope[a['Id']] = new flowchart.ChartViewModel({
                        'nodes': a['nodes'],
                        'connections': a['connections']
                    });
                })
                $rootScope.activeFlow = $rootScope.flowelements[0]
                $rootScope.flowelements[0]['active'] = true
            }
            else {
                $rootScope.flowelements = [{'flowName': 'flow1', 'active': true, 'Id': getID()}]
                $rootScope.activeFlow = $rootScope.flowelements[0]
                $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
            }

        }).error(function (error, msg) {
            console.log(error)
        });
    }
    $rootScope.getAllFlows()
    $rootScope.$watch('displayNodeInfo',function (oldval,newval) {

    })
    $rootScope.$watch('activeFlow',function (oldval,newval) {
        $rootScope.availableColSets = {}
        angular.forEach(oldval['nodes'],function (a,b) {
            if (a['name'] === 'CSVFlow' || a['name'] === 'NeftExt'){
                $rootScope.availableColSets[a['properties']['resultkey']] = a['properties']['dfCols']
            }
        })
    })
});