var myapp = angular.module('flowelementsapp');
myapp.directive('preloaderdirective', ['$scope', '$rootScope', '$http',
    function ($scope, $rootScope, $http) {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        templateUrl: 'app/partials/preloader.html',
        link: function ($scope, element, attrs) {

            $scope.feedFilters = []
            $scope.close = function (type) {
                $rootScope.closeModal(type)
            }

            $rootScope.$on("PreLoader", function (evt, data) {
                $scope.feedFilters = undefined;
                angular.forEach($rootScope[$rootScope.activeFlow['Id']]['data']['nodes'], function (a, b) {
                    console.log($rootScope[$rootScope.activeFlow['Id']]['data']['nodes'])
                    if (a['id'] === $rootScope.selectedNode['id']) {
                        $scope.PreLoader = angular.copy(a['properties'])
                        if (angular.isDefined($scope.PreLoader.feedFilters)) {
                            $scope.feedFilters = $scope.PreLoader.feedFilters
                        }
                    }
                })
                console.log($scope.PreLoader)
            })
            $rootScope.$broadcast('PreLoader', 'CSVFIRST');



            $scope.addfilter = function (data) {
                if (!angular.isDefined($scope.feedFilters)) {
                    $scope.feedFilters = []
                }
                $scope.feedFilters.push(data)
                $scope.PreLoader.feedFilters = ''

            }

            $scope.save = function (data) {
                console.log(data)
                data['feedFilters'] = $scope.feedFilters
                $rootScope.availableColSets[data['resultkey']] = data['dfCols']
                $rootScope.saveNodeProperties(data, angular.copy($rootScope.selectedNode['id']))
            }
        }
    }
}]);
