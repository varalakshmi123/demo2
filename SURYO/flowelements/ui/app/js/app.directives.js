/**
 * Created by dinesh on 24/7/17.
 */
var app = angular.module('flowelementsapp');
app.directive("limitTo", [function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function (e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);
app.directive('fileModel', function ($parse) {
    return {
        restrict: 'A', //the directive can be used as an attribute only

        /*
         link is a function that defines functionality of directive
         scope: scope associated with the element
         element: element on which this directive used
         attrs: key value pair of element attributes
         */
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel),
                modelSetter = model.assign; //define a setter for demoFileModel

            //Bind change event on the element
            element.bind('change', function () {
                //Call apply on scope, it checks for value changes and reflect them on UI
                scope.$apply(function () {
                    //set the model value
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
});



app.directive('externalHtml', ["$parse", "$compile", function($parse, $compile) {
      return {
        restrict: 'E',
        link: function(scope, element, attrs) {
          const data = $parse(attrs.externalData)(scope);
          const el = document.createElement('div');
          el.innerHTML = data;
          const scripts = el.getElementsByTagName("script");
          for (i in scripts) {
            // console.log(scripts[i].textContent)
              if (scripts[i]['type'] === 'text/javascript'){
                  eval(scripts[i].textContent);
              }
          }
          element.append($compile(data)(scope));
        }
      }
    }]);

 app.directive('fileModel', function ($parse) {
        return {
            restrict: 'A', //the directive can be used as an attribute only

            /*
             link is a function that defines functionality of directive
             scope: scope associated with the element
             element: element on which this directive used
             attrs: key value pair of element attributes
             */
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel),
                    modelSetter = model.assign; //define a setter for demoFileModel

                //Bind change event on the element
                element.bind('change', function () {
                    //Call apply on scope, it checks for value changes and reflect them on UI
                    scope.$apply(function () {
                        //set the model value
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
});