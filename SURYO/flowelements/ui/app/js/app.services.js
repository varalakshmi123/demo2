var app = angular.module("flowelementsapp");
app.service("baseService", ["$http","$rootScope", function($http,$rootScope){
    var baseService = {};
    var apiUrl = "/api/";

    baseService.handleError = function(data, status, headers, config) {
        console.log("error: "+data+":"+status);
    };

    baseService.handleData = function(data, dataCallback,errorCallBack) {
        if(data["status"] === "ok")
        {
            if(dataCallback)
            {
                dataCallback(data["msg"]);
            }
        }
/*        else
        {
            //alert("Received Error: "+angular.toJson(data))
		errorCallBack(data);	    
        }*/
	else {
                // $rootScope.openModalPopupClose();
                if (angular.isDefined(errorCallBack))
                {
                    errorCallBack(data);
                }
                else
                {
                    $rootScope.showErrorMsg(data['msg'])
                    // $rootScope.handleErrorMessages(data);
                }
            }
    };

    baseService.getSnakeCaseName = function(camelCase) {
        return camelCase.replace( /([A-Z])/g, "_$1").toLowerCase().replace(/^_(.*)/g,"$1");
    };

    baseService.get = function(collection, id, filter, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        if(id)
        {
            url = url+"/"+id;
        }

        return $http.get(url,{params:filter}).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    };

    baseService.put = function(collection, id, data, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        url = url+"/"+id;
        $http.put(url, data).error(baseService.handleError)	
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    };

    baseService.post = function(collection, data, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        $http.post(url, data).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    };

    baseService.deleteData = function(collection, id, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        url = url+"/"+id;
        /*$http.delete(url).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });*/
        $http({method: 'DELETE', url: url}).error(baseService.handleError)
                    .success(function (data, status, headers, config) {
                        baseService.handleData(data, dataCallback, errorCallBack);
                    });
    };

    baseService.action = function(collection, id, action, data, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection)+"/"+id+"/"+action;
        $http.post(url, data).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    };

    return baseService;
}]);


app.factory('BuildGraphService', function ($filter, FilerGraphService) {
    var returnOptions = {
        options: {},
        getStackToolTip: function (tooltip) {
            var returnString = "<tr><td style=\"color: {series.color}\">{series.name}: </td><td style=\"text-align: right\"><b></b><b>{point.y}</b></td></tr>";
            if (tooltip.isFront) {
                returnString = "<tr><td style=\"color: {series.color}\">{series.name}: </td><td style=\"text-align: right\"><b>" + tooltip.suffix + "</b><b>{point.y}</b></td></tr>";
            }
            else {
                returnString = "<tr><td style=\"color: {series.color}\">{series.name}: </td><td style=\"text-align: right\"><b>{point.y}</b><b>" + tooltip.suffix + "</b></td></tr>";
            }
            return returnString;
        },
        getPlotOptions: function (type, stackType, isDataLabelEnable, datetime) {
            var plotOptions = {};
            var plotType = stackType;
            if (stackType === 'bar') {
                plotType = 'series'
            }
            if (stackType === 'boxplot') {
                plotType = 'column'
            }
            if (datetime === 'true') {
                plotOptions[plotType] = {
                    threshold: null
                }
            }
            else {
                plotOptions[plotType] = {
                    "stacking": (type === 'stack') ? "normal" : "",
                    dataLabels: {
                        enabled: (isDataLabelEnable === 'true') ? true : false
                    }
                };
            }
            return plotOptions;
        },
        getyAxis: function (yaxisTitle) {
            var yXis = [];
            angular.forEach(yaxisTitle.titles, function (titleKey, titleIndex) {
                var innerX = {
                    title: {
                        "text": titleKey.name
                    },
                    labels: {
                        format: (titleKey.isFront) ? titleKey.suffix + '{value}' : '{value}' + titleKey.suffix,
                        "style": {
                            "color": "#3202A8",
                            "fontWeigth": "normal"
                        }
                    }
                };
                if (titleIndex === yaxisTitle.titles.length - 1 && yaxisTitle.titles.length !== 1) {
                    innerX['opposite'] = true;
                }
                yXis.push(innerX)
            });
            return yXis;
        },
        returnCommonColumnOptions: function (title, yaxisTitle, isCount, width, type, stackType, isDataLabelEnable, xaxisFilterType, datetime) {
            var yXis = returnOptions.getyAxis(yaxisTitle);
            var innerOptions = {
                chart: {
                    "zoomType": "xy",
                    "height": width,
                    type: stackType
                },
                credits: {
                    "enabled": false
                },
                title: {
                    "text": title,
                    "margin": 50
                },
                xAxis: (datetime === 'true') ? {"type": 'datetime'} : {
                    categories: [],
                    labels: {
                        "style": {
                            "color": "#3202A8",
                            "fontWeigth": "normal"
                        },
                        formatter: function () {
                            var filterForm = "";
                            var xformat = (xaxisFilterType === 'epochDate') ? $filter('date')(new Date(this.value), 'mediumDate') : this.value;
                            return xformat;
                        }
                    }
                },
                tooltip: {
                    shared: true,
                    crosshair: true,
                    useHTML: true,
                    headerFormat: "<small>{point.key}</small><table>",
                    pointFormat: returnOptions.getStackToolTip(yaxisTitle),
                    footerFormat: "</table>",
                    valueDecimals: 2
                },
                plotOptions: returnOptions.getPlotOptions(type, stackType, isDataLabelEnable, datetime),
                yAxis: yXis
            };
            return innerOptions;
        },
        getStackWithNegOptions: function (title, yaxisTitle, isCount, width, type, stackType, isDataLabelEnable, xaxisFilterType, datetime) {
            var innerOptions = {};
            angular.extend(innerOptions, returnOptions.returnCommonColumnOptions(title, yaxisTitle, isCount, width, type, stackType, isDataLabelEnable, xaxisFilterType, datetime));
            returnOptions.options = {
                options: innerOptions,
                series: []
            };
            return returnOptions.options;
        },
        getGuageOptions: function (serName, serData, maxVal, plotBands, isColorChange, colorChangeColor) {
            returnOptions.options = {
                "options": {
                    chart: {
                        type: 'gauge',
                        //renderTo:'mychartId',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        height: 200
                    },
                    title: {
                        text: ''
                    },
                    pane: {
                        center: ['50%', '60%'],
                        size: '100%',
                        startAngle: -90,
                        endAngle: 90,
                        borderWidth: 1,
                        background: [{
                            backgroundColor: '#fff',
                            borderWidth: 0
                        }]
                    },
                    exporting: {enabled: false},
                    credits: {"enabled": false},
                    yAxis: {
                        min: 0,
                        max: maxVal,
                        lineColor: 'white',
                        tickColor: 'black',
                        minorTickColor: 'black',
                        minorTickPosition: 'outside',
                        tickPosition: 'outside',
                        tickLength: 10,
                        minorTickInterval: 'auto',
                        tickWidth: 2,
                        minorTickLength: 4,
                        minorTickWidth: 1,
                        minTickInterval: 1,
                        endOnTick: true,
                        lineWidth: 1,
                        //tickInterval:0.5,
                        labels: {
                            distance: 20,
                            //padding:maxVal/10,
                            rotation: 0,
                            style: {
                                color: '#3202A8',
                                fontWeigth: 'normal'
                            }
                        },
                        plotBands: plotBands
                    },
                    tooltip: {
                        formatter: function () {
                            return this.series.name + " : " + $filter('currency')(this.y / 1000000, '$', 2) + ' millions';
                        }
                    },
                    plotOptions: {
                        gauge: {
                            dial: {
                                radius: '90%',
                                backgroundColor: 'black',
                                borderColor: 'black',
                                borderWidth: 1,
                                baseWidth: 4,
                                topWidth: 0.5,
                                baseLength: '30%', // of radius
                                rearLength: '2%'
                            },
                            pivot: {
                                radius: 1
                            },
                            dataLabels: {
                                borderWidth: 2,
                                borderColor: '#d3e9f7',
                                padding: 3,
                                borderRadius: 2,
                                verticalAlign: 'center',
                                y: 30,
                                style: {
                                    fontWeight: 'normal'
                                }
                            },
                            wrap: false
                        }
                    }
                },
                tooltip: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: serName,
                    data: serData,
                    dataLabels: {
                        enabled: false

                    }
                }],
                func: function (chart) {
                    //$scope.chartExportForecast = $.proxy(chart.exportChart, chart);
                }
            };
            if (isColorChange) {
                returnOptions.options['series'][0]['dial'] = {
                    radius: '90%',
                    backgroundColor: colorChangeColor,
                    borderColor: colorChangeColor,
                    borderWidth: 1,
                    baseWidth: 4,
                    topWidth: 0.5,
                    baseLength: '30%', // of radius
                    rearLength: '5%'

                }
            }
            return returnOptions.options
        },
        getPieCommonOptions: function (seriesTitle, mainTitle) {
            return {
                options: {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    tooltip: {
                        pointFormat: 'Percentage: <b>{point.percentage:.1f}%</b><br><p>Count: <b>{point.y}</b></p>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            },
                            showInLegend: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: seriesTitle,
                    colorByPoint: true,
                    data: []
                }],
                title: {
                    text: mainTitle
                },
                loading: false
            };
        },
        getBubbleOptions: function (data, title, xAxisTitle, yAxisTitle) {
            returnOptions.options = {
                options: {
                    chart: {
                        type: 'bubble',
                        plotBorderWidth: 1,
                        zoomType: 'xy'
                    },
                    legend: {
                        enabled: false
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        gridLineWidth: 1,
                        title: {
                            text: xAxisTitle.title
                        },
                        labels: {
                            format: (xAxisTitle.inFront === 'true') ? xAxisTitle.suffix + '{value}' : '{value}' + xAxisTitle.suffix
                        },
                        plotLines: [{
                            color: 'black',
                            dashStyle: 'dot',
                            width: 2,
                            value: 65,
                            label: {
                                rotation: 0,
                                y: 15,
                                style: {
                                    fontStyle: 'italic'
                                },
                                text: xAxisTitle.subtitle
                            },
                            zIndex: 3
                        }]
                    },
                    yAxis: {
                        startOnTick: false,
                        endOnTick: false,
                        title: {
                            text: yAxisTitle.title
                        },
                        labels: {
                            format: (yAxisTitle.inFront === 'true') ? yAxisTitle.suffix + '{value}' : '{value}' + yAxisTitle.suffix
                        },
                        maxPadding: 0.2,
                        plotLines: [{
                            color: 'black',
                            dashStyle: 'dot',
                            width: 2,
                            value: 50,
                            label: {
                                align: 'right',
                                style: {
                                    fontStyle: 'italic'
                                },
                                text: yAxisTitle.subtitle,
                                x: -10
                            },
                            zIndex: 3
                        }]
                    },
                    tooltip: {
                        useHTML: true,
                        headerFormat: '<table>',
                        pointFormat: '<tr><th colspan="2"><h3>{point.display}</h3></th></tr>' +
                        '<tr><th>x:</th><td>{point.x}g</td></tr>' +
                        '<tr><th>y:</th><td>{point.y}g</td></tr>' +
                        '<tr><th>z:</th><td>{point.z}%</td></tr>',
                        footerFormat: '</table>',
                        followPointer: true
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    }
                },
                series: [{
                    type: 'bubble',
                    data: data
                }]
            };
            return returnOptions.options;
        }
    };
    return returnOptions;
});
app.factory('FilerGraphService', function ($filter) {
    var returnOptions = {
        options: {},
        sortNumber: function (a, b) {
            //var a = [2,1,10]//test code if you have doubt
            //console.log(a.sort(returnOptions.sortNumber))
            //console.log(a)
            return a - b;
        },
        valueAsSeries: function (options, data, categoryKey, basedOnKey, yAxisKey, xaxisFilterType, datetime) {
            returnOptions.options = angular.copy(options);
            angular.forEach(data, function (dKey, dIndex) {
                if (returnOptions.options.options.xAxis.categories.indexOf(dKey[categoryKey]) === -1) {
                    returnOptions.options.options.xAxis.categories.push(dKey[categoryKey])
                }
            });
            if (xaxisFilterType === 'epochDate') {
                returnOptions.options.options.xAxis.categories.sort(returnOptions.sortNumber);
            }
            else {
                returnOptions.options.options.xAxis.categories.sort();
            }
            returnOptions.options.options.xAxis.categories = returnOptions.options.options.xAxis.categories.sort();
            angular.forEach(returnOptions.options.series, function (serKey, serIndex) {
                angular.forEach(returnOptions.options.options.xAxis.categories, function (catKey, catIndex) {
                    var x = 0;
                    var y = 0;
                    angular.forEach(data, function (dKey, dIndex) {
                        if (catKey === dKey[categoryKey]) {
                            if (serKey.name === dKey[basedOnKey].trim()) {
                                x += dKey[yAxisKey];
                                y++;
                            }
                        }
                    });
                    returnOptions.options.series[serIndex].data.push(x)
                });
            });
            if (datetime === 'true') {
                var dateOptions = angular.copy(options);
                angular.forEach(returnOptions.options.series, function (serKey, serIndex) {
                    var dummyArray = [];
                    angular.forEach(returnOptions.options.options.xAxis.categories, function (catKey, catIndex) {
                        dummyArray.push([catKey, returnOptions.options.series[serIndex].data[catIndex]])
                    });
                    dateOptions.series[serIndex].data = dummyArray;
                });
                delete dateOptions.options.xAxis.categories;
                returnOptions.options = angular.copy(dateOptions);
            }
            return returnOptions.options;
        },
        keyAsSeries: function (options, data, categoryKey, basedOnKey, yAxisKey, xaxisFilterType) {
            returnOptions.options = angular.copy(options);
            angular.forEach(data, function (dKey, dIndex) {
                if (returnOptions.options.options.xAxis.categories.indexOf(dKey[categoryKey]) === -1) {
                    returnOptions.options.options.xAxis.categories.push(dKey[categoryKey])
                }
            });
            angular.forEach(returnOptions.options.series, function (serKey, serIndex) {
                angular.forEach(returnOptions.options.options.xAxis.categories, function (catKey, catIndex) {
                    var x = 0;
                    var y = 0;
                    angular.forEach(data, function (dKey, dIndex) {
                        if (catKey === dKey[categoryKey]) {
                            var other = "";
                            var string = dKey[serKey.name].toString();
                            other = string.replace(/%/g, "");
                            x = parseInt(string);
                        }
                    });
                    returnOptions.options.series[serIndex].data.push(x)
                });
            });
            return returnOptions.options;
        },
        pieKeyAsSeries: function (options, data, xaxisFilter, yaxisFilter, isCount) {
            returnOptions.options = angular.copy(options);
            angular.forEach(returnOptions.options.series[0].data, function (serKey, serIndex) {
                var x = 0;
                angular.forEach(data, function (dkey, dIndex) {
                    if (serKey.name === dkey[xaxisFilter]) {
                        x = dkey[yaxisFilter];
                        returnOptions.options.series[0].data[serIndex].y = x;
                    }
                });
            });
            return returnOptions.options;
        },
        aggregateAllValAsSeries: function (options, data, xaxisFilter, yaxisFilter, isCount) {
            returnOptions.options = angular.copy(options);
            angular.forEach(returnOptions.options.series[0].data, function (serKey, serIndex) {
                var x = 0;
                angular.forEach(data, function (dkey, dIndex) {
                    if (serKey.name === dkey[xaxisFilter]) {
                        if (isCount === 'true') {
                            returnOptions.options.series[0].data[serIndex].y++;
                        }
                        else {
                            returnOptions.options.series[0].data[serIndex].y += dkey[yaxisFilter];
                        }
                    }
                });
            });
            return returnOptions.options
        },
        aggregateAllKeyAsSeries: function (options, data, xaxisFilter, yaxisFilter, isCount) {
            returnOptions.options = angular.copy(options);
            angular.forEach(returnOptions.options.series[0].data, function (serKey, serIndex) {
                var x = 0;
                angular.forEach(data, function (dkey, dIndex) {
                    if (isCount === 'true') {
                        returnOptions.options.series[0].data[serIndex].y++;
                    }
                    else {
                        returnOptions.options.series[0].data[serIndex].y += dkey[serKey.name]
                    }
                });
            });
            return returnOptions.options
        },
        arrayOfArrayOfKeyAsSeries: function (options, data, categoryKey, array) {
            returnOptions.options = angular.copy(options);
            angular.forEach(data, function (dKey, dIndex) {
                if (returnOptions.options.options.xAxis.categories.indexOf(dKey[categoryKey]) === -1) {
                    returnOptions.options.options.xAxis.categories.push(dKey[categoryKey])
                }
            });
            angular.forEach(returnOptions.options.options.xAxis.categories, function (catKey, catIndex) {
                var x = 0;
                var dummyArray = [];
                angular.forEach(data, function (dKey, dIndex) {
                    if (catKey === dKey[categoryKey]) {
                        angular.forEach(array, function (arrayKey, arrayIndex) {
                            x = dKey[arrayKey];
                            dummyArray.push(x);
                        });
                        returnOptions.options.series[0].data.push(dummyArray)
                    }
                });
            });
            return returnOptions.options
        }
    };
    return returnOptions;
});
app.service('fileUploadService', function ($http) {
         this.uploadFileToUrl = function (file, uploadUrl) {
             var fd = new FormData(this);
             fd.append('file', file);
             //FormData, object of key/value pair for form fields and values

             return $http.post(uploadUrl, fd, {
                 headers: {'Content-Type': undefined},
                 transformRequest: angular.identity
             }).then(function (response) {
                 console.log(response.data)
                 return response.data
             });

         }
    });
