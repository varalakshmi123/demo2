/**
 * Copyright JS Foundation and other contributors, http://js.foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
(function() {
    var nodesData = [{"id":"node-red/sentiment","name":"sentiment","types":["sentiment"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/inject","name":"inject","types":["inject"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/catch","name":"catch","types":["catch"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/status","name":"status","types":["status"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/debug","name":"debug","types":["debug"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/link","name":"link","types":["link in","link out"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/exec","name":"exec","types":["exec"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/function","name":"function","types":["function"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/template","name":"template","types":["template"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/delay","name":"delay","types":["delay"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/trigger","name":"trigger","types":["trigger"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/comment","name":"comment","types":["comment"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/unknown","name":"unknown","types":["unknown"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/rpi-gpio","name":"rpi-gpio","types":["rpi-gpio in","rpi-gpio out","rpi-mouse","rpi-keyboard"],"enabled":true,"local":false,"module":"node-red","err":"Info : Ignoring Raspberry Pi specific node","version":"0.16.2"},{"id":"node-red/tls","name":"tls","types":["tls-config"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/mqtt","name":"mqtt","types":["mqtt in","mqtt out","mqtt-broker"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/httpin","name":"httpin","types":["http in","http response"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/httprequest","name":"httprequest","types":["http request"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/websocket","name":"websocket","types":["websocket in","websocket out","websocket-listener","websocket-client"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/watch","name":"watch","types":["watch"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/tcpin","name":"tcpin","types":["tcp in","tcp out","tcp request"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/udp","name":"udp","types":["udp in","udp out"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/switch","name":"switch","types":["switch"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/change","name":"change","types":["change"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/range","name":"range","types":["range"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/split","name":"split","types":["split","join"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/CSV","name":"CSV","types":["csv"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/HTML","name":"HTML","types":["html"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/JSON","name":"JSON","types":["json"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/XML","name":"XML","types":["xml"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/YAML","name":"YAML","types":["yaml"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/tail","name":"tail","types":["tail"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red/file","name":"file","types":["file","file in"],"enabled":true,"local":false,"module":"node-red","version":"0.16.2"},{"id":"node-red-node-email/email","name":"email","types":["e-mail","e-mail in"],"enabled":true,"local":false,"module":"node-red-node-email","version":"0.1.23"},{"id":"node-red-node-feedparser/feedparse","name":"feedparse","types":["feedparse"],"enabled":true,"local":false,"module":"node-red-node-feedparser","version":"0.1.8"},{"id":"node-red-node-rbe/rbe","name":"rbe","types":["rbe"],"enabled":true,"local":false,"module":"node-red-node-rbe","version":"0.1.11"},{"id":"node-red-node-twitter/twitter","name":"twitter","types":["twitter-credentials","twitter in","twitter out"],"enabled":true,"local":false,"module":"node-red-node-twitter","version":"0.1.11"},{"id":"node-red-node-mongodb/mongo","name":"mongo","types":["mongodb","mongodb out","mongodb in"],"enabled":true,"local":true,"module":"node-red-node-mongodb","version":"0.0.11"}]
    var flowData = {"flows":[{"id":"49f4db9e.13ddb4","type":"tab","label":"Flow 1"},{"id":"1840b667.bf1dea","type":"twitter-credentials","z":"","screen_name":"@dineshyelavarth"},{"id":"4fd4b969.bd4fd8","type":"mongodb","z":"","hostname":"127.0.0.1","port":"27017","db":"demo","name":""},{"id":"33adf168.16220e","type":"mongodb out","z":"49f4db9e.13ddb4","mongodb":"4fd4b969.bd4fd8","name":"","collection":"tweets","payonly":false,"upsert":false,"multi":false,"operation":"store","x":632,"y":132,"wires":[]},{"id":"f4001de2.7fbff","type":"inject","z":"49f4db9e.13ddb4","name":"","topic":"","payload":"","payloadType":"date","repeat":"","crontab":"","once":false,"x":155,"y":86,"wires":[["d314ead1.4f0bf8"]]},{"id":"d314ead1.4f0bf8","type":"debug","z":"49f4db9e.13ddb4","name":"","active":true,"console":"true","complete":"payload","x":320,"y":380,"wires":[]},{"id":"9ad9f5d5.c44318","type":"twitter in","z":"49f4db9e.13ddb4","twitter":"1840b667.bf1dea","tags":"#yuvi #iamkohli","user":"false","name":"","topic":"tweets","inputs":0,"x":140,"y":480,"wires":[["d314ead1.4f0bf8","33adf168.16220e"]]},{"id":"c4e94c39.8eb0c","type":"mongodb in","z":"49f4db9e.13ddb4","mongodb":"4fd4b969.bd4fd8","name":"","collection":"tweets","operation":"find","x":310,"y":600,"wires":[["8cec47a0.161338"]]},{"id":"8cec47a0.161338","type":"template","z":"49f4db9e.13ddb4","name":"","field":"payload","fieldType":"msg","format":"handlebars","syntax":"mustache","template":"<ul>\n    {{#payload}}\n    <li>\n        {{tweet.text}}\n    </li>\n    {{/payload}}\n</ul>","x":500,"y":620,"wires":[["4c0b7466.f7adec"]]},{"id":"4c0b7466.f7adec","type":"http response","z":"49f4db9e.13ddb4","name":"","x":660,"y":680,"wires":[]},{"id":"d47c13fe.b10fc","type":"http in","z":"49f4db9e.13ddb4","name":"","url":"/tweets","method":"get","swaggerDoc":"","x":120,"y":660,"wires":[["c4e94c39.8eb0c"]]},{"id":"f86e006f.8b7c4","type":"function","z":"49f4db9e.13ddb4","name":"","func":"\nreturn msg;","outputs":1,"noerr":0,"x":524.9999999999999,"y":791.6666666666665,"wires":[[]]}],"rev":"4e5a9530996e9c79cf4b98f7094be4f2"}
    function loadNodeList() {
        $.ajax({
            headers: {
                "Accept":"application/json",
            },
            cache: false,
            url: 'nodes',
            success: function(data) {
                data = nodesData
                RED.nodes.setNodeList(data);

                var nsCount = 0;
                for (var i=0;i<data.length;i++) {
                    var ns = data[i];
                    if (ns.module != "node-red") {
                        nsCount++;
                        RED.i18n.loadCatalog(ns.id, function() {
                            nsCount--;
                            if (nsCount === 0) {
                                loadNodes();
                            }
                        });
                    }
                }
                if (nsCount === 0) {
                    loadNodes();
                }
            },error: function (responseData, textStatus, errorThrown) {
                console.log(" nodes request failed")
            }
        });
    }

    function loadNodes() {
        $.ajax({
            headers: {
                "Accept":"text/html"
            },
            cache: false,
            url: '/app/partials/generichtml.html',
            success: function(data) {
                $("body").append(data);
                $("body").i18n();
                $("#palette > .palette-spinner").hide();
                $(".palette-scroll").removeClass("hide");
                $("#palette-search").removeClass("hide");
                loadFlows();
            },error: function (responseData, textStatus, errorThrown) {
                console.log(" nodes request failed")
            }
        });
    }

    function loadFlows() {
        $.ajax({
            headers: {
                "Accept":"application/json",
            },
            cache: false,
            url: 'flows',
            success: function(nodes) {
                nodes = flowData
                var currentHash = window.location.hash;
                RED.nodes.version(nodes.rev);
                RED.nodes.import(nodes.flows);
                RED.nodes.dirty(false);
                RED.view.redraw(true);
                if (/^#flow\/.+$/.test(currentHash)) {
                    RED.workspaces.show(currentHash.substring(6));
                }

                var persistentNotifications = {};
                RED.comms.subscribe("notification/#",function(topic,msg) {
                    var parts = topic.split("/");
                    var notificationId = parts[1];
                    if (msg.text) {
                        var text = RED._(msg.text,{default:msg.text});
                        if (!persistentNotifications.hasOwnProperty(notificationId)) {
                            persistentNotifications[notificationId] = RED.notify(text,msg.type,msg.timeout === undefined,msg.timeout);
                        } else {
                            persistentNotifications[notificationId].update(text,msg.timeout);
                        }
                    } else if (persistentNotifications.hasOwnProperty(notificationId)) {
                        persistentNotifications[notificationId].close();
                        delete persistentNotifications[notificationId];
                    }
                });
                RED.comms.subscribe("status/#",function(topic,msg) {
                    var parts = topic.split("/");
                    var node = RED.nodes.node(parts[1]);
                    if (node) {
                        if (msg.hasOwnProperty("text")) {
                            msg.text = node._(msg.text.toString(),{defaultValue:msg.text.toString()});
                        }
                        node.status = msg;
                        node.dirty = true;
                        RED.view.redraw();
                    }
                });
                RED.comms.subscribe("node/#",function(topic,msg) {
                    var i,m;
                    var typeList;
                    var info;
                    if (topic == "node/added") {
                        var addedTypes = [];
                        msg.forEach(function(m) {
                            var id = m.id;
                            RED.nodes.addNodeSet(m);
                            addedTypes = addedTypes.concat(m.types);
                            RED.i18n.loadCatalog(id, function() {
                                $.get('nodes/'+id, function(data) {
                                    $("body").append(data);
                                });
                            });
                        });
                        if (addedTypes.length) {
                            typeList = "<ul><li>"+addedTypes.join("</li><li>")+"</li></ul>";
                            RED.notify(RED._("palette.event.nodeAdded", {count:addedTypes.length})+typeList,"success");
                        }
                    } else if (topic == "node/removed") {
                        for (i=0;i<msg.length;i++) {
                            m = msg[i];
                            info = RED.nodes.removeNodeSet(m.id);
                            if (info.added) {
                                typeList = "<ul><li>"+m.types.join("</li><li>")+"</li></ul>";
                                RED.notify(RED._("palette.event.nodeRemoved", {count:m.types.length})+typeList,"success");
                            }
                        }
                    } else if (topic == "node/enabled") {
                        if (msg.types) {
                            info = RED.nodes.getNodeSet(msg.id);
                            if (info.added) {
                                RED.nodes.enableNodeSet(msg.id);
                                typeList = "<ul><li>"+msg.types.join("</li><li>")+"</li></ul>";
                                RED.notify(RED._("palette.event.nodeEnabled", {count:msg.types.length})+typeList,"success");
                            } else {
                                $.get('nodes/'+msg.id, function(data) {
                                    $("body").append(data);
                                    typeList = "<ul><li>"+msg.types.join("</li><li>")+"</li></ul>";
                                    RED.notify(RED._("palette.event.nodeAdded", {count:msg.types.length})+typeList,"success");
                                });
                            }
                        }
                    } else if (topic == "node/disabled") {
                        if (msg.types) {
                            RED.nodes.disableNodeSet(msg.id);
                            typeList = "<ul><li>"+msg.types.join("</li><li>")+"</li></ul>";
                            RED.notify(RED._("palette.event.nodeDisabled", {count:msg.types.length})+typeList,"success");
                        }
                    }
                    // Refresh flow library to ensure any examples are updated
                    RED.library.loadFlowLibrary();
                });
            }
        });
    }

    function showAbout() {
        $.get('red/about', function(data) {
            var aboutHeader = '<div style="text-align:center;">'+
                                '<img width="50px" src="red/images/node-red-icon.svg" />'+
                              '</div>';

            RED.sidebar.info.set(aboutHeader+marked(data));
            RED.sidebar.info.show();
        });
    }

    function loadEditor() {
        var menuOptions = [];
        menuOptions.push({id:"menu-item-view-menu",label:RED._("menu.label.view.view"),options:[
            {id:"menu-item-view-show-grid",label:RED._("menu.label.view.showGrid"),toggle:true,onselect:"core:toggle-show-grid"},
            {id:"menu-item-view-snap-grid",label:RED._("menu.label.view.snapGrid"),toggle:true,onselect:"core:toggle-snap-grid"},
            {id:"menu-item-status",label:RED._("menu.label.displayStatus"),toggle:true,onselect:"core:toggle-status", selected: true},
            null,
            // {id:"menu-item-bidi",label:RED._("menu.label.view.textDir"),options:[
            //     {id:"menu-item-bidi-default",toggle:"text-direction",label:RED._("menu.label.view.defaultDir"),selected: true, onselect:function(s) { if(s){RED.text.bidi.setTextDirection("")}}},
            //     {id:"menu-item-bidi-ltr",toggle:"text-direction",label:RED._("menu.label.view.ltr"), onselect:function(s) { if(s){RED.text.bidi.setTextDirection("ltr")}}},
            //     {id:"menu-item-bidi-rtl",toggle:"text-direction",label:RED._("menu.label.view.rtl"), onselect:function(s) { if(s){RED.text.bidi.setTextDirection("rtl")}}},
            //     {id:"menu-item-bidi-auto",toggle:"text-direction",label:RED._("menu.label.view.auto"), onselect:function(s) { if(s){RED.text.bidi.setTextDirection("auto")}}}
            // ]},
            // null,
            {id:"menu-item-sidebar",label:RED._("menu.label.sidebar.show"),toggle:true,onselect:"core:toggle-sidebar", selected: true}
        ]});
        menuOptions.push(null);
        menuOptions.push({id:"menu-item-import",label:RED._("menu.label.import"),options:[
            {id:"menu-item-import-clipboard",label:RED._("menu.label.clipboard"),onselect:"core:show-import-dialog"},
            {id:"menu-item-import-library",label:RED._("menu.label.library"),options:[]}
        ]});
        menuOptions.push({id:"menu-item-export",label:RED._("menu.label.export"),disabled:true,options:[
            {id:"menu-item-export-clipboard",label:RED._("menu.label.clipboard"),disabled:true,onselect:"core:show-export-dialog"},
            {id:"menu-item-export-library",label:RED._("menu.label.library"),disabled:true,onselect:"core:library-export"}
        ]});
        menuOptions.push(null);
        menuOptions.push({id:"menu-item-search",label:RED._("menu.label.search"),onselect:"core:search"});
        menuOptions.push(null);
        menuOptions.push({id:"menu-item-config-nodes",label:RED._("menu.label.displayConfig"),onselect:"core:show-config-tab"});
        menuOptions.push({id:"menu-item-workspace",label:RED._("menu.label.flows"),options:[
            {id:"menu-item-workspace-add",label:RED._("menu.label.add"),onselect:"core:add-flow"},
            {id:"menu-item-workspace-edit",label:RED._("menu.label.rename"),onselect:"core:edit-flow"},
            {id:"menu-item-workspace-delete",label:RED._("menu.label.delete"),onselect:"core:remove-flow"}
        ]});
        menuOptions.push({id:"menu-item-subflow",label:RED._("menu.label.subflows"), options: [
            {id:"menu-item-subflow-create",label:RED._("menu.label.createSubflow"),onselect:"core:create-subflow"},
            {id:"menu-item-subflow-convert",label:RED._("menu.label.selectionToSubflow"),disabled:true,onselect:"core:convert-to-subflow"},
        ]});
        menuOptions.push(null);
        if (RED.settings.theme('palette.editable') !== false) {
            RED.palette.editor.init();
            menuOptions.push({id:"menu-item-edit-palette",label:RED._("menu.label.editPalette"),onselect:"core:manage-palette"});
            menuOptions.push(null);
        }

        menuOptions.push({id:"menu-item-keyboard-shortcuts",label:RED._("menu.label.keyboardShortcuts"),onselect:"core:show-help"});
        menuOptions.push({id:"menu-item-show-tips",label:RED._("menu.label.showTips"),toggle:true,selected:true,onselect:"core:toggle-show-tips"});
        menuOptions.push({id:"menu-item-help",
            label: RED.settings.theme("menu.menu-item-help.label","Node-RED website"),
            href: RED.settings.theme("menu.menu-item-help.url","http://nodered.org/docs")
        });
        menuOptions.push({id:"menu-item-node-red-version", label:"v"+RED.settings.version, onselect: "core:show-about" });


        RED.user.init();
        RED.library.init();
        RED.palette.init();
        RED.sidebar.init();
        RED.subflow.init();
        RED.workspaces.init();
        RED.clipboard.init();
        RED.search.init();
        RED.view.init();
        RED.editor.init();
        RED.keyboard.init();
        RED.diff.init();

        RED.menu.init({id:"btn-sidemenu",options: menuOptions});

        RED.deploy.init(RED.settings.theme("deployButton",null));

        RED.actions.add("core:show-about", showAbout);

        // RED.comms.connect();

        $("#main-container").show();
        $(".header-toolbar").show();


        loadNodeList();
    }

    $(function() {

        if ((window.location.hostname !== "localhost") && (window.location.hostname !== "127.0.0.1")) {
            document.title = document.title+" : "+window.location.hostname;
        }

        ace.require("ace/ext/language_tools");

        RED.i18n.init(function() {
            RED.settings.init(loadEditor());
        })
        // RED.settings.init(loadEditor)
        // loadEditor()
    });
})();
