import sys
import os
from pymongo import MongoClient

def exportReconMigration(reconName, exportFolder):
    mongo_client = MongoClient()
    mongo_db = mongo_client.ngerecon_suryo

    # recons
    q = 'mongoexport -d ngerecon_suryo -c  recons -q "{ reconName: ' + repr(
        str(reconName)) + ' }" --out ' + exportFolder + 'recons.json'
    os.system(q)

    #flows
    q = 'mongoexport -d ngerecon_suryo -c  flows -q "{ reconName: ' + repr(
        str(reconName)) + ' }" --out ' + exportFolder + 'flows.json'
    os.system(q)

    #sourceReference
    record = mongo_db.recons.find_one({'reconName': reconName})
    for key, val in record['sources'].iteritems():
        for k, v in val.iteritems():
            q = 'mongoexport -d ngerecon_suryo -c  sourceReference -q "{feedId: ' + repr(
                str(v['feedId']))+ '}" --out ' + exportFolder + str(v['feedId']) + '.json'
            os.system(q)

    print 'Please Find the files in folder :'

if __name__ == '__main__':
    # reconName = sys.argv[1]
    # exportFolder = sys.argv[2]
    reconName = 'UPIOutward'
    exportFolder = '/tmp/UPIOutward/'
    exportReconMigration(reconName, exportFolder)
