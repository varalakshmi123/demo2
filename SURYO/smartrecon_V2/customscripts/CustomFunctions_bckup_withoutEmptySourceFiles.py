import numpy as np
import pandas as pd
import re
import logger
from datetime import datetime
import traceback
import os
import config
logger = logger.Logger.getInstance("smartrecon").getLogger()
from Utilities import Utilities
from openpyxl import load_workbook
import requests
import json
import csv
from pymongo import MongoClient
class CustomFunctions:
    def __init__(self, payload,data=pd.DataFrame()):
        self.payload = payload
        self.data=data
    def count_words_at_url(url):
        print url
        return len(url)

    def vlookUp(self,lookup, dataFields, lookupFields, includeCols, markers={}):
        try:
            util = Utilities()
            lookup = util.getValues(self.payload, lookup).copy()
            data_field = dataFields
            lookup_field = lookupFields
            include_cols = includeCols
            marker_cols = markers
            setFiltered=False
            filterDf = self.data.copy().reset_index(drop=True)

            # incase if look_up data is empty, initialize original data.
            if lookup.empty:
                for key in marker_cols.keys():
                    self.data[key] = self.data.get(key, '')

                for col in include_cols:
                    if col not in self.data.columns:
                        self.data[col] = ''

                # util.setValues(payload, self.properties['resultkey'], data)
                return self.data

            if lookup_field:
                lookup.drop_duplicates(subset=lookup_field, keep='first', inplace=True)

                for column, value in marker_cols.iteritems():
                    if column in lookup.columns:
                        del lookup[column]
                    lookup[column] = value
                    include_cols.append(column)

                # Remove if look up columns already exists in loaded data, avoid duplicate columns on re-look up
                result_field = self.data.columns.tolist()
                for col in include_cols:

                    if col in self.data.columns:
                        setFiltered = True
                        _filters = filterDf[col].apply(lambda x: True if x == ('' or 0) else False)
                        self.data = self.data[_filters]
                        del filterDf[col]

                    if col not in result_field:
                        result_field.append(col)

                lookup = lookup[lookup_field + include_cols]
                lookup_data = filterDf.merge(lookup, left_on=data_field, right_on=lookup_field, how='left',
                                             suffixes=('', '_y'))

                for col in lookup_data.columns:
                    if '_y' in col:
                        del lookup_data[col]

                if setFiltered:
                    lookup_data = pd.concat([lookup_data, self.data], join_axes=[self.data.columns])

                for col in result_field:
                    lookup_data[col].fillna(util.infer_dtypes(lookup_data[col]), inplace=True)

                # util.setValues(payload, self.properties['resultkey'], lookup_data)
                lookup_data = lookup_data.reset_index()
                return lookup_data
        except Exception as e:
            self.payload['error'] = dict(elementName="VLookup", type="critical", exception=str(e),
                                         traceback=traceback.format_exc())
    def jsonReportGenerator(self,template):
        try:
            from prerecon import FilterExecutor
            outputJson = []
            if not 'reports' in self.payload.keys():
                self.payload['reports'] = {}
            outdf = pd.DataFrame()
            outputPath = self.payload['source_path'] + '/' + 'OUTPUT/'
            reportName = os.path.splitext(os.path.basename(template))[0]

            with open(template) as f:
                jsonData = json.load(f)
                writeToMongo=jsonData.get('writeToMongo',False)
                fileType=jsonData['fileType']
                if 'reportName' in jsonData.keys():
                    reportName = jsonData['reportName']
                else:
                    reportName = os.path.splitext(os.path.basename(template))[0].split('_')[0]
            sourceList = []

            for data in jsonData['action']:
                for filter in data['filters']:
                    sourceList.append(filter['resultkey'])
                    df = Utilities().getValues(self.payload, filter['source']).copy()
                    df = FilterExecutor.FilterExecutor().run(df, filter['expressions'], payload=self.payload)
                    self.payload[filter['resultkey']] = df

                # datafilling reports
                if 'rows' in data.keys():

                    for key in data['rows'].keys():
                        if re.match("{.*}", str(data['rows'][key])):
                            for value in re.findall('\{(.*?)\}', data['rows'][key]):
                                source, expression = value.split('?')
                                express = expression.replace('df', "self.payload['" + source + "']")
                                data['rows'][key] = eval(express)
                    if len(data['rows'].keys())>1:
                        outputJson.append(data['rows'])
                        self.payload['template_df'] = pd.DataFrame(outputJson)

                else:

                    file = open(outputPath + reportName + '.csv', 'w')
                    csvwriter = csv.writer(file)
                    for source in sourceList:
                        df =self.payload[source]
                        if 'header' in data.keys() and data['header']:
                            csvwriter.writerow(df.columns.tolist())
                        for index, row in df.iterrows():
                            csvwriter.writerow(list(row))

            if outputJson:
                outdf = self.payload['template_df']
                outdf.to_csv(outputPath + reportName + '.'+fileType, index=False)
            if writeToMongo:
                master_dumps = MongoClient(config.mongoHost)[config.databaseName]['custom_reports']
                master_dumps.insert({'reconName':self.payload['reconName'],'data':self.payload['template_df'].to_dict(orient='records'),
                                     'statementDate':self.payload['statementDate'],"partnerId": "54619c820b1c8b1ff0166dfc","reportName": reportName
                                     })
            self.payload['reports'][reportName] = {"reportName": reportName, "reportPath": reportName + '.'+fileType,
                                             "source": "Reports","writeToMongo":writeToMongo}
        except Exception as e:
            print traceback.format_exc()
            f.close()
            logger.info("Unable to generate the report " + reportName )
            raise ValueError("Unable to generate the report " + reportName)

    def xlsxReportGenerator(self,template):
        try:
            outputPath = self.payload['source_path'] + '/' + 'OUTPUT/'
            reportName = os.path.splitext(os.path.basename(template))[0]
            wb = load_workbook(filename=template)
            sheet_names = wb.get_sheet_names()
            # name = sheet_names[0]

            # sheet_ranges = wb[name]
            for sheet in sheet_names:

                ws = wb.get_sheet_by_name(sheet)
                for rowidex, row in enumerate(ws.iter_cols()):
                    for colindex, cell in enumerate(row):
                        value=str(cell.value).replace("\n", "") if cell.value is not None else ''
                        if re.match("{.*}", value):
                            for value in re.findall('\{(.*?)\}',value):
                                source, expression = value.split('?')
                                express = expression.replace('df', "self.payload['" + source + "']")
                                ws.cell(row=colindex + 1, column=rowidex + 1).value = eval(express)

            wb.save(outputPath + reportName + '.xlsx')
            self.payload['reports'][reportName] = {"reportName": reportName, "reportPath": reportName + '.' + '.xls',
                                                   "source": "Reports"}
        except Exception, e:
            print e
            print traceback.format_exc()
            self.payload['error'] = dict(elementName="DataExtracter", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())
    def masterDumpHandler(self,properties):
        try:
            master_dumps = MongoClient(config.mongoHost)[config.databaseName]['master_dumps']

            if properties.get('operation', None) is None:
                raise ValueError('operation is mandatory input "insert" / "find"')

            elif properties['operation'] == 'find':
                df = properties['inputdf']
                df_json = json.loads(df.to_json(orient='records'))
                master_data = []
                for i in df_json:
                    master_data.extend(list(master_dumps.find(i)))
                return pd.DataFrame(master_data)

            elif properties['operation'] == 'insert':
                source = self.payload[properties['source']]
                if properties.get('insertCols', {}):
                    source.rename(columns=properties.get('insertCols', {}),inplace=True)
                    colList=[val for key,val in properties.get('insertCols', {}).iteritems()]
                    source = source[colList]

                source['type'] = properties['masterType']
                source['reconName'] = self.payload['reconName']
                source['statementDate'] = self.payload['statementDate']
                source["created"] = datetime.strptime(datetime.now().strftime('%d-%b-%Y'),
                                                               '%d-%b-%Y')
                source['dropOnRollback'] = properties.get('dropOnRollback', False)
                # clean dataframe values before saving to db
                source.replace([np.nan, 'nan', 'NaT'], '', inplace=True)
                master_dumps.insert(source.to_dict(orient='records'))

            elif properties['operation'] == 'load':
                master_data = list(
                    master_dumps.find({'type': properties['masterType'], 'reconName': self.payload['reconName']}))
                Utilities().setValues(self.payload, properties['resultkey'], pd.DataFrame(master_data))

            else:
                raise ValueError("Invalid operation %s not implemented" % properties['operation'])

            # if timedelta is defined removed, records older than days specified in time delta
            delta_days = properties.get('timedelta', None)
            if delta_days:
                delta_date = datetime.datetime.strptime(datetime.datetime.now().strftime('%d-%b-%Y'), '%d-%b-%Y') \
                             - datetime.timedelta(delta_days)
                master_dumps.remove({"created": {'$lte': delta_date}})

        except Exception, e:
            print traceback.format_exc()
            self.payload['error'] = dict(elementName="MasterDumpHandler", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())
