import sys
import json
from pymongo import MongoClient
from bson import ObjectId

def importReconDetails(importFolderName):
    mongo_client = MongoClient()
    lmongo_db = mongo_client.ngerecon_suryo

    # recons
    record = json.loads(open(importFolderName +'/'+ 'recons.json', 'r').read())
    record['_id'] = ObjectId(record['_id']['$oid'])
    lmongo_db.recons.save(record)

    # # sourceReference
    for key, feed in record['sources'].iteritems():
        for k, f in feed.iteritems():
            feedPath = importFolderName + '/' + str(f['feedId']) + '.json'
            feedObj = json.loads(open(feedPath, 'r').read())

            feedObj['_id'] = ObjectId(feedObj['_id']['$oid'])
            lmongo_db.sourceReference.save(feedObj)

    # flows
    try:
        record = json.loads(open(importFolderName + '/' + 'flows.json', 'r').read())
        record['_id'] = ObjectId(record['_id']['$oid'])
        lmongo_db.flows.save(record)
    except:
        pass
    print "data inserted into db:"

if __name__ == '__main__':
    # importFolderName = '/home/hinclude/erecon_work/exportFolder/EBANK/'
    importFolderName = '/home/kedhar/Downloads/IMPS_OUTWARD/'
    importReconDetails(importFolderName)

