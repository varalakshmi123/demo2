import pandas as pd
import numpy as np
import os
import config


class DataParser:
    def __init__(self):
        pass

    def data_preprocessor(self, data=pd.DataFrame(), dtypes={}, params={}):
        parsed_data = {}

        if params.get('template'):
            parsed_data = self.parse_template(params['template'], data)
            data = parsed_data.get('txn_data', data)
        else:
            pass

        # format data in all the columns
        for col, dtype in dtypes.iteritems():
            data.loc[:, col] = data[col].str.strip().replace('nan|None', '', regex=True)
            if dtype == 'np.int64':
                data.loc[:, col] = data[col].replace('[^0-9]', '', regex=True).replace('', '0').astype(np.int64)
            elif dtype == 'np.float64':
                data.loc[:, col] = data[col].replace('[^0-9\.\-]|,', '', regex=True).replace('', '0.0').astype(
                    np.float64)
            else:
                pass

        return data

    @staticmethod
    def parse_template(template, data):
        tmp_path = config.basepath + '/templates/' + template
        if not os.path.isfile(tmp_path):
            raise IOError("Template file %s not found, cannot parse data." % template)

        template = pd.read_csv(tmp_path).fillna('')
        data.reset_index(inplace=True, drop=True)

        parsed_data = {}
        for row in template.to_dict(orient='records'):
            df = None
            start, stop = tuple(row['Position'].split(':')) if ':' in row['Position'] else (row['Position'], '')
            column = repr(row['Column']) if row['Column'] else ':'
            exec ("df = data.loc[data.index[{start}:{stop}], {column}]".format(start=start, stop=stop, column=column))

            if isinstance(df, pd.DataFrame):
                filters = [x for x in row['Filters'].strip().split(",") if x]
                for f in filters:
                    exec "df = df[{exp}]".format(exp=f)

            parsed_data[row['Type']] = df
        return parsed_data

    @staticmethod
    def process_validations(params={}):
        pass
