from traceback import format_exc
import numpy as np
import pandas as pd
import xlrd
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class ExcelLoader(FeedLoader):
    def getType(self):
        return "Excel"

    def loadFile(self, fileNames, params):
        feed_format = params['feedformat']
        date_columns = feed_format[feed_format['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)
        feed_format['dataType'] = feed_format['dataType'].str.replace('np.datetime64', 'str')

        data_types = feed_format['dataType']
        data_types = {idx: eval(d_type) for idx, d_type in enumerate(data_types) if not str(d_type) is ''}

        column_names = feed_format['fileColumn'].tolist()

        df = pd.DataFrame(columns=column_names)
        for fileName in fileNames:
            try:

                logger.info("Loading excel file %s" % fileName)
                xls = xlrd.open_workbook(fileName, on_demand=True)
                sheets=xls.sheet_names()

                sheetNames=params.get('sheetNames').split(',')  if params.get('sheetNames') is not None else [0]
                
                if 0 in sheetNames:
                    sheets=[0]

                for sheet in sheets:

                    if sheet in sheetNames:
                        if column_names:
                            current_frame = pd.read_excel(fileName, skipfooter=int(params.get("skipfooter", 0)),
                                                          skiprows=int(params.get("skiprows", 0)), header=None,sheet_name=sheet,
                                                           usecols=list(feed_format.get('position')) or None,
                                                          )
			    current_frame = current_frame.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
                            for index, data in data_types.iteritems():
                                current_frame[index] = current_frame[index].astype(data)
                            current_frame.columns = column_names
                        else:
                            current_frame = pd.read_excel(fileName, skipfooter=int(params.get("skipfooter", 0)),
                                                          skiprows=int(params.get("skiprows", 0)),sheet_name=sheet,
                                                          usecols=list(feed_format.get('position')) or None)

                            current_frame = current_frame.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

                        current_frame['FEED_FILE_NAME'] = fileName.split('/')[-1]

#The concatenation is done on currentframe columns
                        df = pd.concat([df, current_frame], join_axes=[current_frame.columns])
            except Exception, e:
                print e
                logger.info(fileName)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if not df.empty:
            # replace non-ascii error characters, if excel contains non-ascii characters.
            df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in date_columns:
                dtpattern = dateCols["datePattern"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['fileColumn']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['fileColumn']] = df.apply(
                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[dateCols['fileColumn']], axis=1)
                    del df['tdlen']
                df[dateCols['fileColumn']] = pd.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        logger.info('Total Records In Excel File : %s' % str(len(df)))
        return df
