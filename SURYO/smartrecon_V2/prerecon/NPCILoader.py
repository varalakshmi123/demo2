import config
from FixedFormatLoader import FixedFormatLoader


class NPCILoader(FixedFormatLoader):
    def __init__(self):

        self.params = {}
        self.feedformatfilecolumns = []

    def getType(self):
        return "NPCI"

    def loadFile(self, fileName, params):
        if params['type'] == 'INWARD':
            params["feedformatFile"] = "NPCI_INWARD_Structure.csv"
        elif params['type'] == 'OUTWARD':
            params["feedformatFile"] = "NPCI_OUTWARD_Structure.csv"
        elif params['type'] == 'UPI_INWARD':
            params["feedformatFile"] = "UPI_INWARD_Structure.csv"
        elif params['type'] == 'UPI_OUTWARD':
            params["feedformatFile"] = "UPI_OUTWARD_Structure.csv"
        elif params['type'] == 'ACQMERCHANT':
            params["feedformatFile"] = "NPCI_Merchant_Structure.csv"
        elif params['type'] == 'ISSMERCHANT':
            params["feedformatFile"] = "UPI_INWARD_MERCHANT_Structure.csv"
        elif params['type'] == 'AEPS_INWARD':
            params["feedformatFile"] = "AEPS_INWARD_Structure.csv"
        elif params['type'] == 'AEPS_OUTWARD':
            params["feedformatFile"] = "AEPS_OUTWARD_Structure.csv"
        else:
            raise KeyError("NPCI loader type not defined for %s" % params['type'])

        npci_data = FixedFormatLoader().loadFile(fileName, params)
        return npci_data
