from traceback import format_exc

import numpy as np
import pandas as pd

import logger
from prerecon.FeedLoader import FeedLoader
import config

logger = logger.Logger.getInstance("smartrecon").getLogger()


class FixedFormatLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["DataType", "DatePattern", "Description", "To", "From"]

    def getType(self):
        return "FixedFormat"

    def loadFile(self, fileNames, params):
        feed_format = params['feedformat']
        dateColumns = feed_format[feed_format['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will infered as floats if there are blanks in read_csv)
        feed_format['dataType'] = feed_format['dataType'].str.replace('np.datetime64', 'str')

        data_types = feed_format[feed_format['dataType'] != ''].set_index(feed_format['fileColumn'])['dataType'].to_dict()
        data_types = {col: eval(dtype) for col, dtype in data_types.iteritems()}

        col_names = feed_format['fileColumn'].tolist()

        feed_format['from'] = feed_format['from'] - 1
        positions = zip(feed_format['from'].astype(int), feed_format['to'].astype(int))

        df = pd.DataFrame(columns=col_names + ['FEED_FILE_NAME'])
        try:
            for fileName in fileNames:
                logger.info('Loading file %s' % fileName)
                currentframe = pd.read_fwf(fileName, header=None, colspecs=positions, names=col_names,
                                           converters=data_types, skiprows=int(params.get("skiprows", 0)),
                                           skipfooter=int(params.get("skipfooter", 0)))

                currentframe['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = df.append(currentframe)
        except Exception as e:
            logger.info(e)
            logger.info(fileNames)
            logger.info(format_exc())
            logger.info("Unable to load the file " + str(file) + ".\n"
                                                                 "Please check that file is readable and is "
                                                                 "as per the configured structure.\n"
                                                                 "If problem persists contact Administrator.")
            raise
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["datePattern"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['fileColumn']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['fileColumn']] = df.apply(
                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                                dateCols['fileColumn']], axis=1)
                    del df['tdlen']
                df[dateCols['fileColumn']] = pd.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                            errors='raise')  # , errors='coerce'
                
        logger.info("Total Records in Fixed-Length file  = %6d" % len(df))
        return df
