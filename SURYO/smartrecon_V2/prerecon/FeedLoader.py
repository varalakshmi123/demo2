import importlib


class FeedLoaderFactory():
    def __init__(self):
        self.params = {}
        self.feedformatfilecolumns = []

    def getInstance(self, fileType):
        module = importlib.import_module("prerecon." + fileType + "Loader")
        my_class = getattr(module, fileType + "Loader")
        return my_class()


class FeedLoader():
    def __init__(self):
        pass

    def getType(self):
        return "Base"

    def loadFile(self, fileName, params, payload={}):
        return None
