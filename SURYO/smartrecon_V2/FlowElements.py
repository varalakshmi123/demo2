import collections
import datetime
import errno
import inspect
import os
import shutil
import sys
import traceback
import uuid
import zipfile
import csv
import json
import numpy as np
import pandas as pd
from pymongo import MongoClient
import re
from pandas.api.types import *
import Matcher
import config
import logger
from Utilities import Utilities
import mailApi
from prerecon import FeedLoader as FeedLoader, FilterExecutor, FindFiles  # , CustomFunctionLoader
from customscripts import CustomFunctions
from customscripts import DownloadReport

logger = logger.Logger.getInstance("smartrecon").getLogger()

class FlowElement:
    def __init__(self):
        self.type = None
        self.name = None
        self.properties = {}
        self.id = uuid.uuid4().hex

    def run(self, payload, debug):
        pass

class FlowRunner:
    def __init__(self, flow, debug=False):
        self.name = None
        self.elements = flow
        # self.lookup = {k['id']: k for k in self.elements}
        self.classMap = dict()
        self.debug = debug
        classes = self.FindAllSubclasses(FlowElement)
        for x in classes:
            self.classMap[x[1]] = x[0]
        self.result = None
        self.debugoutput = {}

    def FindAllSubclasses(self, classType):
        subclasses = []
        callers_module = sys._getframe(1).f_globals['__name__']
        classes = inspect.getmembers(sys.modules[callers_module], inspect.isclass)
        for name, obj in classes:
            if (obj is not classType) and (classType in inspect.getmro(obj)):
                subclasses.append((obj, name))
        return subclasses

    def run(self, payload=None):
        if payload is None:
            payload = dict()
        if len(self.elements) > 0:
            count = 0
            for x in self.elements:
                count += 1
                klass = self.classMap[x["type"]]
                instance = klass()
                instance.properties = x.get("properties", {})
                instance.nodeId = x.get("nodeId", str(count))
                instance.name = instance.properties.get("name", str(x["type"]) + str(count))
                instance.run(payload, self.debug)
                name = instance.name
                jobcompleted = "{0:.0f}%".format((float(count) / len(self.elements)) * 100)
                mClient = MongoClient(config.mongoHost)[config.databaseName]['recon_execution_details_log']
                if count == 1:
                    stmtDate = datetime.datetime.strptime(instance.properties.get("statementDate", ""), '%d-%b-%Y')
                    mClient.insert(
                        {"statementDate": stmtDate, "jobStatus": 'JobRunning', "errorMsg": '',
                         "reconName": payload['reconName'], "reconProcess": payload['reconProcess'],
                         "reconExecutionId": Utilities().getNextSeq(),
                         "executionDateTime": datetime.datetime.now().strftime('%s'), "jobcompleted": jobcompleted,
                         "partnerId": "54619c820b1c8b1ff0166dfc"})

                else:
                    doc = mClient.find_one(
                        {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
                        sort=[('reconExecutionId', -1)])

                    mClient.update(
                        {"reconName": payload['reconName'], "reconExecutionId": doc['reconExecutionId']},
                        {'$set': {"jobcompleted": jobcompleted}})

                if not name:
                    name = "NoName"
                name += "@" + instance.type + str(count)
                # self.debugoutput[name] = copy.deepcopy(payload)
                if 'error' in payload:
                    print "Error in stage" + str(x)
                    break
        status = 'Failed' if 'error' in payload.keys() else "Success"
        self.result = payload
        if "ignoredb" not in self.elements[0].get("properties", {}):
            self.updateJobStatus(status=status, payload=payload)

    def updateJobStatus(self, status='Success', payload={}):
        mongoClient = MongoClient(config.mongoHost)[config.databaseName]

        errorMsg = ''
        if status == 'Failed':
            for error in payload['error'].get('traceback', '').split('\\n'):
                print error
                logger.info(error)
            mongoClient['master_dumps'].remove(
                {"reconName": payload['reconName'],"statementDate":payload['statementDate']})
            errorMsg = payload['error'].get('exception', 'Error Running Job, Contact Admin.')
            logger.info('Exception Message : %s' % errorMsg)

        # mongoClient['recon_execution_details_log'].insert(
        #     {"statementDate": payload['statementDate'], "jobStatus": status, "errorMsg": errorMsg,
        #      "reconName": payload['reconName'], "reconExecutionId": Utilities().getNextSeq(),
        #      "executionDateTime": datetime.datetime.now().strftime('%s'), "partnerId": "54619c820b1c8b1ff0166dfc"})

        doc = mongoClient['recon_execution_details_log'].find_one(
            {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
            sort=[('reconExecutionId', -1)])

        mongoClient['recon_execution_details_log'].update(
            {"reconName": payload['reconName'], "reconExecutionId": doc['reconExecutionId']},
            {'$set': {"jobStatus": status, 'errorMsg': errorMsg}})

class Initializer(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Initializer"
        self.properties = dict(statementDate="", basePath="", outputPath="", recontype="", compressionType="")

    def run(self, payload, debug):
        try:
            stmtDate = datetime.datetime.strptime(self.properties.get("statementDate", ""), '%d-%b-%Y')
            stmtDateFormat = stmtDate.strftime('%d%m%Y')
            baseDir = self.properties['basePath']
            compressionType = self.properties.get('compressionType', '') or ''
            extract_path = os.path.join(baseDir, self.properties.get('reconName', ''), stmtDate.strftime('%d%m%Y'))
            # take upload file name from user / default to statement date

            # automaticFileDownload = True
            # attribute = 'pvrReconReportDownload'
            # if automaticFileDownload:
            #     fname = getattr(DownloadReport(),attribute)(self.properties.get('reconName', ''),self.properties.get("statementDate",""))
            #     self.properties['uploadFileName'] = fname

            uploadFileName = ""
            if compressionType != "":
                uploadFileName = self.properties['uploadFileName'] if self.properties.get('uploadFileName',
                                                                                          '') else stmtDateFormat + '.' + compressionType

            source_path = os.path.join(baseDir, *[self.properties.get('reconName', ''), uploadFileName])

            payload['statementDate'] = stmtDate
            payload['reconName'] = self.properties.get('reconName', '')
            payload['reconProcess'] = self.properties.get('reconProcess', '')
            payload['dropColumns'] = []
            payload['summaryColumns'] = {}
            if "ignoredb" not in self.properties:
                mClient = MongoClient(config.mongoHost)[config.databaseName]['recon_execution_details_log']
                # mClient.insert(
                #     {"statementDate": payload['statementDate'], "jobStatus": 'JobRunning', "errorMsg": '',
                #      "reconName": payload['reconName'], "reconExecutionId": Utilities().getNextSeq(),
                #      "executionDateTime": datetime.datetime.now().strftime('%s'),
                #      "partnerId": "54619c820b1c8b1ff0166dfc"})
                if self.properties.get('incremental', False):
                    payload['incremental'] = True
                elif mClient.find_one({"statementDate": stmtDate, "jobStatus": "Success",
                                       "reconName": self.properties.get('reconName', '')}):
                    payload['error'] = dict(elementName="Initializer", type="critical",
                                            exception="Statement date already exists :%s" % stmtDate.strftime(
                                                '%d-%m-%Y'))
                    return
                else:
                    pass

            if not os.path.exists(source_path):
                os.mkdir(source_path)

            if os.path.isfile(source_path) and compressionType != "":
                extractFile = zipfile.ZipFile(source_path, mode='a')
                if not os.path.exists(os.path.join(extract_path)):
                    os.mkdir(extract_path)

                # check for old files in the uploaded path from previous jobs, remove & over-write
                # TODO, how to handle the same on incremental run.
                if os.listdir(extract_path) != [] and not payload.get('incremental', False):
                    shutil.rmtree(extract_path)

                extractFile.extractall(extract_path)
            elif compressionType != "":
                payload['error'] = dict(elementName="Initializer", type="critical",
                                        exception="File %s not available in recon mft path" % uploadFileName)
                return

            mftPath = self.properties.get('outputPath', '') or os.path.join(extract_path)

            if not os.path.exists(mftPath):
                os.mkdir(mftPath)

            output_path = self.properties.get('outputPath', '') or os.path.join(extract_path, 'OUTPUT')

            if not os.path.exists(output_path):
                os.mkdir(output_path)

            # check for pre execution's
            execution_doc = None
            if not "ignoredb" in self.properties:
                execution_doc = mClient.find_one(
                    {"reconName": self.properties['reconName'], "jobStatus": "Success"},
                    sort=[('reconExecutionId', -1)])
            if execution_doc:
                payload['prevStmtDate'] = execution_doc['statementDate'].strftime('%d%m%Y')

            payload['source_path'] = extract_path
            payload['output_path'] = output_path
        except Exception as e:
            payload['error'] = dict(elementName="Initializer", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class PreLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DataLoader"
        self.properties = dict(loadType="", source="", feedPattern="", feedParams=dict(), feedFilters=[],
                               resultkey="dataframe")

    def run(self, payload, debug):
        try:
            util = Utilities()
            inst = pd.DataFrame()
            feed_factory = FeedLoader.FeedLoaderFactory()
            findFiles = FindFiles.FindFiles(payload['statementDate'])
            mdb = MongoClient(config.mongoHost)[config.databaseName]
            recon_meta_info = mdb['reconMetaInfo'].find_one({"reconName": payload['reconName']})
            customScriptList = mdb['custom_scripts'].find_one({'reconName': payload['reconName']})

            # load structure for the sources
            structures = mdb['sourceReference'].find(
                {"reconName": payload['reconName']})

            if structures.count() == 0:
                return

            structures = sorted(list(structures), key=lambda structures: structures['position'])
            sourceList = []
            source_wise_mapped_cols = {}
            reorderDict={}
            source_wise_feed_format={}
            for struct in structures:
                feed_params = dict(payload=payload)
                # if not struct.get('referenceSource'):
                #     if struct['source'] not in reorderDict.keys():
                #         reorderDict[struct['source']]={}
                #     for val in struct['feedDetails']:
                #         reorderDict[struct['source']][val['fileColumn']]=val['reorderPosition']
                feed_def = pd.DataFrame(struct['feedDetails']).fillna('')
                required_fields = feed_def[feed_def['required'].replace('', False)]
                required_fields['position'] = required_fields['position'].astype(np.int64) - 1
                feed_params['feedformat'] = required_fields

                # feed_params (skip header, footer, etc defined in config)
                for param in config.feed_params.get(struct.get('loadType', ''), []):
                    if struct.get(param):
                        feed_params[param] = struct.get(param)
                    else:
                        pass

                # SFMS Multisheet loader to use the columnNames to compute skip header & footer
                feed_params['columnNames'] = feed_params.get('columnNames', ','.join(feed_def['fileColumn']))

                if struct.get('static'):
                    feedfac = FeedLoader.FeedLoaderFactory()
                    instance = feedfac.getInstance(struct.get('loadType', ''))
                    file_names = [config.referencePath + struct.get('filePattern','') ]
                    inst = instance.loadFile(file_names, feed_params)
                    if self.properties.get('feedFilters', []):
                        inst = FilterExecutor.FilterExecutor().run(inst, struct.get('feedFilters', []),
                                                                   payload=payload)
                    payload[struct['struct']] = inst
                else:
                    if struct['source'] not in sourceList and not struct.get('referenceSource'):
                        sourceList.append(struct['source'])

                    inst_files = findFiles.findFiles(source=struct['source'],
                                                 feedPath=payload.get('source_path', ''),
                                                     fpattern=struct.get('filePattern', ''),
                                                     delta=struct.get('timeDelta', 0),
                                                     raiseError=struct.get('raiseError', True))

                    # Use to move files to processed folder on incremental loading
                    if 'filesLoaded' not in payload:
                        payload['filesLoaded'] = []
                    payload['filesLoaded'].extend(inst_files)

                    if struct['source'] not in source_wise_feed_format.keys():
                        source_wise_feed_format[struct['source']] = feed_params['feedformat'].copy()

                    inst = feed_factory.getInstance(struct.get('loadType', '')).loadFile(inst_files, feed_params)
                    #uniqcols = [doc['mappedColumn'] for doc in struct.get('mappedColumns', [])]
                    #uniqcols = set(inst.columns.tolist()).intersection(uniqcols)
                    #idxlist = []
                    #for col in uniqcols:
                    #    idxlist.append(inst[col])

                    # inst['UNIQUE_SYS_COL']= inst.iloc[:,np.r_[0,1,5,8][','.join(idxlist)]].apply(lambda x: "$".join(x.astype(str)), axis=1)
                    # inst/['UNIQUE_SYS_COL'] =''
                    # for index, row in inst.iterrows():
                    #     for col in uniqcols:
                    #         inst.loc[index,'UNIQUE_SYS_COL']+="".join(str(inst.loc[index,col]))
                    #
                    # inst['UNIQUE_SYS_COL']=inst['UNIQUE_SYS_COL'].str.replace(',','')
                    if struct.get('filters', []):
                        logger.info('-------------------------------------------------')
                        logger.info('Applying Feed Filters')
                        inst = FilterExecutor.FilterExecutor().run(inst, struct.get('filters', []),
                                                                   payload=payload)
                        logger.info('Total records post filter %s: ' % len(inst))

                    # rename feed columns with mapped columns --> {"src_col" : "ref_col"}
                    mapped_cols = {col['mappedColumn']: col['keyCols'] for col in struct.get('mappedColumns', [])}
                    if struct['source'] not in source_wise_mapped_cols.keys():
                        source_wise_mapped_cols[struct['source']] = mapped_cols
                    inst.rename(columns=mapped_cols, inplace=True)

                    payload['summaryColumns'].update({struct['source']: {'aggrCols': [x['keyCols'] for x in
                                                                                      struct.get('mappedColumns', []) if
                                                                                      x['DataType'] == 'float']}})

                    inst['SOURCE'] = struct['source']
                    inst['STATEMENT_DATE'] = payload['statementDate']

                    if struct['source'] in payload.keys():
                        tmp_df = pd.concat([payload[struct['source']], inst],
                                           join_axes=[inst.columns])
                        tmp_df['System_Idx'] = tmp_df[tmp_df.columns[0]].apply(
                            lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                        payload[struct['source']] = tmp_df

                    else:
                        tmp_df = inst.reset_index(drop=True)
                        tmp_df['System_Idx'] = tmp_df[tmp_df.columns[0]].apply(
                            lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                        payload[struct['source']] = tmp_df
            self.loadSourceWiseCfwd(payload, sourceList, recon_meta_info, source_wise_mapped_cols)
            logger.info('Total data loaded in %s is %s' % (struct['source'], len(inst)))
        except Exception as e:
            payload['error'] = dict(elementName="PreLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    def load_cf_records(self, source, inst, payload):
        if 'prevStmtDate' in payload.keys():
            self.properties['sourceName'] = source
            cfInst = FeedLoader.FeedLoaderFactory().getInstance('CarryForward')
            self.properties['dtypes'] = {col: inst[col].dtype.type for col in inst.columns}
            cfData = cfInst.loadFile([], {"payload": payload, "properties": self.properties})
            logger.info('-------------------------------------------------')
            logger.info('Total Carry Forward records %s: ' % len(cfData))
            cfData['CARRY_FORWARD'] = 'Y'
            if 'LINK_ID' in cfData.columns:
                cfData['LINK_ID'] = cfData['LINK_ID'].astype(str)

            # set index on cfData to retain derived columns if any exists.
            inst = pd.concat([inst, cfData], join_axes=[cfData.columns]).reset_index(drop=True)
        else:
            inst['CARRY_FORWARD'] = ''

        return inst

    def loadSourceWiseCfwd(self, payload, sourceList, metaInfo, mapped_cols):
        util = Utilities()
        for source in sourceList:
            source_meta_info = metaInfo.get(source, {}) if metaInfo else {}

            if source_meta_info.get('disableCarryForward', False):
                inst = payload[source]
                logger.info("Carry forward disabled for source %s" % source)

            else:
                inst = self.load_cf_records(source, payload[source], payload)
                if source_meta_info.get('incrementLoader'):
                    inst = self.load_inc_data(inst, mapped_cols[source].values(), payload)

            inst['Ageing'] = (datetime.datetime.now() - inst['STATEMENT_DATE']).dt.days
            inst['Ageing'] = inst['Ageing'].astype(str)

            for col in inst.columns.tolist():
                inst[col].fillna(util.infer_dtypes(inst[col]), inplace=True)

            util.setValues(payload, source, set_value=inst)

    def load_inc_data(self, inst, key_cols, payload):
        util = Utilities()
        if 'prevStmtDate' in payload.keys():
            source = inst['SOURCE'].unique()[0]
            source_key_cols = key_cols[inst['SOURCE'].unique()[0]]

            # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)

            inst['CARRY_FORWARD'] = inst['CARRY_FORWARD'].fillna('')
            current_df = inst.copy()

            prev_exec_path = config.mftpath + "/{reconName}/{stmtDate}/OUTPUT/".format(
                stmtDate=payload['prevStmtDate'], reconName=payload['reconName'])

            matched_df = pd.read_csv(prev_exec_path + inst['SOURCE'].unique()[0] + '.csv',
                                     converters={})

            source_key_val = [val for key, val in source_key_cols.iteritems()]

            # check if reversal records exists
            reversal_path = prev_exec_path + 'Self Matched.csv'

            if os.path.isfile(reversal_path):
                reversal_records = pd.read_csv(reversal_path, converters={})
                matched_df = pd.concat([matched_df, reversal_records]).drop_duplicates(subset='UNIQUE_SYS_COL',
                                                                                       keep='first')

            force_match_path = prev_exec_path + source + '_ForceMatched.csv'

            if os.path.isfile(force_match_path):
                forcematch_records = pd.read_csv(force_match_path, converters={})
                matched_df = pd.concat([matched_df, forcematch_records]).drop_duplicates(subset='UNIQUE_SYS_COL',
                                                                                         keep='first')

            matched_df.drop(columns=['LINK_ID','System_Idx'], errors='ignore', inplace=True)
            # drop duplicates to avoid cartesian products
            current_df = current_df.drop_duplicates(subset='UNIQUE_SYS_COL', keep='first')

            for col in current_df.columns.tolist():
                current_df[col].fillna(util.infer_dtypes(current_df[col]), inplace=True)

            left_out_data = current_df.merge(matched_df[source_key_val+['UNIQUE_SYS_COL']], on='UNIQUE_SYS_COL', indicator=True,
                                             suffixes=('', '_y'), how='outer')

            for col in left_out_data.columns.values:
                if col.endswith("_y"):
                    del left_out_data[col]

            left_out_data['CARRY_FORWARD'].fillna('', inplace=True)
            left_out_data = left_out_data[
                (left_out_data['_merge'] == 'left_only') | (left_out_data['CARRY_FORWARD'] == 'Y')]
            del left_out_data['_merge']

            merge_cols = source_key_val + ['CARRY_FORWARD']
            inst = inst.merge(left_out_data[merge_cols + ['UNIQUE_SYS_COL', 'System_Idx']],
                              on=['UNIQUE_SYS_COL', 'System_Idx'], indicator=True, how='inner', suffixes=('', '_y'))

            inst = inst[inst['_merge'] == 'both']
            for col in inst.columns.values:
                if col.endswith("_y"):
                    del inst[col]
            del inst['_merge']
        else:
            pass

        return inst

class FilterDuplicates(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "FilterDuplicates"
        self.name = "FilterDuplicates"
        self.properties = dict(source=None, duplicateStore=None, allowOneInMatch=True, keyColumns=[], sourceTag='',
                               inplace=True, markerCol='', resultKey='')

    def run(self, payload, debug):
        try:
            data = payload[self.properties["source"]]

            if 'allowOneInMatch' in self.properties and self.properties['allowOneInMatch']:
                keep = 'first'
            else:
                keep = False

            if not self.properties['duplicateStore'] in payload.keys():
                payload[self.properties['duplicateStore']] = {}

            if data.empty:
                pass
            else:
                duplicate_idx = data.duplicated(subset=self.properties["keyColumns"], keep=keep)

                if self.properties.get('inplace', False):
                    if 'markerCol' not in self.properties.keys():
                        raise KeyError("For inplace, markerCol is mandatory input.")
                    data[self.properties['markerCol']] = ''
                    data.loc[duplicate_idx, self.properties['markerCol']] = 'DUPLICATED'
                    payload['dropColumns'].append(self.properties['markerCol'])
                else:
                    payload[self.properties["duplicateStore"]][self.properties['sourceTag']] = data[duplicate_idx]
                    data = data.drop_duplicates(subset=self.properties["keyColumns"], keep=keep)

            payload[self.properties.get('resultKey', self.properties["source"])] = data
        except Exception as e:
            payload['error'] = dict(elementName="FilterDuplicates", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class NWayMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "NWayMatch"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="", subsetfilter=[])],
                               matchResult=None)

    def run(self, payload, debug):
        logger.info("N-Way Match Initiated")
        result = {}
        remainder = []
        util = Utilities()
        for i in range(0, len(self.properties["sources"])):
            source = self.properties["sources"][i].copy()

            data = Utilities().getValues(payload, source['source'], sep='.').reset_index(drop=True)

            # Do not allow matched records & records with null values
            sub_filters = self.properties["sources"][i].get("subsetfilter", [])
            sub_filters.extend(["df = df[df['%s'] != 'MATCHED']" % col for col in data.columns if ' Match' in col])

            match_cols = source['columns'].get('matchColumns', []) + source['columns'].get('sumColumns', [])
            # match_cols.append(source['columns'].get('sumColumns', []))
            for col in match_cols:
                if data[col].dtype == object:
                    sub_filters.append("df = df[df['{col}'].notnull() & (df['{col}']!='')]".format(col=col))
                else:
                    sub_filters.append("df = df[df['{col}'].notnull()]".format(col=col))
            filtered_data = FilterExecutor.FilterExecutor().run(data, sub_filters, payload, "Filter" + str(i))

            util.setValues(payload, source['source'], set_value=filtered_data)
            remaining_data = data[~data.index.isin(filtered_data.index)]
            remainder.append(remaining_data)

        if len(self.properties["sources"]) == 1:
            self.singlesidematch(payload, result, remainder)
            return

        for i in range(0, len(self.properties["sources"]) - 1):
            leftsource = self.properties["sources"][i]
            leftcolumns = leftsource["columns"]
            for j in range(i + 1, len(self.properties["sources"])):
                rightsource = self.properties["sources"][j]
                rightdata = util.getValues(payload, rightsource['source'], sep='.')
                rightcolumns = rightsource["columns"]

                leftdata = util.getValues(payload, leftsource['source'], sep='.')

                left, right = Matcher.Matcher().match(leftdata, rightdata,
                                                      leftsumColumns=leftcolumns.get('sumColumns', []),
                                                      rightsumColumns=rightcolumns.get('sumColumns', []),
                                                      leftmatchColumns=leftcolumns.get('matchColumns', []),
                                                      rightmatchColumns=rightcolumns.get('matchColumns', []),
                                                      isToleranceMatch=self.properties.get('isToleranceMatch', False),
                                                      toleranceValue=self.properties.get('toleranceValue', None)
                                                      )

                if rightsource["sourceTag"] + " Match" in left.columns:
                    del left[rightsource["sourceTag"] + " Match"]

                if leftsource["sourceTag"] + " Match" in right.columns:
                    del right[leftsource["sourceTag"] + " Match"]

                if rightsource["sourceTag"] + " CarryForward" in left.columns:
                    del left[rightsource["sourceTag"] + " CarryForward"]

                if leftsource["sourceTag"] + " CarryForward" in right.columns:
                    del right[leftsource["sourceTag"] + " CarryForward"]

                left = left.rename(columns={'Matched': rightsource["sourceTag"] + " Match"})
                right = right.rename(columns={'Matched': leftsource["sourceTag"] + " Match"})

                left = left.rename(columns={'Matched': rightsource["sourceTag"] + " Match",
                                            "right_carryforward": rightsource['sourceTag'] + ' CarryForward'})
                right = right.rename(columns={'Matched': leftsource["sourceTag"] + " Match",
                                              "left_carryforward": leftsource['sourceTag'] + ' CarryForward'})

                util.setValues(payload, ref=self.properties['sources'][i]['source'], set_value=left)
                util.setValues(payload, ref=self.properties['sources'][j]['source'], set_value=right)

        for i in range(0, len(self.properties["sources"])):
            result_df = util.getValues(payload, self.properties["sources"][i]["source"])
            result_df = pd.concat([result_df, remainder[i]], join_axes=[result_df.columns]).reset_index(drop=True)

            # force all blank status columns to unmatched
            match_cols = [col for col in result_df.columns if ' Match' in col]
            if match_cols:
                result_df[match_cols] = result_df[match_cols].fillna('UNMATCHED')

            util.setValues(result, self.properties["sources"][i]["sourceTag"], result_df)

        # assign will over-write the existing data
        for srcTag, value in result.iteritems():
            util.setValues(payload, 'results' + '.' + srcTag, set_value=value)

    def singlesidematch(self, payload, result, remainder):
        source = self.properties["sources"][0]
        columns = source["columns"]
        data = Utilities().getValues(payload, source["source"])

        if columns.get('debitCreditColumn'):
            cr_dr_indicator = columns.get('debitCreditColumn')
        else:
            raise KeyError('Dr/Cr mandatory for single side match, not defined.')

        # data[cr_dr_indicator] = data[cr_dr_indicator].apply(lambda x: re.match(x[0], x, re.I).group().upper())
        credit = data[data[cr_dr_indicator] == source.get('crSign', 'C')]
        debit = data[data[cr_dr_indicator] == source.get('drSign', 'D')]
        left, right = Matcher.Matcher().match(credit, debit,
                                              leftmatchColumns=columns.get('matchColumns', []) + columns.get(
                                                  'sumColumns', []),
                                              rightmatchColumns=columns.get('matchColumns', []) + columns.get(
                                                  'sumColumns', []))
        selfmatched = pd.concat([left, right])

        # drop psuedo columns appended by matcher function
        selfmatched.drop(labels=['right_carryforward', 'left_carryforward'], axis='columns', errors='ignore',
                         inplace=True)

        selfmatched = selfmatched.rename(columns={'Matched': "Self Matched"})
        selfmatched.loc[selfmatched["Self Matched"] == 'UNMATCHED', "Self Matched"] = ''

        payload[self.properties["sources"][0]["source"]] = selfmatched
        result[self.properties["sources"][0]["sourceTag"]] = payload[self.properties["sources"][0]["source"]].append(
            remainder[0])
        data = payload[self.properties["sources"][0]["source"]].append(remainder[0])
        Utilities().setValues(payload, self.properties['resultkey'], data)


class ExpressionEvaluator(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ExpressionEvaluator"

        self.properties = dict(expressions=[], source="dataframe", resultkey="dataframe")

    def run(self, payload, debug):
        try:
            expressions = self.properties.get('expressions', [])
            df = Utilities().getValues(payload, self.properties['source']).copy()

            df = FilterExecutor.FilterExecutor().run(df, expressions, payload=payload)

            Utilities().setValues(payload, self.properties['resultkey'], set_value=df)
        except Exception as e:
            payload['error'] = dict(elementName="ExpressionEval", type="critical", exception=str(e),
                                    traceback=traceback.format_exc(), expression=expressions)


class DumpData(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DumpData"

        self.properties = dict()

    def run(self, payload, debug):
        try:
            self.generateReconSummary(payload, self.properties.get('matched', ''))
            outputPath = payload['source_path'] + '/' + 'OUTPUT/'
            logger.info('Storing data in path %s' % outputPath)
            if not os.path.exists(os.path.dirname(outputPath)):
                try:
                    os.makedirs(os.path.dirname(outputPath))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            mode, header = 'w', True
            if payload.get('incremental', False) and os.listdir(outputPath):
                mode, header = 'a', False

            float_format = '%.{roundTo}f'.format(roundTo=int(self.properties.get('roundTo', 4)))
            # segreate matched and un-matched reports
            writer = pd.ExcelWriter(outputPath + 'ConsolidatedReport.xlsx')
            mongoClient = MongoClient(config.mongoHost)[config.databaseName]
            for key, data in payload.get('results', {}).iteritems():

                matchkey_cols = []
                match_cond = []
                un_match_cond = []
                original_len = len(data)

                # replace all non-ascii characters with spaces
                data.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True, inplace=True)
                columnDtype = []
                if not mongoClient['columnDtypes'].find({"reconName": payload['reconName'], "source": key}).count() > 0:
                    for index, dtype in enumerate(data.dtypes):
                        columnDtype.append({'column': data.columns[index], 'dtype': self.typeMapper(str(dtype))})
                    mongoClient['columnDtypes'].insert_one(
                        {"reconName": payload['reconName'], "partnerId": "54619c820b1c8b1ff0166dfc", "source": key,
                         "dtypes": columnDtype})

                drop_cols = payload.get("dropColumns", [])
                if drop_cols:
                    data.drop(labels=drop_cols, axis='columns', errors='ignore', inplace=True)

                for col in data.columns:
                    if col.endswith(' Match') and 'Self Matched' != col:
                        matchkey_cols.append(col)
                        match_cond.append("(data['%s'] == 'MATCHED')" % col)
                        un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
                    else:
                        pass

                reversal_data = pd.DataFrame()
                if 'Self Matched' in data.columns:
                    data['Self Matched'] = data['Self Matched'].fillna('')
                    reversal_data = data[data['Self Matched'] == 'MATCHED']
                    data = data[data['Self Matched'] != 'MATCHED']

                    if not reversal_data.empty:
                        reversal_data.to_csv(outputPath + "%s_Self_Matched.csv" % key, index=False, mode=mode,
                                             header=header, date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,
                                             encoding='utf-8')
                        self.writeToExcel(writer, reversal_data, self.properties.get('exportType'), key + 'Reversals')

                if self.properties.get('matched', '') == 'any':
                    matched_df = data[eval(" | ".join(match_cond))]
                    un_matched_df = data[eval(" & ".join(un_match_cond))]
                else:
                    matched_df = data[eval(" & ".join(match_cond))]
                    un_matched_df = data[eval(" | ".join(un_match_cond))]

                # Filter subset records back to matched queue.
                subset_records = pd.DataFrame()
                if self.properties.get('subsetfilters', {}) and self.properties['subsetfilters'].get(key, []):
                    subset_records = FilterExecutor.FilterExecutor().run(data,
                                                                         self.properties['subsetfilters'].get(key, []))
                    if not subset_records.empty:
                        matched_df = pd.concat([matched_df, subset_records], join_axes=[matched_df.columns])

                # validation to ensure there is no missing data post filters
                post_filter_count = len(matched_df) + len(un_matched_df) + len(reversal_data)
                if original_len != post_filter_count:
                    ref_file_path = '/tmp/%s_%s.csv' % (key, datetime.datetime.now().strftime('%d%m%y_%H%M%S'))
                    data.to_csv(ref_file_path, index=False)
                    logger.info('-------------------------------------------------')
                    logger.info("Mismatch in data length post filter in DumpData")
                    logger.info("Resolution: use subsetfilters to restore filtered records")
                    logger.info("Extract path : %s" % ref_file_path)
                    raise Exception(
                        "Mismatch in data length for source '%s' original length %s , post filter %s" % (
                            key, len(data), post_filter_count))

                if 'Exception Remarks' in matched_df.columns.tolist():
                    del matched_df['Exception Remarks']
                matched_df.to_csv(outputPath + "%s.csv" % key, index=False, mode=mode, header=header,
                                  date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                un_matched_df.to_csv(outputPath + "%s_UnMatched.csv" % key, index=False,
                                     date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')

                self.writeToExcel(writer, data, self.properties.get('exportType'), key)

            if 'duplicateStore' in payload.keys():
                for key, data in payload['duplicateStore'].iteritems():
                    data['Remarks'] = 'Duplicated'
                    data.to_csv(outputPath + '%s_Duplicated.csv' % key, index=False, mode=mode, header=header,
                                date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                    self.writeToExcel(writer, data, self.properties.get('exportType'), key + 'Duplicates')

            if 'custom_reports' in payload.keys():
                for key, data in payload['custom_reports'].iteritems():
                    data.to_csv(outputPath + '%s_Report.csv' % key, mode=mode, header=header, index=False,
                                date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                    self.writeToExcel(writer, data, self.properties.get('exportType'), key)

            if 'reconSummary' in payload.keys():
                payload['reconSummary'].to_csv(outputPath + 'recon_summary.csv', mode=mode, header=header, index=False,
                                               date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,
                                               encoding='utf-8')

                self.writeToExcel(writer, payload['reconSummary'], self.properties.get('exportType'),
                                  payload['reconName'] + 'Summary')

            if payload.get('incremental', False):
                self.moveToProcessed(payload=payload)

            self.generateReconMetaInfo(payload)

            writer.save()

            logger.info('=====> Recon Execution Completed <=====')
        except Exception, e:
            payload['error'] = dict(elementName="DumpData", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    # Move files to processed folder to avoid duplicate data loading

    def typeMapper(self, dtype):
        if dtype == 'object':
            return 'str'
        elif dtype == 'float64':
            return 'np.float64'

        elif dtype == 'int64':
            return 'np.int64'
        else:
            return 'np.datetime64'

    def reorderColumns(self,key, data, payload):
        if key in payload['reorderColumns'].keys():
            reorderDict=payload['reorderColumns'][key]
            derivedCols=list(set(data.columns.tolist())-set(reorderDict.keys())-set(['SOURCE']))
            reorderCols=[]
            for i in sorted(reorderDict.values()):
                for colkey,val in reorderDict.iteritems():
                    if i==val and colkey in data.columns:
                        reorderCols.append(colkey)
            columnList=['SOURCE']+reorderCols+derivedCols
        data=data[columnList]
        return data

    def moveToProcessed(self, payload={}):
        processed_path = payload['source_path'] + '/PROCESSED'
        if not os.path.exists(processed_path):
            os.mkdir(processed_path)

        for file in payload.get('filesLoaded'):
            exists = processed_path + os.sep + file.split('/')[-1]
            if os.path.exists(exists):
                os.remove(exists)

            if os.path.isfile(file):
                shutil.move(file, processed_path)

    def writeToExcel(self, excelWriter=None, dataframe=None, exportType='', key=''):
        if exportType != 'singleSheet':
            dataframe.to_excel(excelWriter, sheet_name=key, index=False)
        else:
            row = 0
            spaces = 1
            header_row = pd.DataFrame([{"header": key}])
            header_row.to_excel(excelWriter, sheet_name=key, startrow=row, startcol=0, index=False,
                                header=None)
            row += 1
            dataframe.to_excel(excelWriter, sheet_name=key, startrow=row, startcol=0, index=False)
            row = row + len(dataframe.index) + spaces + 1

    def generateReconSummary(self, payload, matchType):
        try:
            summary_dict = dict(reconName=payload['reconName'], statementDate=payload['statementDate'],
                                reconType=payload['reconProcess'], reportName="ReconSummary", details=[])

            summary_list = []
            summary_source = []
            reorder_cols = ["Source"]
            totalInput = 0
            # todo ui should send groupby columns as an array.
            summary_cols = collections.OrderedDict(
                [("Matched Amount", 0), ("Matched Count", 0.0), ("UnMatched Count", 0),
                 ("UnMatched Amount", 0.0), ("Reversal Amount", 0.0), ("Reversal Count", 0),
                 ("Carry-Forward Matched Amount", 0.0), ("Carry-Forward Matched Count", 0),
                 ("Carry-Forward UnMatched Amount", 0.0), ("Carry-Forward UnMatched Count", 0),
                 ("Force Matched Count", 0), ("Force Matched Amount", 0.0)])

            for source, data in payload.get('results', {}).iteritems():
                # check if data exists (incremental loads may generate empty dataframes)
                if data.empty:
                    totalInput += len(data)
                    pass
                else:
                    reporting_cols = payload['summaryColumns'].get(source, {})
                    reorder_cols.extend(reporting_cols.get('GroupBy', []))
                    totalInput += len(data)
                    list,summary =self.compute_stats(data.copy(), aggrCol=reporting_cols.get('aggrCols', []),
                                           groupbyCol=["Source"] + reporting_cols.get('GroupBy', ['STATEMENT_DATE']),
                                           matchType=matchType)
                    summary_list.extend(list)
                    summary_source.extend(summary)

            if len(summary_list) == 0:
                return

            summary_df = pd.DataFrame(summary_list)
            source_df = pd.DataFrame(summary_source)

            # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
            rename_mapper = {}
            for col in summary_df.columns:
                if 'Amount#' in col or 'Count#' in col:
                    aggr, match_status = tuple(col.split('#'))
                    rename_mapper[col] = match_status + ' ' + aggr
                else:
                    rename_mapper[col] = col.rstrip('#')

            #missing and Reorder of Columsn
            summary_df = self.missingSummary(summary_df,summary_cols)
            summary_source = self.missingSummary(source_df,summary_cols)

            # add missing summary cols to dataframe to avoid column alignment issues
            for column, value in summary_cols.iteritems():
                if column in summary_df.columns:
                    summary_df[column] = summary_df[column].fillna(value=value)
                else:
                    summary_df[column] = value

            # merge closing balances on sources if exists
            if 'closingBalances' in payload.keys():
                closing_balances = pd.DataFrame(payload['closingBalances'])
                summary_df = summary_df.merge(closing_balances, on='Source', how='left')
            else:
                summary_df['Closing Balance'] = 0.0

            # Re-order source columns to first index
            source = summary_df[reorder_cols]
            summary_df.drop(labels=reorder_cols, axis=1, inplace=True)
            summary_df = summary_df[summary_cols.keys() + ['Closing Balance']]

            for idx, value in enumerate(reorder_cols):
                summary_df.insert(idx, value, source.loc[:, value])

            if 'STATEMENT_DATE' in self.properties.get('groupbyCol', []):
                pass
            else:
                summary_df.insert(len(reorder_cols), 'Statement Date', payload['statementDate'])

            summary_df.insert(len(reorder_cols) + 1, 'Execution Date Time', datetime.datetime.now())
            summary_df.sort_values(by='Execution Date Time', inplace=True, ascending=False)

            summary_df.fillna('', inplace=True)
            summary_source.fillna('',inplace=True)

            mongoClient = MongoClient(config.mongoHost)[config.databaseName]
            doc = mongoClient['recon_execution_details_log'].find_one(
                {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
                sort=[('reconExecutionId', -1)])

            payload['reconSummary'] = summary_df
            # Insert Statement Date Summary
            summary_dict['details'] = summary_df.to_dict(orient='records')
            mongoClient['custom_reports'].insert(summary_dict)

            # Over writing Same dict details and writing Source wise summary
            summary_dict['details'] = summary_source.to_dict(orient='records')
            summary_dict['reconExecutionId'] = doc['reconExecutionId']
            summary_dict['totalInput'] = totalInput
            summary_dict['totalMatched'] = summary_df['Matched Count'].sum() + summary_df['Reversal Count'].sum() + \
                                           summary_df['Carry-Forward Matched Count'].sum()
            summary_dict['totalUnmatched'] = summary_df['UnMatched Count'].sum() + summary_df[
                'Carry-Forward UnMatched Count'].sum()
            summary_dict['totalMatchedAmount'] = summary_df['Matched Amount'].sum() + summary_df[
                'Reversal Amount'].sum() + summary_df['Carry-Forward Matched Amount'].sum()
            summary_dict['totalUnmatchedAmount'] = summary_df['UnMatched Amount'].sum() + summary_df[
                'Carry-Forward UnMatched Amount'].sum()
            mongoClient['recon_summary'].insert(summary_dict)

        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="GenerateReconSummary", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    def missingSummary(self,summary,summary_cols):
        # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
        rename_mapper = {}
        for col in summary.columns:
            if 'Amount#' in col or 'Count#' in col:
                aggr, match_status = tuple(col.split('#'))
                rename_mapper[col] = match_status + ' ' + aggr
            else:
                rename_mapper[col] = col.rstrip('#')

        summary.rename(columns=rename_mapper, inplace=True)

        # add missing summary cols to dataframe to avoid column alignment issues
        for column, value in summary_cols.iteritems():
            if column in summary.columns:
                summary[column] = summary[column].fillna(value=value)
            else:
                summary[column] = value
        return summary

    def compute_stats(self, data=None, aggrCol=[], groupbyCol=[], matchType='all'):
        data['matched_status'] = ''
        data['Count'] = 1
        data['Amount'] = data[aggrCol].fillna(0.0).sum(axis=1)
        data.rename(columns={"SOURCE": "Source"}, inplace=True)

        # segregate matched and un-matched transactions
        match_cond = []
        un_match_cond = []
        for col in data.columns:
            if ' Match' in col and 'Self Matched' != col:
                match_cond.append("(data['%s'] == 'MATCHED')" % col)
                un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
            else:
                pass

        # if matched == 'any', if either of the source is matched assume the records to be matched.
        if matchType == 'any':
            data.loc[eval(" | ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" & ".join(un_match_cond)), "matched_status"] = 'UnMatched'
        else:
            data.loc[eval(" & ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" | ".join(un_match_cond)), "matched_status"] = 'UnMatched'

        if 'Self Matched' in data.columns:
            data['Self Matched'] = data['Self Matched'].fillna('')
            data.loc[data['Self Matched'] == 'MATCHED', 'matched_status'] = 'Reversal'
        else:
            pass

        if 'CARRY_FORWARD' in data.columns:
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                    data['matched_status'] == 'Matched'), 'matched_status'] = 'Carry-Forward Matched'
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                    data['matched_status'] == 'UnMatched'), 'matched_status'] = 'Carry-Forward UnMatched'
        else:
            pass

        # Recon Summary For Date Wise
        pivot_df = pd.pivot_table(data, index=groupbyCol, columns=['matched_status'], values=['Count', 'Amount'],
                                  aggfunc=np.sum).reset_index()

        # Recon Summary For source Wise
        pivot_source = pd.pivot_table(data, index=['Source'], columns=['matched_status'], values=['Count', 'Amount'],
                                      aggfunc=np.sum).reset_index()

        pivot_df.columns = pivot_df.columns.to_series().str.join('#')
        pivot_source.columns = pivot_source.columns.to_series().str.join('#')

        summary_list = pivot_df.to_dict(orient='records')
        source_summary = pivot_source.to_dict(orient='records')

        return summary_list, source_summary

    def generateReconMetaInfo(self, payload):

        collection = MongoClient(config.mongoHost)[config.databaseName]['recon_details']
        reconDict = {"reconName": payload['reconName'], "sourcePath": payload['reconProcess'],
                     "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                     "reconProcess": payload['reconProcess'], "reconId": payload['reconProcess'] + ' RECON',
                     "partnerId": "54619c820b1c8b1ff0166dfc", "reportDetails": []}

        for key, data in payload.get('results', {}).iteritems():
            reconDict['reportDetails'].extend([{'reportName': 'Matched', 'reportPath': '%s.csv' % key, "source": key},
                                               {'reportName': 'Unmatched',
                                                'reportPath': "%s_UnMatched.csv" % key, "source": key}])

            if 'Self Matched' in payload['results'][key]:
                reconDict['reportDetails'].append(
                    {'reportName': key + ' Self Match', 'reportPath': '%s_Self_Matched.csv' % key, "source": key})

        if 'duplicateStore' in payload.keys():
            for key, data in payload['duplicateStore'].iteritems():
                reconDict['reportDetails'].extend(
                    [{'reportName': key + ' Duplicated', 'reportPath': '%s_Duplicated.csv' % key, "source": key}])

        if 'custom_reports' in payload.keys():
            for key, data in payload['custom_reports'].iteritems():
                reconDict['reportDetails'].extend(
                    [{'reportName': key + ' Report', 'reportPath': '%s_Report.csv' % key, "source": "Reports"}])

        if 'reconSummary' in payload.keys():
            reconDict['reportDetails'].append(
                {'reportName': "Recon Summary", 'reportPath': 'recon_summary.csv', "source": 'Recon Summary'})

        if 'reports' in payload.keys():
            for report in payload['reports']:
                reconDict['reportDetails'].append(payload['reports'][report])

        existingDict = collection.find_one({'reconName': payload['reconName']})

        if not existingDict:
            collection.insert(reconDict)

        else:
            delta = list({dict2['reportPath'] for dict2 in reconDict['reportDetails']} -
                         {dict1['reportPath'] for dict1 in existingDict['reportDetails']})

            delta_dict = [{'reportPath': value} for value in delta]
            if delta_dict:
                newlist = [report for report in reconDict['reportDetails'] for data in delta_dict if
                           report['reportPath'] in data.values()]
                collection.update({"reconName": payload['reconName']}, {'$push': {'reportDetails':{'$each': newlist}}})
        # print '===' * 50
        # print 'Recon Defination'
        # print reconDict
        # print 'Recon License Defination'
        # ''''print {"sourcePath": payload['reconName'], "reconId": payload['reconType'] + ' RECON',
        #        "reconName": payload['reconName'], "licenseCount": 1, "licensed": "Licensed",
        #        "reconProcess": payload['reconType'], "updated": datetime.datetime.now().strftime('%s'),
        #        "partnerId": "54619c820b1c8b1ff0166dfc", "jobScript": main.__file__.split('smartrecon')[-1]}'''
        # print '===' * 50





class VLookup(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "VLookup"
        self.properties = dict(data="", lookup="", dataFields=[], lookupFields=[], includeCols=[], restoreCols=[],
                               markers={'colName': 'value'}, resultkey='')

    def run(self, payload, debug):
        try:
            util = Utilities()
            data = util.getValues(payload, self.properties['data']).copy().reset_index(drop=True)
            lookup = util.getValues(payload, self.properties['lookup']).copy().reset_index(drop=True)

            data_field = self.properties['dataFields']
            lookup_field = self.properties.get('lookupFields', [])
            include_cols = self.properties.get('includeCols', [])
            marker_cols = self.properties.get('markers', {})

            # incase if look_up data is empty, initialize original data.
            if lookup.empty:
                for key in marker_cols.keys():
                    data[key] = data.get(key, '')

                for col in include_cols:
                    if col not in data.columns:
                        data[col] = ''

                util.setValues(payload, self.properties['resultkey'], data)
                return

            if lookup_field:
                lookup.drop_duplicates(subset=lookup_field, keep='first', inplace=True)

                if self.properties.get('dataFieldsFilter', []):
                    data = FilterExecutor.FilterExecutor().run(data, self.properties.get('dataFieldsFilter', []),
                                                               payload=payload)
                if self.properties.get('lookupFieldsFilter', []):
                    lookup = FilterExecutor.FilterExecutor().run(lookup, self.properties.get('lookupFieldsFilter', []),
                                                                 payload=payload)

                # use retain column when doing multiple vlookups on single frame to retain previous values
                restore_frame = data[[col for col in self.properties.get('restoreCols', []) if col in data.columns]]
                # DataFrame.update() works only on nan values
                for col in restore_frame:
                    restore_frame[col].replace(util.infer_dtypes(restore_frame[col]), np.nan, inplace=True)

                # Remove existing marker columns if exists in lookup data & re-init with new values
                for column, value in marker_cols.iteritems():
                    if column in lookup.columns:
                        del lookup[column]
                    lookup[column] = value
                    include_cols.append(column)

                # Remove if look up columns already exists in loaded data, avoid duplicate columns on re-look up
                result_field = data.columns.tolist()
                for col in include_cols:
                    if col in data.columns:
                        del data[col]

                    if col not in result_field:
                        result_field.append(col)

                lookup = lookup[lookup_field + include_cols]
                lookup_data = data.merge(lookup, left_on=data_field, right_on=lookup_field, how='left',
                                         suffixes=('', '_y'))
                for col in lookup_data.columns:
                    if '_y' in col:
                        del lookup_data[col]

                # Modify DataFrame in place using non-NA values from passed DataFrame.
                if not restore_frame.empty:
                    lookup_data.update(restore_frame, overwrite=False)

                for col in result_field:
                    lookup_data[col].fillna(util.infer_dtypes(lookup_data[col]), inplace=True)

                util.setValues(payload, self.properties['resultkey'], lookup_data)

        except Exception as e:
            payload['error'] = dict(elementName="VLookup", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class ReportGenerator(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ReportGenerator"

        self.properties = dict(source=None, filterConditions=[], columnNameList=[], reportName="", resultKey="",
                               drop="")

    def run(self, payload, debug):
        try:
            report_df = pd.DataFrame()
            for source in self.properties['sources']:
                org_df = Utilities().getValues(payload, source['source'])
                org_df.reset_index(inplace=True, drop=True)
                df = org_df.copy()

                # if empty frame ignore computations
                if df.empty:
                    continue

                # Applying the Filter Conditions
                if source.get('filterConditions', []):
                    df = FilterExecutor.FilterExecutor().run(df, source['filterConditions'], payload=payload)

                # if drop is set, removes the filtered records from original source frame
                if source.get('drop', False):
                    remaining_df = org_df[~org_df.index.isin(df.index)]
                    Utilities().setValues(payload, source['source'], remaining_df)

                # if frame is empty then concat with join_axes converts integer values to float
                if report_df.empty:
                    report_df = df
                else:
                    # incase if filters results in empty dataframe, to avoid loss of column index during concat use keycolumn
                    join_axes = pd.Index(
                        self.properties['keycolumns']) if 'keycolumns' in self.properties.keys() else df.columns
                    report_df = pd.concat([report_df, df], join_axes=[join_axes])

                # duplicate need to eliminate below block of code (already handled by drop)
                if self.properties.get('discardRecord', False):
                    report_df = report_df.reset_index(drop=True)
                    undiscarded = FilterExecutor.FilterExecutor().run(report_df, source['discardConditions'],
                                                                      payload=payload)
                    Utilities().setValues(payload, source['source'], set_value=undiscarded)
                    report_df = report_df[~report_df.index.isin(undiscarded.index)]

                if self.properties.get('writeToDB', False):
                    collection = self.properties.get('collection', 'report_details')
                    # clean up dataframe before posting to DB
                    report_df.fillna('', inplace=True)
                    report_df.columns = [c.replace('.', '') for c in report_df.columns]

                    MongoClient(config.mongoHost)[config.databaseName][collection].insert(
                        {'reportName': self.properties.get('reportName', ""), 'statementDate': payload['statementDate'],
                         'source': source['sourceTag'], 'reconName': payload['reconName'],
                         'reconProcess': payload['reconProcess'],
                         "created": Utilities().getUtcTime(), 'partnerId': '54619c820b1c8b1ff0166dfc',
                         'details': report_df.to_dict(orient='records')})

            # Apply post filters after loading all the sources
            df = report_df.copy()
            if 'postFilters' in self.properties.keys():
                df = FilterExecutor.FilterExecutor().run(df, self.properties.get('postFilters', []), payload=payload)

            if self.properties.get('writeToFile', False):
                if 'custom_reports' not in payload.keys():
                    payload['custom_reports'] = {self.properties.get('reportName', "Report"): df}
                else:
                    payload['custom_reports'][self.properties.get('reportName', 'Report')] = df

        except Exception as e:
            payload['error'] = dict(elementName="ReportGenerator", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

# class GenerateReconSummary(FlowElement):
#     def __init__(self):
#         FlowElement.__init__(self)
#         self.type = "SummaryReport"
#
#         self.properties = dict(
#             sources=[dict(sourceTag="", aggrCol=[], matched='all/any')], groupbyCol=[])

class AddPostMatchRemarks(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "AddMatchingRemarks"
        self.name = "AddMatchingRemarks"
        self.properties = dict(source='', remarkCol='',
                               remarks={"matchRemarks": "", "selfRemarks": ""},
                               how='all/any', resultKey='')

    def run(self, payload, debug):
        try:
            templates = DataFiller().findTemplates(config.basepath + '/templates/' + payload['reconName'])
            cfs = CustomFunctions(payload=payload)
            for template in templates:
                if 'REMARKS' in template:
                    with open(template) as f:
                        doc = json.load(f)
                    for data in doc:
                        df = Utilities().getValues(payload, data['source'])
                        if not df.empty:
                            df = FilterExecutor.FilterExecutor().run(df, data['conditions'], payload=payload)
                            Utilities().setValues(payload, data['resultKey'], set_value=df)

        except Exception as e:
            payload['error'] = dict(elementName="AddPostMatchRemarks", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class ReferenceLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ReferenceLoader"
        self.properties = dict(loadType="", source="", fileName="", feed_params=dict(), feedFilters=[],
                               resultkey="dataframe")

    def run(self, payload, debug):
        try:
            feedfac = FeedLoader.FeedLoaderFactory()
            instance = feedfac.getInstance(self.properties.get('loadType', ''))
            file_names = [config.basepath + file_name for file_name in self.properties["fileName"]]
            inst = instance.loadFile(file_names, self.properties.get('feed_params', dict()))
            if self.properties.get('feedFilters', []):
                inst = FilterExecutor.FilterExecutor().run(inst, self.properties.get('feedFilters', []),
                                                           payload=payload)
            payload[self.properties["resultkey"]] = inst
        except Exception as e:
            payload['error'] = dict(elementName="ReferenceLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class ValidateMasterDump(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ValidateMasterDump"
        self.properties = dict(source="", masterType="", keyColumn=[], insertCols=[], resultKey='', filters=[])

    def run(self, payload, debug):
        try:
            util = Utilities()
            master_dump = MongoClient(config.mongoHost)[config.databaseName]['master_dumps']
            df = util.getValues(payload, self.properties['source']).copy()
            df['type'] = self.properties['masterType']
            df['reconName'] = payload['reconName']
            df['statementDate'] = payload['statementDate']
            df['dropOnRollback'] = self.properties.get('dropOnRollback', False)
            result_list = []

            # exclude transaction that need to participate in validation
            if 'filters' in self.properties.keys():
                df = FilterExecutor.FilterExecutor().run(df, expressions=self.properties.get('filters', []),
                                                         payload=payload)

            if df.empty:
                pass
            else:
                insert_list = df[
                    self.properties['insertCols'] + ['reconName', 'statementDate', 'type', 'dropOnRollback']].to_dict(
                    orient='records')

                for insert in insert_list:
                    cursor = master_dump.find({key: insert[key] for key in self.properties.get('keyColumn', [])})
                    if cursor.count() > 0:
                        result_list.append(insert)
                    else:
                        insert["partnerId"] = "54619c820b1c8b1ff0166dfc"
                        insert["created"] = util.getUtcTime()
                        master_dump.insert(insert)

                if result_list:
                    result = pd.DataFrame(result_list)[self.properties['insertCols']]
                else:
                    result = pd.DataFrame(columns=self.properties['insertCols'])

                util.setValues(payload, ref=self.properties['resultKey'], set_value=result)
        except Exception as e:
            payload['error'] = dict(elementName="UpdateMasterDump", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class LoadExternalReport(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "LoadFile"
        self.name = "LoadExternalReport"
        self.properties = dict(reconName='')

    def run(self, payload, debug):
        try:
            filelist = list()
            feedfac = FeedLoader.FeedLoaderFactory()
            inst = feedfac.getInstance(self.properties.get('loadType', ''))
            # feed_params = self.properties.get('feed_params', dict())
            # file=config.mftpath+os.sep+"{}".format(self.properties['reconType'])+payload['statementDate'].strftime('%d%m%Y')
            file = "{mftpath}/{reconName}/{stmtDate}/{filepath}".format(mftpath=config.mftpath,
                                                                        reconName=self.properties['reconName'],
                                                                        stmtDate=payload['statementDate'].strftime(
                                                                            '%d%m%Y'),
                                                                        filepath=self.properties['filepath'])
            filelist.append(file)
            inst = inst.loadFile(filelist, self.properties.get('feed_params', dict()))

            payload[self.properties['resultKey']] = inst

        except Exception as e:
            payload['error'] = dict(elementName="LoadExternalReport", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class DuplicateMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DuplicateMatch"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="", subsetfilter=[])],
                               matchResult=None)

    def run(self, payload, debug):
        try:
            print("In Duplicate Match")
            result = {}
            remainder = []
            for i in range(0, len(self.properties["sources"])):
                if "subsetfilter" in self.properties["sources"][i] and len(
                        self.properties["sources"][i]["subsetfilter"]) > 0:
                    source = self.properties["sources"][i]
                    data = Utilities().getValues(payload, source['source'])

                    data.reset_index(drop=True, inplace=True)
                    filtereddata = FilterExecutor.FilterExecutor().run(data,
                                                                       self.properties["sources"][i]["subsetfilter"],
                                                                       payload, "Filter" + str(i))

                    remainingdata = data[~data.index.isin(filtereddata.index)]
                    remainder.append(remainingdata)
                    Utilities().setValues(payload, ref=source["source"], set_value=filtereddata)
                else:
                    remainder.append(pd.DataFrame())
            if len(self.properties["sources"]) == 1:
                return

            for i in range(0, len(self.properties["sources"]) - 1):
                leftsource = self.properties["sources"][i]
                leftcolumns = leftsource["columns"]
                for j in range(i + 1, len(self.properties["sources"])):
                    rightsource = self.properties["sources"][j]
                    rightcolumns = rightsource["columns"]
                    rightdata = Utilities().getValues(payload, rightsource['source'])

                    leftdata = Utilities().getValues(payload, leftsource['source'])

                    leftmatchColumns = leftcolumns.get('matchColumns', [])
                    rightmatchColumns = rightcolumns.get('matchColumns', [])

                    leftjson = leftdata.to_dict(orient='records', into=collections.OrderedDict())
                    rightjson = rightdata.to_dict(orient='records', into=collections.OrderedDict())

                    for x in leftjson:
                        for y in rightjson:
                            if leftsource["sourceTag"] + " Match" in y and y[
                                leftsource["sourceTag"] + " Match"] == "MATCHED":
                                continue

                            matched = True
                            for idx in range(0, len(leftmatchColumns)):
                                if x[leftmatchColumns[idx]] != y[rightmatchColumns[idx]]:
                                    matched = False
                                    break

                            if matched:
                                x[rightsource["sourceTag"] + " Match"] = "MATCHED"
                                y[leftsource["sourceTag"] + " Match"] = "MATCHED"

                    payload[self.properties["sources"][i]["source"]] = pd.DataFrame.from_dict(leftjson)
                    payload[self.properties["sources"][j]["source"]] = pd.DataFrame.from_dict(rightjson)

            for i in range(0, len(self.properties["sources"])):
                tmp_df = payload[self.properties["sources"][i]["source"]]

                # set nan values to un-matched
                for col in tmp_df:
                    if ' Match' in col:
                        tmp_df.loc[tmp_df[col].isnull(), col] = 'UNMATCHED'

                # tmp_df contains match columns(<src> Match), join_axes on tmp_df to retain columns
                # else if tmp_df is empty join_axes join on remainder else data frame becomes empty if done on tmp_df
                result[self.properties["sources"][i]["sourceTag"]] = pd.concat([tmp_df, remainder[i]],
                                                                               join_axes=[remainder[i].columns
                                                                                          if tmp_df.empty
                                                                                          else tmp_df.columns]
                                                                               ).reset_index(drop=True)

            # Avoid over-writing of existing data
            if self.properties['matchResult'] in payload.keys():
                for key, value in result.iteritems():
                    payload[self.properties['matchResult']][key] = value
            else:
                payload[self.properties['matchResult']] = result
        except Exception, e:
            payload['error'] = dict(elementName="DuplicateMatch", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class MergeAndCompare(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "MergeAndCompare"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="")],
                               matchResult=None)

    def run(self, payload, debug):
        print("In MergeAndCompare")
        result = {}
        remainder = []
        if len(self.properties["sources"]) == 1:
            return
        sourcedict = {}
        duplicates = {}
        for i in range(0, len(self.properties["sources"])):
            source = self.properties["sources"][i]
            columns = source["columns"]
            fields = columns.get("keyColumns", [])
            data = Utilities().getValues(payload, source['source'])
            data = data.fillna("")
            for x in data.columns:
                if x in ['STATEMENT_DATE']:
                    continue
                else:
                    data[x] = data[x].astype(str)

            datalist = data.to_dict(orient="records")
            sourcedict[source["source"]] = {}
            duplicates[source["source"]] = []
            for x in datalist:
                keyval = ""
                for y in fields:
                    if len(keyval) > 0:
                        keyval += ":" + str(x[y])
                    else:
                        keyval = str(x[y])
                if keyval in sourcedict[source["source"]]:
                    duplicates[source["source"]].append(x)
                else:
                    sourcedict[source["source"]][keyval] = x
        for i in range(0, len(self.properties["sources"]) - 1):
            leftsource = self.properties["sources"][i]
            leftcolumns = leftsource["columns"]
            leftdata = sourcedict[leftsource['source']]
            for j in range(i + 1, len(self.properties["sources"])):
                rightsource = self.properties["sources"][j]
                rightcolumns = rightsource["columns"]
                rightdata = sourcedict[rightsource['source']]
                leftmatchcolumns = leftcolumns.get("matchColumns", [])
                rightmatchcolumns = rightcolumns.get("matchColumns", [])

                for key in leftdata.keys():
                    if key in rightdata:
                        leftrow = leftdata[key]
                        if type(leftrow) == str:
                            continue
                        leftrow[rightsource['sourceTag'] + " Matched Columns"] = ""
                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] = ""
                        rightrow = rightdata[key]
                        rightrow[leftsource['sourceTag'] + " Matched Columns"] = ""
                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] = ""
                        for i in range(0, len(leftmatchcolumns)):
                            if leftmatchcolumns[i] == '' or rightmatchcolumns[i] == '':
                                continue
                            elif str(leftrow[leftmatchcolumns[i]]).strip() == str(
                                    rightrow[rightmatchcolumns[i]]).strip():
                                leftrow[rightsource['sourceTag'] + " Matched Columns"] += "," + rightmatchcolumns[i]
                                rightrow[leftsource['sourceTag'] + " Matched Columns"] += "," + leftmatchcolumns[i]
                            else:
                                if not str(leftrow[leftmatchcolumns[i]]).strip():
                                    if not str(rightrow[rightmatchcolumns[i]]).strip():
                                        # leftrow[leftmatchcolumns[i]] = '----'
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    '----' + " not same as " + \
                                                                                                    leftmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                    else:
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    rightrow[
                                                                                                        rightmatchcolumns[
                                                                                                            i]] + " not same as " + \
                                                                                                    leftmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                else:
                                    if not str(rightrow[rightmatchcolumns[i]]).strip():
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    '----' + " not same as " + \
                                                                                                    leftmatchcolumns[
                                                                                                        i] + "with value " + \
                                                                                                    leftrow[
                                                                                                        leftmatchcolumns[
                                                                                                            i]] + ","

                                if not str(rightrow[rightmatchcolumns[i]]).strip():
                                    if not str(leftrow[leftmatchcolumns[i]]).strip():
                                        # rightrow[rightmatchcolumns[i]]='----'
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    '----' + "not same as " + \
                                                                                                    rightmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                    else:
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    leftrow[
                                                                                                        leftmatchcolumns[
                                                                                                            i]] + "not same as " + \
                                                                                                    rightmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                else:
                                    if not str(leftrow[leftmatchcolumns[i]]).strip():
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    '----' + "not same as " + \
                                                                                                    rightmatchcolumns[
                                                                                                        i] + "with value " + \
                                                                                                    rightrow[
                                                                                                        rightmatchcolumns[
                                                                                                            i]] + ","

                                leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                i] + " with value " + \
                                                                                            rightrow[rightmatchcolumns[
                                                                                                i]] + " not same as " + \
                                                                                            leftmatchcolumns[
                                                                                                i] + "with value " + \
                                                                                            leftrow[leftmatchcolumns[
                                                                                                i]] + ","
                                rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                i] + " with value " + \
                                                                                            leftrow[leftmatchcolumns[
                                                                                                i]] + "not same as " + \
                                                                                            rightmatchcolumns[
                                                                                                i] + "with value " + \
                                                                                            rightrow[rightmatchcolumns[
                                                                                                i]] + ","
                    else:
                        leftdata[key][rightsource['source'] + " Missing"] = "True"
                        # rightdata[key][leftsource['source']+ " Missing"]="True"

                sourcedict[leftsource['source']] = leftdata
                sourcedict[rightsource['source']] = rightdata

        for i in range(0, len(self.properties["sources"])):
            result[self.properties["sources"][i]["sourceTag"]] = pd.DataFrame(
                list(sourcedict[self.properties["sources"][i]["source"]].values()))
            payload['duplicateStore'] = {}
            for key in duplicates.keys():
                payload['duplicateStore'][key + " duplicates"] = pd.DataFrame(duplicates[key])

        payload[self.properties['matchResult']] = result

class DataFiller(FlowElement):
    def __init__(self):
        self.type = "DataFiller"
        self.name = "DataFiller"
        self.properties = dict()

    def run(self, payload, debug):
        try:
            templates = self.findTemplates(config.basepath + '/templates/' + payload['reconName'])
            cfs = CustomFunctions(payload=payload)
            if len(templates) > 1:
                temp1 = [template.split('_')[-1].split('.')[0] if '_' in template else template for
                                 template in templates]
                templates = [templates[i[0]] for i in sorted(enumerate(temp1), key=lambda x: x[1])]
            for template in templates:
                if 'REMARKS' not in template:
                    try:
                        exec ('cfs.' + template.split('.')[1] + 'ReportGenerator(template)')
                    except Exception as e:
                        raise ValueError("Error in template "+os.path.splitext(os.path.basename(template))[0].split('_')[0])
        except Exception, e:
            # print traceback.format_exc()
            payload['error']\
                = dict(elementName="DataFiller", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    def findTemplates(self, path):
        files = []
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                files.append(os.path.join(root, filename))
        return files

class MailSender(FlowElement):
    def __init__(self):
        self.type = "MailSender"
        self.name = "MailSender"
        self.properties = dict()

    def run(self, payload, debug):
        try:
            mailDetails = list(MongoClient(config.mongoHost)[config.databaseName]['mail_details'].find(
                {"reconName": payload['reconName']}))
            attachFiles = []
            toaddrdict = {}
            for detail in mailDetails:
                if detail['toAddress'] not in toaddrdict.keys():
                    toaddrdict[detail['toAddress']] = []
                toaddrdict[detail['toAddress']].append(detail)

            for address, data in toaddrdict.iteritems():
                for mailData in data:
                    df = Utilities().getValues(payload, 'results.' + mailData['source'], sep='.').reset_index(drop=True)

                    if mailData['expression']:
                        df = FilterExecutor.FilterExecutor().run(df, mailData['expression'], payload=payload)

                    filepath = config.contentDir + os.sep  + mailData['reportName'] + '_' + str(
                        payload['statementDate']) + '.csv'
                    df.to_csv(filepath, index=False)
                    attachFiles.append(filepath)
                    # if detail['ccUsersList']:
                    #     ccUsersList=detail['ccUsersList']
                    # else:
                    #     ccUsersList=None
                mailApi.sendemail(mailData['toAddress'],
                                  subject=payload['reconName'] + '_' + str(payload['statementDate']),
                                  attachFiles=attachFiles)

        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="MailSender", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())
