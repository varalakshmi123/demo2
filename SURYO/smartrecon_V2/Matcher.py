import uuid
import pandas
import numpy as np


class Matcher():
    def __init__(self):
        pass

    def match(self, left=None, right=None, leftsumColumns=[], rightsumColumns=[], leftmatchColumns=[],
              rightmatchColumns=[],isToleranceMatch=False,toleranceValue=None):

        # No data found in either of frame return others as un-matched
        if left.empty or right.empty:
            left['Matched'], right['Matched'] = 'UNMATCHED', 'UNMATCHED'
            return left, right

        left_columns = leftmatchColumns + leftsumColumns
        right_columns = rightmatchColumns + rightsumColumns
        right.drop(columns=['LINK_ID'], errors='ignore', inplace=True)
        left.drop(columns=['LINK_ID'], errors='ignore', inplace=True)
        if len(leftsumColumns) > 0:
            leftworking = left[left_columns].groupby(leftmatchColumns).sum().reset_index()
        else:
            leftworking = left[left_columns]

        if len(rightsumColumns) > 0:
            rightworking = right[right_columns].groupby(rightmatchColumns).sum().reset_index()
        else:
            rightworking = right[right_columns]

        # Incase of carry forward disable do not add carry forward indicator columns
        if 'CARRY_FORWARD' in left.columns:
            leftworking['left_carryforward'] = left['CARRY_FORWARD']
            left_cf_column = ['left_carryforward']
        else:
            left_cf_column = []

        if 'CARRY_FORWARD' in right.columns:
            rightworking['right_carryforward'] = right['CARRY_FORWARD']
            right_cf_column = ['right_carryforward']
        else:
            right_cf_column = []

        if isToleranceMatch:
            left, right = self.toleranceMatch(right, left, leftmatchColumns, leftsumColumns, rightmatchColumns,
                                              rightsumColumns, toleranceValue)

            return left, right

        else:
            finalworking = leftworking.merge(rightworking, how="outer", left_on=left_columns,
                                             right_on=right_columns, indicator=True, suffixes=('', '_y'))

            for col in finalworking.columns.values:
                if col.endswith("_y"):
                    del finalworking[col]

            matched = finalworking[finalworking["_merge"] == "both"]
            matched['LINK_ID'] = matched[matched.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

            if '_merge' in matched.columns:
                del matched['_merge']

        if len(leftsumColumns) > 0:
            leftmatchColumns = list(set(leftmatchColumns) - set(leftsumColumns))

        if len(rightsumColumns) >0:
            rightmatchColumns = list(set(rightmatchColumns) - set(rightsumColumns))

        left = left.merge(matched[leftmatchColumns + right_cf_column+['LINK_ID']], how="left", on=leftmatchColumns,
                          indicator=True)

        right = right.merge(matched[rightmatchColumns + left_cf_column+['LINK_ID']], how="left", on=rightmatchColumns,
                            indicator=True)

        left['Matched'] = 'UNMATCHED'

        left.loc[left['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del left["_merge"]

        right['Matched'] = 'UNMATCHED'
        right.loc[right['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del right['_merge']

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

#        left, right = self.add_match_remarks(left, right, leftmatchColumns, rightmatchColumns, leftsumColumns,
#                                             rightsumColumns)

        return left, right

    def add_match_remarks(self,left, right, leftkeyColumns=[], rightkeycolumns=[], leftsumColumns=[],
                              rightsumColumns=[]):

            left_len, right_len = len(left), len(right)

            left_columns = leftkeyColumns
            right_columns = rightkeycolumns

            if len(leftsumColumns) > 0:
                leftworking = left[left_columns].groupby(leftkeyColumns).sum().reset_index()
            else:
                leftworking = left[left_columns]

            if len(rightsumColumns) > 0:
                rightworking = right[right_columns].groupby(rightkeycolumns).sum().reset_index()
            else:
                rightworking = right[right_columns]

            if 'Exception Remarks' not in right.columns:
                right['Exception Remarks'] = ''

            if 'Exception Remarks' not in left.columns:
                left['Exception Remarks'] = ''

            if 'Matching Remarks' not in right.columns:
                right['Matching Remarks'] = ''

            if 'Matching Remarks' not in left.columns:
                left['Matching Remarks'] = ''

            left.loc[left['Matched'] == 'MATCHED', ['Matching Remarks', 'Exception Remarks']] = '', ''
            right.loc[right['Matched'] == 'MATCHED', ['Matching Remarks', 'Exception Remarks']] = '', ''
            # left.loc[:,['Matching Remarks','Exception Remarks']]+='(','('
            # right.loc[:,['Matching Remarks','Exception Remarks']]+='(','('
            for idx in range(0, len(left_columns)):
                left_col = left_columns[idx]
                right_col = right_columns[idx]

                left_unique = left[[left_col]].drop_duplicates(keep='first')
                right_unique = right[[right_col]].drop_duplicates(keep='first')

                right = right.merge(left_unique, how='left', left_on=right_col, right_on=left_col,
                                    indicator=True,suffixes=('', '_y'))
                left = left.merge(right_unique, how='left', left_on=left_col, right_on=right_col,
                                  indicator=True,suffixes=('', '_y'))

                # r_idx = right[right_col] == Utilities().infer_dtypes(right[right_col])
                # l_idx = left[left_col] == Utilities().infer_dtypes(left[left_col])

                right[['Exception Remarks', 'Matching Remarks']] = right[['Exception Remarks', 'Matching Remarks']].fillna('')
                left[['Exception Remarks', 'Matching Remarks']] = left[['Exception Remarks', 'Matching Remarks']].fillna('')


                right.loc[(right['_merge'] != 'both'), "Exception Remarks"] += right_col + ','
                left.loc[(left['_merge'] != 'both'), "Exception Remarks"] += left_col + ','

                right.loc[(right['_merge'] == 'both'), "Matching Remarks"] += right_col + ','
                left.loc[(left['_merge'] == 'both'), "Matching Remarks"] += left_col + ','

                # right.loc[(right['_merge'] == 'both') & (right['Matched'] == 'MATCHED'), "Exception Remarks"] = ''
                # left.loc[(left['_merge'] == 'both') & (left['Matched'] == 'MATCHED'), "Exception Remarks"] = ''


                right.drop(columns='_merge', inplace=True, errors='ignore')
                left.drop(columns='_merge', inplace=True, errors='ignore')

            left['Exception Remarks'] = left['Exception Remarks'].str.rstrip(',')
            right['Exception Remarks'] = right['Exception Remarks'].str.rstrip(',')

            left['Matching Remarks'] = left['Matching Remarks'].str.rstrip(',')
            right['Matching Remarks'] = right['Matching Remarks'].str.rstrip(',')

            for col in left.columns.values:
                if col.endswith("_y"):
                    del left[col]


            for col in right.columns.values:
                if col.endswith("_y"):
                    del right[col]


            if len(left) != left_len or len(right) != right_len:
                print 'Possible cartesian product while adding matchin remarks !!!'

            return left, right

    def toleranceMatch(self,right,left,leftmatchColumns,leftsumColumns,
                       rightmatchColumns,rightsumColumns,tolerancevalue):


        if len(leftsumColumns) > 0:
            leftworking = left[leftmatchColumns + leftsumColumns].groupby(leftmatchColumns).sum().reset_index()

        else:
            leftworking = left[leftmatchColumns + leftsumColumns]

        if len(rightsumColumns) > 0:
            rightworking = right[rightmatchColumns + rightsumColumns].groupby(rightmatchColumns).sum().reset_index()

        else:
            rightworking = right[rightmatchColumns + rightsumColumns]

        # rightworking['right_index'] = rightworking.index + 1
        #
        # leftworking['left_index'] = leftworking.index + 1

        right.drop(columns=['Tolerance Difference', 'ToleranceMatch'], errors='ignore', inplace=True)
        left.drop(columns=['Tolerance Difference', 'ToleranceMatch'], errors='ignore', inplace=True)

        final = leftworking.merge(rightworking, right_on=rightmatchColumns, left_on=leftmatchColumns, how='outer',
                                  suffixes=('_right', '_left'), indicator=True)

        final = final[final['_merge'] == 'both']

        if '_merge' in final.columns:
            del final['_merge']

        if len(final) > 0:

            if leftsumColumns == rightsumColumns:

                final['Tolerance Difference'] = abs(final[','.join(leftsumColumns)+'_left'] - final[','.join(rightsumColumns)+'_right'])
            else:

                final['Tolerance Difference'] = abs(final[','.join(leftsumColumns)] - final[','.join(rightsumColumns)])

            final.loc[final['Tolerance Difference'].round(2).between(-tolerancevalue,tolerancevalue), "ToleranceMatch"] = "Yes"

            final = final[final['ToleranceMatch'] == 'Yes']

            if not final.empty:
                final['LINK_ID'] = final[final.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                final['LINK_ID'] = final['LINK_ID'].astype(str)

        for col in final.columns.tolist():
            if col.endswith("_left") or col.endswith("_right"):
                del final[col]

        if set(['Tolerance Difference', 'ToleranceMatch', 'LINK_ID']).issubset(final.columns.tolist()):
            left = left.merge(final[leftmatchColumns + ['Tolerance Difference', 'ToleranceMatch', 'LINK_ID']],
                              how="left", on=leftmatchColumns,
                              indicator=True)

            right = right.merge(final[rightmatchColumns + ['Tolerance Difference', 'ToleranceMatch', 'LINK_ID']],
                                how="left", on=rightmatchColumns,
                                indicator=True)
        else:

            left = left.merge(final[leftmatchColumns], how="left", on=leftmatchColumns,
                              indicator=True)

            right = right.merge(final[rightmatchColumns], how="left", on=rightmatchColumns,
                                indicator=True)

        left['Matched'] = 'UNMATCHED'

        left.loc[(left['_merge'] == 'both') & (left['Tolerance Difference'].round(2).between(-tolerancevalue,tolerancevalue)), 'Matched'] = 'MATCHED'

        del left["_merge"]

        right['Matched'] = 'UNMATCHED'

        right.loc[(right['_merge'] == 'both')& (right['Tolerance Difference'].round(2).between(-tolerancevalue,tolerancevalue)), 'Matched'] = 'MATCHED'

        del right['_merge']

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

        return left, right
