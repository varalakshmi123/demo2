logBaseDir = '/var/log/ngerecon/'
basepath = '/usr/share/nginx/smartrecon/'
mftpath = "/opt/reconDB/mft"

mongoHost = 'localhost'
databaseName = 'ngerecon'
bank_name = ''
contentDir='/usr/share/nginx/www/ngerecon/ui/app/files'
# common feed_params to be extracted from the source reference.
feed_params = {"CSV": ['skiprows', 'skipfooter', 'delimiter'],
               "Excel": ['skiprows', 'skipfooter','sheetNames'],"HTML":['parseIndex'],
               'SFMSMultiSheet': ['skiprows', 'skipfooter', 'FooterKey', 'columnNames'], 'XML': ['query']}


splitSourcePath = '/usr/share/nginx/smartrecon/splitSources/'

sourceList=['SFMS_NEFT','SFMS_RTGS','NPCI_SETTLEMENT_REPORT']

reconList=['RTGSInward','RTGSOutward','IMPS_OUTWARD']
