/**
 * Created by kedarnath on 3/9/18.
 */

var app = angular.module('nextGen')

app.controller('mailController', ['$scope', '$rootScope', 'ReconsService', '$uibModal', 'UsersService', 'ServerConfigureService','MailDetailsService','SourceReferenceService', function ($scope, $rootScope, ReconsService, $uibModal, UsersService, ServerConfigureService,MailDetailsService,SourceReferenceService) {
    $scope.getServerDetails=function()
    {
         ServerConfigureService.get('dummy', undefined, function (data) {
            $scope.serverDetails = data
    })
    }
    $scope.mailDetails={}
    $scope.uploadSucess=false
    $scope.getServerDetails()
    $scope.mode = 'Add'
    $scope.connection=['STARTTLS','SSL/TLS']
    $scope.openserverConfig = function () {
        $uibModal.open({
            templateUrl: 'serverSettings.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            controller: function ($uibModalInstance, $scope, $rootScope) {
                $scope.saveServerDetails = function (serverDts) {
                    if ($scope.serverDetails != undefined) {
                        ServerConfigureService.post(serverDts, function (data) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, $rootScope.resources.reconAssignedSucc, 'success')
                            $uibModalInstance.dismiss('cancel')
                            $scope.getServerDetails()

                        }, function (err) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, err.msg, 'error')
                        });
                    }
                    else {
                        ServerConfigureService.put($scope.serverDetails['_id'], serverDts, function (data) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, $rootScope.resources.reconAssignedSucc, 'success')
                        }, function (err) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, err.msg, 'error')
                        });
                    }
                }


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }


     $scope.getmailSettings = function () {
        MailDetailsService.get(undefined, undefined, function (data) {
            $scope.mailData = data['data']
        })
    }

     $scope.setDefault=function()
     {
         $scope.mailDetails={}
         $scope.mode='Add'
         $scope.uploadSucess=false
         $scope.openmailConfig()
     }
     $scope.editMailsettings = function (data) {
        // $scope.triggerDirective=false
        $scope.display = false
        $scope.mode = 'Edit'
        $scope.uploadSucess=true
        $scope.mailDetails=data
        $scope.openmailConfig(data)
        // $scope.mailDetails={}

    }

    $scope.getmailSettings()

    $scope.openmailConfig = function () {

        $uibModal.open({
            templateUrl: 'mailSettings.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            controller: function ($uibModalInstance, $scope, $rootScope) {
                $scope.savemailDetails = function (data) {

                    if ($scope.mode == 'Add') {
                        $scope.display = true
                        MailDetailsService.post(data, function (data) {
                            $scope.recons._id = data._id
                            $rootScope.showMessage($rootScope.resources.reconAssignment, $rootScope.resources.reconAssignedSucc, 'success')
                            $scope.getmailSettings()
                        }, function (err) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, err.msg, 'error')
                        });
                        $scope.mode = 'Edit'
                    }

                    else if ($scope.mode == 'Edit') {
                        $scope.display = true
                        MailDetailsService.put(data["_id"], data, function (data) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, $rootScope.resources.updateSuccess, 'success')
                            $scope.getmailSettings()
                        });
                    }
                    $uibModalInstance.dismiss('cancel')


                }

                $scope.cancel = function () {
                    $scope.mailDetails={}
                    $uibModalInstance.dismiss('cancel')

                }
            }
        })
    }
     $scope.uploadMailFilters = function () {
         $scope.uploadSucess=true
         $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/filter/upload"; //Url of webservice/api/server

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;

                            $rootScope.openModalPopupClose()

                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            $scope.plate = true

                            if (angular.isDefined(file)) {
                                SourceReferenceService.loadSourceRef('dummy', {
                                    'filter': true,
                                    'fileName': file.name
                                }, function (data) {
                                    $scope.mailDetails['expression'] = data
                                }, function (err) {
                                    console.log(err.msg)
                                })
                            }

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

    $scope.removeMailSettings = function (data) {
        console.log(data)
        $uibModal.open({
            templateUrl: '/app/components/notifications/confirmation.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            size: "md",
            controller: function ($uibModalInstance, $scope, $rootScope, MailDetailsService) {

                $scope.deleteTitle = $rootScope.resources.confirmation
                $scope.deleteMsg = $rootScope.resources['deleteMsg']
                $scope.submit = function () {
                    MailDetailsService.deleteData(data['_id'], function (data) {
                        $scope.getmailSettings()
                        $uibModalInstance.dismiss('cancel')
                        $rootScope.showMessage($rootScope.resources['reconAssignment'], $rootScope.resources['removeSuccess'], 'success')
                    }, function (err) {
                        console.log(err)
                    })
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }




}])