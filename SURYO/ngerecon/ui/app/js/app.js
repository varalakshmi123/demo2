agGrid.LicenseManager.setLicenseKey('Evaluation_License_Valid_Until__25_August_2018__MTUzNTE1MTYwMDAwMA==53aedc27a9df43c4e4bd6c97d9e6f413');
agGrid.initialiseAgGridWithAngular1(angular);
var app = angular.module('nextGen', ['ui', 'ui.bootstrap', 'ui.select', 'ngRoute', 'ngAnimate', 'ngSanitize', 'trNgGrid', 'agGrid', 'xeditable', 'btorfs.multiselect', 'isteven-multi-select', 'scrollable-table', 'angular.chosen', 'angularFileUpload', 'ngIdle']);

app.config(['$routeProvider', '$locationProvider', 'KeepaliveProvider', 'IdleProvider',
    function ($routeProvider, $locationProvider, KeepaliveProvider, IdleProvider) {
        IdleProvider.idle(5 * 60); // min * sec
        IdleProvider.timeout(30); // allow for browser refresh

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $routeProvider.when('/login', {
            templateUrl: '/app/partials/login.html'
        }).when('/dashboard', {
            templateUrl: '/app/partials/dashboard.html',
            controller: 'dashBoardController'
        }).when('/recons', {
            templateUrl: '/app/partials/recons.html',
            controller: 'reconController'
        }).when('/profile', {
            templateUrl: '/app/partials/profile.html',
        }).when('/reconJob', {
            templateUrl: '/app/partials/reconJob.html',
            controller: 'reconJobController'
        }).when('/marketplace', {
            templateUrl: '/app/partials/marketplace.html',
            controller: 'marketplaceController'
        }).when('/sourceTemplate', {
            templateUrl: '/app/partials/sourceTemplate.html',
            controller: 'sourceTemplateController'
        }).when('/settings', {
            templateUrl: '/app/partials/settings.html',
            controller: 'settingsController'
        }).when('/usermanagement', {
            templateUrl: '/app/partials/usermanagement.html',
            controller: 'userManagementController'
        }).when('/reconassignment', {
            templateUrl: '/app/partials/reconassignment.html',
            controller: 'reconAssignmentController'
        }).when('/configuration', {
            templateUrl: '/app/partials/configuration.html',
            controller: 'configurationController'
        }).when('/forceMatch', {
            templateUrl: '/app/partials/forceMatch.html',
            controller: 'forceMatchController'
        }).when('/reports', {
            templateUrl: '/app/partials/reports.html',
            controller: 'reportsController'
        }).when('/auditlogs', {
            templateUrl: '/app/partials/audits.html',
            controller: 'auditsController'
        });
    }]);


app.run(['$rootScope', '$http', '$uibModal', '$location', '$timeout', '$routeParams', '$route', '$filter', '$window', 'Idle', 'ReconDetailsService', 'angularCryptoSerivces', 'NoAuthService', 'UsersService', 'ReconAssignmentService', 'ForceMatchService', 'AuditsService',
    function ($rootScope, $http, $uibModal, $location, $timeout, $$routeParams, $route, $filter, $window, Idle, ReconDetailsService, angularCryptoSerivces, NoAuthService, UsersService, ReconAssignmentService, ForceMatchService, AuditsService) {
        $http.get('/app/json/role_mapping.json').success(function (authData) {
            $http.get('/assets/text/us_en/strings.json').success(function (resources) {
                $rootScope.apiURLForS3 = '/api';
                $rootScope.selected = true
                $rootScope.loadingModal = null;
                $rootScope.summaryDash = false
                $rootScope.path = $location.path()
                $rootScope.breadCrumblist = []
                $rootScope.rowLimit = 10
                $rootScope.breadCrumblist.push($rootScope.path.split('/')[1])
                $rootScope.openModalPopupOpen = function () {
                    if ($rootScope.loadingModal == null) {
                        $rootScope.loadingMsg = 'Loading...';
                        $rootScope.loadingModal = $uibModal.open({
                            scope: false,
                            templateUrl: '/app/components/notifications/loading.html',
                            windowClass: 'modal show loaderModalPopup',
                            backdropClass: 'show',
                            backdrop: 'static'
                        });
                    }
                };

                Idle.watch();
                $rootScope.$on('IdleTimeout', function () {
                    // TODO code to do clean up..
                    $rootScope.doLogout();
                });

                $rootScope.reset = function () {
                    Idle.watch();
                }

                // Initialize role mapping & cipher data for authentication.
                $rootScope.resources = resources
                $rootScope.rolesJson = authData.roles
                $rootScope.cipherText = authData.cipher
                $rootScope.approvalOptions = authData.approvalOptions
                $rootScope.roles = Object.keys(authData.roles)
                $rootScope.actionRoles = authData.actionRules
                $rootScope.previleges = []
                angular.forEach(authData.roles, function (val, key) {
                    angular.forEach(val.previleges, function (v, k) {
                        if ($rootScope.previleges.indexOf(v) == -1) {
                            $rootScope.previleges.push(v);
                        }
                    });
                })

                $rootScope.tabs = {}

                $rootScope.openModalPopupClose = function () {
                    if ($rootScope.loadingModal != null) {
                        $rootScope.loadingModal.close();
                        $rootScope.loadingModal = null;
                        $rootScope.loadingMsg = '';
                    }
                };


                function init() {
                    $rootScope.isauthenticated = false;
                    $rootScope.credentials = {};
                    $rootScope.notificationMsg = [];
                    $rootScope.appData = {};
                    $rootScope.signUpCheck = false
                    $rootScope.customeLoadingMsg = '';
                    $rootScope.config = {};
                    $rootScope.enableReconSetting = true
                    $rootScope.config.title = '';
                    $rootScope.reportNames = '';
                    $rootScope.update = {}
                    $rootScope.forgotPasswords = false
                    $rootScope['tabs'] = {}
                    $rootScope.requestedPath = ''
                }

                $rootScope.signUp = {}
                $rootScope.credentials = {}

                $rootScope.showMessage = function (title, msg, type, hideAfter) {
                    $.toast({
                        heading: title,
                        text: msg,
                        position: 'top-right',
                        loaderBg: '#ccff99',
                        icon: type,
                        hideAfter: (angular.isDefined(hideAfter) && hideAfter) ? false : 5000,
                        stack: 6
                    });
                }

                $rootScope.redirectToSignUp = function () {
                    $rootScope.signUpCheck = true
                    $rootScope.signUp = {}
                    $rootScope.policyShow = true
                }


                $rootScope.redirectToLogin = function () {
                    $rootScope.signUpCheck = false
                    $rootScope.signUp = {}
                    $rootScope.policyShow = false
                }


                $rootScope.doSignup = function (details) {
                    $rootScope.signUpCheck = false
                    $rootScope.policyShow = false
                    $rootScope.credentials = {}
                    details.password = angularCryptoSerivces.encrypt($rootScope.cipherText['AES_KEY'], $rootScope.cipherText['IV'], details.password + '/<=%+,?->/')
                    NoAuthService.createUser('dummy', details, function (data) {
                        if (data == 'Success') {
                            $rootScope.showMessage($rootScope.resources['signUp'], $rootScope.resources['signUpSucc'], 'success')
                            $rootScope.signUp = {};
                        }
                    }, function (err) {
                        $rootScope.showMessage('Signup', err.msg, 'error')
                        $rootScope.signUp['password'] = ''
                        $rootScope.signUp['confirmPassword'] = ''
                    })
                }

                var strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})');
                var mediumRegex = new RegExp('^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})');
                var emailFormat = new RegExp('^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$')

                $rootScope.analyzeEmail = function (value) {
                    if (emailFormat.test(value)) {
                        $rootScope.eMsg = false
                    }
                    else {
                        $rootScope.eMsg = true
                    }
                };

                $rootScope.showMsg = false
                $rootScope.analyze = function (value) {
                    if (strongRegex.test(value)) {
                        $rootScope.showMsg = false
                    } else if (mediumRegex.test(value)) {
                        $rootScope.showMsg = true
                    } else {
                        $rootScope.showMsg = true
                    }
                };

                $rootScope.displayMessage = function (msg, size, msgtype) {
                    var modalColor = '';
                    if (msgtype == 'success') {
                        modalColor = 'modal-success';
                    }
                    if (msgtype == 'error') {
                        modalColor = 'modal-error';
                    }
                    if (msgtype == 'warning') {
                        modalColor = 'modal-warning';
                    }
                    if (msgtype == 'info') {
                        modalColor = 'modal-info';
                    }
                    $uibModal.open({
                        templateUrl: '/app/components/notifications/message.html',
                        windowClass: 'modal show modalDialogCenter ' + modalColor,
                        backdrop: 'static',
                        backdropClass: 'show',
                        size: size,
                        controller: function ($scope, $uibModalInstance) {
                            $scope.msgtype = msgtype;
                            $scope.message = msg;
                            $rootScope.clearInstance = $uibModalInstance;
                            $scope.cancel = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                };


                $rootScope.genDataTableQuery = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
                    var conditions = {}
                    if (angular.isDefined(filterByFields) & !angular.equals(filterByFields, {})) {
                        var perColConditions = {}
                        angular.forEach(filterByFields, function (v, k) {
                            perColConditions[k] = v
                        })
                        conditions.query = {'perColConditions': perColConditions}
                    }

                    if (angular.isDefined(filterBy) & !angular.equals(filterBy, {})) {
                        var perColConditions = {}
                        angular.forEach(filterBy, function (v, k) {
                            perColConditions[k] = v
                        })
                        conditions['query'] = {'perColConditions': perColConditions}
                    }

                    if (angular.isDefined(orderBy)) {
                        var tmp = {}
                        tmp[orderBy] = orderByReverse ? 1 : -1
                        conditions['orderBy'] = tmp
                    }

                    conditions['skip'] = $rootScope.rowLimit * currentPage
                    conditions['limit'] = pageItems
                    return conditions
                }
                $rootScope.doLogin = function (credentials) {
                    $rootScope.loginErrorMsg = undefined;
                    $rootScope.apiLoginErrMessage = undefined;
                    $http.defaults.withCredentials = true;
                    credentials['password'] = angularCryptoSerivces.encrypt
                    ($rootScope.cipherText['AES_KEY'], $rootScope.cipherText['IV'], credentials.password + '/<=%+,?->/')
                    $http.post($rootScope.apiURLForS3 + '/login', credentials, {
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        withCredentials: true
                    }).success(function (data, status) {
                        if (data.status == 'ok') {
                            // $rootScope.reset();
                            $rootScope.userData = data['msg']
                            $rootScope.recons = data['msg']['reconProcess']
                            successLogin(data);
                            $rootScope.eventDoc = {
                                'type': 'User Logged In',
                                'actionOn': credentials.userName,
                                'logTime': Date.now(),
                                'createdBy': credentials.userName
                            }
                            AuditsService.post($rootScope.eventDoc, function (data) {
                                console.log(data)
                            })
                        } else {
                            if (data.msg == 'Password Expired') {
                                $rootScope.credentials.userName = credentials.userName
                                $rootScope.pswdExpired = true
                            }
                            $rootScope.showMessage('Login', data.msg, 'error')
                            $rootScope.credentials['password'] = ''
                            $rootScope.loginErrorMsg = data.msg;
                            $rootScope.loginSuccessMessage = undefined;
                        }
                    }).error(function (error, msg) {
                        $rootScope.loginErrorMsg = 'Invalid username or password.';
                        $rootScope.loginSuccessMessage = undefined;
                    });
                };

                function successLogin(data) {
                    if (data.status === 'error') {
                        $rootScope.$broadcast('loginFail');
                        $rootScope.loginErrorMsg = data.msg;
                        $rootScope.loginSuccessMessage = undefined;
                        $rootScope.showLoginErrorMsg = true;
                        $rootScope.isauthenticated = false;
                    } else {
                        $rootScope.loginErrorMsg = undefined;
                        $rootScope.loginSuccessMessage = 'Login successful'
                        $rootScope.userData = data.msg;
                        $rootScope.loginSuccessMessage = undefined;
                        angular.forEach($rootScope.rolesJson[$rootScope.userData['role']]['views'], function (v, k) {
                            $rootScope['tabs'][$rootScope.rolesJson[$rootScope.userData['role']]['views'][k]] = true

                        })

                        $rootScope.$broadcast('loginSuccess', 1);
                    }
                }

                $rootScope.$on('loginSuccess', function (event, message) {
                    $rootScope.isauthenticated = true;
                    $rootScope.requestedPath = $location.path();
                    $rootScope.redirectAfterLogin();
                });

                $rootScope.redirectAfterLogin = function () {
                    $rootScope.requestedPath = $rootScope.rolesJson[$rootScope.userData['role']]['default']
                    $location.url($rootScope.requestedPath);
                    $rootScope.path = $location.path()
                }

                $rootScope.checkSession = function () {
                    $rootScope.requestPath = $location.path()
                    $http.get('/api/login').error(function () {
                        $location.path('/login');
                        $rootScope.loading = false;
                        $rootScope.classFlag = true
                    }).success(function (data) {
                        if (data.status === 'error') {
                            $rootScope.errorMsg = data.msg;
                            $rootScope.isauthenticated = false
                            $rootScope.classFlag = true
                            $location.path('/login');
                        }
                        else {
                            $rootScope.isauthenticated = true
                            $rootScope.classFlag = false
                            $rootScope.userData = data.msg;
                            $rootScope.recons = data['msg']['reconProcess']
                            $rootScope.path = $location.path()
                            $rootScope.$broadcast('doLogoutloginSuccess', 1);
                            angular.forEach($rootScope.rolesJson[$rootScope.userData['role']]['views'], function (v, k) {
                                $rootScope['tabs'][$rootScope.rolesJson[$rootScope.userData['role']]['views'][k]] = true

                            })
                        }

                        if ($location.path() === '/') {
                            $location.path('/' + $rootScope.rolesJson[$rootScope.userData['role']]['default'])
                            $rootScope.path = $location.path()
                        } else {
                            if ($rootScope.tabs[$location.path().split('/')[1]] == undefined) {
                                $location.path('/' + $rootScope.rolesJson[$rootScope.userData['role']]['default'])
                                $rootScope.path = $location.path()
                            }
                            else {
                                $rootScope.path = $location.path()
                            }
                        }
                    });
                }

                init()
                $rootScope.checkSession()

                $rootScope.installedRecons = function () {
                    $rootScope.onlyInstalled = true
                }

                $rootScope.doLogout = function () {
                    $rootScope.recentPurchase = null
                    $rootScope.tempDetail = {};
                    $rootScope.loginErrorMsg = undefined;
                    $rootScope.loginSuccessMessage = undefined;
                    $http.defaults.withCredentials = true;
                    $rootScope.eventDoc = {
                        'type': 'User Logged Out',
                        'actionOn': $rootScope.userData.userName,
                        'logTime': Date.now(),
                        'createdBy': $rootScope.userData.userName
                    }

                    AuditsService.post($rootScope.eventDoc, function (data) {
                        $http.post($rootScope.apiURLForS3 + '/logout', {}, {
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            withCredentials: true
                        }).error(function () {
                            init();
                            $rootScope.credentials = {};
                            $rootScope.requestedPath = '/login';
                            $location.path('/login');
                        });
                    })
                };

                $rootScope.loadingModal = null;

                $rootScope.refreshReconView = function (recon) {
                    $rootScope.activeRecon = recon.name
                    $rootScope.actions = recon.actions
                    $location.path('/recons')
                }

                $rootScope.accountSetting = function () {
                    $rootScope.previousLocation = $location.path()
                    $location.path('/profile')
                }

                $rootScope.goBack = function () {
                    $location.path($rootScope.previousLocation)
                }

                $rootScope.updatePassword = function (details) {
                    details['userName'] = $rootScope.credentials.userName
                    if (details['oldPassword'] != undefined & details['newPassword'] != undefined) {
                        details['oldPassword'] = angularCryptoSerivces.encrypt
                        ($rootScope.cipherText['AES_KEY'], $rootScope.cipherText['IV'], details.oldPassword + '/<=%+,?->/')

                        details['newPassword'] = angularCryptoSerivces.encrypt
                        ($rootScope.cipherText['AES_KEY'], $rootScope.cipherText['IV'], details.newPassword + '/<=%+,?->/')
                    }

                    NoAuthService.updateExpirePassword('dummy', details, function (data) {
                        $rootScope.pswdExpired = false
                        $rootScope.update = {}
                        $rootScope.showMessage($rootScope.resources.updateProfile, data, 'success')
                        $location.path($rootScope.previousLocation)
                    }, function (err) {
                        $rootScope.update = {}
                        $rootScope.showMessage($rootScope.resources.updateProfile, err.msg, 'failure')
                    })
                }


                $rootScope.getForceMatchLog = function () {
                    ForceMatchService.get(undefined, undefined, function (data) {
                        if (data['total'] > 0) {
                            angular.forEach(data['data'], function (v, k) {
                                date = Date(v['dateTime'] * 1000);
                                v['dateTime'] = $filter('date')(date, 'd MMMM yyyy');
                            })
                            $rootScope.forceMatchLogDetails = data['data']
                        }
                        else {
                            $rootScope.showMessage($rootScope.resources.forceMatchLog, $rootScope.resources.forceMatchLogData, 'warning')
                        }

                    }, function (err) {
                        $rootScope.showMessage($rootScope.resources.forceMatchLog, err.msg, 'error')
                    })
                }

                $rootScope.sendPasswordToMail = function (credentials, associatedMail) {
                    var perColCondtions = {
                        'userName': credentials.userName,
                        'associatedMail': associatedMail
                    }
                    NoAuthService.sendPasswordToMail('dummy', perColCondtions, function (data) {
                        $rootScope.forgotPasswords = false
                        $rootScope.credentials = {}
                        $rootScope.showMessage($rootScope.resources.forgotPassword, data, 'success')
                    }, function (err) {
                        $rootScope.showMessage($rootScope.resources.forgotPassword, err.msg, 'error')
                    })
                }

                // Data table related functions
                // $rootScope.rowLimit = 10 // table row limit
                $rootScope.displayCount = [10, 25, 50]
                $rootScope.genDataTableQuery = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
                    var conditions = {}
                    if (angular.isDefined(filterByFields) & !angular.equals(filterByFields, {})) {
                        var perColConditions = {}
                        angular.forEach(filterByFields, function (v, k) {
                            perColConditions[k] = v
                        })
                        conditions['query'] = {'perColConditions': perColConditions}
                    }

                    if (angular.isDefined(orderBy)) {
                        var tmp = {}
                        tmp[orderBy] = orderByReverse ? -1 : 1
                        conditions['orderBy'] = tmp
                    }

                    conditions['skip'] = $rootScope.rowLimit * currentPage
                    conditions['limit'] = pageItems
                    return conditions
                }


                $rootScope.forgotPassword = function (credentials) {
                    $rootScope.forgotPasswords = false
                    if ((Object.keys(credentials).length != 0) & (credentials['userName'] != undefined)) {
                        $rootScope.forgotPasswords = true
                    }
                    else {
                        $rootScope.showMessage('Forgot Password', 'Please Enter the UserName', 'warning')
                    }
                    console.log(credentials)
                }
            })
        })
    }
]);
