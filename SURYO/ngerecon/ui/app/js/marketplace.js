'use strict';
var app = angular.module("nextGen");
app.controller("marketplaceController", ["$scope", "$route", "$location", "$timeout", "$uibModal", "$rootScope", "SourceReferenceService", "ReconsService", "ReconProcessService",
    function ($scope, $route, $location, $timeout, $uibModal, $rootScope, SourceReferenceService, ReconsService, ReconProcessService) {
        $rootScope.reconInfo = {'reconName': '', 'sources': []}
        // $rootScope.licensedRecons = []
        // $rootScope.marketrecons = []
        $rootScope.reconDetails = []
        $rootScope.reconProcessees = []
        $rootScope.processRecons = []
        $rootScope.reconsOfProcess = false
        ReconsService.get(undefined, {'limit': 100}, function (data) {
            $rootScope.licensedRecons = []
            $rootScope.marketrecons = []
            angular.forEach(data['data'], function (k, v) {
                if (k['isLicensed'] == true) {
                    $rootScope.licensedRecons.push(k)
                }
                else {
                    $rootScope.marketrecons.push(k)
                }
                if ($rootScope.marketrecons.length != 0) {
                    $rootScope.onlyLicensed = false
                }
                else {
                    $rootScope.onlyLicensed = true
                }
            })
        })

        ReconProcessService.get(undefined, {'limit': 100}, function (data) {
            $rootScope.reconProcessees = data['data']
            console.log(data)
        })


        $scope.installProcess = function (process) {
            if ($rootScope.breadCrumblist) {
                $rootScope.breadCrumblist = []
                if ($rootScope.breadCrumblist.indexOf($location.path() == -1)) {
                    $rootScope.breadCrumblist.push($location.path().split('/')[1])
                }
            }

            if (Object.keys(process).indexOf('isLicensed')!=-1 & process['isLicensed'] == true) {
                $scope.getProcessRecons(process)
            }
            else {
                $scope.confirmation = $uibModal.open({
                    templateUrl: 'app/components/notifications/installRecons.html',
                    windowClass: 'modal show modalDialogCenter',
                    backdrop: 'static',
                    backdropClass: 'show',
                    scope: $scope,
                    keyboard: false,
                    size: "md",
                    controller: function ($scope, $rootScope, $uibModalInstance) {
                        $scope.title = $rootScope.resources.confirmation
                        $scope.msg = $rootScope.resources.installRecons
                        $scope.submit = function () {
                            var query = {
                                'reconProcess': process['reconProcess'],
                                'isLicensed': true
                            }
                            ReconProcessService.saveProcessDetail('dummy', query, function (data) {
                                $scope.getProcessRecons(process)
                                $uibModalInstance.dismiss('cancel')
                            }, function (err) {
                                console.log(err.msg)
                            })
                        }
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel')
                        }
                    }
                })
            }
        }

        $scope.getProcessRecons = function (process) {
            ReconsService.getProcessRecons('dummy', {'reconProcess': process['reconProcess']}, function (data) {
                $rootScope.processRecons = data
                $rootScope.reconsOfProcess = true
            })
        }


        $scope.installRecon = function (recon) {
            var allSource = []
            $rootScope.breadCrumblist.push(recon['reconName'])
            var query = {}
            query['reconName'] = recon['reconName']
            angular.forEach(recon['sources'], function (structures, source) {
                angular.forEach(structures, function (keyCols, stuctName) {
                    var data = {}
                    data['source'] = source
                    data['struct'] = stuctName
                    data['reconName'] = recon['reconName']
                    data['mappedColumns'] = []
                    data['feedDetails'] = []
                    data['filters'] = []
                    data['referenceSource'] = false
                    allSource.push(data)
                })
            })
            query['sourceRef'] = allSource
            query['_id'] = recon['_id']
            SourceReferenceService.addSourceRef('dummy', query, function (data) {
                console.log(data)
                $scope.redirect(recon)
            }, function (err) {
                $rootScope.showMessage($rootScope.resources.error, err.msg, 'error')
            })
        }

        $scope.redirect = function (process) {
            $rootScope.reconInfo = process
            SourceReferenceService.getSourceForRecon('dummy', {'reconName': process['reconName']}, function (data) {
                if (data.length > 0) {
                    $rootScope.reconDetails = data
                    $location.path('/sourceTemplate')
                }
            }, function (err) {
                $rootScope.showMessage($rootScope.resources.error, err.msg, 'error')
            })
        }

    }])
;
