'use strict';
var app = angular.module("nextGen");
app.controller("reconJobController", ["$scope", "$route", "$window", "$location", "$interval", "$rootScope", "$upload", "$uibModal", "$filter", "$http", "ReconDetailsService", "ReconExecDetailsLogService",
    function ($scope, $route, $window, $location, $interval, $rootScope, $upload, $uibModal, $filter, $http, ReconDetailsService, ReconExecDetailsLogService) {

        // store the interval promise in this variable
        var promise;

        $scope.refreshExeDetails = function () {
            $rootScope.openModalPopupOpen()
            ReconExecDetailsLogService.get_latest_execution_details('dummy', function (data) {
                $rootScope.openModalPopupClose()
                $scope.reconExecLogs = []
                if (data['total'] > 0) {
                    $scope.reconExecLogs = data['data']
                } else {
                    $rootScope.showMessage($rootScope.resources.ndf, '', 'warning')
                }
            }, function (err) {
                $rootScope.showMessage(err.msg, '', 'error')
            })
        }

        $scope.refreshExeDetails()

        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };

        $scope.uploadFeedFile = function (index) {
            $scope.selReconDetail = $scope.reconExecLogs[index]
            $uibModal.open({
                templateUrl: 'job.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconDetailsService, $upload) {
                    // $scope.upload = function (files) {
                    $scope.runReconJob = function () {
                         $uibModalInstance.dismiss('cancel')
                        $scope.selReconDetail['statementDate'] = $filter('date')($scope.toEpoch($scope.selReconDetail['statementDate']) * 1000, 'dd/MM/yyyy')
                        $scope.id = 'dummy'
                        $http.post("/api/utilities/" + $scope.id + "/upload_recon_files", {'query': $scope.selReconDetail}, {headers: {"Content-Type": "application/json"}}).progress(function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            x
                        }).success(function (data, status, headers, config) {
                            if (data['status'] == "error") {
                                $rootScope.openModalPopupClose() 
                                $rootScope.showMessage('JobStatus', data['msg'], 'warning')
                            }
                            else {
                                $rootScope.showMessage('JobStatus', data['msg'], 'success')
                                $rootScope.openModalPopupClose()
                                $uibModalInstance.dismiss('cancel');
                            }
                        });
                    };
                    // };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            });
        }


        $scope.jobLogs = function (index) {
            var filePath = 'app/files/error_logs/' + $scope.reconExecLogs[index]['reconName'] + '_Log.log'
            console.log('---> Log Path ' + filePath)
            $window.open(filePath)
        }

        $scope.disableErrorButton = function (index) {
            console.log(index)
            $scope.reconExecLogs[index]
            if ($scope.reconExecLogs[index]['jobStatus'] != 'Failed') {
                return true
            }
            else {
                return false
            }
        }

        $scope.Rollback = function (index) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconExecDetailsLogService) {
                    $scope.deleteMsg = $rootScope.resources.rollBackMsg + $scope.reconExecLogs[index]['reconName'] + '?'
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    var dataR = {}
                    dataR['reconName'] = $scope.reconExecLogs[index]['reconName']
                    dataR['statementDate'] = $scope.reconExecLogs[index]['statementDate']

                    $scope.submit = function () {
                        ReconExecDetailsLogService.rollbackRecon('dummy', dataR, function (data) {
                            $rootScope.showMessage($rootScope.resources.reconRollback, $scope.reconExecLogs[index]['reconName'] + $rootScope.resources.rollBackSuccMsg, 'success')
                            $scope.refreshExeDetails()
                            $uibModalInstance.dismiss('cancel')
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }

        promise = $interval(function () {
            $scope.refreshExeDetails()
        }, 60000)

        // stops the interval
        $scope.$on('$destroy', function () {
            $interval.cancel(promise);
        });
    }]);
