// 'use strict';
var app = angular.module('nextGen');
app.controller('reconController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'ReconDetailsService',
    '$http', 'ColumnDataTypesSerice', 'ReconExecDetailsLogService',
    'ForceMatchService', 'ReconsService', 'ColumnConfigurationService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, ReconDetailsService, $http, ColumnDataTypesSerice,
              ReconExecDetailsLogService, ForceMatchService, ReconsService, ColumnConfigurationService) {
        $scope.selected = true;
        $scope.minus = false;
        $scope.show = false;
        $scope.statementSearch = false;
        $scope.columnsBaseSearch = false;
        $scope.filter = false;
        $scope.transactionData = []
        $scope.downloadingReports = ['Report', 'All Matched', 'All UnMatched', 'ConsolidatedReport']
        $scope.primaryMatchColumns = []
        var inVoiceColumns = ['NEFT Number', 'NEFT Date', 'Remarks']
        $scope.secondaryMatchColumns = []
        $scope.search = {};
        $scope.doForceMatch = false;
        $scope.summary = true;
        $scope.pageIndex = 1;
        $scope.selectfilter = {'expression': []};
        $scope.advFilters = [{'selectedColumn': '', 'operation': '', 'value': ''}];
        $scope.exportDisable = true;
        $scope.showRows = false
        $scope.selectedRows = {}
        $scope.disableFlag = true
        $scope.columns = {}
        $scope.columnLookup = []
        $scope.reorderMapper = {}


        $scope.initGrid = function () {
            $scope.gridOptions = {
                defaultColDef: {
                    // allow every column to be aggregated
                    enableValue: true,
                    // allow every column to be grouped
                    enableRowGroup: true,
                    // allow every column to be pivoted
                    enablePivot: false
                },
                columnDefs: [],
                enableSorting: true,
                showToolPanel: true,
                rowMultiSelectWithClick: true,
                enableFilter: true,
                enableColResize: true,
                groupSelectsChildren: true,
                groupSelectsFiltered: true,
                rowSelection: 'multiple',
                checkboxSelection: true,
                rowGroupPanelShow: 'always',
                floatingFilter: true,
                enableColResize: true,
                // suppressMenu:true,
                pagination: false,
                onSelectionChanged: selectionChangedFunc
            };
        }

        $scope.primaryGridOptions = {
            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },
            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            onSelectionChanged: $scope.getPrimaryLength,
            enableColResize: true
        };


        $scope.matchGridOptions = {

            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },
            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            groupSelectsChildren: true,
            groupSelectsFiltered: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            enableColResize: true,
            onSelectionChanged: selectionChangedFunc
        };

        $scope.finalGridOptions = {

            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },

            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            enableColResize: true
        };


        function selectionChangedFunc() {
            $scope.selectedRecords = $scope.matchGridOptions.api.getSelectedRows();
            $scope.disableFlag = false;
            $scope.$apply();
        }

        $scope.cols = [];

        $rootScope.initializeActiveRecon = function (query) {
            $scope.reports = [];
            $scope.source = [];
            $scope.chevronList = [];
            $scope.reportDetails = []
            $scope.amounts = {'Matched': {}, 'Exception': {}};
            $scope.sourceReport = {};
            $rootScope.reconselected = true;
            $scope.sourcePanel = true;
            $scope.columnReorder = false
            $scope.gridOptions.api.setColumnDefs([])
            $scope.gridOptions.api.setRowData([])

            if ($rootScope.activeRecon != undefined) {
                $rootScope.openModalPopupOpen()
                ReconDetailsService.getReportDetails('dummy',
                    query, function (data) {
                        $scope.reportDetails = data.details;

                        angular.forEach($scope.reportDetails, function (v, k) {
                            $scope.amounts.Matched[v.Source] = ''
                            $scope.amounts.Exception[v.Source] = ''
                            $scope.amounts.Matched[v.Source] = v['Matched Amount'];
                            $scope.amounts.Exception[v.Source] = v['UnMatched Amount'];
                        });

                        $scope.reports = data['customReports'];
                        $scope.search.statementDate = data.statementDate
                        $scope.sources = data.sources;
                        $scope.show = true

                        $rootScope.openModalPopupClose();
                    }, function (err) {
                        $rootScope.openModalPopupClose();
                        // $scope.initGrid()
                        $scope.show = true
                        $rootScope.showMessage
                        ($rootScope.resources.message, err.msg, 'warning');
                    });
            }
        }

        $scope.$watch('$root.activeRecon', function () {
            var query = {'reconName': $rootScope.activeRecon};
            var perColConditions = {'perColConditions': query}
            $rootScope.initializeActiveRecon(query);
            $scope.doForceMatch = false;
            if ($rootScope.actions == 'Authorization') {
                ReconsService.get('dummy', {
                    'reconName': $rootScope.activeRecon
                }, function (data) {
                    $scope.sources = Object.keys(data.sources);
                });
                $scope.getWaitingForAuthorizatios();
            }
        })

        $scope.getWaitingForAuthorizatios = function () {
            var query = {}
            query.reconName = $rootScope.activeRecon
            ForceMatchService.getWaitingForAuthorization('dummy', query, function (data) {
                if (data.total > 0) {
                    $scope.columnDefs = [{
                        headerName: '',
                        width: 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }]

                    $scope.transactionData = data.data
                    hideFields = []
                    angular.forEach($scope.transactionData[0], function (v, k) {
                        if (k == 'LINK_ID') {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302,
                                'rowGroupIndex': 1
                            });
                        }
                        if (k == 'AUTHORIZATION_STATUS') {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302,
                                'rowGroupIndex': 0
                            });
                        }

                        else {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302
                            });
                        }
                    })
                    $scope.showdiscovery = false
                    $scope.sourcePanel = true
                    $scope.columnReorder = false
                    $scope.selectedReport = 'Authorization'
                    $scope.gridOptions.columnDefs = $scope.columnDefs;
                    $scope.gridOptions.rowData = $scope.transactionData;
                    $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                    $scope.gridOptions.api.setRowData($scope.transactionData);
                }
                else {
                    $rootScope.showMessage('Authourize', data.msg, 'warning');
                }
            });
        };

        $scope.downloadCustomReport = function (index) {
            var query = {}
            query['reconName'] = $scope.activeRecon
            query['report'] = $scope.reports[index]

            ReconDetailsService.downloadCustomReport('dummy', query, function (data) {
                if (data != '') {
                    var filePath = 'app/files/' + data
                    $window.open(filePath)
                }
                else {
                    $rootScope.showMessage('Report', 'File Not Found', 'warning')
                }
            }, function (err) {
                $rootScope.showMessage('Report', err.msg, 'error')
            })
        }

        $scope.authorizeRecord = function (operation) {
            var query = {}
            query.reconName = $scope.activeRecon
            query.operation = operation
            query.selectedRows = $scope.gridOptions.api.getSelectedRows()
            $rootScope.openModalPopupOpen()
            ForceMatchService.authorizeRecord('dummy', query, function (data) {
                $scope.transactionData = []
                if (data.length > 0) {
                    $scope.columnDefs = [{
                        headerName: '',
                        width: 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }]
                    $scope.transactionData = data
                    hideFields = []
                    // check if model fields are configured, if not show all columns
                    angular.forEach($scope.transactionData[0], function (v, k) {
                        if (k == 'LINK_ID') {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302,
                                'rowGroupIndex': 0
                            });
                        }
                        else {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302
                            });
                        }
                    });
                }
                $scope.showdiscovery = false
                $scope.sourcePanel = true
                $rootScope.openModalPopupClose()
                $scope.columnReorder = false
                $scope.selectedReport = 'Authorize'
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData;
                $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.gridOptions.api.setRowData($scope.transactionData);

                $rootScope.reconselected = false;
                $scope.sourcePanel = false;

                $rootScope.initializeActiveRecon({'reconName': $rootScope.activeRecon})
                // $scope.getWaitingForAuthorizatios()
                $rootScope.showMessage('Authorize', 'Done successfully', 'success')
            }, function (err) {
                $rootScope.openModalPopupClose()
                $rootScope.showMessage('Authorize', err.msg, 'error')
            });
        }

        // $scope.rollBackRecords = function () {
        //     var query = {}
        //     query.reconName = $rootScope.activeRecon
        //     query.selectedRows = $scope.gridOptions.api.getSelectedRows()
        //     ForceMatchService.rollBackRecords('dummy', query, function (data) {
        //         $rootScope.showMessage('RollBack', 'Done successfully', 'success')
        //     }, function (err) {
        //         $rootScope.showMessage('RollBack', err.msg, 'error')
        //     })
        // }
        $http.get('/app/json/advanceSearch.json')
            .success(function (data) {
                console.log(data)
                $scope.jsonData = data
                $scope.searchVal = Object.keys(data);
            })
            .error(function (data) {
                console.log('there was an error');
            });


        $scope.initGrid()

        $scope.editReportName = function (name) {
            $scope.editStruct = true;
            $scope.oldReportName = name;
        }

        $scope.saveConfiguration = function () {
            var dataR = {}
            $scope.columnList = []
            angular.forEach($scope.columns.selectedColumns, function (key, val) {
                $scope.columnList.push(key['column'])
            })
            dataR['reconName'] = $rootScope.activeRecon
            dataR['source'] = $scope.reorderSource
            dataR['columns'] = $scope.columnList
            $scope.columnLookup.push(dataR)
            ColumnConfigurationService.post(dataR, function (data) {
                console.log(data);
            });
            viewColumns = true
            $scope.columns = {}
            $scope.reorderSource = ''
            $scope.sourceColumns = []
        }

        $scope.deleteSouceColumnOrder = function (index) {
            var perColCondtions = {
                'reconName': $scope.columnLookup[index]['reconName'],
                'source': $scope.columnLookup[index]['source']
            }
            $scope.columnLookup.splice(index, 1)
            ColumnConfigurationService.removeSourceColumns('dummy', perColCondtions, function (data) {
                $rootScope.showMessage($rootScope.resources.reorderColumn, data, 'success')

            }, function (err) {
                $rootScope.showMessage($rootScope.resources.reorderColumn, err.msg, 'warning')
            });
        }

        $scope.saveReportName = function (report) {
            $scope.editStruct = false
            var perColCondtions = {'selectedRecon': $rootScope.activeRecon, 'report': report}
            angular.forEach($scope.reportDetails, function (k, v) {
                if ($scope.selectedSource == k) {
                    angular.forEach(v, function (k, v) {
                        if (k.reportPath == report.reportPath) {
                            k.reportName = report.reportName;
                        }
                    });
                }
            })
            ReconDetailsService.updateReportName('dummy', perColCondtions, function (data) {
                console.log(data);
            });
        }

        $scope.getpageIndex = function (index) {
            $scope.pageIndex = index;
        };

        $scope.reorderColumns = function () {
            $scope.columnReorder = true
            var perColConditions = {
                'perColConditions': {
                    'reconName': $rootScope.activeRecon,
                }
            }
            ColumnDataTypesSerice.get(undefined, {'query': perColConditions}, function (data) {
                console.log(data)
                $scope.columnOrder = data['data']
            }, function (err) {
                $rootScope.showMessage($rootScope.resources.columnType, $rootScope.resources.defaultMsg, 'warning')
            })


            ColumnConfigurationService.get(undefined, {'query': perColConditions}, function (data) {
                if (data['total'] > 0) {
                    $scope.columnLookup = data['data']
                }
            }), function (err) {
                $rootScope.showMessage($rootScope.resources.columnType, $rootScope.resources.defaultMsg, 'warning')
            }
        }

        $scope.getSourceColumns = function (source) {
            $scope.reorderSource = source;
            angular.forEach($scope.columnOrder, function (val, key) {
                if (val['source'] == source) {
                    $scope.sourceColumns = val['dtypes']
                }
            })
        }

        $scope.changeSelectAll = function (checked) {
            angular.forEach($scope.columOrder, function (val, key) {
                val['isSelected'] = checked;
            });
        }

        $scope.searchPaginationRecords = function () {
            if ($scope.filter == true) {
                $scope.getFilteredRecord($scope.advFilters);
            }
            else {
                $scope.getRecords($scope.report, $scope.pageRecords);
            }
        }

        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };

        $scope.addExpression = function (searchParams) {
            var data = {}
            if (angular.isDefined(searchParams['selectedColumn'])) {
                data['expression'] = $scope.jsonData[searchParams['jsonDtype']][searchParams['operation']]
                data['expression'] = data['expression'].replace('column', searchParams['selectedColumn'])
                data['expression'] = data['expression'].replace('value', searchParams['value'])
                return data['expression']
            }
        }

        $scope.getFilteredRecord = function (searchParams) {
            var exp = []
            angular.forEach(searchParams, function (k, v) {
                exp.push($scope.addExpression(k))
            })
            var data = {}
            data['reconName'] = $scope.transactionData['reconName']
            data['reportPath'] = $scope.transactionData['report']
            data['source'] = $scope.selectedSource
            data['index'] = $scope.pageIndex
            data['pageSize'] = 5000
            data['expressions'] = exp

            if ($scope.search['stmtDate'] != null && angular.isDefined($scope.search['stmtDate'])) {
                data['stmtDate'] = $filter('date')(new Date($scope.toEpoch($scope.search.stmtDate) * 1000), 'dd/MM/yy')
                searchParams['stmtDate'] = $scope.search['stmtDate']
            }

            $rootScope.openModalPopupOpen()
            ReconDetailsService.getFilteredData('dummy', data, function (data) {

                $scope.transactionData = data
                $scope.columnDefs = []

                angular.forEach($scope.transactionData['columns'], function (k, v) {
                    $scope.columnDefs.push({'field': k, 'filter': 'agTextColumnFilter'})
                })
                $scope.maxPages = $scope.transactionData['maxPages']
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData['data'];

                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs)
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)
                $rootScope.openModalPopupClose()

            }, function (err) {
                // TODO, use pop up notification
                console.log(err)
            })

        }

        $scope.removeFilter = function (item) {
            angular.forEach($scope.selectfilter['expression'], function (val, index) {
                if (val == item) {
                    $scope.selectfilter['expression'].splice(index, 1)
                }

            })
        }

        $scope.resetSelectParams = function () {
            // $scope.sourcePanel = !$scope.sourcePanel
            // $scope.filter = !$scope.filter
            // $scope.columnsBaseSearch = false
            $scope.pageIndex = 1
            $scope.search = {}
        }

        $scope.getRecords = function (name, report, index) {
            $scope.wantedReport = $scope.reportDetails[index]
            if ($scope.index != index || $scope.name != name) {
                $scope.pageIndex = 1
            }
            $scope.name = name
            $scope.report = report
            $scope.index = index
            $scope.selectfilter = {'expression': []}
            $scope.advFilters = []
            $scope.pageRecords = $scope.wantedReport
            $scope.amountDetails = $scope.amounts[report]
            var data = {}

            if (name == 'forceMatched') {
                data['reportPath'] = $scope.wantedReport.Source + '_ForceMatched.csv'
            }
            else if (name == 'authorization') {
                data['reportPath'] = $scope.wantedReport.Source + '_Authorization.csv'
                $scope.getWaitingForAuthorizatios()
                return
            }
            else {
                data['reportPath'] = $scope.wantedReport[name]
            }
            data['reconName'] = $rootScope.activeRecon
            data['source'] = $scope.wantedReport.Source
            data['index'] = $scope.pageIndex
            data['pageSize'] = 50000
            data['carryForward'] = false
            if (report == 'carryForward') {
                data['carryForward'] = true
            }
            var perColConditions = {
                'perColConditions': {
                    'reconName': $rootScope.activeRecon,
                    'source': $scope.wantedReport.source
                }
            }

            $scope.selectedSource = $scope.wantedReport.Source
            ColumnDataTypesSerice.get(undefined, {'query': perColConditions}, function (data) {
                console.log(data)
                $scope.columnDataType = data['data']
            }, function (err) {
                $rootScope.showMessage($rootScope.resources.columnType, $rootScope.resources.defaultMsg, 'warning')
            })

            $rootScope.openModalPopupOpen()
            ReconDetailsService.getReportData('dummy', data, function (data) {
                $scope.summary = false
                $scope.transactionData = data
                $scope.columnDefs = []
                $scope.columnDefs = [
                    {
                        headerName: '',
                        'width': 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }
                ]

                angular.forEach($scope.transactionData['columns'], function (k, v) {
                    $scope.columnDefs.push({'field': k, 'filter': 'agTextColumnFilter'})
                })
                $scope.maxPages = $scope.transactionData['maxPages']
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData['data'];

                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs)
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)
                // $scope.toolPanel = true
                $scope.sourcePanel = true
                // $rootScope.rightPanel = true
                $scope.selectedReport = report
                $rootScope.openModalPopupClose()
            }, function (err) {
                $scope.initGrid()
                $rootScope.showMessage($rootScope.resources.records, $rootScope.resources.defaultMsg, 'warning')
            })
        }

        $scope.getOperation = function (params) {
            $scope.operationList = []
            $scope.filter = true
            $scope.pageIndex = 1
            $scope.columnDtypeMapper = $scope.transactionData['columnGrouper']
            angular.forEach($scope.columnDtypeMapper, function (val, key) {
                angular.forEach(val, function (v, index) {
                    if (v == params['selectedColumn']) {
                        params['jsonDtype'] = key
                        $scope.operationList = Object.keys($scope.jsonData[key])
                    }
                })
            })

        }

        $scope.exportsSelectedReportFormat = function (downloadingReport) {
            if (downloadingReport == 'Report') {
                $scope.exportAllData($scope.selectedReport)
            }

            else if (downloadingReport == 'All UnMatched') {
                $scope.exportOldFormat('UnMatched')
            }

            else if (downloadingReport == 'All Matched') {
                $scope.exportOldFormat('Matched')
            }

            else if (downloadingReport == 'ConsolidatedReport') {
                $scope.exportConsolidateReport()
            }

            else {
                $rootScope.showMessage("Report Type", "Please Select Report Type", 'warning')
            }
        }

        $scope.exportAllData = function (report) {
            var data = {}
            if (report == 'ForceMatched') {
                data['reportPath'] = $scope.wantedReport['Source'] + '_ForceMatched.csv'
            }
            else if (report == 'Authorization') {
                data['reportPath'] = $scope.wantedReport['Source'] + '_Authorization.csv'
            }
            else {
                data['reportPath'] = $scope.wantedReport[$scope.name]
            }
            // data['reportName'] = report.reportName
            data['reconName'] = $rootScope.activeRecon
            // data['source'] = report.source
            ReconDetailsService.exportAllCsv('dummy', data, function (data) {
                var filePath = 'app/files/' + data
                $window.open(filePath)
            })
        }

        $scope.addAdvFilter = function () {
            $scope.advFilters.push({'selectedColumn': '', 'operation': '', 'value': '', 'jsonDtype': ''})
        }

        $scope.deleteAdvFilter = function (index) {
            $scope.advFilters.splice(index, 1)
        }

        $scope.getSourceData = function (val) {
            $scope.forceActiveSrc = val
        }


        $scope.getPrimaryCols = function (prsrc) {
            var data = {}
            $scope.allCols = {}
            $scope.otherSources = []
            $scope.primaryMatchColumns = []
            // data.source = prsrc
            data.reconName = $rootScope.activeRecon
            var perColConditions = {'perColConditions': data}
            ColumnDataTypesSerice.get(undefined, {'query': perColConditions}, function (data) {

                angular.forEach(data['data'], function (k, v) {
                    $scope.allCols[k['source']] = k['dtypes']
                    if (k['source'] == prsrc) {
                        angular.forEach(k['dtypes'], function (val, key) {
                            if (val['dtype'] == 'np.float64' || $scope.columns.indexOf(val['column']) != -1) {
                                $scope.primaryMatchColumns.push(val)
                            }
                        })
                    }
                })
                if (angular.isDefined($scope.initialForceData)) {
                    $scope.getPrimaryData(prsrc, $scope.primaryMatchColumns, false)
                }
            })
            angular.forEach($scope.sources, function (k, v) {
                if (k !== prsrc) {
                    $scope.otherSources.push(k)
                }
            })
            // $scope.othersSource = $scope.sources.
        }

        $scope.initializeForcematch = function (operation) {
            $scope.doForceMatch = true
            $scope.operation = operation
            var query = {}
            query['reconName'] = $rootScope.activeRecon
            query['sources'] = $scope.sources
            query['operation'] = operation
            $rootScope.openModalPopupOpen()
            $rootScope.reconselected = false;
            $scope.sourcePanel = false;
            ForceMatchService.readSourceData('dummy', query, function (data) {
                console.log(data)
                $scope.primaryTotal = data['total']
                $scope.columnDefs = [{
                    headerName: '',
                    width: 30,
                    headerCheckboxSelection: true,
                    headerCheckboxSelectionFilteredOnly: true,
                    checkboxSelection: true,
                    suppressAggregation: true,
                    suppressSorting: true,
                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                    pinned: true
                }]

                $scope.transactionData = data.allSourceData
                hideFields = []

                // check if model fields are configured, if not show all columns
                angular.forEach(data['columns'], function (v, k) {
                    if ((operation == 'rollBack') && (v == 'LINK_ID')) {
                        $scope.columnDefs.push({
                            'headerName': v,
                            'field': v,
                            'width': 302,
                            'rowGroupIndex': 0
                        });
                    }
                    else {
                        $scope.columnDefs.push({
                            'headerName': v,
                            'field': v,
                            'width': 302,
                        })

                    }
                })

                $scope.matchGridOptions.columnDefs = $scope.columnDefs;
                $scope.matchGridOptions.rowData = $scope.transactionData;
                $scope.matchGridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.matchGridOptions.api.setRowData($scope.transactionData)
                $rootScope.openModalPopupClose()
            }, function (err) {
                console.log(err)
                $rootScope.showMessage(operation, err.msg, 'warning')
            })
        }


        $scope.gethistoricalData = function () {
            $scope.getOldData = true
            $rootScope.reconselected = false;
            $scope.sourcePanel = false;
            $scope.prevParams = {}
            $scope.prevParams.executionStartDate = 0
            $scope.prevParams.executionEndDate = 0
            $scope.prevParams.statementStartDate = 0
            $scope.prevParams.statementEndDate = 0
        }

        $scope.queryHistoricalData = function () {
            dataD = {}
            dataD['executionStartDate'] = (date.executionStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.prevParams.executionStartDate) * 1000), 'dd/MM/yy') : 0;
            dataD['executionEndDate'] = (date.executionEndDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.prevParams.executionEndDate) * 1000), 'dd/MM/yy') : 0;
            dataD['statementStartDate'] = (date.statementStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.prevParams.statementStartDate) * 1000), 'dd/MM/yy') : 0;
            dataD['statementEndDate'] = (date.statementEndDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.prevParams.statementEndDate) * 1000), 'dd/MM/yy') : 0;
            dataD['recon_name'] = $rootScope.activeRecon
            $rootScope.openModalPopupOpen()

            ReconDetailsService.getHistoricalData('dummy', dataD, function (data) {
                console.log(data)
                if (data != '') {
                    $rootScope.openModalPopupClose()
                    var filePath = 'app/files/' + data
                    $window.open(filePath)
                }
                else {
                    $rootScope.openModalPopupClose()
                    $rootScope.showMessage('Report', 'File Not Found', 'warning')
                }

            })
        }


        $scope.searchRecords = function (setAsDefault) {
            $scope.showdiscovery = true
            $scope.secondaryMatchColumns.push({'column': 'System_Idx', 'dtype': 'str'})
            if (setAsDefault) {
                ForceMatchService.get('dummy', {'reconName': $rootScope.activeRecon}, function (data) {
                    console.log(data)
                    if (data) {
                        data[$scope.otherSrc] = $scope.secondaryMatchColumns
                        ForceMatchService.put(data['_id'], data, function (data) {
                            console.log(data)
                        }, function (err) {
                            console.log(err)
                        })
                    }
                    else {
                        var data = {}
                        data['reconName'] = $rootScope.activeRecon
                        data[$scope.otherSrc] = $scope.secondaryMatchColumns
                        ForceMatchService.post(data, function (data) {
                            console.log(data)
                        })
                    }
                }, function (err) {
                    console.log(err)
                })
            }

            ForceMatchService.readMatchData('dummy', {
                'primary': $scope.primarySource,
                'matchSource': $scope.otherSrc,
                'secondaryMatchColumns': $scope.secondaryMatchColumns,
                'selectedRows': $scope.primaryGridOptions.api.getSelectedRows(), 'primaryMatchColumns': $scope.cols,
                'reconName': $rootScope.activeRecon,
                'role': $rootScope.actions
            }, function (data) {
                console.log(data)
                $scope.columnDefs = [{
                    headerName: '',
                    width: 30,
                    headerCheckboxSelection: true,
                    headerCheckboxSelectionFilteredOnly: true,
                    checkboxSelection: true,
                    suppressAggregation: true,
                    suppressSorting: true,
                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                    pinned: true
                }]

                $scope.othrSrc = data.secondary
                $scope.transactionData = data.matchSourceData
                hideFields = []
                // check if model fields are configured, if not show all columns
                angular.forEach($scope.transactionData[0], function (v, k) {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302,
                    })
                })
                $scope.showdiscovery = false
                $scope.matchGridOptions.columnDefs = $scope.columnDefs;
                $scope.matchGridOptions.rowData = $scope.transactionData;
                $scope.matchGridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.matchGridOptions.api.setRowData($scope.transactionData)
            })
        }

        // $scope.getSecondaryData = function (otherSrc) {
        //     $scope.otherSrc = otherSrc
        //     $scope.selectedRows[otherSrc] = $scope.matchGridOptions.api.getSelectedRows()
        //     $scope.secondaryMatchColumns.push({'column': 'System_Idx', 'dtype': 'str'})
        //     angular.forEach($scope.allCols[otherSrc], function (v, k) {
        //         if (v['dtype'] == 'np.float64') {
        //             $scope.secondaryMatchColumns.push(v)
        //         }
        //     })
        // }

        $scope.getPrimaryData = function (prsrc, primaryMatchColumns, setAsDefault) {
            data = {}
            // $scope.selectColumns.push(index)
            data.source = prsrc
            if (setAsDefault) {
                var src = {}
                src['reconName'] = $rootScope.activeRecon
                src['primarySource'] = prsrc
                src[prsrc] = $scope.primaryMatchColumns
                ForceMatchService.post(src, function (data) {

                })
            }
            primaryMatchColumns.push({'column': 'System_Idx', 'dtype': 'str'})
            data.reconName = $rootScope.activeRecon
            data.dtypes = primaryMatchColumns
            data.role = $rootScope.actions

            $rootScope.openModalPopupOpen()
            ForceMatchService.readReportData('dummy', data, function (data) {
                console.log(data)
                console.log('*****************')
                $scope.primaryColumns = data.primaryCol
                if (Object.keys(data).length == 0) {

                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        // $rootScope.showAlertMsg("No Records Found in DB.")
                    }

                }
                else if (data.total == 0) {
                    if (!$scope.successFlag) {
                        $scope.successFlag = false
                        // $rootScope.showAlertMsg($rootScope.resources.noDataFound)
                    }
                }
                else {
                    //load data
                    $scope.primaryTotal = data['total']
                    $scope.columnDefs = [{
                        headerName: '',
                        width: 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }]

                    $scope.transactionData = data.primaryData
                    hideFields = []

                    // check if model fields are configured, if not show all columns
                    angular.forEach($scope.transactionData[0], function (v, k) {
                        if (k.indexOf('Status') != -1 | k.indexOf('Remarks') != -1) {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'editable': true,
                                'width': 302,
                            })
                        } else {
                            $scope.columnDefs.push({
                                'headerName': k,
                                'field': k,
                                'width': 302,
                            })
                        }
                    })

                    $scope.primaryGridOptions.columnDefs = $scope.columnDefs;
                    $scope.primaryGridOptions.rowData = $scope.transactionData;
                    $scope.primaryGridOptions.api.setColumnDefs($scope.columnDefs)
                    $scope.primaryGridOptions.api.setRowData($scope.transactionData)
                }
                $rootScope.openModalPopupClose()
                // if (angular.isDefined(isForceMatch)) {
                //     $rootScope.showSuccessMsg('Force Match Done Successfully');
                // }
            })
        }

        $scope.active = {}
        $scope.forceMatchSteps = ['Secondary', 'Comments']
        $scope.present = 'Secondary'
        $scope.next = 'Comments'
        $scope.nextStep = function (tab) {
            $scope.prev = tab
            $scope.present = $scope.next
            $scope.active[tab] = true
            if ($scope.present == 'Comments') {
                $scope.cols = []
                $scope.selectedRows = $scope.matchGridOptions.api.getSelectedRows()
            }
            $scope.columnDefs = []
            $scope.columnDefs.push({
                'headerName': 'Force Reconciliation Status',
                'field': 'Force Reconciliation Status',
                "editable": true,
                'cellEditor': "agRichSelectCellEditor",
                'cellEditorParams': {
                    values: ["Reconciled with Less Income", "Reconciled with Excess Income", "Reconciled with Incorrect or Alternate Policy", "Reconciled & Fully Cancelled", "Reconciled & Partially Cancelled", "Cancelled-Double Booking", "Cancelled-Booked In Other Code", "Cancelled-Cheque Dishonour", "Cancelled-Wrong Insurer", "Cancelled-Incorrect Booking", "Cancelled-TP Policies"],
                }
            })
            // check if model fields are configured, if not show all columns
            angular.forEach($scope.selectedRows[0], function (v, k) {
                if (k.indexOf('Status') != -1 | k.indexOf('Remarks') != -1) {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'editable': true,
                        'width': 302,
                    })
                } else {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302,
                    })
                }
            })
            $scope.finalGridOptions.columnDefs = $scope.columnDefs;
            $scope.finalGridOptions.rowData = $scope.selectedRows;
            $scope.finalGridOptions.api.setColumnDefs($scope.finalGridOptions.columnDefs)
            $scope.finalGridOptions.api.setRowData($scope.finalGridOptions.rowData)
        }

        $scope.forceMatch = function (comments) {
            $scope.active = {}
            var dataR = {}
            dataR.forceMatchData = $scope.selectedRows
            dataR.comments = comments
            dataR.reconName = $rootScope.activeRecon
            dataR.operation = $scope.operation

            $rootScope.openModalPopupOpen()
            ForceMatchService.forceMatchReconData('dummy', dataR, function (data) {
                $scope.transactionData = []
                $scope.successFlag = true
                $scope.comments = ''
                $scope.present = 'Secondary';
                $scope.next = 'Comments';
                $rootScope.openModalPopupClose()
                $scope.initializeForcematch($scope.operation)
                $rootScope.showMessage('ForceMatch', 'Record Send To Authorization', 'success')
            }, function (err) {
                $rootScope.openModalPopupClose()
                $rootScope.displayMessage(err.msg, 'md', 'warning')
            })
            console.log($scope.selectedRecords)
        }

        $scope.previousStep = function (prev) {
            $scope.next = $scope.present
            if ($scope.forceMatchSteps.indexOf(prev) != 0) {
                $scope.prev = $scope.forceMatchSteps[$scope.forceMatchSteps.indexOf(prev) - 1]
            }
            $scope.active[prev] = false
            $scope.present = prev
        }

        $scope.expandSelectedSource = function (source) {
            $scope.showRows = true
            $scope.showSrc = source
            $scope.columnDefs = []
            $scope.columnDefs.push({
                'headerName': 'Force Reconciliation Status',
                'field': 'Force Reconciliation Status',
                "editable": true,
                'cellEditor': "agRichSelectCellEditor",
                'cellEditorParams': {
                    values: ["Reconciled with Less Income", "Reconciled with Excess Income", "Reconciled with Incorrect or Alternate Policy", "Reconciled & Fully Cancelled", "Reconciled & Partially Cancelled", "Cancelled-Double Booking", "Cancelled-Booked In Other Code", "Cancelled-Cheque Dishonour", "Cancelled-Wrong Insurer", "Cancelled-Incorrect Booking", "Cancelled-TP Policies"],
                }
            })
            // check if model fields are configured, if not show all columns
            angular.forEach($scope.selectedRows[0], function (v, k) {
                if (k.indexOf('Status') != -1 | k.indexOf('Remarks') != -1) {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'editable': true,
                        'width': 302,
                    })
                } else {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302,
                    })
                }
            })
            // var data = []
            // data.push($scope.selectedRows[source])
            $scope.finalGridOptions.columnDefs = $scope.columnDefs;
            $scope.finalGridOptions.rowData = $scope.selectedRows[source];
            $scope.finalGridOptions.api.setColumnDefs($scope.finalGridOptions.columnDefs)
            $scope.finalGridOptions.api.setRowData($scope.finalGridOptions.rowData)
        }

        $scope.exportConsolidateReport = function () {
            var query = {}
            query['reconName'] = $scope.activeRecon
            query['reportPath'] = 'ConsolidatedReport.xlsx'
            ReconDetailsService.exportAllCsv('dummy', query, function (data) {
                var filePath = 'app/files/' + data
                $window.open(filePath)
            }, function (err) {
                $rootScope.showMessage('File', err.msg, 'warning')
            })
        }

        $scope.exportOldFormat = function (type) {
            var query = {}
            query['reconName'] = $scope.activeRecon
            query['type'] = type
            ReconDetailsService.getOlfFormatReport('dummy', query, function (data) {
                var filePath = 'app/files/' + data
                $window.open(filePath)
            }, function (err) {
                $rootScope.showMessage('File', err.msg, 'warning')
            })
        }

        $scope.getStatementDateFilterReports = function () {
            var dataR = {}
            $scope.searchDate = $filter('date')($scope.toEpoch($scope.searchDate) * 1000, 'dd/MM/yyyy')
            dataR['stmtDate'] = $scope.searchDate
            dataR['reconName'] = $rootScope.activeRecon
            ReconsService.getReports('dummy', dataR, function (data) {
                $scope.downloadReportsDict = data
                $scope.showDownloadReports = true
                // $rootScope.openModalPopupClose()
            }, function (err) {
//                $rootScope.openModalPopupClose()
                $rootScope.displayMessage(err.msg, 'md', 'warning')
            })
        }

        $scope.downloadPreviousDaysReports = function () {
            $scope.downloadReports = true
            $scope.rightPanel = !$scope.rightPanel
//          $rootScope.reconselected = false;
            $scope.sourcePanel = false;

        }


        $scope.downloadFiles = function (index) {
            file = $scope.downloadReportsDict[index]['downloadPath']
            $window.open('app/files/' + file)

        }

        $scope.getStatementDateFilterReports = function () {
            var dataR = {}
            $scope.searchDate = $filter('date')($scope.toEpoch($scope.searchDate) * 1000, 'dd/MM/yyyy')
            dataR['stmtDate'] = $scope.searchDate
            dataR['reconName'] = $rootScope.activeRecon
            ReconsService.getReports('dummy', dataR, function (data) {
                $scope.downloadReportsDict = data
                $scope.showDownloadReports = true
                // $rootScope.openModalPopupClose()
            }, function (err) {
//                $rootScope.openModalPopupClose()
                $rootScope.displayMessage(err.msg, 'md', 'warning')
            })


        }

        $scope.downloadFiles = function (index) {
            file = $scope.downloadReportsDict[index]['downloadPath']
            $window.open('app/files/' + file)

        }

    }

]);



