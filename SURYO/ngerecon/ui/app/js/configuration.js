var app = angular.module('nextGen');
app.controller('configurationController', ['$scope', '$route', '$location', '$timeout', '$uibModal', '$interval', "$window",
    '$rootScope', '$http', 'ReconsService', 'fileUploadService', 'ReconProcessService', 'SourceReferenceService', 'MatchRuleReferenceService', 'ReconDetailsService',
    function ($scope, $route, $location, $timeout, $uibModal, $interval, $window, $rootScope, $http, ReconsService, fileUploadService, ReconProcessService, SourceReferenceService, MatchRuleReferenceService, ReconDetailsService) {

        $scope.expressions = [];
        $scope.sourceDetails = []
        $scope.derivedColumns = []
        $scope.ruleDefined = []
        $scope.feedDef = {'filters': []}
        $scope.sources = []
        $scope.matchColumns = {}
        $scope.sumColumns = {}
        $scope.listSource = false
        $scope.loadTypes = ['CSV', 'Excel']
        $scope.dataTypes = ['np.int64', 'np.float64', 'np.datetime64', 'str']
        $scope.delimiter = ['|', ',']
        $scope.configurationSteps =
            ['defineSource', 'selectMatchColumns', 'resultsOfProcess'];
        $scope.active = {};



        // Limit items to be dropped in list1
        $scope.optionsList1 = {
            accept: function (dragEl) {
                if ($scope.list1.length >= 2) {
                    return false;
                } else {
                    return true;
                }
            }
        };

        $scope.refreshReconView = function () {
            $scope.distinctReconProcess = []
            $scope.recon = {}
            $scope.reconDetails = []
            $scope.sources = {}
            $scope.image = {}
            $scope.listSource = false
            ReconsService.get(undefined, {'skip': 0, 'limit': 100}, function (data) {
                $scope.reconDetails = data['data']
                angular.forEach(data['data'], function (k, v) {
                    if ($scope.distinctReconProcess.indexOf(k['reconProcess']) == -1) {
                        $scope.distinctReconProcess.push(k['reconProcess'])
                        $scope.image[k['reconProcess']] = false
                    }
                })
            })
            ReconProcessService.get(undefined, undefined, function (data) {
                angular.forEach(data['data'], function (v, k) {
                    if (v['imagePath'] != undefined) {
                        $scope.image[v['reconProcess']] = true
                    }
                })
            })
        }

        $scope.refreshReconView()


        $scope.getProcesRecons = function (index) {
            $scope.processRecons = []
            $scope.process = $scope.distinctReconProcess[index]
            $scope.expanded = true
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v['reconProcess'] == $scope.distinctReconProcess[index]) {
                    $scope.processRecons.push(v)
                }
            })
        }

        $scope.removeProcess = function (index) {
            $scope.expanded = false
            $scope.processRecons = []
            $scope.process = ''
        }


        $scope.uploadRecon = function (index) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService, ReconProcessService) {
                    $scope.title = $rootScope.resources['reconImgUpload']

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/image/upload"; //Url of webservice/api/server
                        var filePath = "/files/reconImg"
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.showMessage($rootScope.resources['image'], $rootScope.resources['imageUploadMsg'], 'success');
                            $uibModalInstance.dismiss('cancel');
                            var dataR = {}
                            dataR['imagePath'] = filePath + '/' + data['msg']['fileName']
                            dataR['reconProcess'] = $scope.distinctReconProcess[index]
                            ReconProcessService.saveProcessDetail('dummy', dataR, function (data) {
                                $scope.image[$scope.distinctReconProcess[index]] = true
                            })
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });

                    }

                    $scope.submit = function () {
                        $scope.redirect(recon)
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        $scope.initGrid = function () {
            $scope.gridOptions = {
                columnDefs: [],
                rowData: null,
                enableSorting: true,
                checkboxSelection: true,
                enableColResize: true,
                suppressMenuHide: true,
                enableFilter: true,
                rowSelection: 'multiple',
                defaultColDef: {
                    width: 100,
                    filter: 'agTextColumFilter'
                },
                onCellDoubleClicked: cellFocused,
            };

        }

        $scope.initProcess = function () {
            $scope.create = false;
            $scope.recon = {}
            $scope.sources = {}
            $scope.listSource = false
            $scope.addSource = false
            $scope.initGrid()
            $scope.present = 'defineSource';
            $scope.next = 'selectMatchColumns';

        }
        $scope.initGrid()
        $scope.createRecon = function (data) {
            $scope.create = true;
            $scope.present = 'defineSource';
            $scope.next = 'selectMatchColumns';
        };

        $scope.editRecon = function (recon) {
            $scope.initProcess()
            $scope.recon.reconName = recon.reconName
            $scope.recon.reconProcess = recon.reconProcess
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v.reconName == recon.reconName) {
                    $scope.sources = v.sources
                }
            })
            $scope.sources = recon.sources
            $scope.createRecon()
            $scope.listSource = true
        }

        function getRandomColor() {
            var letters = 'BCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * letters.length)];
            }
            return color;
        }

        $scope.marginLeft = 0

        $scope.getStyle = function (index) {

            if (index = 0) {
                $scope.marginLeft = $scope.marginLeft + 110
            }
            else {
                $scope.marginLeft = $scope.marginLeft + 600
            }
            return {
                'margin-left': $scope.marginLeft
            }
        }

        $scope.nextStep = function (present) {
            $scope.prev = present;
            $scope.present = $scope.next;
            $scope.next = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) + 1];
            $scope.active[present] = true;
            if ($scope.prev == 'defineSource') {
                // $scope.recon.sourceCount = $scope.sourceDetails.length;
                $scope.recon.sources = $scope.sources
                ReconsService.post($scope.recon, function (data) {
                    console.log(data);
                });
                var perColConditions = {
                    'perColConditions': {
                        'reconName': $scope.recon.reconName,
                        'reconProcess': $scope.recon.reconProcess
                    }
                }
                SourceReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                    $scope.sourceCols = {};
                    var _x = 0
                    var _y = 0
                    var newNodeDataModel = []
                    angular.forEach(data.data, function (v, k) {
                        if (_x == 0) {
                            _x = _x + 100
                        }
                        else {
                            _x = _x + 400
                            _y = 0
                        }
                        $scope.sourceCols[v.source] = []
                        angular.forEach(v.feedDetails, function (val, key) {
                            _y = _y + 50
                            var newModel = {
                                "outputConnectors": [
                                    {
                                        "name": "1"
                                    }
                                ],
                                "name": val.fileColumn,
                                "color": getRandomColor(),
                                "inputConnectors": [
                                    {
                                        "name": "X"
                                    }
                                ],
                                "className": v.source,
                                "width": 150,
                                "y": _y,
                                "x": _x,
                                "type": "val.fileColumn",
                                "id": getID()
                            }

                            newNodeDataModel.push(newModel)
                            $scope.sourceCols[v.source].push(val.fileColumn)
                            $scope.matchColumns[v.source] = [];
                            $scope.sumColumns[v.source] = [];
                        })
                        angular.forEach(v.derivedColumns, function (column, key) {
                            $scope.sourceCols[v.source].push(column);

                        })

                        $rootScope.flowelements = [{'flowName': 'flow1', 'active': true, 'Id': getID()}]
                        $rootScope.activeFlow = $rootScope.flowelements[0]
                        $rootScope[$rootScope.activeFlow['Id']] = new flowchart.ChartViewModel({});
                        angular.forEach(newNodeDataModel, function (v, k) {
                            $rootScope[$rootScope.activeFlow['Id']].addNode(v);

                        })

                        $scope.getChartVariable = function () {
                            return $rootScope[$rootScope.activeFlow['Id']]
                        }


                        $scope.filters[v.source] = v.filters;
                    });

                });
            }

            if ($scope.prev == 'selectMatchColumns') {
                var data = {}
                data.reconName = $scope.recon.reconName
                data.reconProcess = $scope.recon.reconProcess
                data.matchRules = $scope.ruleDefined
                $rootScope.openModalPopupOpen()
                MatchRuleReferenceService.saveMatchRule($scope.matchId, data, function (data) {
                    $scope.sourceData = data;
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.openModalPopupClose()
                    $rootScope.showMessage('Job Error', err.msg, 'error');
                });
            }
        };

        $scope.previousStep = function (present) {
            $scope.next = present;
            $scope.present = $scope.prev;
            $scope.active[present] = false;
            if ($scope.configurationSteps.indexOf($scope.prev) != 0) {
                $scope.prev = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) - 2];
            }
            if ($scope.present == 'selectMatchColumns') {
                $scope.resultGridOptions.columnDefs = []
                $scope.resultGridOptions.rowData = null;
                $scope.resultGridOptions.api.setRowData = $scope.gridOptions.rowData
                $scope.resultGridOptions.api.columnDefs = $scope.gridOptions.columnDefs;
                $scope.sourceData = {}
            }
        };

        $scope.skipRowFunction = function () {
            $scope.selectedRows = $scope.gridOptions.api.getSelectedNodes()
            $scope.skiprows = $scope.selectedRows[0].childIndex + 1
            if ($scope.selectedRows.length > 1) {
                $scope.skipfooter = $scope.gridOptions.api.getDisplayedRowCount() - $scope.selectedRows[1].childIndex

            }
            console.log($scope.skiprows)
            console.log($scope.skipfooter)
            var query = {}
            query = {
                'fileName': $scope.fileName,
                'skiprows': $scope.skiprows,
                'skipfooter': $scope.skipfooter,
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName
            }
            SourceReferenceService.skipRows('dummy', query, function (data) {
                if (data) {
                    $scope.updateDetails(data);
                }
                $rootScope.openModalPopupClose()
                $scope.fileUploaded = true;
                $rootScope.displayMessage('Rows Skipped Successfully', 'md', 'success');

            });
        }


        $scope.viewProperties = function (sourceName) {

            var query = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess,
                    'source': sourceName
                }
            };
            SourceReferenceService.get(undefined, {'query': query}, function (data) {
                    $scope.feedDef = data.data[0];
                    $scope.properties = true;
                    $scope.listSource = false;
                }, function (err) {
                    $rootScope.showMessage('Save', err.msg, 'error');
                }
            );
        }

        $scope.saveProperties = function (data) {
            if (!$scope.addSource) {
                $scope.listSource = true
            }
            else {
                $scope.listSource = false
            }
            $scope.properties = false

            SourceReferenceService.put(data._id, data, function (data) {
                $rootScope.showMessage('Save', $scope.resources.sourceSave, 'success');
            }, function (err) {
                $rootScope.showMessage('Save', err.msg, 'error');
            });
        };


        $scope.cancelProperties = function () {
            if (!$scope.addSource) {
                $scope.listSource = true
            }
            else {
                $scope.listSource = false
            }
            $scope.properties = false
        }

        $scope.addNewSource = function () {
            $scope.properties = false
            $scope.listSource = true;
            $scope.addSource = true;
            $scope.gridOptions.columnDefs = [];
            $scope.gridOptions.rowData = [];
            $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
            $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);

        }

        $scope.uploadSourceFile = function () {
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload";
                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var fileName = file.name
                        var uploadUrl = '/api/no_auth/source/upload';
                        $rootScope.openModalPopupOpen()
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $uibModalInstance.dismiss('cancel');
                            var query = {}
                            query = {
                                'fileName': file.name,
                                'skipRows': 0,
                                'reconName': $scope.recon.reconName,
                                'reconProcess': $scope.recon.reconProcess,
                                'source': $scope.sourceName
                            }
                            SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                                if (data) {
                                    $scope.updateDetails(data);
                                }
                                $rootScope.openModalPopupClose()
                                $scope.fileUploaded = true;
                                $rootScope.showMessage("Save", $scope.resources.fileUpload, 'success');

                            });
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        $scope.updateDetails = function (data) {
            $scope.columnDefs = [{
                headerName: '',
                'width': 30,
                checkboxSelection: true,
                suppressAggregation: true,
                suppressSorting: true,
                suppressMenu: true,
                pinned: true
            }]
            $scope.defineSource = true;
            $scope.sourceDetails = data.sourceDetails;
            $scope.expressions = data.filters
            $scope.derivedColumns = data.derivedColumns
            $scope.feedDetails = data.feedDetails
            angular.forEach(data.feedDetails, function (k, v) {
                if (k.dataType == 'str') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agTextColumnFilter',
                        'editable': true,
                        'filterParams': {'caseSensitive': true}
                    });
                }
                else if (k.dataType == 'np.int64' || k.dataType == 'np.float64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agNumberColumnFilter',
                        'editable': true
                    });
                }
                else if (k.dataType == 'np.datetime64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agDateColumnFilter',
                        'editable': true
                    });
                }
                else {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agSetColumnFilter',
                        'editable': true
                    });
                }
            })
            $scope.gridOptions.columnDefs = $scope.columnDefs;
            $scope.gridOptions.rowData = data.data;
            $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
            $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
        }


        function cellFocused(data) {
            $scope.value = data.value;
            $scope.column = data.colDef.field;
            // $scope.type = $scope.columnDefs[$scope.column];
        }

        $scope.$watch(window.getSelection(), function (oldValue, newValue) {
            $scope.selectedText = window.getSelection().toString()
        })

        function onCellClick() {
            console.log('cellClicked');
        }

        $scope.cellClick = 'cellClicked'


        function getText(data) {
            console.log(data);
            var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
            } else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }
            return text;
        }


        var stringExpressions = {
            'notEqual': "(df['$colName'].str.strip()!='$colValue')",
            'equals': "(df['$colName'].str.strip()=='$colValue')",
            'startWith': "(df['$colName'].str.strip().startswith('$colValue'))",
            'notStartWith': "(~df['$colName'].str.strip().str.startswith('$colValue'))",
            'endsWith': "(df['$colName'].str.strip().endswith('$colValue'))",
            'notEndsWith': "(~df['$colName'].str.strip().endswith('$colValue'))",
            'contains': "(df['$colName'].str.strip().str.contains('$colValue',case=False))",
            'notContains': "(~df['$colName'].strip().str.contains('$colValue'))",
            'isIn': "(df['$colName'].strip().isin('$colValue'))",
            'notIsIn': "(~df['$colName'].strip().isin('$colValue'))"
        }

        $scope.filters = [];

        var numbericExpression = {
            'condition': "df[df['$colName'] $operator $colValue]"
        };

        $scope.buildExpression = function (value) {
            var str = "df = df["

            angular.forEach(value, function (v, k) {
                if (Object.keys(value).indexOf('operator')) {
                    var exp = ''
                    lenValue = Object.keys(v).length - 1
                    for (var i = 1; i <= lenValue; i++) {
                        exp += buildStringCondition(v['condition' + i], k);
                        if (v['operator'] == 'AND' && i < lenValue) {
                            exp += '&'
                        }
                        if (v['operator'] == 'OR' && i < lenValue) {
                            exp += '|'
                        }
                    }
                    str += exp
                }
                else {
                    str += buildStringCondition(v, k);
                }
            })
            str += "]"
            return str;
            $scope.gridOptions.api.setFilterModel(null)
            $scope.gridOptions.api.onFilterChanged()
        }

        function buildStringCondition(value, column) {
            return stringExpressions[value.type].replace('$colName', column).replace('$colValue', value.filter == undefined ? '' : value.filter);
        }

        $scope.addDeriveColumn = function (data) {
            $scope.selectedText = window.getSelection().toString()
            var filter = []
            var filter = $scope.gridOptions.api.getFilterModel()
            if (filter != undefined) {
                angular.forEach(filter, function (v, k) {
                    $scope.filterModule = buildStringCondition(v, k)
                    $scope.filterColumn = k
                })
            }
            var query = {}
            angular.forEach($scope.expressions, function (v, k) {
                $scope.filters.push(v)
            })
            query = {
                'completeValue': $scope.value,
                'selectedColumn': $scope.column,
                'columnType': $scope.type,
                'selectedText': $scope.selectedText,
                'reconName': $scope.recon.reconName,
                'source': $scope.sourceName,
                'filters': $scope.filters,
                'derivedColumns': $scope.derivedColumns,
                'filterModule': $scope.filterModule,
                'filterColumn': $scope.filterColumn
            }
            // query['sourceDetails'] = $scope.sourceDetails
            SourceReferenceService.deriveColumns('dummy', query, function (data) {
                if (data) {
                    $scope.columnDefs = []
                    $scope.updateDetails(data)
                }
            })
        };

        document.addEventListener('mouseup', function () {
            console.log(window.getSelection().toString());
        });

        $rootScope.chart = {}

        $scope.sources = []
        $scope.saveSource = function () {
            if ($scope.sources.indexOf($scope.sourceName) == -1 && $scope.sourceName != '') {
                $scope.sources.push($scope.sourceName)
            }
            $scope.addSource = false
            $scope.sourceName = ''
            $scope.listSource = true
        }

// Matching Rule Define section starts
        $scope.addNewMatchRule = function (mode, index) {
            $scope.mode = mode
            $scope.matchRules = {}
            if ($scope.mode == 'Add') {
                if (index == 'selfMatchSource') {
                    $scope.selfMatch = true
                }
                else {
                    $scope.selfMatch = false
                    angular.forEach($scope.sources, function (k, v) {
                        $scope.matchRules[k] = {'matchColumns': [], 'sumColumns': [], 'debitCreditColum': []};
                    });

                }
            }
            if ($scope.mode == 'Edit') {
                $scope.ruleName = $scope.ruleDefined[index]['ruleName']
                $scope.matchRules = $scope.ruleDefined[index]['columns']
            }
            $uibModal.open({
                templateUrl: 'ruleDefine.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: 'md',
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                    $scope.createRule = function (sourceName) {
                        $scope.matchRules[sourceName] = {'matchColumns': [], 'sumColumns': []};
                    }
                    $scope.saveRule = function (ruleName) {
                        if ($scope.mode == 'Edit') {
                            $scope.ruleDefined[index]['ruleName'] = ruleName;
                            $scope.ruleDefined[index]['columns'] = $scope.matchRules
                        }
                        else {
                            $scope.ruleDefined.push({'ruleName': ruleName, 'columns': $scope.matchRules})

                        }
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }

        $scope.refreshReconJob = function () {
            var query = {}
            query['reconName'] = $scope.recon.reconName

            ReconDetailsService.getReportDetails('dummy',
                query, function (data) {
                    $scope.reportDetails = data.details;

                    angular.forEach($scope.reportDetails, function (v, k) {
                        $scope.amounts.Matched[v.Source] = ''
                        $scope.amounts.Exception[v.Source] = ''
                        $scope.amounts.Matched[v.Source] = v['Matched Amount'];
                        $scope.amounts.Exception[v.Source] = v['UnMatched Amount'];
                    });

                    $rootScope.openModalPopupClose();
                }, function (err) {
                    $rootScope.openModalPopupClose();
                    $scope.initGrid()
                    $scope.show = false
                    $rootScope.showMessage
                    ($rootScope.resources.message, err.msg, 'warning');
                });
        }

        $scope.downloadReport = function () {
            var query = {'reconName': $scope.recon.reconName}
            MatchRuleReferenceService.downloadReports('dummy', query, function (data) {
                var filePath = 'files/Outputs/'
                filePath = filePath + data
                $window.open(filePath)
            })
        }
        $scope.pos = {}


        $scope.deleteSource = function (val) {
            var index = $scope.sources.indexOf(val)
            $scope.sources.splice(index, 1)
        }


        function getID() {
            return (Math.floor(Math.random() * 1000000000)).toString()
        }


        $rootScope.saveFlow = function () {
            var data = {
                'flowName': $scope.recon.reconName,
                'Id': $rootScope.activeFlow['Id'],
                'nodes': $rootScope[$rootScope.activeFlow['Id']]['data']['nodes'],
                'connections': $rootScope[$rootScope.activeFlow['Id']]['data']['connections']
            }
            MatchRuleReferenceService.post(data, function (data) {
                console.log(data)
            })
        }


        $scope.resultGridOptions = {

            defaultColDef: {
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: false
            },

            columnDefs: [],
            enableSorting: true,
            showToolPanel: true,
            rowMultiSelectWithClick: true,
            enableFilter: true,
            rowSelection: 'multiple',
            checkboxSelection: true,
            rowGroupPanelShow: 'always',
            pagination: false,
            enableColResize: true
        };

        $scope.expandSelectedSource = function (source) {
            $scope.columnDefs = []
            $scope.rowData = $scope.sourceData[source]
            angular.forEach($scope.rowData[0], function (v, k) {
                if (k == 'MATCHING_STATUS') {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302,
                        'rowGroupIndex': 0
                    })
                }
                else {
                    $scope.columnDefs.push({
                        'headerName': k,
                        'field': k,
                        'width': 302
                    })
                }
            })
            $scope.resultGridOptions.api.setColumnDefs($scope.columnDefs)
            $scope.resultGridOptions.api.setRowData($scope.rowData)
        }


    }])
;