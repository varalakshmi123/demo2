'use strict';
var app = angular.module('nextGen');
app.controller('dashBoardController', ['$scope', '$route', '$location', '$timeout', '$rootScope', 'ReconDetailsService',
    function ($scope, $route, $location, $timeout, $rootScope, ReconDetailsService) {
        $scope.reconProcess = []
        $scope.details = false
        ReconDetailsService.get(undefined, undefined, function (data) {
            if (data.total > 0) {
                $scope.reconDetails = data.data
                angular.forEach(data.data, function (k, v) {
                    if ($scope.reconProcess.indexOf(k.reconProcess) == -1) {
                        $scope.reconProcess.push(k.reconProcess);
                    }
                });
            }
        }, function (err) {
            $rootScope.showMessage($rootScope.resources.dashboard,
                err.msg, 'warning');
        })

        $scope.setRecon = function (val) {
            $rootScope.activeRecon = val;
            $rootScope.path = '/recons'
            $scope.$apply(function () {
                $location.path('/recons');
            });
        }
        $scope.getDetails = function (key) {
            ReconDetailsService.getDashboardSummary
            ('dummy', undefined, function (data) {
                $scope.dashBoardDetails = data
                $scope.reconExecutionDetails = data.reconExecutionDetailsLog;
                $rootScope.reconExecutionDetailsLog =
                    data.reconExecutionDetailsLog;
            }, function (err) {
                $rootScope.showMessage('Data', err.msg, 'warning')
            });
        }

        $scope.getDetails();

    }]);

