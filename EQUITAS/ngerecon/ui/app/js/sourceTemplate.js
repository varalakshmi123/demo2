'use strict';
var app = angular.module("nextGen");
app.controller("sourceTemplateController", ["$scope", "$route", "$location", "$http", "$timeout", "$uibModal", "$rootScope", "$window", "SourceReferenceService", "reconMetaInfoService", "ReconsService",
    function ($scope, $route, $location, $http, $timeout, $uibModal, $rootScope, $window, SourceReferenceService, reconMetaInfoService, ReconsService) {
        $scope.view = true
        $scope.selected = {};
        $scope.templateData = []
        $scope.selectedReportColumns = {}
        $scope.editColumns = false
        $scope.filters = []
        $scope.wizards = 'structure'
        $scope.dataTypes = ['np.int64', 'np.float64', 'np.datetime64', 'str']
        $scope.summary = []
        $scope.sortType = 'name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.filterRows = '';     // set the default search/filter term


        $scope.addSources = function () {
            $scope.view = false
            $scope.feedDef = {"reconName": $rootScope.reconInfo['reconName'], "feedDetails": [], 'derivedColumns': []}
        }

        $http.get('/app/json/summaryColumn.json').success(function (data) {
            $scope.summaryCols = data['columnTypes']
            $scope.loadTypes = data['loadTypes']
            $scope.delimeters = data['delimiters']
            angular.forEach(data['columnTypes'], function (k, v) {
                $scope.selectedReportColumns[v['keyCol']] = []
            })
        })

        // Initializing gridOptions for grid in Filter Section
        $scope.initGrid = function () {
            $scope.gridOptions = {
                columnDefs: [],
                rowData: [],
                enableSorting: true,
                checkboxSelection: true,
                enableColResize: true,
                suppressMenuHide: true,
                enableFilter: true,
                rowSelection: 'multiple',
                defaultColDef: {
                    width: 100,
                    filter: 'agTextColumFilter'
                },
                onCellDoubleClicked: cellFocused,
            };
        }

        // Function called when cell selected in grid
        function cellFocused(data) {
            $scope.value = data.value;
            $scope.column = data.colDef.field;
            // $scope.type = $scope.columnDefs[$scope.column];
        }

        $scope.initGrid()

        $scope.cancelMarket = function () {
            $scope.view = true
            $scope.plate = false
            $location.path('/marketplace')
            $rootScope.breadCrumblist = []
        }

        //List of sectioin available for each Source
        $scope.wizardTemplates = [
            {"wizardIdx": 1, "title": "Source Structure", "template": "structure"},
            {"wizardIdx": 2, "title": "Filter Definition", "template": "filters"},
            {"wizardIdx": 3, "title": "Source FeedDetails", "template": "feedDef"},
            {"wizardIdx": 4, "title": "Column Mapping", "template": "columnMapping"}
        ]

        //Activate present section of source
        $scope.currentTemp = $scope.wizardTemplates[0]

        $scope.nextWizard = function (stepIndex) {
            $scope.wizards = stepIndex
            stepIndex = $scope.currentTemp['wizardIdx'] + (stepIndex || 1)

            for (var i = 0; i < $scope.wizardTemplates.length; i++) {
                if (stepIndex == $scope.wizardTemplates[i]['wizardIdx']) {
                    $scope.currentTemp = $scope.wizardTemplates[i]
                }
            }
        }

        $scope.updateRowData = function (row, index) {
            console.log(row)
            angular.forEach($scope.feedDef['feedDetails'], function (k, v) {
                if (row['fileColumn'] == k['fileColumn']) {
                    $scope.feedDef['feedDetails'][v]['mappedColumn'] = row['mappedColumn']
                }
            })
            $scope.selectedIdx = undefined
        }

        $scope.reset = function () {
            $scope.selectedIdx = undefined
        }

        $scope.deleteRow = function (index) {
            $scope.feedDef['feedDetails'].splice(index, 1)
        }

        // Initializing scope variable to default values
        $scope.initStructEdit = function (source) {
            $rootScope.breadCrumblist.push(source['source'] + '(' + (source['struct']) + ')')
            $scope.currentTemp = []
            $scope.currentTemp = $scope.wizardTemplates[0]
            $scope.plate = true
            $scope.view = false
            $scope.fileColumns = []
            $scope.feedDetails = []
            $scope.fileColumnsList = []
        }

        // Function used for getting derived columns from Filter expression
        $scope.updateDerivedCols = function (filters) {
            if (filters != undefined & filters != []) {
                $scope.filterlength = filters.length
                if (!angular.isDefined(filters)) {
                    $scope.feedDef.derivedColumns = []
                }

                else {
                    $scope.feedDef.derivedColumns = []
                    for (var i = 0; i < filters.length; i++) {
                        var tmp = filters[i]
                        if (tmp.includes('=')) {
                            tmp = tmp.split('=')[0]
                            var tmp = tmp.replace(/(^.*\[|\].*$)/g, '').replace(/df|'|:/g, '').split(',');
                            angular.forEach(tmp, function (col, k) {
                                if (col.trim() != '') {
                                    if ($scope.feedDef.derivedColumns.indexOf(col) == -1) {
                                        $scope.feedDef.derivedColumns.push(col)
                                    }
                                    if ($scope.fileColumns.indexOf(col) == -1) {
                                        $scope.fileColumns.push(col)
                                    }
                                }
                            })
                        }
                    }
                }

            }
        }

        // Uploading source Defination file
        $scope.uploadSourceFile = function (source) {
            $scope.sampleFile = true
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload";
                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var fileName = file.name
                        var uploadUrl = '/api/no_auth/source/upload';
                        $rootScope.openModalPopupOpen()
                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $scope.updateSourceDetails(source, file)
                            $uibModalInstance.dismiss('cancel');
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });

                        };
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.sampleFile = false
                    };
                }
            });
        }


        // confirmation while uploading sample file to upload source Feed Details
        $scope.updateSourceDetails = function (source, file) {
            $uibModal.open({
                templateUrl: 'app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.deleteMsg = "Do you want to update sourceDetails?"
                    $scope.deleteTitle = "Source Details"

                    $scope.submit = function () {
                        $scope.saveDetails = true
                        $uibModalInstance.dismiss('cancel');
                        var query = {}
                        query = {
                            'fileName': file.name,
                            'skipRows': 0,
                            'reconName': source.reconName,
                            'source': source.source,
                            'saveSourceDetails': $scope.saveDetails
                        }
                        SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                            $rootScope.openModalPopupClose()
                            if (Object.keys(data).indexOf('sheetNames') != -1) {
                                data['saveSourceDetails'] = $scope.saveDetails
                                $scope.selectSheets(source, data)
                            }
                            else {
                                $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $scope.fileUploaded = true;
                            $rootScope.showMessage("Save", $scope.resources.fileUpload, 'success');

                        });
                    }

                    $scope.cancel = function () {
                        $scope.saveDetails = false
                        var query = {}
                        query = {
                            'fileName': file.name,
                            'skipRows': 0,
                            'reconName': source.reconName,
                            'source': source.source,
                            'saveSourceDetails': $scope.saveDetails
                        }
                        SourceReferenceService.saveSourceDetails('dummy', query, function (data) {
                            $rootScope.openModalPopupClose()
                            if (Object.keys(data).indexOf('sheetNames') != -1) {
                                data['saveSourceDetails'] = $scope.saveDetails
                                $scope.selectSheets(source, data)
                            }
                            else {
                                $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $scope.fileUploaded = true;
                            $rootScope.showMessage("Save", $scope.resources.fileUpload, 'success');

                        });
                        $uibModalInstance.dismiss('cancel');
                    };


                }
            })
        }

        // Options For selecting excel sheet Name
        $scope.selectSheets = function (source, data) {
            $scope.sheetNames = data['sheetNames']
            $scope.doc = data
            $uibModal.open({
                templateUrl: 'sheetName.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = $rootScope.resources.selectSheet
                    $scope.submit = function () {
                        $scope.doc['sheetNames'] = $scope.sheetName
                        $scope.doc['checkFileProperty'] = true
                        $scope.doc['saveSourceDetails'] = data['saveSourceDetails']
                        if ($scope.sheetNames == true) {
                            $scope.sheetNames == 0
                        }
                        SourceReferenceService.saveSourceDetails('dummy', $scope.doc, function (data) {
                            if (data) {
                                $scope.editTemplate(source)
                                $scope.updateDetails(data)
                            }
                            $uibModalInstance.dismiss('cancel');
                        }, function (err) {
                            console.log(err.msg)
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.sampleFile = false

                    };
                }
            })
        }


        // Function to update source data in to Grid
        $scope.updateDetails = function (data) {
            $scope.sampleData = data
            $scope.columnDefs = [{
                headerName: '',
                'width': 30,
                checkboxSelection: true,
                suppressAggregation: true,
                suppressSorting: true,
                suppressMenu: true,
                pinned: true
            }]
            $scope.defineSource = true;
            $scope.sourceDetails = data.sourceDetails;
            // $scope.expressions = data.filters
            $scope.derivedColumns = data.derivedColumns
            $scope.feedDetails = data.feedDetails
            angular.forEach(data.feedDetails, function (k, v) {
                if (k.dataType == 'str') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agTextColumnFilter',
                        'editable': true,
                        'filterParams': {'caseSensitive': true}
                    });
                }
                else if (k.dataType == 'np.int64' || k.dataType == 'np.float64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agNumberColumnFilter',
                        'editable': true
                    });
                }
                else if (k.dataType == 'np.datetime64') {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agDateColumnFilter',
                        'editable': true
                    });
                }
                else {
                    $scope.columnDefs.push({
                        'field': k.fileColumn,
                        'filter': 'agSetColumnFilter',
                        'editable': true
                    });
                }
            })
            $scope.gridOptions.columnDefs = $scope.columnDefs;
            $scope.gridOptions.rowData = JSON.parse(data['data']);
            $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);
            $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
        }


        // Function to edit each source structure
        $scope.editTemplate = function (source) {
            $scope.updateGridDataToNull()
            $scope.recon = source
            $scope.sourceName = source.source
            var perColConditions = {
                'perColConditions': {
                    'reconName': source['reconName'],
                    'struct': source['struct'],
                    'source': source['source']
                }
            }

            SourceReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                if (data['total'] > 0) {
                    $scope.feedDef = data['data'][0]
                    $scope.initStructEdit(source)
                    $scope.feedSrc = $scope.feedDef['source']
                    $scope.feedStr = $scope.feedDef['struct']
                    $scope.updateDerivedCols($scope.feedDef['filters'])
                    $scope.feedDef['feedId'] = $rootScope.reconInfo['sources'][$scope.feedDef['source']][$scope.feedDef['struct']]['feedId']
                    if ($scope.feedDef['referenceSource'] == false) {
                        var temp = {}
                        if ($scope.feedDef['mappedColumns'].length > 0) {
                            angular.forEach($scope.feedDef['mappedColumns'], function (v, k) {
                                temp[v['keyCols']] = v
                            })
                        }

                        $scope.feedDef['mappedColumns'] = []
                        angular.forEach($rootScope.reconInfo['sources'][$scope.feedSrc][$scope.feedStr]['keyColumns'], function (v, k) {
                            if (temp[v['keyCols']] != undefined) {
                                $scope.feedDef['mappedColumns'].push(temp[v['keyCols']])
                            }
                            else {
                                $scope.feedDef['mappedColumns'].push(v)
                            }
                        })
                    }

                    if (angular.isDefined($scope.feedDef.feedDetails)) {
                        $scope.feedDetails = $scope.feedDef.feedDetails
                    }
                    angular.forEach($scope.feedDef['feedDetails'], function (k, v) {
                        if (k['required'] == true) {
                            $scope.fileColumns.push(k['fileColumn'])
                        }
                    })
                    if (Object.keys($scope.feedDef).indexOf('filters') == -1) {
                        $scope.feedDef.filters = []
                    }
                    $scope.reportMapCols = {}
                    if (angular.isDefined($scope.feedDef['summaryCols']))
                        angular.forEach($scope.feedDef['summaryCols'], function (k, v) {
                            $scope.summaryCols = {'keyCol': k, 'mappendColumn': v}
                        })
                }
                else {
                    $scope.initStructEdit(source)
                    $scope.feedDef = source
                    $scope.feedDef['mappedColumns'] = $rootScope.reconInfo['sources'][$scope.feedDef['source']][$scope.feedDef['struct']]['keyColumns']
                }
            }, function (err) {

            })
        }


        //Function load sample file in filter section
        $scope.loadFile = function () {
            $scope.closeData = false

            if ($scope.sampleData == undefined || $scope.sampleData == '') {
                var query = {
                    'reconName': $scope.recon.reconName,
                    'source': $scope.recon.source,
                    'struct': $scope.recon.struct,
                    'saveSourceDetails': false
                }
                SourceReferenceService.loadSampleFile('dummy', query, function (data) {
                    $scope.updateDetails(data)
                }, function (err) {
                    $rootScope.showMessage('File', err.msg, 'error')
                })
            }
            else {
                $scope.updateDetails($scope.sampleData)
            }
        }


        $scope.skipRowFunction = function () {
            $scope.selectedRows = $scope.gridOptions.api.getSelectedNodes()
            $scope.skiprows = $scope.selectedRows[0].childIndex + 1
            if ($scope.selectedRows.length > 1) {
                $scope.skipfooter = $scope.gridOptions.api.getDisplayedRowCount() - $scope.selectedRows[1].childIndex

            }

            console.log($scope.skiprows)
            console.log($scope.skipfooter)
            var query = {}
            query = {
                'fileName': $scope.fileName,
                'skiprows': $scope.skiprows,
                'skipfooter': $scope.skipfooter,
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName
            }
            SourceReferenceService.skipRows('dummy', query, function (data) {
                if (data) {
                    $scope.updateDetails(data);
                }
                $rootScope.openModalPopupClose()
                $scope.fileUploaded = true;
                $rootScope.displayMessage('Rows Skipped Successfully', 'md', 'success');

            });
        }


        $scope.sortableOptions = {
            stop: function (e, ui) {
                for (var i = 0; i < $rootScope.reconDetails.length; i++) {
                    $rootScope.reconDetails[i]['position'] = i + 1
                }
                // for (var index in $rootScope.reconDetails) {
                //     $rootScope.reconDetails[index].position = $rootScope.reconDetails[index].position
                // }
            },
            axis: 'y'
        };

        //Setback Grid to Null values
        $scope.updateGridDataToNull = function () {
            $scope.sampleData = null
            $scope.gridOptions.api.setRowData([])
            $scope.gridOptions.api.setColumnDefs([])
        }


        // Edit Filters for small Changes
        $scope.editFilter = function (index) {
            $scope.editfilter = true
        }


        $scope.saveFilter = function (index, filter) {
            $scope.feedDef.filters[index] = filter
        }
        // Updating scope for column Names
        $scope.updateScope = function (data) {
            $scope.fileColumns = []
            $scope.feedDef['feedDetails'] = data
            $scope.feedDetails = data
            angular.forEach($scope.feedDef['feedDetails'], function (k, v) {
                if (k['required'] == true) {
                    $scope.fileColumns.push(k['fileColumn'])
                }
            })
            $scope.updateDerivedCols($scope.feedDef.filters)
        }


        // Updating source columns template with dataType
        $scope.uploadSrcTemplate = function () {
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'upload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"
                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/dummy/upload"; //Url of webservice/api/server

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            $scope.plate = true
                            // load data from save file
                            if (angular.isDefined(file)) {
                                SourceReferenceService.loadSourceRef('dummy', {"fileName": file.name}, function (data) {
                                    $scope.updateScope(data)
                                }, function (err) {
                                    console.log(err.msg)
                                })
                            }

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        //Meta Info like disabling carry forward and Incremental Loader selection
        $scope.provideMetaInfo = function () {
            $uibModal.open({
                templateUrl: '/app/components/notifications/checkVariables.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($uibModalInstance, $scope, $rootScope, reconMetaInfoService) {
                    var perColConditions = {'perColConditions': {'reconName': $scope.reconInfo['reconName']}}
                    reconMetaInfoService.get(undefined, {'query': perColConditions}, function (data) {
                        if (data['total'] != 0) {
                            $scope.variable = {}
                            $scope.id = data['data'][0]['_id']
                            angular.forEach($scope.reconInfo['sources'], function (k, v) {
                                if (!(v in $scope.variable)) {
                                    $scope.variable[v] = data['data'][0][v]
                                }
                            })
                        }
                        else {
                            $scope.variable = {}
                            angular.forEach($scope.reconInfo['sources'], function (k, v) {
                                if (!(v in $scope.variable)) {
                                    $scope.variable[v] = {
                                        'incrementLoader': false,
                                        'disableCarryForward': false
                                    }
                                }
                            })
                        }

                    }, function (err) {
                        console.log(err)
                    })
                    $scope.submit = function (data) {
                        if ($scope.id != undefined) {
                            reconMetaInfoService.put($scope.id, data, function (data) {
                                console.log(data)
                            })
                            $uibModalInstance.dismiss('cancel');
                        }
                        else {
                            data['reconName'] = $scope.reconInfo['reconName']
                            reconMetaInfoService.post(data, function (data) {
                                console.log(data)
                            })
                            $uibModalInstance.dismiss('cancel');
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                }
            })
        }

        //Function to save References details
        $scope.saveSrcReference = function (mappedDetails) {
            $scope.feedDef['mappedColumns'] = mappedDetails
            angular.forEach($scope.summaryCols, function (k, v) {
                if (k['keyCol'] in $scope.selectedReportColumns) {
                    $scope.summaryCols[v]['mappedColumn'] = $scope.selectedReportColumns[k['keyCol']]
                }
            })

            $scope.feedDef['summaryColMap'] = {}
            angular.forEach($scope.summaryCols, function (k, v) {
                $scope.feedDef['summaryColMap'][k['keyCol']] = k['mappedColumn']
            })

            if ($scope.feedDef["_id"] != undefined) {
                SourceReferenceService.put($scope.feedDef['_id'], $scope.feedDef, function (data) {
                    $scope.view = true
                    $rootScope.displayMessage('Source reference Updated successfully', 'md', 'sucess');
                }, function (err) {
                    $rootScope.displayMessage('Error while Updating source reference.', 'md', 'warning');
                })
            }
            else {
                SourceReferenceService.post($scope.feedDef, function (data) {
                    $scope.view = true
                    $rootScope.showMessage('Source Refernce', 'Source reference added successfully', 'success');
                }, function (err) {
                    $rootScope.showMessage('Source Refernce', 'Error while Saving source reference.', 'warning');
                })
            }
            $scope.changeView($scope.feedDef['reconName'])
        }

        // Download source Feed Details of columns Template
        $scope.downloadSource = function (feed) {
            var perColConditions = {
                'source': feed['source'],
                'struct': feed['struct'],
                'feedDetails': $scope.feedDetails
            }
            var filePath = window.location.origin + '/files/uploads'
            SourceReferenceService.downloadSource('file', {'query': perColConditions}, function (data) {
                filePath = filePath + '/' + data
                $window.open(filePath);
            })
        }

        // Function to change BreadCrumb list
        $scope.changeView = function (val) {
            $scope.temp = []
            var index = $rootScope.breadCrumblist.indexOf(val)
            var length = $rootScope.breadCrumblist.length
            for (var i = 0; i <= index; i++) {
                $scope.temp.push($rootScope.breadCrumblist[i])
            }
            $rootScope.breadCrumblist = $scope.temp
            if ($rootScope.breadCrumblist.length == 1) {
                $location.path('/marketplace')
            }
            else {
                $scope.view = true
            }
        }

        //Switch to market place Tab
        $rootScope.$watch($rootScope.reconInfo, function (oldVal, newVal) {
            if (!angular.isDefined($rootScope.reconInfo)) {
                $location.path('/marketplace')
            }
        })

        //Save Recon
        $scope.saveRecon = function (details) {
            SourceReferenceService.storeOrderOfSource('dummy', details, function (data) {
                console.log(data)
            })
            $rootScope.breadCrumblist = []
            $location.path('/marketplace')
            $rootScope.breadCrumblist.push($location.path().split('/')[1])
            $rootScope.displayMessage('Recon Save Successfully.', 'md', 'success');
        }

        //Upload filters for each source
        $scope.uploadFilters = function () {
            if (!angular.isDefined($scope.feedDef.filters)) {
                $scope.feedDef['filters'] = []
            }
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/filter/upload"; //Url of webservice/api/server

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            $scope.plate = true
                            if (angular.isDefined(file)) {
                                var query = {
                                    'reconName': $scope.recon.reconName,
                                    'source': $scope.recon.source,
                                    'struct': $scope.recon.struct,
                                    'filter': true,
                                    'fileName': file.name
                                }
                                SourceReferenceService.loadSourceRef('dummy', query, function (data) {
                                    angular.forEach(data, function (k, v) {
                                        if ($scope.feedDef.filters.indexOf(k) == -1) {
                                            $scope.feedDef.filters.push(k)
                                        }
                                    })
                                    // $scope.feedDef.filters = data
                                    $scope.updateDerivedCols(data)
                                }, function (err) {
                                    console.log(err.msg)
                                })
                            }

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        //Delete Column in source Feed Details
        $scope.deleteColumns = function (index) {
            $scope.feedDetails.splice(index, 1)
        }

        //Delete Filters in filter section
        $scope.deleteFilter = function (index) {
            $scope.feedDef.filters.splice(index, 1)
            $scope.updateDerivedCols($scope.feedDef.filters)
        }

        //Function change the column Name
        $scope.saveColumn = function (index, val) {
            $scope.feedDetails[val['position'] - 1] = val
        }

        //Add the Reference source for the recon
        $scope.provideReference = function () {
            $uibModal.open({
                templateUrl: "refrenceSource.html",
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($uibModalInstance, $rootScope, SourceReferenceService) {
                    $scope.submit = function (data) {
                        var dataR = {}
                        dataR['source'] = data['sourceName']
                        dataR['struct'] = data['structName']
                        dataR['feedDetails'] = []
                        dataR['reconName'] = $rootScope.reconInfo['reconName']
                        dataR['referenceSource'] = true
                        dataR['static'] = data['static']

                        SourceReferenceService.post(dataR, function (data) {
                            $rootScope.showMessage($rootScope.resources.referenceSource, $rootScope.resources.addedSuccessfully, 'success')
                            $rootScope.reconDetails.push({
                                'source': data['source'],
                                "struct": data['struct'],
                                "reconName": $rootScope.reconInfo['reconName']
                            })
                        }, function (err) {
                            $rootScope.showMessage($rootScope.resources.error, $rootScope.resources.defaultMsg, 'error')
                        })
                        $uibModalInstance.dismiss('cancel')
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }


        var stringExpressions = {
            'notEqual': "(df['$colName'].str.strip()!='$colValue')",
            'equals': "(df['$colName'].str.strip()=='$colValue')",
            'startWith': "(df['$colName'].str.strip().startswith('$colValue'))",
            'notStartWith': "(~df['$colName'].str.strip().str.startswith('$colValue'))",
            'endsWith': "(df['$colName'].str.strip().endswith('$colValue'))",
            'notEndsWith': "(~df['$colName'].str.strip().endswith('$colValue'))",
            'contains': "(df['$colName'].str.strip().str.contains('$colValue',case=False))",
            'notContains': "(~df['$colName'].strip().str.contains('$colValue'))",
            'isIn': "(df['$colName'].strip().isin('$colValue'))",
            'notIsIn': "(~df['$colName'].strip().isin('$colValue'))"
        }

        $scope.filters = [];

        var numbericExpression = {
            'condition': "df[df['$colName'] $operator $colValue]"
        };

        $scope.buildExpression = function (value) {
            var str = "df = df["

            angular.forEach(value, function (v, k) {
                if (Object.keys(value).indexOf('operator') != -1) {
                    var exp = ''
                    var lenValue = Object.keys(v).length - 1
                    for (var i = 1; i <= lenValue; i++) {
                        exp += buildStringCondition(v['condition' + i], k);
                        if (v['operator'] == 'AND' && i < lenValue) {
                            exp += '&'
                        }
                        if (v['operator'] == 'OR' && i < lenValue) {
                            exp += '|'
                        }
                    }
                    str += exp
                }
                else {
                    str += buildStringCondition(v, k);
                }
            })
            str += "]"
            return str;
            $scope.gridOptions.api.setFilterModel(null)
            $scope.gridOptions.api.onFilterChanged()
        }

        function buildStringCondition(value, column) {
            return stringExpressions[value.type].replace('$colName', column).replace('$colValue', value.filter == undefined ? '' : value.filter);
        }


        $scope.filterModule = []
        // Derive Column from one column
        $scope.addDeriveColumn = function (data) {
            $scope.selectedText = window.getSelection().toString()
            var filter = []
            if ($scope.feedDef['filters'].length == 0) {
                $scope.feedDef.filters = []
            }
            var filter = $scope.gridOptions.api.getFilterModel()
            if (filter != undefined) {
                angular.forEach(filter, function (v, k) {
                    var a = buildStringCondition(v, k)
                    $scope.filterModule.push(a)
                    $scope.filterColumn = k
                })
            }
            var query = {}
            // angular.forEach($scope.expressions, function (v, k) {
            //     $scope.filters.push(v)
            // })
            query = {
                'completeValue': $scope.value,
                'selectedColumn': $scope.column,
                'columnType': $scope.type,
                'selectedText': $scope.selectedText,
                'reconName': $scope.recon.reconName,
                'source': $scope.sourceName,
                'filters': $scope.feedDef.filters,
                'derivedColumns': $scope.derivedColumns,
                'filterModule': $scope.filterModule,
                'filterColumn': $scope.filterColumn
            }
            // query['sourceDetails'] = $scope.sourceDetails
            $rootScope.openModalPopupOpen()
            SourceReferenceService.deriveColumns('dummy', query, function (data) {
                if (data) {
                    $scope.columnDefs = []
                    $scope.feedDef['filters'] = data['filters']
                    $scope.updateDetails(data)
                    $scope.updateDerivedCols(data['filters'])
                    $rootScope.showMessage('Success', 'Column derived', 'success')
                }
                $rootScope.openModalPopupClose()
            }, function (err) {
                $rootScope.showMessage('Error', err.msg, 'error')
            })
        };

        // Add Filter for the data
        $scope.addFilter = function () {
            var filterApplied = $scope.gridOptions.api.getFilterModel();

            $scope.filters.push($scope.buildExpression(filterApplied));
            var query = {
                'reconName': $scope.recon.reconName,
                'reconProcess': $scope.recon.reconProcess,
                'source': $scope.sourceName,
                'filters': $scope.filters
            }
            SourceReferenceService.saveFilters('dummy', query, function (data) {
                console.log(data)
            })
        };

        // To view Grid in Filter section
        $scope.closeFileData = function () {
            $scope.closeData = true
        }

        document.addEventListener('mouseup', function () {
            console.log(window.getSelection().toString());
        });


        //Set new Value in mentioned columns
        $scope.setValueToNewColumn = function () {
            $uibModal.open({
                templateUrl: "setValue.html",
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($uibModalInstance, $rootScope, $scope, SourceReferenceService) {
                    $scope.setValue = function () {
                        var filterApplied = $scope.gridOptions.api.getFilterModel();

                        $scope.filters.push($scope.buildExpression(filterApplied));
                        var query = {
                            'reconName': $scope.recon.reconName,
                            'reconProcess': $scope.recon.reconProcess,
                            'source': $scope.sourceName,
                            'filters': $scope.filters,
                            'newColumn': $scope.newColumn,
                            'valueSet': $scope.valueSet
                        }
                        SourceReferenceService.setValueToNewColumn('dummy', query, function (data) {
                            if (data) {
                                $rootScope.showMessage('Set Value', 'Value update done', 'success')
                            }
                            console.log(data)
                        }, function (err) {
                            $rootScope.showMessage('Set Value', err.msg, 'error')
                        })
                        $scope.$uibModalInstance.dismiss('cancel')
                    }

                    $scope.cancel = function () {
                        console.log($scope.newColumn)
                        console.log($scope.valueSet)
                        $scope.$uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }
    }])
;
