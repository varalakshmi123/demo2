var app = angular.module('nextGen');
app.controller('reportsController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'ReconDetailsService',
    '$http', 'ColumnDataTypesSerice', 'ReconExecDetailsLogService',
    'ForceMatchService', 'ReconsService','SourceReferenceService','CustomReportService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, ReconDetailsService, $http, ColumnDataTypesSerice,
              ReconExecDetailsLogService, ForceMatchService, ReconsService, SourceReferenceService,CustomReportService) {

        //List of sectioin available for each Source
        $scope.wizardReports= [
            {"wizardIdx": 1, "title": "Daily Settlement Summary Report", "template": "dailySettlementReport"}
        ]

        $scope.values=["IMPS DSR","IMPS TTUM","UPI TTUM","UPI DSR"];


        $scope.currentTemp = $scope.wizardReports[0]
        $scope.wizards = "dailySettlementReport"
        $scope.nextWizard = function (stepIndex) {
            $scope.wizards = stepIndex
            stepIndex = $scope.currentTemp['wizardIdx'] + (stepIndex || 1)

            for (var i = 0; i < $scope.wizardReports.length; i++) {
                if (stepIndex == $scope.wizardTemplates[i]['wizardIdx']) {
                    $scope.currentTemp = $scope.wizardTemplates[i]
                }
            }
        }

        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };


        $scope.generateDailySettlement = function () {
            var query = {}
            var statementDate = $scope.toEpoch($scope.statementDate)
            $scope.settelmentData = []
            query['statementDate'] = $filter('date')(statementDate * 1000, 'yyyy-MM-dd')
                query['reportType'] = 'daily_settlement_report'
            CustomReportService.getDailySettlementRpt('dummy', query, function (data) {

                if (!angular.isDefined(data) || data.length == 0 || angular.equals({}, data)) {
                    $rootScope.showMessage('No data found.','','warning')
                } else {
                    $scope.settelmentData = data
                }
            }, function (err) {
                $rootScope.showMessage(err.msg,'','error')
            })
        };
        $scope.getReport=function(selectedReport){
            $scope.datas={};
            $scope.datas['report']=selectedReport;

            var statementDate = $scope.toEpoch($scope.statementDate);
            $scope.datas['statementDate'] = $filter('date')(statementDate * 1000, 'yyyy-MM-dd')
            console.log($scope.datas);

            CustomReportService.getReport('dummy', $scope.datas, function (data){

             if ($scope.datas['report'] == 'IMPS DSR' || $scope.datas['report'] == 'UPI DSR') {
                    if (!angular.isDefined(data) || data.length == 0 || angular.equals({}, data)) {
                        $rootScope.showMessage('No data found.', '', 'warning')
                    }
                    else {
                            // $scope.settelmentData = data
                            $scope.settelmentData = JSON.parse(data)
                    }
             }
             else {
                        var filePath = 'app/files/' + data
                        $window.open(filePath)
                   }


            },function (err) {
                $rootScope.showMessage(err.msg,'','error')
            })

            if (selectedReport=='IMPS DSR')  {

               $rootScope.tableId='dailySettlementSummary';
            }
            else if(selectedReport=='UPI DSR') {

                $rootScope.tableId='upisettlementReport';

            }
		

	    };
//            CustomReportService.getTTUMReport('dummy', $scope.datas, function (data) {
//                var filePath = 'app/files/' + data['reqPath']
//                $window.open(filePath)
//
//            }, function (err) {
//                $rootScope.showMessage(err.msg,'','error')
//            })
//        };

        //utility to export table data to excel
        $scope.exportToExcel = function (tableId, report_name, stmtDate) {
            var day = stmtDate.getDate();
            var month = stmtDate.getMonth() + 1;
            var year = stmtDate.getFullYear();
            var postfix = day + "_" + month + "_" + year;

            var tab_text = "<table border='3px'>";
            var textRange;
            var j = 0;
            tab = document.getElementById(tableId); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // remove input params

            var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'>";
            excelFile += "<head>";
            excelFile += "<!--[if gte mso 9]>";
            excelFile += "<xml>";
            excelFile += "<x:ExcelWorkbook>";
            excelFile += "<x:ExcelWorksheets>";
            excelFile += "<x:ExcelWorksheet>";
            excelFile += "<x:Name>";
            excelFile += "{worksheet}";
            excelFile += "</x:Name>";
            excelFile += "<x:WorksheetOptions>";
            excelFile += "<x:DisplayGridlines/>";
            excelFile += "</x:WorksheetOptions>";
            excelFile += "</x:ExcelWorksheet>";
            excelFile += "</x:ExcelWorksheets>";
            excelFile += "</x:ExcelWorkbook>";
            excelFile += "</xml>";
            excelFile += "<![endif]-->";
            excelFile += "</head>";
            excelFile += "<body>";
            excelFile += tab_text;
            excelFile += "</body>";
            excelFile += "</html>";

            //getting data from our div that contains the HTML table
            var data_type = 'data:application/vnd.ms-excel';
            a = document.createElement("a");
            a.download = report_name + '_' + postfix + '.xls';

            a.href = data_type + ', ' + encodeURIComponent(excelFile);
            document.body.appendChild(a);
            $timeout(function () {
                a.click();
                document.body.removeChild(a);
            })
        }
    }])
