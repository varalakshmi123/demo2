from numpy.distutils.command.config import config
from pandas.computation import expressions

import flask
import hashframe
import pandas
import numpy as np
import hashlib
from pandas.api.types import *
import AesEncrypt
import shutil
import json
import zipfile
from bson.json_util import dumps
from bson import json_util
from copy import copy
import traceback

logr = hashframe.Logger.getInstance("application").getLogger()
import datetime
import subprocess
import os
import csv
import datetime
import re
from flask import request
from shutil import copyfile
import smtplib
from bson import ObjectId
import rstr
from email.mime.multipart import MIMEMultipart
# from email.MIMEText import MIMEText
import uuid
import time
import requests
import itertools


# Login
class Login(hashframe.noAuthHandler):
    def create(self, doc):
        status, data = Users().get({'userName': doc['userName']})
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, doc['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        password = hashlib.md5(decrypt).hexdigest()
        resp = "User not found"

        # check is password is correct
        if status:
            # Checking Approved status
            if data['ApprovalStatus'] == "Not Approved":
                return False, 'Account to be approved'

            # Passsword check
            if password == data['password']:
                if data["role"] not in ["admin", "Configurer"]:
                    if (datetime.datetime.now() - datetime.datetime.fromtimestamp(
                            data['lastPwdUpdt'] / 1000)).days > 45:
                        return False, 'Password Expired'

                    if data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                flask.session['sessionData'] = {}
                flask.session['sessionData']['userName'] = data['userName']
                flask.session['sessionData']['email'] = data['email']
                flask.session['sessionData']['partnerId'] = data['partnerId']
                flask.session["sessionData"]["userId"] = data['_id']
                # flask.session['sessionData']['preveliges'] = data['preveliges']
                data['accountLocked'] = False
                data['loginCount'] = 0
                Users().modify(data['_id'], data)
                if data.get('role'):
                    flask.session['sessionData']['role'] = data['role']
            else:
                # if Password is Wrong
                if data['role'] != 'admin':
                    if 'accountLocked' in data and data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                    if 'loginCount' in data and data['loginCount'] != hashframe.config.loginCount:
                        data['loginCount'] = data['loginCount'] + 1
                        data['accountLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'Password is Incorrect You have ' + str(
                            hashframe.config.loginCount - data['loginCount']) + ' attempts left'

                    elif 'loginCount' in data and data['loginCount'] == hashframe.config.loginCount:
                        data['accountLocked'] = True
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'accountLocked Please Contact Admin'

                    else:
                        data['loginCount'] = 1
                        data['accoungLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                    return False, 'Password Incorrect'
                else:
                    return False, 'Password Incorrect'

            # get all the assigned recons to session
            _, assigned_recons = self.get_assigned_recons(data['userName'])
            flask.session['sessionData'].update(assigned_recons)

            return True, flask.session['sessionData']
        return False, resp

    def getAll(self, query={}):
        if 'sessionData' in flask.session:
            return True, flask.session['sessionData']
        return False, "Logged out."

    def get_assigned_recons(self, user_name):
        users = []
        users.append(user_name)
        data = list(ReconAssignment().find({'users': {"$in": users}}))
        # status,data = ReconAssignment().getAll({'condition': {'userName': user_name}})
        assigned_recons = []
        recon_process = {}
        actions = {}
        if data:
            for r in data:
                assigned_recons.extend(r.get('reconNames', []))
                actions = {recon: r['groupRole'] for recon in assigned_recons}
                recon_details = list(Recons().find({"reconName": {"$in": assigned_recons}}))
                # status,recon_details = list(Recons().getAll({'condition': {"reconName": {"$in": assigned_recons}}}))
            for r in recon_details:
                if recon_process.get(r['reconProcess']) is None:
                    recon_process[r['reconProcess']] = []
                recon_process[r['reconProcess']].append({'name': r['reconName'], 'actions': actions[r['reconName']]})
                # recon_process[r['reconProcess']].append(r['reconName'])
        else:
            pass
        return True, {"reconProcess": recon_process, 'assignedRecons': assigned_recons}


#  signup User/Creating User


class ReconLicense(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconLicense, self).__init__("reconlicense", hideFields)

    def getReconLicense(self, reconName):
        doc = ReconLicense().find_one({'reconName': reconName})
        return True, doc


class NoAuth(hashframe.noAuthHandler):
    def createUser(self, id, query):
        if not re.match(r'[\w.?\d]*@suryodaybank.com', query['email']):
            return False, 'Mail Id is Not Valid'

        if Users().find_one({'userName': query['userName']}):
            return False, 'UserName Already Exist'

        if Users().find_one({'email': query['email']}):
            return False, "Mail Id Is Already Exist"

        query['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        query['password'] = hashlib.md5(decrypt).hexdigest()
        query['ApprovalStatus'] = 'Not Approved'
        query['loginCount'] = 0
        query['accountLocked'] = False
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        Users().insert_one(query)
        return True, 'Success'

    def upload(self, id):
        mftPath = os.path.join(hashframe.config.uploadDir.get(id, hashframe.config.uploadDir['file']))
        fname = flask.request.files['file'].filename

        if os.path.exists(mftPath + '/' + fname):
            os.remove(mftPath + '/' + fname)

        if not os.path.exists(mftPath):
            os.makedirs(mftPath)
        flask.request.files['file'].save(os.path.join(mftPath, fname))
        status = {'status': 'File uploaded.', 'fileName': fname}
        return True, status

    def sendPasswordToMail(self, id, query):
        userDetails = Users().find_one({'userName': query['userName']})
        if userDetails:
            if userDetails['email'] == query['associatedMail']:
                password = rstr.xeger(r'[A-Z][a-z]{2}[@#$][0-9]{4}')
                email = userDetails['email']

                try:
                    smtp = smtplib.SMTP(hashframe.config.smtpServer, hashframe.config.smtpPort)
                    sender = hashframe.config.smtpMail

                    smtp.ehlo()
                    smtp.starttls()

                    smtp.login(hashframe.config.smtpMail, hashframe.config.smtpPassword)

                    recipients = userDetails['email']

                    msg = MIMEMultipart()
                    msg["From"] = sender
                    msg['To'] = recipients
                    msg["Subject"] = 'PASSWORD CHANGE'

                    body = "UserName : " + userDetails[
                        'userName'] + "Kindly Use the following PASSWORD to login" + " : " + str(password)

                    msg.attach(MIMEText(body, 'Plain'))

                    smtp.sendmail(sender, recipients, msg.as_string())
                    smtp.close()

                    logr.info('Password Change Email sent')
                except Exception as e:
                    logr.info(e)
                    logr.info('Error while sending Email')
                    return False, 'Error while sending Mail'
                del userDetails['password']
                userDetails['password'] = hashlib.md5(str(password)).hexdigest()
                Users().modify(userDetails['_id'], userDetails)
                return True, 'Password Sent To your Mail Id'
            else:
                return False, 'Please Enter Associated mail Id'
        else:
            return False, 'User Not Found'

    def updateExpirePassword(self, id, query):
        status, doc = Users().get({'userName': query['userName']})

        if 'newPassword' in query and 'oldPassword' in query:
            decryptnewPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['newPassword'])
            decryptnewPassword = decryptnewPassword.replace('/<=%+,?->/', '')

            decryptoldPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['oldPassword'])
            decryptoldPassword = decryptoldPassword.replace('/<=%+,?->/', '')

            newPassowrd = hashlib.md5(decryptnewPassword).hexdigest()
            oldPassword = hashlib.md5(decryptoldPassword).hexdigest()

            if (doc['password'] != oldPassword):
                return False, 'Old Password is incorrect'
            doc['password'] = newPassowrd
            doc['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000

        Users().modify({'userName': query['userName']}, doc)
        return True, 'User Details Updated SuccessFully'


#  Logout
class Logout(hashframe.noAuthHandler):
    def create(self, doc):
        flask.session.clear()
        flask.abort(401)

    def getAll(self, query={}):
        flask.session.clear()
        flask.abort(401)


class ReconExecDetailsLog(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconExecDetailsLog, self).__init__("recon_execution_details_log", hideFields)

    def getLatestExeDetails(self, reconId):
        doc = ReconExecDetailsLog().find_one({'reconName': reconId, 'jobStatus': 'Success'},
                                             sort=[('reconExecutionId', -1)])
        return True, doc

    def getMaxReconExecutionDoc(self, recon_names=None):
        from bson.son import SON
        pipeline = [{'$match': {'reconName': {'$in': recon_names or []}}},
                    {'$group': {'_id': '$reconName', 'executionId': {'$max': '$reconExecutionId'}}},
                    {"$sort": SON([("_id", -1)])}]

        agg_results = ReconExecDetailsLog().aggregate(pipeline=pipeline)
        filters = []
        if agg_results is not None:
            filters = [x['executionId'] for x in agg_results]

        return ReconExecDetailsLog().find({'reconExecutionId': {'$in': filters}})

    def rollbackRecon(self, id, query):
        date = datetime.datetime.strptime(query['statementDate'], '%d-%m-%Y')

        doc = ReconExecDetailsLog().find(
            {'reconName': query['reconName'], 'jobStatus': 'Success', 'statementDate': {'$gte': date}})

        if doc.count() == 0:
            return False, "No Previous data found."

        li = [d['statementDate'].strftime('%d%m%Y') for d in doc]
        mft_path = hashframe.config.mftPath + '/%s/' % query['reconName']
        for i in li:
            path = mft_path + i
            shutil.rmtree(path, ignore_errors=True)

        logr.info("Rollback Records for recon %s " % query['reconName'])
        ReconExecDetailsLog().remove(
            {'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        ReconSummary().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        CustomReport().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        MasterDumps().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        return True, 'Rollback successful.'

    def get_latest_execution_details(self, id):
        assigned_recons = flask.session['sessionData'].get('assignedRecons', [])
        _, recons = Recons().getAll({"condition": {'reconName': {'$in': assigned_recons}}}, limit=100)

        if recons['total'] == 0:
            return False, 'No Recon Allocated.'

        recon_exec = list(self.getMaxReconExecutionDoc(assigned_recons))
        if recon_exec:
            for _exec in recon_exec:
                _exec['statementDate'] = _exec['statementDate'].strftime('%d-%m-%Y')

            recon_exec = {x['reconName']: x for x in recon_exec}
            for x in recons['data']:
                x.update(recon_exec.get(x['reconName'], {}))

        return True, {'data': recons['data'], 'total': len(recons['data'])}


class MasterDumps(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(MasterDumps, self).__init__("master_dumps", hideFields)


#  store Column DataTypes of Each Sources of all Recons
class ColumnDataTypes(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ColumnDataTypes, self).__init__("columnDtypes", hideFields)

    def getColumnDataTypes(self, reconId, source):
        doc = ColumnDataTypes().find_one({'reconName': reconId, 'source': source})
        return doc


# Store Audit logs
class ReconAudit(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAudit, self).__init__("reconaudit", hideFields)


#  store users details
class Users(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(Users, self).__init__("users", hideFields)

    def create(self, doc):
        (status, userDoc) = super(Users, self).create(doc)
        return status, userDoc

    def deleteUser(self, id, query):
        status, data = Users().delete(id)
        status, data = ReconAssignment().delete({"userName": query['userName']})
        if status:
            return True, 'DELETED'
        else:
            return False, 'error'


#  store Recons Details from settings tab
class CustomReport(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(CustomReport, self).__init__("custom_reports", hideFields)

    def create(self, doc):
        (status, custom_report) = super(CustomReport, self).create(doc)
        return status, custom_report

    def getReport(self,id, query):
        print query

        if(query['report']=='UPI DSR'):
            status,data =self.getUpiSettlementRpt(query)


        elif(query['report']=='UPI TTUM'):
            status, data = self.getUPITTUMReport(query)


        elif(query['report']=='IMPS TTUM'):
            status, data = self.getIMPSTTUMReport(query)

        elif(query['report']=='IMPS DSR'):
            status, data= self.getDailySettlementRpt( id, query)
        return status,data
     
    def getUpiSettlementRpt(self,query):
        reqDict={}
        path = '/opt/reconDB/mft/'
        reqDate = datetime.datetime.strptime(query['statementDate'], '%Y-%m-%d')
        reqDate = reqDate.strftime('%d%m%Y')

        inFiles = os.listdir(path + query['report'].split(' ')[0] + 'Inward')

        outFiles = os.listdir(path + query['report'].split(' ')[0] + 'Outward')
        if reqDate in inFiles:
            reqPath = path + query['report'].split(' ')[0] + 'Inward/' + reqDate + '/'

        elif reqDate in outFiles:
            reqPath = path + query['report'].split(' ')[0] + 'Outward' + '/' + reqDate + '/'

        else:
            reqDict['reqPath'] = 'No Execution Found'
            return False, reqDict['reqPath']
        print reqPath

        if os.path.exists(reqPath):
            files = os.listdir(reqPath)
            files = [i for i in files if re.match('[A-Z]{3}_[A-Z]{7}[0-9]{6}_[0-9]{1}C', i)]

        else:
            print("no execution found")
        print files
        # files = os.listdir(path)
        #
        # files = [i for i in files if re.match('[A-Z]{3}_[A-Z]{7}[0-9]{6}_[0-9]{1}C', i)]

        cycle = ['1C', '2C', '3C', '4C']
        desList = ['Remitter U3 Approved Transaction Amount', 'Deemed Chargeback Acceptance',
                   'Deemed Good Faith Re-Presentment Accept', 'Net Non Compliance Penalty',
                   'Beneficiary/Remitter Sub Totals', 'Deemed Pre-Arbitration Acceptance',
                   'Beneficiary U2 Approved Fee GST', 'Beneficiary U3 Approved Fee GST',
                   'Deemed Pre-compliance-Payer Acceptance', 'Beneficiary U2 Approved NPCI Switching Fee GST',
                   'Remitter U3-RB Approved NPCI Switching Fee GST', 'Net Deemed Good Faith Chargeback Acceptance',
                   'U3 Approved Payer PSP Fee - Paid', 'Dispute Adjustments', 'Deemed Pre-compliance-Payee Acceptance',
                   'Beneficiary U2 Approved NPCI Switching Fee', 'Net Debit Adjustment Switching Fee with Tax',
                   'Net Deemed Good Faith Representment Acceptance', 'Remitter U3 Approved Fee GST',
                   'Net Adjusted Amount', 'Description', 'Final Settlement Amount',
                   'Net Deemed Good Faith Debit Adjustment Acceptance', 'Beneficiary U3-RB Approved Fee',
                   'Remitter U3 Approved NPCI Switching Fee GST', 'Deemed Differed Chargeback Acceptance',
                   'Deemed Good Faith Chargeback Accept', 'Net Adjusted Fee with Tax', 'Settlement Amount',
                   'Beneficiary U3-RB Approved Transaction Amount', 'FOR NON - FINANCIAL MANDATE/OD TXNs',
                   'Net Deemed Good Faith Debit Chargeback Acceptance',
                   'Net Deemed Good Faith Debit Representment Acceptance', 'Beneficiary U2 Approved Fee',
                   'Settlement Charges', 'U3 Approved Payer PSP Fee GST - Paid',
                   'Remitter U3 Approved NPCI Switching Fee', 'Pre-Arbitration Deemed Acceptance Penalty',
                   'Credit Adjustment', 'Remitter U3-RB Approved NPCI Switching Fee',
                   'Beneficiary U3 Approved Transaction Amount',
                   'Net Penalty credited on Pre-Arbitration Acceptance Including GST',
                   'Net Penalty debited on Pre-Arbitration Acceptance Including GST',
                   'U3-RB Approved Payer PSP Fee GST - Paid', 'Adjustment Sub Total', 'Credit Adjustment From ICI',
                   'Remitter U3-RB Approved Fee', 'Remitter U3-RB Approved Transaction Amount',
                   'Net Deemed Deferred Chargeback Acceptance', 'Deemed Differed Pre-Arbitration Acceptance',
                   'Pre-Arbitration Deemed Acceptance', 'Beneficiary U2 Approved Transaction Amount',
                   'U3-RB Approved Payer PSP Fee - Paid', 'Remitter U3-RB Approved Fee GST',
                   'Total Credit Adjustment Amount', 'Remitter U3 Approved Fee', 'Beneficiary U3 Approved Fee',
                   'Beneficiary U3-RB Approved Fee GST','Beneficiary SOD U3 Approved Fee','Beneficiary SOD U3 Approved Fee GST',
                   'Beneficiary SOD U3 Approved Transaction Amount','Beneficiary U2-RB Approved Fee','Beneficiary U2-RB Approved Fee GST',
                   'Beneficiary U2-RB Approved NPCI Switching Fee','Beneficiary U2-RB Approved NPCI Switching Fee GST',
                   'Beneficiary U2-RB Approved Transaction Amount']
        list1 = []

        for a in files:

            df = pandas.read_html(reqPath + a, skiprows=1)


            # df=pd.read_html('/home/pratik/Downloads/UPITTUM files/UPI_NTSLSSF020719_1C.xls',skiprows=1)
            df = pandas.DataFrame(df[0])

            df = df.dropna(axis=0, how='all')
            df["cycle"] = a.split('.')[0].split('_')[-1]

            df.columns = ['Description', 'No of Txns', 'Debit', 'Credit', 'cycle']
            df=df.fillna(0)
            # df=df.iloc[:25]

            df = df.to_dict(orient='records')


            dict1 = {}
            dict1 = {'cycle': a.split('.')[0].split('_')[-1]}
            for i in df:

                if i['Credit'] == 0 and i['Debit'] != 0:
                    dict1[i['Description']] = i['Debit']
                elif i['Credit'] != 0 and i['Debit'] == 0:
                    dict1[i['Description']] = i['Credit']

                elif i['Credit'] != 0 and i['Debit'] != 0:
                    dict1['Beneficiary Sub Totals'] = i['Debit']
                    dict1['Remitter Sub Totals'] = i['Credit']

            for key in desList:
                    if key not in dict1.keys():
                        dict1[key] = 0


            list1.append(dict1)


        return True,json.dumps(list1)

        

    def getUPITTUMReport(self,query):
        reqDict = {}
        path = '/opt/reconDB/mft/'
        reqDate =datetime.datetime.strptime(query['statementDate'], '%Y-%m-%d')
        reqDate = reqDate.strftime('%d%m%Y')

        inFiles = os.listdir(path+query['report'].split(' ')[0]+'Inward')
        outFiles = os.listdir(path + query['report'].split(' ')[0] + 'Outward')
        if reqDate in inFiles:
            reqPath =  path+query['report'].split(' ')[0]+'Inward/'+reqDate+'/'
        elif reqDate in outFiles:
            reqPath = path + query['report'].split(' ')[0] + 'Outward'+'/'+reqDate+'/'
        else:
            reqDict['reqPath'] = 'No Execution Found'
            return False,reqDict['reqPath']




        if os.path.exists(reqPath):
            files = os.listdir(reqPath)
            files = [i for i in files if re.match('[A-Z]{3}_[A-Z]{7}[0-9]{6}_[0-9]{1}C', i)]
        else:
            print("no execution found")
        df = pandas.DataFrame()

        for a in files:
            print a
            htmlList = pandas.read_html(reqPath + a)

            tmpDf = htmlList[3]

            tmpDf.dropna(how='all', inplace=True)

            tmpDf["cycle"] = a.split('.')[0].split('_')[-1]
            tmpDf.columns = ['Description', ' No of Txns', 'Debit', 'Credit', 'cycle']
            df = df.append(tmpDf)
        df = df.sort_values(by='cycle', ascending=False).reset_index()

        # print df.to_csv('/tmp/upittm.csv')

        remark = {
            "Beneficiary U2 Approved Fee": "UPI Beneficiary U2 Approved fees",
            "Beneficiary U2 Approved Fee GST": "UPI Beneficiary U2 Approved fees GST",
            "Beneficiary U3 Approved Fee": "UPI Beneficiary U3 Approved fees",
            "Beneficiary U3 Approved Fee GST": "UPI Beneficiary U3 Approved fees GST",
            "Beneficiary U3 Approved Transaction Amount": "UPI Beneficiary U3  Inward sett dt",
            "Beneficiary U3-RB Approved Fee": "UPI Beneficiary U3-RB Approved fees",
            "Beneficiary U3-RB Approved Fee GST": "UPI Beneficiary U3-RB Approved fees GST",
            "Beneficiary U3-RB Approved Transaction Amount": "UPI Beneficiary U3-RB  Inward sett dt",
            "Beneficiary SOD U3 Approved Fee":"UPI Beneficiary SOD U3 Approved fees",
            "Beneficiary SOD U3 Approved Fee GST":"UPI Beneficiary SOD U3 Approved fees GST",
            "Beneficiary SOD U3 Approved Transaction Amount":"UPI Beneficiary SOD U3  Inward sett dt",
            "Remitter U3 Approved Fee": "Remitter U3 Approved fee dt",
            "Remitter U3 Approved Fee GST": "Remitter U3 Approved Fee GST dt",
            "Remitter U3 Approved NPCI Switching Fee": "Remitter U3 Switching fee dt",
            "Remitter U3 Approved NPCI Switching Fee GST": "Remitter U3 Switchng Fee GST dt",
            "Remitter U3 Approved Transaction Amount": "Remitter U3 approved Outward sett dt",
            "Remitter U3-RB Approved Fee": "Remitter U3-RB Approved fee dt",
            "Remitter U3-RB Approved Fee GST": "Remitter U3-RB Approved Fee GST dt",
            "Remitter U3-RB Approved NPCI Switching Fee": "Remitter U3-RB Switching fee dt",
            "Remitter U3-RB Approved NPCI Switching Fee GST": "Remitter U3-RB Switchng Fee GST dt",
            "Remitter U3-RB Approved Transaction Amount": "Remitter U3-RB approved Outward sett dt",
            "U3 Approved Payer PSP Fee - Paid": "Remitter U3 approved payer PSP fee dt",
            "U3 Approved Payer PSP Fee GST - Paid": "Remitter U3 approved payer PSP fee GST dt",
            "U3-RB Approved Payer PSP Fee - Paid": "Remitter U3-RB approved payer PSP fee dt",
            "U3-RB Approved Payer PSP Fee GST - Paid": "Remitter U3-RB approved payer PSP fee GST dt",
            "Beneficiary U2 Approved NPCI Switching Fee": "Beneficiary U2 Switching fee dt",
            "Beneficiary U2 Approved NPCI Switching Fee GST": "Beneficiary U2 Switchng Fee GST dt",
            "Beneficiary U2 Approved Transaction Amount": "Beneficiary U2 approved Outward sett dt",
            "Beneficiary U2-RB Approved Fee":"UPI Beneficiary U2-RB Approved fees",
            "Beneficiary U2-RB Approved Fee GST":"UPI Beneficiary U2-RB Approved fees GST",
            "Beneficiary U2-RB Approved Transaction Amount":"Beneficiary U2-RB approved Outward sett dt",
            "Beneficiary U2-RB Approved NPCI Switching Fee":"Beneficiary U2-RB Switching fee dt",
            "Beneficiary U2-RB Approved NPCI Switching Fee GST":"Beneficiary U2-RB Switchng Fee GST dt",
            "Settlement Amount": "UPI NET settlement dt"
        }

        CreditAcc = {
            "Beneficiary U2 Approved Fee": "761210260001",
            "Beneficiary U2 Approved Fee GST": "270910050027",
            "Beneficiary U2-RB Approved Fee": "761210260001",
            "Beneficiary U2-RB Approved Fee GST": "270910050027",
            "Beneficiary U2-RB Approved NPCI Switching Fee":"860610140001",
            "Beneficiary U2-RB Approved NPCI Switching Fee GST":"560810010027",
            "Beneficiary U2-RB Approved Transaction Amount": "271220660001",
            "Beneficiary U3 Approved Fee": "761210260001",
            "Beneficiary U3 Approved Fee GST": "270910050027",
            "Beneficiary U3 Approved Transaction Amount": "600310040001",
            "Beneficiary U3-RB Approved Fee": "761210260001",
            "Beneficiary U3-RB Approved Fee GST": "270910050027",
            "Beneficiary U3-RB Approved Transaction Amount": "600310040001",
            "Beneficiary SOD U3 Approved Fee": "761210260001",
            "Beneficiary SOD U3 Approved Fee GST": "270910050027",
            "Beneficiary SOD U3 Approved Transaction Amount": "600310040001"

        }

        DebitAcc = {
            "Remitter U3 Approved Fee": "860610140001",
            "Remitter U3 Approved Fee GST": "560810010027",
            "Remitter U3 Approved NPCI Switching Fee": "860610140001",
            "Remitter U3 Approved NPCI Switching Fee GST": "560810010027",
            "Remitter U3 Approved Transaction Amount": "271220660001",
            "Remitter U3-RB Approved Fee": "860610140001",
            "Remitter U3-RB Approved Fee GST": "560810010027",
            "Settlement Amount": " ",
            "Remitter U3-RB Approved NPCI Switching Fee": "860610140001",
            "Remitter U3-RB Approved NPCI Switching Fee GST": "560810010027",
            "Remitter U3-RB Approved Transaction Amount": "271220660001",
            "U3 Approved Payer PSP Fee - Paid": "860610140001",
            "U3 Approved Payer PSP Fee GST - Paid": "560810010027",
            "U3-RB Approved Payer PSP Fee - Paid": "860610140001",
            "U3-RB Approved Payer PSP Fee GST - Paid": "560810010027",
            "Beneficiary U2 Approved NPCI Switching Fee": "860610140001",
            "Beneficiary U2 Approved NPCI Switching Fee GST": "560810010027",
            "Beneficiary U2 Approved Transaction Amount": "271220660001"

        }

        desclist = DebitAcc.keys()
        desclist += CreditAcc.keys()

        df = df[df['Description'].isin(desclist)]
        df['DEBIT GL'] = df.Description.map(DebitAcc)
        df['CREDIT GL'] = df.Description.map(CreditAcc)

        df['Credit'] = df['Credit'].astype(float)
        df['Debit'] = df['Debit'].astype(float)
        df['AMOUNT'] = df['Debit'] + df['Credit']
        ndebit = df[df['CREDIT GL'].isin(CreditAcc.values())]

        ndebit['Beneficiary Cost Centre'] = '10000'

        debit1 = df[(df['DEBIT GL'].isin(DebitAcc.values())) & (df['Description'] != 'Settlement Amount')]

        debit1['Debit Cost Centre'] = '10000'
        withoutsettle = pandas.concat([ndebit, debit1])

        settle = df[(df['Description'] == 'Settlement Amount') & (df['Credit'] == 0)]

        if len(settle) > 0:
            settle['CREDIT GL'] = '271210030001'
            settle['Beneficiary Cost Centre'] = '10000'

        settle1 = df[(df['Description'] == 'Settlement Amount') & (df['Debit'] == 0)]

        if len(settle1) > 0:
            settle1['DEBIT GL'] = '271210030001'
            settle1['Debit Cost Centre'] = '10000'

        df = pandas.concat([withoutsettle, settle, settle1])

        date = reqDate

        df['REMARKS'] = df.Description.map(remark) + reqDate + '-' + df["cycle"]

        df = df.rename(columns={'Beneficiary Cost Centre': 'CREDIT CC', 'Debit Cost Centre': 'DEBIT CC'})

        newcols = ['DEBIT GL', 'DEBIT CC', 'CREDIT GL', 'CREDIT CC', 'AMOUNT', 'REMARKS']
        df = df[newcols]

        prevLen = len(os.listdir(hashframe.config.contentDir))
        df.to_csv(path+'OUTPUT/'+'TTUM'+'_'+reqDate+'.csv',sep ='|',index = False)
        df.to_csv(hashframe.config.contentDir+'TTUM'+'_'+reqDate+'.csv',sep = '|',index = False)
        

        reqDict['reqPath'] = 'TTUM' + '_' + reqDate + '.csv'
        return True, reqDict['reqPath']

    def getIMPSTTUMReport(self,query):
        reqDict = {}

        path = '/opt/reconDB/mft/'
        reqDate = datetime.datetime.strptime(query['statementDate'], '%Y-%m-%d')
        reqDate = reqDate.strftime('%d%m%Y')

        inFiles = os.listdir(path+query['report'].split(' ')[0]+'_INWARD')

        outFiles = os.listdir(path + query['report'].split(' ')[0] + '_OUTWARD')

        if reqDate in inFiles:

            reqPath =  path+query['report'].split(' ')[0]+'_INWARD/'+reqDate+'/'
        elif reqDate in outFiles:

            reqPath = path + query['report'].split(' ')[0] + '_OUTWARD'+'/'+reqDate+'/'
        else:
            reqDict['reqPath'] = 'No Execution Found'
            return False,reqDict['reqPath']


        # path = hashframe.config.mftPath+'IMPS_INWARD/'+reqDate+'/'
        files = os.listdir(reqPath)
        files = [i for i in files if re.match('[A-Z]{11}[0-9]{6}_[0-9]{1}C', i)]

        logr.info(files)
        df = pandas.DataFrame()

       #cycle = ['1C', '2C', '3C', '4C']

        for a in files:
            print(a,a.split('_')[1].split('.')[0].strip())
            htmlList = pandas.read_html(reqPath + a)

            tmpDf = htmlList[3]

            tmpDf.dropna(how='all', inplace=True)

            tmpDf["cycle"] = a.split('_')[1].split('.')[0].strip()

            tmpDf.columns = ['Description', ' No of Txns', 'Debit', 'Credit', 'cycle']

            df = df.append(tmpDf)

        df = df.sort_values(by='cycle', ascending=False).reset_index()
        remark = {
            "Beneficiary P2A Approved Fee": "IMPS I/w Approved fees ",
            "Beneficiary P2A Approved Fee GST": "IMPS I/w Approved fees GST ",
            "Beneficiary P2A Approved Transaction Amount": "IMPS Inward sett dt",
            "Remitter P2A Approved Fee": "IMPS O/w Approved fee dt ",
            "Remitter P2A Approved Fee GST": "IMPS O/w Approved Fee GST dt ",
            "Remitter P2A Approved NPCI Switching Fee": "IMPS O/w Switching fee dt",
            "Remitter P2A Approved NPCI Switching Fee GST": "IMPS O/w Switchng Fee GST dt",
            "Remitter P2A Approved Transaction Amount": "IMPS Outward sett dt",
            "Settlement Amount": "IMPS Net settlement dt ",
            "Remitter P2A-08 Approved Fee": "IMPS O/w P-08 Approved fee dt",
            "Remitter P2A-08 Approved Fee GST": "IMPS O/w P-08 Approved Fee GST dt",
            "Remitter P2A-08 Approved NPCI Switching Fee": "IMPS O/w P-08 Switching fee dt",
            "Remitter P2A-08 Approved NPCI Switching Fee GST": "IMPS O/w P-08  Switchng Fee GST dt",
            "Remitter P2A-08 Approved Transaction Amount": "IMPS P-08  Outward sett dt",
            "Beneficiary P2A-08 Approved Fee": "IMPS I/w P-08 Approved fee dt",
            "Beneficiary P2A-08 Approved Fee GST": "IMPS I/w P-08 Approved Fee GST dt",
            "Beneficiary P2A-08 Approved Transaction Amount": "IMPS Inward P-08 sett dt"
        }
        CreditAcc = {
            "Beneficiary P2A Approved Transaction Amount": "271210050001",
            "Beneficiary P2A Approved Fee": "761210210001",
            "Beneficiary P2A Approved Fee GST": "270910050027",
            "Beneficiary P2A-08 Approved Fee": "761210210001",
            "Beneficiary P2A-08 Approved Fee GST": "270910050027",
            "Beneficiary P2A-08 Approved Transaction Amount": "271210050001"
        }
        DebitAcc = {
            "Remitter P2A Approved Transaction Amount": "271210370001",
            "Remitter P2A Approved Fee": "860610130001",
            "Remitter P2A Approved NPCI Switching Fee": "860610130001",
            "Remitter P2A Approved Fee GST": "560810010027",
            "Remitter P2A Approved NPCI Switching Fee GST": "560810010027",
            "Settlement Amount": "",
            "Remitter P2A-08 Approved Fee": "860610130001",
            "Remitter P2A-08 Approved Fee GST": "560810010027",
            "Remitter P2A-08 Approved NPCI Switching Fee": "860610130001",
            "Remitter P2A-08 Approved NPCI Switching Fee GST": "560810010027",
            "Remitter P2A-08 Approved Transaction Amount": "271210370001"
        }

        desclist = DebitAcc.keys()
        desclist += CreditAcc.keys()

        df = df[df['Description'].isin(desclist)]
        df['DEBIT GL'] = df.Description.map(DebitAcc)
        df['CREDIT GL'] = df.Description.map(CreditAcc)

        df['Credit'] = df['Credit'].astype(float)
        df['Debit'] = df['Debit'].astype(float)
        df['AMOUNT'] = df['Debit'] + df['Credit']
        # print df
        ndebit = df[df['CREDIT GL'].isin(CreditAcc.values())]

        ndebit['Beneficiary Cost Centre'] = '10000'

        debit1 = df[(df['DEBIT GL'].isin(DebitAcc.values())) & (df['Description'] != 'Settlement Amount')]

        print(len(debit1))

        debit1['Debit Cost Centre'] = '10000'
        withoutsettle = pandas.concat([ndebit, debit1])
        settle = df[(df['Description'] == 'Settlement Amount') & (df['Credit'] == 0)]

        if len(settle) > 0:
            settle['CREDIT GL'] = '271210030001'
            settle['Beneficiary Cost Centre'] = '10000'

        settle1 = df[(df['Description'] == 'Settlement Amount') & (df['Debit'] == 0)]

        if len(settle1) > 0:
            settle1['DEBIT GL'] = '271210030001'
            settle1['Debit Cost Centre'] = '10000'

        df = pandas.concat([withoutsettle, settle, settle1])


        date = reqDate

        df['REMARKS'] = df.Description.map(remark) + reqDate + '-' + df["cycle"]

        df = df.rename(columns={'Beneficiary Cost Centre': 'CREDIT CC', 'Debit Cost Centre': 'DEBIT CC'})
        # print "******************************************"

        newcols = ['DEBIT GL', 'DEBIT CC', 'CREDIT GL', 'CREDIT CC', 'AMOUNT', 'REMARKS']

        df = df[newcols]


        reqDict['columns'] = df.columns.tolist()
        # reqDict['data'] = df.to_dict(orient = 'records')
        prevLen = len(os.listdir(hashframe.config.contentDir))
        df.to_csv(path+'OUTPUT/'+'TTUM'+'_'+reqDate+'.csv',sep ='|',index = False)
        df.to_csv(hashframe.config.contentDir+'TTUM'+'_'+reqDate+'.csv',sep = '|',index = False)
        reqDict['reqPath'] = 'TTUM'+'_'+reqDate+'.csv'

        return True,reqDict['reqPath']

    def getDailySettlementRpt(self, id, query):
        try:
            query['statementDate'] = datetime.datetime.strptime(query['statementDate'], '%Y-%m-%d')
            # print {'statementDate': query['statementDate'], "reportName": query["reportType"]}
            aqqData = pandas.DataFrame()
            issData = pandas.DataFrame()
            # Get Aqquirer Data


            status, data= CustomReport().get(
                {'statementDate': query['statementDate'], "reportName": 'remmiter', 'reconName': 'IMPS_OUTWARD'})
            if status:
                aqqData = pandas.DataFrame(data['data'])
                del aqqData['Remmiter P2A-08#Cycle']
                col = {}
                for i in aqqData.columns:
                    if '#' in i:
                        if i.split('#')[1] not in ['Debit','Credit','Cycle']:
                            col[i] = i.split('#')[1]+'aqqCol'
                        else:
                            col[i] = i.split('#')[1]
                aqqData.rename(columns=col, inplace=True)
                # aqqData['CYCLE'] = aqqData['FEED_FILE_NAME'].str.split('_').str[1].str[:2]
                # del aqqData['FEED_FILE_NAME']

            # Get Issuer Data
            status, data= CustomReport().get(
                {'statementDate': query['statementDate'], "reportName": 'beneficiary', 'reconName': 'IMPS_INWARD'})

            if status:
                issData = pandas.DataFrame(data['data'])
                del issData["Beneficiary P2A-08#Cycle"]
                col1 = {}
                for i in issData.columns:
                    if '#' in i:
                        if i.split('#')[1] not in ['Debit','Credit','Cycle']:
                            col1[i] = i.split('#')[1]+'issCol'
                        else:
                            col1[i] = i.split('#')[1]
                issData.rename(columns=col1, inplace=True)
                # del issData['FEED_FILE_NAME']


            if len(aqqData) or len(issData):
                df = pandas.concat([aqqData, issData], axis=1)
                df['REMITTER'] = False
                df['BENEFICIARY'] = False
                df.loc[df['Debit'] == df['remitter_sub_totalaqqCol'], 'REMITTER'] = True
                df.loc[df['Credit'] == df['beneficiary_sub_totalissCol'], 'BENEFICIARY'] = True
                doc=df.to_dict(orient='records')
                print type(doc)
                return True, json.dumps(doc)
            else:
                return True, {}
        except Exception as e:
            print e
            print traceback.format_exc()


#  store Recons Details from settings tab
class Recons(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(Recons, self).__init__("recons", hideFields)

    def create(self, doc):
        (status, reconsDoc) = super(Recons, self).create(doc)
        return status, reconsDoc

    def deleteData(self, condition, query):
        try:
            status, result = Recons().delete({'reconName': query['reconName']})
            if status:
                status, result = SourceReference().delete({'reconName': query['reconName']})
                if status:
                    return True, result
                else:
                    return False, 'Fail'
            else:
                return False, 'Failed'
        except Exception as e:
            logr.info(traceback.format_exc())

    def getReconDetails(self, id):
        status, reconDetail = Recons().getAll({})

    def getProcessRecons(self, id, query):
        status, recons = Recons().getAll({"condition": {"reconProcess": query['reconProcess']}})
        allRecons = []
        if status and len(recons['data']) > 0:
            for recon in recons['data']:
                doc = {}
                doc['reconName'] = recon['reconName']
                doc['reconProcess'] = recon['reconProcess']
                doc['sourcesCount'] = len(recon['sources'].keys())
                doc['sources'] = recon['sources']
                doc['_id'] = recon['_id']
                allRecons.append(doc)
        return True, allRecons

    def cloneSelectedRecon(self, id, query):
        recon = Recons().find_one({'reconName': query['reconName']})
        feedId = {}
        if recon:
            for source in recon['sources']:
                for struct, values in recon['sources'][source].iteritems():
                    sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime('%d%m%Y%H%M%S')
                    values['feedId'] = sourceId
                    feedId[source] = sourceId

            recon['reconName'] = recon['reconName'] + '_Cloned'
            doc = Recons().find_one({'reconName': recon['reconName']})
            if doc:
                recon['reconName'] = recon['reconName'] + '_Cloned'
            del recon['_id']
            Recons().save(recon)

        else:
            return False, 'No data found for cloning'
        if query['cloneSrc']:
            srcReferences = list(SourceReference().find({'reconName': query['reconName']}))
            if len(srcReferences) > 0:
                for source in srcReferences:
                    del source['_id']
                    source['reconName'] = recon['reconName']
                    source['feedId'] = feedId[source['source']]
                    SourceReference().save(source)
            else:
                return True, 'Source references not found'

        else:
            return False, 'No data found for cloning'
        return True, 'Recon cloned successfully'

    def saveRecon(self, id, query):
        try:
            if '_id' in query.keys():
                recon_details = Recons().find_one({'_id': ObjectId(query['_id'])})
                if recon_details:
                    for source in query['sources']:
                        for struct, values in query['sources'][source].iteritems():
                            sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime(
                                '%d%m%Y%H%M%S')
                            if 'feedId' not in values.keys():
                                values['feedId'] = sourceId
                    Recons().modify(query['_id'], query)

                for source in query['sources']:
                    for struct, values in query['sources'][source].iteritems():
                        reference = SourceReference().find_one({'feedId': values['feedId']})
                        if reference:
                            reference['reconName'] = query['reconName']
                            reference['source'] = source
                            reference['struct'] = struct
                            SourceReference().modify(reference['_id'], reference)
                return True, 'Updated successfully'
            else:
                query['isLicensed'] = False
                query['partnerId'] = "54619c820b1c8b1ff0166dfc"
                for source in query['sources']:
                    for struct in query['sources'][source]:
                        sourceId = str(uuid.uuid4().int & (1 << 64) - 1) + datetime.datetime.now().strftime(
                            '%d%m%Y%H%M%S')
                        query['sources'][source][struct]['feedId'] = sourceId
                Recons().save(query)
                return True, 'Added successfully'
        except Exception as e:
            print traceback.format_exc()
            logr.info(e)
            return False, e

    def getReports(self,id,query):
        if query['stmtDate']:
            stmtDate=datetime.datetime.strptime(query['stmtDate'],'%d/%m/%Y')
        else:
            return False, 'Please Select StatementDate'

        doc = list(ReconExecDetailsLog().find(
            {'reconName': query['reconName'], 'jobStatus': 'Success', 'statementDate':stmtDate }))

        if doc:
            path=hashframe.config.mftPath+os.sep+query['reconName']+os.sep+datetime.datetime.strftime(stmtDate,'%d%m%Y')+'/OUTPUT/'
            filesList=os.listdir(path)
            downloadPathList=[]
            for file in filesList:
                fileDict={'fileName':'','downloadPath':''}
                shutil.copy(path+file,hashframe.config.contentDir)
                key=os.path.basename(path+file).split('.')[0]
                fileDict['fileName']=key
                fileDict['downloadPath']=file
                downloadPathList.append(fileDict)
            return True,downloadPathList
        return False,'No Execution Found For This Date'

#  Structure Details From MarketPlace Tab For each Structure
class SourceReference(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(SourceReference, self).__init__("sourceReference", hideFields)

    def create(self, doc):
        (status, sourceRefDoc) = super(SourceReference, self).create(doc)
        return status, sourceRefDoc

    def downloadSource(self, id, query):
        refernce = SourceReference().find_one({'source': query['source'], 'struct': query['struct']})
        if refernce is None:
            df = pandas.DataFrame(refernce['feedDetails'])
        else:
            df = pandas.DataFrame(query['feedDetails'])

        fileName = hashframe.config.uploadDir.get(id, 'file') + os.sep + query['struct'] + 'Reference.csv'
        df = df[['fileColumn', 'dataType', 'datePattern', 'position']]
        if refernce is not None:
            # open a file for writing

            df.to_csv(fileName, index=False)
        else:
            return False, 'No Data Found'
        return True, query['struct'] + 'Reference.csv'

    def loadSourceRef(self, id, query={}):
        if query.get('filter'):
            _file = hashframe.config.uploadDir.get('filter') + os.sep + query.get('fileName')
            with open(_file) as f:
                lines = [x for x in f.readlines() if x.strip().replace('\n', '')]
                feed = SourceReference().find_one(
                    {'reconName': query['reconName'], 'source': query['source'], 'struct': query['struct']})
                if feed:
                    if 'filters' not in feed.keys():
                        feed['filters'] = []
                    for filter in feed['filters']:
                        lines.append(filter)
                return True, lines
        else:
            ref_file = pandas.read_csv(hashframe.config.uploadDir['file'] + os.sep + query.get('fileName'),
                                       engine='python')
            ref_file.replace('nan', '', inplace=True)
            ref_file.rename(columns=hashframe.config.sourceRefMapper, inplace=True)
            ref_file['position'] = ref_file.index + 1
            ref_file.fillna('', inplace=True)
            return True, ref_file.to_dict(orient="records")

    def getSourceForRecon(self, id, query):
        status, sources = SourceReference().getAll({'condition': {'reconName': query['reconName']}})
        reconDetails = []
        if status:
            for source in sources['data']:
                doc = {}
                doc['reconName'] = source['reconName']
                doc['struct'] = source['struct']
                doc['source'] = source['source']
                doc['referenceSource'] = source['referenceSource']

                if 'position' in source.keys():
                    doc['position'] = source['position']
                else:
                    doc['position'] = sources['data'].index(source) + 1
                reconDetails.append(doc)
            reconDetails = sorted(reconDetails, key=lambda k: k['position'])
        return True, reconDetails

    def storeOrderOfSource(self, id, query):
        if query is not None:
            for i in query:
                SourceReference().modify(
                    {'source': i['source'], 'struct': i['struct'], 'reconName': i['reconName']},
                    {'position': i['position']})
        return True, 'Updated SuccessFully'

    def addSourceRef(self, id, query):
        recon_details = Recons().find_one({'_id': ObjectId(query['_id'])})
        for ref in query['sourceRef']:
            reconPath = hashframe.config.mftPath + ref['reconName']
            if not os.path.exists(reconPath):
                os.mkdir(reconPath)
            status, doc = SourceReference().get(
                {'reconName': ref['reconName'], 'struct': ref['struct'], 'source': ref['source']})
            if status:
                for source in recon_details['sources']:
                    for struct, values in recon_details['sources'][source].iteritems():
                        if 'feedId' not in doc.keys():
                            if ref['struct'] == struct:
                                doc['feedId'] = values['feedId']
                SourceReference().modify(doc['_id'], doc)
            else:
                for source in recon_details['sources']:
                    for struct, values in recon_details['sources'][source].iteritems():
                        if 'feedId' not in ref.keys():
                            if ref['struct'] == struct:
                                ref['feedId'] = values['feedId']
                status, _ = SourceReference().create(ref)

        return True, ''

    def saveSourceDetails(self, id, query, checkFileProperty=True, deriveColumn=False):
        columns = []

        # Find Recon Details
        recon = Recons().find_one({'reconName': query['reconName']})
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"

        # Read Properties Of sourceFile if required
        if checkFileProperty:
            status, doc = self.getSourceProperties(
                hashframe.config.uploadDir['source'] + '/' + query.get('fileName', ''))
            data = {'delimiter': doc['delimiter'], 'fileName': query['fileName'],
                    'loadType': doc['loadType'], 'skiprows': query.get('skipRows', 0),
                    'filePattern': doc['filePattern']}
            data['filters'] = []
            data['derivedColumns'] = []
            data['reconName'] = query.get('reconName', '')

            data['reconProcess'] = recon.get('reconProcess', '') if recon else ''
            data['source'] = query.get('source', '')
            data['fileName'] = query['fileName']

        else:
            # Properties of file will be in Query
            doc = query
            data = query

        # Read Sample File of Source
        if query.get('fileName'):
            sep = str(doc['delimiter'])
            if os.path.exists(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName')):
                # Read CSV Files
                if doc['loadType'] == 'CSV':
                    skiprows = str(query.get('skiprows', 0)).replace('.0', '')
                    skiprows = int(skiprows)
                    df = pandas.read_csv(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'),
                                         skiprows=skiprows, skipfooter=query.get('skipfooter', 0),
                                         sep=str(doc['delimiter']), engine='python')
                    df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

                # Read Excel Files
                if doc['loadType'] == 'Excel':
                    if 'sheetNames' not in query:
                        xl = pandas.ExcelFile(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'))
                        data['sheetNames'] = xl.sheet_names
                        return True, data
                    data['sheetNames'] = query['sheetNames']

                    if query.get('skiprows', 0) != 0:
                        skiprows = query.get('skiprows', 0) - 1
                    else:
                        skiprows = str(query.get('skiprows', 0)).replace('.0', '')

                    df = pandas.read_excel(hashframe.config.uploadDir['source'] + os.sep + query.get('fileName'),
                                           sheet_name=query.get('sheetNames', 0),
                                           skiprows=skiprows, skipfooter=query.get('skipfooter', 0))
                    df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)

                # Generate Column Dtypes For sourceReference
                for col in df.columns:
                    columns.append({'fileColumn': col, 'dataType': 'str', 'required': True, 'datePattern': '',
                                    'position': df.columns.get_loc(col) + 1})
                # print columns
                # Call Function For columnType
                columns = self.getColumnType(df)
                data['feedDetails'] = columns

            else:
                return False, 'File is not found. Please Upload File Once'

            # Condition to save Source Details or not
            data['saveSourceDetails'] = query.get('saveSourceDetails', True)

            # Call Function to save Feed Details
            self.saveFeedDetails(data, deriveColumn)

            # Deletd _id in data to return
            if '_id' in data:
                del data['_id']

            data['data'] = df.to_dict(orient='records')
            data['data'] = json.dumps(data['data'], default=self.myConverter)
        return True, data

    def myConverter(self, data):
        if isinstance(data, datetime.datetime):
            return data.__str__()

    def skipRows(self, id, query):
        feed = SourceReference().find_one({'reconName': query['reconName'], 'source': query['source']})
        feed['skiprows'] = feed.get('skiprows', 0) + query['skiprows']
        feed['skipfooter'] = feed.get('skipfooter', 0) + query.get('skipfooter',0)
        SourceReference().modify(feed['_id'], feed)
        status, data = self.saveSourceDetails('dummy', feed, checkFileProperty=False)
        return True, data

    def saveFeedDetails(self, query, derivedColumn=False):
        # Copy of sourceDetails
        feedBackUp = query.copy()

        # Find existing Feed For source
        feed = SourceReference().find_one(
            {'reconName': feedBackUp['reconName'], 'source': feedBackUp.get('source', '')})

        if feed:
            # Fucntion called From derivedColumns Function
            if derivedColumn:
                feedBackUp['feedDetails'] = feed['feedDetails']

            # Function will be called from loadSampleFile or while Uploading sampleFile
            if query['saveSourceDetails'] == False:
                if 'saveSourceDetails' in feed.keys():
                    del feedBackUp['saveSourceDetails']
                if 'fileName' in query:
                    feed['fileName'] = query['fileName']
                    status, doc = SourceReference().modify(feed['_id'], feed)
                return True, ''

            if 'saveSourceDetails' in feedBackUp.keys():
                del feedBackUp['saveSourceDetails']

            # Modify the existing FeedDef with updated Details
            status, doc = SourceReference().modify(feed['_id'], feedBackUp)

        # If no Feed found for source saveDetails directly to mongo
        else:

            # not to store saveSourceDetails varibale in sourceEntry
            if 'saveSourceDetails' in feedBackUp.keys():
                del feedBackUp['saveSourceDetails']

            # Add Partner Id into Feed and Save
            feedBackUp['partnerId'] = "54619c820b1c8b1ff0166dfc"
            SourceReference().save(feedBackUp)
            return True, ''

    def getColumnType(self, df):
        columns = []
        for col in df.columns:
            df[col] = df[col].fillna(ReconDetails().infer_dtypes(df[col]))
            a = ReconDetails().infer_dtypes(df[col])
            if a == '' or 0:
                dtype = 'str'
            elif a == 0.0:
                dtype = 'np.float64'
            else:
                df[col] = df[col].astype(str)
                dtype = 'np.datetime64'
            columns.append({'fileColumn': col, 'dataType': 'str', 'required': True, 'datePattern': '',
                            'position': df.columns.get_loc(col) + 1})
        return columns

    def loadSampleFile(self, id, query):
        doc = SourceReference().find_one(
            {'reconName': query['reconName'], 'source': query['source'], 'struct': query['struct']})

        doc['saveSourceDetails'] = False

        if doc and 'fileName' in doc:
            status, data = self.saveSourceDetails('dummy', doc, checkFileProperty=False)
            return status, data
        else:
            return False, 'Please upload sample File'

    def saveFilters(self, id, query):
        sourceRefrences = list(
            SourceReference().find({'reconName': query['reconName'], 'reconProcess': query['reconProcess']}))
        for i in sourceRefrences:
            i['filters'] = query['filters']
            SourceReference().modify(i['_id'], i)
        return True, ''

    def deleteSource(self, id, query):
        data = SourceReference().delete(query)
        return True, ''

    # def deriveColumns(self, id, query):
    #     # if query['columnType'] == 'str':
    #     completeString = str(query['completeValue'])
    #     subString = query['selectedText']
    #     specialChar = ['/', ':', '-', '~', '_', '%']
    #     doc = {}
    #     for i in specialChar:
    #         count = completeString.count(i)
    #         if (count != 0):
    #             list = completeString.split(i)
    #             index = list.index(subString) if subString in list else ''
    #             if index is not '':
    #                 doc = {}
    #                 doc['type'] = 'derivedColStrExp'
    #                 doc['filter_type'] = 'split'
    #                 doc['value'] = i
    #                 doc['index'] = index
    #                 doc['columnName'] = query['selectedColumn']

    # if doc == {}:
    #     starValue = completeString.index(subString)
    #     if len(completeString) == len(subString) and completeString == subString:
    #         endValue = ''
    #         starValue = ''
    #     elif len(subString) + starValue == len(completeString):
    #         endValue = ''
    #         starValue = completeString.index(subString)
    #     elif starValue == 0:
    #         starValue = 0
    #         endValue = len(subString)
    #     else:
    #         endValue = completeString.rindex(subString[-1:]) + 1
    #
    #     doc = {}
    #     doc['type'] = 'derivedColStrExp'
    #     doc['filter_type'] = 'sub string'
    #     doc['endValue'] = endValue
    #     doc['columnName'] = query['selectedColumn']
    #     doc['startValue'] = starValue
    #     doc['value'] = str(starValue) + ':' + str(endValue)
    #     logr.infoquery
    #     logr.info'*' * 100
    #     status, exp = self.checkFilePropertyExpression(doc, query)
    #     data = {}
    #     query['filters'] = exp['filters']
    #     columns = self.getColumnType(exp['data'])
    #     query['feedDetails'] = columns
    #     query['derivedColumns'] = exp['derivedColumns']
    #
    #     self.saveSourceDetails(query, derivedColumn=True)
    #     if '_id' in query:
    #         del query['_id']
    #     query['data'] = exp['data'].to_dict(orient='records')
    #     return True, query

    def checkFilePropertyExpression(self, doc, data):
        with open('/usr/share/nginx/www/ngerecon/flask/JSON/expression.json', "r") as f:
            expressions = json.load(f)
        reqExpe = expressions[doc['type']][doc['filter_type']]
        reqExpe = reqExpe.replace('$colName', doc['columnName'])
        reqExpe = reqExpe.replace('$colValue', doc['value'])
        exp = "df['" + doc['columnName'].replace(' ', '') + "Derived'] = " + reqExpe
        if 'index' in doc and doc['index'] is not None:
            exp = exp.replace('$index', str(doc['index']))
        else:
            exp = exp.replace('$index', '')

        feed = SourceReference().find_one({'reconName': data['reconName'], 'source': data['source']})
        data['filters'].append(exp)
        data['derivedColumns'].append(doc['columnName'].replace(' ', '') + "Derived")
        if feed:
            status, sourceData = self.saveSourceDetails('dummy', feed, checkFileProperty=True)
        df = pandas.DataFrame(sourceData['data'])
        if df is not None:
            for exp in data['filters']:
                logr.infoexp
                exec (exp)
        doc = {}
        doc['filters'] = data['filters']
        doc['derivedColumns'] = data['derivedColumns']
        doc['data'] = df
        return True, doc

    def getDelimiter(self, sourcefile, delimiterlist):
        try:
            print('Checking For Delimeter')
            print('======================')
            file = open(sourcefile, 'r')
            data = " ".join(file.readlines()[0:25])
            countlist = []

            for delimiter in delimiterlist:
                countlist.append(data.count(str(delimiter)))
            print 'here not att all comes'
            print('Delimeters Counts in File %s' % (countlist))

            max_value = max(countlist)
            print("Max Value in CountList %s" % (max_value))

            max_index = countlist.index(max_value)
            logr.info("Delimeter in file is %s" % (delimiterlist[max_index]))
            print('======================')

            return delimiterlist[max_index]
        except Exception as e:
            logr.info("Below Error Occured while Geting Delimiter")
            logr.info(e)

    def getSourceProperties(self, sourcefile):
        delimiter = None
        fileName = os.path.basename(sourcefile)
        fileExtension = fileName.split('.')
        if len(fileExtension) > 2:
            for ext in fileExtension:
                if ext in hashframe.config.extensionCheck:
                    fileExtension = ext
        else:
            fileExtension = fileName.split('.')[1]
        filePattern = self.getfilePattern(fileName.split('.')[0]) + '.' + fileExtension
        loadType, delimiter = self.fileTypeMapper(sourcefile, fileExtension)
        doc = {}
        doc['loadType'] = loadType
        doc['delimiter'] = delimiter
        doc['filePattern'] = filePattern
        return True, doc

    def fileTypeMapper(self, sourcefile, fileExtension):
        delimiter = None
        delimiterList = self.getDelimiterList()
        fileExtension = fileExtension.lower()
        if fileExtension not in hashframe.config.checkforDelimiter['listOfNonDelimitedTypes']:
            delimiter = self.getDelimiter(sourcefile, delimiterList)
        loadType = self.getLoadType(fileExtension, delimiter)

        return loadType, delimiter

    def getfilePattern(self, filename):
        ext = "".join(['d' if x.isdigit() else x for x in filename])
        prev = ext[0]
        count = 1
        for c in ext[1:]:
            if prev != c:
                if prev == 'd':
                    ext = ext.replace(prev * count, '[0-9]{' + str(count) + '}', 1)
                    count = 1
                else:
                    count = 1
            else:
                count += 1
            prev = c
        else:
            if prev == 'd':
                ext = ext.replace(prev * count, '[0-9]{' + str(count) + '}', 1)
        return ext

    def getDelimiterList(self):
        delimiterList = []
        for key, property in hashframe.config.fileproperties.iteritems():
            if 'delimiter' in property.keys():
                delimiterList.extend(property['delimiter'])
        return delimiterList

    def getLoadType(self, fileExtension, delimiter):
        loadType = ''
        fileExtension = fileExtension.lower()
        if delimiter is None:
            for key, property in hashframe.config.fileproperties.iteritems():
                if fileExtension in property['types']:
                    loadType = key
        else:
            for key, property in hashframe.config.fileproperties.iteritems():
                if fileExtension in property['types'] and delimiter in property.get('delimiter', ''):
                    loadType = key
        return loadType

    def generateFilter(self, column, fullVal, partVal, regX, frontChar, rearChar, filterModule, filterColumn,
                       filters=[]):
        regexVal = frontChar + partVal + rearChar
        nopartVal = False
        if not filters:
            string = "df['" + column + "_Derived'" + ']='

        for fil in filters:
            if column in fil.split('=')[0]:
                string = 'df.loc[df["' + column + '_Derived"]=="",' + '"' + column + '_Derived"]='
            else:
                string = 'df["' + column + '_Derived"' + ']='
        groups = re.search(regX, fullVal).groups() if re.search(regX, fullVal) else []

        if regexVal not in groups:
            nopartVal = True
            regX = regX.replace(frontChar, '').replace(rearChar, '').replace('\\', '')
            groups = re.findall(regX, fullVal) if re.findall(regX, fullVal) else []

        for index, group in enumerate(groups):
            if partVal in group:
                idx = str(index)
                break
        if not nopartVal:
            exp = 'df["' + column + '"].apply(lambda x:' + 're.search("' + regX + '",str(x)).groups()[' + str(
                idx) + '].replace("' + frontChar + '","").replace("' + rearChar + '","") if re.search("' + regX + '",str(x)) else "" )'
        else:
            exp = 'df["' + column + '"].apply(lambda x:' + 're.findall("' + regX + '",str(x))[' + str(
                idx) + '].replace("' + frontChar + '","").replace("' + rearChar + '","") if re.findall("' + regX + '",str(x)) else "" )'

        if len(filterModule) > 0:
            string = "df.loc[" + filterModule + ',"' + filterColumn + '"]=' + exp

        else:
            string += exp

        return True, string

    def deriveColumns(self, id, query):
        try:
            partVal = str(query['selectedText'])
            fullVal = str(query['completeValue'])
            column = str(query['selectedColumn'])
            filterModule = query.get('filterModule', '')
            filterColumn = query.get('filterColumn', '')

            expression = self.checkforSubstring(partVal, fullVal, column)
            structure = SourceReference().find_one({'reconName': query['reconName'], 'source': query['source']})

            if expression:
                if len(query['filters']) > 0:
                    try:
                        query['filters'].index(expression)
                        index = 0
                    except:
                        index = -1
                    if index == -1:
                        query['filters'].append(expression)
                else:
                    query['filters'].append(expression)

            else:
                ext = "".join(['w' if x.isalpha() else '' 'd' if x.isdigit() else x for x in partVal])

                specialChar = ['/', ':', '-', '~', '_', '%']
                charList = [char for char in specialChar if char in partVal]

                wcount = ext.count('w')
                lcount = ext.count('d')
                scount = len("".join([x for x in ext if x not in ['w', 'd']]))

                if lcount > 0 and wcount > 0:
                    regX = '([A-Za-z0-9' + ''.join(charList) + ']+)'
                elif lcount + scount == len(partVal) or wcount + scount == len(partVal):
                    if lcount > 0:
                        regX = '([0-9' + ''.join(charList) + ']+)'
                    else:
                        regX = '([A-Za-z ' + ''.join(charList) + ']+)'

                frontVal, rearVal, frontChar, rearChar = self.checkforSurrounding(fullVal, partVal, regX)
                if frontVal:
                    regX = regX.replace('(', frontVal)
                if rearVal:
                    regX = regX.replace(')', rearVal)

                if '/' in regX:
                    regX = regX.replace('/', '\/')

                status, exp = self.generateFilter(column, fullVal, partVal, regX, frontChar, rearChar, filterModule,
                                                  filterColumn)

                if len(query['filters']) > 0:
                    try:
                        query['filters'].index(exp)
                        index = 0
                    except:
                        index = -1
                    if index == -1:
                        query['filters'].append(exp)
                else:
                    query['filters'].append(exp)

            query['derivedColumns'].append(query['selectedColumn'] + "_Derived")
            structure['saveSourceDetails'] = False
            if structure:
                status, sourceData = self.saveSourceDetails('dummy', structure, checkFileProperty=False,
                                                            deriveColumn=True)
            if isinstance(sourceData['data'],str):
                sourceData['data'] = eval(sourceData['data'])
            df = pandas.DataFrame(sourceData['data'])
            if df is not None:
                for expre in query['filters']:
                    exec (expre)

            columns = self.getColumnType(df)
            query['saveSourceDetails'] = True

            query['feedDetails'] = columns
            query['data'] = df
            if '_id' in query:
                del query['_id']
            query['data'] = query['data'].to_dict(orient='records')

            return True, query
        except Exception as e:
            logr.info(e)
            logr.info(traceback.format_exc())
            return False, 'Internal Error'

    def checkforSubstring(self, partVal, fullVal, column):
        expression = None
        if fullVal == partVal:
            expression = "df['" + column + "_Derived']=df['" + column + "']"
            return expression
        fullVal = fullVal.replace(partVal, '$' * len(partVal))
        derived = fullVal.replace(' ', '')
        occursatFirstPosition = False
        occursatLastPosition = False

        if derived.find('$') == 0:
            occursatFirstPosition = True
            if derived[derived.find('$') + len(partVal)].isalnum():
                expression = "df['" + column + "_Derived'] = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if derived.find('$') + len(partVal) == len(derived):
            occursatLastPosition = True
            if derived[len(derived) - len(partVal) - 1].isalnum():
                expression = "df['" + column + "_Derived'] = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if not occursatFirstPosition:
            if derived[derived.find('$') - 1].isalnum():
                expression = "df['" + column + "_Derived'] = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        if not occursatLastPosition:
            if derived[derived.find('$') + len(partVal)].isalnum():
                expression = "df['" + column + "_Derived'] = df['" + column + "'].fillna('').astype(str).str[" + str(
                    fullVal.find('$')) + ":" + str(fullVal.find('$') + len(partVal)) + "]"

        return expression

    def checkforSurrounding(self, fullVal, partVal, regX):
        substring1 = fullVal[:fullVal.find(partVal)]
        substring2 = fullVal[fullVal.find(partVal) + len(partVal):]
        index1, index2 = self.getNearestSplIdx(fullVal, partVal, substring1, substring2)
        frontVal = None
        rearVal = None
        frontChar = ''
        rearChar = ''

        if index1 is not None:
            frontspaces = fullVal[index1 + 1:fullVal.find(partVal)]
            frontVal = '(' + substring1[index1] + frontspaces
            frontChar = substring1[index1]

        if index2 is not None:
            backspaces = fullVal[fullVal.find(partVal) + len(partVal):fullVal.find(partVal) + len(partVal) + index2]
            rearVal = backspaces + substring2[index2] + ')'
            rearChar = substring2[index2]
        return frontVal, rearVal, frontChar, rearChar

    def getNearestSplIdx(self, fullVal, partVal, substring1, substring2):
        specialChar = {'/', ':', '-', '~', '_', '%'}
        indexList1 = []
        indexList2 = []
        for char in specialChar:
            if char in substring1:
                indexList1.append(max(self.findOccurrences(substring1, char)))
            if char in substring2:
                indexList2.append(min(self.findOccurrences(substring2, char)))

        # logr.infoindexList1, indexList2

        if indexList1 and indexList2:
            return max(indexList1), min(indexList2)

        elif indexList1 or indexList2:
            if indexList1:
                return max(indexList1), None
            else:
                return None, min(indexList2)
        else:
            return None, None

    def findOccurrences(self, s, ch):
        return [i for i, letter in enumerate(s) if letter == ch]


#  Recon Meta Info
class ReconMetaInfo(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconMetaInfo, self).__init__("reconMetaInfo", hideFields)

    def create(self, doc):
        (status, reconMeta) = super(ReconMetaInfo, self).create(doc)
        return status, reconMeta


# Recon Processes information collection
class ReconProcess(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconProcess, self).__init__("reconProcessDetails", hideFields)

    def create(self, doc):
        (status, reconProcessDetails) = super(ReconProcess, self).create(doc)
        return status, reconProcessDetails

    def saveProcessDetail(self, id, query):
        doc = ReconProcess().find_one({'reconProcess': query['reconProcess']})
        if doc:
            doc['reconProcess'] = query['reconProcess']
            if 'imagePath' in query.keys():
                doc['imagePath'] = query['imagePath']
            if 'isLicensed' in query.keys():
                doc['isLicensed'] = query['isLicensed']
            status, data = ReconProcess().modify(doc['_id'], doc)
            return True, 'Process updated successfully'
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        query['isLicensed'] = False
        doc = ReconProcess().save(query)
        return True, 'Uploaded'

    def getJobProcess(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            doc['statementDate'] = doc['statementDate'].strftime("%d%m%Y")
        return True, doc


#  Recon Assigned Collection
class ReconAssignment(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def create(self, doc):
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def deleteData(self, condition):
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        return super(ReconAssignment, self).delete(condition)

    # Get Assigned Recons For logged In User
    def assignedRecons(self, id, query):
        status, assignData = ReconAssignment().getAll({'condition': {'userName': query['userName']}})
        if status and assignData['total'] == 0:
            return False, 'No Recon Assignment For this User'
        status, reconsData = Recons().getAll({})
        reconsList = {}
        assignedRecons = []
        for doc in assignData['data']:
            for recon in reconsData['data']:
                if recon['reconProcess'] in doc['reconProcesses']:
                    reconsList[recon['reconProcess']] = [] if recon['reconProcess'] not in reconsList.keys() else \
                        reconsList[recon['reconProcess']]
                    if recon['reconName'] in doc['reconNames']:
                        reconsList[recon['reconProcess']].append(recon['reconName'])
                        assignedRecons.append(recon['reconName'])
        detailsLog = []
        for i in assignedRecons:
            stat, logDetails = ReconExecDetailsLog().getLatestExeDetails(i)
            if logDetails:
                for process in reconsList:
                    if i in reconsList[process]:
                        logDetails['reconProcess'] = process
                        logDetails['statementDate'] = datetime.datetime.strftime(logDetails['statementDate'],
                                                                                 '%Y-%m-%d')
                detailsLog.append(logDetails)
            else:
                doc = {'reconName': i}
                for process in reconsList:
                    if i in reconsList[process]:
                        doc['reconProcess'] = process
                detailsLog.append(doc)
        data = {'total': len(detailsLog), 'reconsLog': detailsLog, 'data': reconsList}
        return True, data

    def getReconDetails(self):
        recondata = list(Recons().find({}))
        recondetails = []
        if len(recondata):
            reconAssign = ReconAssignment()
            users = []
            users.append(flask.session["sessionData"]["userName"])
            assignData = list(reconAssign.find({'users': {"$in": users}}))

            if assignData:
                for data in recondata:
                    for recon in assignData:
                        if data['reconName'] in recon['reconNames']:
                            recondetails.append(data)
        return True, recondetails

    def getUsers(self, id, query):
        usernames = []
        roles = hashframe.config.roles
        reconAssignments = list(ReconAssignment().find(
            {'groupRole': roles[query['role']], 'reconNames': {'$in': query['recons']}}))

        for i in reconAssignments:
            usernames.extend(i['users'])
        users = list(Users().find({"role": {'$ne': 'admin'}, 'ApprovalStatus': 'Approved'}))
        allUsers = [user['userName'] for user in users]
        availableUsers = list(set(allUsers) - set(usernames))
        return True, availableUsers


# reorder Column Collection
class ColumnConfiguration(hashframe.MongoCollection):
    def __init__(self):
        super(ColumnConfiguration, self).__init__('columnConfiguration')

    def create(self, doc):
        (status, columnConfiguration) = super(ColumnConfiguration, self).create(doc)
        return status, columnConfiguration

    def removeSourceColumns(self,id,query):
        status,doc=ColumnConfiguration().get(query)
        if status:
            ColumnConfiguration().remove(query)
            return True,'Columns Reordering Removed Successfully'
        return False,'Cannot delete the reordering columns'
# recon Summary Collection
class ReconSummary(hashframe.MongoCollection):
    def __init__(self):
        super(ReconSummary, self).__init__('recon_summary')

    def create(self, doc):
        (status, reconSummary) = super(ReconSummary, self).create(doc)
        return status, reconSummary

    def getSummary(self, query):
        doc = ReconSummary().find_one(query)
        return True, doc


# Recon Details Collections
class ReconDetails(hashframe.MongoCollection):
    def __init__(self):
        super(ReconDetails, self).__init__("recon_details")

    def create(self, doc):
        (status, recon_details) = super(ReconDetails, self).create(doc)
        return status, recon_details

    def getDashboardSummary(self, id):
        status, doc = ReconAssignment().getReconDetails()
        if status:
            total = len(doc)
            ExecLog = []
            data = {'reconsExceuted': 0, 'successCount': 0, 'failureCount': 0}
            for recon in doc:
                if recon['reconProcess'] not in data.keys():
                    # data[recon['reconProcess']] = []
                    data['executedReconsList'] = []
                    data[recon['reconProcess']] = []
                data[recon['reconProcess']].append(recon['reconName'])
                execlog = list(ReconExecDetailsLog().getMaxReconExecutionDoc([recon['reconName']]))

                # execlog = ReconExecDetailsLog().find_one(
                #     {"reconName": recon['reconName']},
                #     sort=[('RECON_EXECUTION_ID', -1)])
                if execlog:
                    execlog = execlog[0]
                    data['reconsExceuted'] += 1
                    data['executedReconsList'].append(recon['reconName'])
                    if execlog['jobStatus'] == 'Success':
                        data['successCount'] += 1
                    else:
                        data['failureCount'] += 1
                det = list(ReconExecDetailsLog().getMaxReconExecutionDoc([recon['reconName']]))

                if det is not None and len(det)>0:
                    det = det[0]
                    det['reconProcess'] = recon['reconProcess']
                    ExecLog.append(det)
            if len(ExecLog):
                data['msg'] = 'reconExecution details'
                data['total'] = len(ExecLog)
                for i in ExecLog:
                    i['statementDate'] = datetime.datetime.strftime(i['statementDate'], '%Y-%m-%d')
            # else:
            #     return True, "There is no execution details"
            data['totalAssignedRecons'] = total
            data['reconDeatails'] = doc
            data['reconExecutionDetailsLog'] = ExecLog
            return True, data
        else:
            return False, 'No Recons assigned for user'

    def exportAllCsv(self, id, query):
        data = {}
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            fileName = filePath + '/' + query['reportPath']
            reportPath = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/' + query['reportPath']
            copyfile(reportPath, fileName)
            fileName = flask.session["sessionData"]["userId"] + '/' + query['reportPath']
            return True, fileName
        return False, 'No Execution Details Available'

    def getOlfFormatReport(self, id, query):
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            path = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/'
            outfiles = os.listdir(path)
            columnList=[]
            alldf = pandas.DataFrame()
            doc=list(ColumnConfiguration().find({"reconName":query['reconName']}))
            # print doc
            if status:
                for source in doc:
                    for file in [source['source']+'.csv',source['source']+'_UnMatched.csv',source['source']+'_Self_Matched']:
                        if  file in os.listdir(path):
                            status, converters = ReconDetails().getConverters(query['reconName'], source['source'])
                            # converters['System_Idx'] = str
                            logr.info(path+file)
                            df = pandas.read_csv(path + file)
                            for key,val in converters.iteritems():
                                df[key]=df[key].astype(val)
                            if df.empty:
                                continue
                            status, doc = ColumnConfiguration().get({"reconName": query['reconName'],"source":source})
                            if status:
                                columnList.extend(doc['columns'])
                                seen = set()
                                columnList=[x for x in columnList if not (x in seen or seen.add(x))]

                            # matchfil = ['(df["%s"].fillna("") == "MATCHED")' % col for col in df.columns if
                            #             "Match" in col and 'Self Matched' not in col]
                            # matchfil = "&".join(matchfil)
                            # if 'Self Matched' in df.columns:
                            #     matchfil = matchfil + '|(df["Self Matched"].fillna("")=="MATCHED")'
                            # matchfil = eval(matchfil)

                            if len(df)>0:
                                # logr.info(len(df))
                                if query['type'] == 'Matched':
                                    df['MATCHING_STATUS'] = 'MATCHED'
                                elif query['type'] == 'UnMatched':
                                    df['MATCHING_STATUS'] = 'UNMATCHED'

                                # df.loc[matchfil, 'MATCHING_STATUS'] = 'MATCHED'
                                # df.loc[df['MATCHING_STATUS'].fillna('') == '', 'MATCHING_STATUS'] = 'UNMATCHED'
                                # df=df[keyColumns+['MATCHED']]
                                # if alldf.empty:
                                #     alldf = pandas.concat([df, alldf], join_axes=[df.columns])
                                # else:
                                #     alldf = pandas.concat([df, alldf], join_axes=[alldf.columns])
                                alldf=alldf.append(df)
                                logr.info(alldf.head())
                                if len(columnList)>0:
                                    alldf=alldf[columnList+['MATCHING_STATUS']]
                                alldf = alldf.reset_index(drop=True)
                # logr.info(alldf['MATCHING_STATUS'].unique())
                # if query['type']=='Matched':
                #     alldf=alldf[alldf['MATCHING_STATUS']=='MATCHED']
                # else:
                #     alldf=alldf[alldf['MATCHING_STATUS']=='UNMATCHED']


            for col in alldf.columns.tolist():
                alldf[col] = alldf[col].fillna(ReconDetails().infer_dtypes(alldf[col]))

            alldf=alldf.replace('nan','')
            alldf.rename(columns={'SOURCE':"SOURCE_NAME"},inplace=True)

            # if query['reconName'] in hashframe.config.reconMapper.keys():
            #     infoJson=hashframe.config.reconMapper[query['reconName']]
            #     if 'expressions' in infoJson:
            #         for exp in infoJson['expressions']:
            #             exec(exp)
            #
            #     if 'sortOn' in infoJson:
            #         alldf=alldf.sort_values(by=infoJson['sortOn'])
            #     if 'apostrophe' in infoJson:
            #         for col in infoJson['apostrophe']:
            #             alldf[col]="'"+alldf[col].astype(str)

            fileName = query['reconName'] + '_' + query['type'] + '_' + str(
                log['statementDate']) + '.csv'
            alldf.to_csv(filePath + os.sep + fileName, index=False)
            return True, flask.session["sessionData"]["userId"] + '/' + fileName
        else:
            return False, 'No Execution Details Available'

    def getReportDetails(self, id, query):
        details = ReconDetails().find_one({'reconName': query['reconName']})
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
            summaryQuery = {'reconName': query['reconName'], 'statementDate': doc['statementDate'],
                            'reconExecutionId': doc['reconExecutionId']}
            status, summary = ReconSummary().getSummary(summaryQuery)
            if summary:
                summary['sources'] = []
                summary['matchedCount'] = []
                summary['unMatchedCount'] = []
                sourceGroup = {}
                sourceGroup['defaultReport'] = {}
                for report in details['reportDetails']:
                    # report['source'] = str(report['source']).replace(' ', '_')
                    # if report['source'] not in summary['sources']:
                    #     summary['sources'].append(report['source'])
                    #     summary['matchedCount'].append()
                    for i in summary['details']:
                        if i['Source'] == report['source']:
                            if report['source'] not in summary['sources']:
                                summary['sources'].append(report['source'])
                                summary['matchedCount'].append(i['Matched Count'])
                                summary['unMatchedCount'].append(i['UnMatched Count'])
                            # i['Execution Date Time'] = datetime.datetime.strptime(i['Execution Date Time'],"%d-%m-%Y")
                            #           if type(i['statementDate']) == datetime.datetime:
                            #               i['statementDate'] = datetime.datetime.strftime(i['Statement Date'], "%d-%m-%Y")
                            #               i['Execution Date Time'] = i['Execution Date Time'].strftime("%d-%m-%Y")
                            if report['reportName'] == 'Unmatched':
                                i['unMatchedReport'] = report['reportPath']
                            elif report['reportName'] == 'Matched':
                                i['matchedReport'] = report['reportPath']
                            elif report['reportName'] == report['source'] + ' ' + 'Self Match':
                                i['reversalReport'] = report['reportPath']

                    if report['source'] == 'Reports':
                        if 'customReports' not in summary:
                            summary['customReports'] = []
                        summary['customReports'].append(
                            {"name": report['reportName'], 'reportPath': report['reportPath']})

                summary['statementDate'] = summary['statementDate'].strftime("%d-%m-%Y")
            else:
                return False, 'No summary Found'

        else:
            return False, 'No execution found for this recon'
        return True, summary

    #  Filtered Data with advanced Search
    def getFilteredData(self, id, query):
        records = {}
        df = pandas.DataFrame()
        if 'stmtDate' in query.keys() and query['stmtDate']:
            stmtDate = datetime.datetime.strptime(str(query['stmtDate']), '%d/%m/%y').strftime("%d%m%Y")

        else:
            status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], query['source'])

        path = hashframe.config.mftPath + str(query['reconName']) + '/' + stmtDate + '/' + 'OUTPUT/' + str(
            query['reportPath'])
        if not os.path.exists(path):
            return False, "File Path Doesn't Exist"
        df = pandas.read_csv(path)

        if 'filters' in query.keys() and query['filters']:
            for expression in query['filters']:
                exec expression

        index = query['index']
        pageSize = query['pageSize']
        length = len(df)

        maxPages = ((length / pageSize) + 1)
        skip = (index * pageSize) - pageSize

        limit = (index * pageSize)
        if limit < 0:
            limit = len(df) + 1

        df = df[skip:limit]

        for col in df.columns:
            df[col] = df[col].fillna(self.infer_dtypes(df[col]))
        columnGrouper = self.columnDTypeGrouper(converters)

        records['data'] = df.to_dict(orient='records')
        records['maxPages'] = maxPages
        records['columns'] = [col for col in df.columns]
        records['reconName'] = query['reconName']
        records['report'] = query['reportPath']
        records['columnGrouper'] = columnGrouper
        return True, records

    # get Authorize Recoords
    def getAuthourizedRecords(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/'
        details = Recons().find_one({'reconName': query['reconName']})
        sources = details['sources']
        totalDf = pandas.DataFrame()
        for source in sources:
            sourcePath = path + source + '_Authorization.csv'
            if (os.path.exists(sourcePath)):
                df = pandas.read_csv(sourcePath)
                totalDf.append(df)
        if len(totalDf) > 0:
            return True, totalDf
        else:
            return False, 'No Data Found'

    #  get Reports Data With Pagination
    def getReportData(self, id, query):
        records = {}
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], query['source'])
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query['reportPath']
        print(path)
        if (os.path.exists(path)):
            index = query['index']
            pageSize = query['pageSize']
            length = int(subprocess.check_output("wc -l " + path.replace(' ', '\ '), shell=True).split()[0])
            maxPages = ((length / pageSize) + 1)

            skip = (index * pageSize) - pageSize
            limit = length - (index * pageSize)
            if maxPages:
                records['maxPages'] = maxPages
            # initialize headers
            with open(hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                'reportPath'], 'r') as file:
                reader = csv.reader(file)
                header = reader.next()
                df = pandas.read_csv(
                    hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                        'reportPath'],
                    skiprows=skip + 1, skipfooter=0 if limit < 0 else limit, names=header,
                    header=None,
                    engine='python')

            for col in df.columns:
                df[col] = df[col].fillna(self.infer_dtypes(df[col]))
            columnGrouper = self.columnDTypeGrouper(converters)

            if query['carryForward']:
                df['CARRY_FORWARD'] = df['CARRY_FORWARD'].astype(str)
                df = df[df['CARRY_FORWARD'] == 'Y']
            else:
                if 'CARRY_FORWARD' in df.columns and ('UnMatched' in query['reportPath'] or 'Matched' in query['reportPath']):
                    df['CARRY_FORWARD'] = df['CARRY_FORWARD'].astype(str)
                    df = df[df['CARRY_FORWARD'] != 'Y']

            status,columnsDoc=ColumnConfiguration().get({'reconName':query['reconName'],'source':query['source']})
            if status:
                df=df[columnsDoc['columns']]
            records['data'] = df.to_dict(orient='records')
            records['columns'] = [col for col in df.columns]
            records['reconName'] = query['reconName']
            records['report'] = query['reportPath']
            records['columnGrouper'] = columnGrouper
        else:
            return False, "No Files are there"
        return True, records

    def getConverters(self, reconName, source):
        doc = ColumnDataTypes().getColumnDataTypes(reconName, source)
        converters = {}
        if doc:
            for val in doc['dtypes']:
                if str(val['dtype']) != 'np.datetime64':
                    converters[val['column']] = self.typeMapper(val['dtype'])
                else:
                    converters[val['column']] = str
        else:
            logr.info('-------------------------')
            logr.info('No Column Dtypes Found')
            return False, 'No Column Dtypes Found'
        return True, converters

    # Download CustomReports
    def downloadCustomReport(self, id, query):
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if status:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            filepath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filepath):
                os.mkdir(filepath)
            if not query.get('writeToMongo'):
                path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                       query['report'][
                           'reportPath']
                if not os.path.exists(path):
                    return True, 'Report file not found'
                copyfile(path, filepath + '/' + query['report']['reportPath'])
            else:
                pass
            return True, flask.session["sessionData"]["userId"] + '/' + query['report']['reportPath']

    def columnDTypeGrouper(self, converters):
        columnGrouper = {"string": [], "numeric": []}
        for key, val in converters.iteritems():
            if val == str:
                columnGrouper["string"].append(key)
            elif val == (np.float64 or np.int64):
                columnGrouper["numeric"].append(key)
        return columnGrouper

    def typeMapper(self, dtype):
        if dtype == 'str':
            return str
        elif dtype == 'np.float64':
            return np.float64

        elif dtype == 'np.int64':
            return np.int64
        else:
            return np.datetime64

    def infer_dtypes(self, series):
        if is_float_dtype(series):
            return 0.0
        elif is_integer_dtype(series):
            return 0
        elif is_string_dtype(series):
            return ''
        elif is_datetime64_dtype(series):
            return pandas.NaT
        else:
            np.nan

    def updateReportName(self, id, query):
        report = query['report']
        data = ReconDetails().update(
            {'reconName': query['selectedRecon'], 'reportDetails.reportPath': report['reportPath']},
            {'$set': {'reportDetails.$.reportName': report['reportName']}})
        return True, data

    def getReports(self, id, query):
        reportData = ReconDetails().find_one({'reconName': query['reconName']})
        return True, reportData



    def getHistoricalData(self, id, query):
        user = flask.session['sessionData']['userName']
        password = flask.session['sessionData']['password']
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, password)
        decrypt = decrypt.replace('/<=%+,?->/', '')
        s = requests.Session()
        a = s.post('https://erecon.com/api/login',
                   data=json.dumps({"userName": user,
                                    "password": decrypt}),
                   headers={"Content-Type": "application/json"}, verify=False)
        reconMapper = hashframe.config.reconMapper
        if a.status_code == 200:
            doc = {"accountNumber": "",
                   "executionEndDate": 0,
                   "executionStartDate": 0,
                   "exportData": True,
                   "recon_id": reconMapper[query['recon_name']]['reconId'],
                   "recon_name": reconMapper[query['recon_name']]['reconName'],
                   "statementEndDate": query['statementEndDate'],
                   "statementStartDate": query['statementStartDate'],
                   }
            headers = {'Content-Type': 'application/json'}
            data = {'query': doc}
            result = s.post('https://erecon.com/api/recon_assignment/dummy/getReconMatchedStatementDate',
                            headers=headers, data=json.dumps(data))
            result = json.loads(str(result.text))
            fileName = ''
            print result
            if result['status'] == 'ok' and result['msg']['txn_count'] > 0:
                fileName = result['msg']['fileName']
                oid = fileName.split('/')[0]
                downloadedfileName = '\ '.join(fileName.split('/')[1].split(' '))
                completeFileName = '/usr/share/nginx/www/erecon/ui/app/files/recon_reports/' + oid + '/' + downloadedfileName
                filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
                if not os.path.exists(filePath):
                    os.mkdir(filePath)
                cmd = 'mv ' + completeFileName + ' ' + filePath
                os.system(cmd)
                fileName = flask.session["sessionData"]["userId"] + '/' + fileName.split('/')[-1]

        return True,fileName

#  Force Match Operations
class ForceMatch(hashframe.MongoCollection):
    def __init__(self):
        super(ForceMatch, self).__init__("forceMatch_default_config")

    def create(self, doc):
        (status, forceMatch_default_config) = super(ForceMatch, self).create(doc)
        return status, forceMatch_default_config

    #  Pimary source Data while force Match
    def readReportData(self, id, query):
        data = {}
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if execLog:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            if query['source']:
                columns = []
                columns = [str(i['column']) for i in query['dtypes']]
                converter = {i['column']: eval(i['dtype']) for i in query['dtypes']}
                converter['System_Idx'] = str
                file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                    'source'] + '_UnMatched.csv'

                df = pandas.read_csv(file_name, sep=',', usecols=columns, converters=converter).fillna('')
                data['primaryCol'] = df.columns.tolist()
                data['primaryData'] = df.to_dict(orient='records')
                data['total'] = len(df)
        return True, data

    def readSourceData(self, id, query):
        logr.info("Reading All Source Data For Force Match")
        allSourceData = pandas.DataFrame()
        matchingColumns = set()
        orderOfCols = []
        reconDetails = Recons().find_one({'reconName': query['reconName']})

        try:
            status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
            if execLog:
                logr.info("Execution Details Foucd For the statement Data:%s" % execLog['statementDate'])
                stmtDate = execLog['statementDate'].strftime("%d%m%Y")
                for source in query['sources']:
                    for struct, keyCols in reconDetails['sources'][source].items():
                        cols = [col['keyCols'] for col in keyCols['keyColumns']]
                        matchingColumns.update(cols)
                    status, converters = ReconDetails().getConverters(query['reconName'], source)
                    converters['System_Idx'] = str
                    logr.info(converters)
                    if query['operation'] == 'rollBack':
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_ForceMatched.csv'
                    elif query['operation'] == 'forceMatch':
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_UnMatched.csv'

                    if not os.path.exists(file_name):
                        logr.info("%s file Not Found" % file_name)
                        continue
                        # return False, 'File Not Found'
                    df = pandas.read_csv(file_name, converters={'System_Idx': str}, sep=',', index_col=None).fillna('')
                    allSourceData = pandas.concat([allSourceData, df], ignore_index=True)

                # Getting Key column first in dataframe to view in UI
                allSourceData.fillna('', inplace=True)
                columns = allSourceData.columns.tolist()
                commonCols = list(set(columns) & (matchingColumns))
                nonCommonCols = list(set(columns) - (matchingColumns))
                commonCols.extend(nonCommonCols)
                allSourceData = allSourceData[commonCols]

                data = {}
                data['columns'] = allSourceData.columns.tolist()
                data['allSourceData'] = allSourceData.to_dict(orient='records')
                data['total'] = len(allSourceData)
                logr.info("Reading Data is Done")
                return True, data
            else:
                return False, "No Execution Details Present"
        except Exception as e:
            logr.info(traceback.format_exc())
            return False, "Internal Error"

    #  Matched Data of secondary source while force Match
    def readMatchData(self, id, query):
        data = {}
        if 'matchSource' in query.keys():
            matchSource = query['matchSource']
        primaryColList = query['primaryMatchColumns']
        matchColList = [i['column'] for i in query['secondaryMatchColumns']]
        converter = {i['column']: eval(i['dtype']) for i in query['secondaryMatchColumns']}
        converter['System_Idx'] = str
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if execLog:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            primaryDf = pandas.DataFrame(query['selectedRows'])

            if matchSource:
                file_name = hashframe.config.mftPath + query[
                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + matchSource + '_UnMatched.csv'

            matchdf = pandas.read_csv(file_name, usecols=matchColList, converters=converter).fillna('')

            for col in matchdf.columns:
                matchdf[col] = matchdf[col].fillna(ReconDetails().infer_dtypes(matchdf[col]))

            matched = pandas.DataFrame()
            for col in primaryColList:
                # matched = matchdf.loc[:, matchColList].isin(primaryDf[col].tolist())
                for col1 in matchColList:
                    for i in primaryDf[col].tolist():
                        if matchdf[col1].dtype == str:
                            matched.append(matchdf[matchdf[col1].apply(lambda x: x.str.contains(i))])

                # matchdf = matchdf[matched.values == True]

            data['total'] = len(matchdf)
            data['secondary'] = matchSource
            data['matchCols'] = matchdf.columns.tolist()
            data['matchSourceData'] = matchdf.to_dict(orient='records')
            return True, data

    # Function To Get Recon_details
    def getForceMatchReconDetails(self, reconName):
        details = Recons().find_one({'reconName': reconName})
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(reconName)
        if status:
            return True, details, execLog
        else:
            return False, details, 'No Data Found'

    # Function To Re Compute Recon Summary After Doing Force Match
    def recomputeReconSummary(self, reconName, execLog, forceMatchSources, authorization=False, sendToAuth=False,
                              rollBack=False):
        try:
            logr.info('===============Recompute Recon Summary is started===============')
            status, reconDetails, execLog = self.getForceMatchReconDetails(reconName)
            details = ReconDetails().find_one({'reconName': reconName})
            if status:
                stmtDate = execLog['statementDate'].strftime("%d%m%Y")
                logr.info('Statement Date in RecomputeSummary' + stmtDate)
                summary = ReconSummary().find_one({'statementDate': execLog['statementDate'], 'reconName': reconName,'reconExecutionId': execLog['reconExecutionId']})
                logr.info('Summary Found For' + summary['reconName'])
                logr.info('source participated in ForceMatch ' + ",".join(forceMatchSources))
                for report in details['reportDetails']:
                    if report['reportName'] == 'Unmatched' and report['source'] in forceMatchSources:
                        logr.info('Looping Source For Recomput of summary ' + report['source'])
                        for struct, cols in reconDetails['sources'][report['source']].iteritems():
                            aggrCols = [col['keyCols'] for col in cols['keyColumns'] if col['DataType'] == 'float']
                            logr.info('Looping source aggregation Columns ' + ",".join(aggrCols))
                            converter = {col: eval('np.float64') for col in aggrCols}
                            authName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + report[
                                'source'] + '_Authorization.csv'
                            forceName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                        report[
                                            'source'] + '_ForceMatched.csv'

                            unMatchName = hashframe.config.mftPath + reconName + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                          report[
                                              'source'] + '_UnMatched.csv'

                            logr.info('Authorization Path for source ' + authName)
                            logr.info('ForceMatchFile Path for source ' + forceName)
                            authLength = 0
                            forceLength = 0
                            if os.path.exists(authName):
                                logr.info('Authorization File exist')
                                auth = pandas.read_csv(authName, converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                authLength = len(auth)
                                cfd = 0
                            else:
                                auth = pandas.DataFrame()

                            if os.path.exists(forceName):
                                logr.info('Authorization is on progress and ForceMatch File Exits')
                                force = pandas.read_csv(forceName, converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                forceLength = len(force)

                            unMatchLen = 0
                            if os.path.exists(unMatchName):
                                unMatch = pandas.read_csv(unMatchName,
                                                          converters={'SystemIdx': str, 'CARRY_FORWARD': str})
                                unMatchLen = len(unMatch)

                            for det in summary['details']:
                                if det['Source'] == report['source']:
                                    det['Authorization_Count'] = authLength
                                    # det['Authorization_Amount'] = auth[aggrCols].sum()[0] if authLength > 0 else 0
                                    # logr.info('Authorization Records Count '+authLength)
                                    if sendToAuth and not rollBack:
                                        if 'CARRY_FORWARD' in auth.columns:
                                            logr.info(auth['CARRY_FORWARD'])
                                            cfd = len(auth[auth['CARRY_FORWARD'] == 'Y'])
                                            det['Carry-Forward UnMatched Count'] = len(unMatch[unMatch['CARRY_FORWARD'] == 'Y'])
                                            # det['Carry-Forward UnMatched Amount'] = det[
                                            #                                             'Carry-Forward UnMatched Amount'] - (
                                            #                                             auth[auth[
                                            #                                                      'CARRY_FORWARD'] == 'Y'][
                                            #                                                 aggrCols].sum()[0])

                                        det['UnMatched Count'] = unMatchLen
                                        # det['UnMatched Amount'] = det['UnMatched Amount'] - (auth[aggrCols].sum()[0])

                                    det['Force_Matched_count'] = forceLength
                                    # det['Force_Matched_Amount'] = force[aggrCols].sum()[0] if forceLength > 0 else 0
                                    logr.info('Force Match Records Count ' + str(forceLength))

                                    if rollBack:
                                        if 'CARRY_FORWARD' in auth.columns:
                                            cfd = len(unMatch[unMatch['CARRY_FORWARD'] == 'Y'])
                                            det['Carry-Forward UnMatched Count'] = cfd
                                            # det['Carry-Forward UnMatched Amount'] = \
                                            #     unMatch[unMatch['CARRY_FORWARD'] == 'Y'][aggrCols].sum()[
                                            #         0] if unMatchLen > 0 else det['Carry-Forward UnMatched Amount']
                                        #
                                        det['UnMatched Count'] = unMatchLen
                                        # det['UnMatched Amount'] = (unMatch[aggrCols].sum()[0]) if unMatchLen > 0 else \
                                        #     det['UnMatched Amount']

                from bson import ObjectId
                a = ReconSummary().update({'_id': ObjectId(summary['_id'])}, {'$set': {'details': summary['details']}})
                logr.info('===============Reconmpute Recon summary SuccessFull===============')
                return True, ''
            else:
                logr.info('No execution Found for recon')
                return status, execLog
        except Exception as e:
            return False, ''
            logr.info('===============Recompute Recon Summary Failed with error===============')
            logr.info(traceback.format_exc())

    #  ForceMatch of selected Transaction
    def forceMatchReconData(self, id, query):
        logr.info("================" + query['operation'] + " Record Started==============")
        data = {}
        orginalSourceData = {}
        orginalAuthData = {}
        totalDF = pandas.DataFrame(query['forceMatchData'])
        sources = totalDF['SOURCE'].unique().tolist()

        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        link_id = str(uuid.uuid4().int & (1 << 64) - 1)
        for source in sources:
            sourceDF = totalDF.loc[totalDF['SOURCE'] == source]
            if not sourceDF.empty:
                try:
                    if query['operation'] == 'rollBack':
                        rollBack = True
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_ForceMatched.csv'

                    elif query['operation'] == 'forceMatch':
                        rollBack = False
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_UnMatched.csv'

                    dtypes = ColumnDataTypes().find_one({'reconName': query['reconName'], 'source': source})
                    columnsDtypes = {
                        col['column']: eval(col['dtype']) if col['dtype'] != 'np.datetime64' else col[
                                                                                                      'dtype'] != 'np.datetime64'
                        for col in dtypes['dtypes']}
                    columnsDtypes['System_Idx'] = str

                    for col, datatype in columnsDtypes.iteritems():
                        if 'CarryForward' in col or col == 'LINK_ID':
                            columnsDtypes[col] = str

                    # logr.info(columnsDtypes)
                    logr.info(file_name)

                    sourceAllData = pandas.read_csv(file_name, converters=columnsDtypes)

                    # Preserve data if any exception comes
                    orginalSourceData[source] = {'data': sourceAllData, 'fileName': file_name}

                    primarDFSysInd = sourceDF['System_Idx'].tolist()

                    sourceDF['System_Idx'] = sourceDF['System_Idx'].astype(str)
                    logr.info(sourceDF['System_Idx'])
                    concatmatchdf = pandas.concat([sourceAllData, sourceDF], join_axes=[sourceAllData.columns])
                    for col in concatmatchdf.columns:
                        concatmatchdf[col] = concatmatchdf[col].fillna(ReconDetails().infer_dtypes(concatmatchdf[col]))
                    nonduplicated = concatmatchdf[~concatmatchdf.duplicated(subset=['System_Idx'], keep=False)]
                    nonduplicated.to_csv(file_name, index=False)

                    Authorization = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                    source + '_Authorization.csv'

                    logr.info('%s Authorization File Path %s' % (source, Authorization))

                    authorizedRecords = sourceAllData.loc[sourceAllData['System_Idx'].isin(primarDFSysInd)]
                    logr.info("Number of Records Found in %s :%s " % (source, str(len(authorizedRecords))))

                    authorizedRecords['ForceMatchRemarks'] = query['comments']

                    if query['operation'] == 'forceMatch':
                        authorizedRecords['AUTHORIZATION_STATUS'] = 'FORCE MATCH AUTHOURIZATION'
                    elif query['operation'] == 'rollBack':
                        authorizedRecords['AUTHORIZATION_STATUS'] = 'ROLL BACK AUTHOURIZATION'

                    authorizedRecords['LINK_ID'] = link_id
                    authorizedRecords['UserID'] = flask.session["sessionData"]["userName"]
                    if os.path.exists(Authorization):
                        authorizationDf = pandas.read_csv(Authorization)
                        orginalAuthData[source] = {'data': authorizationDf, 'fileName': Authorization}
                        logr.info('Number of Records Exits in authorization ' + str(len(authorizationDf)))
                        authorizedRecords = pandas.concat([authorizationDf, authorizedRecords],
                                                          join_axes=[authorizationDf.columns])
                    authorizedRecords.to_csv(
                        hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + source + '_Authorization.csv',
                        index=False)
                except Exception as e:
                    for key, val in orginalSourceData.iteritems():
                        val['data'].to_csv(val['fileName'], index=False)

                    for key, val in orginalAuthData.iteritems():
                        val['data'].to_csv(val['fileName'], index=False)

                    logr.info("Exception Occured During ForceMatch")
                    logr.info(traceback.format_exc())
                    return False, 'Exception Occured'

            doc = {}
            doc['reconName'] = query['reconName']
            # doc['reconProcess'] = query['reconProcess']
            doc['user'] = flask.session['sessionData']['userName']
            doc['dateTime'] = int(datetime.datetime.now().strftime("%s")) * 1000
            doc['operation'] = 'ForceMatch Records'
            # forceSummary = {query['primarySource']: len(primaryDF), query['matchSource']: len(matchDF)}
        status, msg = self.recomputeReconSummary(query['reconName'], execLog,
                                                 sources,
                                                 sendToAuth=True, rollBack=rollBack)
        if not status:
            for key, val in orginalSourceData.iteritems():
                val['data'].to_csv(val['fileName'], index=False)

            for key, val in orginalAuthData.iteritems():
                val['data'].to_csv(val['fileName'], index=False)

        # ForceMatch().insert(doc)
        return True, data

    # Getting wiating For Authorization Data
    def getWaitingForAuthorization(self, id, query):
        logr.info('===============Getting waiting for authorization Records===============')
        status, details, execLog = self.getForceMatchReconDetails(query['reconName'])

        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        sources = details['sources'].keys()
        authorizeRecords = pandas.DataFrame()
        for source in sources:
            file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                        source + '_Authorization.csv'
            if os.path.exists(file_name):
                status, converters = ReconDetails().getConverters(query['reconName'], source)
                converters['System_Idx'] = str
                df = pandas.read_csv(file_name,converters={'System_Idx':str})
                authorizeRecords = pandas.concat([authorizeRecords, df], ignore_index=True).fillna('')
        data = {}
        if len(authorizeRecords) <= 0:
            data['total'] = 0
            data['msg'] = 'No Data Found For Authorization'
            return True, data
        data['total'] = len(authorizeRecords)
        data['data'] = authorizeRecords.to_dict(orient='records')
        return True, data

    #  Authorize Records
    def authorizeRecord(self, id, query):
        logr.info('===================Authorization Process Started===================')
        selectedRows = pandas.DataFrame(query['selectedRows'])
        status, details, execLog = self.getForceMatchReconDetails(query['reconName'])
        stmtDate = execLog['statementDate'].strftime("%d%m%Y")
        # sources = details['sources'].keys()
        orginalSelectedData = {}
        for authStatus in ['FORCE MATCH AUTHOURIZATION', 'ROLL BACK AUTHOURIZATION']:
            authData = selectedRows[selectedRows['AUTHORIZATION_STATUS'] == authStatus]
            if not authData.empty:
                logr.info('Number of Rows selected for authorization ' + str(len(authData)))
                rowsSystemInx = authData['System_Idx'].tolist()
                sources = authData['SOURCE'].unique().tolist()
                logr.info(rowsSystemInx)
                # logr.info('Row System Index of selected Rows ' + ",".join(rowsSystemInx))
                matched = pandas.DataFrame()
                authorizatinSource = []
                for source in sources:
                    try:
                        file_name = hashframe.config.mftPath + query[
                            'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                    source + '_Authorization.csv'
                        converters = {}
                        if (os.path.exists(file_name)):
                            logr.info('Authourization File exits ' + file_name)
                            # status,converters = ReconDetails().getConverters(query['reconName'], source)
                            converters['System_Idx'] = str
                            df = pandas.read_csv(file_name, converters=converters)

                            if file_name not in orginalSelectedData:
                                orginalSelectedData[source] = {'data': df, 'fileName': file_name}

                            matched = df[df['System_Idx'].isin(rowsSystemInx)]
                            if (len(matched) > 0):
                                logr.info("Number Of Records matched to authorize " + str(len(matched)))
                                matched['AUTHOURIZED_BY'] = flask.session["sessionData"]["userName"]
                                authorizatinSource.append(source)
                            df = df[~df['System_Idx'].isin(rowsSystemInx)]

                            if len(df) > 0:
                                df.to_csv(file_name, index=False)
                            else:
                                logr.info('All authorization pending records authorized')
                                os.remove(file_name)

                            # File Name after authorizing forceMatched records
                            if authStatus == 'FORCE MATCH AUTHOURIZATION' and query['operation'] != 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'FORCE_MATCHED'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_ForceMatched.csv'

                            # File Name after authorizing rollBack records which are force matched
                            elif authStatus == 'ROLL BACK AUTHOURIZATION' and query['operation'] != 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'ROLL BACK'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_UnMatched.csv'

                            # File Name after rejecting force Matched records
                            elif authStatus == 'FORCE MATCH AUTHOURIZATION' and query['operation'] == 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'FORCE_MATCHED_REJECT'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_UnMatched.csv'

                            # File Name after rejecting roll Back records which are forceMatched
                            elif authStatus == 'ROLL BACK AUTHOURIZATION' and query['operation'] == 'reject':
                                matched['AUTHORIZATION_STATUS'] = 'ROLL_BACK_REJECT'
                                File_name = hashframe.config.mftPath + query[
                                    'reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
                                            source + '_ForceMatched.csv'

                            if os.path.exists(File_name):
                                logr.info('Match File exits ' + File_name)
                                forced = pandas.read_csv(File_name, index_col=None, converters=converters)
                                if File_name not in orginalSelectedData:
                                    orginalSelectedData[File_name] = {'data': forced}
                                matched = pandas.concat([forced, matched], join_axes=[matched.columns],
                                                        ignore_index=True)
                            matched.to_csv(File_name, index=False)
                    except Exception as e:
                        logr.info(traceback.format_exc())
                        for key, val in orginalSelectedData.iteritems():
                            val['data'].to_csv(key, index=False)

                logr.info('Source which are participate in authorization is ' + ",".join(authorizatinSource))
        status, msg = self.recomputeReconSummary(query['reconName'], execLog, authorizatinSource, authorization=True,
                                                 rollBack=True)
        if not status:
            for key, val in orginalSelectedData.iteritems():
                val['data'].to_csv(key, index=False)

        return True, 'Authorize Done SuccessFully'

    # Roll Back Records
    # def authRollBackRecords(self, id, query):
    #     try:
    #         selectedRows = pandas.DataFrame(query['selectedRows'])
    #         status, details, execLog = self.getForceMatchReconDetails(query['reconName'])
    #         stmtDate = execLog['statementDate'].strftime("%d%m%Y")
    #         sources = details['sources'].keys()
    #         authorizeRecords = pandas.DataFrame()
    #         rowsSystemInx = selectedRows['System_Idx'].tolist()
    #         selectedSources = selectedRows['SOURCE'].unique().tolist()
    #         selectedRows['ROLLBACK_AUTHORIZED_BY'] = flask.session['sessionData']['userName']
    #
    #         for source in sources:
    #
    #             file_name = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
    #                         source + '_UnMatched.csv'
    #
    #             authName = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + \
    #                        source + '_Authorization.csv'
    #
    #             if (os.path.exists(authName)):
    #                 status, converters = ReconDetails().getConverters(query['reconName'], source)
    #                 converters['System_Idx'] = str
    #                 df = pandas.read_csv(authName, converters=converters)
    #                 df = df[~df['System_Idx'].isin(rowsSystemInx)]
    #                 if len(df) > 0:
    #                     df.to_csv(authName, index=False)
    #                 else:
    #                     os.remove(authName)
    #
    #             if (os.path.exists(file_name)) and source in selectedSources:
    #                 status, converters = ReconDetails().getConverters(query['reconName'], source)
    #                 converters['System_Idx'] = str
    #                 df = pandas.read_csv(file_name, converters=converters)
    #                 df = pandas.concat([df, selectedRows[selectedRows['SOURCE'] == source]],
    #                                    join_axes=[selectedRows.columns])
    #                 df.to_csv(file_name, index=False)
    #
    #         self.recomputeReconSummary(query['reconName'], execLog, sources, rollBack=True)
    #         return True, ''
    #     except Exception as e:
    #         return False, e


#  Recon Job Operations
class Utilities(object):
    def __init__(self):
        self.sourceData = {}

    def upload(self, id):
        data = dict()
        mftPath = os.path.join(hashframe.config.mftPath)
        if not os.path.exists(mftPath):
            os.makedirs(mftPath)

        filesDir = mftPath + "/" + data['date'][0]
        if not os.path.exists(filesDir):
            os.mkdir(filesDir)

        if data['fileName'][0]:
            # request.files['file'].save(os.path.join(filesDir, data['fileName'][0]))
            if not os.path.exists(os.path.join(hashframe.config.mftPath, data['date'][0])):
                os.mkdir(hashframe.config.mftPath + data['date'][0])
            else:
                return False, 'Files already uploaded with this date'

            os.system('mv %s %s' % (
                os.path.join(filesDir, data['fileName'][0]), hashframe.config.mftPath + data['date'][0] + os.sep))
            # request.files['sendfile'].save('/tmp/'+dummy)
            return True, id
        else:
            return False, data

    def upload_recon_files(self, id):
        try:
            params = json.loads(request.form['data'])
            stmt_date = params['statementDate']

            contentDir = os.path.join(hashframe.config.contentDir)
            if not os.path.exists(contentDir):
                os.makedirs(contentDir)

            request.files['file'].save(os.path.join(contentDir, id))
            logr.info("File Uploaded : %s" % os.path.join(contentDir, id))
            logr.info(hashframe.config.mftPath + '/' + params['reconName'])

            shutil.copy(os.path.join(contentDir, id), hashframe.config.mftPath + '/' + params['reconName'])

            statementDate = datetime.datetime.strptime(stmt_date, '%d/%m/%Y')

            # create upstart script & init
            # init_service = '_'.join(params['reconName'].split())
            # fname = "/etc/init/%s" % init_service + '_RJob.conf'
            #
            # if os.path.exists(fname):
            #     return False, "Previous Job in progress, please wait until it completes."
            #
            # with open(fname, "w") as f:
            #     lines = list("script\n")
            #     lines.append("truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
            #                                                                               logFile=params['reconName']))
            #
            #     cmd = "{envPath} {flowRunner} {reconName} {stmtDate} {reportFile} > {dir}/error_logs/'{logFile}_Log.log' \n".format(
            #         envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
            #         stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
            #         reportFile=request.files['file'].filename , dir=hashframe.config.contentDir,
            #         logFile=params['reconName'])
            #     lines.append(cmd)
            #     lines.append("rm -f %s\n" % fname)
            #     lines.append("end script\n")
            #     f.writelines(lines)
            #
            # logr.info('=================================== UP-Start Job ===================================')
            # logr.info('Execution Command')
            # logr.info(cmd)
            # (status, out, err) = self.execution('initctl start "%s_RJob"' % init_service)
            # logr.info("====================================================================================")
            # os.remove(contentDir + '/' + id)
            #
            # return True, "Recon Job Initiated, please wait."


            #create upstart script & init
            init_service = '_'.join(params['reconName'].split())
            fname = "/etc/systemd/system/%s" % init_service + '_RJob.service'
            if os.path.exists(fname):
                return False, "Previous Job in progress, please wait until it completes."

            with open(fname, "w") as f:
                lines = list()
                lines.append('[Unit]\n')
                lines.append("Description = Recon Job\n")
                lines.append("After = network.target \n")
                lines.append("[Service] \n")
                lines.append("User = root \n")
                pre="truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
                                                                                              logFile=params['reconName'])

                cmd = "{envPath} {flowRunner} {reconName} {stmtDate} {reportFile} > {dir}/error_logs/'{logFile}_Log.log' \n".format(
                        envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
                        stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
                        reportFile=request.files['file'].filename , dir=hashframe.config.contentDir,
                        logFile=params['reconName'])
                lines.append("ExecStartPre=%s"%pre)
                lines.append("ExecStart=%s"%cmd)
                lines.append("ExecStop=rm -f %s\n" % fname)
                lines.append("end script\n")
                f.writelines(lines)

            logr.info('=================================== UP-Start Job ===================================')
            logr.info('Execution Command')
            logr.info(cmd)

            os.system('systemctl daemon-reload')
            os.system('systemctl start "%s_RJob.service"' % init_service)
            logr.info("====================================================================================")
            os.remove(contentDir + '/' + id)

            return True, "Recon Job Initiated, please wait."
        except Exception as e:
            print traceback.format_exc()
            return False, 'Internal Error, Contact Admin.'

    def execution(self, command):
        childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        close_fds=True)
        out, err = childProcess.communicate()
        status = childProcess.returncode
        return status



