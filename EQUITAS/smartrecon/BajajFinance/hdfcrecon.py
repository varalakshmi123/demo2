import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    stmtdate = '26-Apr-2018'
    uploadFileName = 'HDFCSTAT.zip'
    # stmtdate="08-jan-2018"
    basePath = "/usr/share/nginx/smartrecon/mft/"

    params = {}
    params["feedformatFile"] = "legder.csv"
    params['skiprows']=1
    params["columnNames"] = "VOUCHER DATE,VOUCHERID,NARRATION,REFERENCE,DEALID,CUSTOMER NAME,TRANSACTION ID,MAKER ID,CHECKER ID,VALUE_DATE,DIVISION,LOCATION,AMOUNT,Loan Agreement No,CHEQUE NO,CHEQUE ID,BRANCH CODE,BRANCH NAME,INSTALMENT NO"
    params['FooterKey'] = "Day Closing"

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="FINANCE", compressionType="zip", resultkey='', reconName='HDFCSTAT'))

    elem2 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="26 April 2018 ledger.xlsx",
                                                                 feedParams=params,errors=False,

                                                                 resultkey="legderdf"))
    elem3 = dict(type="PreLoader", properties=dict(loadType="Excel", source="SFMS",
                                                   feedPattern="0007XXXXXX9191_*.*",
                                                   feedParams=dict(feedformatFile="statementstructure.csv"),
                                                   resultkey="stmtdf"))

    # elem3 = dict(type="PreLoader", properties=dict(loadType="Excel",
    #                                                source="GI",
    #                                                feedPattern="ETC Transaction Report.xls",
    #                                                feedParams={
    #                                                    "feedformatFile": "ETC_TXN_Report_Structure.csv",
    #                                                    "skiprows": 1, 'skipfooter': 1},
    #                                                feedFilters=gifilters, resultkey="gidf", txnAgeingThreshold=3,
    #                                                derivedCols={"Unique Ref No": "str"}))



    elements = [elem1,elem3]
    f = FlowRunner(elements)
    f.run()
