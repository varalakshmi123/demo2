import sys

sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '14-May-2018'
    uploadFileName = 'ICICI.zip'
    # stmtdate="08-jan-2018"
    basePath = "/usr/share/nginx/smartrecon/mft/"
    params = {}
    params["feedformatFile"] = "legder.csv"
    params['skiprows'] = 6
    params['skipfooter'] = 7
    # params["columnNames"] = "VOUCHER DATE,VOUCHERID,NARRATION,REFERENCE,DEALID,CUSTOMER NAME,TRANSACTION ID,MAKER ID,CHECKER ID,VALUE_DATE,DIVISION,LOCATION,AMOUNT,Loan Agreement No,CHEQUE NO,CHEQUE ID,BRANCH CODE,BRANCH NAME,INSTALMENT NO"
    # params['FooterKey'] = "Day Closing"

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 recontype="FINANCE", compressionType="zip", resultkey='', reconName='ICICI'))

    ledgerfil = ["df['DEBIT_CREDIT']=''", "df.loc[df['DRCR']=='CR','DEBIT_CREDIT']='DR'",
                 "df.loc[df['DRCR']=='DR','DEBIT_CREDIT']='CR'","df['ANNEXURE']='ANX3'","df.loc[df['DRCR']=='CR','ANNEXURE']='ANX4'"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="CSV", source="Legder", feedFilters=ledgerfil,
                                                   feedPattern="LEGERWISE_1 - 15-MAY-18101934.CSV",
                                                   feedParams=params,

                                                   resultkey="legderdf"))

    stmtfilter = ["df=df[df['Account Number'].str.endswith('2846')]",
                  "df['Dr Tran Amt']=df['Dr Tran Amt'].astype(np.float64)",
                  "df['Cr Tran Amt']=df['Cr Tran Amt'].astype(np.float64)",

                  # "df=df[df['Tran Particular'].str.contains('/CMS')]",
                  "df['DEBIT_CREDIT']=''",
                  "df['AMOUNT']=0",
                  "df.loc[df['Dr Tran Amt']>0,'DEBIT_CREDIT']='DR'",
                  "df.loc[df['Cr Tran Amt']>0,'DEBIT_CREDIT']='CR'",
                  "df.loc[df['Cr Tran Amt']>0,'AMOUNT']=df.loc[df['Cr Tran Amt']>0,'Cr Tran Amt']",
                  "df.loc[df['Dr Tran Amt']>0,'AMOUNT']=df.loc[df['Dr Tran Amt']>0,'Dr Tran Amt']",
                  "df['ANNEXURE']='ANX1'",
                  "df.loc[df['DEBIT_CREDIT']=='CR','ANNEXURE']='ANX2'",
                  "df['UTR NO']=df['Tran Particular'].apply(lambda x:x.split('/')[-1] if '/CMS' in x else x)"]

    elem3 = dict(type="PreLoader", properties=dict(loadType="CSV", source="STMT", feedFilters=stmtfilter,
                                                   feedPattern="lbo.recon@bizsupportbo.com_XL1_D.fqs.csv",
                                                   feedParams=dict(feedformatFile="statementstructure.csv"),
                                                   resultkey="stmtdf"))

    validatestatement = dict(type='ValidateClosingBalance', properties=dict(source='stmtdf', sourceTag='STMT',
                                                                            debitexp="df[df['DEBIT_CREDIT']=='DR']['AMOUNT'].sum()",
                                                                            creditexp="df[df['DEBIT_CREDIT']=='CR']['AMOUNT'].sum()",
                                                                            openingBalExp="df.loc[0, 'Bal Amt']",
                                                                            closingBalExp="df.loc[len(df) - 1, 'Bal Amt']"))

    computeopening = dict(type='ExpressionEvaluator',
                          properties=dict(source='legderdf',
                                          expressions=[
                                              "df=df[df['CUSTOMER NAME']=='GL Opening Balance']",
                                              "payload['openingBal']=df['MAKER ID'][0]",
                                              "payload['legderdf']=payload['legderdf'].iloc[1:]",
                                              "payload['legderdf']=payload['legderdf'].reset_index(drop=True)"
                                              # "df.drop(columns = ['Psuedo','Rev_DR_CR'], inplace = True, errors = 'ignore')"
                                          ], resultkey='openingdf'))

    LoadIncrementalData = dict(type='LoadIncrementalData',
                               properties=dict(source='legderdf', sourceTag='Legder', uniqCol='VOUCHERID',
                                               dateColumn='VOUCHER DATE', amountColumn='AMOUNT', DrCrIndicator='DRCR'))
    difbursefil = ["df=df[df['TRAN_ID'].fillna('').str.isdigit()]"]

    elem4 = dict(type="PreLoader", properties=dict(loadType="Excel", source="DISBURSE",
                                                   feedPattern="Disburse*.*", feedFilters=difbursefil,
                                                   feedParams=dict(feedformatFile='disburse.csv'),
                                                   resultkey="disbdf"))

    elem5 = dict(type="PreLoader", properties=dict(loadType="Excel", source="UTR",
                                                   feedPattern="ICICI_UTR_*.*",
                                                   feedParams=dict(skiprows=4, feedformatFile='UtrStructure.csv'),
                                                   resultkey="utrdf"))

    vlookup1 = dict(type='VLookup',
                    properties=dict(data='legderdf', lookup='disbdf', dataFields=['CHEQUE ID'],
                                    lookupFields=['CHEQUEID'], includeCols=['TRAN_ID'],
                                    resultkey="legderdf"))

    vlookup2 = dict(type='VLookup',
                    properties=dict(data='legderdf', lookup='utrdf', dataFields=['TRAN_ID'],
                                    lookupFields=['BAFL Transaction ID ( Ref 2)'], includeCols=['UTR NO'],
                                    resultkey="legderdf"))

    vlookup3 = dict(type='VLookup',
                    properties=dict(data='stmtdf', lookup='utrdf', dataFields=['UTR NO'],
                                    lookupFields=['UTR NO'], includeCols=['BAFL Transaction ID ( Ref 2)'],
                                    resultkey="stmtdf"))

    leftcol = dict(keyColumns=["UTR NO", "DEBIT_CREDIT", "AMOUNT"], sumColumns=[],
                   matchColumns=["UTR NO", "DEBIT_CREDIT", "AMOUNT"])

    rightcol = dict(keyColumns=["UTR NO", "DEBIT_CREDIT", "AMOUNT"], sumColumns=[],
                    matchColumns=["UTR NO", "DEBIT_CREDIT", "AMOUNT"])

    NwayMatch = dict(type="NWayMatch",
                     properties=dict(sources=[dict(source="stmtdf",
                                                   columns=leftcol,
                                                   sourceTag="STMT",
                                                   ),
                                              dict(source="legderdf",
                                                   columns=rightcol, sourceTag="Legder")],
                                     matchResult="results"))
    report_filters = ["df=df.groupby(['SOURCE','ANNEXURE'])['No of Transactions','Total Amount'].sum().reset_index()"]

    tally_report_gen = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='stmtdf', filterConditions=["df['No of Transactions'] = 1",
                                                         "df['AMOUNT']=df['AMOUNT'].astype(np.float64)",
                                                         "df['Total Amount'] = df['AMOUNT'].fillna(0.0).sum()",
                                                         "df['ANNEXURE']='ANX1'",
                                                         "df.loc[df['DEBIT_CREDIT']=='CR','ANNEXURE']='ANX2'",
                                                         "df = df[['SOURCE','No of Transactions', 'Total Amount','ANNEXURE']]",
                                                         ],
                      sourceTag="Purchase"),
                 dict(source='legderdf', filterConditions=["df['No of Transactions'] = 1",
                                                           "df['AMOUNT']=df['AMOUNT'].astype(np.float64)",
                                                           "df['Total Amount'] = df['AMOUNT'].fillna(0.0).sum()",
                                                           "df['ANNEXURE']='ANX3'",
                                                           "df.loc[df['DEBIT_CREDIT']=='CR','ANNEXURE']='ANX4'",
                                                           "df = df[['SOURCE','No of Transactions', 'Total Amount','ANNEXURE']]"],
                      sourceTag="Tag Issuance")], writeToFile=True, reportName='Tally', postFilters=report_filters))

    ledger_anexure_remarks = dict(type='ExpressionEvaluator',
                                  properties=dict(source='results.Legder', resultkey='results.Legder',
                                                  expressions=[
                                                      "df.loc[df['STMT Match'] == 'MATCHED',"
                                                      "'ANNEXURE Remarks'] = 'ANX1 = ANX4'",
                                                      "df.loc[(df['DRCR'] == 'DR') & (df['STMT Match'] == 'MATCHED')"
                                                      ",'ANNEXURE Remarks'] = 'ANX2 = ANX3'",
                                                       "df['ANNEXURE Remarks']=df['ANNEXURE Remarks'].fillna('')"]))

    statement_anexure_remarks = dict(type='ExpressionEvaluator',
                                     properties=dict(source='results.STMT', resultkey='results.STMT',
                                                     expressions=[
                                                         "df.loc[df['Legder Match'] == 'MATCHED',"
                                                         "'ANNEXURE Remarks'] = 'ANX1 = ANX4'",
                                                         "df.loc[(df['DEBIT_CREDIT'] == 'CR') & (df['Legder Match'] == 'MATCHED')"
                                                         ",'ANNEXURE Remarks'] = 'ANX2=ANX3'",
                                                         "df['ANNEXURE Remarks']=df['ANNEXURE Remarks'].fillna('')"
                                                         # "df.drop(columns = ['Psuedo','Rev_DR_CR'], inplace = True, errors = 'ignore')"
                                                     ]))

    elem7 = dict(id=7, next=8, type='GenerateReconSummary', properties=dict(
        sources=[dict(resultKey="STMT", sourceTag='STMT', aggrCol=['AMOUNT']),
                 dict(resultKey="Legder", sourceTag='Legder', aggrCol=['AMOUNT'])],
        groupbyCol=['STATEMENT_DATE','ANNEXURE']))

    dumpdata = dict(type='DumpData', properties=dict())

    elements = [elem1, elem2, elem3,validatestatement,computeopening,LoadIncrementalData,elem4, elem5, vlookup1, vlookup2, vlookup3, NwayMatch, tally_report_gen,
                ledger_anexure_remarks, statement_anexure_remarks,elem7,dumpdata]
    # elements = [elem1,elem5]
    f = FlowRunner(elements)
    f.run()
