import sys
sys.path.append('/usr/share/nginx/smartrecon')

from FlowElements import FlowRunner

if __name__ == "__main__":
    # stmtdate = '04-May-2018'
    # uploadFileName = 'EBanking.zip'

    stmtdate=sys.argv[1]
    uploadFileName=sys.argv[2]
    basePath = "/usr/share/nginx/smartrecon/mft/"


    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="EBank", compressionType="zip",resultkey='',reconName='EBanking'))

    elem2 = dict(type="PreLoader", properties=dict(loadType="CSV", source="FEBA",
                                                                 feedPattern="TRAN_RECON_FEBA_*.*",
                                                                 feedParams=dict(feedformatFile='Feba_Structure.csv',
                                                                                 delimiter='|', skiprows=1),
    elem3 = dict(type="PreLoader", properties=dict(loadType="CSV", source="FINCORE",
                                                                 feedPattern="TRAN_RECON_FINCORE_*.*",
                                                                 feedParams=dict(feedformatFile='Fincore_Structure.csv',
                                                                                 delimiter='|', skiprows=1),

    elem4 = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="findf",
                                               columns=dict(
                                                   keyColumns=["FEBA TRAN ID", "DR ACCOUNT NUMBER","CR ACCOUNT NUMBER",'TRAN AMOUNT'],
                                                   sumColumns=[],
                                                   matchColumns=["FEBA TRAN ID", "DR ACCOUNT NUMBER","CR ACCOUNT NUMBER",'TRAN AMOUNT']), sourceTag="FINCORE"),
                                          dict(source="febadf", columns=dict(
                                              keyColumns=["TXN_ID",'DR_ACCT_NO','CR_ACCT_NO',"TRAN_AMT"],
                                              sumColumns=[],
                                              matchColumns=["TXN_ID",'DR_ACCT_NO','CR_ACCT_NO',"TRAN_AMT"]), sourceTag="FEBA"),
                                         ],
                                 matchResult="results"))

    dumpdata = dict(type='DumpData', properties=dict())

    meta=dict(type='GenerateReconMetaInfo',properties=dict())

    elements = [elem1, elem2, elem3,elem4,dumpdata,meta]


    f = FlowRunner(elements)
    f.run()