from traceback import format_exc
import numpy as np
import pandas as pd
import re
import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class CSVLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "delimiter": ",", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["Description", "DataType", "DatePattern"]

    def getType(self):
        return "CSV"

    def loadFile(self, fileNames, params):
        dataTypes = {}
        csvTypes = {}
        dateColumns = []
        columnNames = None
        header = 0
        requiredColumns = list()
        if "feedformatFile" in params and params['feedformatFile']:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile)
            df = df.fillna('')
            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist()

            # case of carry forward use file headers instead of structure header.
            if not 'useFileHeader' in params.keys():
                columnNames = df['Description'].tolist()
                header = None

            for index, row in df.iterrows():
                if row.get('DataType', '') == 'np.datetime64':
                    dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                    dataTypes[index] = str
                    continue
                if row.get('DataType', '') == 'np.int64':
                    continue
                csvTypes[row['Description']] = eval(row.get('DataType', 'str'))
                # Convert column based on index, work-around for read_excel dataType converters
                dataTypes[index] = eval(row['DataType'])

            # while loading carry forward records, check if any derived columns defined format accordingly
            for column, dtype in params.get('derivedCols', {}).iteritems():
                csvTypes[column] = eval(dtype)

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:
                currentFrame = pd.read_csv(fileName, delimiter=params.get("delimiter", ","),
                                           skipfooter=params.get("skipfooter", 0), skiprows=params.get("skiprows", 0),
                                           header=header, names=columnNames, keep_default_na=params.get('keep_default_na',True),converters=csvTypes,engine='python')

                currentFrame= currentFrame.replace(to_replace=r'[^\x00-\x7E]+', value='', regex=True)

                # for index, data in dataTypes.iteritems():
                #     currentFrame[index] = currentFrame[index].astype(data)

                # Retain existing source file name instead of over-writing system output-file
                if 'isCarryForward' not in params.keys():
                    currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                # df = df.append(currentFrame, ignore_index=True)
                df = pd.concat([df, currentFrame], join_axes=[currentFrame.columns])
            except Exception, e:
                print e
                logger.info(fileName)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            if not 'isCarryForward' in params.keys():
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

                for dateCols in dateColumns:
                    dtpattern = dateCols["dtpatterns"]
                    if dtpattern == '%d%m%Y':
                        df[dateCols['column']] = df[dateCols['column']].astype(str)
                        df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                        if list(df['tdlen'].unique()) != [8]:
                            df[dateCols['column']] = df.apply(
                                lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']],
                                axis=1)
                        del df['tdlen']
                    df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                            errors='raise')  # , errors='coerce'
            else:
                # Adding static columns for formatting with file columns
                dateColumns.extend([{"column": 'STATEMENT_DATE'}])
                for dateCols in dateColumns:

                    df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format='%d-%m-%Y %H:%M:%S',
                                                            errors='raise')

        # discard non-mandatory columns
        if requiredColumns:
            if params.get('isCarryForward', False):
                # load required columns along with system columns on carry forward
                requiredColumns.extend(['STATEMENT_DATE', 'FEED_FILE_NAME', 'CARRY_FORWARD'])
            else:
                df = df[requiredColumns]

        logger.info('Total Records In CSV File : %s' % str(len(df)))

        return df
