import pandas as pd

import config
from prerecon.FeedLoader import FeedLoader

class BTHLoader(FeedLoader):
    def __init__(self):
        self.txnFields = pd.read_csv('/usr/share/nginx/smartrecon/Reference/TXN_HEADER_FIELDS.csv')
        self.merchantFields = pd.read_csv('/usr/share/nginx/smartrecon/Reference/MERCHANT_HEADER_FIELDS.csv')

    def getType(self):
        return "BTH"

    def loadFile(self, fileNames, params):
        df = None
        csvTypes = {}
        dateColumns = []
        columnNames = None
        header = 0
        requiredColumns = None
        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile)
            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist()
            requiredColumns = list()


            for index, row in df.iterrows():
                if row.get('DataType', '') == 'np.datetime64':
                    dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                    continue
                if row.get('DataType', '') == 'np.int64':
                    continue
                csvTypes[row['Description']] = eval(row.get('DataType', 'str'))
        resultdf=pd.DataFrame()
        for fileName in fileNames:
            headers = [{'df': self.merchantFields, 'recID': '3', 'result': 'merchDf'},
                       {'df': self.txnFields, 'recID': '4', 'result': 'txnDf'}]
            results = {}
            for doc in headers:
                df = doc['df']
                colspecs = zip(df['START_POSITION'], df['END_POSITION'])
                convtr = {i: str for i in df['FIELD DESCRIPTION']}
                inpDf = pd.read_fwf(fileName, colspecs=colspecs, names=df['FIELD DESCRIPTION'],
                                        converters=convtr)
                # Code to Link the transactions.
                startTxnHead = '2'
                inpDf.index = range(0, len(inpDf))
                inpDf.ix[inpDf[inpDf['Record ID'] == startTxnHead].index, 'BATCH_LINK_ID'] = 1
                inpDf['BATCH_LINK_ID'] = inpDf.BATCH_LINK_ID.cumsum()
                inpDf['BATCH_LINK_ID'] = inpDf['BATCH_LINK_ID'].fillna(method='ffill').astype(long).astype(str)

                recId = doc['recID']
                frame = inpDf[inpDf['Record ID'] == recId]
                results[doc['result']] = frame

            merchDf = results['merchDf']
            txnDf = results['txnDf']
            txnDf = pd.merge(txnDf, merchDf, how='left', on='BATCH_LINK_ID', suffixes=('', '_Mer'))
            resultdf=resultdf.append(txnDf)
            resultdf.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True, inplace=True)
        return resultdf