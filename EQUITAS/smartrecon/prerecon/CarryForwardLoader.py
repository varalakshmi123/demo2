import logger
from prerecon import FindFiles
from prerecon.FeedLoader import FeedLoader, FeedLoaderFactory
import pandas as pd

logger = logger.Logger.getInstance("smartrecon").getLogger()


class CarryForwardLoader(FeedLoader):
    def __init__(self):
        self.params={"feedParams":{'feedformatFile':""}}

    def getType(self):
        return "CarryForward"

    def loadFile(self, fileNames=[], params={}):
        payload = params['payload']
        properties = params['properties']

        carryFwdPath = "/usr/share/nginx/smartrecon/mft/{reconName}/{stmtDate}/OUTPUT/".format(
            stmtDate=payload['prevStmtDate'], reconName=payload['reconName'])
        file = "{file}*_UnMatched.*.csv".format(file=properties['source'])

        findFiles = FindFiles.FindFiles(payload['prevStmtDate'])
        inst_files = findFiles.findFiles(feedPath=carryFwdPath, fpattern=file)

        logger.info('-----------------------------------------------')
        logger.info("Loading carry forward from file : %s" % carryFwdPath)

        feedfac = FeedLoaderFactory()
        inst = feedfac.getInstance('CSV')
        cfData = inst.loadFile(inst_files,
                               {"feedformatFile": properties['feedParams']['feedformatFile'], "useFileHeader": True,
                                "isCarryForward": True, "derivedCols":properties.get('derivedCols', {})})

        pd.set_option('display.float_format', lambda x: '%.3f' % x)

        filterCols = []
        if not cfData.empty:
            for col in cfData.columns:
                if ' Match' in col:
                    filterCols.append(col)

            filterCond = ["(cfData['%s'].fillna('') == 'UNMATCHED')" % col for col in filterCols]
            if filterCond:
                filterCond = eval(" | ".join(filterCond))
                cfData = cfData[filterCond]

        # Remove Status Columns, to be updated by NWay Matcher & Self Matcher
        filterCols.append('Self Matched')
        for col in filterCols:
            if col in cfData:
                del cfData[col]

        logger.info('Total Records In CarryForward File : %s' % str(len(cfData)))
        logger.info('-----------------------------------------------')
        return cfData
