import pandas as pd
import os
import re
from datetime import datetime
import re
from traceback import format_exc

import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

class EomchTranwiseLoader(FeedLoader):
    def __init__(self):
        self.finallist = []
        self.finaldict = {}
        self.depcardlist = []

        self.withdraw_card = []
        self.deposit_card = []
        self.deposit_cardless = []
        self.redict_deposit = []
        self.redict_cashless = []
        self.redict_withdraw = []
        pass

    def getType(self):
        return "EOMCH_TRANLEVEL"

    def loadFile(self, fileNames, params):
        req_Data = EomchTranwiseLoader().parseFile(fileNames)
        req_Data.to_csv('/tmp/ej_data.csv')
        return req_Data



    def parseFile(self,fileNames):
        count = 0
        remi = []
        res = {}
        looplist = []
        # redict = {'RRN': [], 'Card No': [], 'ATMCODE': [], 'TRANSACTION MODE IN EJ': [], 'amount': [], 'Account Number': []}
        newl = []
        funlines = []
        funlines_cashless = []
        cashlesslines = []
        req, pin, status,cashless = 0,0,0,0
        fl = []
        for file in fileNames:
          with open(file,'r') as f:

            fileName = file

            lines = f.readlines()
            count = 0
            check = []
            for en,i in enumerate(lines):

                if 'PIN ENTERED' in i:
                    pin = True
                if 'CASH DEPOSIT' in i and 'TO A/C#:' not in lines[en+1]:
                    if 'CARD:;' in lines[en-30] :
                        self.depcardlist.append(lines[en-30].split(';')[1].strip())
                    elif  'CARD:;' in lines[en-35]:
                        self.depcardlist.append(lines[en-35].split(';')[1].strip())

                if 'TXN-STATUS :' in i:

                    funlines.append(i)

                    if len(funlines)>0  :
                        for i in funlines:
                            if 'CASH WITHDRAWAL' in i.strip():
                                self.withdraw_card.append(funlines)
                            elif 'CASH DEPOSIT' in i.strip():
                                self.deposit_card.append(funlines)

                        funlines = []
                        pin = False

                if pin:
                    newl.append(i)
                    funlines.append(i)

                if 'CARDLESS TNX STARTED' in i:
                    cashless = True

                if 'TXN-STATUS :' in i.strip():
                    funlines_cashless.append(i)
                    if len (funlines_cashless)>1:
                        for i in funlines_cashless:
                            if 'CARD LESS CASH DEPOSIT' in i.strip():
                                self.deposit_cardless.append(funlines_cashless)

                    funlines_cashless = []
                    cashless = False

                if cashless:
                    cashlesslines.append(i)
                    funlines_cashless.append(i)

        withdraw_data =self.withdrawData(self.withdraw_card)

        print("==========WITHDRAW  DATA LOADED==========")

        df_withdraw = pd.DataFrame(withdraw_data)

        print("=========WITH DRAW DATA INTO SOURCE  I/P ==========")

        deposit_data = self.depositData(self.deposit_card,self.depcardlist)

        print("==========DEPOSIT  DATA LOADED==========")

        df_deposit = pd.DataFrame(deposit_data)

        print("=========DEPOSIT DATA INTO SOURCE I/P==========")

        cardlessdeposit_data = self.cardlessDepositData(self.deposit_cardless)

        print("==========CARDLESS -DEPOSIT  DATA LOADED==========")


        df_cardless = pd.DataFrame(cardlessdeposit_data)

        print("=========CARDLESS-DEPOSIT DATA INTO SOURCE I/P==========")


        df_deposit = df_deposit.append(df_cardless)

        print("=========BOTH DEPOSIT DATA INTO SINGLE  SOURCE I/P==========")

        df_data = df_withdraw.append(df_deposit)

        print("=========ALL DATA INTO SINGLE  SOURCE I/P==========")

        return df_data

    def withdrawData(self,wc):

        for every in wc:
            self.tmpdict_withdraw = {}
            for i, l in enumerate(every):

                if '##STAN##' in l and 'CASH REQUEST FROM HOST:' in every[i-1]:

                    self.tmpdict_withdraw['Date of Transaction'] =l.split()[0].split('---')[1]
                    self.tmpdict_withdraw['Time of Transaction'] = l.split()[1]
                    if re.match(r'EOMCH',every[i+1].strip()):
                        self.tmpdict_withdraw['ATMCODE'] = every[i+1].split()[0].strip()



                if 'CASH WITHDRAWAL' in l:
                    self.tmpdict_withdraw['TRANSACTION MODE IN EJ'] = l.strip().split()[1] + l.strip().split()[2]
                    self.tmpdict_withdraw['TXNNO'] = l.split()[0].strip()

                    if 'FR A/C#:'in every[i+1]:
                        self.tmpdict_withdraw['Account Number']=every[i+1].split(':')[1].strip()

                    if re.match(r'EOMCH',every[i-2].strip()) and re.match(r'\*',every[i-1]):
                        self.tmpdict_withdraw['ATMCODE'] = every[i-2].strip()[0:9].strip()
                        self.tmpdict_withdraw['Card No'] = every[i - 2].split('CARD NO:')[1].strip()+every[i-1].strip()


                    if 'RESP CD:' in every[i+2]:
                        self.tmpdict_withdraw['Amount in EJ']=every[i+2].split('RESP CD:')[0].strip()
                        self.tmpdict_withdraw['Response Code'] = every[i+2].split('RESP CD:')[1].strip()
                        if re.match(r'\d{12}',every[i+3]):
                            self.tmpdict_withdraw['RRN'] = every[i+3].strip()

                if 'TXN-STATUS :' in l:
                        self.tmpdict_withdraw['Transaction Status EJ'] = l.split(':')[1].strip()

            self.redict_withdraw.append(self.tmpdict_withdraw)

        return self.redict_withdraw

    def depositData(self,dc,dcl):

        for every in dc:
            self.tmpdict_deposit = {}
            for i, line in enumerate(every):

                if 'CASH DEPOSIT' in line:
                    self.tmpdict_deposit['TRANSACTION MODE IN EJ'] = line.strip()
                    if 'EOMCH' in every[i-2] and 'CARD NO:' not in every[i-2]:
                        self.tmpdict_deposit['Date of Transaction'] = every[i-2].split()[0].strip()
                        self.tmpdict_deposit['Time of Transaction'] = every[i-2].split()[1].strip()
                        self.tmpdict_deposit['ATMCODE']= every[i-2].split()[2].strip()
                        self.tmpdict_deposit['RRN'] = every[i-2].split()[3].strip()

                if re.match(r'CARD NO:',line.strip()):
                    self.tmpdict_deposit['Card No'] = line.split('CARD NO:')[1].strip()

                if 'TO A/C#:' in line:
                        self.tmpdict_deposit['Account Number'] = line.split('TO A/C#:')[1].strip()
                if 'DEPOSITED AMOUNT RS:' in line:
                        self.tmpdict_deposit['Amount in EJ'] = line.split(':')[1].strip()
                        self.tmpdict_deposit['TXNNO'] = 'NA'
                        self.tmpdict_deposit['Response Code'] = 'NA'
                if 'TXN-STATUS :' in line:
                        self.tmpdict_deposit['Transaction Status EJ'] = line.split(':')[1].strip()
            for card in dcl:
                if self.tmpdict_deposit.keys():
                    if card.strip('?')[-4:].strip() == self.tmpdict_deposit['Card No'][-4:]:
                        self.tmpdict_deposit['Card No'] = card.strip('?').strip()

            self.redict_deposit.append(self.tmpdict_deposit)


        return self.redict_deposit

    def cardlessDepositData(self,wcl):
        for every in wcl:
            self.tmpdict_cardless = {}
            for i,line in enumerate(every):
                if 'EOMCH' in line and 'CARD NO:'  not in line:
                    self.tmpdict_cardless['ATMCODE']= line.strip().split()[2]
                    self.tmpdict_cardless['Date of Transaction'] = line.strip().split()[0]
                    self.tmpdict_cardless['Time of Transaction']= line.strip().split()[1]
                    self.tmpdict_cardless['RRN'] = line.strip().split()[3]
                    self.tmpdict_cardless['TXNNO'] = 'NA'
                    self.tmpdict_cardless['Response Code'] = 'NA'

                if 'CARD LESS CASH DEPOSIT' in line:
                    self.tmpdict_cardless['TRANSACTION MODE IN EJ']= line.strip()
                    self.tmpdict_cardless['Card No']= 'NA'

                if 'CUSTOMER INFORMATION ENTERED' in line and 'TNX REQUEST, OPCODE:      A' in every[i+2]  and 'CARDLESS TNX STARTED' in every[i-4]:
                    line = every[i+1]
                    if re.match(r'\d{12}',line.strip()):
                        self.tmpdict_cardless['Account Number'] = line.strip()

                if 'DEPOSITED AMOUNT RS:' in line:
                    self.tmpdict_cardless['Amount in EJ'] = line.split(':')[1].strip()
                if 'TXN-STATUS :' in line:
                    self.tmpdict_cardless['Transaction Status EJ'] = line.split(':')[1].strip()

            self.redict_cashless.append(self.tmpdict_cardless)

        return self.redict_cashless


# if __name__== "__main__":
#     wc, dc, wcl,dcl = Parse().parseFile()
#
#
#     withdraw_data = Parse().withdrawdata(wc)
#
#     df_withdraw = pd.DataFrame(withdraw_data)
#
#     deposit_data = Parse().depositdata(dc,dcl)
#
#     df_deposit = pd.DataFrame(deposit_data)
#
#     cardlessdeposit_data = Parse().cardlessDepositData(wcl)
#
#     df_cardless = pd.DataFrame(cardlessdeposit_data)
#
#     df_deposit =  df_deposit.append(df_cardless)
#
#     df_data = df_withdraw.append(df_deposit)
#
#     df_data.to_csv('/home/varun/Downloads/parsing/19-12-2018/ej_data.csv')















