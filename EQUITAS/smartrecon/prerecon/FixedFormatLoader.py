from traceback import format_exc

import numpy as np
import pandas as pd

import logger
from prerecon.FeedLoader import FeedLoader
import config

logger = logger.Logger.getInstance("smartrecon").getLogger()


class FixedFormatLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["DataType","DatePattern","Description","To","From"]

    def getType(self):
        return "FixedFormat"

    def loadFile(self, fileNames, params):
        output = []
        names = []
        positions = []
        typedic = {}
        dateColumns = []
        requiredColumns = list()

        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile,
                             converters={"From": np.int64, "To": np.int64}, engine='python')

            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist()
                requiredColumns.extend(['FEED_FILE_NAME'])

            for index, row in df.iterrows():
                positions.append((row["From"] - 1, row["To"]))
                names.append(row['Description'])
                if row[0] == 'np.datetime64':
                    dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                if row[0] == 'np.int64' or row[0] == 'np.float64':
                    typedic[row['Description']] = eval(row[0])
                    continue
                typedic[row['Description']] = str

        if len(names) == 0:
            logger.info("Cannot load Fixed format file without feed structure")
            raise

        df = pd.DataFrame(columns=names + ['FEED_FILE_NAME'])
        try:
            for fileName in fileNames:
		print fileName
                logger.info('Loading file %s' % fileName)
                currentframe = pd.read_fwf(fileName, header=None, colspecs=positions, names=names,
                                           converters=typedic, skiprows=params.get("skiprows", 0),
                                           skipfooter=params.get("skipfooter", 0))
		
                currentframe['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = df.append(currentframe)
        except Exception as e:
            logger.info(e)
            logger.info(fileNames)
            logger.info(format_exc())
            logger.info("Unable to load the file " + str(file) + ".\n"
                                                                 "Please check that file is readable and is "
                                                                 "as per the configured structure.\n"
                                                                 "If problem persists contact Administrator.")
            raise
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']], axis=1)
                    del df['tdlen']

                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern, errors='raise')

            logger.info("Total Records in Fixed-Length file  = %6d" % len(df))

        # discard non-mandatory columns
        if requiredColumns:
            df = df[requiredColumns]
        else:
            pass

        return df

    def getFLPositions(self, feedDef):
        positions = []
        for col in feedDef['fieldDetails']:
            start = int(col['startIndex']) - 1
            end = int(col['endIndex'])
            positions.append((start, end))
        return positions

# if __name__ == "__main__":
#     fixedLoader = FixedFormatLoader('NPCI_Structure.csv')
#     params = dict()
#     params['skipTopRows'] = 0
#     params['skipBottomRows'] = 0
#     fixedLoader.loadFile('/usr/share/nginx/v8/mft/NPCI/ACQRPEQT220817_1C.mEQT', params=params)
