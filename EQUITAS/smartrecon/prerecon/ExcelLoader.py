from traceback import format_exc

import numpy as np
import pandas as pd
import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class ExcelLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["Description", "DataType", "DatePattern"]

    def getType(self):
        return "Excel"

    def loadFile(self, fileNames, params):
        dataTypes = {}
        dateColumns = []
        columnNames = []
        requiredColumns = []
        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile).fillna('')
            requiredColumns = list()
            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist() + ['FEED_FILE_NAME']

            columnNames = df['Description'].tolist()
            for index, row in df.iterrows():
                # Ingore fomatting on column with non-ascii characters.
                if row['DataType']:
                    if row['DataType'] == 'np.datetime64':
                        dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                        dataTypes[index] = str
                        continue
                    if row['DataType'] == 'np.int64':
                        continue
                else:
                    continue

                # Convert column based on index, work-around for read_excel dataType converters
                dataTypes[index] = eval(row['DataType'])

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:
                logger.info("Loading excel file %s" % fileName)
                if len(columnNames) > 0:
                    currentFrame = pd.read_excel(fileName, skipfooter=params.get("skipfooter", 0),
                                                 skiprows=params.get("skiprows", 0),
                                                 header=None, names=columnNames,
                                                 converters=dataTypes, squeeze=True)
                else:
                    currentFrame = pd.read_excel(fileName, skipfooter=params.get("skipfooter", 0),
                                                 skiprows=params.get("skiprows", 0),
                                                 squeeze=True)
                if len(columnNames)>0:
                    currentFrame.columns = columnNames
                currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = pd.concat([df, currentFrame], join_axes=[currentFrame.columns])
            except Exception, e:
                logger.info(fileName)

                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            # replace non-ascii error characters, if excel contains non-ascii characters.
            df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']], axis=1)
                    del df['tdlen']
                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        # discard non-mandatory columns
        if requiredColumns and not df.empty:
            df = df[requiredColumns]

        logger.info('Total Records In Excel File : %s' % str(len(df)))
       
	return df
