import pandas as pd
import numpy as np
import collections

dict1 = {'name': ['a', 'b', 'c', 'd', 'e', 'f'], 'rollno': [1, 2, 3, 4, 5, 6],
         'branch': ['cs', 'ds', 'es', 'fs', 'gs', 'hs']}
dict2 = {'name': ['g', 'h', 'i', 'j', 'e', 'f'], 'rollno': [11, 12, 13, 14, 5, 6],
         'branch': ['ccs', 'dds', 'ees', 'ffs', 'gs', 'hs']}
df1 = pd.DataFrame(dict1)
df2 = pd.DataFrame(dict2)
print df1
print df2
matdic = {'name': ['e', 'f'], 'rollno': [5, 6], 'branch': ['gs', 'hs']}

# df = pd.read_csv('/usr/share/nginx/smartrecon/mft/IMPS Inward/10112018/PROCESSED/IMPS_INWARD_3C_RPRT_as_on_10-Nov-2018.txt',delimiter='|')
# print df.head()
# df = pd.read_csv('/usr/share/nginx/smartrecon/mft/MF_PINCODE/15112018/OUTPUT/Pincode_Count_Report.csv')
# df = df.groupby(['CENTER ID', 'PINCODE_COUNT', 'PINCODE']).size().reset_index(name='counts')
#
# df.loc[df.duplicated(['CENTER ID', 'PINCODE_COUNT']), 'Duplicated'] = 'Yes'
# df.loc[df['Duplicated'] == 'Yes', ['CENTER ID', 'PINCODE_COUNT']] = '0', '0'
# df['CENTER ID'] = df['CENTER ID'].str.strip()
# df['CENTER ID'] = df['CENTER ID'].astype(str)
# print df.head()
# exit()
# df['CENTER ID'] = df['CENTER ID'].str.replace('','0').astype(np.int64)
# df['PINCODE_COUNT'] = df['PINCODE_COUNT'].astype(np.int64)
# del df['Duplicated']
# df.loc[df['PINCODE_COUNT'] == 0,'PINCODE_COUNT'] = " "
# df.loc[df['CENTER ID'] == 0,'CENTER ID'] = " "
# print df.dtypes
# df['CENTER ID'] = df['CENTER ID'].astype(str)
# print df['CENTER ID']
# df['CENTER ID'] = df['CENTER ID'].str.replace('','0')
# df1 = pd.read_csv('/home/varun/UPI_UAT/adjdf.csv',index_col = None)
# df2 = pd.read_csv('/home/varun/UPI_UAT/html.csv',index_col = None)
# df2['RRN'] = df2['RRN'].str.lstrip(" \t' ")
# # df2['RRN_USE'] =  df2['RRN'].apply(lambda x:x.lstrip() for x in df2['RRN'] )
# df2.to_csv('/home/varun/UPI_UAT/test.csv')
# # df2 = df2.merge(df1[['RRN','TO_ACCOUNT_NUMBER']],on = 'RRN',how = 'left')
# # df2.to_csv('/home/varun/UPI_UAT/result.csv')
# df1['RRN'] =  df1['RRN'].astype(str)
# # print df1['RRN'].head()
# # print df2['RRN'].head()
# for i in df1['RRN']:
#     for j in df2['RRN']:
#         if i == j:
#             print i
# df2 = df2.merge(df1[['RRN','TO_ACCOUNT_NUMBER']],on = 'RRN',how = 'left')
# print df2.head()
# df2.to_csv('/home/varun/UPI_UAT/result.csv',index = False)


leftdata = pd.DataFrame(
    {'policy number': ['abc', 'def', 'abc'], 'alt policy': ['efz', 'def', 'cxs'], 'Bamount': [12, 12, 13],
     'Ramount': [10, 13, 13]})
rightdata = pd.DataFrame(
    {'policy number': ['abc', 'efz', 'abc'], 'alt policy': ['dcs', 'cds', 'qew'], 'Bamount': [12, 12, 11],
     'Ramount': [10, 18, 12]})

sources = [dict(source="Broker", columns=dict(sumColumns=['Bamount', 'Ramount'],
                                              matchColumns=["policy number", 'alt policy']),
                subsetfilter=["df=df[(df['Reward Amount']!=0)]"],
                sourceTag="Brokerage"),
           dict(source="Business",
                columns=dict(sumColumns=['Bamount', 'Ramount'], matchColumns=["policy number", 'alt policy']),
                subsetfilter=['Bamount', 'Ramount'],
                sourceTag="Business")]

for i in range(0, len(sources) - 1):
    leftsource = sources[i]
    leftcolumns = leftsource["columns"]

    for j in range(i + 1, len(sources)):
        rightsource = sources[j]
        rightcolumns = rightsource["columns"]
        rightdata = rightdata

        leftdata = leftdata
        toleranceValue = 5
        leftmatchColumns = leftcolumns.get('matchColumns', [])
        rightmatchColumns = rightcolumns.get('matchColumns', [])

        leftsumColumns = leftcolumns.get('sumColumns', [])
        rightsumColumns = rightcolumns.get('sumColumns', [])

        leftjson = leftdata.to_dict(orient='records', into=collections.OrderedDict())
        rightjson = rightdata.to_dict(orient='records', into=collections.OrderedDict())

        for x in leftjson:
            for y in rightjson:
                if leftsource["sourceTag"] + " Match" in y and y[
                    leftsource["sourceTag"] + " Match"] == "MATCHED":
                    continue

                matched = False
                for idx in range(0, len(leftmatchColumns)):
                    for rdx in range(0, len(rightmatchColumns)):
                        if x[leftmatchColumns[idx]] == y[rightmatchColumns[rdx]]:
                            count = 0

                            for index in range(0, len(leftsumColumns)):
                                if x[leftsumColumns[index]] == y[leftsumColumns[index]]:
                                    count = count + 1
                                elif (x[leftsumColumns[index]] - y[rightsumColumns[index]]) >= (-toleranceValue) and (
                                        x[leftsumColumns[index]] - y[rightsumColumns[index]]) <= (toleranceValue):
                                    x['tolerance Difference' + leftsumColumns[index]] = x[leftsumColumns[index]] - y[
                                        rightsumColumns[index]]
                                    x[leftsumColumns[index] + ' toleranceMatch'] = 'Within Range'

                            if count == len(leftsumColumns):
                                matched = True
                                break
                            else:
                                matched = True
                                for index in range(0, len(leftsumColumns)):
                                    if x[leftsumColumns[index] + ' toleranceMatch'] != 'Within Range':
                                        matched = False

                    if matched:
                        x[rightsource["sourceTag"] + " Match"] = "MATCHED"
                        y[leftsource["sourceTag"] + " Match"] = "MATCHED"
                    else:
                        x[rightsource["sourceTag"] + " Match"] = "UNMATCHED"
                        y[leftsource["sourceTag"] + " Match"] = "UNMATCHED"
