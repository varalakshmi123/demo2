import os
import re
from datetime import timedelta

import logger

logger = logger.Logger.getInstance("smartrecon").getLogger()


class FindFiles:
    def __init__(self, stmtDate):
        self.stmtDate = stmtDate

    # Set raiseError = False to ignore errors incase if files not found.
    def findFiles(self, feedPath, fpattern, delta=0, raiseError=True):
        if '#' in fpattern:
            indx1 = fpattern.index('#')
            indx2 = fpattern.index('#', indx1 + 1)
            dpattern = fpattern[indx1 + 1:indx2]
            dstr = (self.stmtDate - timedelta(delta)).strftime(dpattern)
            fpattern = fpattern.replace('#' + dpattern + '#', dstr)
        else:
            pass
            # raise BaseException("Invalid file pattern %s" % fpattern)
        if not os.path.exists(feedPath):
            raise BaseException('Feed Path %s Does not exits' % feedPath)
        files = [feedPath + "/" + f for f in os.listdir(feedPath) if re.match(fpattern.lower(), f.lower())]

        logger.info('-------------------------------------------------')
        logger.info('Files found with pattern %s : %s' % (fpattern, len(files)))

        if len(files) == 0 and raiseError:
            logger.info('-------------------------------------------------')
            logger.info("Feed file with pattern %s not received" % fpattern)
            raise ValueError("Could not find feed file with pattern %s" % fpattern)
        else:
            logger.info('-------------------------------------------------')
            logger.info("Raise Error has been set to false")

        return files
