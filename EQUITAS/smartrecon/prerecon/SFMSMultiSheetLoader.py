import sys
import traceback
from traceback import format_exc

import numpy as np
import pandas
import pandas as pd
import xlrd

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class SFMSMultiSheetLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "columnNames": [], "skiprows": '', "skipfooter": '', "FooterKey": "",
                       "sheetIndex": []}
        self.feedformatfilecolumns = ["Description", "DataType", "DatePattern"]

    def getType(self):
        return "SFMSMultiSheetLoader"

    def loadFile(self, fileNames, params):
        dataTypes = {}
        dateColumns = []
        columnNames = []
        requiredColumns = []
        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile)

            columnNames = df['Description'].tolist()

            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist()

            for index, row in df.iterrows():
                if row['DataType'] == 'np.datetime64':
                    dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                    continue
                if row['DataType'] == 'np.int64':
                    continue
                dataTypes[index] = eval(row['DataType'])
                # Convert column based on index, work-around for read_excel dataType converters

        df = pd.DataFrame(columns = columnNames)

        for fileName in fileNames:
            try:
                xls = xlrd.open_workbook(fileName, on_demand=True)
                sheets = xls.sheet_names()
                colNames = None
                # if column names not defined in params, consider from ref file
                colNames = params.get("columnNames", ','.join(columnNames))
                if colNames:
                    checklen=True
                    result = pandas.DataFrame(columns=colNames.split(","))
                else:
                    result = pandas.DataFrame(columns=columnNames)
                    colNames = ','.join(columnNames)
                for sheetNo in params.get('sheetIndex', range(0, len(sheets))):
                    sheetName = sheets[sheetNo]
                    sheet = xls.sheet_by_index(sheetNo)
                    c = -1
                    footerRow = 0
                    for rownum in xrange(sheet.nrows):
                        # avoid empty records loading
                        rowvalues = [value for value in sheet.row_values(rownum) if value]
                        matchCount = 0
                        
                        for col in colNames.split(','):
                            if col.strip() in rowvalues:
                                matchCount += 1

                        if rowvalues and matchCount >= 0.75 * len(rowvalues):
                            #check sheetvalues is subset of reference columnvalues
                            if checklen and len(rowvalues)!=len(colNames.split(",")):
                                continue
                            c = rownum
                            break

                    footerKey = params.get('FooterKey', '')
                    if footerKey:
                        for rownum in xrange(sheet.nrows):
                            if footerKey in ','.join([str(i) for i in sheet.row_values(rownum)]):
                                footerRow = rownum

                    if c == -1:
                        continue
                    skip_footer = rownum - footerRow + 2
                    if footerRow == 0:
                        skip_footer = 0

                    currentFrame = pandas.read_excel(fileName, sheetname=sheetNo,
                                                     skiprows=c + 1 + params.get('skiprows', 0),
                                                     skip_footer=skip_footer + params.get('skipfooter', 0),
                                                     converters=dataTypes, header=None)

                    for index, data in dataTypes.iteritems():
                        currentFrame[index] = currentFrame[index].astype(data)

                    currentFrame = currentFrame.loc[:, range(0, len(columnNames))]
                    currentFrame.columns = columnNames
                    currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                    df = pandas.concat([df, currentFrame], ignore_index=True)
                    df = df.replace(["\\n", "\\r"], " ", regex=True)
                df.dropna(how='all',subset=columnNames,inplace=True)

            except Exception as e:
                print e
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print "*** print_tb:"
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                print "*** print_exception:"
                traceback.print_exception(exc_type, exc_value, exc_traceback,
                                          limit=2, file=sys.stdout)
                print e
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']], axis=1)
                    del df['tdlen']
                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        # discard non-mandatory columns
        if not df.empty and requiredColumns:
            df = df[requiredColumns + ['FEED_FILE_NAME']]

        logger.info('Total Records In Excel File : %s' % str(len(df)))
        return df
