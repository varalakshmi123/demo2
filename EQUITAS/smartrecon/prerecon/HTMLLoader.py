from traceback import format_exc
import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class HTMLLoader(FeedLoader):
    def __init__(self):
        pass

    def getType(self):
        return "CSV"

    def loadFile(self, fileNames, params):
        df = None

        csvTypes = {}
        dateColumns = []
        columnNames = None
        requiredColumns = list()
        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile)

            requiredColumns = list()
            if 'Required' in df.columns:
                requiredColumns = df[df['Required'] == 'y']['Description'].tolist()

            if not 'useFileHeader' in params.keys():
                columnNames = df['Description'].tolist()

            for index, row in df.iterrows():
                if row.get('DataType', '') == 'np.datetime64':
                    dateColumns.append({"column": row['Description'], 'dtpatterns': row['DatePattern']})
                    continue
                if row.get('DataType', '') == 'np.int64':
                    continue
                csvTypes[row['Description']] = eval(row.get('DataType', 'str'))

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:
                df_list = pd.read_html(fileName)
                for index,skiprow in zip(params.get('parseIndex', []),params.get('skiprows', [])):
                  currentFrame = pd.DataFrame(df_list[index])
                  if params.get('skiprows', []):
                    currentFrame=currentFrame[skiprow:]
                    currentFrame.columns = columnNames
                  df = df.append(currentFrame, ignore_index=True)
                  df['FEED_FILE_NAME'] = fileName.split('/')[-1]

            except  ValueError as e:
                df = pd.DataFrame(columns=columnNames)


            except Exception, e:

                df = pd.DataFrame()
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)


        if len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).replace('=','') if str(x).startswith('=') else str(x))
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).replace('"','') if str(x).startswith('"') else str(x))
            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']], axis=1)
                    del df['tdlen']
                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                        errors='coerce')  # , errors='coerce'




        # discard non-mandatory columns
        if requiredColumns:
            df = df[requiredColumns]

        logger.info('Total Records In HTML File : %s' % str(len(df)))
        return df
