from traceback import format_exc

import numpy as np
import pandas as pd

import logger
from prerecon.FeedLoader import FeedLoader
import config
from prerecon.FindFiles import FindFiles


class CustomDataLoader(FeedLoader):
    def __init__(self):
        pass

    def getType(self):
        return "CombineFileLoader"

    def loadFile(self, filename='', params={}):
        result = getattr(CustomFunctions(), params['func'])(params.get('payload', {}))
        return result


class CustomFunctions():
    def loadCombinedGL(self, params={}):
        columns = pd.read_csv(config.basepath + "Reference/GL MAPPING.csv")
        glFiles = []
        glFiles.extend(FindFiles(params['statementDate']).findFiles(params['source_path'], 'allgl*.csv'))
        glFiles.extend(FindFiles(params['statementDate']).findFiles(params['source_path'], 'GL*.*csv'))
        glDf = pd.DataFrame()

        for glfile in glFiles:
            if 'allgl.csv' in glfile:
                srccolumns = columns["All GL"].tolist()
                mappedcols = columns["Description"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                gl = pd.read_csv(glfile)
                gl = gl[srccolumns]
                gl = gl.rename(columns=renamedict)
                gl['FEED_FILE_NAME'] = glfile.split('/')[-1]
                glDf = glDf.append(gl, ignore_index=True)
            else:
                srccolumns = columns["GL Report"].tolist()
                mappedcols = columns["Description"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                gl = pd.read_csv(glfile)
                gl = gl[srccolumns]
                gl = gl.rename(columns=renamedict)
                gl['FEED_FILE_NAME'] = glfile.split('/')[-1]
                glDf = glDf.append(gl, ignore_index=True)

        if 'Txn Date' in glDf.columns:
            # load only today's records for All-GL report
            glDf['Txn Date_filter'] = pd.to_datetime(glDf['Txn Date'], format='%d-%m-%Y %H:%M:%S').dt.strftime('%d-%m-%Y')
            filterDate = params['statementDate'].strftime('%d-%m-%Y')
            glDf = glDf[glDf['Txn Date_filter'] == filterDate]
            del glDf['Txn Date_filter']
        else:
            pass

        return glDf

    def loadCombinedETCTagReports(self, params={}):
        tag_mapping = pd.read_csv(config.basepath + "Reference/ETC Tag Mapping.csv")
        tagRpt = pd.DataFrame()
        source = ''

        fileFinder = FindFiles(params['statementDate'])
        etcTagFiles = fileFinder.findFiles(params['source_path'],
                                           r'Tag Issuance*.*xls|Self Registration*.*xls|Tag Recharge*.*xls')

        for file in etcTagFiles:
            if 'Self Registration' in file:
                source = 'Self Registration'
                rename_cols = tag_mapping.dropna(subset=['Self Reg']).set_index('Self Reg')['Description'].to_dict()
            elif 'Tag Issuance' in file:
                source = 'Tag Issuance'
                rename_cols = tag_mapping.dropna(subset=['Tag Iss']).set_index('Tag Iss')['Description'].to_dict()
            elif 'Tag Recharge' in file:
                source = 'Tag Recharge'
                rename_cols = tag_mapping.dropna(subset=['Tag Rech']).set_index('Tag Rech')['Description'].to_dict()
            else:
                pass
            tmp = pd.read_excel(file, skip_footer=1).rename(columns=rename_cols)
            tmp['Source'] = source
            tagRpt = tagRpt.append(tmp)

        commonCols = tag_mapping[tag_mapping['Required'] == 'y']['Description'].tolist()
        return tagRpt[commonCols]
