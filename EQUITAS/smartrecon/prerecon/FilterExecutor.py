import numpy as np
import pandas as pd
import re
import logger
import datetime as dt
from datetime import datetime
from traceback import format_exc

logger = logger.Logger.getInstance("smartrecon").getLogger()


class FilterExecutor:
    def __init__(self):
        # Dont remove as this removes the import for optimization
        type(64.0) == np.float64

    def run(self, df, expressions, payload={}, resultkey=''):
        if payload:
            payload[resultkey + "@BeforeFilter"] = len(df)

        for expression in expressions:
            try:
                print expression
                logger.info(expression)
                payload[resultkey + "@BeforeFilter " + expression] = len(df)

                # check to avoid "ValueError: cannot set a frame with no defined index and a scalar"
                if isinstance(df, pd.DataFrame) and df.empty:
                    continue

                exec expression
                payload[resultkey + "@AfterFilter " + expression] = len(df)
            except Exception, e:
                print e
                payload[resultkey + "@AfterFilter " + expression] = "Exception in", expression, e
                logger.info(e)
                logger.info(format_exc())
                logger.info("Exception in %s" % expression)
                raise ValueError('Error Evaluting expression %s' % expression)
        return df
         
