import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="LoadManagement", compressionType="zip",
                                resultkey='', reconName="HDFC_STP"))

    ledger_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="Ledger",
                                                           feedPattern="Ledger.xlsx",
                                                           feedParams={
                                                               "feedformatFile": "HDFC_STP_Ledger_Structure.csv"},
                                                           feedFilters=[
                                                               "df = df[df['CHEQUE ID'].fillna('').str.isdigit()]",
                                                               "df['AMOUNT'] = df['AMOUNT'].astype(np.float64)"],
                                                           resultkey="ledgerdf"))

    wrf_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="WRF",
                                                        feedPattern="WRF Ledger.xlsx",
                                                        feedParams={
                                                            "feedformatFile": "HDFC_STP_WRF_Structure.csv",
                                                            "skiprow": 2},
                                                        feedFilters=[
                                                            "df['Reference No'].dropna(inplace = True)",
                                                            "df['Debit'] = df['Debit'].astype(np.float64)",
                                                            "df['Credit'] = df['Credit'].astype(np.float64)",
                                                            "df['CR/DR'] = 'CR'",
                                                            "df.loc[df['DEBITS'] > 0,'CR/DR'] = 'DR'"],
                                                        resultkey="wrfledgerdf"))

    statement_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="Statement",
                                                              feedPattern="0007XXXXXX9191_02c455c8*",
                                                              feedParams={
                                                                  "feedformatFile": "HDFC_STP_Statement_Structure.csv",
                                                                  "sheets": 'All', 'skiprows': 5},
                                                              resultkey="stmtdf",
                                                              feedFilters=[
                                                                  "df['DEBITS'] = df['DEBITS'].fillna('').str.replace(',','')",
                                                                  "df['CREDITS'] = df['CREDITS'].fillna('').str.replace(',','')",
                                                                  "df['DEBITS'] = df['DEBITS'].apply(lambda x : x if x else 0.0)",
                                                                  "df['CREDITS'] = df['CREDITS'].apply(lambda x : x if x else 0.0)",
                                                                  "df[['DEBITS','CREDITS']] = df[['DEBITS','CREDITS']].astype(np.float64)",
                                                                  "df['CR/DR'] = 'CR'",
                                                                  "df.loc[df['DEBITS'] > 0,'CR/DR'] = 'DR'",
                                                                  "df['AMOUNT'] = df['DEBITS'] + df['CREDITS']",
                                                                  "df.loc[df['CREDITS'] == 0, 'Rev_DR_CR'] = 'CR'",
                                                                  "df.loc[df['DEBITS'] == 0, 'Rev_DR_CR'] = 'DR'",
                                                                  "df['Annexure'] = 'ANX1'",
                                                                  "df.loc[df['CR/DR'] == 'CR', 'Annexure'] = 'ANX2'"]))

    validatestatement = dict(type='ValidateClosingBalance', properties=dict(source='stmtdf', sourceTag='Statement',
                                                                            debitexp=
                                                                            "df[df['BRANCH'].notna()]['DEBITS'].sum()",
                                                                            creditexp=
                                                                            "df[df['BRANCH'].notna()]['CREDITS'].sum()",
                                                                            openingBalExp=
                                                                            "float(df[df['DATE'] == 'Opening Balance']['BALANCE'].replace(',',''))",
                                                                            closingBalExp=
                                                                            "float(df[df['DATE'] == 'Closing Balance']['BALANCE'].replace(',',''))"))

    drop_statment_records = dict(type="ExpressionEvaluator", properties=dict(source='stmtdf', resultkey='stmtdf',
                                                                             Expression=[
                                                                                 "df = df[df['BRANCH'].notna()]"]))

    utr_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="UTR",
                                                        feedPattern="UTR*",
                                                        feedParams={
                                                            "feedformatFile": "HDFC_STP_UTR_Structure.csv",
                                                            "skiprows": 4}, feedFilters=[], disableCarryFwd=True,
                                                        resultkey="utrdf"))

    disbursal_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="DISBURSAL",
                                                              feedPattern="Disbursed_details*",
                                                              feedParams={
                                                                  "feedformatFile": "HDFC_STP_DISBURSAL_Structure.csv",
                                                                  "skiprows": 1}, disableCarryFwd=True,
                                                              feedFilters=[], resultkey="disbursaldf"))

    jb_recovery_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="JB_Dealer",
                                                                feedPattern="JB_Dealer_Recovery*",
                                                                feedParams={
                                                                    "feedformatFile": "HDFC_STP_JBRec_Structure.csv",
                                                                    "skiprows": 1}, disableCarryFwd=True,
                                                                feedFilters=["df = df[df['DRAMT'] > 0]"],
                                                                resultkey="jbdf"))

    ledger_disbursal_lookup = dict(type='VLookup',
                                   properties=dict(data='ledgerdf', lookup='disbursaldf', dataFields=['CHECKER ID'],
                                                   lookupFields=['CHEQUEID'], includeCols=['TRAN_ID'],
                                                   resultkey="ledgerdf"))

    fill_transactionid = dict(type="ExpressionEvaluator",
                              properties=dict(source='ledgerdf', resultkey='ledgerdf',
                                              expressions=[
                                                  "df.loc[df['TRANSACTION ID'].isna(), 'TRANSACTION ID'] = df.loc[df['TRANSACTION ID'].isna(), 'TRAN_ID']",
                                                  "del df['TRAN_ID']"]))

    ledger_utr_lookup = dict(type='VLookup',
                             properties=dict(data='ledgerdf', lookup='utrdf', dataFields=['TRANSACTION ID'],
                                             lookupFields=['Tranaction Id'], includeCols=['UTR Number'],
                                             resultkey="ledgerdf"))

    stmt_utr_lookup = dict(type='VLookup',
                           properties=dict(data='stmtdf', lookup='utrdf', dataFields=['DESCRIPTION'],
                                           lookupFields=['Tranaction Id'], includeCols=['UTR Number'],
                                           resultkey="stmtdf"))
    jb_recovery_lookup = dict(type='VLookup',
                              properties=dict(data='ledgerdf', lookup='jbdf',
                                              dataFields=['AMOUNT', 'CHEQUE ID', 'Loan Agreement No'],
                                              lookupFields=['DRAMT', 'CHEQUEID', 'AGREEMENTNO'],
                                              markers={"JB Recovery": "Found"}, resultkey="ledgerdf"))

    jb_recovery_entires = dict(type='ExpressionEvaluator',
                               properties=dict(source='ledgerdf', resultkey='jb_recovery_entires',
                                               expressions=["df = df[df['JB Recovery'] == 'Found']"]))

    drop_jbrecovery_entires = dict(type='ExpressionEvaluator',
                                   properties=dict(source='ledgerdf', resultkey='ledgerdf',
                                                   expressions=["df = df[df['JB Recovery'] != 'Found']"]))

    ledger_match = dict(type="NWayMatch",
                        properties=dict(sources=[dict(source="ledgerdf",
                                                      columns=dict(
                                                          keyColumns=['UTR Number', 'AMOUNT', 'CR/DR'], sumColumns=[],
                                                          matchColumns=['UTR Number', 'AMOUNT', 'CR/DR']),
                                                      sourceTag="Ledger"),
                                                 dict(source="stmtdf", columns=dict(
                                                     keyColumns=['UTR Number', 'AMOUNT', 'Rev_DR_CR'], sumColumns=[],
                                                     matchColumns=['UTR Number', 'AMOUNT', 'Rev_DR_CR']),
                                                      sourceTag="Statement")],
                                        matchResult="results"))

    wrf_match = dict(type="NWayMatch",
                     properties=dict(sources=[dict(source="wrfledgerdf",
                                                   columns=dict(
                                                       keyColumns=['UTR Number', 'AMOUNT', 'CR/DR'], sumColumns=[],
                                                       matchColumns=['UTR Number', 'AMOUNT', 'CR/DR']),
                                                   sourceTag="Ledger"),
                                              dict(source="stmtdf", columns=dict(
                                                  keyColumns=['UTR Number', 'AMOUNT', 'Rev_DR_CR'], sumColumns=[],
                                                  matchColumns=['UTR Number', 'AMOUNT', 'Rev_DR_CR']),
                                                   sourceTag="Statement")],
                                     matchResult="results"))

    ledger_anexure_remarks = dict(type='ExpressionEvaluator',
                                  properties=dict(source='results.Ledger', resultkey='results.Ledger',
                                                  expressions=[
                                                      "df.loc[df['Statement Match'] == 'MATCHED',"
                                                      "'ANEXURE Remarks'] = 'ANX1 = ANX4'",
                                                      "df.loc[(df['CR/DR'] == 'DR') & (df['Statement Match'] == 'MATCHED')"
                                                      ",'ANEXURE Remarks'] = 'ANX2 = ANX3'"]))

    statement_anexure_remarks = dict(type='ExpressionEvaluator',
                                     properties=dict(source='results.Statement', resultkey='results.Statement',
                                                     expressions=[
                                                         "df.loc[df['Ledger Match'] == 'MATCHED',"
                                                         "'ANEXURE Remarks'] = 'ANX1 = ANX4'",
                                                         "df.loc[(df['CR/DR'] == 'DR') & (df['Ledger Match'] == 'MATCHED')"
                                                         ",'ANEXURE Remarks'] = 'ANX2=ANX3'",
                                                         "df.drop(columns = ['Psuedo','Rev_DR_CR'], inplace = True, errors = 'ignore')"]))

    report_filters = ["df=df.groupby(['SOURCE','ANNEXURE'])['No of Transactions','AMOUNT'].sum().reset_index()"]

    tally_report_gen = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='stmtdf', filterConditions=["df['No of Transactions'] = 1",
                                                         "df['ANNEXURE']='ANX1'",
                                                         "df.loc[df['CR/DR']=='CR','ANNEXURE']='ANX2'"],
                      sourceTag="Statement"),
                 dict(source='ledgerdf', filterConditions=["df['No of Transactions'] = 1",
                                                           "df['ANNEXURE']='ANX3'",
                                                           "df.loc[df['CR/DR']=='CR','ANNEXURE']='ANX4'"],
                      sourceTag="Ledger")], writeToFile=True, reportName='Tally', postFilters=report_filters))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='HDFC_STP', matched='any'))

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, ledger_report, statement_report, validatestatement, drop_statment_records, utr_report,
                disbursal_report,
                jb_recovery_report, ledger_disbursal_lookup, fill_transactionid, ledger_utr_lookup, stmt_utr_lookup,
                ledger_match,
                ledger_anexure_remarks, statement_anexure_remarks, tally_report_gen, dump_data, gen_meta_info]

    f = FlowRunner(elements)

    f.run()
