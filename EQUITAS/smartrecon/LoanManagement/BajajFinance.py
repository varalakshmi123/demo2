import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '7-May-2018'
    uploadFileName = 'bajaj_input_files.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="LoadManagement", compressionType="zip",
                                resultkey='',
                                reconName="Bajaj Fin"))

    loan_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="Loan Report",
                                                         feedParams={"skiprows": 4},
                                                         feedPattern="0007XXXXXX9191_e04ffca9_13Apr2018_TO_13Apr2018_061042257.xls",
                                                         resultkey="cbsdf", feedFilters=[]))

    dispute_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="Disputes",
                                                            feedPattern="Disbursal dump.xlsb",
                                                            feedParams={}, feedFilters=[], resultkey="disputedf"))

    ledger_report = dict(type="PreLoader", properties=dict(loadType='CSV', source="Ledger",
                                                           feedPattern="Sample ledger for HDFC STP  13 April2018.CSV",
                                                           feedParams={}, feedFilters=[], resultkey="ledgerdf"))

    utr_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="UTR", feedPattern="UTR Dump.xlsb",
                                                        feedParams={}, feedFilters=[], resultkey="utrdf"))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='JRI Recon'))

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, loan_report, dispute_report, ledger_report, utr_report, dump_data, gen_meta_info]

    f = FlowRunner(elements)

    f.run()
