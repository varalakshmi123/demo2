import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
if __name__ == "__main__":
    stmtdate = '03-Jun-2019'
    uploadFileName = '31st_May_19_Disbursement.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                recontype="Disburse", compressionType="zip",
                                resultkey='', reconName="MFI_Disbursement",uploadFileName = uploadFileName))

    load_loan_list = dict(type="PreLoader", properties=dict(loadType='Excel', source="T24", disableCarryFwd=True,
                                                            feedParams={"feedformatFile": 'Loan_List_T24.csv',
                                                                        "skiprows": 1},
                                                            feedPattern="Loan List*", resultkey="T24",
                                                            feedFilters=[
                                                                "df['Total Members'] = 1",
                                                                "df['Insurance'] = df['Insurance'].fillna(0).astype(np.float64)",
                                                                "df['Processing Fee'] = df['Processing Fee'].fillna(0).astype(np.float64)",
                                                                "df['OD Amount'] = df['OD Amount'].fillna(0).astype(np.float64)",
                                                                         "df['Insurance'] *= 1",
                                                                         "df['Processing Fee'] *= 1",
                                                                         "df['MEMBERS_ID'] = 1",

                                                                         "df['Center ID'] = df['Center ID'].fillna('0').astype(str).str.strip()",
                                                                         "df['Loan ID'] = df['Loan ID'].str.strip()",
                                                                         "df['Due Disbursement'] = df['LOAN_AMOUNT']-df['Insurance']-df['Processing Fee']"]))

    load_iboss_report = dict(type="PreLoader", properties=dict(loadType='Excel', source="IBOSS", disableCarryFwd=True,
                                                               feedParams={
                                                                   "feedformatFile": 'DISBURSED_STATUS_IBOSS.csv',
                                                                   "skiprows": 1},
                                                               feedPattern="Disbursement*", resultkey="iboss",
                                                               feedFilters=[
                                                                   "df['Actual members'] = 1",
                                                                   "df['CenterID'] = df['CenterID'].fillna('0').astype(str).str.strip()",
                                                                   "df['LoanID'] = df['LoanID'].str.strip()",
                                                                            "df['Actual Disbursement'] = df[df['Status'] == 'Disbursed']['NetAmount']",
                                                                            "df['Undisbursed Net Amount'] = df[df['Status'] == 'Undisbursed']['NetAmount']",
                                                                            "df['Undisbursed Gross Amount'] = df[df['Status'] == 'Undisbursed']['LoanAmt']",
                                                                            ]))


    vlookup = dict(type='VLookup',
                   properties=dict(data='T24', lookup='iboss', dataFields=["Center ID", "Loan ID"],
                                   includeCols=["Status","NetAmount"],
                                   lookupFields=["CenterID", "LoanID"],
                                   resultkey="T24orgfst"))
    vlookup_report = dict(type="ReportGenerator",
                          properties=dict(sources=[dict(source='T24orgfst', sourceTag="VLOOK_REPORT"
                                                        )],
                                          writeToFile=True, reportName='Loan StatusReport'))
    # t24_filters_due = dict(type="ExpressionEvaluator",
    #                                           properties=dict(source="T24",
    #                                                           expressions=["print df.columns",
    #                                                                        "df = df.groupby(['Branch ID','Center ID','Branch Name','Member.ID'],as_index = False)['MEMBERSI_D','Due Disbursement'].sum()"],

    #                                                           resultkey='T24_groupbyDue'))
    #
    # t24_filters_report = dict(type="ReportGenerator",
    #                       properties=dict(sources=[dict(source='T24_groupbyDue', sourceTag="I24_FILTERS_REPORT"
    #                                                     )],
    #                                       writeToFile=True, reportName='Due_Disbursement_Report'))
    t24_filters_due_second = dict(type="ExpressionEvaluator",
                           properties=dict(source="T24orgfst",
                                           expressions=[
                                                        "df = df.groupby(['Branch ID','Branch Name','Center ID'],as_index = False)['MEMBERS_ID','Due Disbursement'].sum()"],
                                           resultkey='T24_groupbyDue_Second'))

    t24_filters_report_second = dict(type="ReportGenerator",
                              properties=dict(sources=[dict(source='T24_groupbyDue_Second', sourceTag="T24_FILTERS_REPORT_SECOND"
                                                            )],
                                              writeToFile=True, reportName='Due_Disbursement_Report_Second'))
    t24_filters_actual_disbursed = dict(type="ExpressionEvaluator",
                                              properties=dict(source="T24orgfst",
                                                              expressions=[
                                                                           "df['Actual Mem'] = 1",
                                                                           "df['Actual Disbursement'] = 0",
                                                                           "df.loc[df['Status'] == 'Disbursed','Actual Disbursement'] = df['NetAmount']",
                                                                           "df = df.loc[df['Status'] == 'Disbursed']",
                                                                           "df = df.groupby(['Branch ID','Branch Name','Center ID'],as_index = False)['Actual Mem','Actual Disbursement'].sum()"],
                                                              resultkey='t24_actualDue'))

    t24_filters_actual_report_disbursed = dict(type="ReportGenerator",
                          properties=dict(sources=[dict(source='t24_actualDue', sourceTag="T24_FILTERS_ACTUAL_REPORT_DISBURSED")],
                                          writeToFile=True, reportName='Actual_Disbursement_Report'))

    due_actual_vlookup = dict(type='VLookup',
                              properties=dict(data='T24_groupbyDue_Second', lookup='t24_actualDue', dataFields=['Branch ID','Branch Name','Center ID'],
                                              includeCols=['Actual Mem','Actual Disbursement'],
                                              lookupFields=['Branch ID','Branch Name','Center ID'],
                                              resultkey="due_actual"))
    due_actual_vlookup_report = dict(type="ReportGenerator",
                                               properties=dict(sources=[dict(source='due_actual',
                                                                             sourceTag="DUE_ACTUAL_VLOOKUP_REPORT")],
                                                               writeToFile=True,
                                                               reportName='due_actual_vlookup_report'))

    t24_filters_actual_undisbursed = dict(type="ExpressionEvaluator",
                                        properties=dict(source="T24orgfst",
                                                        expressions=[
                                                                     "df['Actual Mem'] = 0",
                                                                     "df.loc[df['Status'] == 'Disbursed','Actual Mem'] =1",
                                                                     "df['Actual Disbursement'] = 0",
                                                                     "df['Undisbursed Gross Amount'] = 0",
                                                                     "df.loc[df['Status'] == 'UnDisbursed','Actual UnDisbursement'] = df['NetAmount']",
                                                                     "df.loc[df['Status'] == 'UnDisbursed','Undisbursed Gross Amount'] = df['LOAN_AMOUNT']",
                                                                     "df = df.loc[df['Status'] == 'UnDisbursed']",
                                                                     "df = df.groupby(['Branch ID','Branch Name','Center ID'],as_index = False)['Actual UnDisbursement','Undisbursed Gross Amount'].sum()"],
                                                        resultkey='t24_actualDueUndis'))
    t24_filters_actual_report_undisbursed = dict(type="ReportGenerator",
                                               properties=dict(sources=[dict(source='t24_actualDueUndis',
                                                                             sourceTag="T24_FILTERS_ACTUAL_REPORT_UNDISBURSED")],
                                                               writeToFile=True,
                                                               reportName='Actual_UnDisbursement_Report'))
    due_actual_undis_vlookup = dict(type='VLookup',
                              properties=dict(data='due_actual', lookup='t24_actualDueUndis',
                                              dataFields=['Branch ID', 'Branch Name', 'Center ID'],
                                              includeCols=['Actual UnDisbursement', 'Undisbursed Gross Amount'],
                                              lookupFields=['Branch ID', 'Branch Name', 'Center ID'],
                                              resultkey="due_actual_undis"))
    due_actual_undis_vlookup_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[dict(source='due_actual_undis',
                                                                   sourceTag="DUE_ACTUAL_UNDIS_VLOOKUP_REPORT")],
                                                     writeToFile=True,
                                                     reportName='due_actual_undis_vlookup_report'))

    final_vlookup1 = dict(type='SimpleMerge',
                         properties=dict(merge_data='due_actual_undis', merge_lookup='T24orgfst',
                                         merge_how = 'left',
                                         merge_dataFields=['Branch ID', 'Branch Name', 'Center ID'],
                                         merge_lookupFieldsFilter = ["df = df.loc[df['Status'] == 'UnDisbursed']"],
                                         merge_includeCols=['NetAmount','Member_ID','LoanID'],
                                         merge_lookupFields=['Branch ID', 'Branch Name', 'Center ID'],
                                         resultkey="final_vlookup1"))
    final_vlookup1_report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='final_vlookup1',
                                                              filterConditions = ["idx =0","SNo = df.index+1",
                                                                                    "df.insert(loc=idx,column='S.No',value=SNo)",
                                                                                  "df.loc[df.duplicated(['Branch Name','Center ID','MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement']),'Duplicated']='Yes'",
                                                                                  "df.loc[df['Duplicated'] =='Yes',['Branch Name','Center ID','MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement']]='0','0','0','0','0','0','0' ",
                                                                                  "l=['Branch ID','Branch Name','Center ID','MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']",
                                                                                  "df.loc[df.duplicated(l),'Duplicated']='Yes'",
                                                                                  "df.loc[df['Duplicated']=='Yes',l]='0' ",
                                                                                  "del df['Duplicated']","df.loc[len(df)+1,'Branch ID' ]= 'GRAND_TOTAL' ",

                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','MEMBERS_ID'] = df['MEMBERS_ID'].sum()",
                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Due Disbursement'] = df['Due Disbursement'].sum()",
                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual Mem'] = df['Actual Mem'].sum()",
                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual Disbursement'] = df['Actual Disbursement'].sum()",
                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual UnDisbursement'] = df['Actual UnDisbursement'].sum()",
                                                                                  # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Undisbursed Gross Amount'] = df['Undisbursed Gross Amount'].sum()"
                                                                                  ],
                                                              sourceTag="FINAL_VLOOKUP1_REPORT")],
                                                writeToFile=True,
                                                reportName='Final_Custom_Report'))
    final_vlookup1_report_2 = dict(type="ReportGenerator",
                                 properties=dict(sources=[dict(source= 'custom_reports.Final_Custom_Report' ,
                                                               filterConditions=[
                                                                                    "dfcus = df.copy()",

                                                                    "dfcus['MEMBERS_ID'] = dfcus['MEMBERS_ID'].astype(np.float64)",
                                                                    "dfcus['Due Disbursement'] = dfcus['Due Disbursement'].astype(np.float64)",
                                                                   "dfcus['Actual Mem'] = dfcus['Actual Mem'].astype(np.float64)",
                                                                   "dfcus['Actual Disbursement'] = dfcus['Actual Disbursement'].astype(np.float64)",
                                                                   "dfcus['Actual UnDisbursement'] = dfcus['Actual UnDisbursement'].astype(np.float64)",
                                                                   "dfcus['Undisbursed Gross Amount'] = dfcus['Undisbursed Gross Amount'].astype(np.float64)",
                                                                   # "dfcus[['MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']] = dfcus[['MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']].astype(np.int64)",


                                                                    "dfcus.loc[dfcus['Branch ID'] == 'GRAND_TOTAL',['MEMBERS_ID','Due Disbursement','Actual Mem','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']] = dfcus['MEMBERS_ID'].sum(),dfcus['Due Disbursement'].sum(),dfcus['Actual Mem'].sum(),dfcus['Actual Disbursement'].sum(),dfcus['Actual UnDisbursement'].sum(),dfcus['Undisbursed Gross Amount'].sum()",

                                                                   "dfcus['Branch ID'] = dfcus['Branch ID'].apply(lambda x: ' ' if x == '0' else x)",
                                                                   "dfcus['Branch Name'] = dfcus['Branch Name'].apply(lambda x: ' ' if x == '0' else x)",
                                                                   "dfcus[['Center ID','MEMBERS_ID','Actual Mem']] = dfcus[['Center ID','MEMBERS_ID','Actual Mem']].astype(str)",
                                                                   "dfcus['Center ID'] = dfcus['Center ID'].apply(lambda x: ' '  if x == '0' else x)",
                                                                   "dfcus['MEMBERS_ID'] = dfcus['MEMBERS_ID'].apply(lambda x: ' '  if x == '0.0' else x)",
                                                                   "dfcus['Actual Mem'] = dfcus['Actual Mem'].apply(lambda x: ' '  if x == '0.0' else x)",
                                                                   "df = dfcus.copy()"
                                                                   # "dfcus[dfcus.columns.tolist()] = dfcus[dfcus.columns.tolist()].astype(str).fillna('0')",

                                                                   # "dfcus = dfcus.replace(to_replace='^0+((\.0+)|(-0+)*)',value='',regex = True)",
                                                                   # "dfcus[['NetAmount','Member_ID','LoanID']] = dfcus[['NetAmount','Member_ID','LoanID']].replace(to_replace = 'nan',value = '0')",
                                                                   # "dfcus = dfcus.replace(to_replace='0.0',value='')",
                                                                   # "df = df.fillna('')",
                                                                   # "dfcus[['Due Disbursement','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']] = dfcus[['Due Disbursement','Actual UnDisbursement','Actual UnDisbursement','Undisbursed Gross Amount']].astype(str)",
                                                                   # "dfcus[['Due Disbursement','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']] = dfcus[['Due Disbursement','Actual Disbursement','Actual UnDisbursement','Undisbursed Gross Amount']].replace('nan','0')",
                                                                   # "df = dfcus.copy()"
                                                                   # "dfcus.loc[dfcus['Branch ID'] == 'GRAND_TOTAL','Due Disbursement'] = dfcus['Due Disbursement'].sum()"
                                                                   # "print dfcus['MEMBERS_ID']",
                                                                   # "print dfcus.dtypes"
                                                                                 # "dfcus['MEMBERS_ID'] = dfcus['MEMBERS_ID'].fillna('0')",



                                                                                 # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Due Disbursement'] = df['Due Disbursement'].sum()",
                                                                                 # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual Mem'] = df['Actual Mem'].sum()",
                                                                                 # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual Disbursement'] = df['Actual Disbursement'].sum()",
                                                                                 # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Actual UnDisbursement'] = df['Actual UnDisbursement'].sum()",
                                                                                 # "df.loc[df['Branch ID'] == 'GRAND_TOTAL','Undisbursed Gross Amount'] = df['Undisbursed Gross Amount'].sum()"
                                                                                 ],
                                                               sourceTag="FINAL_VLOOKUP1_REPORT2")],
                                                 writeToFile=True,
                                                 reportName='FInal_Disbursement_Summary'))

    # t24_filters_undisbursed = dict(type="ExpressionEvaluator",
    #                                     properties=dict(source="T24org",
    #                                                     expressions=[
    #                                                                  "df['Undisbursed Net Amount'] = 0",
    #                                                                  "df['Undisbursed Gross Amount'] = 0",
    #                                                                  "df.loc[df['Status'] == 'UnDisbursed','Undisbursed Net Amount'] = df['NetAmount']",
    #                                                                  "df.loc[df['Status'] == 'UnDisbursed','Undisbursed Gross Amount'] = df['LOAN_AMOUNT']",
    #                                                                  "df = df.loc[df['Status'] == 'UnDisbursed']",
    #                                                                  "df = df.groupby(['Branch ID','Branch Name','Center ID','Member_ID','Loan ID'],as_index = False)['Undisbursed Gross Amount'].sum()"],
    #                                                     resultkey='t24_Dueundis'))
    # t24_filters_report_undisbursed = dict(type="ReportGenerator",
    #                                            properties=dict(sources=[dict(source='t24_Dueundis',
    #                                                                          sourceTag="T24_FILTERS_ACTUAL_REPORT_DISBURSED")],
    #                                                            writeToFile=True,
    #                                                            reportName='UnDisbursement_Report'))
    # vlookup_memidloanid = dict(type='VLookup',
    #                properties=dict(data='t24_actualDueUndis', lookup='t24_Dueundis', dataFields=["Branch ID","Branch Name", "Center ID"],
    #                                includeCols=["Member.ID", "Loan ID"],
    #                                lookupFields=["Branch ID","Branch Name", "Center ID"],
    #                                resultkey="MEMIDLOANID"))
    # vlookup_report_memidloanid = dict(type="ReportGenerator",
    #                       properties=dict(sources=[dict(source='MEMIDLOANID', sourceTag="VLOOK_REPORT_MEMIDLOANID"
    #                                                     )],
    #                                       writeToFile=True, reportName='MEMIDLOANID_Report'))
    # vlookup_splitamount = dict(type='VLookup',
    #                            properties=dict(data='MEMIDLOANID', lookup='T24',
    #                                            dataFields=["Branch ID","Branch Name","Center ID","Member.ID","Loan ID"],
    #                                            includeCols=["NetAmount"],
    #                                            lookupFields=["Branch ID","Branch Name","Center ID","Member.ID","Loan ID"],
    #                                            resultkey="MEMIDLOANID"))
    # vlookup_report_splitamount = dict(type="ReportGenerator",
    #                                   properties=dict(
    #                                       sources=[dict(source='MEMIDLOANID', sourceTag="VLOOK_REPORT_SPLITAMOUNT"
    #                                                     )],
    #                                       writeToFile=True, reportName='MEMIDLOANID_Report'))


    # t24Dueundis_filters = dict(type="ExpressionEvaluator",
    #                                properties=dict(source="t24_actualDue",
    #                                                expressions=[
    #                                                    "df = df[['Branch ID','Branch Name','Center ID']]"],
    #                                                resultkey='t24_actualDue_part'))
    # vlookup_duedis = dict(type='VLookup',
    #                          properties=dict(data='T24_groupbyDue_Second', lookup='t24_actualDue',
    #                                          dataFields=["Branch ID",  "Branch Name","Center ID"],
    #                                          includeCols=["Actual Mem","Actual Disbursement"],
    #                                          lookupFields=["Branch ID", "Branch Name" ,"Center ID"],
    #                                          resultkey="FINALOUTPUTKEY"))
    # vlookup_report_duedis = dict(type="ReportGenerator",
    #                                 properties=dict(
    #                                     sources=[dict(source='FINALOUTPUTKEY', sourceTag="VLOOK_REPORT_DUEDIS"
    #                                                   )],
    #                                     writeToFile=True, reportName='FINALOUTPUT'))
    # vlookup_actualdis = dict(type='VLookup',
    #                            properties=dict(data='FINALOUTPUTKEY', lookup='t24_actualDueUndis',
    #                                            dataFields=["Branch ID", "Branch Name", "Center ID",],
    #                                            includeCols=["Actual UnDisbursement","Undisbursed Gross Amount"],
    #                                            lookupFields=["Branch ID","Branch Name", "Center ID", ],
    #                                            resultkey="FINALOUTPUTKEY"))
    # vlookup_report_actualdis = dict(type="ReportGenerator",
    #                              properties=dict(
    #                                  sources=[dict(source='FINALOUTPUTKEY', sourceTag="VLOOK_REPORT_ACTUALDIS"
    #                                                )],
    #                                  writeToFile=True, reportName='FINALOUTPUT'))
    # # vlookup_actualundis = dict(type='VLookup',
    # #                          properties=dict(data='FINALOUTPUTKEY', lookup='t24_actualDueUndis',
    # #                                          dataFields=["Branch ID", "Branch Name", "Center ID"],
    # #                                          includeCols=["Actual UnDisbursement", "Undisbursed Gross Amount"],
    # #                                          lookupFields=["Branch ID", "Branch Name", "Center ID"],
    # #                                          resultkey="FINALOUTPUT"))
    # # vlookup_report_actualundis = dict(type="ReportGenerator",
    # #                                 properties=dict(
    # #                                     sources=[dict(source='FINALOUTPUTKEY', sourceTag="VLOOK_REPORT_ACTUALUNDIS"
    # #                                                   )],
    # #                                     writeToFile=True, reportName='FINALOUTPUT'))
    # vlookup_memidloanid = dict(type='VLookup',
    #                properties=dict(data='FINALOUTPUTKEY', lookup='t24_Dueundis', dataFields=["Branch ID","Branch Name", "Center ID"],
    #                                includeCols=["Member_ID", "Loan ID"],
    #                                lookupFields=["Branch ID","Branch Name", "Center ID"],
    #                                resultkey="FINALOUTPUTKEY"))
    # vlookup_report_memidloanid = dict(type="ReportGenerator",
    #                       properties=dict(sources=[dict(source='FINALOUTPUTKEY', sourceTag="VLOOK_REPORT_MEMIDLOANID"
    #                                                     )],
    #                                       writeToFile=True, reportName='FINALOUTPUT'))
    # vlookup_splitamount = dict(type='VLookup',
    #                            properties=dict(data='FINALOUTPUTKEY', lookup='T24',
    #                                            dataFields=["Branch ID","Branch Name","Center ID","Member_ID","Loan ID"],
    #                                            includeCols=["NetAmount"],
    #                                            lookupFields=["Branch ID","Branch Name","Center ID","Member_ID","Loan ID"],
    #                                            resultkey="FINALOUTPUTKEY"))
    # vlookup_report_splitamount = dict(type="ReportGenerator",
    #                                   properties=dict(
    #                                       sources=[dict(source='FINALOUTPUTKEY', sourceTag="VLOOK_REPORT_SPLITAMOUNT"
    #                                                     )],
    #                                       writeToFile=True, reportName='FINALOUTPUT'))
    # t24_filters_final = dict(type="ExpressionEvaluator",
    #                                     properties=dict(source="T24org",
    #                                                     expressions=[
    #                                                         "df = df.loc[df['Status'] == 'UnDisbursed']",
    #                                                         ],
    #                                                     resultkey='T24orgfinal'))
    # t24_filters_final_report = dict(type="ReportGenerator",
    #                                   properties=dict(
    #                                       sources=[dict(source='T24orgfinal', sourceTag="t24_filters_final_report"
    #                                                     )],
    #                                       writeToFile=True, reportName='T24_FILTER_CHECK'))
    # vlookup_splitall = dict(type='VLookup',
    #                            properties=dict(data='T24orgfinal', lookup='FINALOUTPUTKEY',
    #                                            dataFields=["Branch ID", "Branch Name", "Center ID"],
    #                                            includeCols=["MEMBERS_ID","Due Disbursement","Actual Mem","Actual Disbursement","Actual UnDisbursement","Undisbursed Gross Amount"],
    #                                            lookupFields=["Branch ID", "Branch Name", "Center ID"],
    #                                            resultkey="FINALOUTPUTKEY"))
    # vlookup_report_splitall = dict(type="ReportGenerator",
    #                                   properties=dict(
    #                                       sources=[dict(source='FINALOUTPUTKEY',sourceTag="VLOOK_REPORT_SPLITALL"
    #                                                     )],
    #                                       writeToFile=True, reportName='FINALOUTPUT_CHECK'))


    # vlookup_report_actualdis = dict(type="ReportGenerator",
    #                                   properties=dict(
    #                                       sources=[dict(source='MEMIDLOANID', sourceTag="VLOOK_REPORT_ACTUALDIS"
    #                                                     )],
    #                                       writeToFile=True, reportName='MEMIDLOANID_Report'))

    output_filter_sum= dict(type="ExpressionEvaluator",
                                   properties=dict(source="due_actual_undis",
                                                   expressions=[
                                                       "payload['dfout'] = pd.DataFrame([{'Due Disbursement_sum': df['Due Disbursement'].sum(), 'Actual Disbursement_sum': df['Actual Disbursement'].sum(),'UnDisbursement Net Loan Amount_sum': df['Actual UnDisbursement'].sum()}])",
                                                       "payload['dfout']['Difference'] = payload['dfout']['Due Disbursement_sum'] -(payload['dfout']['Actual Disbursement_sum'] +payload['dfout']['UnDisbursement Net Loan Amount_sum'])",

                                                   ],
                                                   resultkey='unwanted'))
    output_filter_sum_report = dict(type="ReportGenerator",
                                          properties=dict(sources=[dict(source='dfout',
                                                                        sourceTag="OUTPUT_FILTER_SUM_REPORT")],
                                                          writeToFile=True,
                                                          reportName='OUTPUT_SUMMARY'))

    # iboss_filters = dict(type="ExpressionEvaluator",
    #                      properties=dict(source="iboss",
    #                                      expressions=["df['Actual Members'] = 1",
    #                                                   "print df.columns",
    #                                                   "df['Actual Disbursement'] = df[df['Status'] == 'Disbursed']['NetAmount'].sum()",
    #                                                   "df['Undisbursed Net Amount'] = df[df['Status'] == 'Undisbursed']['NetAmount'].sum()",
    #                                                   "df['Undisbursed Gross Amount'] = df[df['Status'] == 'Disbursed']['NetAmount'].sum()",
    #                                                   "df['Undisbursed Member ID'] = df[df['Status'] == 'Undisbursed']['Member ID']",
    #                                                   "df['Undisbursed Loan ID'] = df[df['Status'] == 'Undisbursed']['Loan ID']",
    #                                                   "df = df.groupby(['Branch ID','Branch Name','Center ID','Loan ID','Member ID']).sum().reset_index()"],
    #                                      resultkey='iboss'))
    #
    # t24_report = dict(type="ReportGenerator",
    #                   properties=dict(
    #                       sources=[dict(source='T24', sourceTag="T24", filterConditions=[])], writeToFile=True,
    #                       reportName='T24_Report'))
    #
    # iboss_report = dict(type="ReportGenerator",
    #                     properties=dict(
    #                         sources=[dict(source='iboss', sourceTag="iboss", filterConditions=[])], writeToFile=True,
    #                         reportName='iboss_Report'))

    dump_data = dict(type="DumpData", properties=dict(matched='any'))

    meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, load_loan_list, load_iboss_report,vlookup,vlookup_report,
                t24_filters_due_second,t24_filters_report_second,
                t24_filters_actual_disbursed,t24_filters_actual_report_disbursed,
                due_actual_vlookup,due_actual_vlookup_report,
                t24_filters_actual_undisbursed,t24_filters_actual_report_undisbursed,
                due_actual_undis_vlookup,due_actual_undis_vlookup_report,
                final_vlookup1,final_vlookup1_report,final_vlookup1_report_2,


                output_filter_sum,output_filter_sum_report,
                dump_data, meta_info]

    f = FlowRunner(elements)

    f.run()
