from urllib.request import Request, urlopen
import bs4 as bs
req = Request('https://www.worldcoinindex.com/coin/ethereum', headers={'User-Agent': 'chrome/Version 69.0.3497.100 '})
webpage = urlopen(req).read()
soup = bs.BeautifulSoup(webpage, 'lxml')
table = soup.find('table', class_="coin-markets-table")
# print(table.text)

rows=table.find_all("tr")
heads=table.find_all("th")
heads=[x.text.strip()for x in heads]
print(heads)
# print(rows)
for row in rows:
    cols=row.find_all('td')
    cols=[x.text.strip() for x in cols]
    print(cols)