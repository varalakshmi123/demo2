import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":

    stmtdate = '22-May-2019'
    uploadFileName = 'Hypo.zip'
    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                recontype="Hypo", compressionType="zip",
                                resultkey='', reconName="MF_HYPO", uploadFileName=uploadFileName))
    load_hypo = dict(type="PreLoader", properties=dict(loadType='CSV', source="OPS",disableCarryFwd = True,
                                                   feedParams=dict(delimiter="|", skiprows=1,keep_default_na=False,
                                                                   feedformatFile='EQT_MF_HYPO_Structure.csv'),
                                                   feedPattern="OPS.HYPO_*",
                                                   resultkey="opsdf", feedFilters=["print len(df)"]))
    load_worksheet = dict(type="PreLoader", properties=dict(loadType='Excel', source="WORK",disableCarryFwd = True,
                                                   feedParams=dict(delimiter="", skiprows=1,
                                                                   feedformatFile='HYPO_WRITTEN_Structure.csv'),
                                                   feedPattern="Fund ID *",
                                                   resultkey="workdf"))

    hypo_Securitised_Weaker_Section_filter = dict(type="ExpressionEvaluator",
                        properties=dict(source="opsdf",
                                        expressions=[
                                            "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'Securitised Weaker Section']",
                                            "dffree = dffree.fillna('NaN')",
                                            "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                            "df['Fund ID'] = df['Fund ID'].astype(str)",
                                            "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]",],
                                            # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                        resultkey='opsdf_securitised_filter'))
    hypo_Securitised_Weaker_Section_filter_report = dict(type="ReportGenerator",
                                               properties=dict(sources=[
                                                   dict(source='opsdf_securitised_filter', sourceTag="HYPO_SECURITISED_WEAKER_SECTION_FILTER_REPORT"
                                                        )],
                                                   writeToFile=True, reportName='Securitised_Weaker_Section'))
    hypo_freeassetsweaker_filter = dict(type="ExpressionEvaluator",
                        properties=dict(source="opsdf",
                                        expressions=[
                                            "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'Free Assets Weaker Section']",
                                            "dffree = dffree.fillna('NaN')",
                                            "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                            "df['Fund ID'] = df['Fund ID'].astype(str)",
                                            "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]",],
                                            # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                        resultkey='opsdf_freeasset_filter'))
    hypo_freeassetsweaker_filter_report = dict(type="ReportGenerator",
                               properties=dict(sources=[
                                   dict(source='opsdf_freeasset_filter', sourceTag="HYPO_FREEASSETSWEAKER_FILTER_REPORT"
                                        )],
                                   writeToFile=True, reportName='Free_Assets_Weaker_Section'))
    hypo_Assets_Hypothecated_to_PSLC_filter = dict(type="ExpressionEvaluator",
                                                  properties=dict(source="opsdf",
                                                                  expressions=[
                                                                      "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'Assets Hypothecated to PSLC']",
                                                                      "dffree = dffree.fillna('NaN')",
                                                                      "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                      "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                      "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                  # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                  resultkey='opsdf_Assets_Hypothecated_to_PSLC_filter'))
    hypo_Assets_Hypothecated_to_PSLC_filter_report = dict(type="ReportGenerator",
                                               properties=dict(sources=[
                                                   dict(source='opsdf_Assets_Hypothecated_to_PSLC_filter', sourceTag="HYPO_ASSETS_HYPOTHECATED_TO_PSLC_FILTER_REPORT"
                                                        )],
                                                   writeToFile=True, reportName='Hypothecated_PSLC'))
    hypo_HYP_to_Bank_Weaker_Section_filter = dict(type="ExpressionEvaluator",
                                                   properties=dict(source="opsdf",
                                                                   expressions=[
                                                                       "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'HYP to Bank Weaker Section']",
                                                                       "dffree = dffree.fillna('NaN')",
                                                                       "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                       "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                       "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                   # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                   resultkey='opsdf_HYP_to_Bank_Weaker_Section_filter'))
    hypo_HYP_to_Bank_Weaker_Section_filter_report = dict(type="ReportGenerator",
                                                          properties=dict(sources=[
                                                              dict(source='opsdf_HYP_to_Bank_Weaker_Section_filter',
                                                                   sourceTag="HYPO_TO_BANK_WEAKER_SECTION_FILTER_REPORT"
                                                                   )],
                                                              writeToFile=True,
                                                              reportName='HYP_Bank_Weaker_Section'))

    hypo_nbl_refinananceweaker_filter =dict(type="ExpressionEvaluator",
                                                   properties=dict(source="opsdf",
                                                                   expressions=[
                                                                       "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'NBL & Refinance Weaker Section']",
                                                                       "dffree = dffree.fillna('NaN')",
                                                                       "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                       "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                       "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                   # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                   resultkey='opsdf_hypo_nbl_refinananceweaker_filter'))
    hypo_nbl_refinananceweaker_filter_report = dict(type="ReportGenerator",
                                               properties=dict(sources=[
                                                   dict(source='opsdf_hypo_nbl_refinananceweaker_filter', sourceTag=" HYPO_NBL_REFINANACEWEAKER"
                                                        )],
                                                   writeToFile=True, reportName='NBL&Refinanance_Weaker_Section'))

    writtencases_filter =dict(type="ExpressionEvaluator",
                                                   properties=dict(source="opsdf",
                                                                   expressions=[
                                                                       "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'Written cases']",
                                                                       "dffree = dffree.fillna('NaN')",
                                                                       "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                       "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                       "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                   # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                   resultkey='opsdf_hypo_writtencases_filter'))
    writtencases_filter_report = dict(type="ReportGenerator",
                                                    properties=dict(sources=[
                                                        dict(source='opsdf_hypo_writtencases_filter',
                                                             sourceTag=" WRITTENCASES_FILTER"
                                                             )],
                                                        writeToFile=True, reportName='Writtencases'))
    ibpcweaker_filter =dict(type="ExpressionEvaluator",
                                                   properties=dict(source="opsdf",
                                                                   expressions=[
                                                                       "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'IBPC Weaker Section']",
                                                                       "dffree = dffree.fillna('NaN')",
                                                                       "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                       "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                       "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                   # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                   resultkey='opsdf_hypo_ibpcweaker_filter'))
    ibpcweaker_filter_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='opsdf_hypo_ibpcweaker_filter',
                                               sourceTag=" IBPCWEAKER_FILTER"
                                               )],
                                          writeToFile=True, reportName='IBPC_Weaker_Section'))
    overall_weaker_section_filter = dict(type="ExpressionEvaluator",
                                                   properties=dict(source="opsdf",
                                                                   expressions=[
                                                                       "dffree = payload['workdf'][payload['workdf']['Rep Name'] == 'Overall Weaker Section']",
                                                                       "dffree = dffree.fillna('NaN')",
                                                                       "vlist = str(dffree['FUND_ID'].values[0]).split(',') if isinstance(dffree['FUND_ID'].values[0], int)  else dffree['FUND_ID'].values[0].split(',')",
                                                                       "df['Fund ID'] = df['Fund ID'].astype(str)",
                                                                       "df = df[df['Fund ID'].isin([str(val.strip()) for val in vlist])]", ],
                                                                   # "df = df.drop_duplicates(['Branch ID','Center ID','Loan ID'])"],
                                                                   resultkey='opsdf_hypo_overall_weaker_section_filter'))
    overall_weaker_filter_report = dict(type="ReportGenerator",
                                    properties=dict(sources=[
                                        dict(source='opsdf_hypo_overall_weaker_section_filter',
                                             sourceTag=" OVERALL_WEAKER_FILTER"
                                             )],
                                        writeToFile=True, reportName='Overall_Weaker_Section'))



    # hypo_filter2 = dict(type="ExpressionEvaluator",
    #                        properties=dict(source="opsdf",
    #                                        expressions=[
    #                                            "df = df.loc[df['Fund ID'].isin(['993','994','995'])]",
    #                                                     "df = df.groupby(['Branch ID','Center ID','Loan ID'],as_index = False)['Annual  Household Income','Disbursed Amount','Collection Amount','Outstanding Amount'].sum()"],
    #                                        resultkey='opsdf_filter2'))
    # hypo_filter_report2= dict(type="ReportGenerator",
    #                                  properties=dict(sources=[
    #                                      dict(source='opsdf_filter2', sourceTag="HYPO_FILTER_REPORT2"
    #                                           )],
    #                                                  writeToFile=True, reportName='Free Assets Weaker Section_2'))
    # hypo_filter3 = dict(type="ExpressionEvaluator",
    #                    properties=dict(source="opsdf",
    #                                    expressions=[
    #                                        "df = df.loc[df['Fund ID'].isin(['993','994','995'])]",
    #                                        "df = df.groupby(['Branch ID','Loan ID'],as_index = False)['Annual  Household Income','Disbursed Amount','Collection Amount','Outstanding Amount'].sum()"],
    #                                    resultkey='opsdf_filter3'))
    # hypo_filter_report3 = dict(type="ReportGenerator",
    #                           properties=dict(sources=[
    #                               dict(source='opsdf_filter3', sourceTag="HYPO_FILTER_REPORT3"
    #                                    )],
    #                               writeToFile=True, reportName='Free Assets Weaker Section_3'))
    # hypo_filter4 = dict(type="ExpressionEvaluator",
    #                     properties=dict(source="opsdf",
    #                                     expressions=[
    #                                         "df = df.loc[df['Fund ID'].isin(['993','994','995'])]",
    #                                         "df = df.groupby(['Branch ID','Center ID'],as_index = False)['Annual  Household Income','Disbursed Amount','Collection Amount','Outstanding Amount'].sum()"],
    #                                     resultkey='opsdf_filter4'))
    # hypo_filter_report4 = dict(type="ReportGenerator",
    #                            properties=dict(sources=[
    #                                dict(source='opsdf_filter4', sourceTag="HYPO_FILTER_REPORT4"
    #                                     )],
    #                                writeToFile=True, reportName='Free Assets Weaker Section_4'))

    dumpData = dict(type="DumpData", properties=dict())

    metaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init,load_hypo,load_worksheet,
                hypo_Securitised_Weaker_Section_filter,hypo_Securitised_Weaker_Section_filter_report,
                hypo_freeassetsweaker_filter,hypo_freeassetsweaker_filter_report,
                hypo_Assets_Hypothecated_to_PSLC_filter,hypo_Assets_Hypothecated_to_PSLC_filter_report,
                hypo_HYP_to_Bank_Weaker_Section_filter,hypo_HYP_to_Bank_Weaker_Section_filter_report,
                hypo_nbl_refinananceweaker_filter,hypo_nbl_refinananceweaker_filter_report,
                writtencases_filter,writtencases_filter_report,
                ibpcweaker_filter,ibpcweaker_filter_report,
                overall_weaker_section_filter,overall_weaker_filter_report,
                dumpData,metaInfo
                ]
    f = FlowRunner(elements)

    f.run()
