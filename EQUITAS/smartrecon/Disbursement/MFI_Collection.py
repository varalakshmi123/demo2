import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
if __name__ == "__main__":
    stmtdate = '04-Feb-2019'
    uploadFileName = '01022019.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                recontype="Collection", compressionType="zip",
                                resultkey='', reconName="MFI_Collection",uploadFileName = uploadFileName))

    load_loan_list = dict(type="PreLoader", properties=dict(loadType='Excel', source="T24", disableCarryFwd=True,
                                                            feedParams={"feedformatFile": 'T24_Collection_Structure.csv',
                                                                        "skiprows": 1},
                                                            feedPattern="Advance Collection*.*", resultkey="T24",
                                                            feedFilters=[]))
    advance_coll = dict(type="PreLoader", properties=dict(loadType='Excel', source="Advance_Coll", disableCarryFwd=True,
                                                            feedParams={"feedformatFile": 'Advance_Coll_Structure.csv',
                                                                        "skiprows": 1},
                                                            feedPattern="Advance Coll*.*", resultkey="advance_coll",
                                                            feedFilters=[]))


    dump_data = dict(type="DumpData", properties=dict(matched='any'))

    meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, load_loan_list,advance_coll,
                dump_data, meta_info]

    f = FlowRunner(elements)

    f.run()





