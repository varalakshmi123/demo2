import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '01-Feb-2018'
    uploadFileName = 'Disburse.zip'
    basePath = '/usr/share/nginx/smartrecon/mft/'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="Disburse", compressionType="zip",
                                resultkey='',
                                reconName="DisburseRecon"))


    gl_filters = ["df['Debit_Credit'] = 'C'",
                  "df.loc[df['Debit']>0,'Debit_Credit']= 'D'",
                  "df['Instrno'] = df['Instrno'].str[-6:]",
                  "df['Instrno'] = df['Instrno'].fillna(0)",
                  "df['Debit'] = df['Debit'].astype(np.float64)",
                  "df['Credit'] = df['Credit'].astype(np.float64)",
                  "df['DC_SUM'] = df['Debit'] + df['Credit']"]

    stmnt_filters = ["df['DR'] = df['DR'].replace(',','').astype('float64')", "df['CR'] = df['CR'].fillna(0)",
                     "df['DR'] = df['DR'].fillna(0)", "df['CHQ NO'] = df['CHQ NO'].str[-6:]",
                     "df['Debit_Credit'] = 'CREDIT'", "df.loc[df['DR']>0,'Debit_Credit']='D'",
                     "df['DR'] = df['DR'].astype(np.float64)", "df['CR'] = df['CR'].astype(np.float64)",
                     "df['DC_SUM'] = df['DR'] + df['CR']"]
    
    esfb_filters = ["df= df[df['Cheque/Ref. No.'].astype(str).str.isdigit()]", "print df['Cheque/Ref. No.']"]
    unmatch_filters = []
    load_gl = dict(type="PreLoader", properties=dict(loadType='Excel', source="GL",
                                                     feedParams={"feedformatFile": 'Disburse_gl_Structure.csv',
                                                                 "skiprows": 3, "skipfooter": 2},
                                                     feedPattern="GL*.*xls",
                                                     resultkey="gldf", feedFilters=gl_filters))

    load_statement = dict(type="PreLoader", properties=dict(loadType='Excel', source="STMNT",
                                                            feedParams={
                                                                "feedformatFile": 'Disburse_Statement_Structure.csv',
                                                                "skiprows": 1},
                                                            feedPattern="Statement_*.*xls",
                                                            resultkey="stmntdf", feedFilters=stmnt_filters))
    load_ESFB = dict(type="PreLoader", properties=dict(loadType='Excel', source="ESFB",
                                                       feedParams={
                                                           "feedformatFile": 'Disburse_ESFB_Structure.csv',
                                                           "skiprows": 1, "skipfooter": 2},
                                                       feedPattern="ESFB*.*xlsx",
                                                       resultkey="esfbdf", feedFilters=esfb_filters))

    load_UNMATCH = dict(type="PreLoader", properties=dict(loadType='Excel', source="UNMATCH",
                                                          feedParams={
                                                              "feedformatFile": 'Disburse_UNMATCH_Structure.csv',
                                                              "skiprows": 1, "skipfooter": 2},
                                                          feedPattern="STMNT_*.*xlsx",
                                                          resultkey="unmatchdf", feedFilters=unmatch_filters))


    rev_gl = dict(type="NWayMatch", properties=dict(sources=[dict(source="gldf",
                                                                  columns=dict(
                                                                      keyColumns=['Instrno'],
                                                                      amountColumns=["DC_SUM"],
                                                                      crdrcolumn=["Debit_Credit"],
                                                                      CreditDebitSign=False),
                                                                  sourceTag="GL")], matchResult="results"))
    rev_stmnt = dict(type="NWayMatch", properties=dict(sources=[dict(source="stmntdf",
                                                                     columns=dict(
                                                                         keyColumns=["CHQ NO"],
                                                                         amountColumns=["DC_SUM"],
                                                                         crdrcolumn=["Debit_Credit"],
                                                                         CreditDebitSign=False),
                                                                     sourceTag="STMNT")], matchResult="results"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="results.GL", columns=dict(
                          keyColumns=['Debit', 'Credit', 'Instrno'], sumColumns=[],
                          matchColumns=['Debit', 'Credit', 'Instrno']),
                                                    sourceTag="GL"),
                                               dict(source="results.STMNT", columns=dict(
                                                   keyColumns=['CR', 'DR', 'CHQ NO'], sumColumns=[],
                                                   matchColumns=['CR', 'DR', 'CHQ NO', ]),
                                                    sourceTag="STMNT")],
                                      matchResult="results"))

    vlookup_esfb = dict(type='VLookup',
                        properties=dict(data='results.STMNT', lookup='esfbdf', dataFields=['CHQ NO'],
                                        lookupFields=['Cheque/Ref. No.'], markers={'ESF Match': 'MATCHED'},
                                        resultkey="results.STMNT"))
    # load_esbf_nan = dict(type="ExpressionEvaluator",
    #                     properties=dict(source="results.STMNT", resultkey="results.STMNT",
    #                                     expressions=["df = df[df[''] == 'Successful']"]))

    unsucc_remarks = dict(type="ExpressionEvaluator",
                          properties=dict(source="results.STMNT",
                                          expressions=[
                                              "df.loc[df['ESF Match'] == '','ESF Match'] = 'UNMATCHED' "],
                                          resultkey='results.STMNT'))

    esfb_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="esfbdf", columns=dict(
                          keyColumns=['Cheque/Ref. No.'], sumColumns=[],
                          matchColumns=['Cheque/Ref. No.']),
                                                    sourceTag="ESFB"),
                                               dict(source="unmatchdf", columns=dict(
                                                   keyColumns=['CHQ NO'], sumColumns=[],
                                                   matchColumns=['CHQ NO']),
                                                    sourceTag="STMNT")],
                                      matchResult="results"))


    gen_summary = dict(type="GenerateReconSummary",
    properties=dict(
    sources=[dict(sourceTag="ESFB", aggrCol=['']),dict(sourceTag="GL", aggrCol=['Debit', 'Credit', ])]))

    bank_debit_report = dict(type="ReportGenerator",
                          properties=dict(sources=[dict(source='results.STMNT', sourceTag="STMNT",
                                                        filterConditions=[
                                                            "df = df[df['GL Match'].str.strip()=='UNMATCHED']",
                                                            "df = df[df['Debit_Credit'] == 'D']",
                                                            "df = df[['PARTICULARS','DATE','CHQ NO','DC_SUM']]","df = df.rename(columns = {'PARTICULARS':'Description','DATE':'Date','CHQ NO':'Cheque/Ref. No.','DC_SUM':' Amount in Rs. '})",
                                                        "df['Remarks'] = ' ' ","df['Cleared/Cancelled date'] = ' ' "])],
                                          writeToFile=True, reportName='BANK Debit Pending'))

    bank_credit_report = dict(type="ReportGenerator", properties=dict(sources=[
        dict(source='results.STMNT', sourceTag="STMNT",
             filterConditions=["df = df[df['GL Match'] == ' UNMATCHED']",
                               "df = df[df['Debit_Credit'] == 'C']",
                               "df =df[['PARTICULARS','DATE','CHQ NO','DC_SUM']]"])],
        writeToFile=True, reportName='BANK credit Pending'))

    uno_credit_report = dict(type="ReportGenerator", properties=dict(sources=[
        dict(source='results.GL', sourceTag="GL",
             filterConditions=[
                 "df = df[(df['STMNT Match'] == 'UNMATCHED') & (df['Debit_Credit'] == 'D')]",
                 "df =df[['Narration','STATEMENT_DATE','Instrno','DC_SUM']]",
                 "df = df.rename(columns = {'Narration':'Description','STATEMENT_DATE':'Date','Instrno':'Cheque/Ref. No.','DC_SUM':' Amount in Rs. '})"])],
        writeToFile=True, reportName=' UNO Credit Pending'))

    uno_debit_report = dict(type="ReportGenerator", properties=dict(sources=[
        dict(source='results.GL', sourceTag="GL",
             filterConditions=["df = df[(df['STMNT Match'] == 'UNMATCHED') & (df['Debit_Credit'] == 'C')]",
                               "df =df[['Narration','STATEMENT_DATE','Instrno','DC_SUM']]",
                               "df = df.rename(columns = {'Narration':'Description','STATEMENT_DATE':'Date','Instrno':'Cheque/Ref. No.','DC_SUM':' Amount in Rs. '})"])],
        writeToFile=True, reportName='UNO Debit Pending'))

    dump_data = dict(type="DumpData", properties=dict(matched='any'))
    elements = [init, load_gl, load_statement, load_ESFB, rev_gl, rev_stmnt, nway_match,vlookup_esfb,
                unsucc_remarks,bank_credit_report,bank_debit_report,uno_credit_report,uno_debit_report,dump_data]
    f = FlowRunner(elements)

    f.run()
