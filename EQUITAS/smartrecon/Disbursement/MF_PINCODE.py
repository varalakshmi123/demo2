import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '09-Nov-2018'
    uploadFileName = 'Pincode_exists.zip'


    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                recontype="PIN", compressionType="zip",
                                resultkey='', reconName="MF_PINCODE", uploadFileName=uploadFileName))
    load_master = dict(type="PreLoader", properties=dict(loadType='Excel', source="MASTER", disableCarryFwd=True,
                                                         feedParams=dict(delimiter="", skiprows=1,
                                                                         feedformatFile='MASTER_PIN_Structure.csv'),
                                                         feedPattern="pincode*.*",
                                                         resultkey="masterdf", feedFilters=[]))
    memberfilter = ["df['CENTER ID']=df['CENTER ID'].astype(str).str.replace('\.(0)*','')"]
    load_member = dict(type="PreLoader", properties=dict(loadType='Excel', source="MEMBER", disableCarryFwd=True,
                                                         feedParams=dict(delimiter="", skiprows=1,
                                                                         feedformatFile='MEMBER_PIN_Structure.csv'),
                                                         feedPattern="memberdata.*",feedFilters = memberfilter,
                                                         resultkey="memberdf"))
    Detail_Report_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="memberdf",
                                                expressions=[
                                                             "pinlist =  payload['masterdf']['pincode'].tolist()",
                                                             "df = df[~df['PINCODE'].isin(pinlist)]",
                                                             ],

                                                resultkey='Detail_Report_df'))

    pincodeFilter = dict(type="ExpressionEvaluator",
                         properties=dict(source="memberdf",
                                         expressions=["payload['mdf'] = df[['CENTER ID', 'MEMBER NAME']]",
                                                    "payload['mdf'] = payload['mdf'].fillna('')",
                                                    "payload['mdf'] = payload['mdf'].drop_duplicates(subset=['CENTER ID', 'MEMBER NAME'])",
                                                    "payload['mdf'] = payload['mdf'].groupby(['CENTER ID']).count().reset_index()",
                                                    "payload['pdf'] = df[['CENTER ID', 'PINCODE']]",
                                                    "payload['pdf'] = payload['pdf'].fillna('')",
                                                    "payload['pdf'] = payload['pdf'].drop_duplicates(subset=['CENTER ID', 'PINCODE'])",
                                                    "payload['pdf'] = payload['pdf'].groupby(['CENTER ID']).count().reset_index()",

                                                    "payload['mdf']['PINCODE'] = payload['pdf']['PINCODE']",
                                                    "df = df[['BRANCH ID', 'CENTER ID', 'REGION']]",
                                                    ],resultkey = 'Det'))




    Detail_Report_report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='Detail_Report_df',
                                                              sourceTag="DETAIL_REPORT")],
                                                writeToFile=True,
                                                reportName='Detail_Report'))


    vlookup_ifsc = dict(type='VLookup',
                            properties=dict(data='mdf', lookup='memberdf', dataFields=['CENTER ID'],
                                            lookupFields=['CENTER ID'],
                                            includeCols=['BRANCH ID','REGION'],
                                            lookupFieldsFilter=[],
                                            resultkey="mdf"))


    Pincode_Report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='mdf',
                                                              sourceTag="PINCODE_REPORT")],
                                                writeToFile=True,
                                                reportName='Pincode_Summary'))

    pincodeReportFilter = dict(type="ExpressionEvaluator",
                         properties=dict(source="custom_reports.Pincode_Summary",
                                         expressions=["df.rename(columns={'MEMBER NAME':'MEMBER COUNT','PINCODE':'PINCODE_COUNT'},inplace=True)",
                                                      "df=df.groupby(['CENTER ID','MEMBER COUNT','PINCODE_COUNT']).count().reset_index()",
                                                      "df = df[df['PINCODE_COUNT'] > 1]",
                                                      "df=pd.merge(payload['memberdf'],df,on=['CENTER ID'],how='inner',suffixes=('', '_y'))",
                                                      "df=df[['BRANCH ID','CENTER ID','PINCODE_COUNT','PINCODE','MEMBER COUNT','REGION','CUSTOMER ID','SUBGROUP ID','E.KYC','CITY']]"
                                                      ], resultkey='pinFilter'))



    Pincode_Count_Report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='pinFilter',
                                                              sourceTag="PINCODE_Count_REPORT")],
                                                writeToFile=True,
                                                reportName='Pincode_Count'))
    Pincode_Summary_Report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='pinFilter',filterConditions = ["df = df.groupby(['CENTER ID', 'PINCODE_COUNT', 'PINCODE']).size().reset_index(name='counts')",
   "df.loc[df.duplicated(['CENTER ID', 'PINCODE_COUNT']), 'Duplicated'] = 'Yes' ",
    "df.loc[df['Duplicated'] == 'Yes', ['CENTER ID', 'PINCODE_COUNT']] = '', '0'","df['CENTER ID'] = df['CENTER ID'].str.strip()",
                                                          # "df['CENTER ID'] = df['CENTER ID'].str.replace(' ','0').astype(np.int64)",
                                                                                                     "df['PINCODE_COUNT'] = df['PINCODE_COUNT'].astype(np.int64) ",

                                                                                                     "del df['Duplicated']",
                                                                                                     "df.loc[df['PINCODE_COUNT'] == 0,'PINCODE_COUNT'] = '' ",
                                                                                                     "df.loc[df['CENTER ID'] == 0,'CENTER ID'] = '' ",
                                                                                                    ],
                                                              sourceTag="PINCODE_SUMMARY_REPORT")],
                                                writeToFile=True,
                                                reportName='Pincode_Summary_Count'))



    dump_data = dict(type="DumpData", properties=dict())
    metaInfo = dict(type="GenerateReconMetaInfo", properties=dict())
    elements = [init, load_master, load_member, Detail_Report_filter,pincodeFilter, Detail_Report_report,vlookup_ifsc,Pincode_Report,pincodeReportFilter,Pincode_Count_Report,Pincode_Summary_Report,dump_data, metaInfo]
    f = FlowRunner(elements)
    f.run()
