from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "21-Nov-2017"
    basePath = "/Users/shivashankar/Desktop/ReconData/Equitas/Raw Data"
    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    sfms_filters = []

    issuerswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                    'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                    'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                    'df["RRNISSUER"]=df["RRNISSUER"].astype(np.int64)',
                    'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                    'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)'
                    ]
    inititalizer = dict(id=1, next=2, type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="ATM", compressionType="", resultkey='',ignoredb=True))
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_SWITCH_Structure.csv"

    switchdata = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                                      feedPattern="^EANFZ.*\.txt",
                                                                      feedParams=params,
                                                                      feedFilters=issuerswitchfilter,
                                                                      resultkey="switchposdf"))



    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100036"]','df["RRN"]=df["RRN"].astype(np.int64)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0]["CR_DR_IND"]="D"']

    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_HOST_Structure.csv"
    cbsdata = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                   feedPattern="^H2H.*\.txt",
                                                                   feedParams=params,
                                                                   feedFilters=cbsfilters,
                                                                   resultkey="cbsdf"))



    npciissuer = dict(id=5, next=6, type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                                      feedPattern="ISSR*.*mE.*",
                                                                      feedParams=dict(type="INWARD"),
                                                                      feedFilters=['df["RRN"]=df["RRN"].astype(np.int64)'],
                                                                      resultkey="npciissdf"))



    selfmatch =dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                          columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     amountColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     crdrcolumn=[
                                                                                                         "CR_DR_IND"],
                                                                                                     CreditDebitSign=True),
                                                                          sourceTag="CBS")], matchResult="iiscbsdf"))


    threewaymatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="switchposdf",
                                                                               columns=dict(keyColumns=["RRNISSUER", "CARD_NUMBER"],
                                                                                    sumColumns=["TXN_AMOUNT"],
                                                                                    matchColumns=["RRNISSUER", "CARD_NUMBER",
                                                                                                  "TXN_AMOUNT"]),
                                                                               sourceTag="Switch"), dict(source="iiscbsdf.CBS",
                                                                                                 columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     sumColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     matchColumns=[
                                                                                                         "RRN",
                                                                                                         "CARD_NUMBER",
                                                                                                         "TXN_AMOUNT"]),
                                                                                                 sourceTag="CBS"),
                                                                          dict(source="npciissdf",
                                                                       columns=dict(
                                                                           keyColumns=["RRN",
                                                                                       "PAN Number"],
                                                                           sumColumns=[
                                                                               "Actual Transaction Amount"],
                                                                           matchColumns=[
                                                                               "RRN",
                                                                               "PAN Number",
                                                                               "Actual Transaction Amount"]),
                                                                       sourceTag="NPCIISSUER")],
                                                                 matchResult="results"))

    elements = [inititalizer, switchdata, cbsdata, npciissuer, selfmatch, threewaymatch]
    f = FlowRunner(elements)
    f.run()
    f.result['switchposdf'].to_csv("Output/switchposdf.csv", index=False)
    f.result['cbsdf'].to_csv("Output/cbsdf.csv", index=False)
    f.result['npciissdf'].to_csv("Output/npciissdf.csv", index=False)
    for x in f.result['results'].keys():
        f.result['results'][x].to_csv("Output/Issuer/" + x + ".csv")

