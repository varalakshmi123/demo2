logBaseDir = '/var/log/smartrecon/'
basepath = '/usr/share/nginx/smartrecon/'
mftpath = "/usr/share/nginx/smartrecon/mft"

# IMPS Settlement report details

# GST Tax Rate
gst_rate = 0.18  # Change as per current GST rate

# Amount start is exclusive, consider accordingly
npci_switching_fee = {0.25: {'start': 0, "end": 1000}, 0.50: {'start': 1000, "end": 200000}}

interchange_fee = {0.25: {'start': 0, "end": 1000}, 1: {'start': 1000, "end": 25000},
                   5: {'start': 25000, "end": 200000}}

settlement_report = 'IMPSNTSLEQT#%d%m%y#_[1-4]{1}C.xls'  # Change as per the bank file pattern
ignoreConsolidateReportRecons = ['MF_HYPO']
####################################

mongoHost = 'localhost'
databaseName = 'erecon'
bank_name = 'equitas'
