from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "13-Oct-2017"
    basePath = "/Users/shivashankar/Desktop/ReconData/SyndicateBank"

    sfms_filters = []
    cbs_filters = ['df["V_TXN_DESC"]=df["V_TXN_DESC"].apply(lambda x: x.split(":")[-1])']

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="neft", compressionType="zip", resultkey=''))
    params = {}
    params["feedformatFile"] = "NEFT_INWARD_Structure.csv"
    params[
        "columnNames"] = "S.No,Seq No,Transaction Ref,Related Reference,Sender IFSC,Sender A/c Type,Sender A/c No,Sender Name,Amount (Rs.),Value. Date,Remit. Date,Batch Id,FRESH / RETURN,Benf. IFSC,Status,Benf. A/c Type,Benf. A/c No.,Benf. Name,Return Code,Return Reason,Originator of Remittance,Sender To Receiver Information"
    params['FooterKey'] = "Incoming_Rejected_Transactions"
    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern=".*?_Inward.csv",
                                                                 feedParams=dict(
                                                                     feedFileStructure='SyndicateBank_NEFT_CBS_Structure.csv'),
                                                                 feedFilters=cbs_filters,
                                                                 resultkey="cbsdf"))
    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="Inward_SFMS.*?.xls",
                                                                 feedParams=params,
                                                                 feedFilters=sfms_filters,
                                                                 resultkey="sfmsdf"))

    leftcol = dict(keyColumns=["V_TXN_DESC"], sumColumns=["N_TXN_AMT_LCY"],
                   matchColumns=["V_TXN_DESC", "N_TXN_AMT_LCY"])
    rightcol = dict(keyColumns=["Transaction Ref"], sumColumns=["Amount (Rs.)"],
                    matchColumns=["Transaction Ref", "Amount (Rs.)"])
    elem4 = dict(id=4, type="PerformMatch", properties=dict(leftData="cbsdf", rightData="sfmsdf",
                                                            left=leftcol, right=rightcol, leftUnmatch='cbsexp',
                                                            rightUnmatch='sfmsexp', matchResult='outwardMatch'))

    elements = [elem1, elem2, elem3, elem4]
    f = FlowRunner(elements)
    f.run()
