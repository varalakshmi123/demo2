import pandas as pd
import xlrd
import re
import csv
import os
import numpy as np
import zipfile


class PreLoaderScript:
    def __init__(self, stmtDate, reconName):
        self.reconName = reconName
        self.statmentDate = stmtDate
        self.mftPath = '/usr/share/nginx/smartrecon/mft/'


    def OfsaaScript(self):
        mftPath = self.mftPath + self.reconName + '/' + self.statmentDate.strftime('%d%m%Y')
        files = os.listdir(mftPath)

        for file in files:
            if len(file.split('.'))>1:
                if file.split('.')[1] == 'txt':
                    txtfileName = file.split('.')[0]+'_txt'+ '.csv'
                    listf = [i.rstrip('|\n\r') for i in open(mftPath + '/' + file).readlines()]
                    final = [i.split('|') for i in listf]
                    with open(mftPath + '/' + txtfileName, 'w') as myfile:
                        writer = csv.writer(myfile, delimiter=',')
                        for i in final:
                            writer.writerow(i)
                    os.remove(mftPath + '/' + file)
                elif file.split('.')[1] == 'dat':
                    csvfileName = file.split('.')[0] +'_dat'+ '.csv'
                    listf = [i.rstrip('|\n') for i in open(mftPath + '/' + file).readlines()]
                    final = [i.split('|') for i in listf]
                    with open(mftPath + '/' + csvfileName, 'w') as myfile:
                        writer = csv.writer(myfile, delimiter=',')
                        for i in final:
                            writer.writerow(i)
                    os.remove(mftPath + '/' + file)
