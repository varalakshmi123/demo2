import os
import shutil
import sys
import zipfile
from datetime import datetime
from traceback import format_exc

import numpy as np
import pandas as pd
from pymongo import MongoClient

import config
import logger
from Matcher import Matcher
from Utilities import Utilities
from prerecon.FeedLoader import FeedLoaderFactory
from prerecon.FilterExecutor import FilterExecutor
from prerecon.FindFiles import FindFiles
from prerecon.NPCISettlementLoader import NPCISettlementLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


def findDifference(filelist, mdblist):
    retlist = filelist
    for mdl in mdblist:
        for fl in filelist:
            if mdl in fl:
                retlist.remove(fl)

    return retlist


if __name__ == "__main__":
    statementDate = sys.argv[1]
    try:
        stmtDate = datetime.strptime(statementDate, '%d-%b-%Y')
        day_of_year = (stmtDate - datetime(stmtDate.year, 1, 1)).days + 1
    except:
        logger.info("Statement date provided is not in the required format 'DD-MMM-YYYY'")
        logger.info('----------------------------------------')
        logger.info('Recon job execution ABORTED')
        logger.info('----------------------------------------')
        exit(0)

    mdb = MongoClient('localhost')['erecon']

    # check if statement date already exists
    execDoc = mdb['recon_execution_details_log'].find_one({"STATEMENT_DATE": stmtDate, 'RECON_ID': "IMPS_RECON",
                                                           'PROCESSING_STATE_STATUS': 'Matching Completed'})
    # if execDoc:
    #     logger.info("Statement Date Already Exists")
    #     logger.info('----------------------------------------')
    #     logger.info('Recon job execution ABORTED')
    #     logger.info('----------------------------------------')
    #     raise ValueError("Statement %s Date Already Exists" % stmtDate.strftime("%d-%b-%Y"))
    jobSummary = dict()
    outwardfiles = []
    inwardfiles = []
    try:
        jobID = Utilities().getNextSeq()
        prev_job_doc = Utilities().getPreviousJobDoc('IMPS_RECON')

        jobSummary['RECON_ID'] = "IMPS_RECON"
        jobSummary['RECON_EXECUTION_ID'] = jobID
        jobSummary["partnerId"] = "54619c820b1c8b1ff0166dfc"
        jobSummary["machineId"] = "16af9057-69af-4c69-bbcf-70d99334e027"
        jobSummary['EXECUTION_DATE_TIME'] = datetime.now().strftime('%s')
        jobSummary['STATEMENT_DATE'] = stmtDate
        cbsinwardcyclelist = []
        npciinwardcyclelist = []
        cbsoutwardcyclelist = []
        npcioutwardcyclelist = []
        npciTimeOutReport = pd.DataFrame()

        if prev_job_doc is not None and stmtDate < prev_job_doc['STATEMENT_DATE']:
            logger.info("Statement Date already exists")
            raise ValueError("Statement date already exists")

        flf = FeedLoaderFactory()

        # check if files exists in path
        if os.path.isfile(config.basepath + "mft/IMPS/" + stmtDate.strftime('%d%m%Y') + '.zip'):
            extractFile = zipfile.ZipFile(config.basepath + "mft/IMPS/" + stmtDate.strftime('%d%m%Y') + '.zip')
            extractFile.extractall(config.basepath + "mft/IMPS/")

        source_path = config.basepath + "mft/IMPS/" + stmtDate.strftime('%d%m%Y') + '/'
        output_path = source_path + 'OUTPUT/'
        processed_path = source_path + 'PROCESSED/'
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        if not os.path.exists(processed_path):
            os.mkdir(processed_path)

        findFiles = FindFiles(stmtDate)
        npci_inst = flf.getInstance("NPCI")
        csv_inst = flf.getInstance('CSV')
        carryForwardLoader = flf.getInstance('CarryForward')

        npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                        "df = df[df['Response Code'] == '00']",
                        "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100.00"]

        # IMPS Issr Reconciliation
        execDoc = mdb['recon_execution_details_log'].find_one({'RECON_ID': "IMPS_RECON",
                                                               'PROCESSING_STATE_STATUS': 'Matching Completed'})
        npci_files = findFiles.findFiles(feedPath=source_path, fpattern="ISSRPEQT*.*")

        if prev_job_doc is not None and prev_job_doc['STATEMENT_DATE'] == stmtDate:
            executednpcilist = prev_job_doc.get('NPCI_INWARD_CYCLE_LIST', [])
            npci_files = findDifference(npci_files, executednpcilist)
        if len(npci_files) == 0:
            raise ValueError("No files to process for NPCI INWARD")
        for c in npci_files:
            npcycle = c.rsplit('_')[1].rsplit('.')[0]
            npciinwardcyclelist.append(npcycle)

        cbs_files = findFiles.findFiles(feedPath=source_path, fpattern="IMPS_INWARD*.*")
        if prev_job_doc is not None and prev_job_doc['STATEMENT_DATE'] == stmtDate:
            executedcbslist = prev_job_doc.get('CBS_INWARD_CYCLE_LIST', [])
            cbs_files = findDifference(cbs_files, executedcbslist)
        if len(cbs_files) == 0:
            raise ValueError("No files to process for CBS INWARD")
        for c in cbs_files:
            print c
            cbscycle = c.rsplit('_')[-1].rsplit('.')[0]
            cbsinwardcyclelist.append(cbscycle)

        npci_all = npci_inst.loadFile(npci_files, {"type": "INWARD"})
        npci = npci_all.copy()

        npciSettlementFiles = findFiles.findFiles(feedPath=source_path, fpattern=config.settlement_report)
        if len(npciSettlementFiles):
            NPCISettlementLoader().validateNPCISettlement(npci,
                                                          {'reportType': 'beneficiary', "respCode": 'Response Code',
                                                           'statementDate': stmtDate,
                                                           'actualTxnAmt': 'Actual Transaction Amount',
                                                           'txnAmt': "Transaction Amount",
                                                           "settlementFiles": npciSettlementFiles})
        else:
            logger.info("******************** NPCI Settelement file is not there *************************")

        npci['SOURCE'] = "NPCI"
        npci = FilterExecutor().run(npci, npci_filters)

        cbs = csv_inst.loadFile(cbs_files,
                                {"delimiter": "|", "feedformatFile": "IMPS_CBS_Structure.csv", "skiprows": 1})
        cbs['SOURCE'] = "CBS"

        cbs['TRANSACTION_DESCRIPTION'] = cbs['TRANSACTION_DESCRIPTION'].astype(str)
        cbs = cbs[~(cbs['TRANSACTION_DESCRIPTION'].str.contains('IMPS Beneficiary', case=False))]
        cbs = cbs[~(cbs['TRANSACTION_DESCRIPTION'].str.contains('IMPS Net', case=False))]

        cbs['Ref Number'] = cbs['TRANSACTION_DESCRIPTION'].str.extract("([0-9]{12})")

        # cbs = cbs[cbs['BRANCH_CODE'] == 9999]

        if prev_job_doc is not None:
            t = prev_job_doc['STATEMENT_DATE']
            t = datetime(t.year, t.month, t.day)
            prevdate = t.strftime('%d%m%Y')
            if os.path.exists(config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/NPCI_Unmatched.csv'):
                logger.info(
                    'loading carryorward file' + config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/NPCI_Unmatched.csv')
                unmatchnpcidf = pd.read_csv(
                    config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/NPCI_Unmatched.csv',
                    converters={'Ref Number': str, 'Transaction Amount': np.float64}, engine='python')
                npci = npci.append(unmatchnpcidf)
            if os.path.exists(config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/CBS_Unmatched.csv'):
                logger.info(
                    'loading carryorward file' + config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/CBS_Unmatched.csv')

                unmatchcbsdf = pd.read_csv(
                    config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/INWARD/CBS_Unmatched.csv',
                    engine='python', converters={'Ref Number': str, 'Transaction Amount': np.float64})
                unmatchcbsdf['VALUE_DATE'] = pd.to_datetime(unmatchcbsdf['VALUE_DATE'], format='%Y-%m-%d')
                unmatchcbsdf['Ref Number'] = unmatchcbsdf['Ref Number'].astype(str)
                cbs = cbs.append(unmatchcbsdf)

        if '_merge' in cbs.columns:
            del cbs['_merge']

        left, right = Matcher().match(npci, cbs,
                                      leftkeyColumns=["Ref Number"],
                                      rightkeycolumns=["Ref Number"],
                                      leftsumColumns=['Transaction Amount'],
                                      rightsumColumns=['Transaction Amount'],
                                      leftmatchColumns=["Ref Number", "Transaction Amount"],
                                      rightmatchColumns=["Ref Number", "Transaction Amount"])

        if not os.path.exists(output_path + 'INWARD/'):
            os.makedirs(output_path + 'INWARD/')

        left_matched = left[left['Matched'] == 'MATCHED']
        left_unmatched = left[left['Matched'] == 'UNMATCHED']
        right_matched = right[right['Matched'] == 'MATCHED']
        right_unmatched = right[right['Matched'] == 'UNMATCHED']
        right_unmatched['CREDIT_DEBIT'] = right_unmatched['CREDIT_DEBIT'].astype(str)
        credit_reversal = right_unmatched[right_unmatched['CREDIT_DEBIT'] == 'C']
        debit_reversal = right_unmatched[right_unmatched['CREDIT_DEBIT'] == 'D']

        left, right = Matcher().match(credit_reversal, debit_reversal, leftkeyColumns=["Ref Number"],
                                      rightkeycolumns=['Ref Number'],
                                      leftsumColumns=['Transaction Amount'], rightsumColumns=['Transaction Amount'],
                                      leftmatchColumns=["Ref Number", "Transaction Amount"],
                                      rightmatchColumns=["Ref Number", "Transaction Amount"])

        left['MatchType'] = 'Reversal'
        right['MatchType'] = 'Reversal'

        right_unmatched = left[left['Matched'] == 'UNMATCHED']
        right_unmatched = right_unmatched.append(right[right['Matched'] == 'UNMATCHED'])

        right_matched = right_matched.append(left[left['Matched'] == 'MATCHED'])
        right_matched = right_matched.append(right[right['Matched'] == 'MATCHED'])

        # NPCI TIMEOUT FILES LOADING and REPORT GENERATION
        npciTimeoutFiles = findFiles.findFiles(feedPath=source_path, fpattern="Time Out Cases Report*.*", errors='ignore')
        if len(npciTimeoutFiles):
            npciTimeOutReportCols = ['TXN UID', 'TXN Type', 'TXN Date', 'TXN Time', 'Settlement Date', 'Response Code',
                                     'Ref Number', 'STAN', 'Remitter', 'Beneficiary',
                                     'Beneficiary Mobile /Account/Aadhar Number',
                                     'Remitter Number', 'Column1']

            for file in npciTimeoutFiles:
                tmp = pd.read_html(file, skiprows=1)
                if tmp:
                    npciTimeOutReport = npciTimeOutReport.append(tmp[-1], ignore_index=True)
                else:
                    pass

            if '_merge' in right_unmatched.columns:
                del right_unmatched['_merge']
            npciTimeOutReport.columns = npciTimeOutReportCols
            npciTimeOutReport.loc[:, 'Ref Number'] = npciTimeOutReport.loc[:, 'Ref Number'].astype(str).str.lstrip("'")
            right_unmatched = right_unmatched.merge(npciTimeOutReport, on='Ref Number', how='left', suffixes=('', "_y"),
                                                    indicator=True)

            for col in right_unmatched:
                if '_y' in col:
                    del right_unmatched[col]

            right_unmatched.loc[right_unmatched['_merge'] == 'both', 'Matched'] = 'MATCHED'
            right_unmatched[right_unmatched['_merge'] == 'both'].to_csv(
                output_path + 'INWARD/' + "INWARD_Timeout_Succesful.csv", index=False)
            right_unmatched = right_unmatched[right_unmatched['_merge'] != 'both']

            # Generate CBS Reversal Time-Out Transactions file
            # npciTimeOutTxn = npci_all[npci_all['Response Code'] == '08']
            # print npciTimeOutTxn.columns
            # print "-------------------"
            # print npciTimeOutReport.columns
            # npciTimeOutTxn = npciTimeOutTxn.merge(npciTimeOutReport, on='Ref Number', how='left', indicator=True)
            # refNumberList = npciTimeOutTxn[npciTimeOutTxn['_merge'] == 'left']['Ref Number'].unique().tolist()
            # cbs_timeout_reversal = cbs[cbs['Ref Number'].isin(refNumberList)]
            # cbs_timeout_reversal.to_csv(output_path +'INWARD/'+"IMPS_INWARD_TIMEOUT_REVERSAL.csv")
        else:
            logger.info("#### NPCI time out file is not there ####")

        # total_records = left_matched.append(right_matched)
        total_records = Utilities().mergeMatchedRecords(left_matched, right_matched, leftSrc='_NPCI', rightSrc='_CBS',
                                                        leftOn='Ref Number',
                                                        rightOn='Ref Number')

        inward_report = output_path + 'INWARD/' + 'INWARD_Matched.csv'

        if os.path.exists(inward_report):
            prevnpci = pd.read_csv(inward_report)
            total_records = total_records.append(prevnpci)

            # left_matched.to_csv(output_path + 'INWARD/' + 'NPCI_matched.csv')
        right_matched[right_matched['MatchType'] == 'Reversal'].to_csv(output_path + 'INWARD/'
                                                                       + 'INWARD_Reversal_Succesful.csv',
                                                                       index=False)

        # update response code from NPCI to CBS-Unmatched.
        npci_tmp = npci_all.copy()
        if "_merge" in right_unmatched.columns:
            del right_unmatched['_merge']

        right_unmatched = right_unmatched.merge(npci_tmp, on='Ref Number', how='left', suffixes=('', '_y'),
                                                indicator=True)
        right_unmatched = right_unmatched[right_unmatched['_merge'].isin(['left_only', 'both'])]

        for col in right_unmatched.columns:
            if '_y' in col and col in right_unmatched.columns:
                del right_unmatched[col]

        print right_unmatched['VALUE_DATE'].unique()
        cbsclosingbalance=right_unmatched[right_unmatched["VALUE_DATE"].dt.dayofyear<=day_of_year]['Transaction Amount'].sum()

        npciSummaryDict = dict()
        npciSummaryDict['CBS Closing Balance'] = cbsclosingbalance
        npciSummaryDict['Total Matched Count'] = len(total_records)
        npciSummaryDict['NPCI Total Match Amount'] = total_records['Transaction Amount_NPCI'].sum()
        npciSummaryDict['CBS Total Match Amount'] = total_records['Transaction Amount_CBS'].sum()
        #npciSummaryDict['Total Match Amount'] = total_records['Transaction Amount_NPCI'].sum()
        npciSummaryDict['CBS Total UnMatch Amount'] = right_unmatched['Transaction Amount'].sum()
        npciSummaryDict['NPCI Total UnMatch Amount'] = left_unmatched['Transaction Amount'].sum()
        npciSummaryDict['CBS Total UnMatch Count'] = len(right_unmatched)
        npciSummaryDict['NPCI Total UnMatch Count'] = len(left_unmatched)
        pd.DataFrame([npciSummaryDict]).to_csv(output_path + 'INWARD/' + "NPCI Inward Summary.csv", index=False)

        total_records.to_csv(output_path + 'INWARD/' + 'INWARD_Matched.csv', index=False)
        left_unmatched.to_csv(output_path + 'INWARD/' + 'NPCI_Unmatched.csv', index=False)
        right_unmatched.to_csv(output_path + 'INWARD/' + 'CBS_Unmatched.csv', index=False)
        total_records.to_csv(output_path + 'INWARD_Report.csv', index=False)
        inwardReportPath = output_path + 'INWARD/'
        os.system("cd %s ;zip -r UPI_INWARD.zip ." % inwardReportPath)

        logger.info('IMPS-Inward output report path : ' + inward_report)

        inwardfiles = npci_files + cbs_files

        ################################################################################################################

        # IMPS Acqr Reconciliation
        npci_files = findFiles.findFiles(feedPath=source_path, fpattern="ACQRPEQT*.*")
        if prev_job_doc is not None and prev_job_doc['STATEMENT_DATE'] == stmtDate:
            executednpcilist = prev_job_doc.get('NPCI_OUTWARD_CYCLE_LIST', [])
            npci_files = findDifference(npci_files, executednpcilist)
        if len(npci_files) == 0:
            raise ValueError("No files to process for NPCI OUTWARD")
        for c in npci_files:
            npcycle = c.rsplit('_')[1].rsplit('.')[0]
            npcioutwardcyclelist.append(npcycle)

        cbs_files = findFiles.findFiles(feedPath=source_path, fpattern="IMPS_OUTWARD*.*")
        if prev_job_doc is not None and prev_job_doc['STATEMENT_DATE'] == stmtDate:
            executedcbslist = prev_job_doc.get('CBS_OUTWARD_CYCLE_LIST', [])
            cbs_files = findDifference(cbs_files, executedcbslist)
        if len(cbs_files) == 0:
            raise ValueError("No files to process for CBS OUTWARD")
        for c in cbs_files:
            print c
            cbscycle = c.rsplit('_')[-1].rsplit('.')[0]
            cbsoutwardcyclelist.append(cbscycle)

        npci = npci_inst.loadFile(npci_files, {"type": "OUTWARD"})

        cbs = csv_inst.loadFile(cbs_files,
                                {"delimiter": "|", "feedformatFile": "IMPS_CBS_Structure.csv", "skiprows": 1})


        npciSettlementFiles = findFiles.findFiles(feedPath=source_path, fpattern=config.settlement_report)
        if len(npciSettlementFiles):
            NPCISettlementLoader().validateNPCISettlement(npci,
                                                          {'reportType': 'remitter', "respCode": 'Response Code',
                                                           'statementDate': stmtDate,
                                                           'actualTxnAmt': 'Actual Transaction Amount',
                                                           'txnAmt': "Transaction Amount",
                                                           "settlementFiles": npciSettlementFiles})
        else:
            logger.info("******************** NPCI Settelement file is not there *************************")

        npci['SOURCE'] = "NPCI"
        npci = FilterExecutor().run(npci, npci_filters)

        cbs['SOURCE'] = "CBS"
        # replaceValues = {'IMP REV': '', 'IMPS P2A REV': '', 'IMP P2A': '', "IMP": "", "IMPS P2A Intra": "","IMPS P2P Intra":""}
        # for key, value in replaceValues.iteritems():
        #     cbs['TRANSACTION_DESCRIPTION'] = cbs['TRANSACTION_DESCRIPTION'].str.replace(key, value)

        # cbs = cbs[cbs['BRANCH_CODE'] == 9999]
        cbs = cbs[~(cbs['TRANSACTION_DESCRIPTION'].str.contains('IMPS Net', case=False))]
        cbs = cbs[~(cbs['TRANSACTION_DESCRIPTION'].str.contains('IMPS Remitter', case=False))]

        cbs['Ref Number'] = cbs['TRANSACTION_DESCRIPTION'].str.extract("([0-9]{12})")

        if prev_job_doc is not None:
            t = prev_job_doc['STATEMENT_DATE']
            t = datetime(t.year, t.month, t.day)
            prevdate = t.strftime('%d%m%Y')
            if os.path.exists(config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/NPCI_Unmatched.csv'):
                logger.info(
                    'loading carryorward file' + config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/NPCI_Unmatched.csv')
                unmatchnpcidf = pd.read_csv(
                    config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/NPCI_Unmatched.csv', engine='python',
                    converters={'Ref Number': str, 'Transaction Amount': np.float64})
                npci = npci.append(unmatchnpcidf)
            if os.path.exists(config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/CBS_Unmatched.csv'):
                logger.info(
                    'loading carryorward file' + config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/CBS_Unmatched.csv')
                unmatchcbsdf = pd.read_csv(
                    config.basepath + "mft/IMPS/" + prevdate + '/OUTPUT/OUTWARD/CBS_Unmatched.csv', engine='python',
                    converters={'Ref Number': str, 'Transaction Amount': np.float64}, )
                unmatchcbsdf['VALUE_DATE'] = pd.to_datetime(unmatchcbsdf['VALUE_DATE'], format='%Y-%m-%d')
                cbs = cbs.append(unmatchcbsdf)
        if '_merge' in cbs.columns:
            del cbs['_merge']

        left, right = Matcher().match(npci, cbs,
                                      leftkeyColumns=["Ref Number"],
                                      rightkeycolumns=["Ref Number"],
                                      leftsumColumns=['Transaction Amount'],
                                      rightsumColumns=['Transaction Amount'],
                                      leftmatchColumns=["Ref Number", "Transaction Amount"],
                                      rightmatchColumns=["Ref Number", "Transaction Amount"])

        if not os.path.exists(output_path + 'OUTWARD/'):
            os.makedirs(output_path + 'OUTWARD/')

        left_matched = left[left['Matched'] == 'MATCHED']
        left_unmatched = left[left['Matched'] == 'UNMATCHED']
        right_matched = right[right['Matched'] == 'MATCHED']
        right_unmatched = right[right['Matched'] == 'UNMATCHED']

        useInReversal = right_unmatched
        if '_merge' in useInReversal.columns:
            del useInReversal['_merge']

        credit_reversal = useInReversal[useInReversal['CREDIT_DEBIT'] == 'C']
        debit_reversal = useInReversal[useInReversal['CREDIT_DEBIT'] == 'D']

        left, right = Matcher().match(credit_reversal, debit_reversal, leftkeyColumns=["Ref Number"],
                                      rightkeycolumns=['Ref Number'],
                                      leftsumColumns=['Transaction Amount'], rightsumColumns=['Transaction Amount'],
                                      leftmatchColumns=["Ref Number", "Transaction Amount"],
                                      rightmatchColumns=["Ref Number", "Transaction Amount"])

        left['MatchType'] = 'Reversal'
        right['MatchType'] = 'Reversal'

        right_unmatched = left[left['Matched'] == 'UNMATCHED']
        right_unmatched = right_unmatched.append(right[right['Matched'] == 'UNMATCHED'])

        right_matched = right_matched.append(left[left['Matched'] == 'MATCHED'])
        right_matched = right_matched.append(right[right['Matched'] == 'MATCHED'])

        if not npciTimeOutReport.empty:
            if '_merge' in right_unmatched.columns:
                del right_unmatched['_merge']
            npciTimeOutReport.columns = npciTimeOutReportCols
            npciTimeOutReport.loc[:, 'Ref Number'] = npciTimeOutReport.loc[:, 'Ref Number'].astype(str).str.lstrip("'")
            right_unmatched = right_unmatched.merge(npciTimeOutReport, on='Ref Number', how='left', suffixes=('', "_y"),
                                                    indicator=True)

            for col in right_unmatched:
                if '_y' in col:
                    del right_unmatched[col]
            right_unmatched.loc[right_unmatched['_merge'] == 'both', 'Matched'] = 'MATCHED'
            right_unmatched[right_unmatched['_merge'] == 'both'].to_csv(
                output_path + 'OUTWARD/' + "Outward_Timeout_Succesfull.csv", index=False)
            right_unmatched = right_unmatched[right_unmatched['_merge'] != 'both']

            # Generate CBS Reversal Time-Out Transactions file
            # npciTimeOutTxn = npci_all[npci_all['Response Code'] == '08']
            # print npciTimeOutTxn.columns
            # print "-------------------"
            # print npciTimeOutReport.columns
            # npciTimeOutTxn = npciTimeOutTxn.merge(npciTimeOutReport, on='Ref Number', how='left', indicator=True)
            # refNumberList = npciTimeOutTxn[npciTimeOutTxn['_merge'] == 'left']['Ref Number'].unique().tolist()
            # cbs_timeout_reversal = cbs[cbs['Ref Number'].isin(refNumberList)]
            # cbs_timeout_reversal.to_csv(output_path +'OUTWARD/'+"IMPS_OUTWARD_TIMEOUT_REVERSAL.csv")
        else:
            logger.info("******************** NPCI time out file is not there *************************")

        # total_records = left_matched.append(right_matched)
        # if os.path.exists(outward_report):
        #     prevtotal = pd.read_csv(outward_report)
        #     total_records = total_records.append(prevtotal)
        outward_report = output_path + 'OUTWARD/' + 'OUTWARD_Matched.csv'
        total_records = Utilities().mergeMatchedRecords(left_matched, right_matched, leftSrc='_NPCI', rightSrc='_CBS',
                                                        leftOn='Ref Number',
                                                        rightOn='Ref Number')

        right_matched[right_matched['MatchType'] == 'Reversal'].to_csv(output_path + 'OUTWARD/'
                                                                       + 'Outward_Reversal_Succesful.csv',
                                                                       index=False)

        # update response code from NPCI to CBS-Unmatched.
        npci_tmp = npci_all.copy()
        if "_merge" in right_unmatched.columns:
            del right_unmatched['_merge']

        right_unmatched = right_unmatched.merge(npci_tmp, on='Ref Number', how='left', suffixes=('', '_y'), indicator=True)
        right_unmatched = right_unmatched[right_unmatched['_merge'].isin(['left_only', 'both'])]
        for col in right_unmatched.columns:
            if '_y' in col and col in right_unmatched.columns:
                del right_unmatched[col]
        cbsclosingbalance = right_unmatched[right_unmatched["VALUE_DATE"].dt.dayofyear <= day_of_year][
            'Transaction Amount'].sum()
        npciSummaryDict = dict()
        npciSummaryDict['CBS Closing Balance'] = cbsclosingbalance
        npciSummaryDict['Total Matched Count'] = len(total_records)
        npciSummaryDict['NPCI Total Match Amount'] = total_records['Transaction Amount_NPCI'].sum()
        npciSummaryDict['CBS Total Match Amount'] = total_records['Transaction Amount_CBS'].sum()
        #npciSummaryDict['Total Match Amount'] = total_records['Transaction Amount_NPCI'].sum()
        npciSummaryDict['CBS Total UnMatch Amount'] = right_unmatched['Transaction Amount'].sum()
        npciSummaryDict['NPCI Total UnMatch Amount'] = left_unmatched['Transaction Amount'].sum()
        npciSummaryDict['CBS Total UnMatch Count'] = len(right_unmatched)
        npciSummaryDict['NPCI Total UnMatch Count'] = len(left_unmatched)
        pd.DataFrame([npciSummaryDict]).to_csv(output_path + 'OUTWARD/' + "NPCI Outward Summary.csv", index=False)

        total_records.to_csv(output_path + 'OUTWARD/' + 'OUTWARD_Matched.csv', index=False)
        left_unmatched.to_csv(output_path + 'OUTWARD/' + 'NPCI_Unmatched.csv', index=False)
        right_unmatched.to_csv(output_path + 'OUTWARD/' + 'CBS_Unmatched.csv', index=False)

        total_records.to_csv(output_path + 'OUTWARD_Report.csv', index=False)
        outwardReportPath = output_path + 'OUTWARD/'
        os.system("cd %s ;zip -r UPI_OUTWARD.zip ." % outwardReportPath)

        logger.info('IMPS-Outward output report path : ' + outward_report)

        outwardfiles = npci_files + cbs_files

        if prev_job_doc is not None and prev_job_doc['STATEMENT_DATE'] == stmtDate:
            cbsincylist = prev_job_doc['CBS_INWARD_CYCLE_LIST']
            cbsincylist.append(cbsinwardcyclelist)
            cbsoutcylist = prev_job_doc['CBS_OUTWARD_CYCLE_LIST']
            cbsoutcylist.append(cbsoutwardcyclelist)
            npciincycle = prev_job_doc['NPCI_INWARD_CYCLE_LIST']
            npciincycle.append(npciinwardcyclelist)
            npcioutcycle = prev_job_doc['NPCI_OUTWARD_CYCLE_LIST']
            npcioutcycle.append(npcioutwardcyclelist)

            jobSummary['_id'] = prev_job_doc['_id']
        else:
            cbsincylist = [cbsinwardcyclelist]
            cbsoutcylist = [cbsoutwardcyclelist]
            npciincycle = [npciinwardcyclelist]
            npcioutcycle = [npcioutwardcyclelist]

        jobSummary['PROCESSING_STATE_STATUS'] = "Matching Completed"
        jobSummary['JOB_STATUS'] = "SUCCESS"
        jobSummary['INWARD_REPORT_PATH'] = inward_report
        jobSummary['OUTWARD_REPORT_PATH'] = outward_report
        jobSummary['CBS_INWARD_CYCLE_LIST'] = cbsinwardcyclelist
        jobSummary['CBS_OUTWARD_CYCLE_LIST'] = cbsoutwardcyclelist
        jobSummary['NPCI_INWARD_CYCLE_LIST'] = npciinwardcyclelist
        jobSummary['NPCI_OUTWARD_CYCLE_LIST'] = npcioutwardcyclelist
    except BaseException, e:
        logger.info('----------------------------------------')
        logger.info('Recon job execution ABORTED')
        jobSummary['PROCESSING_STATE_STATUS'] = "Matching Failed"
        jobSummary['JOB_STATUS'] = "FAILURE"
        jobSummary['INWARD_REPORT_PATH'] = ''
        jobSummary['OUTWARD_REPORT_PATH'] = ''
        jobSummary['errorMsg'] = str(e)
        logger.info(format_exc())
        logger.info('----------------------------------------')

    except Exception, e:
        logger.info('----------------------------------------')
        logger.info('Recon job execution ABORTED')
        jobSummary['PROCESSING_STATE_STATUS'] = "Matching Failed"
        jobSummary['JOB_STATUS'] = "FAILURE"
        jobSummary['INWARD_REPORT_PATH'] = ''
        jobSummary['OUTWARD_REPORT_PATH'] = ''
        jobSummary['errorMsg'] = str(e)

        logger.info(format_exc())
        logger.info('----------------------------------------')

    mdb['recon_execution_details_log'].save(jobSummary)
    for f1 in outwardfiles:
        shutil.move(f1, processed_path)
    for f1 in inwardfiles:
        shutil.move(f1, processed_path)
