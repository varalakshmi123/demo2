import os
from collections import OrderedDict
from datetime import datetime

import numpy as np

import config
import logger

logger = logger.Logger.getInstance("smartrecon").getLogger()


class GEFUHandler:
    def __init__(self, recon='', statementDate=''):
        logger.info('GEFU file initiated for recon %s' % recon)
        self.bank = config
        self.recon = recon
        self.statementDate = statementDate

    def _equitas(self, df=None):
        if df is not None:
            _columns = ['Account Number', 'Debit / Credit Code', 'Amount', 'Txn Desc']

            for col in _columns:
                if col not in df.columns:
                    raise Exception('Key columns %s not found in the input data' % col)

            df = df[_columns]
            df.reset_index(drop=True, inplace=False)

            '''01 for CASA Account (12 digit account number)
               03 for General Ledgers (9 digit account number)'''
            tran_type = {12: "01", 9: "03"}

            '''CASA Account Credit    : 1408
               CASA Account Debit     : 1008
               GL Account Credit      : 1460
               GL Account Debit       : 1060'''
            tran_code = {"01C": "1408", "01D": "1008", "03C": "1460", "03D": "1060"}

            df['Transaction Type'] = df['Account Number'].astype(str).apply(lambda x: tran_type.get(len(x), ''))

            # TODO, will be provided by Equitas IT
            df['Branch code'] = ''
            df.loc[df['Transaction Type'] == '03', 'Branch code'] = '09999'

            df['Transaction Code'] = df.apply(
                lambda x: tran_code.get(x['Transaction Type'] + x['Debit / Credit Code'], ''), axis=1)

            df['Transaction Date'] = datetime.now().strftime('%Y%m%d')
            df['Value Date'] = datetime.now().strftime('%Y%m%d')
            df['Transaction Currency'] = '1'

            # GEFU should be multiple off 100, retain two decimal precision
            df['Amount'] = df['Amount'].astype(np.float64).round(2)
            df.rename(columns={"Amount": "Amount in LCY"}, inplace=True)
            # Doing astype(int) is giving precision error converting .60 to .59 ,using below method
            df['Amount in LCY'] = df['Amount in LCY'].apply(lambda x: str(x * 100.00).replace('.0', ''))
            df['Amount in TCY'] = df['Amount in LCY']
            df['Conversion Rate'] = '1'
            df['Reference No'] = np.arange(1, len(df) + 1)
            df['Ref. Doc No'] = ''

            position_dict = OrderedDict(
                [('Transaction Type', {'width': 2, 'fillchar': ' '}),
                 ('Account Number', {'width': 16, 'fillchar': ' '}),
                 ('Branch code', {'width': 5, 'fillchar': ' '}), ('Transaction Code', {'width': 5, 'fillchar': ' '}),
                 ('Transaction Date', {'width': 8, 'fillchar': ' '}),
                 ('Debit / Credit Code', {'width': 1, 'fillchar': ' '}), ('Value Date', {'width': 8, 'fillchar': ' '}),
                 ('Transaction Currency', {'width': 5, 'fillchar': ' '}),
                 ('Amount in LCY', {'width': 14, 'fillchar': ' '}), ('Amount in TCY', {'width': 14, 'fillchar': ' '}),
                 ('Conversion Rate', {'width': 8, 'fillchar': ' '}), ('Reference No', {'width': 12, 'fillchar': ' '}),
                 ('Ref. Doc No', {'width': 12, 'fillchar': ' '}), ('Txn Desc', {'width': 40, 'fillchar': ' '})])

            # Re-order columns as per the header
            df = df[position_dict.keys()]

            for column in df.columns:
                df[column] = df[column].astype(str).str.ljust(position_dict[column]['width'],
                                                              position_dict[column]['fillchar'])

            gefu_path = config.mftpath + os.sep + self.recon + os.sep + 'GEFU_FILES/'

            if not os.path.exists(gefu_path + self.statementDate + '/'):
                os.mkdir(gefu_path + self.statementDate + '/')

            logger.info("GEFU File available in path %s" % gefu_path + self.statementDate)

            with open(gefu_path + self.statementDate + '/' + 'GEFU_Upload.txt', 'w') as f:
                f.write('1%s\n' % datetime.now().strftime('%Y%m%d'))

                for _v in df[position_dict.keys()].values:
                    f.write("2" + ''.join(_v) + '\n')

                f.write(
                    "3{debit_cnt}{debit_amt}{credit_cnt}{credit_amt}".format(
                        debit_cnt=str(len(df[df['Debit / Credit Code'] == 'D'])).ljust(9, ' '),
                        debit_amt=str(
                            df[df['Debit / Credit Code'] == 'D']['Amount in LCY'].astype(np.int64).sum()).ljust(15,
                                                                                                                ' '),
                        credit_cnt=str(len(df[df['Debit / Credit Code'] == 'C'])).ljust(9, ' '),
                        credit_amt=str(
                            df[df['Debit / Credit Code'] == 'C']['Amount in LCY'].astype(np.int64).sum()).ljust(15,
                                                                                                                ' ')))
            df['~~END~~'] = '~~END~~'

            # strip white spaces appended due to fixed lenght file updates.
            for col in df.columns:
                df[col] = df[col].astype(str).str.strip()
            df.to_csv(gefu_path + self.statementDate + '/' + 'GEFU_Ref.csv', index=False)

            logger.info('GEFU file generated Successfully for date %s.' % self.statementDate)
        else:
            return ''
