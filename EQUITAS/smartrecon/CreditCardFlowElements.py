#Guruvayoorappan Thunai
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "21-Jan-2018"
    basePath = "/Users/shivashankar/Desktop/ReconData/AndhraBank/"
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="CreditCard", compressionType="", resultkey='', ignoredb=True))
    params={}
    params["feedformatFile"] = "Reference/VisaFixedFormatTCR00.csv"


    visafilter=['df=df[df["Source Amount"].fillna("").str.isdigit()]','df=df[(df["Transaction Code Qualifier"].fillna("")=="00")&(df["Transaction Code"].fillna("").isin(["05","06","07","15","20","25","27"]))]','df["Destination Amount"]=df["Destination Amount"].astype(np.float64)','df["Source Amount"]=df["Source Amount"].astype(np.float64)','df["Source Amount"]=df["Source Amount"]/100','df["Card Number"]','df["Card Number"]="`"+df["Card Number"].str[0:16]','df["BIN"]=df["Card Number"].str[1:7]','df["BIN"]=df["BIN"].astype(np.int64)']

    elem2 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISAIC",
                                                                 feedPattern="^CTF.*\.TXT",
                                                                 feedParams=params,
                                                                 feedFilters=visafilter,
                                                                 resultkey="visadf"))
    params={}
    elem3 = dict(type="ReferenceLoader", properties=dict(loadType="CSV", source="BinMaster",
                                                   fileName=["Reference/BinMaster.csv"],
                                                   feedParams=params,
                                                   feedFilters=[],
                                                   resultkey="binmaster"))
    elem4 = dict(type="ReferenceLoader", properties=dict(loadType="CSV", source="CRDRMapping",
                                                         fileName=["Reference/TransCodetoDRCR.csv"],
                                                         feedParams=params,
                                                         feedFilters=['df["Transaction Code"]=df["Transaction Code"].astype(str)','df["Transaction Code"]=df["Transaction Code"].apply(lambda x:"0"+x if len(x)==1 else x)' ],
                                                         resultkey="crdrmapping"))

    rupaybinmasterloader = dict(type="ReferenceLoader", properties=dict(loadType="Excel", source="RupayBins",
                                                         fileName=["Reference/Rupay_Bins.xls"],
                                                         feedParams=params,
                                                         feedFilters=[],
                                                         resultkey="rupaybins"))



    elem5 = dict(type="VLookup", properties=dict(data="visadf", lookup="binmaster",
                                                 dataFields=["BIN"],
                                                 lookupFields=["BIN"],
                                                 includeCols=["Card Type","Card Variant","BIN GL"],
                                                 resultkey="visadf"))

    acqissmarker = ["df['Settlement Type']='ISSUER'","df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'","df=df[df['Card Type']=='Credit Card']"]
    elem6 = dict(id=4, next=5, type="ExpressionEvaluator",
                 properties=dict(source="visadf", resultkey="visadf", expressions=acqissmarker))

    elem7 = dict(type="VLookup", properties=dict(data="visadf", lookup="crdrmapping",
                                                 dataFields=["SOURCE","Settlement Type","Transaction Code"],
                                                 lookupFields=["DataSource","Settlement Type","Transaction Code"],
                                                 includeCols=["DrCRIND","Description"],
                                                 resultkey="visadf"))
    params={}
    params["feedformatFile"]="UnbilledFormatAndhraBank.csv"
    elem8 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="UnBilled",
                                                                 feedPattern="^UNBILL.*\.xls",
                                                                 feedParams=params,
                                                                 feedFilters=['df["Amount"]=df["DRAMT"]+df["CRAMT"]','df.loc[df["DRCR"]=="D","DrCRIND"]="Dr"','df.loc[df["DRCR"]=="C","DrCRIND"]="Cr"'],
                                                                 resultkey="unbilledf"))
    params={}
    filters=['df.loc[df["Transaction Type"]=="9","DrCRIND"]="Cr"','df.loc[df["Transaction Type"]!="9","DrCRIND"]="Dr"','df["Card Number"]="`"+df["Card Number"].str[0:16]','df["Transaction Amount"]=df["Transaction Amount"].astype(np.float64)','df["Transaction Amount"]=df["Transaction Amount"]/100','df["BIN"]=df["Card Number"].str[1:7]','df["BIN"]=df["BIN"].astype(np.int64)']
    elem9 = dict(type="PreLoader", properties=dict(loadType="BTH", source="BTH",
                                                   feedPattern="^.*\.bth",
                                                   feedParams=params,
                                                   feedFilters=filters,
                                                   resultkey="bthdf"))

    bth_bin_update = dict(type="VLookup", properties=dict(data="bthdf", lookup="binmaster",
                                                 dataFields=["BIN"],
                                                 lookupFields=["BIN"],
                                                 includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                 resultkey="bthdf"))

    bth_bin_update_filters = ["df['Settlement Type']='ISSUER'", "df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'"]

    bth_bin_update_eval = dict(type="ExpressionEvaluator",
                 properties=dict(source="bthdf", resultkey="bthdf", expressions=bth_bin_update_filters))

    claimfilter=['df.loc[df["FEED_FILE_NAME"].str[-3:]=="CLM","DrCRIND"]="Dr"','df.loc[df["FEED_FILE_NAME"].str[-3:]=="STL","DrCRIND"]="Cr"']
    params={}
    params["feedformatFile"] = "Reference/ATMCLMFormat.csv"
    elem10 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VisaDebitCardClaim",
                                                   feedPattern=".*\.(CLM|STL)",
                                                   feedParams=params,
                                                   feedFilters=claimfilter,
                                                   resultkey="visadcdf"))

    params = {}
    #params["feedformatFile"] = "Reference/ATMCLMFormat.csv"
    rupayfilter=['df["nPAN"]','df["nPAN"]="`"+df["nPAN"].str[0:16]','df["BIN"]=df["nPAN"].str[1:7]','df["BIN"]=df["BIN"].astype(np.int64)','df.loc[df["nSetDCInd"]=="D","DrCRIND"]="Dr"','df.loc[df["nSetDCInd"]=="C","DrCRIND"]="Cr"','df["nAmtTxn"]=df["nAmtTxn"].astype(np.float64)','df["nAmtTxn"]=df["nAmtTxn"]/100',"df['Transaction Code']=df['nMTI'].astype(str)+df['nFunCd'].astype(str)","df['BTHRRN']=df['nARD'].str[10:-1]"]
    elem11 = dict(type="PreLoader", properties=dict(loadType="RupayXML", source="RUPAYOC",
                                                    feedPattern="^000AND.*\.xml",
                                                    feedParams=params,
                                                    feedFilters=rupayfilter,
                                                    resultkey="rupayoutdf"))

    params = {}
    # params["feedformatFile"] = "Reference/ATMCLMFormat.csv"
    #rupayfilter = {}
    elem12 = dict(type="PreLoader", properties=dict(loadType="RupayXML", source="RUPAYIC",
                                                    feedPattern="^0[1-9]1ANDB.*\.xml",
                                                    feedParams=params,
                                                    feedFilters=rupayfilter,
                                                    resultkey="rupayindf"))

    elem13 = dict(type="VLookup", properties=dict(data="rupayindf", lookup="binmaster",
                                                 dataFields=["BIN"],
                                                 lookupFields=["BIN"],
                                                 includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                 resultkey="rupayindf"))

    elem14 = dict(type="VLookup", properties=dict(data="rupayoutdf", lookup="binmaster",
                                                  dataFields=["BIN"],
                                                  lookupFields=["BIN"],
                                                  includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                  resultkey="rupayoutdf"))

    acqissmarker = ["df['Settlement Type']='ISSUER'", "df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'"]
    rupayoutexp = dict(id=4, next=5, type="ExpressionEvaluator",
                  properties=dict(source="rupayoutdf", resultkey="rupayoutdf", expressions=acqissmarker))

    rupaylookup = dict(type="VLookup", properties=dict(data="rupayoutdf", lookup="crdrmapping",
                                                  dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                  lookupFields=["DataSource", "Settlement Type", "Transaction Code"],
                                                  includeCols=["DrCRIND", "Description"],
                                                  resultkey="rupayoutdf"))

    params = {}
    params["feedformatFile"] = "Reference/VisaFixedFormatTCR00.csv"
    elem15 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISAOG",
                                                   feedPattern="^visaout.*.*",
                                                   feedParams=params,
                                                   feedFilters=visafilter+["df['BTHRRN']=df['ARN '].str[10:-1]"],
                                                   resultkey="visaoutgoingdf"))

    elem16 = dict(type="VLookup", properties=dict(data="visaoutgoingdf", lookup="binmaster",
                                                 dataFields=["BIN"],
                                                 lookupFields=["BIN"],
                                                 includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                 resultkey="visaoutgoingdf"))


    elem17 = dict(id=4, next=5, type="ExpressionEvaluator",
                 properties=dict(source="visaoutgoingdf", resultkey="visaoutgoingdf", expressions=acqissmarker))

    elem18 = dict(type="VLookup", properties=dict(data="visaoutgoingdf", lookup="crdrmapping",
                                                 dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                 lookupFields=["DataSource", "Settlement Type", "Transaction Code"],
                                                 includeCols=["DrCRIND", "Description"],
                                                 resultkey="visaoutgoingdf"))

    elem19=dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="visadf",
                                                               columns=dict(keyColumns=["ARN ", "Card Number","DrCRIND"],
                                                                            sumColumns=["Source Amount"],
                                                                            matchColumns=["ARN ", "Card Number","DrCRIND","Source Amount"]),
                                                               sourceTag="VISA IC"), dict(source="unbilledf",
                                                                                         columns=dict(
                                                                                             keyColumns=["REFNO",
                                                                                                         "CARDNO","DrCRIND"],
                                                                                             sumColumns=[
                                                                                                 "Amount"],
                                                                                             matchColumns=[
                                                                                                 "REFNO",
                                                                                                 "CARDNO",
                                                                                                 "DrCRIND",
                                                                                                 "Amount"]),
                                                                                         sourceTag="UNBILLED")],matchResult="results"))
    #nApprvlCd
    elem20 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="rupayindf",
                                                                        columns=dict(keyColumns=["nARD", "nPAN",
                                                                                                 "DrCRIND"],
                                                                                     sumColumns=["nAmtTxn"],
                                                                                     matchColumns=["nARD",
                                                                                                   "nPAN",
                                                                                                   "DrCRIND",
                                                                                                   "nAmtTxn"]),
                                                                        sourceTag="RUPAYIC"), dict(source="results.UNBILLED",
                                                                                                   columns=dict(
                                                                                                       keyColumns=[
                                                                                                           "REFNO",
                                                                                                           "CARDNO",
                                                                                                           "DrCRIND"],
                                                                                                       sumColumns=[
                                                                                                           "Amount"],
                                                                                                       matchColumns=[
                                                                                                           "REFNO",
                                                                                                           "CARDNO",
                                                                                                           "DrCRIND",
                                                                                                           "Amount"]),
                                                                                                   sourceTag="UNBILLED")],
                                                          matchResult="results"))



    #bht_expression = dict(id=4, next=5, type="ExpressionEvaluator",
    #             properties=dict(source="bthdf", resultkey="bthdf", expressions=["df.head().to_csv('/Users/shivashankar/PycharmProjects/smartrecon/bthdf.csv', index = False)"]))

    #visa_expression = dict(id=4, next=5, type="ExpressionEvaluator",
    #                      properties=dict(source="visaoutgoingdf", resultkey="visaoutgoingdf", expressions=["df = df['Settlement Type'] == 'ACQUIRER']","df.head().to_csv('/Users/shivashankar/PycharmProjects/smartrecon/visaoutgoing.csv', index = False)"]))

    # nApprvlCd
    elem21 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="bthdf",
                                                                        columns=dict(keyColumns=["Card Number",
                                                                                                 "Approval Code","Transaction Amount"],

                                                                                     matchColumns=[
                                                                                                   "Card Number",

                                                                                                   "Approval Code","Transaction Amount"]),
                                                                        sourceTag="BTH"),
                                                                   dict(source="visaoutgoingdf",
                                                                        columns=dict(
                                                                            keyColumns=[
                                                                                "Card Number","Authorization Code","Source Amount"],

                                                                            matchColumns=[
                                                                                "Card Number","Authorization Code","Source Amount"]),
                                                                        sourceTag="VISAOG")],
                                                          matchResult="results"))
#'''sumColumns=["nAmtTxn"]''',sumColumns=["Transaction Amount"],
    elem22= dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="results.BTH",
                                                                        columns=dict(keyColumns=["Card Number","Retrieval reference number",
                                                                                                 "Approval Code"],

                                                                                     matchColumns=[
                                                                                         "Card Number",
                                                                                         "Retrieval reference number",
                                                                                         "Approval Code",
                                                                                         ]),
                                                                        sourceTag="BTH"),
                                                                   dict(source="rupayoutdf",
                                                                        columns=dict(
                                                                            keyColumns=["nPAN","BTHRRN","nApprvlCd"],

                                                                            matchColumns=[
                                                                                "nPAN","BTHRRN","nApprvlCd",
                                                                                ]),
                                                                        sourceTag="RUPAYOG")],
                                                          matchResult="results"))


    elements = [elem1,elem2,elem3,elem4,elem5,elem6,elem7,elem8,elem9,bth_bin_update, bth_bin_update_eval,elem10,elem11,elem12,elem13,elem14,rupayoutexp,rupaylookup,elem15,elem16,elem17,elem18,elem19,elem20,elem21,elem22]
    f = FlowRunner(elements)
    f.run()
#    f.result['rupaybins'].to_csv("Output/rupaybins.csv")
    for x in f.result['results'].keys():
        for col in f.result['results'][x].columns:
            f.result['results'][x]=f.result['results'][x].astype(str)
        f.result['results'][x].to_csv("Output/" + x + ".csv")
    #for key in f.result.keys():
    #    print key
    #    if isinstance(f.result[key], pd.DataFrame):
    #        f.result[key].to_csv("Output/"+key+".csv")
    #f.result['visaoutgoingdf'].to_csv("Output/visaoutgoingparsed.csv")
    #f.result['unbilledf'].head().to_csv("Output/unbilled.csv")
    #f.result['visadf'].head().to_csv("Output/unbilled.csv")
    #f.result['rupayindf'].to_csv("Output/rupayincoming.csv")
    #print f.result['bthdf']["DrCRIND"].unique()
    #print df.head()
    #print df["Transaction Code"].unique()
    #print df[df['Card Type'] == '']
    #df.loc[df['Card Type'] == '', 'Settlement Type'] = 'ACQUIRER'
    #print df[df['Card Type'] == '']
    #f.result['unbilledf'].to_csv("Output/unbilledtransactions.csv", index=False)
    #df[(df["Transaction Code Qualifier"] == "00") & (df["Transaction Code"].isin(["05", "06", "07", "15", "20", "25", "27"]))]