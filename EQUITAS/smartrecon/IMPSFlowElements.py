from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "08-Dec-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"
    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100"]
    cbs_filters = ["df = df[df['BRANCH_CODE'] == 9999]",
                   "df.loc[:, 'Ref Number'] = df['TRANSACTION_DESCRIPTION'].str.extract('([0-9]{12})')"]

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="IMPS", compressionType="zip", resultkey=''))
    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                                 feedPattern="ACQRPEQT*.*mEQT",
                                                                 feedParams=dict(type="OUTWARD"),
                                                                 feedFilters=npci_filters,
                                                                 resultkey="npcidf"))
    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern="IMPS_OUTWARD*.*dsv",
                                                                 feedParams=dict(delimiter="|", skiprows=1,
                                                                                 feedFileStructure='IMPS_CBS_Structure.csv'),
                                                                 feedFilters=cbs_filters,
                                                                 resultkey="cbsdf"))

    npciSuccTxnFilter = ["df = df[df['Response Code'] == '00']"]
    elem4 = dict(id=4, next=5, type="ExpressionEvaluator",
                 properties=[dict(source="npcidf", resultkey="npciSuccdf", expressions=npciSuccTxnFilter)])

    leftcol = dict(keyColumns=["Ref Number"], sumColumns=['Transaction Amount'],
                   matchColumns=["Ref Number", "Transaction Amount"])
    rightcol = dict(keyColumns=["Ref Number"], sumColumns=['Transaction Amount'],
                    matchColumns=["Ref Number", "Transaction Amount"])
    elem5 = dict(id=5, next=6, type="PerformMatch", properties=dict(leftData="npciSuccdf", rightData="cbsdf",
                                                                    left=leftcol, right=rightcol, leftUnmatch='npciexp',
                                                                    rightUnmatch='cbsexp', matchResult='outwardMatch'))

    cbsCreditRevFilter = ["df = df[df['CREDIT_DEBIT'] == 'C']"]
    cbsDebitRevFilter = ["df = df[df['CREDIT_DEBIT'] == 'D']"]
    elem6 = dict(id=6, next=7, type="ExpressionEvaluator",
                 properties=[dict(source="cbsexp", resultkey="cbscreditdf", expressions=cbsCreditRevFilter),
                             dict(source="cbsexp", resultkey="cbsdebitdf", expressions=cbsDebitRevFilter)])

    elem7 = dict(id=7, next=8, type="PerformMatch", properties=dict(leftData="cbscreditdf", rightData="cbsdebitdf",
                                                                    left=leftcol,
                                                                    right=rightcol,
                                                                    leftUnmatch="cbsexp",
                                                                    rightUnmatch="cbsexp",
                                                                    matchResult='cbsReversalDf'))

    elem8 = dict(id=8, next=9, type="NPCIReportsLoader", properties=dict(timeOutReport='Time Out Cases Report*.*xls'))

    elem9 = dict(id=9, type="PerformMatch", properties=dict(leftData="npciTimeOutReport", rightData="cbsexp",
                                                            left=dict(keyColumns=["Ref Number"],
                                                                      matchColumns=["Ref Number"]),
                                                            right=dict(keyColumns=["Ref Number"],
                                                                       matchColumns=["Ref Number"]),
                                                            leftUnmatch="npciTimeoutResult",
                                                            rightUnmatch="cbsexp",
                                                            matchResult="cbsTimeoutDf"))

    elements = [elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8, elem9]
    f = FlowRunner(elements)
    f.run()
