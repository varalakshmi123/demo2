from datetime import datetime
import re
from traceback import format_exc

import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

class EOMCHLoader(FeedLoader):
    def __init__(self):
        pass
    def getType(self):
        return "EOMCH"

    def loadFile(self, fileNames, params):
        df_cwddepdata = pd.DataFrame()
        df_retract = pd.DataFrame()
        final_data = []
        finame = []
        df_daywiseatm = []
        rstreport_data = []
        rstreport_data_new = []
        pre_timing = {}
        post_timing = {}
        for file in fileNames:
            with open(file, 'r') as f:
                fname = file
                finame.append(file.split('-')[0])
                lines = f.read().splitlines()
                cas, rstcas = 0, 0
                cwddep = 0
                count = 0
                rstcount = 0
                casCnt = 0
                rstcasCnt = 0
                rem = 0
                add = False
                doc = []
                data = []
                rstreport_data_dup, rstdenom = [], {'2000': [], '500': [], '100': [], '50': [], 'VOID': []}
                rstreport_data_dict = {'ret_2000': [], 'ret_500': [], 'ret_100': [], 'ret_50': [], 'ret_VOID': [],
                                       'ATM ID': []}
                cwddepdata = []
                cwddepdata_dict = {'ej_dispense_count': [], 'ej_dispense_amount': [], 'ej_deposit_count': [],
                                   'ej_deposit_amount': [], 'ATM_ID': []}
                cwd = []
                cw = 0
                dep = []
                de = 0
                rep = []

                cwd_dict = {'denom': [], 'count': []}
                dep_dict = {'denom': [], 'count': []}
                cwddata = []
                depdata = []
                rstdata = []
                repdata = []
                cbrdata = []
                cwdrjidata = []
                closing = {'cassette': [], 'denom': [], 'Closing Balance': [], 'branch': '', 'date': '', 'ATM_ID': []}
                rep_dict = {'denom': [], 'count': []}
                rst_dict = {'denom': [], 'count': []}
                replenishment = False
                final_dic = {'2000': {}, '500': {}, '100': {}, '50': {}}
                # looping over the lines in the file
                for i, l in enumerate(lines):
                    # serch fofprintr TIA CARD BALANCING FOR second Time
                    if "TIA CARD BALANCING" in l:
                        count = count + 1
                    if count >= 2:
                        if casCnt == 0:
                            # print l
                            if '           TXN CNT-AMOUNT' in l:
                                cwddep = True
                            if '           DENOM NOTE CNT-AMOUNT' in l:
                                cwddep = False

                            if 'DENOM NOTE CNT-AMOUNT' in l:
                                cas = True
                            if 'REMAINING NOTES' in l:
                                rem += 1

                            if rem == 1 and 'TOTAL' in l:
                                casCnt += 1
                                data.append(l)
                                cas = False
                    if cwddep:
                        if 'TXN CNT-AMOUNT' in l:
                            if (re.match(r'D', lines[i - 3])):
                                linst_post = lines[i - 3]
                            elif (re.match(r'D', lines[i - 4])):
                                linst_post = lines[i - 4]
                            instdate_today = linst_post.split()[0].split(':')[1]
                            insttime_today = linst_post.split()[1].split('E:')[1].replace(':', ',')
                            a_post = linst_post.split()[0].split(':')[1] + ' ' + linst_post.split()[1].split('E:')[1]
                            a_post = datetime.strptime(a_post, '%m/%d/%y %H:%M:%S')
                            b_post = datetime.strftime(a_post, '%Y-%m-%d %H:%M:%S')
                            # post_timing[fname.split('-')[0]] = b_post
                            if not 'ATM ID' in post_timing.keys():
                                post_timing['ATM ID'] = []
                                post_timing['POSTTIME'] = []
                            post_timing['ATM ID'].append(fname.split('/')[-1].split('-')[0])
                            post_timing['POSTTIME'].append(b_post)

                        cwddepdata.append(l)
                        # print cwddepdata

                    if cas:
                        data.append(l)
                    if 'REPLENISHMENT' in l.strip():
                        replenishment = True

                    # Extract Closing Balance Part in File
                    if replenishment:
                        if (re.search('CASSETTE[2-9]', l) and (len(l.split()) == 3)):
                            cassette, denom, count = tuple(l.split())
                            closing['cassette'].append(cassette)
                            closing['denom'].append(denom)
                            closing['Closing Balance'].append(count)
                        elif re.search(r'[0-9]{2}/[0-9]{2}/[0-9]{2}', l.strip()):
                            break
                            # looping over the data list which contains all data and getting rst,def,retract lines
                for id, l in enumerate(lines):
                    if "TIA CARD BALANCING" in l:
                        rstcount = rstcount + 1
                    if rstcount >= 2:
                        if rstcasCnt == 0:
                            if 'CASSETTE1' in l:
                                rstcas = True
                            if 'CASSETTE2' in l or 'CASSETTE3' in l:
                                rstcasCnt += 1
                    if rstcasCnt == 0 and rstcas:
                        if 'CASSETTE1' in l:
                            if re.match(r'\D{4}\s{1}\D{7}:', lines[id - 4]):
                                linst_pre = lines[id - 4]
                                # print linst_pre
                                a_pre = linst_pre.split('D:')[1].rstrip()
                                if (len(a_pre.split()[1].split(':'))) == 2:
                                    a_pre = a_pre + ':00'
                                    a_pre = datetime.strptime(a_pre, '%m/%d/%y %H:%M:%S')
                                    b_pre = datetime.strftime(a_pre, '%Y-%m-%d %H:%M:%S')
                                else:
                                    a_pre = datetime.strptime(a_pre, '%m/%d/%y %H:%M:%S')
                                    b_pre = datetime.strftime(a_pre, '%Y-%m-%d %H:%M:%S')
                                # print fname
                                # print b_pre
                                # pre_timing[fname.split('/')[-1].split('-')[0]] = b_pre
                                if not 'ATM ID' in pre_timing.keys():
                                    pre_timing['ATM ID']=[]
                                    pre_timing['PRETIME']=[]
                                pre_timing['ATM ID'].append(fname.split('/')[-1].split('-')[0])
                                pre_timing['PRETIME'].append(b_pre)

                        rstreport_data_dup.append(l)
                        rstreport_data.append(l + ' ' + fname.split('-')[0])
                        # looping over the required data (cwddeplist) and getting the cwd and dep values
            for i, line in enumerate(cwddepdata):
                if 'CWD' in line:
                    final_data.append({'ATM ID': fname.split('/')[-1].split('-')[0], 'ej_dispense_count': line.split()[1].split('-')[0],
                                       'ej_dispense_amount': line.split()[1].split('-')[1]})
                if 'DEP' in line:
                    final_data.append({'ATM ID': fname.split('/')[-1].split('-')[0], 'ej_deposit_count': line.split()[1].split('-')[0],
                                       'ej_deposit_amount': line.split()[1].split('-')[1]})
                    # looping over the required data and getting the cwd data out toa list called cwd
            for i, line in enumerate(lines):
                if '##STAN##' in line:
                    if 'BANK' in lines[i + 1]:
                        branch = line
                else:
                    branch = 'NA'
                if 'LAST CLEARED:' in line:
                    date = line.split()[1].split(':')[1]
            closing['branch'] = (branch)
            closing['date'] = (date)
            closing['ATM_ID'].append(fname)
            for i, line in enumerate(data):
                if 'DENOM NOTE CNT-AMOUNT' in line:
                    cw = True
                if 'DEP' in line:
                    break
                if cw:
                    cwd.append(line)
            if len(cwd) > 0:
                del cwd[0]
            else:
                cwd = []
            # cwd is a list which contains the cwd data and here we are writing that data to  dict called cwd_dict
            for i in cwd:
                if (len(i.split('-')[0].split())) == 2:
                    cwd_dict['denom'].append(i.split('-')[0].split()[0])
                    cwd_dict['count'].append(i.split('-')[0].split()[1])
                elif (len(i.split('-')[0].split())) == 3:
                    cwd_dict['denom'].append(i.split('-')[0].split()[1])
                    cwd_dict['count'].append(i.split('-')[0].split()[2])
            for i in rstreport_data:
                if i not in rstreport_data_new:
                    rstreport_data_new.append(i)

        for id, i in enumerate(rstreport_data_new):
            if 'RST' in i:
                rstreport_data_dict['ret_2000'].append(i.split()[1:-1][0])
                rstreport_data_dict['ret_500'].append(i.split()[1:-1][1])
                rstreport_data_dict['ret_100'].append(i.split()[1:-1][2])
                rstreport_data_dict['ret_50'].append(i.split()[1:-1][3])
                rstreport_data_dict['ret_VOID'].append(i.split()[1:-1][4])
                rstreport_data_dict['ATM ID'].append(i.split('/')[-1])
            if 'DEF' in i:
                rstreport_data_dict['ret_2000'].append(i.split()[1:-1][0])
                rstreport_data_dict['ret_500'].append(i.split()[1:-1][1])
                rstreport_data_dict['ret_100'].append(i.split()[1:-1][2])
                rstreport_data_dict['ret_50'].append(i.split()[1:-1][3])
                rstreport_data_dict['ret_VOID'].append(i.split()[1:-1][4])
                rstreport_data_dict['ATM ID'].append(i.split('/')[-1])
            if 'RETRACT' in i:
                rstreport_data_dict['ret_2000'].append(i.split()[1:-1][0])
                rstreport_data_dict['ret_500'].append(i.split()[1:-1][1])
                rstreport_data_dict['ret_100'].append(i.split()[1:-1][2])
                rstreport_data_dict['ret_50'].append(i.split()[1:-1][3])
                rstreport_data_dict['ret_VOID'].append(i.split()[1:-1][4])
                rstreport_data_dict['ATM ID'].append(i.split('/')[-1])

        # here we go the retract data from the file into a dictionary called rstreport_data_dict and converti ng it into the dataframe
        df_retract = pd.DataFrame(rstreport_data_dict)
        # filters
        df_retract[['ret_2000', 'ret_500', 'ret_100', 'ret_50', 'ret_VOID']] = df_retract[
            ['ret_2000', 'ret_500', 'ret_100', 'ret_50', 'ret_VOID']].astype(np.int64)
        df_retract = df_retract.groupby(['ATM ID'], as_index=False)[
            'ret_2000', 'ret_500', 'ret_100', 'ret_50', 'ret_VOID'].sum()
        df_retract['Retract Count'] = df_retract.sum(axis=1)
        params['payload']['df_daywise_initial'] = params['payload']['df_daywise_initial'].merge(df_retract[['ATM ID', 'Retract Count']], on=['ATM ID'], how='outer',
                                      indicator=False)

        df_cwddepdata = df_cwddepdata.append(final_data)
        df_cwddepdata = df_cwddepdata.fillna(0)
        df_cwddepdata[['ej_deposit_amount', 'ej_deposit_count', 'ej_dispense_amount', 'ej_dispense_count']] = \
        df_cwddepdata[['ej_deposit_amount', 'ej_deposit_count', 'ej_dispense_amount', 'ej_dispense_count']].astype(
            np.float64)
        df_cwddepdata = df_cwddepdata.groupby(['ATM ID'], as_index=False)[
            'ej_dispense_count', 'ej_deposit_count', 'ej_dispense_amount', 'ej_deposit_amount'].sum().reset_index()
        df_cwddepdata = df_cwddepdata.drop(['index'], axis=1)
        df_daywise = params['payload']['df_daywise_initial'].merge(df_cwddepdata, on=['ATM ID'], how='outer', indicator=False)
        df_daywise = df_daywise.rename(columns={'ej_dispense_count': 'Dispense_Count of Txns - EJ',
                                                'ej_deposit_count': 'Deposit_Count of Txns - EJ',
                                                'ej_dispense_amount': 'Dispense as per EJ',
                                                'ej_deposit_amount': 'Deposits as per EJ'})

        pre_timing_df=pd.DataFrame(pre_timing)
        post_timing_df=pd.DataFrame(post_timing)
        df_daywise = df_daywise.merge(pre_timing_df, on=['ATM ID'], how='left', indicator=False)
        df_daywise = df_daywise.merge(post_timing_df, on=['ATM ID'], how='left', indicator=False)
        return df_daywise