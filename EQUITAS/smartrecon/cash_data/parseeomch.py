import sys
# from StyleFrame import StyleFrame, Styler
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas
import parseeonch
import parseokieomco
import parseokieonco
if __name__ == "__main__":
    stmtdate = '03-Aug-2018'
    uploadFileName = 'combined.zip'
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, reconName="EOMCH", recontype='HITACHI',
                                 compressionType="zip", resultkey='',disableAllCarryFwd=True))


    cbrfilters=[
                "df['ATM ID']=df['ATM ID'].replace({'': np.nan}).ffill()",
                "df['Sol Id']=df['Sol Id'].astype(str).fillna('').str.replace('\.(0)*','')",
                 "df['Sol Id']=df['Sol Id'].replace({'': np.nan}).ffill()",
                 "branch_code_list_dup=[]",
                 "atm_id_list=[]",
                 "branch_code_list_dup.extend(df[df['ATM ID'].str.contains('EOMCH')]['Sol Id'].tolist())",
                  "atm_id_list.extend(df['ATM ID'][df['ATM ID'].str.contains('EOMCH')].unique())",
                  "payload['branch_code_list']=list(set(branch_code_list_dup))",
                  "df = df[df['ATM ID'].str.contains('EOMCH')]",
                  "df['ATM Location']=df['ATM Location'].replace({'': np.nan}).ffill()",
                ]

    cbrload = dict(type="PreLoader", properties=dict(loadType='Excel', source="CBS",
                                                    feedParams={"feedformatFile": 'CBR_STRUCT.csv',
                                                                "skiprows": 1},
                                                    feedPattern="Recycler*.*",
                                                    resultkey="cbrdf", feedFilters=cbrfilters))

    expression1=["dfreq=df.copy()",
                 "payload['dfreq'] = dfreq[dfreq['ATM Location'] != 'Grand Total']",
                 "df_daywise = payload['dfreq'].groupby(['Sol Id','ATM Location','ATM ID','Transaction Type'],as_index= False)['Opening balance_amount','Cash Dispense_amount','Cash Deposit_amount','Overage_amount','Shortage_amount','Closing Before replishment_amount','Cash Offload/Removal_amount','Fresh Cash Loading_amount','Closing Balance_amount'].sum().reset_index()",
                 "payload['df_daywise_initial'] = df_daywise.drop(['index'], axis=1)",

                 ]

    exp1 = dict(type="ExpressionEvaluator", properties=dict(source="cbrdf",
                                                            expressions=expression1,
                                                            resultkey='cbrdf'))

    eomchfilters=["payload['df_daywiseatm']=[]",
                 "payload['df_daywiseatm'].append(df[df['ATM ID'].str.contains('EOMCH')])",
                  "cdf=df[['ATM ID','POSTTIME','PRETIME']]",
                  "payload['post_timing']={row['ATM ID']:row['POSTTIME'] for index,row in df.iterrows()}",
                  "payload['pre_timing']={row['ATM ID']:row['PRETIME'] for index,row in df.iterrows()}",
                  "df=df.drop(labels=['POSTTIME','PRETIME'],axis=1)",

                  ]


    eomchload = dict(type="PreLoader", properties=dict(loadType='EOMCH', source="EOMCH",
                                                       feedParams={},
                                                    feedPattern="EOMCH*.*",
                                                    resultkey="df_daywise", feedFilters=eomchfilters))
    gldumpprevfilters=[
                    "payload['glprevcopy']=df.copy()",
                    "df = df[(df['COD CC BRN'].isin(payload['branch_code_list']))&(df['COD DRCR'] == 'C') & (df['MN TXT TXN DESC'].isin(['ATM CASH WITHDRAWAL','ATM. Off Us Notification']))]",
                    "df['Sol Id'] = df['COD CC BRN'].copy()",

                    "df = df.drop_duplicates(subset = ['Sol Id'])",
                    "cbrdf = payload['cbrdf'].drop_duplicates(subset = ['Sol Id'])",

                    "df = df.merge(cbrdf[['Sol Id','ATM ID']],how='left',indicator = False)",

        # "payload['glprevcopy'] = payload['glprevcopy'].merge(df[['Sol Id','ATM ID']],how = 'left',indicator = False)",

                    "df=df[df['ATM ID'].fillna('')!='']",
                    "pre_timing=payload['pre_timing']",
                    # "df['TIME']=pd.Series([pre_timing[x] for index,x in  df['ATM ID'].iteritems()])",
                    "doc={}",
                    "doc['ATM ID']=[key for key,val in pre_timing.iteritems() ]",
                    "doc['TIME']=[val for key,val in pre_timing.iteritems() ]",
                    "timedf=pd.DataFrame(doc)",
                    "df=df.merge(timedf,on=['ATM ID'],how='left',suffixes=('','_y'))",
                    "df=df[df['TIME'].fillna('')!='']",
                    "df['TIME'] = pd.to_datetime(df['TIME'],format = '%Y-%m-%d %H:%M:%S')",
        "df['dayscount_credit'] = df['TIME'] - df['DAT TXN PROCESSING']",
        "df = df[df['dayscount_credit'].apply(lambda x: x.days == -1)]",
        "maper=[{row['ATM ID']: row['Sol Id']} for index, row in payload['cbrdf'].iterrows()]",
        "payload['maper']=  map(dict, set(tuple(sorted(d.items())) for d in maper))",
        "df['count'] = 1",
            "print df.columns.tolist()",
        "df = df.groupby(['ATM ID', 'Sol Id'], as_index=False)['AMT TXN LCY', 'count'].sum()",
        "df = df.rename(columns={'count': 'tran_count_credit'})",

    ]

    gldumpprev = dict(type="PreLoader", properties=dict(loadType='Excel', source="GLDumpPrev",
                                                       feedParams={"feedformatFile":'GLDUMP_CASH_BALANCING_STRUCT.csv',
                                                                   'skiprows':1},
                                                    feedPattern="GL_DUMP1_as_on_#%d-%b-%Y#.xlsx",
                                                    resultkey="gldumpprev", feedFilters=gldumpprevfilters,delta=1))


    gldumppresentfilters = [
        "payload['glpresentcopy']=df.copy()",
        "df = df[(df['COD CC BRN'].isin(payload['branch_code_list']))&(df['COD DRCR'] == 'C') & (df['MN TXT TXN DESC'].str.contains('ATM CASH WITHDRAWAL|ATM. Off Us Notification',regex = True))]",
        "df['Sol Id'] = df['COD CC BRN'].copy()",

        "df = df.drop_duplicates(subset = ['Sol Id','ATM ID'])",

        "df = df.merge(payload['cbrdf'][['Sol Id','ATM ID']],how='left',indicator = False)",
        # "print df.columns.tolist()","print dfp.columns.tolist()",


        "post_timing=payload['post_timing']",
        "df=df[df['ATM ID'].fillna('')!='']",
        "doc={}",
        "doc['ATM ID']=[key for key,val in post_timing.iteritems() ]",
        "doc['TIME']=[val for key,val in post_timing.iteritems() ]",
        "timedf=pd.DataFrame(doc)",
        "df=df.merge(timedf,on=['ATM ID'],how='left',suffixes=('','_y'))",
        "df=df[df['TIME'].fillna('')!='']",
        # "df['TIME']=pd.Series([post_timing[x] for index,x in  df['ATM ID'].iteritems()])",
        "df['TIME'] = pd.to_datetime(df['TIME'],format = '%Y-%m-%d %H:%M:%S')",
        "df['dayscount_credit'] = df['TIME'] - df['DAT TXN PROCESSING']",

        "df = df[df['dayscount_credit'].apply(lambda x: x.days == 0)]",
        "df['count'] = 1",
        "df = df.groupby(['ATM ID', 'Sol Id'], as_index=False)['AMT TXN LCY', 'count'].sum()",
        "df = df.rename(columns={'count': 'tran_count_credit'})",
        # "payload['glpresentcopy'] = payload['glpresentcopy'].merge(df[['Sol Id','ATM ID','tran_count_credit']],how = 'left',indicator = False)",

    ]

    gldumppresent= dict(type="PreLoader", properties=dict(loadType='Excel', source="GLDumpPresent",
                                                       feedParams={"feedformatFile":'GLDUMP_CASH_BALANCING_STRUCT.csv',
                                                                   'skiprows':1},
                                                    feedPattern="GL_DUMP1_as_on_#%d-%b-%Y#.xlsx",
                                                    resultkey="gldumppresent", feedFilters=gldumppresentfilters))

    expression2=[
                 "dfc_aug2 = payload['gldumpprev'].append(payload['gldumppresent'])",
    "dfc_aug2 = dfc_aug2.groupby(['ATM ID'],as_index= False)['AMT TXN LCY','tran_count_credit'].sum()",
    "df = df.merge(dfc_aug2,on = ['ATM ID'],how = 'outer',indicator=False)",
    "df =df.rename(columns ={'tran_count_credit':'Count of Wdl Txn','AMT TXN LCY':'Wdl Txns in Host'})"]


    exp2 = dict(type="ExpressionEvaluator", properties=dict(source="df_daywise",
                                                            expressions=expression2,
                                                            resultkey='df_daywise'))



    gldumprevdebit=[ "df = df[(df['COD CC BRN'].isin(payload['branch_code_list']))&(df['COD DRCR'] == 'D') & (df['MN TXT TXN DESC'].str.contains('ATM CASH DEPOSIT',regex = True))]",
                    "df['Sol Id'] = df['COD CC BRN'].copy()",
                    "df = df.drop_duplicates(subset = ['Sol Id','ATM ID'])",
                    "df = df.merge(payload['cbrdf'][['Sol Id','ATM ID']],how='left',indicator = False)",
                     "df=df[df['ATM ID'].fillna('')!='']",
                    "pre_timing=payload['pre_timing']",
                    # "df['TIME']=pd.Series([pre_timing[x] for index,x in  df['ATM ID'].iteritems()])",
                     "doc={}",
                     "doc['ATM ID']=[key for key,val in pre_timing.iteritems() ]",
                     "doc['TIME']=[val for key,val in pre_timing.iteritems() ]",
                     "timedf=pd.DataFrame(doc)",
                     "df=df.merge(timedf,on=['ATM ID'],how='left',suffixes=('','_y'))",
                     "df=df[df['TIME'].fillna('')!='']",
                    "df['TIME'] = pd.to_datetime(df['TIME'],format = '%Y-%m-%d %H:%M:%S')",
        "df['dayscount_credit'] = df['TIME'] - df['DAT TXN PROCESSING']",
        "df = df[df['dayscount_credit'].apply(lambda x: x.days == -1)]",
        "df['count'] = 1",
        "df = df.groupby(['ATM ID', 'Sol Id'], as_index=False)['AMT TXN LCY', 'count'].sum()",
        "df = df.rename(columns={'count': 'tran_count_credit'})"]


    exp3 = dict(type="ExpressionEvaluator", properties=dict(source="glprevcopy",
                                                            expressions=gldumprevdebit,
                                                            resultkey='glprevcopy'))


    gldumppresentdebit=[
        "payload['postgl']=df.copy()",
        "df = df[(df['COD CC BRN'].isin(payload['branch_code_list']))&(df['COD DRCR'] == 'D') & (df['MN TXT TXN DESC'].str.contains('ATM CASH DEPOSIT',regex = True))]",
        "df['Sol Id'] = df['COD CC BRN'].copy()",
       "print df.columns.tolist()",
        "df = df.drop_duplicates(subset = ['Sol Id','ATM ID'])",
        "df = df.merge(payload['cbrdf'][['Sol Id','ATM ID']],how='left',indicator = False)",
        "df=df[df['ATM ID'].fillna('')!='']",
        "post_timing=payload['post_timing']",
        "doc={}",
        "doc['ATM ID']=[key for key,val in post_timing.iteritems() ]",
        "doc['TIME']=[val for key,val in post_timing.iteritems() ]",
        "timedf=pd.DataFrame(doc)",
        "df=df.merge(timedf,on=['ATM ID'],how='left',suffixes=('','_y'))",
        "df=df[df['TIME'].fillna('')!='']",
        # "df['TIME']=pd.Series([post_timing[x] for index,x in  df['ATM ID'].iteritems()])",
        "df['TIME'] = pd.to_datetime(df['TIME'],format = '%Y-%m-%d %H:%M:%S')",
        "df['dayscount_credit'] = df['TIME'] - df['DAT TXN PROCESSING']",
        "df = df[df['dayscount_credit'].apply(lambda x: x.days == 0)]",
        "df['count'] = 1",
        "df = df.groupby(['ATM ID', 'Sol Id'], as_index=False)['AMT TXN LCY', 'count'].sum()",
        "df = df.rename(columns={'count': 'tran_count_credit'})"]

    exp4 = dict(type="ExpressionEvaluator", properties=dict(source="glpresentcopy",
                                                            expressions=gldumppresentdebit,
                                                            resultkey='glpresentcopy'))

    expression3 = [
                    "dfc_aug2 = payload['glprevcopy'].append(payload['glpresentcopy'])",
                   "dfc_aug2 = dfc_aug2.groupby(['ATM ID'],as_index= False)['AMT TXN LCY','tran_count_credit'].sum()",
                   "df = df.merge(dfc_aug2,on = ['ATM ID'],how = 'outer',indicator=False)",
                   "df =df.rename(columns ={'tran_count_credit':'Count of Wdl Txn','AMT TXN LCY':'Wdl Txns in Host'})"]


    exppo = dict(type="ExpressionEvaluator", properties=dict(source="df_daywise",
                                                            expressions=expression3,
                                                            resultkey='df_daywise'))


    glhand = dict(type="PreLoader", properties=dict(loadType='Excel', source="GLHAND",
                                                           feedParams={
                                                               "feedformatFile": 'CASH_IN_GL_HAND_Struct.csv',
                                                               'skiprows': 1},
                                                           feedPattern="Cash_in_Hand_GL_*.*",
                                                           resultkey="glhand", feedFilters=["df = df[ (df['FCRBRANCHCODE'].isin(payload['branch_code_list']))&(df['ACC'].astype(str).str.contains('110020001'))]",
                                                                                            "df['Sol Id'] = df['FCRBRANCHCODE'].copy()",
                                                                                            "doc={}",
                                                                                            "doc['ATM ID']=[key for d in payload['maper'] for key,val in d.iteritems() ]",
                                                                                            "doc['Sol Id']=[val for d in payload['maper'] for key,val in d.iteritems() ]",
                                                                                            "payload['mapdf']=pd.DataFrame(doc)",
                                                                                            "df=df.merge(payload['mapdf'],on=['Sol Id'],how='left',suffixes=('','_y'))",
                                                                                            "print df.columns",

                                                                                            # "df=df.drop(labels=['ATM ID_y'],axis=1)",
                                                                                            "payload['df_daywise'] = payload['df_daywise'].merge(df[['ATM ID','BALLCY']],on = ['ATM ID'],how= 'outer',indicator = False)",
                                                                                            "payload['df_daywise'] = payload['df_daywise'].rename(columns = {'BALLCY':'GL Closing Bal'})"
                                                        
                                                                                            ]))

    postexpevalfilters=[
                        "df['COD CC BRN'] = df['COD CC BRN'].astype(str)",
                        "df['AMT TXN LCY_DUP'] = df['AMT TXN LCY']",
                        "df =df[(df['COD CC BRN'].isin(payload['branch_code_list']))&(df['MN TXT TXN DESC'].isin(['ATM. Off Us Notification','ATM CASH WITHDRAWAL','ATM CASH DEPOSIT']))]",
                        "df.loc[df['COD DRCR'] == 'D','AMT TXN LCY_DUP']  *=-1",
                        "df['DAT TXN PROCESSING'] = pd.to_datetime(df['DAT TXN PROCESSING'],format = '%Y-%m-%d %H:%M:%S')",
                        "df['COD CC BRN'] = df['COD CC BRN'].astype(str)",
                        "df['Sol Id'] = df['COD CC BRN'].copy()",
                        "df=df.merge(payload['mapdf'],on=['Sol Id'],how='left',suffixes=('','_y'))",
                        # "df=df.drop(labels='ATM ID_y')",
                        "df=df[df['ATM ID'].fillna('')!='']",
                        "post_timing=payload['post_timing']",
                        "doc={}",
                        "doc['ATM ID']=[key for key,val in post_timing.iteritems() ]",
                        "doc['TIME']=[val for key,val in post_timing.iteritems() ]",
                        "timedf=pd.DataFrame(doc)",
                        "df=df.merge(timedf,on=['ATM ID'],how='left',suffixes=('','_y'))",
                        "df=df[df['TIME'].fillna('')!='']",
                        # "df['TIME']=pd.Series([post_timing[x] for index,x in  df['ATM ID'].iteritems()])",
                        "df['TIME'] = pd.to_datetime(df['TIME'],format = '%Y-%m-%d %H:%M:%S')",
                        "df['dayscount_post_tia'] = df['TIME'] - df['DAT TXN PROCESSING']",
                        "df = df[df['dayscount_post_tia'].apply(lambda x: x.days == -1)]",
                        "df = df.groupby(['ATM ID'], as_index=False)['AMT TXN LCY_DUP'].sum()",
                        "payload['df_daywise'] = payload['df_daywise'].merge(df,on = ['ATM ID'],how = 'outer',indicator = False)",
                        "payload['df_daywise'] = payload['df_daywise'].rename(columns = {'AMT TXN LCY_DUP':'Txns Post TIA'})",
                        "payload['df_daywise']['Difference'] = (2 *payload['df_daywise']['GL Closing Bal'])+(2*payload['df_daywise']['Closing Balance_amount']) - (2*payload['df_daywise']['Txns Post TIA'])",
                        "payload['df_daywise']['Remarks'] = 'Not Talied'",
                        "payload['df_daywise'].loc[payload['df_daywise']['Difference'] == 0.0,'Remarks'] = 'Talied'"
        ]
    
    postexpressioneval = dict(type="ExpressionEvaluator", properties=dict(source="postgl",
                                                            expressions=postexpevalfilters,
                                                            resultkey='postgl'))

    exp7 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='df_daywise',
                                              sourceTag="DAYWISE")], resultKey='df_daywise',
                                writeToFile=True,
                                reportName='DAYWISE_EOMCH'))


    concat = dict(type="ExpressionEvaluator", properties=dict(source="custom_reports.DAYWISE_EOMCH",
                                                            expressions=["df=pd.concat([df,payload['custom_reports']['DAYWISE_EONCH'],payload['custom_reports']['DAYWISE_OKIEOMCO'],payload['custom_reports']['DAYWISE_EONOCO']],join_axes=[df.columns])"],
                                                            resultkey='postgl'))

    conso = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='postgl',
                                              sourceTag="DAYWISE")], resultKey='postgl',
                                writeToFile=True,
                                reportName='CONSO'))


    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elem19 = dict(type="DumpData",
                  properties=dict())





    elements = [elem1,cbrload,exp1,eomchload,gldumpprev,gldumppresent,exp2,exp3,exp4,exppo,glhand,postexpressioneval,exp7]

    eonch = parseeonch.EONCH()
    okieomco=parseokieomco.OKIEOMCO()
    okieonco=parseokieonco.OKIEONOCO()

    elements1 = [concat,conso,gen_meta_info, elem19]

    elements.extend(eonch)
    elements.extend(okieomco)
    elements.extend(okieonco)

    elements.extend(elements1)

    f = FlowRunner(elements)

    f.run()

