from datetime import datetime
import re
from traceback import format_exc

import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

class EONCOOKILoader(FeedLoader):
    def __init__(self):
        pass
    def getType(self):
        return "EONCOOKI"

    def loadFile(self, fileNames, params):
        llist = []
        dict = []
        val = ''
        cbal = []
        data_list = []
        datasum_list = []
        pre_timing = {}
        post_timing = {}
        dictej = {'Deposit_Count of Txns - EJ': [], 'Dispense_Count of Txns - EJ': [], 'ATM ID': [],
                  'Retract_count': []}
        ejdepdis = []
        df_ejdepdis = pd.DataFrame()
        df3 = pd.DataFrame(columns=['Denom', 'OpeningbalanceCount', 'CashDispense/DepositCount',
                                    'ClosingBeforeReplishmentCount', 'Sample', 'ATM_ID',
                                    'ClosingBalanceCount'])
        closedf = pd.DataFrame(columns=['ClosingBalanceCount', 'ATM_ID', 'Denom'])
        result = pd.DataFrame()
        for file in fileNames:
            with open(file, 'r') as f:
                df1 = pd.DataFrame()
                df2 = pd.DataFrame()
                filename = file
                fname = filename.split('/')[-1]
                tiaCount = 0
                casCount = 0
                repCount = 0
                add = False
                data = []
                closing = ''
                req = 0
                cas = 0
                casret = 0
                replenishment = False
                lines = f.read().splitlines()
                for id, line in enumerate(lines):
                    if 'TIA CARD BALANCING' in line:
                        tiaCount += 1
                    if tiaCount >= 2:
                        if casCount == 0:
                            if re.search(r'\*\*\*\*\s+C.+', line):
                                if 'Start' in lines[id - 2] and re.search(r'\d{2}-\d{2}-\d{4}',
                                                                          lines[id - 2].split('Start:')[1].lstrip()):
                                    # pre_timing[fname] = lines[id - 2].split('Start:')[1].lstrip()
                                    if not 'ATM ID' in pre_timing.keys():
                                        pre_timing['ATM ID'] = []
                                        pre_timing['PRETIME'] = []
                                    pre_timing['ATM ID'].append(fname.split('-')[0])
                                    pre_timing['PRETIME'].append(lines[id - 2].split('Start:')[1].lstrip())
                                casret = True
                            if re.match(r'---\D{8}', line):
                                if re.match(r'\d{2}-\d{2}-\d{4}', lines[id + 2].lstrip()):
                                    casret = False
                            if casret:
                                if 'CAS_F' in line:
                                    dictej['Retract_count'].append(line.split('-')[1].split()[1] + '+' + fname.split('-')[0])
                            if re.search(r'\*\*\*\*\s+D.+', line):
                                if re.search(r'\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}:\d{2}', lines[id - 1].lstrip()):
                                    # post_timing[fname] = lines[id - 1].lstrip()
                                    if not 'ATM ID' in post_timing.keys():
                                        post_timing['ATM ID'] = []
                                        post_timing['POSTTIME'] = []
                                    post_timing['ATM ID'].append(fname.split('-')[0])
                                    post_timing['POSTTIME'].append(lines[id - 1].lstrip())
                                cas = True
                            if 'Total amount' in line:
                                casCount += 1
                                data.append(line)
                                cas = False
                                if re.search(r'\d{2}-\d{2}-\d{2}', lines[id + 8]):
                                    if re.search(r'\*\*\sTOTAL', lines[id + 9]):
                                        ejdepdis.append(lines[id + 11:id + 13] + [fname])
                                elif re.search(r'\d{2}-\d{2}-\d{2}', lines[id + 9]):
                                    if re.search(r'\*\*\sTOTAL', lines[id + 10]):
                                        ejdepdis.append(lines[id + 12:id + 14] + [fname])
                                else:
                                    ejdepdis.append([' Total Deposit Transaction       :    0',
                                                     ' Total Withdrawal Transaction    :    0'] + [fname])

                    if cas == True:
                        if 'Denomination' not in line:
                            data.append(line)
                    if 'Replenishment' in line:
                        req = id
                close = lines[req:req + 6]
                for cl in close:
                    if re.search(r'\sCn.+', cl):
                        closing = cl
                    else:
                        closing = ''
                closing = closing.split()
                tmp = [re.split(r'\s{2,}', i) for i in data[1:]]
                if tmp:
                    data_list.extend([x + [fname] for x in tmp])
                if len(closing) > 0:
                    if re.match(r'^\D{3}', closing[0]):
                        del closing[0]
                closing = np.transpose(closing)
                # making closing balance counts into one dataframe
                df2 = pd.DataFrame((closing), columns=['ClosingBalanceCount'], index=None)
                df2['Denom'] = ['Rs.100', 'Rs.500', 'Rs.2000', 'all', 'ALL']
                df2['ATM_ID'] = fname.split('-')[0]
                amount_lookup = {'Rs.100': 0, 'Rs.500': 1, 'Rs.2000': 2}
                # removing totalamount row from the data
                if len(data_list) > 0:
                    datasum_list.append((data_list[len(data_list) - 1][2], data_list[len(data_list) - 1][3],
                                         data_list[len(data_list) - 1][5].split('-')[0]))
                    del data_list[len(data_list) - 1]
                df1 = pd.DataFrame(data_list,
                                   columns=['Denom', 'OpeningbalanceCount', 'CashDispense/DepositCount',
                                            'ClosingBeforeReplishmentCount',
                                            'Sample', 'ATM_ID'])
                dfsum = pd.DataFrame(datasum_list,
                                     columns=['Dispense as per EJ',
                                              'Deposits as per EJ',
                                              'ATM ID'])
                df1['Denom'] = df1['Denom'].str.replace('Rs.', '')
                closedf = closedf.append(df2)
            closedf = closedf[~closedf['Denom'].isin(['all', 'ALL'])]
            df3 = df3.append(df1)
        for i in ejdepdis:
            dictej['Deposit_Count of Txns - EJ'].append(i[0].split(':')[1].lstrip())
            dictej['Dispense_Count of Txns - EJ'].append(i[1].split(':')[1].lstrip())
            dictej['ATM ID'].append(i[2].split('-')[0])
        for i in dictej['Retract_count']:
            if i not in llist:
                llist.append(i)
        dictej['Retract_count'] = [i.split('+')[0] for i in dictej['Retract_count'] if
                                       i.split('+')[1] in dictej['ATM ID']]
        df_ejdepdis = pd.DataFrame(dictej)
        dfsum = dfsum.merge(df_ejdepdis, on=['ATM ID'], how='outer', indicator=False)
        df_daywise = params['payload']['df_daywise_initial'].merge(dfsum, on=['ATM ID'], how='outer',indicator=False)

        pre_timing_df = pd.DataFrame(pre_timing)
        post_timing_df = pd.DataFrame(post_timing)
        df_daywise = df_daywise.merge(pre_timing_df, on=['ATM ID'], how='left', indicator=False)
        df_daywise = df_daywise.merge(post_timing_df, on=['ATM ID'], how='left', indicator=False)
        df_daywise = df_daywise[df_daywise['PRETIME'].fillna('') != '']
        df_daywise = df_daywise[df_daywise['POSTTIME'].fillna('') != '']
        return df_daywise
