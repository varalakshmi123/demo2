import pandas as pd
import os
import re
import numpy as np
cbs = pd.read_excel('/home/varun/Downloads/parsing/Requirements to Algo Team 090818/19-12-2018/GL_DUMP1_as_on_19-Dec-2018 (3).xlsx')

ej = pd.read_csv('/tmp/ej_data.csv')

# cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Acc No'] = cbs['GL TXT TXN DESC'].str[:12].str.strip()
# making atm off us notifications transactions as cash with drawal transactions
cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM. Off Us Notification','MN TXT TXN DESC'] = 'ATM CASH WITHDRAWAL'
# taking only cash withdrawal transactions from cbs
cbs = cbs[cbs['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL']
# taking only cash withdrawal transactions from ej
ej = ej[ej['TRANSACTION MODE IN EJ'] == 'CASHWITHDRAWAL']
# making "cash with drawal"  value in trnasaction mode column as "atm cash withdrawal"
ej.loc[ej['TRANSACTION MODE IN EJ'] == 'CASHWITHDRAWAL','TRANSACTION MODE IN EJ'] = 'ATM CASH WITHDRAWAL'
# taking ref_doc_no column as RRN_Match in cbs
cbs['RRN_Match'] = cbs['REF DOC NO'].copy()
# for equitas bank cards taking the rrn  as last four digits of the rrn in cbs
cbs.loc[cbs['GL TXT TXN DESC'].str.startswith('508998') | cbs['GL TXT TXN DESC'].str.startswith('508999') |cbs['GL TXT TXN DESC'].str.startswith('472357'),'RRN_Match'] = cbs['GL TXT TXN DESC'].str.split().str[1]
# taking RRN column as RRN_Match in ej
ej['RRN_Match'] = ej['RRN'].copy()
# coinvert RRN column to str type
ej['RRN']= ej['RRN'].fillna('0').astype(str)
ej['RRN_Match'] = ej['RRN'].copy()
# for equitas bank cards taking the rrn  as last four digits of the rrn in ej
ej.loc[ej['Card No'].str.startswith('508998') | ej['Card No'].str.startswith('508999') |ej['Card No'].str.startswith('472357'),'RRN_Match'] =  ej['RRN'].str[8:].str.strip()

# taking only rrn ,amount,tran_desc from cbs
cbs_withdraw = cbs[['RRN_Match','AMT TXN LCY','MN TXT TXN DESC']]
# taking only rrn,amount,tran_desc from the ej
ej_withdraw = ej[['RRN_Match','Amount in EJ','TRANSACTION MODE IN EJ']]

ej_withdraw['Amount in EJ'] = ej_withdraw['Amount in EJ'].fillna('0')
ej_withdraw['Amount in EJ'] = ej_withdraw['Amount in EJ'].str.replace(',','')
ej_withdraw['Amount in EJ'] = ej_withdraw['Amount in EJ'].astype(np.float64)

cbs_withdraw['RRN_Match'] = cbs_withdraw['RRN_Match'].fillna('0')
cbs_withdraw['RRN_Match'] = cbs_withdraw['RRN_Match'].astype(str)

cbs_withdraw['AMT TXN LCY'] = cbs_withdraw['AMT TXN LCY'].fillna('0')
cbs_withdraw['AMT TXN LCY'] = cbs_withdraw['AMT TXN LCY'].astype(str).astype(np.float64)

print cbs_withdraw.dtypes
print ej_withdraw.dtypes
# ej.to_csv('/tmp/ej_pre.csv')
# cbs.to_csv('/tmp/cbs_pre.csv')

cbs_withdraw.to_csv('/tmp/cbs_withdraw.csv')
ej_withdraw.to_csv('/tmp/ej_withdraw.csv')

exit()

# merging ej with cbs based on ther rrn and amount
ej_withdraw = ej_withdraw.merge(cbs_withdraw,left_on = ['RRN_Match','Amount in EJ'],right_on = ['RRN_Match','AMT TXN LCY'],how = 'left')
print ej_withdraw.columns
print  ej_withdraw['MN TXT TXN DESC']
ej_withdraw.to_csv('/tmp/res.csv')
exit()
cbs = cbs[cbs['MN TXT TXN DESC'].isin(['ATM CASH WITHDRAWAL','ATM CASH DEPOSIT','ATM. Off Us Notification'])]
cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL','Card No'] = cbs['GL TXT TXN DESC'].str[:16].str.strip()
cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL','Acc No'] = cbs['GL TXT TXN DESC'].str[-12:].str.strip()

cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Card No'] = '0000000000000000'
cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Acc No'] = cbs['GL TXT TXN DESC'].str[:12].str.strip()


cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM. Off Us Notification','Card No'] =  cbs['GL TXT TXN DESC'].str[:16].str.strip()
cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM. Off Us Notification','Acc No'] = '000000000000'

cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM. Off Us Notification','MN TXT TXN DESC'] = 'ATM CASH WITHDRAWAL'




# ej = ej[ej['TRANSACTION MODE IN EJ'].isin(['CASHWITHDRAWAL'])]
ej.loc[ej['TRANSACTION MODE IN EJ'] == 'CASHWITHDRAWAL','Card No_Match'] = ej['Card No'].str[:6] + ej['Card No'].str[-4:]
ej.loc[ej['TRANSACTION MODE IN EJ'].isin(['CASH DEPOSIT','CARD LESS CASH DEPOSIT']),'Card No_Match'] = '0000000000000000' + ej['Card No'].str[-4:]
ej['RRN_Match'] = ej['RRN'].copy()
l = ['508998','508999','472357']
ej['Card No'] = ej['Card No'].fillna('0')
ej.loc[ej['Card No'].str.startswith('508998') | ej['Card No'].str.startswith('508999') |ej['Card No'].str.startswith('472357'),'RRN_Match'] = ej['RRN']

ej.loc[ej['TRANSACTION MODE IN EJ'] == 'CASHWITHDRAWAL','TRANSACTION MODE IN EJ'] = 'ATM CASH WITHDRAWAL'
ej.loc[ej['TRANSACTION MODE IN EJ'].isin(['CASH DEPOSIT','CARD LESS CASH DEPOSIT']),'TRANSACTION MODE IN EJ'] = 'ATM CASH DEPOSIT'

ej['RRN_Match'] = ej['RRN_Match'].fillna('0')
ej['RRN_Match'] = ej['RRN_Match'].astype(str)
ej['Amount in EJ']=ej['Amount in EJ'].astype(str).str.replace('\.(0)*','')
print ej['Card No_Match']
exit()
ej['re_col'] = ej['TRANSACTION MODE IN EJ'] +ej['Amount in EJ']+ej['Card No_Match'] +ej['RRN_Match'] +ej['Account Number']


print ej['Card No_Match']
exit()
cbs['REF DOC NO'] = cbs['REF DOC NO'].fillna('0')
cbs['REF DOC NO'] = cbs['REF DOC NO'].astype(str)
cbs['REF DOC NO']=cbs['REF DOC NO'].astype(str).str.replace('\.(0)*','')
cbs['AMT TXN LCY'] = cbs['AMT TXN LCY'].fillna('0')
cbs['AMT TXN LCY'] = cbs['AMT TXN LCY'].astype(str)
# print cbs.dtypes


# exit()
cbs['re_col'] = cbs['MN TXT TXN DESC'] +cbs['AMT TXN LCY']+cbs['Card No'] +cbs['REF DOC NO'] +cbs['Acc No']
print cbs['REF DOC NO']
exit()

# cbs = cbs[['AMT TXN LCY','REF DOC NO','Card No','Acc No','MN TXT TXN DESC']]
# ej = ej[['TRANSACTION MODE IN EJ','Amount in EJ','Card No_Match','RRN_Match','Account Number']]

ej.loc[ej['TRANSACTION MODE IN EJ'] == 'ATM CASH DEPOSIT','Card No_Match'] = '0'
ej['Amount in EJ'] = ej['Amount in EJ'].str.replace(',','')

ej['']


exit()
switch = pd.read_csv('/usr/share/nginx/smartrecon/mft/EOMCH_TRANLEVEL/19122018/Equitas_TRXNDUMP_REPORT_19122018.csv')
# print "===================================="
# print switch.dtypes

print ej.columns

# print "===================================="

# switch['Transaction Description'] = switch['Transaction Description'].str.strip()
# switch = switch[switch['Transaction Description'].isin(['WITHDRAWAL','Cash Deposit','CASH DEPOSIT  W/O CARDO CARD'])]

# cbs = cbs[cbs['MN TXT TXN DESC'].isin(['ATM CASH WITHDRAWAL','ATM CASH DEPOSIT'])]
# cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL','Card No'] = cbs['GL TXT TXN DESC'].str[:16]
# cbs.loc[cbs['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Card No'] = cbs['GL TXT TXN DESC'].str[:12]