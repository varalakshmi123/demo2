import sys

sys.path.append('/usr/share/nginx/smartrecon/')
import config
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                reconName='UCIC', uploadFileName=uploadFileName, recontype="UCIC",
                                compressionType="zip", resultkey=''))

    ucic_data = dict(type="PreLoader", properties=dict(loadType="Excel", source="UCIC",
                                                       feedPattern="UCIC.xlsx",
                                                       feedParams={"feedformatFile": "UCIC_Structure.csv",
                                                                   "skiprows": 1},
                                                       feedFilters=["df = df[df['UCIC'].fillna('') != '']"],
                                                       resultkey="ucic_df", disableCarryFwd=True))

    pan_data = dict(type="PreLoader", properties=dict(loadType="Excel", source="PAN",
                                                      feedPattern="PAN.xlsx",
                                                      feedParams={"feedformatFile": "PAN_Structure.csv",
                                                                  "skiprows": 1},
                                                      feedFilters=["df = df[df['PAN_NUMBER'].fillna('') != '']"],
                                                      resultkey="pan_df", disableCarryFwd=True))

    vlookup = dict(type='VLookup',
                   properties=dict(data='ucic_df', lookup='pan_df', dataFields=['Loan_Number'],
                                   lookupFields=['Loan_Number'], includeCols=['PAN_NUMBER','Aadhaar','Voters ID','Passport'],
                                   resultkey="ucic_df_look"))

    ucic_report = dict(type="ReportGenerator", properties=dict(
        sources=[
            dict(source='ucic_df_look', filterConditions=[
                "df.sort_values(by = ['Loan_Number','UCIC'],inplace = True)"])],
        writeToFile=True, reportName='PAN_Report'))

	
    ucic_duplicate_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='ucic_df_look',
                      filterConditions=["df = df[(df['PAN_NUMBER'].fillna('') != '') & (df['PAN_NUMBER'] != '0')]",
                                        "df.drop_duplicates(subset=['PAN_NUMBER','UCIC'], keep='first', inplace = True)",
                                        "df = df[df.duplicated(subset = ['PAN_NUMBER'], keep = False)]",
                                        "df = df[['PAN_NUMBER', 'UCIC']]",
                                        "df.sort_values(by=['PAN_NUMBER'], inplace = True)"])],
        writeToFile=True, reportName='Duplicate_PAN_Report'))
    nwaymatch_ucic = dict(type="NWayMatch",
                            properties=dict(sources=[dict(source="ucic_df",
                                                          columns=dict(
                                                              keyColumns=["Loan_Number"
                                                                          ],
                                                              sumColumns=[],
                                                              matchColumns=["Loan_Number"
                                                                            ]),
                                                          sourceTag="UCIC"),
                                                     dict(source="pan_df", columns=dict(
                                                         keyColumns=["Loan_Number"
                                                                     ],
                                                         sumColumns=[],
                                                         matchColumns=["Loan_Number"
                                                                      ]),
                                                          sourceTag="PAN")],
                                            matchResult="results"))
    same_pan_number_filter =  dict(type="ExpressionEvaluator",
                                        properties=dict(source="ucic_df_look",
                                                        expressions=["dfpan = df.copy()",
                                                            "dfpan = dfpan[dfpan['PAN_NUMBER'] != '']",
                                                                     "dfpan_final = dfpan[['PAN_NUMBER','UCIC']]",
                                                                     "dfpan_final['PAN_NUMBER'] = dfpan_final['PAN_NUMBER'].fillna('')",
                                                                    "dfpan_final['UCIC'] = dfpan_final['UCIC'].astype(str).fillna('')",
                                                                    "dfpan_final = dfpan_final[dfpan_final['PAN_NUMBER'] != '']",
                                                                    "dfpan_final = dfpan_final.groupby(['PAN_NUMBER', 'UCIC']).size().reset_index(name='counts')",
                                                                    "df_duppan = dfpan_final[dfpan_final.duplicated(subset=['PAN_NUMBER'], keep=False)]",
                                                                    "df_duppan = df_duppan[['PAN_NUMBER', 'UCIC']]","df = df_duppan.copy()"],
                                                        resultkey='ucic_df_look_pan'))
    same_pan_filter_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='ucic_df_look_pan',
                                           filterConditions=[
                                               "df = df[df['PAN_NUMBER'] != '']"],
                                           sourceTag="SAME_PAN_FILTER"
                                           )],
                                      writeToFile=True, reportName='same_pan_filter'))
    same_pan_merge = dict(type='SimpleMerge',
                          properties=dict(merge_data='ucic_df_look_pan', merge_lookup='ucic_df_look',
                                          merge_how='left',
                                          merge_dataFields=['PAN_NUMBER','UCIC'],
                                          merge_lookupFieldsFilter = ["df['UCIC'] = df['UCIC'].astype(str)"],
                                          merge_includeCols=['Loan_Number'],
                                          merge_lookupFields=['PAN_NUMBER','UCIC'],
                                          resultkey="pan_merge"))
    same_pan_merge_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='pan_merge',
                                           filterConditions = ["df = df.drop_duplicates(subset = ['PAN_NUMBER','UCIC'])",
                                                                   "df = df[['Loan_Number','PAN_NUMBER','UCIC']]"],
                                           sourceTag="SAME_PAN_MERGE_REPORT"
                                           )],
                                      writeToFile=True, reportName='SAME_PAN_NUMBER'))


    same_aadhar_filter = dict(type="ExpressionEvaluator",
                           properties=dict(source="ucic_df_look",
                                           expressions=["dfaadhar = df.copy()",
                                                            "dfaadhar = dfaadhar[dfaadhar['Aadhaar'] != '']",
                                                                     "dfaadhar_final = dfaadhar[['Aadhaar','UCIC']]",
                                                                     "dfaadhar_final['Aadhaar'] = dfaadhar_final['Aadhaar'].fillna('')",
                                                                    "dfaadhar_final['UCIC'] = dfaadhar_final['UCIC'].astype(str).fillna('')",
                                                                    "dfaadhar_final = dfaadhar_final[dfaadhar_final['Aadhaar'] != '']",
                                                                    "dfaadhar_final = dfaadhar_final.groupby(['Aadhaar', 'UCIC']).size().reset_index(name='counts')",
                                                                    "df_dupaadhar = dfaadhar_final[dfaadhar_final.duplicated(subset=['Aadhaar'], keep=False)]",
                                                                    "df_dupaadhar = df_dupaadhar[['Aadhaar', 'UCIC']]","df = df_dupaadhar.copy()"],
                                           resultkey='ucic_df_look_aadhar'))
    same_aadhar_filter_report = dict(type="ReportGenerator",
                                   properties=dict(sources=[
                                       dict(source='ucic_df_look_aadhar',
                                            filterConditions=[
                                                "df = df[df['Aadhaar'] != '']"],
                                            sourceTag="SAME_AADHAR_FILTER"
                                            )],
                                       writeToFile=True, reportName='same_aadhar_filter'))
    same_aadhar_merge = dict(type='SimpleMerge',
                           properties=dict(merge_data='ucic_df_look_aadhar', merge_lookup='ucic_df_look',
                                           merge_how='left',
                                           merge_dataFields=['Aadhaar','UCIC'],
                                           merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)"],
                                           merge_includeCols=['Loan_Number'],
                                           merge_lookupFields=['Aadhaar','UCIC'],
                                           resultkey="aadhar_merge"))
    same_aadhar_merge_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='aadhar_merge',
                                           filterConditions=["df = df.drop_duplicates(subset = ['Aadhaar','UCIC'])",
                                                                   "df = df[['Loan_Number','Aadhaar','UCIC']]"],
                                           sourceTag="SAME_AADHAR_MERGE_REPORT"
                                           )],
                                      writeToFile=True, reportName='SAME_AADHAR'))
    same_votersid_filter = dict(type="ExpressionEvaluator",
                              properties=dict(source="ucic_df_look",
                                              expressions=["dfvoter = df.copy()",
                                                            "dfvoter = dfvoter[dfvoter['Voters ID'] != '']",
                                                                     "dfvoter_final = dfvoter[['Voters ID','UCIC']]",
                                                                     "dfvoter_final['Voters ID'] = dfvoter_final['Voters ID'].fillna('')",
                                                                    "dfvoter_final['UCIC'] = dfvoter_final['UCIC'].astype(str).fillna('')",
                                                                    "dfvoter_final = dfvoter_final[dfvoter_final['Voters ID'] != '']",
                                                                    "dfvoter_final = dfvoter_final.groupby(['Voters ID', 'UCIC']).size().reset_index(name='counts')",
                                                                    "df_dupvoter = dfvoter_final[dfvoter_final.duplicated(subset=['Voters ID'], keep=False)]",
                                                                    "df_dupvoter = df_dupvoter[['Voters ID', 'UCIC']]","df = df_dupvoter.copy()"],
                                              resultkey='ucic_df_look_votersid'))
    same_votersid_filter_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[
                                         dict(source='ucic_df_look_votersid',
                                              filterConditions=[
                                                  "df = df[df['Voters ID'] != '']"],
                                              sourceTag="SAME_VOTERSID_FILTER"
                                              )],
                                         writeToFile=True, reportName='same_votersid_filter'))
    same_votersid_merge = dict(type='SimpleMerge',
                             properties=dict(merge_data='ucic_df_look_votersid', merge_lookup='ucic_df_look',
                                             merge_how='left',
                                             merge_dataFields=['Voters ID','UCIC'],
                                             merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)"],
                                             merge_includeCols=['Loan_Number'],
                                             merge_lookupFields=['Voters ID','UCIC'],
                                             resultkey="votersid_merge"))
    same_votersid_merge_report = dict(type="ReportGenerator",
                                    properties=dict(sources=[
                                        dict(source='votersid_merge',
                                             filterConditions=["df = df.drop_duplicates(subset = ['Voters ID','UCIC'])",
                                                                   "df = df[['Loan_Number','Voters ID','UCIC']]"],
                                             sourceTag="SAME_VOTERSID_MERGE_REPORT"
                                             )],
                                        writeToFile=True, reportName='SAME_VOTERSID'))
    same_passport_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="ucic_df_look",
                                                expressions=["dfpassport= df.copy()",
                                                            "dfpassport = dfpassport[dfpassport['Passport'] != '']",
                                                                     "dfpassport_final = dfpassport[['Passport','UCIC']]",
                                                                     "dfpassport_final['Passport'] = dfpassport_final['Passport'].fillna('')",
                                                                    "dfpassport_final['UCIC'] = dfpassport_final['UCIC'].astype(str).fillna('')",
                                                                    "dfpassport_final = dfpassport_final[dfpassport_final['Passport'] != '']",
                                                                    "dfpassport_final = dfpassport_final.groupby(['Passport', 'UCIC']).size().reset_index(name='counts')",
                                                                    "df_duppassport = dfpassport_final[dfpassport_final.duplicated(subset=['Passport'], keep=False)]",
                                                                    "df_duppassport = df_duppassport[['Passport', 'UCIC']]","df = df_duppassport.copy()","print df"],
                                                resultkey='ucic_df_look_passport'))
    same_passport_filter_report = dict(type="ReportGenerator",
                                       properties=dict(sources=[
                                           dict(source='ucic_df_look_passport',
                                                filterConditions=[
                                                    "df = df[df['Passport'] != '']"],
                                                sourceTag="SAME_PASSPORT_FILTER"
                                                )],
                                           writeToFile=True, reportName='same_passport_filter'))
    same_passport_merge = dict(type='SimpleMerge',
                               properties=dict(merge_data='ucic_df_look_passport', merge_lookup='ucic_df_look',
                                               merge_how='left',
                                               merge_dataFields=['Passport','UCIC'],
                                               merge_dataFieldsFilter=["df = df[df['Passport'] != '']",
                                                                         ],
                                               merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)",
                                                                         ],
                                               merge_includeCols=['Loan_Number'],
                                               merge_lookupFields=['Passport','UCIC'],
                                               resultkey="passport_merge"))
    same_passport_merge_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='passport_merge',
                                               filterConditions=["df = df.drop_duplicates(subset = ['Passport','UCIC'])",
                                                                   "df = df[['Loan_Number','Passport','UCIC']]"],
                                               sourceTag="SAME_PASSPORT_MERGE_REPORT"
                                               )],
                                          writeToFile=True, reportName='SAME_PASSPORT'))
    ucic_diffpan_filter = dict(type="ExpressionEvaluator",
                                  properties=dict(source="ucic_df_look",
                                                  expressions=["dfpan = df.copy()",
                                                               "dfpan = dfpan[dfpan['PAN_NUMBER'] != '']",
                                                               "dfpan_final = dfpan[['PAN_NUMBER','UCIC']]",
                                                               "dfpan_final['PAN_NUMBER'] = dfpan_final['PAN_NUMBER'].fillna('')",
                                                               "dfpan_final['UCIC'] = dfpan_final['UCIC'].astype(str).fillna('')",
                                                               "dfpan_final = dfpan_final[dfpan_final['PAN_NUMBER'] != '']",
                                                               "dfpan_final = dfpan_final.groupby(['PAN_NUMBER', 'UCIC']).size().reset_index(name='counts')",
                                                               "df_diffpan = dfpan_final[dfpan_final.duplicated(subset=['UCIC'], keep=False)]",
                                                               "df_diffpan = df_diffpan[['PAN_NUMBER', 'UCIC']]",
                                                               "df = df_diffpan.copy()"],
                                                  resultkey='ucic_diffpan'))
    ucic_diffpan_filter_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='ucic_diffpan',
                                           filterConditions=[
                                               "df = df[df['PAN_NUMBER'] != '']"],
                                           sourceTag="UCIC_DIFFPAN_FILTER"
                                           )],
                                      writeToFile=True, reportName='ucic_diffpan_filter'))
    ucic_pan_merge = dict(type='SimpleMerge',
                          properties=dict(merge_data='ucic_diffpan', merge_lookup='ucic_df_look',
                                          merge_how='left',
                                          merge_dataFields=['PAN_NUMBER', 'UCIC'],
                                          merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)"],
                                          merge_includeCols=['Loan_Number'],
                                          merge_lookupFields=['PAN_NUMBER', 'UCIC'],
                                          resultkey="diff_pan_merge"))
    ucic_pan_merge_report = dict(type="ReportGenerator",
                                 properties=dict(sources=[
                                     dict(source='diff_pan_merge',
                                          filterConditions=["df = df.drop_duplicates(subset = ['PAN_NUMBER','UCIC'])",
                                                            "df = df[['Loan_Number','PAN_NUMBER','UCIC']]"],
                                          sourceTag="UCIC_PAN_MERGE_REPORT"
                                          )],
                                     writeToFile=True, reportName='DIFF_PAN_NUMBER'))
    ucic_diffaadhar_filter = dict(type="ExpressionEvaluator",
                              properties=dict(source="ucic_df_look",
                                              expressions=["dfaadhar = df.copy()",
                                                           "dfaadhar = dfaadhar[dfaadhar['Aadhaar'] != '']",
                                                           "dfaadhar_final = dfaadhar[['Aadhaar','UCIC']]",
                                                           "dfaadhar_final['Aadhaar'] = dfaadhar_final['Aadhaar'].fillna('')",
                                                           "dfaadhar_final['UCIC'] = dfaadhar_final['UCIC'].astype(str).fillna('')",
                                                           "dfaadhar_final = dfaadhar_final[dfaadhar_final['Aadhaar'] != '']",
                                                           "dfaadhar_final = dfaadhar_final.groupby(['Aadhaar', 'UCIC']).size().reset_index(name='counts')",
                                                           "df_diffaadhar = dfaadhar_final[dfaadhar_final.duplicated(subset=['UCIC'], keep=False)]",
                                                           "df_diffaadhar = df_diffaadhar[['Aadhaar', 'UCIC']]",
                                                           "df = df_diffaadhar.copy()"],
                                              resultkey='ucic_diffaadhar'))
    ucic_diffaadhar_filter_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[
                                         dict(source='ucic_diffaadhar',
                                              filterConditions=[
                                                  "df = df[df['Aadhaar'] != '']"],
                                              sourceTag="UCIC_DIFFAADHAR_FILTER"
                                              )],
                                         writeToFile=True, reportName='ucic_diffaadhar_filter'))
    ucic_aadhar_merge = dict(type='SimpleMerge',
                             properties=dict(merge_data='ucic_diffaadhar', merge_lookup='ucic_df_look',
                                             merge_how='left',
                                             merge_dataFields=['Aadhaar', 'UCIC'],
                                             merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)"],
                                             merge_includeCols=['Loan_Number'],
                                             merge_lookupFields=['Aadhaar', 'UCIC'],
                                             resultkey="diff_aadhar_merge"))
    ucic_aadhar_merge_report = dict(type="ReportGenerator",
                                    properties=dict(sources=[
                                        dict(source='diff_aadhar_merge',
                                             filterConditions=["df = df.drop_duplicates(subset = ['Aadhaar','UCIC'])",
                                                               "df = df[['Loan_Number','Aadhaar','UCIC']]"],
                                             sourceTag="UCIC_AADHAR_MERGE_REPORT"
                                             )],
                                        writeToFile=True, reportName='DIFF_AADHAR'))
    ucic_diffvotersid_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="ucic_df_look",
                                                expressions=["dfvoter = df.copy()",
                                                             "dfvoter = dfvoter[dfvoter['Voters ID'] != '']",
                                                             "dfvoter_final = dfvoter[['Voters ID','UCIC']]",
                                                             "dfvoter_final['Voters ID'] = dfvoter_final['Voters ID'].fillna('')",
                                                             "dfvoter_final['UCIC'] = dfvoter_final['UCIC'].astype(str).fillna('')",
                                                             "dfvoter_final = dfvoter_final[dfvoter_final['Voters ID'] != '']",
                                                             "dfvoter_final = dfvoter_final.groupby(['Voters ID', 'UCIC']).size().reset_index(name='counts')",
                                                             "df_diffvoter = dfvoter_final[dfvoter_final.duplicated(subset=['UCIC'], keep=False)]",
                                                             "df_diffvoter = df_diffvoter[['Voters ID', 'UCIC']]",
                                                             "df = df_diffvoter.copy()"],
                                                resultkey='ucic_diffvotersid'))
    ucic_diffvotersid_filter_report = dict(type="ReportGenerator",
                                       properties=dict(sources=[
                                           dict(source='ucic_diffvotersid',
                                                filterConditions=[
                                                    "df = df[df['Voters ID'] != '']"],
                                                sourceTag="UCIC_DIFFVOTERSID_FILTER"
                                                )],
                                           writeToFile=True, reportName='ucic_diffvotersid_filter'))
    ucic_votersid_merge = dict(type='SimpleMerge',
                               properties=dict(merge_data='ucic_diffvotersid', merge_lookup='ucic_df_look',
                                               merge_how='left',
                                               merge_dataFields=['Voters ID', 'UCIC'],
                                               merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)"],
                                               merge_includeCols=['Loan_Number'],
                                               merge_lookupFields=['Voters ID', 'UCIC'],
                                               resultkey="diff_votersid_merge"))
    ucic_votersid_merge_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='diff_votersid_merge',
                                               filterConditions=[
                                                   "df = df.drop_duplicates(subset = ['Voters ID','UCIC'])",
                                                   "df = df[['Loan_Number','Voters ID','UCIC']]"],
                                               sourceTag="SAME_VOTERSID_MERGE_REPORT"
                                               )],
                                          writeToFile=True, reportName='DIFF_VOTERSID'))
    ucic_diffpassport_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="ucic_df_look",
                                                expressions=["dfpassport= df.copy()",
                                                             "dfpassport = dfpassport[dfpassport['Passport'] != '']",
                                                             "dfpassport_final = dfpassport[['Passport','UCIC']]",
                                                             "dfpassport_final['Passport'] = dfpassport_final['Passport'].fillna('')",
                                                             "dfpassport_final['UCIC'] = dfpassport_final['UCIC'].astype(str).fillna('')",
                                                             "dfpassport_final = dfpassport_final[dfpassport_final['Passport'] != '']",
                                                             "dfpassport_final = dfpassport_final.groupby(['Passport', 'UCIC']).size().reset_index(name='counts')",
                                                             "df_duppassport = dfpassport_final[dfpassport_final.duplicated(subset=['UCIC'], keep=False)]",
                                                             "df_duppassport = df_duppassport[['Passport', 'UCIC']]",
                                                             "df = df_duppassport.copy()", "print df"],
                                                resultkey='ucic_diffpassport'))
    ucic_diffpassport_filter_report = dict(type="ReportGenerator",
                                       properties=dict(sources=[
                                           dict(source='ucic_diffpassport',
                                                filterConditions=[
                                                    "df = df[df['Passport'] != '']"],
                                                sourceTag="UCIC_DIFFPASSPORT_FILTER"
                                                )],
                                           writeToFile=True, reportName='ucic_diffpassport_filter'))
    ucic_passport_merge = dict(type='SimpleMerge',
                               properties=dict(merge_data='ucic_diffpassport', merge_lookup='ucic_df_look',
                                               merge_how='left',
                                               merge_dataFields=['Passport', 'UCIC'],
                                               merge_dataFieldsFilter=["df = df[df['Passport'] != '']",
                                                                       ],
                                               merge_lookupFieldsFilter=["df['UCIC'] = df['UCIC'].astype(str)",
                                                                         ],
                                               merge_includeCols=['Loan_Number'],
                                               merge_lookupFields=['Passport', 'UCIC'],
                                               resultkey="diff_passport_merge"))
    ucic_passport_merge_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='diff_passport_merge',
                                               filterConditions=[
                                                   "df = df.drop_duplicates(subset = ['Passport','UCIC'])",
                                                   "df = df[['Loan_Number','Passport','UCIC']]"],
                                               sourceTag="UCIC_PASSPORT_MERGE_REPORT"
                                               )],
                                          writeToFile=True, reportName='DIFF_PASSPORT'))





    dumpData = dict(type="DumpData", properties=dict(dumpPath='UCIC', matched='all'))

    metaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, ucic_data, pan_data, vlookup, ucic_report,
                same_pan_number_filter,same_pan_filter_report,same_pan_merge,same_pan_merge_report,
                same_aadhar_filter,same_aadhar_filter_report,same_aadhar_merge,same_aadhar_merge_report,
                same_votersid_filter,same_votersid_filter_report,same_votersid_merge,same_votersid_merge_report,
                same_passport_filter,same_passport_filter_report,same_passport_merge,same_passport_merge_report,
                ucic_diffpan_filter,ucic_diffpan_filter_report,ucic_pan_merge,ucic_pan_merge_report,
                ucic_diffaadhar_filter,ucic_diffaadhar_filter_report,ucic_aadhar_merge,ucic_aadhar_merge_report,
                ucic_diffvotersid_filter,ucic_diffvotersid_filter_report,ucic_votersid_merge,ucic_votersid_merge_report,
                ucic_diffpassport_filter,ucic_diffpassport_filter_report,ucic_passport_merge,ucic_passport_merge_report,
                dumpData, metaInfo]

    f = FlowRunner(elements)

    f.run()

