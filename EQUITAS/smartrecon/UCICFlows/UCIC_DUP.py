import sys

sys.path.append('/usr/share/nginx/smartrecon/')
import config
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '03-Oct-2018'
    uploadFileName = 'algo_fusion_oct_3.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                reconName='UCIC', uploadFileName=uploadFileName, recontype="UCIC",
                                compressionType="zip", resultkey=''))

    ucic_data = dict(type="PreLoader", properties=dict(loadType="Excel", source="UCIC",
                                                       feedPattern="UCIC.xlsx",
                                                       feedParams={"feedformatFile": "UCIC_Structure.csv",
                                                                   "skiprows": 1},
                                                       feedFilters=["df = df[df['UCIC'].fillna('') != '']"],
                                                       resultkey="ucic_df", disableCarryFwd=True))

    pan_data = dict(type="PreLoader", properties=dict(loadType="Excel", source="PAN",
                                                      feedPattern="PAN.xlsx",
                                                      feedParams={"feedformatFile": "PAN_Structure.csv",
                                                                  "skiprows": 1},
                                                      feedFilters=["df = df[df['PAN_NUMBER'].fillna('') != '']"],
                                                      resultkey="pan_df", disableCarryFwd=True))

    vlookup = dict(type='VLookup',
                   properties=dict(data='ucic_df', lookup='pan_df', dataFields=['Loan_Number'],
                                   lookupFields=['Loan_Number'],
                                   includeCols=['PAN_NUMBER', 'Aadhaar', 'Voters ID', 'Passport'],
                                   resultkey="ucic_df_look"))

    ucic_report = dict(type="ReportGenerator", properties=dict(
        sources=[
            dict(source='ucic_df_look', filterConditions=[
                "df.sort_values(by = ['Loan_Number','UCIC'],inplace = True)"])],
        writeToFile=True, reportName='PAN_Report'))

    ucic_duplicate_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='ucic_df_look',
                      filterConditions=["df = df[(df['PAN_NUMBER'].fillna('') != '') & (df['PAN_NUMBER'] != '0')]",
                                        "df.drop_duplicates(subset=['PAN_NUMBER','UCIC'], keep='first', inplace = True)",
                                        "df = df[df.duplicated(subset = ['PAN_NUMBER'], keep = False)]",
                                        "df = df[['PAN_NUMBER', 'UCIC']]",
                                        "df.sort_values(by=['PAN_NUMBER'], inplace = True)"])],
        writeToFile=True, reportName='Duplicate_PAN_Report'))
    nwaymatch_ucic = dict(type="NWayMatch",
                          properties=dict(sources=[dict(source="ucic_df",
                                                        columns=dict(
                                                            keyColumns=["Loan_Number"
                                                                        ],
                                                            sumColumns=[],
                                                            matchColumns=["Loan_Number"
                                                                          ]),
                                                        sourceTag="UCIC"),
                                                   dict(source="pan_df", columns=dict(
                                                       keyColumns=["Loan_Number"
                                                                   ],
                                                       sumColumns=[],
                                                       matchColumns=["Loan_Number"
                                                                     ]),
                                                        sourceTag="PAN")],
                                          matchResult="results"))
    same_pan_number_filter = dict(type="ExpressionEvaluator",
                                  properties=dict(source="ucic_df_look",
                                                  expressions=["df = df.fillna('NaN')",
                                                               "df = df.groupby(['PAN_NUMBER']).sum().reset_index()"],
                                                  resultkey='ucic_df_look_pan'))
    same_pan_filter_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='ucic_df_look_pan',
                                           filterConditions=[
                                               "df = df[df['PAN_NUMBER'] != '']"],
                                           sourceTag="SAME_PAN_FILTER"
                                           )],
                                      writeToFile=True, reportName='same_pan_filter'))
    same_pan_merge = dict(type='SimpleMerge',
                          properties=dict(merge_data='ucic_df_look_pan', merge_lookup='ucic_df_look',
                                          merge_how='left',
                                          merge_dataFields=['PAN_NUMBER'],
                                          merge_includeCols=['Loan_Number', 'UCIC', 'Aadhaar', 'Voters ID', 'Passport'],
                                          merge_lookupFields=['PAN_NUMBER'],
                                          resultkey="pan_merge"))
    same_pan_merge_report = dict(type="ReportGenerator",
                                 properties=dict(sources=[
                                     dict(source='pan_merge',
                                          filterConditions=["df = df[df['PAN_NUMBER'] != '']",
                                                            "dfpan = df.copy()",
                                                            "dfpan = dfpan.groupby('PAN_NUMBER').size().reset_index(name='counts')",
                                                            "dfpan = dfpan[dfpan['counts'] >1]",
                                                            "dfpan = dfpan[dfpan['PAN_NUMBER'] != 'NaN']",
                                                            "plist = dfpan['PAN_NUMBER'].tolist()",
                                                            "df = df[df['PAN_NUMBER'].isin(plist)]",
                                                            "df = df[['PAN_NUMBER','Loan_Number','UCIC','Aadhaar','Voters ID','Passport']]"
                                                            ""],
                                          sourceTag="SAME_PAN_MERGE_REPORT"
                                          )],
                                     writeToFile=True, reportName='SAME_PAN_NUMBER'))

    same_ucic_filter = dict(type="ExpressionEvaluator",
                            properties=dict(source="ucic_df_look",
                                            expressions=["df = df.fillna('NaN')",
                                                         "df = df.groupby(['UCIC']).sum().reset_index()"],
                                            resultkey='ucic_df_look_ucic'))
    same_ucic_filter_report = dict(type="ReportGenerator",
                                   properties=dict(sources=[
                                       dict(source='ucic_df_look_ucic',
                                            filterConditions=[
                                                "df = df[df['UCIC'] != '']"],
                                            sourceTag="SAME_UCIC_FILTER"
                                            )],
                                       writeToFile=True, reportName='same_ucic_filter'))
    same_ucic_merge = dict(type='SimpleMerge',
                           properties=dict(merge_data='ucic_df_look_ucic', merge_lookup='ucic_df_look',
                                           merge_how='left',
                                           merge_dataFields=['UCIC'],
                                           merge_includeCols=['Loan_Number', 'PAN_NUMBER', 'Aadhaar', 'Voters ID',
                                                              'Passport'],
                                           merge_lookupFields=['UCIC'],
                                           resultkey="ucic_merge"))
    same_ucic_merge_report = dict(type="ReportGenerator",
                                  properties=dict(sources=[
                                      dict(source='ucic_merge',
                                           filterConditions=["df = df[df['UCIC'] != '']",
                                                             "dfucic = df.copy()",
                                                             "dfucic = dfucic.groupby('UCIC').size().reset_index(name='counts')",
                                                             "dfucic = dfucic[dfucic['counts'] >1]",
                                                             "dfucic = dfucic[dfucic['UCIC'] != 'NaN']",
                                                             "uciclist = dfucic['UCIC'].tolist()",
                                                             "df = df[df['UCIC'].isin(uciclist)]",

                                                             "df = df[['UCIC','Loan_Number','PAN_NUMBER','Aadhaar','Voters ID','Passport']]"],
                                           sourceTag="SAME_UCIC_MERGE_REPORT"
                                           )],
                                      writeToFile=True, reportName='SAME_UCIC'))
    same_aadhar_filter = dict(type="ExpressionEvaluator",
                              properties=dict(source="ucic_df_look",
                                              expressions=["df = df.fillna('NaN')",
                                                           "df = df.groupby(['Aadhaar']).sum().reset_index()"],
                                              resultkey='ucic_df_look_aadhar'))
    same_aadhar_filter_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[
                                         dict(source='ucic_df_look_aadhar',
                                              filterConditions=[
                                                  "df = df[df['Aadhaar'] != '']"],
                                              sourceTag="SAME_AADHAR_FILTER"
                                              )],
                                         writeToFile=True, reportName='same_aadhar_filter'))
    same_aadhar_merge = dict(type='SimpleMerge',
                             properties=dict(merge_data='ucic_df_look_aadhar', merge_lookup='ucic_df_look',
                                             merge_how='left',
                                             merge_dataFields=['Aadhaar'],
                                             merge_includeCols=['Loan_Number', 'PAN_NUMBER', 'UCIC', 'Voters ID',
                                                                'Passport'],
                                             merge_lookupFields=['Aadhaar'],
                                             resultkey="aadhar_merge"))
    same_aadhar_merge_report = dict(type="ReportGenerator",
                                    properties=dict(sources=[
                                        dict(source='aadhar_merge',
                                             filterConditions=["df = df[df['Aadhaar'] != '']",
                                                               "dfaadhar = df.copy()",
                                                               "dfaadhar = dfaadhar.groupby('Aadhaar').size().reset_index(name='counts')",
                                                               "dfaadhar = dfaadhar[dfaadhar['counts'] >1]",
                                                               "dfaadhar = dfaadhar[dfaadhar['Aadhaar'] != 'NaN']",
                                                               "aadharlist = dfaadhar['Aadhaar'].tolist()",
                                                               "df = df[df['Aadhaar'].isin(aadharlist)]",
                                                               "df = df[['Aadhaar','Loan_Number','PAN_NUMBER','UCIC','Voters ID','Passport']]"],
                                             sourceTag="SAME_AADHAR_MERGE_REPORT"
                                             )],
                                        writeToFile=True, reportName='SAME_AADHAR'))
    same_votersid_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="ucic_df_look",
                                                expressions=["df = df.fillna('NaN')",
                                                             "df = df.groupby(['Voters ID']).sum().reset_index()"],
                                                resultkey='ucic_df_look_votersid'))
    same_votersid_filter_report = dict(type="ReportGenerator",
                                       properties=dict(sources=[
                                           dict(source='ucic_df_look_votersid',
                                                filterConditions=[
                                                    "df = df[df['Voters ID'] != '']"],
                                                sourceTag="SAME_VOTERSID_FILTER"
                                                )],
                                           writeToFile=True, reportName='same_votersid_filter'))
    same_votersid_merge = dict(type='SimpleMerge',
                               properties=dict(merge_data='ucic_df_look_votersid', merge_lookup='ucic_df_look',
                                               merge_how='left',
                                               merge_dataFields=['Voters ID'],
                                               merge_includeCols=['Loan_Number', 'PAN_NUMBER', 'UCIC', 'Aadhaar',
                                                                  'Passport'],
                                               merge_lookupFields=['Voters ID'],
                                               resultkey="votersid_merge"))
    same_votersid_merge_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='votersid_merge',
                                               filterConditions=["df = df[df['Voters ID'] != '']",
                                                                 "dfvotersid = df.copy()",
                                                                 "dfvotersid = dfvotersid.groupby('Voters ID').size().reset_index(name='counts')",
                                                                 "dfvotersid = dfvotersid[dfvotersid['counts'] >1]",
                                                                 "dfvotersid = dfvotersid[dfvotersid['Voters ID'] != 'NaN']",
                                                                 "votersidlist = dfvotersid['Voters ID'].tolist()",
                                                                 "df = df[df['Voters ID'].isin(votersidlist)]",
                                                                 "df = df[['Voters ID','Loan_Number','PAN_NUMBER','UCIC','Aadhaar','Passport']]"],
                                               sourceTag="SAME_VOTERSID_MERGE_REPORT"
                                               )],
                                          writeToFile=True, reportName='SAME_VOTERSID'))
    same_passport_filter = dict(type="ExpressionEvaluator",
                                properties=dict(source="ucic_df_look",
                                                expressions=["df = df.fillna('NaN')",
                                                             "df = df.groupby(['Passport']).sum().reset_index()"],
                                                resultkey='ucic_df_look_passport'))
    same_passport_filter_report = dict(type="ReportGenerator",
                                       properties=dict(sources=[
                                           dict(source='ucic_df_look_passport',
                                                filterConditions=[
                                                    "df = df[df['Passport'] != '']"],
                                                sourceTag="SAME_PASSPORT_FILTER"
                                                )],
                                           writeToFile=True, reportName='same_passport_filter'))
    same_passport_merge = dict(type='SimpleMerge',
                               properties=dict(merge_data='ucic_df_look_passport', merge_lookup='ucic_df_look',
                                               merge_how='left',
                                               merge_dataFields=['Passport'],
                                               merge_includeCols=['Loan_Number', 'PAN_NUMBER', 'UCIC', 'Aadhaar',
                                                                  'Voters ID'],
                                               merge_lookupFields=['Passport'],
                                               resultkey="passport_merge"))
    same_passport_merge_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='passport_merge',
                                               filterConditions=["df = df[df['Passport'] != '']",
                                                                 "dfpassport = df.copy()",
                                                                 "dfpassport = dfpassport.groupby('Passport').size().reset_index(name='counts')",
                                                                 "dfpassport = dfpassport[dfpassport['Passport'] >1]",
                                                                 "dfpassport = dfpassport[dfpassport['Passport'] != 'NaN']",
                                                                 "passlist = dfpassport['Passport'].tolist()",
                                                                 "df = df[df['Passport'].isin(passlist)]",
                                                                 "df = df[['Passport','Loan_Number','PAN_NUMBER','UCIC','Aadhaar','Voters ID']]"],
                                               sourceTag="SAME_PASSPORT_MERGE_REPORT"
                                               )],
                                          writeToFile=True, reportName='SAME_PASSPORT'))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='UCIC', matched='all'))

    metaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, ucic_data, pan_data, vlookup, ucic_report,
                same_pan_number_filter, same_pan_filter_report, same_pan_merge, same_pan_merge_report,
                same_ucic_filter, same_ucic_filter_report, same_ucic_merge, same_ucic_merge_report,
                same_aadhar_filter, same_aadhar_filter_report, same_aadhar_merge, same_aadhar_merge_report,
                same_votersid_filter, same_votersid_filter_report, same_votersid_merge, same_votersid_merge_report,
                same_passport_filter, same_passport_filter_report, same_passport_merge, same_passport_merge_report,
                dumpData, metaInfo]

    f = FlowRunner(elements)

    f.run()

