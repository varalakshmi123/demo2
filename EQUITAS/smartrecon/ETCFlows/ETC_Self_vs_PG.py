import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '04-Jan-2018'
    uploadFileName = 'self_pg.zip'

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                 recontype="ETC", compressionType="zip", resultkey='', reconName="Self Reg vs PG",
                                 uploadFileName=uploadFileName))

    selfRegFilters = ["df = df[df['Debit Account'].astype(str) == '114210142']"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                   source="Self Registration",
                                                   feedPattern="Self Registration*.*xls",
                                                   feedParams={
                                                       "feedformatFile": "Self_Registration_Structure.csv",
                                                       "skiprows": 1, 'skipfooter': 1},
                                                   feedFilters=selfRegFilters,
                                                   resultkey="selfRegDf"))

    pgFilters = ["index = df[df['Biller Id'] == 'REFUND TRANSACTIONS'].index",
                 "df = df[:index.values[0]]",
                 "df = df.dropna(subset=['Biller Id'])",
                 "df = df[df['Ref 1'].str.match('[0-9]')]",
                 "df['Total Amount'] = df['Total Amount'].astype(np.float64)"]

    elem4 = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                   source="PG Equitas",
                                                   feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                   feedParams={
                                                       "feedformatFile": "PG_Structure.csv",
                                                       "skiprows": 3},
                                                   feedFilters=pgFilters,
                                                   resultkey="pgDf"))

    elem6 = dict(type="NWayMatch", properties=dict(sources=[dict(source="pgDf",
                                                                 columns=dict(
                                                                     keyColumns=['Ref 1'],
                                                                     sumColumns=["Total Amount"],
                                                                     matchColumns=["Ref 1"]),
                                                                 sourceTag="PG"),
                                                            dict(source="selfRegDf",
                                                                 columns=dict(
                                                                     keyColumns=["PG RefNo"],
                                                                     sumColumns=["Total Amount"],
                                                                     matchColumns=["PG RefNo"]),
                                                                 sourceTag="Self Registration")],
                                                   matchResult="results"))

    elem7 = dict(type="DumpData", properties=dict(dumpPath='Self Reg vs PG'))

    flows = [elem1, elem2, elem4, elem6, elem7]
    FlowRunner(flows).run()
