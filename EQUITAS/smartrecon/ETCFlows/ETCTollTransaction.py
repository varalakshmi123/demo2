import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '26-Jun-2019'
    uploadFileName = 'TOLL_260619.zip'

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft', outputPath="",
                                 uploadFileName=uploadFileName, recontype="ETC", compressionType="zip", resultkey='',
                                 reconName="Toll Transaction"))

    npcifilters = ["df['Unique Ref No'] = df['TransactionNo'].str.lstrip(\"'\")",
                   "df['Unique Ref No'] = df['Unique Ref No'].str.lstrip('0')",
                   "df['Unique Ref No'] = df['Unique Ref No'] + df['Tag ID']",
                   "df['Transaction Amount'] = df['Transaction Amount'].astype(np.float64) / 100.00",
                   "df['Settlement Amount'] = df['Settlement Amount'].astype(np.float64) / 100.00",
                   "df['Transaction Type'] = df['Transaction Type'].fillna('NON_FIN')"]

    load_post_settlement = dict(type="PreLoader", properties=dict(loadType="CSV",
                                                                  source="PostSettlement",
                                                                  feedPattern="842ESFB756*|841ESFB756*",
                                                                  feedParams={
                                                                      "feedformatFile": "NPCI_POST_Settlement_Structure.csv",
                                                                      "skiprows": 1, "delimiter": ","},
                                                                  feedFilters=npcifilters, resultkey="npcidf",
                                                                  derivedCols={"Unique Ref No": "str"}))

    drop_ps_nonfin = dict(type="ExpressionEvaluator",
                          properties=dict(source='npcidf', resultkey='npcidf',
                                          expressions=[
                                              "if 'CARRY_FORWARD' in df.columns: df = df[~((df['CARRY_FORWARD'] == 'Y') & (df['Transaction Type'] == 'NON_FIN'))]"]))

    gifilters = ["df['Unique Ref No'] = df['TransactionNo'].str.lstrip('0') + df['Tag ID']",
                 "df['TransactionType'] = df['TransactionType'].fillna('NON_FIN')",
                 "df['TransactionType'] = df['TransactionType'].str.replace('NON_FIN_ADV', 'NON_FIN')"]

    elem3 = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                   source="GI",
                                                   feedPattern="ETC Transaction Report.xls",
                                                   feedParams={
                                                       "feedformatFile": "ETC_TXN_Report_Structure.csv",
                                                       "skiprows": 1, 'skipfooter': 0},
                                                   feedFilters=gifilters, resultkey="gidf",
                                                   derivedCols={"Unique Ref No": "str"}))

    drop_unreg_tags = dict(type="ExpressionEvaluator",
                           properties=dict(source='gidf', resultkey='gidf',
                                           expressions=[
                                               "df = df[~((df['MobileNo'].isna()) & (df['CARRY_FORWARD'] == 'Y'))]",
                                               "df = df[~((df['TransactionType'] == 'NON_FIN') & (df['CARRY_FORWARD'] == 'Y'))]",
                                               "if 'Recon Remarks' in df.columns:"
                                               "df = df[df['Recon Remarks'].fillna('').isin(['','Not in PostSettlement'])]"]))

    cbs_gi_filters = ["df = df[df['Narration'].fillna('').str.startswith('ETCT')]",
                      "df['TRANSACTIONID'] = df['TRANSACTIONID'].astype(str).str.replace('MNAL','')",
                      r"df['Unique Ref No'] = df['TRANSACTIONID'].str.lstrip('\'').str.lstrip('0').str[:-12]",
                      "df['Unique Ref No'] = df['Unique Ref No'] + df['Tag_Id']"]

    load_cbs_gi = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                         source="CBS_GI",
                                                         feedPattern="CBS Transaction Report.xls",
                                                         feedParams={
                                                             "feedformatFile": "CBS_TXN_REPORT_Structure.csv",
                                                             "skiprows": 1},
                                                         feedFilters=cbs_gi_filters,
                                                         resultkey="cbsgidf", derivedCols={"Unique Ref No": "str"}))

    cbsgi_exp_filters = dict(type="ExpressionEvaluator",
                             properties=dict(source='cbsgidf', resultkey='cbsgidf',
                                             expressions=["if 'CBS_GI Exception' in df.columns:"
                                                          "df = df[df['CBS_GI Exception'].fillna('') != 'Y']",
                                                          "if 'Recon Remarks' in df.columns:"
                                                          "df = df[df['Recon Remarks'].fillna('').isin(['','Not in PostSettlement'])]"]))

    load_cbs_gl = dict(type="PreLoader", properties=dict(loadType="FixedHeaderCSV", source="CBS",
                                                         feedPattern="GL*.*csv",
                                                         feedParams={"feedformatFile": "ETC_GL_Structure.csv",
                                                                     "delimiter": ","},
                                                         feedFilters=["df['SOURCE_TYPE'] = 'GL'",
                                                                      "df['Ref Number'] = df['Ref Number'].str.strip()"],
                                                         resultkey="cbsdf"))

    load_cbs_allgl = dict(type="PreLoader", properties=dict(loadType="FixedHeaderCSV", source="CBS",
                                                            feedPattern="ALLGL*.csv",
                                                            feedParams={"feedformatFile": "ETC_ALLGL_Structure.csv",
                                                                        "delimiter": ","},
                                                            feedFilters=["df['SOURCE_TYPE'] = 'ALL_GL'",
                                                                         "df = df[df['Dr / Cr'] == 'C']",
                                                                         "df['Ref Number'] = df['Ref Number'].str.strip()"],
                                                            resultkey="cbsdf"))

    cbs_cbsgi_vlookup = dict(type='VLookup',
                             properties=dict(data='cbsdf', lookup='cbsgidf', dataFields=['Ref Number'],
                                             lookupFields=['CNVID'], includeCols=['TRANSACTIONID', 'Tag_Id'],
                                             resultkey="cbsdf"))

    drop_cbsmissing_records = dict(type="ExpressionEvaluator",
                                   properties=dict(source='cbsdf', resultkey='cbsdf',
                                                   expressions=[
                                                       "df = df[(df['TRANSACTIONID'] != '') & (df['Tag_Id'] != '')]"]))

    cbsgl_filters = ["df = df[df['Description'].str.contains('ETCT')]",
                     "df['Account Number'] = df['Account Number'].astype(str).str.replace('\\.0', '')",
                     "df = df[~df['Account Number'].isin(['99990204020028', '99990204020027'])]",
                     "df = df[df['Maker Id'] == 'GIUSER']", "df['Amount(LCY)'] = df['Amount(LCY)'].astype(np.float64)",
                     r"df['Unique Ref No'] = df['TRANSACTIONID'].str.lstrip('\'').str.lstrip('0').str[:-12]",
                     "df['Unique Ref No'] = df['Unique Ref No'] + df['Tag_Id']"]

    cbs_filters = dict(type="ExpressionEvaluator",
                       properties=dict(source='cbsdf', resultkey='cbsdf', expressions=cbsgl_filters))

    cbs_incr_loader = dict(type="IncrementalLoader",
                           properties=dict(source="cbsdf", sourceTag="CBS", keycolumns=['Unique Ref No'], keep='first',
                                           resultkey="cbsdf"))

    filter_npci_dup = dict(type="FilterDuplicates",
                           properties=dict(source="npcidf", duplicateStore="duplicateStore",
                                           keyColumns=["Unique Ref No"], sourceTag="PostSettlement",
                                           resultKey="npcidf", inplace=True, markerCol='duplicated'))

    filter_cbs_dup = dict(type="FilterDuplicates",
                          properties=dict(source="cbsdf", duplicateStore="duplicateStore",
                                          keyColumns=["Unique Ref No"], sourceTag="CBS",
                                          resultKey="cbsdf", inplace=True, markerCol='duplicated'))

    filter_gi_dup = dict(type="FilterDuplicates",
                         properties=dict(source="gidf", duplicateStore="duplicateStore",
                                         keyColumns=["Unique Ref No"], sourceTag="GI",
                                         resultKey="gidf", inplace=True, markerCol='duplicated'))

    filter_cbsgidf_dup = dict(type="FilterDuplicates",
                              properties=dict(source="cbsgidf", duplicateStore="duplicateStore",
                                              keyColumns=["Unique Ref No"], sourceTag="CBS_GI",
                                              resultKey="cbsgidf", inplace=True, markerCol='duplicated'))

    cbsgi_vlookup = dict(type='VLookup',
                         properties=dict(data='cbsgidf', lookup='gidf', dataFields=['Unique Ref No', 'Mobile'],
                                         lookupFields=['Unique Ref No', 'MobileNo'], includeCols=['TransactionType'],
                                         resultkey="cbsgidf"))

    first_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="npcidf",
                                                    columns=dict(keyColumns=["Unique Ref No",
                                                                            ],
                                                                 sumColumns=["Transaction Amount"],
                                                                 matchColumns=["Unique Ref No"
                                                                               ]),
                                                    sourceTag="PostSettlement",
                                                    subsetfilter=["df = df[df['duplicated'] == 'DUPLICATED']",
                                                                  "df = df[df['Transaction Type'] != 'NON_FIN']"]),
                                               dict(source="cbsdf",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No"
                                                                    ],
                                                        sumColumns=["Amount(LCY)"],
                                                        matchColumns=["Unique Ref No"
                                                                     ]),
                                                    sourceTag="CBS",
                                                    subsetfilter=[
                                                        "df = df[df['duplicated'] == 'DUPLICATED']"]),
                                               dict(source="gidf",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No"],
                                                        sumColumns=["Transaction Amount"],
                                                        matchColumns=[
                                                            "Unique Ref No"
                                                            ]),
                                                    sourceTag="GI",
                                                    subsetfilter=[
                                                        "df = df[df['duplicated'] == 'DUPLICATED']",
                                                        "df = df[df['TransactionType'] != 'NON_FIN']"]),
                                               dict(source="cbsgidf",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No",
                                                                    ],
                                                        sumColumns=['Transaction Amount'],
                                                        matchColumns=["Unique Ref No"
                                                                      ]),
                                                    sourceTag="CBS_GI", subsetfilter=[
                                                       "df = df[df['duplicated'] == 'DUPLICATED']"])],
                                      matchResult="fstresults"))

    duplicate_match = dict(type="DuplicateMatch",
                           properties=dict(sources=[dict(source="fstresults.PostSettlement",
                                                         columns=dict(keyColumns=["Unique Ref No",
                                                                                  "Transaction Amount"],
                                                                      sumColumns=[],
                                                                      matchColumns=["Unique Ref No",
                                                                                    "Transaction Amount"]),
                                                         sourceTag="PostSettlement",
                                                         subsetfilter=[
                                                             "df = df[df['duplicated'] == 'DUPLICATED']",
                                                             "df = df[df['Transaction Type'] != 'NON_FIN']"]),
                                                    dict(source="fstresults.CBS",
                                                         columns=dict(
                                                             keyColumns=["Unique Ref No",
                                                                         "Amount(LCY)"],
                                                             sumColumns=[],
                                                             matchColumns=["Unique Ref No",
                                                                           "Amount(LCY)"]),
                                                         sourceTag="CBS", subsetfilter=[
                                                            "df = df[df['duplicated'] == 'DUPLICATED']"]),
                                                    dict(source="fstresults.GI",
                                                         columns=dict(
                                                             keyColumns=["Unique Ref No",
                                                                         "Transaction Amount"],
                                                             sumColumns=[],
                                                             matchColumns=[
                                                                 "Unique Ref No",
                                                                 "Transaction Amount"]),
                                                         sourceTag="GI", subsetfilter=[
                                                            "df = df[df['duplicated'] == 'DUPLICATED']",
                                                            "df = df[df['TransactionType'] != 'NON_FIN']"]),
                                                    dict(source="fstresults.CBS_GI",
                                                         columns=dict(
                                                             keyColumns=["Unique Ref No",
                                                                         'Transaction Amount'],
                                                             sumColumns=[],
                                                             matchColumns=["Unique Ref No",
                                                                           'Transaction Amount']),
                                                         sourceTag="CBS_GI", subsetfilter=[
                                                            "df = df[df['duplicated'] == 'DUPLICATED']"])],
                                           matchResult="dupresults"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="dupresults.PostSettlement",
                                                    columns=dict(keyColumns=["Unique Ref No",
                                                                             "Transaction Amount"],
                                                                 sumColumns=[],
                                                                 matchColumns=["Unique Ref No",
                                                                               "Transaction Amount"]),
                                                    sourceTag="PostSettlement",
                                                    subsetfilter=["df = df[df['duplicated'] != 'DUPLICATED']",
                                                                  "df = df[df['Transaction Type'] != 'NON_FIN']"]),
                                               dict(source="dupresults.CBS",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No",
                                                                    "Amount(LCY)"],
                                                        sumColumns=[],
                                                        matchColumns=["Unique Ref No",
                                                                      "Amount(LCY)"]),
                                                    sourceTag="CBS",
                                                    subsetfilter=[
                                                        "df = df[df['duplicated'] != 'DUPLICATED']"]),
                                               dict(source="dupresults.GI",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No",
                                                                    "Transaction Amount"],
                                                        sumColumns=[],
                                                        matchColumns=[
                                                            "Unique Ref No",
                                                            "Transaction Amount"]),
                                                    sourceTag="GI",
                                                    subsetfilter=[
                                                        "df = df[df['duplicated'] != 'DUPLICATED']",
                                                        "df = df[df['TransactionType'] != 'NON_FIN']"]),
                                               dict(source="dupresults.CBS_GI",
                                                    columns=dict(
                                                        keyColumns=["Unique Ref No",
                                                                    'Transaction Amount'],
                                                        sumColumns=[],
                                                        matchColumns=["Unique Ref No",
                                                                      'Transaction Amount']),
                                                    sourceTag="CBS_GI", subsetfilter=[
                                                       "df = df[df['duplicated'] != 'DUPLICATED']"])],
                                      matchResult="results"))

    nonfin_match = dict(type="NWayMatch",
                        properties=dict(sources=[dict(source="results.PostSettlement",
                                                      columns=dict(keyColumns=["Unique Ref No",
                                                                               "Transaction Amount"],
                                                                   sumColumns=[],
                                                                   matchColumns=["Unique Ref No",
                                                                                 "Transaction Amount"]),
                                                      sourceTag="PostSettlement",
                                                      subsetfilter=["df = df[df['Transaction Type'] == 'NON_FIN']"]),
                                                 dict(source="results.GI",
                                                      columns=dict(
                                                          keyColumns=["Unique Ref No",
                                                                      "Transaction Amount"],
                                                          sumColumns=[],
                                                          matchColumns=[
                                                              "Unique Ref No",
                                                              "Transaction Amount"]),
                                                      sourceTag="GI",
                                                      subsetfilter=["df = df[df['TransactionType'] == 'NON_FIN']"])],
                                        matchResult="results"))

    postsett_nonfin_marker = dict(type="ExpressionEvaluator",
                                  properties=dict(source='results.PostSettlement', resultkey='results.PostSettlement',
                                                  expressions=[
                                                      "df.loc[(df['GI Match'] == 'MATCHED') & (df['Transaction Type'] == 'NON_FIN'), 'Recon Remarks'] = 'System Matched'"]))

    gi_nonfin_marker = dict(type="ExpressionEvaluator",
                            properties=dict(source='results.GI', resultkey='results.GI',
                                            expressions=[
                                                "df.loc[((df['PostSettlement Match'] == 'MATCHED') & (df['TransactionType'] == 'NON_FIN')), 'Recon Remarks'] = 'System Matched'"]))

    post_remarks = dict(type="AddPostMatchRemarks",
                        properties=dict(source='results.PostSettlement', remarkCol='Recon Remarks',
                                        remarks={"matchRemarks": "System Matched"}, how='all'))

    cbs_remarks = dict(type="AddPostMatchRemarks",
                       properties=dict(source='results.CBS', remarkCol='Recon Remarks',
                                       remarks={"matchRemarks": "System Matched"}, how='all'))

    gi_remarks = dict(type="AddPostMatchRemarks",
                      properties=dict(source='results.GI', remarkCol='Recon Remarks',
                                      remarks={"matchRemarks": "System Matched"}, how='all'))

    cbsgi_remarks = dict(type="AddPostMatchRemarks",
                         properties=dict(source='results.CBS_GI', remarkCol='Recon Remarks',
                                         remarks={"matchRemarks": "System Matched"}, how='all'))

    # add post settlement remarks for all the reports on PostSettlement unmatched records
    cbs_post_exception_remarks = dict(type="ExpressionEvaluator",
                                      properties=dict(source='results.CBS', resultkey='results.CBS',
                                                      expressions=[
                                                          "df.loc[(df['PostSettlement CarryForward'] == 'Y'), "
                                                          "'Recon Remarks'] = 'Yesterdays PostSettlement'"]))

    gi_post_exception_remarks = dict(type="ExpressionEvaluator",
                                     properties=dict(source='results.GI', resultkey='results.GI',
                                                     expressions=[
                                                         "df.loc[(df['PostSettlement CarryForward'] == 'Y'),"
                                                         "'Recon Remarks'] = 'Yesterdays PostSettlement'"]))

    cbsgi_post_exception_remarks = dict(type="ExpressionEvaluator",
                                        properties=dict(source='results.CBS_GI', resultkey='results.CBS_GI',
                                                        expressions=[
                                                            "df.loc[df['CBS Match'] == 'UNMATCHED','CBS_GI Exception'] = 'Y'"]))

    # generate CBS_GI vs CBS unmatched report
    cbsgi_exception_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='results.CBS_GI', filterConditions=["df = df[df['CBS Match'] == 'UNMATCHED']"],
                      sourceTag="CBS_GI")], writeToFile=True, reportName='CBS_GI Exception Report',
        writeToDB=True, collection='output_txn'))

    # Discard all exceptions from in CBS occured due to ALL_GL file
    allgl_expection_filter = dict(type="ExpressionEvaluator",
                                  properties=dict(source='results.CBS', resultkey='results.CBS',
                                                  expressions=[
                                                      "df = df[~((df['SOURCE_TYPE'] == 'ALL_GL') & \
                                                      (df['PostSettlement Match'] == 'UNMATCHED') & \
                                                      (df['GI Match'] == 'UNMATCHED') & \
                                                      (df['CBS_GI Match'] == 'UNMATCHED'))]"]))

    unregistered_tags = dict(type="ReportGenerator", properties=dict(
        sources=[
            dict(source='results.GI', filterConditions=["df = df[(df['MobileNo'].isna()) | (df['MobileNo'] == '')]",
                                                        "df = df[df['CBS_GI Match'] == 'UNMATCHED']",
                                                        "df = df[df['TransactionType'] == 'DEBIT']"],
                 sourceTag="GI")], writeToFile=True, reportName='Unregistered Tags', writeToDB=True,
        collection='output_txn'))

    ps_cf = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='results.PostSettlement',
                      filterConditions=["df['CARRY_FORWARD'] = df.get('CARRY_FORWARD','')",
                                        "df = df[df.get('CARRY_FORWARD') == 'Y']"],
                      sourceTag="PostSettlement")], writeToFile=True, reportName='PostSettlement_Carryforward'))

    dump_data = dict(type="DumpData",
                     properties=dict(dumpPath='Toll Transaction', matched='all'))

    elem9 = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [elem1, load_post_settlement, drop_ps_nonfin, elem3, drop_unreg_tags, load_cbs_gi, cbsgi_exp_filters,
             load_cbs_gl, load_cbs_allgl, cbs_cbsgi_vlookup, drop_cbsmissing_records, cbs_filters, cbs_incr_loader,
             filter_npci_dup, filter_cbs_dup, filter_cbsgidf_dup, filter_gi_dup, cbsgi_vlookup, first_match,duplicate_match,
             nway_match, nonfin_match, post_remarks, cbs_remarks, gi_remarks, cbsgi_remarks, postsett_nonfin_marker,
             gi_nonfin_marker, cbsgi_post_exception_remarks, cbsgi_exception_report, allgl_expection_filter,
             unregistered_tags, ps_cf, dump_data, elem9]

    FlowRunner(flows).run()
