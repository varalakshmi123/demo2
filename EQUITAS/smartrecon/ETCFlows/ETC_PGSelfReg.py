import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                recontype="ETC", compressionType="zip", resultkey='', reconName="PG Self Registration",
                                uploadFileName=uploadFileName, disableAllCarryFwd=True))

    tag_filters = ["df = df[df['Debit Account'].astype(str) == '114210142']"]

    load_selfreg_txn = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                              source="Self Registration",
                                                              feedPattern="Self Registration*.*xls",
                                                              feedParams={
                                                                  "feedformatFile": "Self_Registration_Structure(Tag_Rech).csv",
                                                                  "skiprows": 1, 'skipfooter': 1},
                                                              feedFilters=tag_filters,
                                                              resultkey="selfRegDf"))

    cbsPgFilter = ["df = df[df['Maker Id'] == 'GIUSER']",
                   "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1].strip())",
                   "df = df[df['Unique Ref No'].str.contains('ETCSR')]",
                   "stmt_date = datetime.strftime(payload['statementDate'], '%d-%m-%Y')",
                   "df['filter_date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')",
                   "df = df[df['filter_date'] == stmt_date]", "del df['filter_date']"]

    load_cbs_txn = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                          source="CBS_Tally",
                                                          feedPattern="CBS PG*.*xls",
                                                          feedParams={
                                                              "feedformatFile": "CBS_PG_Structure.csv",
                                                              "skiprows": 1},
                                                          feedFilters=cbsPgFilter,
                                                          resultkey="cbsPgDf"))

    pgFilters = ["df = df[(df['Ref. 1'].str.match('[0-9]')) | (df['Ref. 1'].str.startswith('TR'))]",
                 "df['Gross Amount(Rs.Ps)'] = df['Gross Amount(Rs.Ps)'].astype(np.float64)",
                 "df['Actual Transaction Amount'] = (df['Gross Amount(Rs.Ps)'] * 100) / 101.5"]

    load_settled_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG",
                                                              feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                              feedParams={"feedformatFile": "PG_Structure.csv",
                                                                          "skipfooter": 1, "sheetIndex": [0],
                                                                          "FooterKey": "REFUND TRANSACTIONS"},
                                                              feedFilters=pgFilters, resultkey="pgDf"))

    load_refund_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG_Refund",
                                                             feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                             feedParams={"feedformatFile": "PG_Refund_Structure.csv",
                                                                         "skipfooter": 1, "sheetIndex": [0],
                                                                         "FooterKey": "CHARGEBACK TRANSACTIONS"},
                                                             resultkey="pgRefundDf"))

    load_chargebk_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG_ChargeBack",
                                                               feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                               feedParams={"columnNames": "CHARGEBACK TRANSACTIONS",
                                                                           "feedformatFile": "PG_Chargeback_Structure.csv",
                                                                           "skipfooter": 1, "FooterKey": "Notes",
                                                                           "skiprows": 1, "sheetIndex": [0]},
                                                               resultkey="pgChargbkDf"))

    upd_pgref = dict(type='VLookup',
                     properties=dict(data='cbsPgDf', lookup='selfRegDf', dataFields=['Unique Ref No'],
                                     lookupFields=['Narration'], includeCols=['PG RefNo'], resultkey="cbsPgDf"))

    pg_tagrch_filter = dict(type="ExpressionEvaluator",
                            properties=dict(source='pgDf', resultkey='pgTagRchDf',
                                            expressions=["df = df[df['Ref. 1'].str.startswith('TR')]"]))

    pg_filter = dict(type="ExpressionEvaluator",
                     properties=dict(source='pgDf', resultkey='pgDf',
                                     expressions=["df = df[~df['Ref. 1'].str.startswith('TR')]"]))

    nway_match = dict(type="NWayMatch", properties=dict(sources=[dict(source="pgDf",
                                                                      columns=dict(
                                                                          keyColumns=['Ref. 1'],
                                                                          sumColumns=["Gross Amount(Rs.Ps)"],
                                                                          matchColumns=["Ref. 1"]),
                                                                      sourceTag="PG"),
                                                                 dict(source="selfRegDf",
                                                                      columns=dict(
                                                                          keyColumns=["PG RefNo"],
                                                                          sumColumns=["Total Amount"],
                                                                          matchColumns=["PG RefNo"]),
                                                                      sourceTag="Self Registration"),
                                                                 dict(source="cbsPgDf",
                                                                      columns=dict(
                                                                          keyColumns=["PG RefNo"],
                                                                          sumColumns=["Amount(LCY)"],
                                                                          matchColumns=["PG RefNo"]),
                                                                      sourceTag="CBS_Tally")],
                                                        matchResult="results"))

    tag_issuance_loader = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                                 source="Tag Replacement",
                                                                 feedPattern="Tag Issuance*.*xls",
                                                                 feedParams={
                                                                     "feedformatFile": "Tag_Issuance_CBS_Structure(Tag_Rech).csv",
                                                                     "skiprows": 1, 'skipfooter': 1},
                                                                 feedFilters=[
                                                                     "df = df[df['Debit Account'].astype(str) == '114210142']",
                                                                     "df = df[df['Trans Type'] == 'Tag Replacement']",
                                                                     "df = df[pd.notnull(df['PG Ref Number'])]"],
                                                                 resultkey="tagReplDf"))

    upd_tag_remarks = dict(type='VLookup',
                           properties=dict(data='tagReplDf', lookup='pgTagRchDf', dataFields=['TransactionID'],
                                           lookupFields=['Ref. 1'], markers={'PG Remarks': 'Found'},
                                           resultkey="tagReplDf"))

    gen_tag_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='tagReplDf', filterConditions=[], sourceTag="Tag Replacement")], writeToFile=True,
        reportName='Tag Replacement'))

    gen_refund_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgRefundDf', filterConditions=[], sourceTag="PG_Refund")], writeToFile=True,
        reportName='PG_Refund'))

    gen_chargebk_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgChargbkDf', filterConditions=[], sourceTag="PG_ChargeBack")], writeToFile=True,
        reportName='PG_ChargeBack'))

    gen_selftally_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='selfRegDf', filterConditions=["df = df[df['Credit Account'].astype(str) == '204020027']"],
                      sourceTag="Self_Tally")], writeToFile=True,
        reportName='Self_Tally'))

    gen_tally_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgDf',
                      filterConditions=["pg_tally = []",
                                        "txn_amount = df['Actual Transaction Amount'].fillna(0.0).sum()",
                                        "our_charges = (txn_amount * 1.5) / 100",
                                        'total_txn_amount = txn_amount + our_charges',
                                        "pg_charges = df['Charges (Rs.Ps)'].fillna(0.0).astype(np.float64).sum() + \
                                                      df['GST (Rs Ps)'].fillna(0.0).astype(np.float64).sum()",
                                        "neft_rec = total_txn_amount - pg_charges",
                                        "pg_tally.append({'Entry Type':'Actual Transaction Amount', \
                                                         'Amount': txn_amount})",
                                        "pg_tally.append({'Entry Type':'Our charges', 'Amount': our_charges})",
                                        "pg_tally.append({'Entry Type':'Total Txn amount', 'Amount': total_txn_amount})",
                                        "pg_tally.append({'Entry Type':'PG charges', 'Amount': pg_charges})",
                                        "pg_tally.append({'Entry Type':'NEFT to be received', \
                                                          'Amount': neft_rec})",
                                        "pg_tally.append({'Entry Type':'NEFT received', 'Amount': 0.0})",
                                        "pg_tally.append({'Entry Type':'Refund Transaction', 'Amount': neft_rec - 0.0})",
                                        "df = pd.DataFrame(pg_tally, columns = ['Entry Type', 'Amount'])"],
                      sourceTag="Tally")], writeToFile=True, reportName='Tally', postFilters=[]))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='PG Self Registration'), matched='any')

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [init, load_selfreg_txn, load_cbs_txn, load_settled_txn, load_refund_txn, load_chargebk_txn, upd_pgref,
             pg_tagrch_filter, pg_filter, nway_match, tag_issuance_loader, upd_tag_remarks, gen_tag_report,
             gen_chargebk_report, gen_refund_report, gen_selftally_report, gen_tally_report, dump_data, gen_meta_info]

    FlowRunner(flows).run()
