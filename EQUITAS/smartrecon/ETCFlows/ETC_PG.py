import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = 'Upload_files.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                recontype="ETC", compressionType="zip", resultkey='', reconName="ETC PG",
                                uploadFileName=uploadFileName, disableAllCarryFwd=True))

    tag_filters = ["df = df[df['Debit Account'].astype(str) == '114210142']",
                   "df['Unique Ref No'] = df['Narration'].str[5:]"]

    self_filters = ["df = df[df['Debit Account'].astype(str) == '114210142']",
                   "df['Unique Ref No'] = df['Narration'].str[5:]",
                   "print(df.columns)",
                   "df['PG RefNo'] = df['PG RefNo'].astype(str).str.replace('\.(0)*','')"]

    load_selfreg_txn = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                              source="Self Registration",
                                                              feedPattern="Self Registration*.*xls",
                                                              feedParams={
                                                                  "feedformatFile": "Self_Registration_Structure(Tag_Iss).csv",
                                                                  "skiprows": 1, 'skipfooter': 1},
                                                              feedFilters=self_filters, resultkey="selfRegDf"))

    load_tagrch_txn = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                             source="Tag Recharge",
                                                             feedPattern="Tag Recharge*.*xls",
                                                             feedParams={
                                                                 "feedformatFile": "Tag_Recharge_CBS_Structure.csv",
                                                                 "skiprows": 1},
                                                             feedFilters=tag_filters, resultkey="tagRecDf"))

    load_tagreplac_txn = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                                source="Tag Replacement",
                                                                feedPattern="Tag Issuance*.*xls",
                                                                feedParams={
                                                                    "feedformatFile": "Tag_Issuance_CBS_Structure(Tag_Iss).csv",
                                                                    "skiprows": 1, 'skipfooter': 1},
                                                                feedFilters=[
                                                                    "df = df[df['Debit Account'].astype(str) == '114210142']",
                                                                    "df = df[df['Trans Type'] == 'Tag Replacement']",
                                                                    "df = df[pd.notnull(df['PG Ref Number'])]",
                                                                    "df['Unique Ref No'] = df['Narration'].str[5:]"],
                                                                resultkey="tagReplDf"))

    cbsPgFilter = ["df = df[df['Maker Id'] == 'GIUSER']",
                   "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1].strip())",
                   "df['Unique Ref No'] = df['Unique Ref No'].str[5:]",
                   "stmt_date = datetime.strftime(payload['statementDate'], '%d-%m-%Y')",
                   "df['filter_date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')",
                   "df = df[df['filter_date'] == stmt_date]", "del df['filter_date']",
                   "df['Amount(LCY)'] = df['Amount(LCY)'].str.replace(',', '')",
                   "df['Amount(FCY)'] = df['Amount(FCY)'].str.replace(',', '')",
                   "df.drop(labels = 'Psuedo',inplace = True, errors = 'ignore')"]

    load_cbs_txn = dict(type="PreLoader", properties=dict(loadType="CSV",
                                                          source="CBS_Tally",
                                                          feedPattern="CBS PG*.*csv",
                                                          feedParams={
                                                              "feedformatFile": "CBS_PG_Structure.csv",
                                                              "skiprows": 1},
                                                          feedFilters=cbsPgFilter,
                                                          resultkey="cbsPgDf"))

    pgFilters = ["df['Gross Amount(Rs.Ps)'] = df['Gross Amount(Rs.Ps)'].astype(np.float64)",
                 "df['Actual Transaction Amount'] = (df['Gross Amount(Rs.Ps)'] * 100.00) / 101.5",
                 "df['filter_date'] = df['Date of Txn'].dt.strftime('%d-%m-%Y')",
                 "stmt_date = datetime.strftime(payload['statementDate'], '%d-%m-%Y')",
                 "df = df[df['filter_date'] == stmt_date]",
                 "df.drop(columns = 'filter_date',errors = 'ignore',inplace=True)"]

    load_settled_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG",
                                                              feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                              feedParams={"feedformatFile": "PG_Structure.csv",
                                                                          "skipfooter": 1, "sheetIndex": [0],
                                                                          "FooterKey": "REFUND TRANSACTIONS"},
                                                              feedFilters=pgFilters, resultkey="pgDf"))

    load_refund_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG_Refund",
                                                             feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                             feedParams={"feedformatFile": "PG_Refund_Structure.csv",
                                                                         "skipfooter": 1, "sheetIndex": [0],
                                                                         "FooterKey": "CHARGEBACK TRANSACTIONS"},
                                                             feedFilters=[],
                                                             resultkey="pgRefundDf"))

    load_chargebk_txn = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG_ChargeBack",
                                                               feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                               feedParams={"columnNames": "CHARGEBACK TRANSACTIONS",
                                                                           "feedformatFile": "PG_Chargeback_Structure.csv",
                                                                           "skipfooter": 1, "FooterKey": "Notes",
                                                                           "skiprows": 1, "sheetIndex": [0]},
                                                               feedFilters=[],
                                                               resultkey="pgChargbkDf"))

    upd_pgref = dict(type='VLookup',
                     properties=dict(data='cbsPgDf', lookup='selfRegDf', dataFields=['Unique Ref No'],
                                     lookupFields=['Unique Ref No'], includeCols=['PG RefNo'], resultkey="cbsPgDf"))

    upd_cbsref = dict(type='VLookup',
                      properties=dict(data='cbsPgDf', lookup='tagReplDf', dataFields=['Unique Ref No'],
                                      lookupFields=['Unique Ref No'], includeCols=['PG Ref Number'],
                                      resultkey="cbsPgDf"))

    nway_match1 = dict(type="NWayMatch", properties=dict(sources=[dict(source="pgDf",
                                                                       columns=dict(
                                                                           keyColumns=['Ref. 1'],
                                                                           sumColumns=["Gross Amount(Rs.Ps)"],
                                                                           matchColumns=["Ref. 1"]),
                                                                       sourceTag="PG"),
                                                                  dict(source="selfRegDf",
                                                                       columns=dict(
                                                                           keyColumns=["PG RefNo"],
                                                                           sumColumns=["Total Amount"],
                                                                           matchColumns=["PG RefNo"]),
                                                                       sourceTag="Self Registration"),
                                                                  dict(source="cbsPgDf",
                                                                       columns=dict(
                                                                           keyColumns=["PG RefNo"],
                                                                           sumColumns=["Amount(LCY)"],
                                                                           matchColumns=["PG RefNo"]),
                                                                       sourceTag="CBS_Tally")],
                                                         matchResult="results"))

    nway_match2 = dict(type="NWayMatch", properties=dict(sources=[dict(source="results.PG",
                                                                       columns=dict(
                                                                           keyColumns=['Ref. 1'],
                                                                           sumColumns=["Gross Amount(Rs.Ps)"],
                                                                           matchColumns=["Ref. 1"]),
                                                                       subsetfilter=[
                                                                           "df = df[df['Self Registration Match'] == 'UNMATCHED']"],
                                                                       sourceTag="PG"),
                                                                  dict(source="tagRecDf",
                                                                       columns=dict(
                                                                           keyColumns=["Unique Ref No"],
                                                                           sumColumns=["Total Amount"],
                                                                           matchColumns=["Unique Ref No"]),
                                                                       sourceTag="Tag Recharge"),
                                                                  dict(source="results.CBS_Tally",
                                                                       columns=dict(
                                                                           keyColumns=["Unique Ref No"],
                                                                           sumColumns=["Amount(LCY)"],
                                                                           matchColumns=["Unique Ref No"]),
                                                                       subsetfilter=[
                                                                           "df = df[df['Self Registration Match'] == 'UNMATCHED']"],
                                                                       sourceTag="CBS_Tally")],
                                                         matchResult="results"))

    nway_match3 = dict(type="NWayMatch", properties=dict(sources=[dict(source="results.PG",
                                                                       columns=dict(
                                                                           keyColumns=['PGI Ref. No.'],
                                                                           sumColumns=["Gross Amount(Rs.Ps)"],
                                                                           matchColumns=["PGI Ref. No."]),
                                                                       subsetfilter=[
                                                                           "df = df[df['Self Registration Match'] == 'UNMATCHED']",
                                                                           "df = df[df['Tag Recharge Match'] == 'UNMATCHED']"],
                                                                       sourceTag="PG"),
                                                                  dict(source="tagReplDf",
                                                                       columns=dict(
                                                                           keyColumns=["PG Ref Number"],
                                                                           sumColumns=["CBS Amount"],
                                                                           matchColumns=["PG Ref Number"]),
                                                                       sourceTag="Tag Replacement"),
                                                                  dict(source="results.CBS_Tally",
                                                                       columns=dict(
                                                                           keyColumns=["PG Ref Number"],
                                                                           sumColumns=["Amount(LCY)"],
                                                                           matchColumns=["PG Ref Number"]),
                                                                       subsetfilter=[
                                                                           "df = df[df['Self Registration Match'] == 'UNMATCHED']",
                                                                           "df = df[df['Tag Recharge Match'] == 'UNMATCHED']"],
                                                                       sourceTag="CBS_Tally")],
                                                         matchResult="results"))

    pg_tag_remarks = dict(type="ExpressionEvaluator",
                          properties=dict(source='results.PG', resultkey='results.PG',
                                          expressions=[
                                              "df.loc[df['Self Registration Match'] == 'MATCHED','Tag Remarks'] = 'Self Registration'",
                                              "df.loc[df['Tag Recharge Match'] == 'MATCHED','Tag Remarks'] = 'Tag Recharge'",
                                              "df.loc[df['Tag Replacement Match'] == 'MATCHED','Tag Remarks'] = 'Tag Replacement'",
                                              "df.loc[df['Tag Remarks'].isnull() ,'Tag Remarks'] = 'Not Found'"]))

    cbs_tag_remarks = dict(type="ExpressionEvaluator",
                           properties=dict(source='results.CBS_Tally', resultkey='results.CBS_Tally',
                                           expressions=[
                                               "df.loc[df['Self Registration Match'] == 'MATCHED','Tag Remarks'] = 'Self Registration'",
                                               "df.loc[df['Tag Recharge Match'] == 'MATCHED','Tag Remarks'] = 'Tag Recharge'",
                                               "df.loc[df['Tag Replacement Match'] == 'MATCHED','Tag Remarks'] = 'Tag Replacement'",
                                               "df.loc[df['Tag Remarks'].isnull() ,'Tag Remarks'] = 'Not Found'"]))

    gen_refund_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgRefundDf', filterConditions=[], sourceTag="PG_Refund")], writeToFile=True,
        reportName='PG_Refund'))

    gen_chargebk_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgChargbkDf', filterConditions=[], sourceTag="PG_ChargeBack")], writeToFile=True,
        reportName='PG_ChargeBack'))

    gen_selftally_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='selfRegDf', filterConditions=["df = df[df['Credit Account'].astype(str) == '204020027']"],
                      sourceTag="Self_Tally")], writeToFile=True,
        reportName='Self_Tally'))

    gen_tally_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgDf',
                      filterConditions=["pg_tally = []",
                                        "txn_amount = df['Actual Transaction Amount'].fillna(0.0).sum()",
                                        "our_charges = (txn_amount * 1.5) / 100.00",
                                        'total_txn_amount = txn_amount + our_charges',
                                        "pg_charges = df['Charges (Rs.Ps)'].fillna(0.0).astype(np.float64).sum() + \
                                                      df['GST (Rs Ps)'].fillna(0.0).astype(np.float64).sum()",
                                        "neft_rec = total_txn_amount - pg_charges",
                                        "pg_tally.append({'Entry Type':'Actual Transaction Amount', \
                                                         'Amount': txn_amount})",
                                        "pg_tally.append({'Entry Type':'Our charges', 'Amount': our_charges})",
                                        "pg_tally.append({'Entry Type':'Total Txn amount', 'Amount': total_txn_amount})",
                                        "pg_tally.append({'Entry Type':'PG charges', 'Amount': pg_charges})",
                                        "pg_tally.append({'Entry Type':'NEFT to be received', \
                                                          'Amount': neft_rec})",
                                        "pg_tally.append({'Entry Type':'NEFT received', 'Amount': 0.0})",
                                        "pg_tally.append({'Entry Type':'Refund Transaction', 'Amount': neft_rec - 0.0})",
                                        "df = pd.DataFrame(pg_tally, columns = ['Entry Type', 'Amount'])"],
                      sourceTag="Tally")], writeToFile=True, reportName='Tally', postFilters=[]))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='ETC PG', matched='any', roundTo=4))

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [init, load_selfreg_txn, load_tagrch_txn, load_tagreplac_txn, load_cbs_txn, load_settled_txn,
             load_refund_txn, load_chargebk_txn, upd_pgref, upd_cbsref, nway_match1, nway_match2, nway_match3,
             pg_tag_remarks, cbs_tag_remarks, gen_chargebk_report, gen_refund_report, gen_selftally_report,
             gen_tally_report, dump_data, gen_meta_info]

    FlowRunner(flows).run()
