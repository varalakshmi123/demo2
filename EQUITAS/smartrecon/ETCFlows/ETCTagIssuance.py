import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '15-Jun-2019'
    uploadFileName = '150619_TI.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                recontype="ETC", compressionType="zip", resultkey='', reconName="Tag Issuance",
                                uploadFileName=uploadFileName, disableAllCarryFwd=True))

    selfRegFilters = ["df['Sl No'] = df['Sl No'].fillna(0).astype(np.int64)",
                            "df['Security Fee'] = df['Security Fee'].fillna(0).astype(np.float64)",
                            "df['JoiningFee'] = df['JoiningFee'].fillna(0).astype(np.float64)",
                            "df['GST'] = df['GST'].fillna(0).astype(np.float64)",
                      "print(df.columns)",

                      "df['Total Amount'] = df['Total Amount'].fillna(0).astype(np.float64)",

                      "df['Transaction Date'] = pd.to_datetime(df['Transaction Date'],format = '%d/%m/%Y %H:%M:%S')",


        "df['Credit Account'] = df['Credit Account'].astype(str).str.replace('\.(0)*','')",
                      "df = df[df['Credit Account'] == '204020027']",
                      "df['Unique Ref No'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                      "df['Unique Ref No'] = df['Unique Ref No'].str.strip()",
                      "df.loc[df['Total Amount'] == 59,'Unique Ref No'] = df.loc[df['Total Amount'] == 59,'Unique Ref No'].str[:-2] + 'SD'"]

    self_reg_loader = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                             source="Self Registration",
                                                             feedPattern="Self Registration*.*xls",
                                                             feedParams={
                                                                 "feedformatFile": "Self_Registration_Structure(Tag_Iss).csv",
                                                                 "skiprows": 1, 'skipfooter': 1},
                                                             feedFilters=selfRegFilters, errors=False,
                                                             resultkey="selfRegDf"))

    tagIssFilters = ["df['Credit Account'] = df['Credit Account'].astype(str).str.replace('\.(0)*','')",
                     "df['Security Deposit'] = df['Security Deposit'].fillna(0).astype(np.float64)",
                     "df['Joining Fee'] = df['Joining Fee'].fillna(0).astype(np.float64)",
                     "df['GST'] = df['GST'].fillna(0).astype(np.float64)",
                     "df['CBS Amount'] = df['CBS Amount'].fillna(0).astype(np.float64)",
                     # "df['CBS RequestTime'] = pd.to_datetime(df['CBS RequestTime'],format = '%d/%m/%Y %H:%M:%S')",
                     # "df['Tag IssuedTime'] = pd.to_datetime(df['Tag IssuedTime'],format = '%d/%m/%Y %H:%M:%S')",
                     "df = df[df['Credit Account'] == '204020027']",
                     "df['Unique Ref No'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                     "df['Unique Ref No'] = df['Unique Ref No'].str.strip()",
                     "df.loc[df['CBS Amount'] == 59,'Unique Ref No'] = df.loc[df['CBS Amount'] == 59,'Unique Ref No'].str[:-2] + 'SD'"]

    tag_iss_loader = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                            source="Tag Issuance",
                                                            feedPattern="Tag Issuance - CBS*.*xls",
                                                            feedParams={
                                                                "feedformatFile": "Tag_Issuance_CBS_Structure(Tag_Iss).csv",
                                                                "skiprows": 1},
                                                            feedFilters=tagIssFilters,
                                                            resultkey="tagIssDf"))

    tagRchFilters = ["df['Credit Account'] = df['Credit Account'].astype(str)",
                     "df = df[df['Credit Account'] == '204020028']",
                     "df['Unique Ref No'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                     "df['Unique Ref No'] = df['Unique Ref No'].str.strip()"]

    cbs_gl_loader = dict(type="PreLoader", properties=dict(loadType="FixedHeaderCSV", source="CBS",
                                                           feedPattern="GL*.*csv",
                                                           feedParams={"feedformatFile": "ETC_GL_Structure.csv",
                                                                       "delimiter": ","}, resultkey="cbsdf"))

    cbs_allgl_loader = dict(type="PreLoader", properties=dict(loadType="FixedHeaderCSV", source="CBS",
                                                              feedPattern="allgl*.csv",
                                                              feedParams={"feedformatFile": "ETC_ALLGL_Structure.csv",
                                                                          "delimiter": ","}, resultkey="cbsdf"))

    cbsFilters = ["df['Tran_Date'] = df['Txn Date'].str.split(expand=True)[0]",
                  "df['Tran_Date'] = pd.to_datetime(df['Tran_Date'], format = '%d-%m-%Y')",
                  "df = df[df['Tran_Date'] == payload['statementDate']]",
                  "df = df[~df['Description'].str.contains('ETCCB')]",
                  "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1])",
                  "df['Unique Ref No'] = df['Unique Ref No'].str[5:]",
                  "df['Unique Ref No'] = df['Unique Ref No'].str.strip()",
                  "df['Unique Ref No'] = df['Unique Ref No'].str.lstrip('0')",
                  "df = df[df['Maker Id'] == 'GIUSER']",
                  "df = df[df['Account Number'].astype(str).str.strip().str[-9:] == '204020027']",
                  "df = df[df['Dr / Cr'] == 'C']"]

    cbs_filter_eval = dict(type="ExpressionEvaluator",
                           properties=dict(source='cbsdf', resultkey='cbsdf', expressions=cbsFilters))

    drop_selfreg_duplicates = dict(type="FilterDuplicates",
                                   properties=dict(source="selfRegDf", duplicateStore="duplicateStore",
                                                   allowOneInMatch=True,
                                                   keyColumns=["Unique Ref No"], sourceTag="Self Registration",
                                                   resultKey="selfRegDf_unq"))

    drop_tagiss_duplicates = dict(type="FilterDuplicates",
                                  properties=dict(source="tagIssDf", duplicateStore="duplicateStore",
                                                  allowOneInMatch=True,
                                                  keyColumns=["Unique Ref No"], sourceTag="Tag Issuance",
                                                  resultKey="tagIssDf_unq"))

    drop_cbs_duplicates = dict(type="FilterDuplicates",
                               properties=dict(source="cbsdf", duplicateStore="duplicateStore", allowOneInMatch=True,
                                               keyColumns=["Unique Ref No"], sourceTag="CBS", resultKey="cbsdf_unq"))

    nway_matcher = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf_unq",
                                                                        columns=dict(
                                                                            keyColumns=["Unique Ref No"],
                                                                            sumColumns=[],
                                                                            matchColumns=["Unique Ref No"]),
                                                                        sourceTag="CBS"),
                                                                   dict(source="selfRegDf_unq",
                                                                        columns=dict(
                                                                            keyColumns=["Unique Ref No"],
                                                                            sumColumns=[],
                                                                            matchColumns=["Unique Ref No"]),
                                                                        sourceTag="Self Registration"),
                                                                   dict(source="tagIssDf_unq",
                                                                        columns=dict(
                                                                            keyColumns=["Unique Ref No"],
                                                                            sumColumns=[],
                                                                            matchColumns=["Unique Ref No"]),
                                                                        sourceTag="Tag Issuance")],
                                                          matchResult="results"))

    cbs_remarks = dict(type="ExpressionEvaluator", properties=dict(source='results.CBS', resultkey='results.CBS',
                                                                   expressions=
                                                                   ["df['Remarks'] = 'Not Found'",
                                                                    "df.loc[df['Self Registration Match'] == 'MATCHED', 'Remarks'] = 'Self Registraion'",
                                                                    "df.loc[df['Tag Issuance Match'] == 'MATCHED', 'Remarks'] = 'Tag Issuance'"]))

    dropSelfCols = dict(type="ExpressionEvaluator",
                        properties=dict(source="results.Tag Issuance", resultkey="results.Tag Issuance",
                                        expressions=[
                                            "df = df.drop(['Self Registration Match'],axis = 1)"]))

    dropTagCols = dict(type="ExpressionEvaluator",
                       properties=dict(source="results.Self Registration", resultkey="results.Self Registration",
                                       expressions=[
                                           "df = df.drop(['Tag Issuance Match'],axis = 1)"]))

    report_filters = ["df.reset_index(drop=True, inplace = True)", "tmp_df = df[df['SOURCE'] != 'CBS']",
                      "df.loc[len(df), ['SOURCE','No of Transactions', 'Total Amount']] = ['Grand Total', tmp_df['No of Transactions'].sum(), tmp_df['Total Amount'].sum()]",
                      "tmp_df = df[df['SOURCE'] != 'Grand Total']",
                      "tmp_df.loc[tmp_df['SOURCE'] == 'CBS', ['Total Amount','No of Transactions']] *= -1",
                      "df.loc[len(df), ['SOURCE','No of Transactions', 'Total Amount']] = ['Difference', tmp_df['No of Transactions'].sum(), tmp_df['Total Amount'].sum()]",
                      "grand_total,cbs_total = df.iloc[-2].copy(), df.iloc[-3].copy()",
                      "df.iloc[-2] , df.iloc[-3] = cbs_total, grand_total",
                      "df['No of Transactions'] = df['No of Transactions'].astype(np.int64)"]

    report_elem = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='selfRegDf', filterConditions=["df['No of Transactions'] = 1",
                                                            "df['Total Amount'] = df['Total Amount'].fillna(0.0)",
                                                            "df = df.groupby('SOURCE')[['No of Transactions','Total Amount']].sum().reset_index()"],
                      sourceTag="Self Registration"),
                 dict(source='tagIssDf', filterConditions=["df['No of Transactions'] = 1",
                                                           "df['Total Amount'] = df['CBS Amount'].fillna(0.0)",
                                                           "df = df.groupby('SOURCE')[['No of Transactions','Total Amount']].sum().reset_index()"],
                      sourceTag="Tag Issuance"),
                 dict(source='cbsdf', filterConditions=["df['No of Transactions'] = 1",
                                                        "df['Total Amount'] = df['Amount(LCY)'].fillna(0.0)",
                                                        "df = df.groupby('SOURCE')[['No of Transactions','Total Amount']].sum().reset_index()"],
                      sourceTag="CBS")], writeToFile=True, reportName='Tally', postFilters=report_filters))

    tagiss_master = dict(type='ValidateMasterDump',
                         properties=dict(source='tagIssDf_unq', keyColumn=['Vehicle Number'],
                                         insertCols=['SOURCE', "TransactionID", 'Vehicle Number'],
                                         masterType='ETC_Tags', resultKey='duplicateTagIss',
                                         filters=["df = df[df['Trans Type'] != 'Tag Replacement']",
                                                  "df['Vehicle Number'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                                                  "df['Vehicle Number'] = df['Vehicle Number'].str.strip()"],
                                         dropOnRollback=True))

    selfreg_master = dict(type='ValidateMasterDump',
                          properties=dict(source='selfRegDf_unq', keyColumn=['Vehicle Number'], masterType='ETC_Tags',
                                          insertCols=['SOURCE', "TransactionID", 'Vehicle Number'],
                                          filters=["df['Vehicle Number'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                                                   "df['Vehicle Number'] = df['Vehicle Number'].str.strip()"],
                                          resultKey='duplicateSelfReg', dropOnRollback=True))

    concat_duplicates = dict(type='SourceConcat',
                             properties=dict(sourceList=['duplicateTagIss', 'duplicateSelfReg'],
                                             resultKey='duplicateTags'))

    gen_duplicate_report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='duplicateTags', sourceTag="Duplicated Tags",
                                                              filterConditions=[])], writeToFile=True,
                                                reportName='ReIssued Tags'))

    elem12 = dict(type="DumpData", properties=dict(dumpPath='Tag Issuance', matched='any'))

    elem13 = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [init, self_reg_loader, tag_iss_loader, cbs_gl_loader, cbs_allgl_loader, cbs_filter_eval,
             drop_selfreg_duplicates, drop_tagiss_duplicates, drop_cbs_duplicates, nway_matcher, cbs_remarks,
             dropSelfCols, dropTagCols, report_elem, tagiss_master, selfreg_master, concat_duplicates,
             gen_duplicate_report, elem12]

    FlowRunner(flows).run()
