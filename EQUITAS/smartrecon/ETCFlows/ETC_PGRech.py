import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                 uploadFileName=uploadFileName, disableAllCarryFwd=True,
                                 recontype="ETC", compressionType="zip", resultkey='', reconName="PG Tag Recharge"))

    tagRchFilters = ["df = df[df['Debit Account'].astype(str) == '114210142']",
                     "df['Narration'] = df['Narration'].str[5:]"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                   source="Tag Recharge",
                                                   feedPattern="Tag Recharge*.*xls",
                                                   feedParams={
                                                       "feedformatFile": "Tag_Recharge_CBS_Structure.csv",
                                                       "skiprows": 1},
                                                   feedFilters=tagRchFilters,
                                                   resultkey="tagRecDf"))

    pgFilters = ["df = df[df['Ref. 1'].str.startswith('PG')]",
                 "df['Gross Amount(Rs.Ps)'] = df['Gross Amount(Rs.Ps)'].astype(np.float64)",
                 "df['Actual Transaction Amount'] = (df['Gross Amount(Rs.Ps)'] * 100) / 101.5"]

    elem3 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="PG",
                                                   feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                   feedParams={"feedformatFile": "PG_Structure.csv",
                                                               "skipfooter": 1, "sheetIndex": [0],
                                                               "sheetNames": ['Transaction Records'],
                                                               "FooterKey": "REFUND TRANSACTIONS"},
                                                   feedFilters=pgFilters, resultkey="pgDf"))

    cbsPgFilter = ["df = df[df['Maker Id'] == 'GIUSER']",
                   "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1].strip())",
                   "df['Unique Ref No'] = df['Unique Ref No'].str[5:]",
                   "df = df[df['Unique Ref No'].str.startswith('PG')]",
                   "stmt_date = datetime.strftime(payload['statementDate'], '%d-%m-%Y')",
                   "df['filter_date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')",
                   "df = df[df['filter_date'] == stmt_date]", "del df['filter_date']"]

    elem4 = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                   source="CBS PG",
                                                   feedPattern="CBS PG*.*xls",
                                                   feedParams={
                                                       "feedformatFile": "CBS_PG_Structure.csv",
                                                       "skiprows": 1},
                                                   feedFilters=cbsPgFilter,
                                                   resultkey="cbsPgDf"))

    elem5 = dict(type="NWayMatch", properties=dict(sources=[dict(source="pgDf",
                                                                 columns=dict(
                                                                     keyColumns=['Ref. 1'],
                                                                     sumColumns=["Gross Amount(Rs.Ps)"],
                                                                     matchColumns=["Ref. 1"]),
                                                                 sourceTag="PG"),
                                                            dict(source="tagRecDf",
                                                                 columns=dict(
                                                                     keyColumns=["Narration"],
                                                                     sumColumns=["Total Amount"],
                                                                     matchColumns=["Narration"]),
                                                                 sourceTag="Tag Recharge"),
                                                            dict(source="cbsPgDf",
                                                                 columns=dict(
                                                                     keyColumns=["Unique Ref No"],
                                                                     sumColumns=["Amount(LCY)"],
                                                                     matchColumns=["Unique Ref No"]),
                                                                 sourceTag="CBS PG")],
                                                   matchResult="results"))

    gen_tally_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgDf',
                      filterConditions=["pg_tally = []",
                                        "txn_amount = df['Actual Transaction Amount'].fillna(0.0).sum()",
                                        "our_charges = (txn_amount * 1.5) / 100",
                                        'total_txn_amount = txn_amount + our_charges',
                                        "pg_charges = df['Charges (Rs.Ps)'].fillna(0.0).astype(np.float64).sum() + \
                                                      df['GST (Rs Ps)'].fillna(0.0).astype(np.float64).sum()",
                                        "neft_rec = total_txn_amount - pg_charges",
                                        "pg_tally.append({'Entry Type':'Actual Transaction Amount', \
                                                         'Amount': txn_amount})",
                                        "pg_tally.append({'Entry Type':'Our charges', 'Amount': our_charges})",
                                        "pg_tally.append({'Entry Type':'Total Txn amount', 'Amount': total_txn_amount})",
                                        "pg_tally.append({'Entry Type':'PG charges', 'Amount': pg_charges})",
                                        "pg_tally.append({'Entry Type':'NEFT to be received', \
                                                          'Amount': neft_rec})",
                                        "pg_tally.append({'Entry Type':'NEFT received', 'Amount': 0.0})",
                                        "pg_tally.append({'Entry Type':'Refund Transaction', 'Amount': neft_rec - 0.0})",
                                        "df = pd.DataFrame(pg_tally, columns = ['Entry Type', 'Amount'])"],
                      sourceTag="Tally")], writeToFile=True, reportName='Tally', postFilters=[]))

    elem6 = dict(id=5, type="DumpData", properties=dict(dumpPath='PG Tag Recharge'))

    elem8 = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [elem1, elem2, elem3, elem4, elem5, gen_tally_report, elem6, elem8]
    FlowRunner(flows).run()
