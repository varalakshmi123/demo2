import sys

sys.path.append("/usr/share/nginx/smartrecon/ETCFlows/")
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '04-Jan-2018'
    uploadFileName = 'cbs_pg.zip'

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                 uploadFileName=uploadFileName, recontype="ETC", compressionType="zip", resultkey='',
                                 reconName="CBS PG vs PG"))

    cbsPgFilter = ["df = df[df['Maker Id'] == 'GIUSER']",
                   "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1].strip())",
                   "df['Unique Ref No'] = df['Unique Ref No'].str[5:]",
                   "df = df[df['Unique Ref No'].str.startswith('PG')]"]

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="Excel",
                                                                 source="CBS PG",
                                                                 feedPattern="CBS PG*.*xls",
                                                                 feedParams={
                                                                     "feedformatFile": "CBS_PG_Structure.csv",
                                                                     "skiprows": 1},
                                                                 feedFilters=cbsPgFilter,
                                                                 resultkey="cbsPgDf"))

    pgFilters = ["index = df[df['Biller Id'] == 'REFUND TRANSACTIONS'].index",
                 "df = df[:index.values[0]]",
                 "df = df.dropna(subset=['Biller Id'])",
                 "df = df[df['Ref 1'].str.startswith('PG')]",
                 "df['Total Amount'] = df['Total Amount'].astype(np.float64)"]

    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="Excel",
                                                                 source="PG Equitas",
                                                                 feedPattern="PV_EQUITASETC_OPCIT*.*xls",
                                                                 feedParams={
                                                                     "feedformatFile": "PG_Structure.csv",
                                                                     "skiprows": 3},
                                                                 feedFilters=pgFilters,
                                                                 resultkey="pgDf"))

    elem4 = dict(id=4, next=5, type="NWayMatch", properties=dict(sources=[dict(source="pgDf",
                                                                               columns=dict(
                                                                                   keyColumns=['Ref 1'],
                                                                                   sumColumns=["Total Amount"],
                                                                                   matchColumns=["Ref 1"]),
                                                                               sourceTag="PG"),
                                                                          dict(source="cbsPgDf",
                                                                               columns=dict(
                                                                                   keyColumns=["Unique Ref No"],
                                                                                   sumColumns=["Amount(LCY)"],
                                                                                   matchColumns=["Unique Ref No"]),
                                                                               sourceTag="CBS PG")],
                                                                 matchResult="results"))

    elem5 = dict(id=5, type="DumpData", properties=dict(dumpPath='CBS PG vs PG'))

    flows = [elem1, elem2, elem3, elem4, elem5]
    FlowRunner(flows).run()
