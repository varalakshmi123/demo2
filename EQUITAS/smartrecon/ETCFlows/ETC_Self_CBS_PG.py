import sys

sys.path.append('..')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '04-Jan-2018'
    uploadFileName = 'self_cbs.zip'

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                 recontype="ETC", compressionType="zip", resultkey='', reconName="Self Reg vs CBS PG",
                                 uploadFileName=uploadFileName))

    selfRegFilters = ["df = df[df['Debit Account'].astype(str) == '114210142']"]

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="Excel",
                                                                 source="Self Registration",
                                                                 feedPattern="Self Registration*.*xls",
                                                                 feedParams={
                                                                     "feedformatFile": "Self_Registration_Structure.csv",
                                                                     "skiprows": 1, 'skipfooter': 1},
                                                                 feedFilters=selfRegFilters,
                                                                 resultkey="selfRegDf"))

    cbsPgFilter = ["df = df[df['Maker Id'] == 'GIUSER']",
                   "df['Unique Ref No'] = df['Description'].apply(lambda x: x.split(':')[-1].strip())",
                   "df = df[df['Unique Ref No'].str.contains('ETCSR')]"]

    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="Excel",
                                                                 source="CBS PG",
                                                                 feedPattern="CBS*.*xls",
                                                                 feedParams={
                                                                     "feedformatFile": "CBS_PG_Structure.csv",
                                                                     "skiprows": 1},
                                                                 feedFilters=cbsPgFilter,
                                                                 resultkey="cbsPgDf"))

    elem4 = dict(id=4, next=5, type="NWayMatch", properties=dict(sources=[dict(source="selfRegDf",
                                                                               columns=dict(
                                                                                   keyColumns=['Narration'],
                                                                                   sumColumns=["Total Amount"],
                                                                                   matchColumns=["Narration"]),
                                                                               sourceTag="Self"),
                                                                          dict(source="cbsPgDf",
                                                                               columns=dict(
                                                                                   keyColumns=["Unique Ref No"],
                                                                                   sumColumns=["Amount(LCY)"],
                                                                                   matchColumns=["Unique Ref No"]),
                                                                               sourceTag="CBS PG")],
                                                                 matchResult="results"))

    elem5 = dict(id=5, type="DumpData", properties=dict(dumpPath='Self Reg vs CBS PG'))

    flows = [elem1, elem2, elem3, elem4, elem5]
    FlowRunner(flows).run()
