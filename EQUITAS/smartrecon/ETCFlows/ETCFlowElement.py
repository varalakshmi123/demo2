import datetime
import inspect
import os
import sys
import traceback
import uuid
import zipfile

import pandas as pd

import Matcher
import prerecon.FeedLoader as FeedLoader
import prerecon.FilterExecutor as FilterExecutor
import prerecon.FindFiles as FindFiles
import config
import logger
import numpy as np
from pymongo import MongoClient
import errno
from prerecon.CustomFunctionLoader import CustomFunctionLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class FlowElement:
    def __init__(self):
        self.type = None
        self.name = None
        self.properties = {}
        self.id = uuid.uuid4().hex

    def run(self, payload, debug):
        pass


class FlowRunner:
    def __init__(self, flow, debug=False):
        self.name = None
        self.elements = flow
        self.lookup = {k['id']: k for k in self.elements}
        self.classMap = dict()
        self.debug = debug
        classes = self.FindAllSubclasses(FlowElement)
        for x in classes:
            self.classMap[x[1]] = x[0]

    def FindAllSubclasses(self, classType):
        subclasses = []
        callers_module = sys._getframe(1).f_globals['__name__']
        classes = inspect.getmembers(sys.modules[callers_module], inspect.isclass)
        for name, obj in classes:
            if (obj is not classType) and (classType in inspect.getmro(obj)):
                subclasses.append((obj, name))
        return subclasses

    def run(self, payload=None):
        if payload is None:
            payload = dict()
        if len(self.elements) > 0:
            fe = self.elements[0]
            next = [fe]
            while next and len(next) > 0:
                temp = []
                for x in next:
                    klass = self.classMap[x["type"]]
                    instance = klass()
                    instance.properties = x.get("properties", {})
                    instance.nodeId = x.get("nodeId", "")
                    instance.run(payload, self.debug)
                    if 'error' in payload.keys():
                        break
                    elif "next" in x:
                        temp.append(self.lookup[x["next"]])
                    else:
                        pass
                next = temp
            self.dumpJobStatus(payload)

    def dumpJobStatus(self, payload):
        if 'error' in payload:
            for x in payload['error']['traceback'].split('/n'):
                print x

                # print payload['error']

                # payload['outwardMatch'].to_csv("/home/ubuntu/Desktop/Equitas_Data/IMPS/Output/outwardMatch.csv",
                #                                index=False)
                # payload['cbsexp'].to_csv("/home/ubuntu/Desktop/Equitas_Data/IMPS/Output/cbsexp.csv",
                #                          index=False)
                # payload['npciexp'].to_csv("/home/ubuntu/Desktop/Equitas_Data/IMPS/Output/npciexp.csv",
                #                           index=False)
                # payload['cbsTimeoutDf'].to_csv("/home/ubuntu/Desktop/Equitas_Data/IMPS/Output/cbsTimeoutDf.csv", index=False)
                # payload['cbsReversalDf'].to_csv("/home/ubuntu/Desktop/Equitas_Data/IMPS/Output/cbsReversal.csv",
                #                                 index=False)


class Initializer(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Initializer"
        self.name = "Extracter"
        self.properties = dict(statementDate="", outputPath="", recontype="", compressionType="", moveToProcessed='')

    def run(self, payload, debug):
        try:
            stmtDate = datetime.datetime.strptime(self.properties.get("statementDate", ""), '%d-%b-%Y')
            stmtDateFormat = stmtDate.strftime('%d%m%Y')
            mftDir = config.basepath + os.sep + 'mft'
            compressionType = self.properties.get('compressionType', '') or ''
            extract_path = os.path.join(mftDir, self.properties.get('recontype', ''))
            source_path = os.path.join(mftDir, *[self.properties.get('recontype', ''), stmtDateFormat])
            if not os.path.exists(extract_path):
                payload['error'] = dict(elementName="Initializer", type="critical",
                                        exception="Source path not available:%s" % extract_path)
                return
            output_path = self.properties.get('outputPath', '') or os.path.join(extract_path, 'OUTPUT')
            if os.path.isfile(source_path + '.%s' % compressionType):
                extractFile = zipfile.ZipFile(source_path + '.%s' % compressionType)
                extractFile.extractall(extract_path)

            if not os.path.exists(output_path):
                os.mkdir(output_path)
            payload['statementDate'] = stmtDate
            payload['source_path'] = source_path
            payload['output_path'] = output_path
            payload['moveToProcessed'] = self.properties.get('moveToProcessed', False)
        except Exception as e:
            logger.info(traceback.format_exc())
            payload['error'] = dict(elementName="Initializer", type="critical", exception=str(e))


class PreLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DataLoader"
        self.name = "PreLoader"
        self.properties = dict(loadType="", source="", feedPattern="", feedParams=dict(), feedFilters=[],
                               resultkey="dataframe")

    def run(self, payload, debug):
        try:
            feedfac = FeedLoader.FeedLoaderFactory()
            stmtDate = payload['statementDate']
            findFiles = FindFiles.FindFiles(stmtDate)
            inst = feedfac.getInstance(self.properties.get('loadType', ''))
            inst_files = findFiles.findFiles(feedPath=payload.get('source_path', ''),
                                             fpattern=self.properties.get('feedPattern', ''))

            df = inst.loadFile(inst_files, self.properties.get('feedParams', dict()))
            df['SOURCE'] = self.properties.get('source', '')

            if self.properties.get('feedFilters', []):
                for expression in self.properties.get('feedFilters', []):
                    exec expression
            payload[self.properties["resultkey"]] = df
        except Exception as e:
            logger.info(traceback.format_exc())
            payload['error'] = dict(elementName="PreLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class PerformMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Match"
        self.name = "PerformMatch"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(leftData=None, rightData=None, left=col, right=col, leftUnmatch=None,
                               rightUnmatch=None, matchResult=None, leftSrc='', rightSrc='')

    def run(self, payload, debug):
        try:
            leftdata = payload[self.properties['leftData']]
            rightdata = payload[self.properties['rightData']]

            leftcolumns = self.properties.get('left', {})
            rightcolumns = self.properties.get('right', {})
            left, right = Matcher.Matcher().match(leftdata, rightdata,
                                                  leftkeyColumns=leftcolumns.get('keyColumns', []),
                                                  rightkeycolumns=rightcolumns.get('keyColumns', []),
                                                  leftsumColumns=leftcolumns.get('sumColumns', []),
                                                  rightsumColumns=rightcolumns.get('sumColumns', []),
                                                  leftmatchColumns=leftcolumns.get('matchColumns', []),
                                                  rightmatchColumns=rightcolumns.get('matchColumns', []))

            payload[self.properties['leftUnmatch']] = left[left['Matched'] == 'UNMATCHED']

            # if left & right source are same (incase of reversal) append by checking leftUnmatch key & rightUnmatch key
            # to avoid data over-lapping.
            if self.properties['rightUnmatch'] == self.properties['leftUnmatch']:
                payload[self.properties['rightUnmatch']] = payload[self.properties['rightUnmatch']].append(
                    right[right['Matched'] == 'UNMATCHED'], ignore_index=True)
            else:
                payload[self.properties['rightUnmatch']] = right[right['Matched'] == 'UNMATCHED']

            leftMatch = left[left['Matched'] == 'MATCHED']
            rightMatch = right[right['Matched'] == 'MATCHED']
            totalMatch = leftMatch.merge(rightMatch, left_on=leftcolumns.get('matchColumns', []),
                                         right_on=rightcolumns.get('matchColumns', []),
                                         how='inner', suffixes=('_' + self.properties.get('leftSrc', 'x'),
                                                                '_' + self.properties.get('rightSrc', 'y')))
            totalMatch.dropna(axis=1, how='all', inplace=True)

            if self.properties['matchResult'] in payload.keys():
                payload[self.properties['matchResult']] = payload[self.properties['matchResult']].append(totalMatch,
                                                                                                         ignore_index=True)
            else:
                payload[self.properties['matchResult']] = totalMatch

        except Exception as e:
            logger.info(traceback.format_exc())
            payload['error'] = dict(elementName="PerformMatch", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ExpressionEvaluator(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DataLoader"
        self.name = "ExpressionEvaluator"
        self.properties = dict(expressions=[], source="dataframe", resultkey="dataframe")

    def run(self, payload, debug):
        try:
            df = payload[self.properties['source']]
            for expression in self.properties.get('expressions', []):
                exec expression

            payload[self.properties["resultkey"]] = df
        except Exception as e:
            logger.info(traceback.format_exc())
            payload['error'] = dict(elementName="PreLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class NPCIReportsLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DataLoader"
        self.name = "NPCIReportsLoader"
        self.properties = dict()

    def run(self, payload, debug):
        try:
            findFiles = FindFiles.FindFiles(payload['statementDate'])
            timeOutReports = findFiles.findFiles(feedPath=payload.get('source_path', ''),
                                                 fpattern=self.properties.get('timeOutReport', ''),
                                                 errors='ignore')

            npciTimeOutCols = ['TXN UID', 'TXN Type', 'TXN Date', 'TXN Time', 'Settlement Date',
                               'Response Code', 'Ref Number', 'STAN', 'Remitter', 'Beneficiary',
                               'Beneficiary Mobile /Account/Aadhar Number', 'Remitter Number',
                               'Column1']
            npciTimeOutReport = pd.DataFrame()
            if len(timeOutReports) > 0:
                for report in timeOutReports:
                    tmp = pd.read_html(report, skiprows=1)
                    if tmp:
                        npciTimeOutReport = npciTimeOutReport.append(tmp[-1], ignore_index=True)
                    else:
                        pass
                npciTimeOutReport.columns = npciTimeOutCols
                npciTimeOutReport.loc[:, 'Ref Number'] = npciTimeOutReport.loc[:, 'Ref Number'].astype(str).str.lstrip(
                    "'")
            else:
                print 'Could not find any time reports,'
            payload["npciTimeOutReport"] = npciTimeOutReport

        except Exception as e:
            payload['error'] = dict(elementName="NPCIReportsLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class DumpToFile(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DumpToFile"
        self.name = "DumpDataToFile"
        self.properties = dict(reconName='')

    def run(self, payload, debug):
        try:
            rdbClient = MongoClient('localhost')['erecon']['recon_details']
            reconConf = rdbClient.find_one({"reconName": self.properties['reconName']})

            if reconConf is None:
                raise ValueError("Could not find recon configuration for recon %s" % self.properties['reconName'])
            else:
                outputPath = config.basepath + '/' + 'mft' + '/' + reconConf['sourcePath'] + '/' + payload[
                    'statementDate'].strftime('%d%m%Y')
                for report in reconConf.get('reportDetails', []):
                    # Create directory before writing file.
                    filename = outputPath + '/' + report['reportPath']
                    if not os.path.exists(os.path.dirname(filename)):
                        try:
                            os.makedirs(os.path.dirname(filename))
                        except OSError as exc:  # Guard against race condition
                            if exc.errno != errno.EEXIST:
                                raise

                    payload[report['payLoadKey']].to_csv(filename, index=False)
        except Exception as e:
            payload['error'] = dict(elementName="DumpToFile", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class CustomFunctionExec(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ExecuteCustomFun"
        self.name = "CustomFunctionExec"
        self.properties = dict(reconName='')

    def run(self, payload, debug):
        try:
            result = getattr(CustomFunctionLoader(), self.properties['customFun'])(payload)
            payload[self.properties['resultkey']] = result
        except Exception as e:
            payload['error'] = dict(elementName="CustomFunctionExec", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


# Validate Report Post Match
class CSVTestWriter(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DumpData"
        self.name = "DumpDataToFile"
        self.properties = dict()

    def run(self, payload, debug):
        try:
            pass
        except Exception as e:
            payload['error'] = dict(elementName="DumpData", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


if __name__ == "__main__":
    stmtdate = "21-Dec-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"
    reconType = 'ETC'

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="ETC", compressionType="zip", resultkey=''))
    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="NPCI",
                                                                 feedPattern="841ESFB756*.*csv",
                                                                 feedParams=dict(delimiter=",", skiprows=1,
                                                                                 feedFileStructure='NPCI_POST_Settlement_Structure.csv'),
                                                                 feedFilters=["df = df[df['Transaction Amount'] > 0]",
                                                                              "df['TransactionNo'] = df['TransactionNo'].str.lstrip(\"'\")",
                                                                              "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
                                                                              "df['Transaction Amount'] = df['Transaction Amount'].astype(np.float64) / 100.00"],
                                                                 resultkey="npcidf"))

    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="Excel", source="ETC Txn Report",
                                                                 feedPattern="ETC Transaction Report*.*xls",
                                                                 feedParams=dict(
                                                                     feedFileStructure="ETC_TXN_Report_Structure.csv",
                                                                     skiprows=1, skipfooter=1),
                                                                 feedFilters=[
                                                                     "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
                                                                     "df = df[df['TransactionType'] != 'NON_FIN']"],
                                                                 resultkey="etcTxndf"))

    elem4 = dict(id=4, next=5, type="CustomFunctionExec",
                 properties=dict(customFun="loadCombinedGL", resultkey="cbsgldf"))

    elem5 = dict(id=5, next=6,
                 type="ExpressionEvaluator",
                 properties=dict(source="cbsgldf", resultkey="npciSuccdf",
                                 expressions=["df = df[df['User ID (maker)'] == 'GIUSER']",
                                              "df = df[df['User ID (Checker)'] == 'SYSTELLER']",
                                              "df['Unique Ref No'] = df['Description'].apply(lambda x : x.split(':')[-1].strip())",
                                              "df['trn_date'] = pd.to_datetime(df['Transaction Initiation Date'], format = '%d-%m-%Y %H:%M:%S',errors = 'ignore')",
                                              "df['Account Number'] = df['Account Number'].astype(str)"]))

    elem6 = dict(id=6, next=7, type="PerformMatch", properties=dict(leftData="npcidf", rightData="etcTxndf",
                                                                    left=dict(keyColumns=["TransactionNo",
                                                                                          'Transaction Amount'],
                                                                              matchColumns=["TransactionNo",
                                                                                            'Transaction Amount']),
                                                                    right=dict(keyColumns=["TransactionNo",
                                                                                           'Transaction Amount'],
                                                                               matchColumns=["TransactionNo",
                                                                                             'Transaction Amount']),
                                                                    leftUnmatch='npciexp',
                                                                    rightUnmatch='giexp',
                                                                    matchResult='giNpciMatch',
                                                                    leftSrc="NPCI",
                                                                    rightSrc="ETC"))

    elem7 = dict(id=7, type="DumpToFile", properties=dict(reconName='GI vs NPCI'))

    elements = [elem1, elem2, elem3, elem4, elem5, elem6, elem7]
    f = FlowRunner(elements)
    f.run()
