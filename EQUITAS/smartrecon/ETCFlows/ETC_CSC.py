import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = '14-Feb-2019'
    uploadFileName = '140219_CSC.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath="/usr/share/nginx/smartrecon/mft", outputPath="",
                                uploadFileName=uploadFileName, recontype="ETC", compressionType="zip", resultkey='',
                                reconName="CSC Recon"))

    mis_data_loader = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                             source="CSC MIS",
                                                             feedPattern="MIS_46909_Equitas_*.*",
                                                             feedParams={
                                                                 "feedformatFile": "CSC_MIS_Structure.csv",
                                                                 "skiprows": 1},
                                                             feedFilters=[
                                                                 "df = df[df['txn_date'] == payload['statementDate']]"],
                                                             resultkey="cscmidf", disableCarryFwd=True))

    csc_sett_data_loader = dict(type="PreLoader", properties=dict(loadType="Excel",
                                                                  source="CSC Settlement",
                                                                  feedPattern="Settlement Report*.*xls",
                                                                  feedParams={
                                                                      "feedformatFile": "CSC_Settlement_Report.csv",
                                                                      "skiprows": 1, 'skipfooter': 1},
                                                                  feedFilters=[
                                                                      "df['Txn amount'] = df['Txn amount'].str.replace(',' , '').astype(np.float64)",
                                                                      "df['Status'] = df['Status'].fillna('0').astype(str)",
                                                                      "df = df[df['Status'] == '100']"],
                                                                  resultkey="cscsett", disableCarryFwd=True))

    purchase_data_loader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Purchase",
                                                                  feedPattern="Purchase Details*.*xls",
                                                                  feedParams={"feedformatFile": "CSC_Purchase.csv",
                                                                              "skiprows": 1},
                                                                  feedFilters=[
                                                                      "df = df[df['Payment Status'] == 'Success']"],
                                                                  resultkey="purchasedf", disableCarryFwd=True,
                                                                  errors=False))

    purchase_report = dict(type="ReportGenerator",
                           properties=dict(sources=[dict(source='purchasedf', sourceTag="Purchase")], writeToFile=True,
                                           reportName='Purchase'))

    issuance_data_loader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Tag Issuance",
                                                                  feedPattern="Tag Issuance*.*xls",
                                                                  feedParams={"feedformatFile": "CSC_TagIssuance.csv",
                                                                              "skiprows": 1},
                                                                  feedFilters=[
                                                                      "df = df[df['Payment Status'] == 'Success']"],
                                                                  resultkey="issuancedf", disableCarryFwd=True,
                                                                  errors=False))

    issuance_report = dict(type="ReportGenerator",
                           properties=dict(sources=[dict(source='issuancedf', sourceTag="Tag Issuance")],
                                           writeToFile=True,
                                           reportName='Tag Issuance'))

    rech_data_loader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Recharge",
                                                              feedPattern="Recharge Details*.*xls",
                                                              feedParams={"feedformatFile": "CSC_Recharge.csv",
                                                                          "skiprows": 1, 'skipfooter': 1},
                                                              feedFilters=[
                                                                  "df = df[df['Payment Status'] == 'Success']",
                                                                  "df['Amount'] = df['Amount'].fillna('').str.replace(',', '')",
                                                                  "df['Amount'] = df['Amount'].astype(np.float64)"],
                                                              resultkey="rechargedf", disableCarryFwd=True,
                                                              errors=False))

    recharge_report = dict(type="ReportGenerator",
                           properties=dict(sources=[dict(source='rechargedf', sourceTag="Recharge")], writeToFile=True,
                                           reportName='Recharge'))

    matcher = dict(id=4, next=5, type="NWayMatch", properties=dict(sources=[dict(source="cscmidf",
                                                                                 columns=dict(
                                                                                     keyColumns=["csc_txn",
                                                                                                 "Txn amount"],
                                                                                     sumColumns=[],
                                                                                     matchColumns=["csc_txn",
                                                                                                   "Txn amount"]),
                                                                                 sourceTag="CSC MIS"),
                                                                            dict(source="cscsett",
                                                                                 columns=dict(
                                                                                     keyColumns=["CSC txn",
                                                                                                 "Txn amount"],
                                                                                     sumColumns=[],
                                                                                     matchColumns=["CSC txn",
                                                                                                   "Txn amount"]),
                                                                                 sourceTag="CSC Settlement")],
                                                                   matchResult="results"))

    report_filters = ["df = df.drop_duplicates(subset = df.columns).reset_index()",
                      "tmp_df = df[df['SOURCE'] != 'CSC Total']",
                      "df.loc[len(df), ['SOURCE','No of Transactions', 'Total Amount']] = ['Grand Total', tmp_df['No of Transactions'].sum(), tmp_df['Total Amount'].sum()]",
                      "tmp_df = df[df['SOURCE'] != 'Grand Total']",
                      "tmp_df.loc[tmp_df['SOURCE'] == 'CSC Total', ['Total Amount','No of Transactions']] *= -1",
                      "df.loc[len(df), ['SOURCE','No of Transactions', 'Total Amount']] = ['Difference', tmp_df['No of Transactions'].sum(), tmp_df['Total Amount'].sum()]",
                      "df.drop(labels = 'index',axis = 'columns',inplace = True,errors = 'ignore')",
                      "grandTotal,cbs_total = df.iloc[len(df) - 2].copy(), df.iloc[len(df) - 3].copy()",
                      "df.iloc[len(df) - 2] , df.iloc[len(df) - 3] = cbs_total, grandTotal"]

    tally_report_gen = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='purchasedf', filterConditions=["df['No of Transactions'] = len(df)",
                                                             "df['Total Amount'] = df['Joining Fee'].fillna(0.0).sum()",
                                                             "df = df[['SOURCE','No of Transactions', 'Total Amount']]"],
                      sourceTag="Purchase"),
                 dict(source='issuancedf', filterConditions=["df['No of Transactions'] = len(df)",
                                                             "df['Total Amount'] = df[['Security Deposit','Threshold Limit']].fillna(0.0).sum(axis = 1).sum()",
                                                             "df = df[[ 'SOURCE','No of Transactions', 'Total Amount']]"],
                      sourceTag="Tag Issuance"),
                 dict(source='rechargedf', filterConditions=["df['No of Transactions'] = len(df)",
                                                             "df['Total Amount'] = df['Amount'].sum()",
                                                             "df['SOURCE'] = 'Recharge'",
                                                             "df = df[['SOURCE','No of Transactions', 'Total Amount']]"],
                      sourceTag="Recharge"),
                 dict(source='cscmidf', filterConditions=["df['No of Transactions'] = len(df)",
                                                          "df['Total Amount'] = df['Txn amount'].sum()",
                                                          "df['SOURCE'] = 'CSC Total'",
                                                          "df = df[['SOURCE','No of Transactions', 'Total Amount']]"],
                      sourceTag="CSC Total")], writeToFile=True, reportName='Tally', postFilters=report_filters))

    cbs_dummy_report = dict(type="ReportGenerator",
                            properties=dict(sources=[
                                dict(source='rechargedf', sourceTag="CBS", filterConditions=["df = pd.DataFrame()"])],
                                writeToFile=True,
                                reportName='CBS'))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='CSC Recon'))

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    flows = [init, mis_data_loader, csc_sett_data_loader, purchase_data_loader, purchase_report, rech_data_loader,
             recharge_report, issuance_data_loader, issuance_report, matcher, tally_report_gen, cbs_dummy_report,
             dump_data, gen_meta_info]

    FlowRunner(flows).run()
