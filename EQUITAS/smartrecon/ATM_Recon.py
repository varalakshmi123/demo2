from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "21-Nov-2017"
    basePath = "/Users/shivashankar/Desktop/ReconData/Equitas/Raw Data"
    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    sfms_filters = []
    acqswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                    'df=df.loc[df["BENEFICIARYCODE"]!="EQU"]',
                    'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                    'df=df.loc[(df["TXN_TYPE"]=="04") | (df["TXN_TYPE"]=="88")]',

                    ]  # ,'df["TXN_AMOUNT"]=df["TXN_AMOUNT"]/100.0'
    issuerswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                    'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                    'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                    #'df=df.loc[(df["TXN_TYPE"]=="04") | (df["TXN_TYPE"]=="88")]',
                    'df["RRNISSUER"]=df["RRNISSUER"].astype(np.int64)',
                    'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                    'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)'
                    ]  # ,'df["TXN_AMOUNT"]=df["TXN_AMOUNT"]/100.0'
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="ATM", compressionType="", resultkey='',ignoredb=True))
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_SWITCH_Structure.csv"
    # elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
    #                                                              feedPattern="^EAEQU.*\.txt",
    #                                                              feedParams=params,
    #                                                              feedFilters=switchfilter,
    #                                                              resultkey="switchposdf"))

    # elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
    #                                                              feedPattern="^EANFZ.*\.txt",
    #                                                              feedParams=params,
    #                                                              feedFilters=issuerswitchfilter,
    #                                                              resultkey="switchposdf"))

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                                 feedPattern="^EANFY.*\.txt",
                                                                 feedParams=params,
                                                                 feedFilters=issuerswitchfilter,
                                                                 resultkey="switchposdf"))

    #cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="114210042"]',
    #              'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df=df.loc[df["TXN_AMOUNT"]>0]']
    #cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100036"]',
    #              'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0]["CR_DR_IND"]="D"']
    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0]["CR_DR_IND"]="D"','df["RRN"].fillna(0,inplace=True)','df["RRN"]=df["RRN"].astype(np.int64)',
                    'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)','df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_HOST_Structure.csv"
    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                 feedPattern="^H2H.*\.txt",
                                                                 feedParams=params,
                                                                 feedFilters=cbsfilters,
                                                                 resultkey="cbsdf"))

    elem4 = dict(id=4, next=5, type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                                 feedPattern="ACQRP*.*mE*.",
                                                                 feedParams=dict(type="OUTWARD"),
                                                                 feedFilters=[
                                                                     'df=df.loc[df["Actual Transaction Amount"]>0]'],
                                                                 resultkey="npciacqdf"))

    elem5 = dict(id=5, next=6, type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                                 feedPattern="ISSR*.*mE.*",
                                                                 feedParams=dict(type="INWARD"),
                                                                 feedFilters=[],
                                                                 resultkey="npciissdf"))

    elem6=dict(id=5, next=6, type="PreLoader", properties=dict(loadType="FixedFormat", source="861Pos",
                                                                 feedPattern="861ES*.*.*",
                                                                 feedParams=dict(feedformatFile="Reference/861_NPCI_RUPAY_STMT_STRUCT.csv"),
                                                                 feedFilters=['df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)','df["RRN"].fillna(0,inplace=True)','df["RRN"]=df["RRN"].astype(np.int64)','df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)'],
                                                                 resultkey="npciposdf"))

    elem7 =dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                                                 columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     amountColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     crdrcolumn=[
                                                                                                         "CR_DR_IND"],
                                                                                                     CreditDebitSign=True),
                                                                                                 sourceTag="CBS",subsetfilter=['df=df.loc[df["GLCODE"]=="208100034"]'])],matchResult="iiscbsdf"))


    elem8 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="switchposdf",
                                                                       columns=dict(keyColumns=["RRNISSUER", "CARD_NUMBER"],
                                                                                    sumColumns=["TXN_AMOUNT"],
                                                                                    matchColumns=["RRNISSUER", "CARD_NUMBER",
                                                                                                  "TXN_AMOUNT"]),
                                                                       sourceTag="Switch"), dict(source="iiscbsdf.CBS",
                                                                                                 columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     sumColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     matchColumns=[
                                                                                                         "RRN",
                                                                                                         "CARD_NUMBER",
                                                                                                         "TXN_AMOUNT"]),subsetfilter=['df=df.loc[df["GLCODE"]=="208100034"]'],
                                                                                                 sourceTag="CBS"),
                                                                  dict(source="npciposdf",
                                                                       columns=dict(
                                                                           keyColumns=["RRN",
                                                                                       "CARD_NUMBER"],
                                                                           sumColumns=[
                                                                               "TXN_AMOUNT"],
                                                                           matchColumns=[
                                                                               "RRN",
                                                                               "CARD_NUMBER",
                                                                               "TXN_AMOUNT"]),
                                                                       sourceTag="NPCIPOS")],
                                                                  # dict(source="npciacqdf",
                                                                  #      columns=dict(
                                                                  #          keyColumns=["RRN",
                                                                  #                      "PAN Number"],
                                                                  #          sumColumns=[
                                                                  #              "Actual Transaction Amount"],
                                                                  #          matchColumns=[
                                                                  #              "RRN",
                                                                  #              "PAN Number",
                                                                  #              "Actual Transaction Amount"]),
                                                                  #      sourceTag="NPCIAcquirer")],
                                                         matchResult="results"))

    elements = [elem1, elem2, elem3, elem4, elem5, elem6,elem7,elem8]
    f = FlowRunner(elements)
    f.run()
    f.result['switchposdf'].to_csv("Output/switchposdf.csv", index=False)
    f.result['cbsdf'].to_csv("Output/cbsdf.csv", index=False)
    f.result['npciacqdf'].to_csv("Output/npciacqdf.csv", index=False)
    f.result['npciissdf'].to_csv("Output/npciissdf.csv", index=False)
    for x in f.result['results'].keys():
        f.result['results'][x].to_csv("Output/" + x + ".csv")

        # params["columnNames"] = "S.No,Seq No,Transaction Ref,Related Reference,Sender IFSC,Sender A/c Type,Sender A/c No,Sender Name,Amount (Rs.),Value. Date,Remit. Date,Batch Id,FRESH / RETURN,Benf. IFSC,Status,Benf. A/c Type,Benf. A/c No.,Benf. Name,Return Code,Return Reason,Originator of Remittance,Sender To Receiver Information"
        # params['FooterKey'] = "Incoming_Rejected_Transactions"
        # elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
        #                                                              feedPattern=".*?_Inward.csv",
        #                                                              feedParams=dict(
        #                                                                  feedFileStructure='SyndicateBank_NEFT_CBS_Structure.csv'),
        #                                                              feedFilters=cbs_filters,
        #                                                              resultkey="cbsdf"))
        # elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
        #                                                              feedPattern="Inward_SFMS.*?.xls",
        #                                                              feedParams=params,
        #                                                              feedFilters=sfms_filters,
        #                                                              resultkey="sfmsdf"))
        #
        # leftcol = dict(keyColumns=["V_TXN_DESC"], sumColumns=["N_TXN_AMT_LCY"],
        #                matchColumns=["V_TXN_DESC", "N_TXN_AMT_LCY"])
        # rightcol = dict(keyColumns=["Transaction Ref"], sumColumns=["Amount (Rs.)"],
        #                 matchColumns=["Transaction Ref", "Amount (Rs.)"])
        # elem4 = dict(id=4, type="PerformMatch", properties=dict(leftData="cbsdf", rightData="sfmsdf",
        #                                                         left=leftcol, right=rightcol, leftUnmatch='cbsexp',
        #                                                         rightUnmatch='sfmsexp', matchResult='outwardMatch'))
        #
        # elements = [elem1, elem2, elem3, elem4]
        # f = FlowRunner(elements)
        # f.run()
