import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="FONE", compressionType="zip", resultkey='',
                                reconName="FONE_PAISA"))
    fiveone_filters = ["df['TRANSACTION_TYPE'] = 'NORMAL'","df.loc[df['Transaction Description'].str.contains('REV'),'TRANSACTION_TYPE'] = 'REVERSAL'",
                       "df.loc[df['Transaction Description'].str.contains('NEFT-RETURN'),'TRANSACTION_TYPE'] = 'REVERSAL'",
                       "df.loc[df['Transaction Description'].str.contains('RTGS_RETURN'),'TRANSACTION_TYPE'] = 'REVERSAL'",
                       "df['BENIFICIARY_NAME']=df['Transaction Description'].apply(lambda x:x.split('-')[2] if 'NEFT Cr-' in x or 'RTGS Cr-'in x else '')",
                       "df['IFSC_CODE'] = ' '",
                       "df['CR_DR_IND'] = df['Debit / Credit'].copy()",
                       "df['TRANSACTION_AMOUNT_ABS'] = df['Transaction Amount'].abs()",
                       "df.loc[((df['Transaction Description'].str.contains('IMPS'))&(df['TRANSACTION_TYPE']=='REVERSAL')&(df['Debit / Credit']=='D')&(df['Transaction Amount']<0)),'CR_DR_IND'] = 'C'",
                       "df['PRODUCT'] = df['Transaction Description'].apply(lambda x: 'IMPS' if re.match(r'IMPS', x) else ('RTGS Cr-' if re.match(r'RTGS Cr-', x) else ('NEFT Cr' if re.match(r'NEFT Cr-', x) else ('NEFT-RETURN' if re.match(r'NEFT-RETURN', x) else ('NEFT OUT' if re.match(r'NEFT O/W', x) else ('RTGS-' if re.match(r'RTGS-', x) else ('NEFT-' if 'NEFT-' in x and 'RETURN' not in x else ( 'RTGS-RETURN' if re.match(r'RTGS-RETURN', x) else('UPI' if re.match(r'UPI', x)else('GST' if 'GST' in x or 'CMS GST GL'in x else('API' if 'API' in x else('REV OF IMPS' if re.match(r'REV OF IMPS',x)else('FUNDS' if 'Funds' in x else('INSTANT CONSUMER TEC' if 'INSTANT CONSUMER TEC' in x and 'IMPS' not in x and 'NEFT' not in x else(re.match(r'[0-9]{9}',x).group() if re.match(r'[0-9]{9}',x) and 'INSTANT CONSUMER TEC' not in x else 'OTHERS')))))))))))))))",
                       "df['IFSC_CODE'] = df['Transaction Description'].apply(lambda x:x.split('-')[1] if 'NEFT Cr-' in x or 'RTGS Cr-' in x  else(x.split('@')[2].split('.')[0] if 'UPI' in x else ''))",
                        "df['RRN_Value'] = df['Transaction Description'].apply(lambda x: x.split('-')[2] if 'NEFT-RETURN' in x or 'RTGS-RETURN' in x else( x.split('-')[-1] if 'NEFT Cr-' in x or 'RTGS Cr-' in x  else(x.split(' ')[2] if re.match(r'NEFT O/W' ,x)  else( x.split('-')[1] if 'NEFT-' in x and 'RETURN' not in x else(x.split('-')[1] if re.match(r'RTGS-',x)else (x.split('/')[1] if re.match(r'UPI',x)else(x.split(':')[1].split('/')[0]if re.match(r'IMPS',x)else(x.split(':')[2] if re.match(r'REV OF IMPS',x)else(x.split('/')[1]  if 'INSTANT CONSUMER TEC' in x and 'IMPS' not in x and 'NEFT' not in x else(x.split('/')[1] if re.match(r'[0-9]{9}',x) and '/' in x and 'INSTANT CONSUMER TEC' not in x else(x.split(':')[1].split('/')[0] if 'Funds' in x and 'RRN :' in x else 0)))))))))))",
                       "df['RRN_Value'] = df['RRN_Value'].astype(str)"]



    load_fiveone = dict(type="PreLoader", properties=dict(loadType="CSV", source="YES_BANK_STATEMENT", feedPattern="SANDESHATS_*.*",
                                                       feedParams=dict(feedformatFile='FONE_PAISA_FIVEONE_Structure.csv',
                                                                       type="NEWTYPE",skiprows = 4), feedFilters=fiveone_filters,
                                                       resultkey="fpsdf"))
    load_cashin = dict(type="PreLoader", properties=dict(loadType="CSV", source="CIN", feedPattern="cashin.*",
                                                          feedParams=dict(
                                                              feedformatFile='CASH_IN_Structure.csv',
                                                              type="CASH_IN", skiprows=1),feedFilters = [],
                                                          resultkey="cindf"))
    cashout_filters = ["df['PAYMENT_AMT'] = df['PAYMENT_AMT'].fillna(0)","df['PAYMENT_AMT'] = df['PAYMENT_AMT'].astype(np.float64)"]
    load_cashout = dict(type="PreLoader", properties=dict(loadType="CSV", source="COUT", feedPattern="cashout.*",
                                                         feedParams=dict(
                                                             feedformatFile='CASH_OUT_Structure.csv',
                                                             type="CASH_IN", skiprows=1),feedFilters = cashout_filters,
                                                         resultkey="coutdf"))

    combined_filters = ["df = df.fillna('')","df['MERCHANT_REFERENCE_MATCHUSE'] =df['MERCHANT_REFERENCE'].copy()",
                        "df.loc[df['EVENT_ID'] == 'CSHOT','MERCHANT_REFERENCE_MATCHUSE'] = df['REFERENCE_1']"]
    load_combined = dict(type="PreLoader", properties=dict(loadType="CSV", source="CASH_LEDGER", feedPattern="comb_rpt.*",
                                                                                                               feedParams=dict(
                                                                                                                   feedformatFile='COMBINED_CSV_Structure.csv',
                                                                                                                   type="COMBINE", skiprows=1,delimiter = '|'),feedFilters = combined_filters,
                                                                                                               resultkey="combdf"))

    # load_cashin = dict(type="PreLoader", properties=dict(loadType="CSV", source="CIN", feedPattern="cashin.*",
    #                                                       feedParams=dict(
    #                                                           feedformatFile='CASH_IN_Structure.csv',
    #                                                           type="CASH_IN", ski                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 prows=1),feedFilters = [],
    #                                                       resultkey="cindf"))
    # cashout_filters = ["df['PAYMENT_AMT'] = df['PAYMENT_AMT'].fillna(0)","df['PAYMENT_AMT'] = df['PAYMENT_AMT'].astype(np.float64)"]
    # load_cashout = dict(type="PreLoader", properties=dict(loadType="CSV", source="COUT", feedPattern="cashout.*",
    #                                                      feedParams=dict(
    #                                                          feedformatFile='CASH_OUT_Structure.csv',
    #                                                          type="CASH_IN", skiprows=1),feedFilters = cashout_filters,
    #                                                      resultkey="coutdf"))
    # nwayin_match = dict(type="NWayMatch",
    #                                     properties=dict(sources=[dict(source="fpsdf",
    #                                                                   columns=dict(
    #                                                                       keyColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value"],
    #                                                                       sumColumns=[],
    #                                                                       matchColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value"]),
    #                                                                   sourceTag="FIVEONE"),
    #                                                              dict(source="cindf", columns=dict(
    #                                                                  keyColumns=["PART_TRAN_AMT","REMARKS"], sumColumns=[],
    #                                                                  matchColumns=["PART_TRAN_AMT", "REMARKS"]),
    #                                                                   sourceTag="CASHIN")],
    #                                                     matchResult="results"))
    # nwayout_match = dict(type="NWayMatch",
    #                   properties=dict(sources=[dict(source="fpsdf",
    #                                                 columns=dict(
    #                                                     keyColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value"],
    #                                                     sumColumns=[],
    #                                                     matchColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value"]),
    #                                                 sourceTag="FIVEONE"),
    #                                            dict(source="coutdf", columns=dict(
    #                                                keyColumns=["PAYMENT_AMT","REFERENCE_1"], sumColumns=[],
    #                                                matchColumns=["PAYMENT_AMT","REFERENCE_1"]),
    #                                                 sourceTag="CASHOUT")],
    #                                   matchResult="results"))
    nwaycombine_match = dict(type="NWayMatch",
                                           properties=dict(sources=[dict(source="fpsdf",
                                                                         columns=dict(
                                                                             keyColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value","CR_DR_IND"],
                                                                             sumColumns=[],
                                                                             matchColumns=["TRANSACTION_AMOUNT_ABS", "RRN_Value","CR_DR_IND"]),
                                                                         subsetfilter=["df = df[df['RRN_Value']!= '0'] "],
                                                                         sourceTag="YES_BANK_STATEMENT"),
                                                                    dict(source="combdf", columns=dict(
                                                                        keyColumns=["PART_TRAN_AMT","MERCHANT_REFERENCE_MATCHUSE","CR_DR_IND"], sumColumns=[],
                                                                        matchColumns=["PART_TRAN_AMT","MERCHANT_REFERENCE_MATCHUSE","CR_DR_IND"]),
                                                                         subsetfilter=["df = df[df['MERCHANT_REFERENCE_MATCHUSE'].fillna('') != ''] "],
                                                                         sourceTag="CASH_LEDGER")],
                                                           matchResult="results"))
    nwaydatewise_match = dict(type="NWayMatch",
                                           properties=dict(sources=[dict(source="results.YES_BANK_STATEMENT",
                                                                         columns=dict(
                                                                             keyColumns=['Transaction Date','Transaction Amount'],
                                                                             sumColumns=[],
                                                                             matchColumns=['Transaction Date','Transaction Amount']),
                                                                         subsetfilter = ["df = df.loc[df['CASH_LEDGER Match'] == 'UNMATCHED'] "],

                                                                         sourceTag="YES_BANK_STATEMENT"),
                                                                    dict(source="results.CASH_LEDGER", columns=dict(
                                                                        keyColumns=['TRAN_DATE','PART_TRAN_AMT'], sumColumns=[],
                                                                        matchColumns=['TRAN_DATE','PART_TRAN_AMT']),
                                                                         subsetfilter = ["df = df.loc[df['YES_BANK_STATEMENT Match'] == 'UNMATCHED'] "],
                                                                         sourceTag="CASH_LEDGER")],
                                                           matchResult="results"))
    # nwaydatewise_match = dict(type="NWayMatch",
    #                                        properties=dict(sources=[dict(source="",
    #                                                                      columns=dict(
    #                                                                          keyColumns=['Transaction Date','Transaction Amount','CR_DR_IND'],
    #                                                                          sumColumns=[],
    #                                                                          matchColumns=['Transaction Date','Transaction Amount','CR_DR_IND']),
    #                                                                      sourceTag="YES_BANK_STATEMENT"),
    #                                                                 dict(source="combdf", columns=dict(
    #                                                                     keyColumns=['TRAN_DATE','PART_TRAN_AMT','MERCHANT_REFERENCE_MATCHUSE'], sumColumns=[],
    #                                                                     matchColumns=['TRAN_DATE','PART_TRAN_AMT','MERCHANT_REFERENCE_MATCHUSE']),
    #
    #                                                                      sourceTag="CASH_LEDGER")],
    #                                                        matchResult="results"))
    dump_data = dict(type="DumpData", properties=dict(matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')
    # elements = [init,load_fiveone,load_cashin,load_cashout,nwayin_match,nwayout_match,dump_data,gen_meta_info]
    elements = [init, load_fiveone,load_combined,nwaycombine_match,nwaydatewise_match,dump_data,gen_meta_info]
    f = FlowRunner(elements)
    f.run()
