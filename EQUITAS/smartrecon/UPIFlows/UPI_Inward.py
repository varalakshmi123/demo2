import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
<<<<<<< HEAD
    stmtdate = '27-Sep-2019'
=======
    stmtdate = '01-Oct-2019'
>>>>>>> developement
    uploadFileName ='07072018.zip'

    elem1 = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="UPI", compressionType="zip",
                                resultkey='',

                                reconName="UPI Inward",incremental=True))

    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    "df = df[(df['Response Code'] == '00')|(df['Response Code'] == 'RB')]",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    elem2 = dict(type="MultiLoader", properties=dict(sources=[dict(loadType="NPCI", source="NPCI",
                                                                   feedPattern="UPIRAWDATAISS*.*",
                                                                   feedParams=dict(type="UPI_INWARD", skiprows=0),
                                                                   feedFilters=npci_filters, resultkey="npcidf"),
                                                              dict(loadType="NPCI", source="NPCI",
                                                                   feedPattern="UPIMERCHANTRAWDATAISSEQT*.*",
                                                                   feedParams=dict(type="ISSMERCHANT", skiprows=0),
                                                                   feedFilters=npci_filters, disableCarryFwd=True,
                                                                   errors=False, resultkey="npcidf")]))

    # elem3 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
    #                                                feedPattern="UPIMERCHANTRAWDATAISSEQT*.*",
    #                                                feedParams=dict(type="ISSMERCHANT", skiprows=0),
    #                                                feedFilters=npci_filters,
    #                                                resultkey="npcidf"))

    elem4= dict(type="IncrementalLoader",
                               properties=dict(source="npcidf", sourceTag="NPCI", keycolumns=['Ref Number'],
                                               resultkey="npcidf",converters={"Ref Number": 'str'}))

    elem5 = dict(type="ExpressionEvaluator",
                 properties=dict(source="npcidf", expressions=["df=df[df['Response Code'] == 'RB']"],
                                 resultkey='npciRBdf'))

    cbs_filters = [
        "df=df[~(df['TRANSACTION_DESCRIPTION'].str.contains('|'.join(['UPI - Bene','UPI- Remitter','UPI - Net'])))]",
        "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()",
        "df=df[df['BRANCH_CODE']=='9999']",
        "df['RRN'] = df['TRANSACTION_DESCRIPTION'].str.extract('([0-9]{12})')"]

    elem6 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                                 feedParams=dict(delimiter="|", skiprows=1,
                                                                                 feedformatFile='UPI_CBS_INWARD.csv'),
                                                                 feedPattern="UPI_INWARD_*.*",
                                                                 resultkey="cbsdf", feedFilters=cbs_filters))
    elem7 = dict(type="IncrementalLoader",
                              properties=dict(source="cbsdf", sourceTag="CBS", keycolumns=['RRN'],
                                              resultkey="cbsdf", converters={"RRN": 'str'}))
    elem8=dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='npcidf', dataFields=['RRN'],
                                            lookupFields=['Ref Number'], includeCols=['Response Code'],
                                            resultkey="cbsdf"))


    elem9 = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                               columns=dict(
                                                                                   keyColumns=["RRN"],
                                                                                   amountColumns=['AMOUNT'],
                                                                                   crdrcolumn=["CREDIT_DEBIT"],
                                                                                   CreditDebitSign=False),
                                                                               sourceTag="CBS")],
                                                                 matchResult="cbsresults"))

    elem10 = dict(type="NWayMatch", properties=dict(sources=[dict(source="npcidf",
                                               columns=dict(
                                                   keyColumns=["Ref Number", 'Transaction Amount'],
                                                   sumColumns=[],
                                                   matchColumns=["Ref Number", "Transaction Amount"]),
                                               sourceTag="NPCI"),
                                          dict(source="cbsresults.CBS",columns=dict(
                                              keyColumns=["RRN", 'AMOUNT'], sumColumns=[],
                                              matchColumns=["RRN", 'AMOUNT']), sourceTag="CBS")],
                                 matchResult="results"))


    timeout_filters = [r"df['RRN'] = df['RRN'].str.lstrip('\'')", "df=df[df['Remitter']!='EQT']","df['Reconcilation_REMARKS']='TIMEOUT'"]
    elem11 = dict(type="PreLoader", properties=dict(loadType='HTML', source="TIMEOUT",
                                                                 feedPattern="UPI Time Out Cases Report*.*",
                                                  feedParams=dict(feedformatFile='UPI_TIMEOUT_Structure.csv'),errors=False,
                                                                 feedFilters=timeout_filters,disableCarryFwd=True,
                                                                 resultkey="timeoutdf"))

    npcirb_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    "df = df[df['Response Code'] == 'RB']",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]


    elem12= dict(type="ExpressionEvaluator",
                 properties=dict(source="results.CBS", expressions=["df=df[df['Self Matched']=='UNMATCHED']","df=df[df['NPCI Match']=='UNMATCHED']"],
                                 resultkey='cbs_unmatch'))

    elem13 = dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='cbs_unmatch', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'CBS_RB': 'FOUND'}, resultkey="npciRBdf"))

    elem14= dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='timeoutdf', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'TIMEOUT REMARKS': 'FOUND'},resultkey="rawreport"))

    elem15= dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='rawreport',
                            filterConditions=["df.columnns = ['']"
                                "df=df[(df.get('CBS_RB','')=='FOUND')&(df['TIMEOUT REMARKS'].fillna('')=='')]"],
                                                sourceTag="NPCICBSTIME")],
                                  resultKey='Successful_credit_transaction',writeToFile=True,reportName='Successful_credit_transaction'))

    elem16 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='rawreport', filterConditions=[
                      "df=df[(df.get('CBS_RB','')=='FOUND')&(df['TIMEOUT REMARKS']=='FOUND')]"],
                                                sourceTag="NPCICBSTIME")],
                                  resultKey='Successful_credit_postings', writeToFile=True, reportName='Successful_credit_postings'))
    elem17 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='rawreport', filterConditions=[
                      "df=df[(df.get('CBS_RB','')=='')&(df['TIMEOUT REMARKS']=='FOUND')]"],
                                                sourceTag="NPCICBSTIME")],
                                  resultKey='Credit_posting_to_be_processed', writeToFile=True, reportName='Credit_posting_to_be_processed'))

    compute_close_bal = dict(type='ComputeClosingBalance', properties=dict(
        sources=[dict(source="results.CBS", sourceTag="CBS",
                      filterConditions=["stmtDate = payload['statementDate']",
                                        "day_of_year = (stmtDate - datetime(stmtDate.year, 1, 1)).days + 1",
                                        "df = df[(df['VALUE_DATE'].dt.dayofyear <= day_of_year) & (df['NPCI Match'] == 'UNMATCHED')]"],
                      keyColumn='AMOUNT')]))
    BBPSreport = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='reconSummary', sourceTag="SUMMARY",
                      filterConditions=[
                          "df = df.groupby(['Source'])['Matched Amount','Matched Count'].sum().reset_index()",
                          "df.loc[len(df)]=['Difference',0,0]",
                          "df.loc[df['Source']=='Difference','Matched Amount']=df[df['Source']=='NPCI']['Matched Amount'].sum()-df[df['Source']=='CBS']['Matched Amount'].sum()",
                          "df.loc[df['Source']=='Difference','Matched Count']=df[df['Source']=='NPCI']['Matched Count'].sum()-df[df['Source']=='CBS']['Matched Count'].sum()"])],
        writeToFile=True, reportName='BBPS report'))

    elem18= dict(type="GenerateReconSummary",
                            properties=dict(
                            sources=[dict(resultKey="NPCI",sourceTag="NPCI",aggrCol=['Transaction Amount']),
                            dict(resultKey="CBS", sourceTag="CBS",aggrCol=['AMOUNT'])], groupbyCol=['Cycle']))
    cbs_post_remarks = dict(type="AddPostMatchRemarks",
                            properties=dict(source='results.CBS', remarkCol='Recon Remarks',
                                            remarks={"matchRemarks": "System Matched",
                                                     "selfRemarks": "Reverse Matched"}, how='all'))

    npci_post_remarks = dict(type="AddPostMatchRemarks",
                             properties=dict(source='results.NPCI', remarkCol='Recon Remarks',
                                             remarks={"matchRemarks": "System Matched"}, how='all'))

    gen_rev_report = dict(type="ReportGenerator", properties=dict(
        sources=[
            dict(source='results.CBS', sourceTag="CBS",
                 filterConditions=["df = df[df['Recon Remarks'] == 'Reverse Matched']"])],
        writeToFile=True, reportName='CBS Reversals'))
    exclude_rev_records = dict(type="ExpressionEvaluator",
                               properties=dict(source="results.CBS", resultkey="results.CBS",
                                               expressions=["df = df[df['Recon Remarks'] != 'Reverse Matched']"]))


    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='UPI Inward', matched='any'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2,elem4, elem5, elem6, elem7,elem8,elem9,elem10,elem11,elem12,elem13,elem14,elem15,elem16,compute_close_bal,elem17,elem18,BBPSreport,
                cbs_post_remarks,npci_post_remarks,gen_rev_report,exclude_rev_records,elem19,gen_meta_info]
    f = FlowRunner(elements)
    f.run()
