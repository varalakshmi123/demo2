import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '01-Mar-2018'
    uploadFileName = '01032018IOB.zip'
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, recontype="UPI", compressionType="zip", resultkey='',
                                 reconName="UPI Inward"))

    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    # "df = df[df['Response Code'] == 'RB']",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                   feedPattern="UPIRAWDATAISS*.*",
                                                   feedParams=dict(type="UPI_INWARD", skiprows=0),
                                                   feedFilters=npci_filters,
                                                   resultkey="npciRBdf"))
    cbs_filters = ["df['RRN'] = df.Remarks.str.split(pat='/').str[1]",
                   "df['UPIindicator']= df.Remarks.str.split(pat='/').str[0]",
                   "df['DRCRIND'] = df.Remarks.str.split(pat='/').str[3]",
                   "df['DRCRIND']=df['DRCRIND'].fillna('')",
                   "df.loc[df['DRCRIND'] == '', 'DRCRIND'] = df.Remarks.str.split(pat='/').str[2]",
                   "df['DRCRIND'] = df['DRCRIND'].astype(str).str[0]",
                   "df=df[df['DRCRIND']=='C']",
                   "df.loc[df['UPIindicator']=='UPIREV','DRCRIND']='D'"
        ]

    elem3 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                   feedParams=dict(delimiter="|", skiprows=1,
                                                                   feedformatFile='IOB_UPI_CBS_Structure.csv'),
                                                   feedPattern="UPI_Extraction_New*.*",
                                                   resultkey="cbsdf", feedFilters=cbs_filters))
    elem4 = dict(type='VLookup',
                 properties=dict(data='cbsdf', lookup='npciRBdf', dataFields=['RRN'],
                                 lookupFields=['Ref Number'], includeCols=['Response Code'], resultkey="cbsdf"))

    elem5 = dict(type="ExpressionEvaluator",
                  properties=dict(source="cbsdf", expressions=["df=df[df['Response Code']=='RB']"],
                                  resultkey='cbsdf'))

    elem6 = dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='cbsdf', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'Status_Code': '102'}, resultkey="npciCBS"))
    elem7 = dict(type="ExpressionEvaluator",
                 properties=dict(source="npciCBS", expressions=["df.loc[df['Status_Code'].isnull(),'Status_Code']='103'"],
                                 resultkey='cbsdf'))

    elem8 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='cbsdf',
                                                filterConditions=[],
                                                sourceTag="NPCICBS")],
                                  resultKey='102103', writeToFile=True,
                                  reportName='102103'))
    issdump=dict(type='MasterDumpHandler',
                 properties=dict(masterType="ISSUER_STATUS", operation="insert", source='cbsdf',
                                 dropOnRollback=True, insertCols=[],resultKey='isscbsdb'))

    npciacq_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    "df = df[(df['Response Code'] != 'RB')&(df['Response Code'] != '00')]",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]


    elem9= dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCIACQURIER",
                                                   feedPattern="UPIRAWDATAACQ*.*",
                                                   feedParams=dict(type="UPI_OUTWARD", skiprows=0),
                                                   feedFilters=npciacq_filters,
                                                   resultkey="acqnpcidf"))

    elem10 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCIMERCHANT",
                                                   feedPattern="UPIMERCHANTRAWDATAACQ*.*",
                                                   feedParams=dict(type="ACQMERCHANT", skiprows=0),
                                                   feedFilters=npciacq_filters,
                                                   resultkey="acqnpcidf"))
    acqcbs_filters = ["df['RRN'] = df.Remarks.str.split(pat='/').str[1]",
                   "df['UPIindicator']= df.Remarks.str.split(pat='/').str[0]",
                   "df['DRCRIND'] = df.Remarks.str.split(pat='/').str[3]",
                   "df['DRCRIND']=df['DRCRIND'].fillna('')",
                   "df.loc[df['DRCRIND'] == '', 'DRCRIND'] = df.Remarks.str.split(pat='/').str[2]",
                   "df['DRCRIND'] = df['DRCRIND'].astype(str).str[0]",
                   "df=df[df['DRCRIND']=='D']",
                   "df.loc[df['UPIindicator']=='UPIREV','DRCRIND']='C'"
                   ]

    elem11 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                   feedParams=dict(delimiter="|", skiprows=1,
                                                                   feedformatFile='IOB_UPI_CBS_Structure.csv'),
                                                   feedPattern="UPI_Extraction_New*.*",
                                                   resultkey="acqcbsdf", feedFilters=acqcbs_filters))

    elem12= dict(type='VLookup',
                 properties=dict(data='acqcbsdf', lookup='acqnpcidf', dataFields=['RRN'],
                                 lookupFields=['Ref Number'], includeCols=['Response Code'], resultkey="acqcbsdf"))

    elem13 = dict(type="ExpressionEvaluator",
                 properties=dict(source="acqcbsdf", expressions=["df = df[(df['Response Code'] != 'RB')&(df['Response Code'] != '00')]"],
                                 resultkey='acqcbsdf'))
    elem14 = dict(type="NWayMatch", properties=dict(sources=[dict(source="acqcbsdf",
                                                                 columns=dict(
                                                                     keyColumns=["RRN"],
                                                                     amountColumns=['Tran Amt'],
                                                                     crdrcolumn=["DRCRIND"],
                                                                     CreditDebitSign=False),
                                                                 sourceTag="CBS")],
                                                   matchResult="cbsresults"))
    elem15 = dict(type="ExpressionEvaluator",
                 properties=dict(source="cbsresults.CBS",
                                 expressions=["df.loc[df['Self Matched']=='MATCHED','Status_Code']='102'",
                                              "df.loc[df['Self Matched']=='UNMATCHED','Status_Code']='103'"],
                                 resultkey='acqcbsdf'))
    elem16 = dict(type='VLookup',
                  properties=dict(data='acqnpcidf', lookup='acqcbsdf', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], includeCols=['Status_Code'], resultkey="acqnpcidf"))
    elem17 = dict(type="ExpressionEvaluator",
                 properties=dict(source="acqnpcidf",
                                 expressions=["df.loc[df['Status_Code'].isnull(),'Status_Code']='104'"],
                                 resultkey='acqnpcidf'))
    elem18=dict(type='MasterDumpHandler',
                 properties=dict(masterType="ACQUIRER_STATUS", operation="insert", source='acqnpcidf',
                                 dropOnRollback=True, insertCols=[],resultKey='cbsdb'))

    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='UPI Outward', matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8,
                elem9, elem10, elem11, elem12, elem13, elem14,
                elem15, elem16, elem17,elem18,
                elem19,gen_meta_info]
    f = FlowRunner(elements)
    f.run()


















