import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '01-Mar-2018'
    uploadFileName = '01032018IOB.zip'
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, recontype="UPI", compressionType="zip", resultkey='',
                                 reconName="UPI_102_103"))

    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    # "df = df[df['Response Code'] == 'RB']",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                   feedPattern="UPIRAWDATAISS*.*",
                                                   feedParams=dict(type="UPI_INWARD", skiprows=0),
                                                   feedFilters=npci_filters,
                                                   resultkey="npcissuerdf"))

    cbs_filters = ["df['RRN'] = df.Remarks.str.split(pat='/').str[1]",
                   "df['UPIindicator']= df.Remarks.str.split(pat='/').str[0]",
                   "df['DRCRIND'] = df.Remarks.str.split(pat='/').str[3]",
                   "df['DRCRIND']=df['DRCRIND'].fillna('')",
                   "df.loc[df['DRCRIND'] == '', 'DRCRIND'] = df.Remarks.str.split(pat='/').str[2]",
                   "df['DRCRIND'] = df['DRCRIND'].astype(str).str[0]",
                   "df=df[df['DRCRIND']=='C']",
                   "df.loc[df['UPIindicator']=='UPIREV','DRCRIND']='D'"
                   ]

    elem3 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                   feedParams=dict(delimiter="|", skiprows=1,
                                                                   feedformatFile='IOB_UPI_CBS_Structure.csv'),
                                                   feedPattern="UPI_Extraction_New*.*",
                                                   resultkey="cbsdf", feedFilters=cbs_filters))

    timeout_filters = [r"df['RRN'] = df['RRN'].str.lstrip('\'')"]

    TimeOut = dict(type="PreLoader", properties=dict(loadType='CSV', source="TIMEOUT",
                                                     feedPattern="UPI Time Out Cases Report*.*",
                                                     feedParams=dict(delimiter=",", skiprows=1,
                                                                     feedformatFile='IOB_Timeout_Structure.csv'),
                                                     errors=False,
                                                     feedFilters=timeout_filters, disableCarryFwd=True,
                                                     resultkey="timeoutdf"))

    elem4 = dict(type='VLookup',
                 properties=dict(data='cbsdf', lookup='npcissuerdf', dataFields=['RRN'],
                                 lookupFields=['Ref Number'], includeCols=['Response Code'], resultkey="cbsdf"))

    CbsReversal = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                       columns=dict(
                                                                           keyColumns=["RRN"],
                                                                           amountColumns=['Tran Amt'],
                                                                           crdrcolumn=["DRCRIND"],
                                                                           CreditDebitSign=False),
                                                                       sourceTag="CBS")],
                                                         matchResult="cbsresults"))

    # elem5 = dict(type="ExpressionEvaluator",
    #               properties=dict(source="cbsdf", expressions=["df=df[df['Response Code']=='RB']"],
    #                               resultkey='cbsdf'))

    IssuerNwayMatch = dict(type="NWayMatch",
                           properties=dict(sources=[dict(source="cbsresults.CBS",
                                                         columns=dict(
                                                             keyColumns=['RRN', 'Tran Amt'],
                                                             sumColumns=[],
                                                             matchColumns=['RRN', 'Tran Amt']),
                                                         subsetfilter=["df=df[(df['Self Matched']!='MATCHED')]",
                                                                       "df=df[df['Response Code']=='RB']"],
                                                         sourceTag="CBS"),
                                                    dict(source="npcissuerdf", columns=dict(
                                                        keyColumns=['Ref Number', 'Transaction Amount'],
                                                        sumColumns=[],
                                                        matchColumns=['Ref Number', 'Transaction Amount']),
                                                         subsetfilter=["df=df[df['Response Code']=='RB']"],
                                                         sourceTag="ISSUER"),

                                                    dict(source="timeoutdf", columns=dict(
                                                        keyColumns=['RRN', 'Amount'],
                                                        sumColumns=[],
                                                        matchColumns=['RRN', 'Amount']), subsetfilter=[
                                                        'df["Response Code"]=df["Response Code"].str.lstrip("\'")',
                                                        'df["RRN"]=df["RRN"].str.lstrip("\'")',
                                                        "df=df[df['Response Code']=='RB']",
                                                        "df=df[df['Beneficiary']=='IOB']"], sourceTag="TIMEOUT")],
                                           matchResult="issuerresults"))

    # elem6 = dict(type='VLookup',
    #               properties=dict(data='cbsdf', lookup='npcissuerdf', dataFields=['RRN'],dataFieldsFilter=["df=df[df['Response Code']=='RB']","df=df[df['Self Matched']!='MATCHED']"],
    #                               lookupFieldsFilter=["df=df[df['Response Code']=='RB']"] ,lookupFields=['Ref Number'], resultkey="iss-cbs-df"))




    # Credit_Confirmations= dict(type='VLookup',
    #                   properties=dict(data='iss-cbs-df', lookup='timeoutdf', dataFields=['RRN'],dataFieldsFilter=["df=df[df['Response Code']=='RB']"],
    #                                   lookupFieldsFilter=['df["Response Code"]=df["Response Code"].str.lstrip("\'")','df["RRN"]=df["RRN"].str.lstrip("\'")',"df=df[df['Response Code']=='RB']","df=df[df['Beneficiary']=='IOB']"] ,lookupFields=['RRN'], resultkey="iss-cbs-df"))
    #
    #
    # Credit_Confirmations_Report=dict(type='ReportGenerator',
    #               properties=dict(sources=[dict(source='Credit_Confirmations',
    #                                             filterConditions=["df=df[df['Response Code']=='RB']"],
    #                                             sourceTag="NPCICBS")], writeToFile=True,
    #                               reportName='Credit_Confirmations'))

    # elem7 = dict(type="ExpressionEvaluator",
    #              properties=dict(source="npciCBS", expressions=["df.loc[df['Status_Code'].isnull(),'Status_Code']='103'"],
    #                              resultkey='cbsdf'))
    #
    # elem8 = dict(type='ReportGenerator',
    #               properties=dict(sources=[dict(source='cbsdf',
    #                                             filterConditions=[],
    #                                             sourceTag="NPCICBS")],
    #                               resultKey='102103', writeToFile=True,
    #                               reportName='102103'))
    #

    issdump = dict(type='MasterDumpHandler',
                   properties=dict(masterType="ISSUER_STATUS", operation="insert", source='cbsdf',
                                   dropOnRollback=True, insertCols=[], resultKey='isscbsdb'))

    npciacq_filters = ["df = df[df['Transaction Amount'] > 0]",
                       "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                       # "df = df[(df['Response Code'] != 'RB')&(df['Response Code'] != '00')]",
                       "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    elem9 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCIACQURIER",
                                                   feedPattern="UPIRAWDATAACQ*.*",
                                                   feedParams=dict(type="UPI_OUTWARD", skiprows=0),
                                                   feedFilters=npciacq_filters,
                                                   resultkey="acqnpcidf"))

    elem10 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCIMERCHANT",
                                                    feedPattern="UPIMERCHANTRAWDATAACQ*.*",
                                                    feedParams=dict(type="ACQMERCHANT", skiprows=0),
                                                    feedFilters=npciacq_filters,
                                                    resultkey="acqnpcidf"))

    acqcbs_filters = ["df['RRN'] = df.Remarks.str.split(pat='/').str[1]",
                      "df['UPIindicator']= df.Remarks.str.split(pat='/').str[0]",
                      "df['DRCRIND'] = df.Remarks.str.split(pat='/').str[3]",
                      "df['DRCRIND']=df['DRCRIND'].fillna('')",
                      "df.loc[df['DRCRIND'] == '', 'DRCRIND'] = df.Remarks.str.split(pat='/').str[2]",
                      "df['DRCRIND'] = df['DRCRIND'].astype(str).str[0]",
                      "df=df[df['DRCRIND']=='D']",
                      "df.loc[df['UPIindicator']=='UPIREV','DRCRIND']='C'"
                      ]

    AccquirerNwayMatch = dict(type="NWayMatch",
                              properties=dict(sources=[dict(source="issuerresults.CBS",
                                                            columns=dict(
                                                                keyColumns=['RRN', 'Tran Amt'],
                                                                sumColumns=[],
                                                                matchColumns=['RRN', 'Tran Amt']),
                                                            subsetfilter=["df=df[(df['Self Matched']!='MATCHED')]",
                                                                          "df=df[df['Response Code']=='RB']"],
                                                            sourceTag="CBSACQ"),
                                                       dict(source="acqnpcidf", columns=dict(
                                                           keyColumns=['Ref Number', 'Transaction Amount'],
                                                           sumColumns=[],
                                                           matchColumns=['Ref Number', 'Transaction Amount']),
                                                            subsetfilter=["df=df[df['Response Code']=='RB']"],
                                                            sourceTag="ACCQUIRER"),

                                                       dict(source="issuerresults.TIMEOUT", columns=dict(
                                                           keyColumns=['RRN', 'Amount'],
                                                           sumColumns=[],
                                                           matchColumns=['RRN', 'Amount']), subsetfilter=[
                                                           # 'df["Response Code"]=df["Response Code"].str.lstrip("\'")',
                                                           # 'df["RRN"]=df["RRN"].str.lstrip("\'")',
                                                           # "df=df[df['Response Code']=='RB']",
                                                           "df=df[df['Remitter']=='IOB']"], sourceTag="TIMEOUTACQ")],
                                              matchResult="acqresults"))

    Report1 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="issuerresults.ISSUER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBS Match']=='MATCHED')&(df['TIMEOUT Match']=='MATCHED')]",
                                                     "df['Status_Code']=102"])],
                                   reportName='Issuer-Credit Confirmations', writeToFile=True))

    Report2 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="issuerresults.ISSUER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBS Match']=='UNMATCHED')&(df['TIMEOUT Match']=='MATCHED')]",
                                                     "df['Status_Code']=103"])],
                                   reportName='TCC Credit Adjustment', writeToFile=True))

    Report3 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="issuerresults.ISSUER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBS Match']=='UNMATCHED')&(df['TIMEOUT Match']=='UNMATCHED')]",
                                                     "df['Status_Code']='CBS & TimeOut not Exists'"])],
                                   reportName=' IssuerCBS&TimeOutnotExists', writeToFile=True))

    Report4 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="issuerresults.ISSUER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBS Match']=='MATCHED')&(df['TIMEOUT Match']=='UNMATCHED')]"
                                                 ])],
                                   reportName='IssuerCreditReversalAdjustment', writeToFile=True))

    Report5 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="acqresults.ACCQUIRER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBSACQ Match']=='MATCHED')&(df['TIMEOUTACQ Match']=='MATCHED')]",
                                                     "df['Status_Code']=102"])],
                                   reportName='Acquirer Debit Confirmations', writeToFile=True))

    Report6 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="acqresults.ACCQUIRER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBSACQ Match']=='UNMATCHED')&(df['TIMEOUTACQ Match']=='MATCHED')]",
                                                     "df['Status_Code']=103"])],
                                   reportName='TCC Debit Adjustment', writeToFile=True))

    Report7 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="acqresults.ACCQUIRER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBSACQ Match']=='UNMATCHED')&(df['TIMEOUTACQ Match']=='UNMATCHED')]",
                                                     "df['Status_Code']='CBS & TimeOut not Exists'"])],
                                   reportName=' AcquirerCBS&TimeOutnotExists', writeToFile=True))

    Report8 = dict(type="ReportGenerator",
                   properties=dict(sources=[dict(source="acqresults.ACCQUIRER", sourceTag='NPCI',
                                                 filterConditions=[
                                                     "df=df[(df['CBSACQ Match']=='MATCHED')&(df['TIMEOUTACQ Match']=='UNMATCHED')]"
                                                 ])],
                                   reportName='Debit Reversal Adjustment file',
                                   writeToFile=True))

    # elem11 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
    #                                                feedParams=dict(delimiter="|", skiprows=1,
    #                                                                feedformatFile='IOB_UPI_CBS_Structure.csv'),
    #                                                feedPattern="UPI_Extraction_New*.*",
    #                                                resultkey="acqcbsdf", feedFilters=acqcbs_filters))
    #
    # elem12= dict(type='VLookup',
    #              properties=dict(data='acqcbsdf', lookup='acqnpcidf', dataFields=['RRN'],
    #                              lookupFields=['Ref Number'], includeCols=['Response Code'], resultkey="acqcbsdf"))
    #
    # elem13 = dict(type="ExpressionEvaluator",
    #              properties=dict(source="acqcbsdf", expressions=["df = df[(df['Response Code'] != 'RB')&(df['Response Code'] != '00')]"],
    #                              resultkey='acqcbsdf'))
    # elem14 = dict(type="NWayMatch", properties=dict(sources=[dict(source="acqcbsdf",
    #                                                              columns=dict(
    #                                                                  keyColumns=["RRN"],
    #                                                                  amountColumns=['Tran Amt'],
    #                                                                  crdrcolumn=["DRCRIND"],
    #                                                                  CreditDebitSign=False),
    #                                                              sourceTag="CBS")],
    #                                                matchResult="cbsresults"))

    filter1 = ["df.loc[df['Self Matched']=='MATCHED','Status_Code']='102'",
               "df.loc[df['Self Matched']=='UNMATCHED','Status_Code']='103'"]

    elem15 = dict(type="ExpressionEvaluator",
                  properties=dict(source="cbsresults.CBS",
                                  expressions=filter1,
                                  resultkey='acqcbsdf'))

    elem16 = dict(type='VLookup',
                  properties=dict(data='acqnpcidf', lookup='acqcbsdf', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], includeCols=['Status_Code'], resultkey="acqnpcidf"))
    elem17 = dict(type="ExpressionEvaluator",
                  properties=dict(source="acqnpcidf",
                                  expressions=["df.loc[df['Status_Code'].isnull(),'Status_Code']='104'"],
                                  resultkey='acqnpcidf'))

    # report2 = dict(type="ReportGenerator", properties=dict(sources=[dict(source="acqnpcidf", sourceTag='CBS',
    #                                                                          filterConditions=["df.loc[df['Status_Code'].isnull(),'Status_Code']='104'"])],
    #                                                            reportName='Report104',writeToFile=True))
    elem18 = dict(type='MasterDumpHandler',
                  properties=dict(masterType="ACQUIRER_STATUS", operation="insert", source='acqnpcidf',
                                  dropOnRollback=True, insertCols=[], resultKey='cbsdb'))

    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='UPI Outward', matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2, elem3, TimeOut, elem4, CbsReversal, IssuerNwayMatch,
                elem9, elem10, AccquirerNwayMatch, Report1, Report2, Report3, Report4, Report5, Report6, Report7,
                Report8,
                elem19, gen_meta_info]
    f = FlowRunner(elements)
    f.run()

















