import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    elem1 = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="UPI", compressionType="zip", resultkey='',
                                reconName="UPI Outward"))


    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                     # "df = df[(df['Response Code'] == '00')|(df['Response Code'] == 'RB')]",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    elem2 = dict(type="MultiLoader", properties=dict(sources=[dict(loadType="NPCI", source="NPCI",
                                                                   feedPattern="UPIRAWDATAACQ*.*",
                                                                   feedParams=dict(type="UPI_OUTWARD", skiprows=0),
                                                                   feedFilters=npci_filters,
                                                                   resultkey="npcitotal"),
                                                              dict(loadType="NPCI", source="NPCI",
                                                                   feedPattern="UPIMERCHANTRAWDATAACQ*.*",
                                                                   feedParams=dict(type="ACQMERCHANT", skiprows=0),
                                                                   feedFilters=npci_filters, disableCarryFwd=True,
                                                                   resultkey="npcitotal", errors=False)]))
    elem3 = dict(type="ExpressionEvaluator",
                 properties=dict(source="npcitotal", expressions=["df = df[df['Response Code'] == '00']"],
                                 resultkey='npcidf'))
    elem4 = dict(type="ExpressionEvaluator",
                  properties=dict(source="npcitotal", expressions=["df=df[df['Response Code'] == 'RB']"],
                                  resultkey='npciRBdf'))

    elem5 =dict(type="IncrementalLoader",
                               properties=dict(source="npcidf", sourceTag="NPCI", keycolumns=['Ref Number'],
                                               resultkey="npcidf",converters={"Ref Number": 'str'}))

    cbs_filters = [
        "df['RRN'] = df.Remarks.str.split(pat='/').str[1]",
        "df['UPIindicator']= df.Remarks.str.split(pat='/').str[0]",
        "df['DRCRIND'] = df.Remarks.str.split(pat='/').str[3]",
        "df['DRCRIND']=df['DRCRIND'].fillna('')",
        "df.loc[df['DRCRIND'] == '', 'DRCRIND'] = df.Remarks.str.split(pat='/').str[2]",
        "df['DRCRIND'] = df['DRCRIND'].astype(str).str[0]",
        "df=df[df['DRCRIND']=='D']",
        "df.loc[df['UPIindicator']=='UPIREV','DRCRIND']='C'"
    ]

    elem6= dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                                 feedParams=dict(delimiter="|", skiprows=1,
                                                                                 feedformatFile='IOB_UPI_CBS_Structure.csv'),
                                                                 feedPattern="UPI_Extraction_New.*.*",
                                                                 resultkey="cbsdf", feedFilters=cbs_filters))
    elem7 = dict(type="IncrementalLoader",
                              properties=dict(source="cbsdf", sourceTag="CBS", keycolumns=['RRN'],
                                              resultkey="cbsdf", converters={"RRN": 'str'}))
    elem8 = dict(type='VLookup',
                 properties=dict(data='cbsdf', lookup='npcitotal', dataFields=['RRN'],
                                 lookupFields=['Ref Number'], includeCols=['Response Code'],
                                 resultkey="cbsdf"))

    elem9 = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                               columns=dict(
                                                                                   keyColumns=["RRN"],
                                                                                   amountColumns=['Tran Amt'],
                                                                                   crdrcolumn=["DRCRIND"],
                                                                                   CreditDebitSign=False),
                                                                               sourceTag="CBS")],
                                                                 matchResult="cbsresults"))
    elem10= dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="npcidf",
                                               columns=dict(
                                                   keyColumns=["Ref Number", 'Transaction Amount'],
                                                   sumColumns=[],
                                                   matchColumns=["Ref Number", "Transaction Amount"]),
                                               sourceTag="NPCI"),
                                          dict(source="cbsresults.CBS", columns=dict(
                                              keyColumns=["RRN", 'Tran Amt'], sumColumns=[],
                                              matchColumns=["RRN", 'Tran Amt']), sourceTag="CBS")],
                                 matchResult="results"))

    timeout_filters = [r"df['RRN'] = df['RRN'].str.lstrip('\'')", "df=df[df['Remitter']!='IOB']"]
    elem11 = dict(type="PreLoader", properties=dict(loadType='CSV', source="TIMEOUT",
                                                    feedPattern="UPI Time Out Cases Report*.*",
                                                    feedParams=dict(delimiter=",", skiprows=1,
                                                                    feedformatFile='IOB_Timeout_Structure.csv'),
                                                    errors=False,
                                                    feedFilters=timeout_filters, disableCarryFwd=True,
                                                    resultkey="timeoutdf"))
    elem12 = dict(type="ExpressionEvaluator",
                  properties=dict(source="results.CBS", expressions=[
                      "df=df[df['Response Code']=='RB']","df=df[df['Self Matched']!='MATCHED']"
                                                                     ],
                                  resultkey='cbs_unmatch'))

    elem13 = dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='cbs_unmatch', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'CBS_RB': 'FOUND'}, resultkey="npciRBdf"))

    elem14 = dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='timeoutdf', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'TIMEOUT REMARKS': 'FOUND'}, resultkey="rawreport"))

    elem15 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='rawreport',
                                                filterConditions=[
                                                    "df=df[(df.get('CBS_RB','')=='FOUND')&(df['TIMEOUT REMARKS']=='FOUND')]",
                                                    "stmt_date = datetime.strftime(payload['statementDate'], '%y%m%d')",
                                                    "df.reset_index(drop=True, inplace = True)",
                                                    "df['seq'] = df.index + 1",
                                                    "df['seq'] = df['seq'].astype(str).str.rjust(6, '0')",
                                                    "df['BankAdjRef'] = 'BEN/REM/C/' + stmt_date + df['seq']",
                                                    "df['Flag']='DRC'", "df['FileName']='IOB_Debit_Confirm_Report.csv'",
                                                    "df['Reason']='104'", "df['SpecifyOther']='Success'",
                                                    # "df['Transaction Date']=df['Transaction Date'].dt.strftime('%d-%m-%Y')",
                                                    "df=df.rename(columns={'Transaction Date':'ShtDat','Transaction Amount':'AdjAmt','Ref Number':'ShSer','ACCOUNT NUMBER':'ShCrd'})",
                                                    "df=df[['BankAdjRef','Flag','ShtDat','AdjAmt','ShSer','ShCrd','FileName','Reason','SpecifyOther']]"
                                                ],
                                                sourceTag="NPCICBSTIME")],
                                  resultKey='IOB_Debit_Confirm', writeToFile=True,
                                  reportName='IOB_Debit_Confirm'))
    #
    # elem11 = dict(type="ExpressionEvaluator",
    #               properties=dict(source="npcitotal",
    #                               expressions=["df = df[(df['Response Code'] != '00')&(df['Response Code'] != 'RB')]"],
    #                               resultkey='npcifailed'))
    elem16 = dict(type="ExpressionEvaluator",
                  properties=dict(source="results.CBS", expressions=["df = df[(df['Self Matched'] != 'Matched')]"],
                                  resultkey='cbsexcluderev'))
    elem17 = dict(type='VLookup',
                  properties=dict(data='npciRBdf', lookup='cbsexcluderev', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'IN CBS': 'FOUND'}, resultkey="npcifaileddf"))

    elem18 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='npcifaileddf', filterConditions=[
                      "df=df[df['IN CBS']=='FOUND']",
                      "stmt_date = datetime.strftime(payload['statementDate'], '%y%m%d')",
                      "df.reset_index(drop=True, inplace = True)",
                      "df['seq'] = df.index + 1",
                      "df['seq'] = df['seq'].astype(str).str.rjust(6, '0')",
                      "df['BankAdjRef'] = 'BEN/REM/C/' + stmt_date + df['seq']",
                      "df['Flag']='DRC'", "df['FileName']='IOB_RetCA_ReversalConfirm_Report.csv'",
                      "df['Reason']='103'", "df['SpecifyOther']='Manual Reversal'",
                      # "df['Transaction Date']=pd.to_datetime(df['Transaction Date'],'%d-%m-%Y')",
                      # "df['Transaction Date']=df['Transaction Date'].dt.strftime('%d-%m-%Y')",
                      "df=df.rename(columns={'Transaction Date':'ShtDat','Transaction Amount':'AdjAmt','Ref Number':'ShSer','ACCOUNT NUMBER':'ShCrd'})",
                      "df=df[['BankAdjRef','Flag','ShtDat','AdjAmt','ShSer','ShCrd','FileName','Reason','SpecifyOther']]"
                  ],
                                                sourceTag="NPCICBS")],
                                  resultKey='IOB_RetCA_ReversalConfirm', writeToFile=True,
                                  reportName='IOB_RetCA_ReversalConfirm'))
    #
    # elem15 = dict(type='VLookup',
    #               properties=dict(data='npcidf', lookup='cbsexcluderev', dataFields=['Ref Number'],
    #                               lookupFields=['RRN'], markers={'IN CBS': 'FOUND'}, resultkey="npcisuccessdf"))

    elem19 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='npcifaileddf', filterConditions=[
                      "df=df[df['IN CBS']!='FOUND']",
                      "stmt_date = datetime.strftime(payload['statementDate'], '%y%m%d')",
                      "df.reset_index(drop=True, inplace = True)",
                      "df['seq'] = df.index + 1",
                      "df['seq'] = df['seq'].astype(str).str.rjust(6, '0')",
                      "df['BankAdjRef'] = 'BEN/REM/C/' + stmt_date + df['seq']",
                      "df['Flag']='DRC'", "df['FileName']='IOB_ManualDebitConfirm_Report.csv'",
                      "df['Reason']='102'", "df['SpecifyOther']='Manual Reversal'",
                      # "df['Transaction Date']=pd.to_datetime(df['Transaction Date'],'%d-%m-%Y')",
                      # "df['Transaction Date']=df['Transaction Date'].dt.strftime('%d-%m-%Y')",
                      "df=df.rename(columns={'Transaction Date':'ShtDat','Transaction Amount':'AdjAmt','Ref Number':'ShSer','ACCOUNT NUMBER':'ShCrd'})",
                      "df=df[['BankAdjRef','Flag','ShtDat','AdjAmt','ShSer','ShCrd','FileName','Reason','SpecifyOther']]"],
                                                sourceTag="NPCICBS")],
                                  resultKey='IOB_ManualDebitConfirm', writeToFile=True,
                                  reportName='IOB_ManualDebitConfirm'))
    failed=dict(type="ExpressionEvaluator",
                  properties=dict(source="npcitotal", expressions=["df=df[(df['Response Code']!='RB')&(df['Response Code']!='00')]"],
                                  resultkey='npcifailed'))

    debitreversal=dict(type='VLookup',
                  properties=dict(data='npcifailed', lookup='cbsexcluderev', dataFields=['Ref Number'],
                                  lookupFields=['RRN'], markers={'IN CBS': 'FOUND'}, resultkey="npcireversaldf"))
    debitreversalreport=dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='npcireversaldf', filterConditions=[
                      # "df=df[df['IN CBS']!='FOUND']"
                  ],
                                                sourceTag="NPCICBS")],
                                  resultKey='IOB_failed', writeToFile=True,
                                  reportName='IOB_failed'))

    compute_close_bal = dict(type='ComputeClosingBalance', properties=dict(
        sources=[dict(source="results.CBS", sourceTag="CBS",
                      filterConditions=["stmtDate = payload['statementDate']",
                                        "day_of_year = (stmtDate - datetime(stmtDate.year, 1, 1)).days + 1",
                                        "df = df[(df['Value Date'].dt.dayofyear <= day_of_year) & (df['NPCI Match'] == 'UNMATCHED')]"],
                      keyColumn='Tran Amt')]))

    elem20 = dict(type="GenerateReconSummary",
                  properties=dict(
                      sources=[dict(resultKey="NPCI", sourceTag="NPCI", aggrCol=['Transaction Amount']),
                               dict(resultKey="CBS", sourceTag="CBS", aggrCol=['Tran Amt'])], groupbyCol=['STATEMENT_DATE']))

    elem21 = dict(type="DumpData",
                  properties=dict(dumpPath='UPI Outward', matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')


    elements = [elem1, elem2, elem3,elem4, elem5, elem6, elem7, elem8, elem9, elem10, elem11, elem12,elem13,elem14,elem15,elem16,elem17,
                elem18,elem19,failed,debitreversal,debitreversalreport,compute_close_bal,
                elem20,elem21,gen_meta_info]
    f = FlowRunner(elements)
    f.run()
