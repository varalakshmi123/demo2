import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '26-Sep-2019'
    uploadFileName = '09072018.zip'

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="IMPS", compressionType="zip", resultkey='',
                                reconName="IMPS Outward", incremental=True))

    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    "df = df[df['Response Code'].isin(['00','08'])]",
                    "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    load_npci = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                       feedPattern="ACQRPEQT*.*",
                                                       feedParams=dict(type="OUTWARD"),
                                                       feedFilters=npci_filters,
                                                       resultkey="npcidf"))
    drop_npci_inc_data = dict(type="IncrementalLoader",
                              properties=dict(source="npcidf", sourceTag="NPCI", keycolumns=['Ref Number'],
                                              resultkey="npcidf", converters={'Ref Number': 'str'}))

    cbs_filters = ["df=df[~(df['TRANSACTION_DESCRIPTION'].str.contains('|'.join(['IMPS Net','IMPS Remitter'])))]",
                   "df['Ref Number'] = df['TRANSACTION_DESCRIPTION'].str.extract('([0-9]{12})')",
                   "df['Cycle'] = df['FEED_FILE_NAME'].str.extract('([1-4][C|c])').fillna('').str.upper()"]

    load_cbs = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                      feedParams=dict(delimiter="|", skiprows=1,
                                                                      feedformatFile='IMPS_CBS_Structure.csv'),
                                                      feedPattern="IMPS_OUTWARD*.*",
                                                      resultkey="cbsdf", feedFilters=cbs_filters))

    drop_cbs_inc_data = dict(type="IncrementalLoader",
                             properties=dict(source="cbsdf", sourceTag="CBS", keycolumns=['Ref Number'],
                                             resultkey="cbsdf", converters={'Ref Number': 'str'}))

    vlookup_respcode = dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='npcidf', dataFields=['Ref Number'],
                                            lookupFields=['Ref Number'], includeCols=['Response Code'],
                                            resultkey="cbsdf"))

    rev_match = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                     columns=dict(
                                                                         keyColumns=["Ref Number"],
                                                                         amountColumns=['Transaction Amount'],
                                                                         crdrcolumn=["CREDIT_DEBIT"],
                                                                         CreditDebitSign=False),
                                                                     sourceTag="CBS")],
                                                       matchResult="cbsresults"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="npcidf",
                                                    columns=dict(
                                                        keyColumns=["Ref Number", 'Transaction Amount'],
                                                        sumColumns=[],
                                                        matchColumns=["Ref Number", "Transaction Amount"]),
                                                    sourceTag="NPCI"),
                                               dict(source="cbsresults.CBS", columns=dict(
                                                   keyColumns=["Ref Number", 'Transaction Amount'], sumColumns=[],
                                                   matchColumns=["Ref Number", 'Transaction Amount']),
                                                    sourceTag="CBS")],
                                      matchResult="results"))

    compute_close_bal = dict(type='ComputeClosingBalance', properties=dict(
        sources=[dict(source="results.CBS", sourceTag="CBS",
                      filterConditions=["stmtDate = payload['statementDate']",
                                        "day_of_year = (stmtDate - datetime(stmtDate.year, 1, 1)).days + 1",
                                        "df = df[(df['VALUE_DATE'].dt.dayofyear <= day_of_year) & (df['NPCI Match'] == 'UNMATCHED')]"],
                      keyColumn='Transaction Amount')]))

    cbs_post_remarks = dict(type="AddPostMatchRemarks",
                            properties=dict(source='results.CBS', remarkCol='Recon Remarks',
                                            remarks={"matchRemarks": "System Matched",
                                                     "selfRemarks": "Reverse Matched"}, how='all'))

    npci_post_remarks = dict(type="AddPostMatchRemarks",
                             properties=dict(source='results.NPCI', remarkCol='Recon Remarks',
                                             remarks={"matchRemarks": "System Matched",
                                                      "selfRemarks": "Reverse Matched"}, how='all'))

    gen_summary = dict(type="GenerateReconSummary",
                       properties=dict(
                           sources=[dict(resultKey="NPCI", sourceTag="NPCI", aggrCol=['Transaction Amount']),
                                    dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Transaction Amount'])],
                           groupbyCol=['Cycle']))

    gen_rev_report = dict(type="ReportGenerator", properties=dict(
        sources=[
            dict(source='results.CBS', sourceTag="CBS",
                 filterConditions=["df = df[df['Recon Remarks'] == 'Reverse Matched']"])],
        writeToFile=True, reportName='CBS Reversals'))

    exclude_rev_records = dict(type="ExpressionEvaluator",
                               properties=dict(source="results.CBS", resultkey="results.CBS",
                                               expressions=["df = df[df['Recon Remarks'] != 'Reverse Matched']"]))

    dumpdata = dict(type="DumpData", properties=dict(resultkey='reportresults', dumpPath='IMPS Outward'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [init, load_npci, drop_npci_inc_data, load_cbs, drop_cbs_inc_data, vlookup_respcode, rev_match,
                nway_match, compute_close_bal, cbs_post_remarks, npci_post_remarks, gen_summary, gen_rev_report,
                exclude_rev_records, dumpdata, gen_meta_info]

    f = FlowRunner(elements)
    f.run()
