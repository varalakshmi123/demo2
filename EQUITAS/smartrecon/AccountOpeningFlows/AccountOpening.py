import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    df = pandas.read_csv(config.basepath + "Reference/AccountOpening.csv").fillna('')
    crmcolumns = df["CRM"].tolist()
    wizardcolumns = df["Wizard"].tolist()
    cbscolumns = df["CBS"].tolist()

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mft, outputPath="",
                                 uploadFileName=uploadFileName, reconName="AccountOpening", recontype='AccountOpening',
                                 compressionType="zip", resultkey=''))

    cbsloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                       feedPattern="^.*\cbs.xls",
                                                       feedParams={},
                                                       feedFilters=[
                                                           "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                           "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')"],
                                                       resultkey="cbsdf"))

    wizardloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Wizard",
                                                          feedPattern="^.*\wizard.xls",
                                                          feedParams={},
                                                          feedFilters=[
                                                              "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                              "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')"],
                                                          resultkey="wizarddf"))

    crmloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CRM",
                                                       feedPattern="^.*\crm.xls",
                                                       feedParams={},
                                                       feedFilters=[
                                                           "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                           "df['UCIC']=df['UCIC'].astype(str).str.replace('\.(0)*','')"],
                                                       resultkey="crmdf"))

    matcher = dict(type="MergeAndCompare", properties=dict(sources=[dict(source="cbsdf",
                                                                         columns=dict(
                                                                             keyColumns=["Customer ID"],
                                                                             matchColumns=cbscolumns),
                                                                         sourceTag="CBS"),
                                                                    dict(source="crmdf",
                                                                         columns=dict(
                                                                             keyColumns=["UCIC"],
                                                                             matchColumns=crmcolumns),
                                                                         sourceTag="CRM"),
                                                                    dict(source="wizarddf",
                                                                         columns=dict(
                                                                             keyColumns=["Customer ID"],
                                                                             matchColumns=wizardcolumns),
                                                                         sourceTag="WIZARD")],
                                                           matchResult="results"))

    CBS = dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
                                                           expressions=[
                                                               "df['WIZARD Matched Columns'] = df.get('WIZARD Matched Columns','')",
                                                               "df.loc[(df['CRM Matched Columns'].fillna('') ==''),'CRM Match']='UNMATCHED'",
                                                               "df.loc[(df['CRM Matched Columns'].fillna('') !=''),'CRM Match']='MATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('') ==''),'WIZARD Match']='UNMATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')  !=''),'WIZARD Match']='MATCHED'"
                                                           ], resultkey='results.CBS'))

    CRM = dict(type="ExpressionEvaluator", properties=dict(source="results.CRM",
                                                           expressions=[
                                                               "df['WIZARD Matched Columns'] = df.get('WIZARD Matched Columns','')",
                                                               "df.loc[(df['CBS Matched Columns'].fillna('')==''),'CBS Match']='UNMATCHED'",
                                                               "df.loc[(df['CBS Matched Columns'].fillna('')!='') ,'CBS Match']='MATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')==''),'WIZARD Match']='UNMATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')!='') ,'WIZARD Match']='MATCHED'"
                                                           ], resultkey='results.CRM'))

    WIZARD = dict(type="ExpressionEvaluator", properties=dict(source="results.WIZARD",
                                                              expressions=[
                                                                  "df['CRM Matched Columns'] = df.get('WIZARD Matched Columns','')",
                                                                  "df.loc[(df['CBS Matched Columns'].fillna('')==''),'CBS Match']='UNMATCHED'",
                                                                  "df.loc[(df['CBS Matched Columns'].fillna('')!='') ,'CBS Match']='MATCHED'",
                                                                  "df.loc[(df['CRM Matched Columns'].fillna('')==''),'CRM Match']='UNMATCHED'",
                                                                  "df.loc[(df['CRM Matched Columns'].fillna('')!=''),'CRM Match']='MATCHED'",
                                                              ], resultkey='results.WIZARD'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='AccountOpening', matched='any'))

    elements = [elem1, cbsloader, wizardloader, crmloader, matcher, CBS, CRM, WIZARD, gen_meta_info, elem19]

    f = FlowRunner(elements)

    f.run()
