import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas
if __name__ == "__main__":
    stmtdate ='08-Jan-2019'
    uploadFileName ='7-Jan-2019.zip'
    df_mkt = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_mkt.csv")
    ofsaacolumns_mkt = df_mkt["OFSAA"].tolist()
    wizardcolumns_mkt = df_mkt["WIZARD"].tolist()
    df_prod = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_prod.csv")
    ofsaacolumns_prod = df_prod["OFSAA"].tolist()
    wizardcolumns_prod = df_prod["WIZARD"].tolist()
    df_countryrelation = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_countryrelation.csv")
    ofsaacolumns_countryrelation = df_countryrelation["OFSAA"].tolist()
    wizardcolumns_countryrelation = df_countryrelation["WIZARD"].tolist()
    df_customeridentification = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_customeridentification.csv")
    ofsaacolumns_customeridentifiaction = df_customeridentification["OFSAA"].tolist()
    wizardcolumns_customeridentification = df_customeridentification["WIZARD"].tolist()
    df_partyaddress = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partyaddress.csv")
    ofsaacolumns_partyaddress = df_partyaddress["OFSAA"].tolist()
    wizardcolumns_partyaddress = df_partyaddress["WIZARD"].tolist()
    df_partydetails = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partydetails.csv")
    ofsaacolumns_partydetails = df_partydetails["OFSAA"].tolist()
    wizardcolumns_partydetails = df_partydetails["WIZARD"].tolist()
    df_partymaster = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partymaster.csv")
    ofsaacolumns_partymaster = df_partymaster["OFSAA"].tolist()
    wizardcolumns_partymaster = df_partymaster["WIZARD"].tolist()
    df_partyphone = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partyphone.csv")
    ofsaacolumns_partyphone = df_partyphone["OFSAA"].tolist()
    wizardcolumns_partyphone = df_partyphone["WIZARD"].tolist()
    df_partyrelationship = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partyrelationship.csv")
    ofsaacolumns_partyrelationship = df_partyrelationship["OFSAA"].tolist()
    wizardcolumns_partyreationship = df_partyrelationship["WIZARD"].tolist()
    df_partyrolemap = pandas.read_csv(config.basepath + "Reference/OFSAAMatchingColumns_partyrolemap.csv")
    ofsaacolumns_partyrolemap = df_partyrolemap["OFSAA"].tolist()
    wizardcolumns_partyrolemap = df_partyrolemap["WIZARD"].tolist()



    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 reconName="OFSAA_RECON", recontype='OFSAA', disableAllCarryFwd=True,
                                 disableCarryFwd = True,
                                 compressionType="zip", resultkey=''))
    ofsaavswizard_mktserved = dict(type="MultiLoader", properties=dict(sources=[dict(loadType="CSV", source="MKT_SERVED_DAT",
                                                                   feedPattern="STG_CORRES_MKT_SERVED_dat.csv",ifPreloadScript=True,preLoadFunc='OfsaaScript',
                                                                   feedParams=dict(delimiter = ',',feedformatFile = 'OFSA_CORRES_MKT_Structure.csv',skiprows=1),
                                                                   feedFilters='',disableCarryFwd=True, resultkey="ofsaavswizard_mktserved_df"),
                                                              dict(loadType="CSV", source="MKT_SERVED_TXT",
                                                                   feedPattern="STG_CORRES_MKT_SERVED_txt.csv",ifPreloadScript=True,preLoadFunc='OfsaaScript',
                                                                   feedParams=dict( delimiter = ',',feedformatFile = 'OFSA_CORRES_MKT_Structure.csv',skiprows=1),
                                                                   feedFilters='', disableCarryFwd=True,
                                                                   errors=False, resultkey="ofsaavswizard_mktserved_df")]))
    ofsaavswizard_prodserved = dict(type="MultiLoader", properties=dict(sources=[dict(loadType="CSV", source="PROD_SERVED_DAT",
                                                                   feedPattern="STG_CORRES_PROD_SERVED_dat.csv",
                                                                   feedParams=dict(delimiter = ',',feedformatFile = 'OFSA_CORRES_PROD_Structure.csv',skiprows=1),
                                                                   feedFilters=[],disableCarryFwd=True, resultkey="ofsaavswizard_prodserved_df"),

                                                              dict(loadType="CSV", source="PROD_SERVED_TXT",
                                                                   feedPattern="STG_CORRES_PROD_SERVED_txt.csv",
                                                                   feedParams=dict( delimiter = ',',feedformatFile = 'OFSA_CORRES_PROD_Structure.csv',skiprows=1),
                                                                   feedFilters='', disableCarryFwd=True,
                                                                   errors=False, resultkey="ofsaavswizard_prodserved_df")]))
    ofsaavswizard_countryrelation = dict(type="MultiLoader",
                                    properties=dict(sources=[dict(loadType="CSV", source="COUNTRY_RELATION_DAT",
                                                                  feedPattern="STG_CUST_COUNTRY_RELATION_dat.csv",
                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_COUNTRY_RELATION_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  resultkey="ofsaavswizard_countryrelation_df"),
                                                             dict(loadType="CSV", source="COUNTRY_RELATION_TXT",
                                                                  feedPattern="STG_CUST_COUNTRY_RELATION_txt.csv",
                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_COUNTRY_RELATION_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  errors=False,
                                                                  resultkey="ofsaavswizard_countryrelation_df")]))
    ofsaavswizard_customeridentification = dict(type="MultiLoader",
                                    properties=dict(sources=[dict(loadType="CSV", source="CUSTOMER_IDENTIFICATION_DAT",
                                                                  feedPattern="STG_CUSTOMER_IDENTIFCTN_dat.csv",

                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_CUSTOMER_IDENTIFICATION_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  resultkey="ofsaavswizard_customeridentification_df"),
                                                             dict(loadType="CSV", source="CUSTOMER_IDENTIFICATION_TXT",
                                                                  feedPattern="STG_CUSTOMER_IDENTIFCTN_txt.csv",
                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_CUSTOMER_IDENTIFICATION_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  errors=False,
                                                                  resultkey="ofsaavswizard_customeridentification_df")]))
    ofsaavswizard_partyaddress = dict(type="MultiLoader",
                                                properties=dict(
                                                    sources=[dict(loadType="CSV", source="PARTY_ADDRESS_DAT",
                                                                  feedPattern="STG_PARTY_ADDRESS_dat.csv",

                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_PARTY_ADDRESS_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  resultkey="ofsaavswizard_partyaddress_df"),
                                                             dict(loadType="CSV", source="PARTY_ADDRESS_TXT",
                                                                  feedPattern="STG_PARTY_ADDRESS_txt.csv",
                                                                  feedParams=dict(delimiter=',',
                                                                                  feedformatFile='OFSA_PARTY_ADDRESS_Structure.csv',
                                                                                  skiprows=1),
                                                                  feedFilters='', disableCarryFwd=True,
                                                                  errors=False,
                                                                  resultkey="ofsaavswizard_partyaddress_df")]))
    ofsaavswizard_partydetails = dict(type="MultiLoader",
                                      properties=dict(
                                          sources=[dict(loadType="CSV", source="PARTY_DETAILS_DAT",
                                                        feedPattern="STG_PARTY_DETAILS_dat.csv",

                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_DETAILS_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        resultkey="ofsaavswizard_partydetails_df"),
                                                   dict(loadType="CSV", source="PARTY_DETAILS_TXT",
                                                        feedPattern="STG_PARTY_DETAILS_txt.csv",
                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_DETAILS_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        errors=False,
                                                        resultkey="ofsaavswizard_partydetails_df")]))
    ofsaavswizard_partymaster = dict(type="MultiLoader",
                                      properties=dict(
                                          sources=[dict(loadType="CSV", source="PARTY_MASTER_DAT",
                                                        feedPattern="STG_PARTY_MASTER_dat.csv",

                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_MASTER_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        resultkey="ofsaavswizard_partymaster_df"),
                                                   dict(loadType="CSV", source="PARTY_MASTER_TXT",
                                                        feedPattern="STG_PARTY_MASTER_txt.csv",
                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_MASTER_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        errors=False,
                                                        resultkey="ofsaavswizard_partymaster_df")]))
    ofsaavswizard_partyphone = dict(type="MultiLoader",
                                      properties=dict(
                                          sources=[dict(loadType="CSV", source="PARTY_PHONE_DAT",
                                                        feedPattern="STG_PARTY_PHONE_dat.csv",

                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_PHONE_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        resultkey="ofsaavswizard_partyphone_df"),
                                                   dict(loadType="CSV", source="PARTY_PHONE_TXT",
                                                        feedPattern="STG_PARTY_PHONE_txt.csv",
                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_PHONE_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        errors=False,
                                                        resultkey="ofsaavswizard_partyphone_df")]))
    ofsaavswizard_partyrelationship = dict(type="MultiLoader",
                                      properties=dict(
                                          sources=[dict(loadType="CSV", source="PARTY_RELATIONSHIP_DAT",
                                                        feedPattern="STG_PARTY_RELATIONSHIP_dat.csv",

                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_RELATIONSHIP_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        resultkey="ofsaavswizard_partyrelation_df"),
                                                   dict(loadType="CSV", source="PARTY_RELATIONSHIP_TXT",
                                                        feedPattern="STG_PARTY_RELATIONSHIP_txt.csv",
                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_RELATIONSHIP_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        errors=False,
                                                        resultkey="ofsaavswizard_partyrelation_df")]))
    ofsaavswizard_partyrolemap = dict(type="MultiLoader",
                                      properties=dict(
                                          sources=[dict(loadType="CSV", source="PARTY_ROLEMAP_DAT",
                                                        feedPattern="STG_PARTY_ROLE_MAP_dat.csv",

                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_ROLE_MAP_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        resultkey="ofsaavswizard_partyrolemap_df"),
                                                   dict(loadType="CSV", source="PARTY_ROLEMAP_TXT",
                                                        feedPattern="STG_PARTY_ROLE_MAP_txt.csv",
                                                        feedParams=dict(delimiter=',',
                                                                        feedformatFile='OFSA_PARTY_ROLE_MAP_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters='', disableCarryFwd=True,
                                                        errors=False,
                                                        resultkey="ofsaavswizard_partyrolemap_df")]))
    # concat_fst = dict(type="SourceConcat", properties=dict(
    #     sourceList=['ofsaavswizard_mktserved_df','ofsaavswizard_prodserved_df','ofsaavswizard_countryrelation_df','ofsaavswizard_customeridentification_df','ofsaavswizard_partyaddress_df','ofsaavswizard_partydetails_df','ofsaavswizard_partymaster_df','ofsaavswizard_partyphone_df','ofsaavswizard_partyrelationship_df','ofsaavswizard_partyrolemap_df'],
    #     resultKey="wizardofsaa_all"))
    # concat_fst_report = dict(type="ReportGenerator",
    #                      properties=dict(sources=[dict(source="wizardofsaa_all",
    #                                                    filterConditions=[
    #
    #                                                    ],
    #                                                    sourceTag="OFSAAVSWIZARD_ALL")],
    #                                      resultkey='wizardofsaa_all_report', writeToFile=True,
    #                                      reportName='wizardofsaa_all_files'))


    exp_mkt = dict(type="ExpressionEvaluator", properties=dict(source="ofsaavswizard_mktserved_df",
                                                            expressions=["df=df.sort_values(by=['V_PARTY_ID'])"]
                                                            , resultkey='OFSAVSWIZARD_MKT'))
    updatecols_mkt = dict(type='UpdateColumns',
                      properties=dict(source='OFSAVSWIZARD_MKT', columns=wizardcolumns_mkt,
                                      KeyColumns=['V_PARTY_ID']
                                      , resultkey='final_mkt'))

    exp_mkt_report = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='final_mkt',
                                              filterConditions=[
                                                  "df.loc[(df['SOURCE']=='MKT_SERVED_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in MKT_SERVED_TXT'",
                                                  "df.loc[(df['SOURCE']=='MKT_SERVED_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  MKT_SERVED_DAT'",
                                                  "df.loc[(df['SOURCE']=='MKT_SERVED_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in MKT_SERVED_TXT'",
                                                  "df.loc[(df['SOURCE']=='MKT_SERVED_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  MKT_SERVED_DAT'",
                                                  "df.loc[(df['SOURCE']=='MKT_SERVED_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in MKT_SERVED_TXT'",
                                                  "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                  "df = df.sort_values(by = ['V_PARTY_ID'])"

                                              ],
                                              sourceTag="OFS(VS)WIZ_MKTSERVED")], resultKey='cbsvscrm_mkt',
                                writeToFile=True,
                                reportName='OFSVSWIZ_MKTSERVED'))
    exp_prod= dict(type="ExpressionEvaluator", properties=dict(source="ofsaavswizard_prodserved_df",
                                                               expressions=["df=df.sort_values(by=['V_PARTY_ID'])"]
                                                               , resultkey='OFSAVSWIZARD_PROD'))
    updatecols_prod = dict(type='UpdateColumns',
                      properties=dict(source='OFSAVSWIZARD_PROD', columns=wizardcolumns_prod,
                                      KeyColumns=['V_PARTY_ID'],resultkey='final_prod'))

    exp_prod_report = dict(type='ReportGenerator',
                          properties=dict(sources=[dict(source='final_prod',
                                                        filterConditions=[
                                                            "df.loc[(df['SOURCE']=='PROD_SERVED_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PROD_SERVED_TXT'",
                                                            "df.loc[(df['SOURCE']=='PROD_SERVED_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PROD_SERVED_DAT'",
                                                            "df.loc[(df['SOURCE']=='PROD_SERVED_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PROD_SERVED_TXT'",
                                                            "df.loc[(df['SOURCE']=='PROD_SERVED_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PROD_SERVED_DAT'",
                                                            "df.loc[(df['SOURCE']=='PROD_SERVED_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PROD_SERVED_TXT'",
                                                            "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                            "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                        ],
                                                        sourceTag="OFS(VS)WIZ_PRODSERVED")], resultKey='cbsvscrm_prod',
                                          writeToFile=True,
                                          reportName='OFSVSWIZ_PRODSERVED'))
    exp_country_relation = dict(type="ExpressionEvaluator", properties=dict(source="ofsaavswizard_countryrelation_df",
                                                                expressions=["df=df.sort_values(by=['V_PARTY_ID'])"]
                                                                , resultkey='OFSAVSWIZARD_COUNTRY_RELATION'))
    updatecols_country_relation = dict(type='UpdateColumns',
                           properties=dict(source='OFSAVSWIZARD_COUNTRY_RELATION', columns=wizardcolumns_countryrelation,
                                           KeyColumns=['V_PARTY_ID']
                                           , resultkey='final_countryrelation'))

    exp_country_relation_report = dict(type='ReportGenerator',
                           properties=dict(sources=[dict(source='final_countryrelation',
                                                         filterConditions=[
                                                             "df.loc[(df['SOURCE']=='COUNTRY_RELATION_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in COUNTRY_RELATION_TXT'",
                                                             "df.loc[(df['SOURCE']=='COUNTRY_RELATION_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  COUNTRY_RELATION_DAT'",
                                                             "df.loc[(df['SOURCE']=='COUNTRY_RELATION_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in COUNTRY_RELATION_TXT'",
                                                             "df.loc[(df['SOURCE']=='COUNTRY_RELATION_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  COUNTRY_RELATION_DAT'",
                                                             "df.loc[(df['SOURCE']=='COUNTRY_RELATION_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in COUNTRY_RELATION_TXT'",
                                                             "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                             "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                         ],
                                                         sourceTag="OFS(VS)WIZ_COUNTRY")], resultKey='cbsvscrm_countryrelation',
                                           writeToFile=True,
                                           reportName='OFSVSWIZ_COUNTRY'))
    exp_customer_identification = dict(type="ExpressionEvaluator", properties=dict(source="ofsaavswizard_customeridentification_df",
                                                                            expressions=[
                                                                                "df=df.sort_values(by=['V_PARTY_ID'])"]
                                                                            , resultkey='OFSAVSWIZARD_COUNTRY_IDENTIFICATION'))
    updatecols_customer_identification = dict(type='UpdateColumns',
                                       properties=dict(source='OFSAVSWIZARD_COUNTRY_IDENTIFICATION', columns=wizardcolumns_customeridentification,
                                                       KeyColumns=['V_PARTY_ID']
                                                       , resultkey='final_countryidentification'))

    exp_customer_identification_report = dict(type='ReportGenerator',
                                       properties=dict(sources=[dict(source='final_countryidentification',
                                                                     filterConditions=[
                                                                         "df.loc[(df['SOURCE']=='CUSTOMER_IDENTIFICATION_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in CUSTOMER_IDENTIFICATION_TXT'",
                                                                         "df.loc[(df['SOURCE']=='CUSTOMER_IDENTIFICATION_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  CUSTOMER_IDENTIFICATION_DAT'",
                                                                         "df.loc[(df['SOURCE']=='CUSTOMER_IDENTIFICATION_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CUSTOMER_IDENTIFICATION_TXT'",
                                                                         "df.loc[(df['SOURCE']=='CUSTOMER_IDENTIFICATION_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  CUSTOMER_IDENTIFICATION_DAT'",
                                                                         "df.loc[(df['SOURCE']=='CUSTOMER_IDENTIFICATION_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in CUSTOMER_IDENTIFICATION_TXT'",
                                                                         "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                         "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                     ],
                                                                     sourceTag="OFS(VS)WIZ_CUSTOMER")],
                                                       resultKey='cbsvscrm_customeridentification',
                                                       writeToFile=True,
                                                       reportName='OFSVSWIZ_CUSTOMER'))
    exp_party_address = dict(type="ExpressionEvaluator",
                                      properties=dict(source="ofsaavswizard_partyaddress_df",
                                                      expressions=[
                                                          "df=df.sort_values(by=['V_PARTY_ID'])"]
                                                      , resultkey='OFSAVSWIZARD_PARTY_ADDRESS'))
    updatecols_party_address = dict(type='UpdateColumns',
                                             properties=dict(source='OFSAVSWIZARD_PARTY_ADDRESS',
                                                             columns=wizardcolumns_partyaddress,
                                                             KeyColumns=['V_PARTY_ID']
                                                             , resultkey='final_partyaddress'))

    exp_party_address_report = dict(type='ReportGenerator',
                                             properties=dict(sources=[dict(source='final_partyaddress',
                                                                           filterConditions=[
                                                                               "df.loc[(df['SOURCE']=='PARTY_ADDRESS_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_ADDRESS_TXT'",
                                                                               "df.loc[(df['SOURCE']=='PARTY_ADDRESS_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_ADDRESS_DAT'",
                                                                               "df.loc[(df['SOURCE']=='PARTY_ADDRESS_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_ADDRESS_TXT'",
                                                                               "df.loc[(df['SOURCE']=='PARTY_ADDRESS_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_ADDRESS_DAT'",
                                                                               "df.loc[(df['SOURCE']=='PARTY_ADDRESS_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_ADDRESS_TXT'",
                                                                               "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                               "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                           ],
                                                                           sourceTag="OFS(VS)WIZ_PAR_ADDRESS")],
                                                             resultKey='cbsvscrm_partyaddress',
                                                             writeToFile=True,
                                                             reportName='OFSVSWIZ_PAR_ADDRESS'))
    exp_party_details = dict(type="ExpressionEvaluator",
                             properties=dict(source="ofsaavswizard_partydetails_df",
                                             expressions=[
                                                 "df=df.sort_values(by=['V_PARTY_ID'])"]
                                             , resultkey='OFSAVSWIZARD_PARTY_DETAILS'))
    updatecols_party_details= dict(type='UpdateColumns',
                                    properties=dict(source='OFSAVSWIZARD_PARTY_DETAILS',
                                                    columns=wizardcolumns_partydetails,
                                                    KeyColumns=['V_PARTY_ID']
                                                    , resultkey='final_partydetails'))

    exp_party_details_report = dict(type='ReportGenerator',
                                    properties=dict(sources=[dict(source='final_partydetails',
                                                                  filterConditions=[
                                                                      "df.loc[(df['SOURCE']=='PARTY_DETAILS_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_DETAILS_TXT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_DETAILS_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_DETAILS_DAT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_DETAILS_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_DETAILS_TXT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_DETAILS_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_DETAILS_DAT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_DETAILS_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_DETAILS_TXT'",
                                                                      "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                      "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                  ],
                                                                  sourceTag="OFS(VS)WIZ_PAR_DETAILS")],
                                                    resultKey='cbsvscrm_partydetails',
                                                    writeToFile=True,
                                                    reportName='OFSVSWIZ_PAR_DETAILS'))
    exp_party_master = dict(type="ExpressionEvaluator",
                             properties=dict(source="ofsaavswizard_partymaster_df",
                                             expressions=[
                                                 "df=df.sort_values(by=['V_PARTY_ID'])"]
                                             , resultkey='OFSAVSWIZARD_PARTY_MASTER'))
    updatecols_party_master = dict(type='UpdateColumns',
                                    properties=dict(source='OFSAVSWIZARD_PARTY_MASTER',
                                                    columns=wizardcolumns_partymaster,
                                                    KeyColumns=['V_PARTY_ID']
                                                    , resultkey='final_partymaster'))

    exp_party_master_report = dict(type='ReportGenerator',
                                    properties=dict(sources=[dict(source='final_partymaster',
                                                                  filterConditions=[
                                                                      "df.loc[(df['SOURCE']=='PARTY_MASTER_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_MASTER_TXT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_MASTER_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_MASTER_DAT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_MASTER_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_MASTER_TXT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_MASTER_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_MASTER_DAT'",
                                                                      "df.loc[(df['SOURCE']=='PARTY_MASTER_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_MASTER_TXT'",
                                                                      "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                      "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                  ],
                                                                  sourceTag="OFS(VS)WIZ_PAR_MASTER")],
                                                    resultKey='cbsvscrm_partymaster',
                                                    writeToFile=True,
                                                    reportName='OFSVSWIZ_PAR_MASTER'))
    exp_party_phone = dict(type="ExpressionEvaluator",
                            properties=dict(source="ofsaavswizard_partyphone_df",
                                            expressions=[
                                                "df=df.sort_values(by=['V_PARTY_ID'])"]
                                            , resultkey='OFSAVSWIZARD_PARTY_PHONE'))
    updatecols_party_phone = dict(type='UpdateColumns',
                                   properties=dict(source='OFSAVSWIZARD_PARTY_PHONE',
                                                   columns=wizardcolumns_partyphone,
                                                   KeyColumns=['V_PARTY_ID']
                                                   , resultkey='final_partyphone'))

    exp_party_phone_report = dict(type='ReportGenerator',
                                   properties=dict(sources=[dict(source='final_partyphone',
                                                                 filterConditions=[
                                                                     "df.loc[(df['SOURCE']=='PARTY_PHONE_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_PHONE_TXT'",
                                                                     "df.loc[(df['SOURCE']=='PARTY_PHONE_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_PHONE_DAT'",
                                                                     "df.loc[(df['SOURCE']=='PARTY_PHONE_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_PHONE_TXT'",
                                                                     "df.loc[(df['SOURCE']=='PARTY_PHONE_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_PHONE_DAT'",
                                                                     "df.loc[(df['SOURCE']=='PARTY_PHONE_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_PHONE_TXT'",
                                                                     "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                     "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                 ],
                                                                 sourceTag="OFS(VS)WIZ_PAR_PHONE")],
                                                   resultKey='cbsvscrm_partyphone',
                                                   writeToFile=True,
                                                   reportName='OFSVSWIZ_PAR_PHONE'))
    exp_party_relationship = dict(type="ExpressionEvaluator",
                           properties=dict(source="ofsaavswizard_partyrelation_df",
                                           expressions=[
                                               "df=df.sort_values(by=['V_PARTY_ID'])"]
                                           , resultkey='OFSAVSWIZARD_PARTY_REL'))
    updatecols_party_relationship = dict(type='UpdateColumns',
                                  properties=dict(source='OFSAVSWIZARD_PARTY_REL',
                                                  columns=wizardcolumns_partyreationship,
                                                  KeyColumns=['V_PARTY_ID']
                                                  , resultkey='final_partyrelationship'))

    exp_party_relationship_report = dict(type='ReportGenerator',
                                  properties=dict(sources=[dict(source='final_partyrelationship',
                                                                filterConditions=[
                                                                    "df.loc[(df['SOURCE']=='PARTY_RELATIONSHIP_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_RELATIONSHIP_TXT'",
                                                                    "df.loc[(df['SOURCE']=='PARTY_RELATIONSHIP_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_RELATIONSHIP_DAT'",
                                                                    "df.loc[(df['SOURCE']=='PARTY_RELATIONSHIP_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_RELATIONSHIP_TXT'",
                                                                    "df.loc[(df['SOURCE']=='PARTY_RELATIONSHIP_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_RELATIONSHIP_DAT'",
                                                                    "df.loc[(df['SOURCE']=='PARTY_RELATIONSHIP_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_RELATIONSHIP_TXT'",
                                                                    "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                    "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                ],
                                                                sourceTag="OFS(VS)WIZ_PAR_REL")],
                                                  resultKey='cbsvscrm_partyrelationship',
                                                  writeToFile=True,
                                                  reportName='OFSVSWIZ_PAR_REL'))
    exp_party_rolemap = dict(type="ExpressionEvaluator",
                                  properties=dict(source="ofsaavswizard_partyrolemap_df",
                                                  expressions=[
                                                      "df=df.sort_values(by=['V_PARTY_ID'])"]
                                                  , resultkey='OFSAVSWIZARD_PARTY_ROLEMAP'))
    updatecols_party_rolemap = dict(type='UpdateColumns',
                                         properties=dict(source='OFSAVSWIZARD_PARTY_ROLEMAP',
                                                         columns=wizardcolumns_partyrolemap,
                                                         KeyColumns=['V_PARTY_ID']
                                                         , resultkey='final_partyrolemap'))

    exp_party_rolemap_report = dict(type='ReportGenerator',
                                         properties=dict(sources=[dict(source='final_partyrolemap',
                                                                       filterConditions=[
                                                                           "df.loc[(df['SOURCE']=='PARTY_ROLEMAP_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in MKT_SERVED_TXT'",
                                                                           "df.loc[(df['SOURCE']=='PARTY_ROLEMAP_TXT')&(df['Matched']=='Missing'),'Matched']='Missing in  PARTY_ROLEMAP_DAT'",
                                                                           "df.loc[(df['SOURCE']=='PARTY_ROLEMAP_DAT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in PARTY_ROLEMAP_TXT'",
                                                                           "df.loc[(df['SOURCE']=='PARTY_ROLEMAP_TXT')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in  PARTY_ROLEMAP_DAT'",
                                                                           "df.loc[(df['SOURCE']=='PARTY_ROLEMAP_DAT')&(df['Matched']=='Missing'),'Matched']='Missing in PARTY_ROLEMAP_TXT'",
                                                                           "df['V_PARTY_ID'] = df['V_PARTY_ID'].astype(np.int64)",
                                                                           "df = df.sort_values(by = ['V_PARTY_ID'])"

                                                                       ],
                                                                       sourceTag="OFS(VS)WIZ_PAR_ROLEMAP")],
                                                         resultKey='cbsvscrm_partyrolemap',
                                                         writeToFile=True,
                                                         reportName='OFSAVSWIZARD_PAR_ROLEMAP'))

    wizardloader_mktserved = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_MKT",
                                                                 feedPattern="STG_CORRES_MKT_SERVED_dat.csv",
                                                                 feedParams=dict(skiprows=1,
                                                                   feedformatFile='OFSA_CORRES_MKT_Structure.csv'),
                                                                 feedFilters=[],disableCarryFwd = True,
                                                                 resultkey="wizard_mktserveddf"))
    wizard_served_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="wizard_mktserveddf",
                                                                                                                                           filterConditions=[
                                                                                                                                               "df = df.fillna(' ')",
                                                                                                                                                        "df[['FIC_MIS_DATE','V_MARKET_SERVED','D_SERVICE_EFFECTIVE_DATE','D_SERVICE_EXPIRATION_DATE','V_PARTY_ID']] = df[['FIC_MIS_DATE','V_MARKET_SERVED','D_SERVICE_EFFECTIVE_DATE','D_SERVICE_EXPIRATION_DATE','V_PARTY_ID']].astype(str)",
                                                                                                                                                          "df = df.fillna(' ')",

                                                                                                                                                        "df['V_MARKET_SERVED'] = df['V_MARKET_SERVED'].str.replace('[None]*',' ')",
                                                                                                                                               # "print df",
                                                                                                                                               "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in[' ','']]),axis=1)",
                                                                                                                                               # "print df.columns",
                                                                                                                                               "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                                                                                                        ]
                                                                                                                                           , sourceTag = "OFSAA_WIZARD_SERVED")],resultkey='wizard_served',writeToFile = True,reportName = 'WIZ_MANDATORY_SERVED' ))
    wizardloader_prodserved = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_SERVED",
                                                                    feedPattern="STG_CORRES_PROD_SERVED_dat.csv",
                                                                    feedParams=dict(skiprows=1,
                                                                                    feedformatFile='OFSA_CORRES_PROD_Structure.csv'),
                                                                    feedFilters=[],disableCarryFwd = True,
                                                                    resultkey="wizard_mktproddf"))
    wizard_prodserved_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="wizard_mktproddf",
                                                                                      filterConditions=[
                                                                                          "df[['FIC_MIS_DATE','V_PARTY_ID','V_PROD_CODE','D_PROD_OFFER_EXPIRATION_DATE','D_PROD_OFFER_INITIATION_DATE']] = df[['FIC_MIS_DATE','V_PARTY_ID','V_PROD_CODE','D_PROD_OFFER_EXPIRATION_DATE','D_PROD_OFFER_INITIATION_DATE']].astype(str)",

                                                                                          "df = df.fillna(' ')",
                                                                                          "df['V_PROD_CODE'] = df['V_PROD_CODE'].str.replace('[None]*',' ')",
                                                                                          "df['D_PROD_OFFER_EXPIRATION_DATE'] = df['D_PROD_OFFER_EXPIRATION_DATE'].str.replace('[None]*',' ')",
                                                                                         "df['D_PROD_OFFER_INITIATION_DATE'] = df['D_PROD_OFFER_INITIATION_DATE'].str.replace('[None]*',' ')",
                                                                                          # "print df['D_PROD_OFFER_INITIATION_DATE']",
                                                                                          "df = df.fillna(' ')",

                                                                                          "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col]  in [' ','']]),axis=1)",
                                                                                          # "print df",
                                                                                          "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                                      ]
                                                                                      , sourceTag="OFSAA_WIZARD_PROD")],
                                                                        resultkey='wizard_prodserved', writeToFile=True,
                                                                        reportName='WIZARD_PRODSERVED'))
    wizardloader_countryrelation = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_COUNTRY",
                                                                     feedPattern="STG_CUST_COUNTRY_RELATION_dat.csv",
                                                                     feedParams=dict(skiprows=1,
                                                                                     feedformatFile='OFSA_COUNTRY_RELATION_Structure.csv'),
                                                                     feedFilters=[],disableCarryFwd = True,
                                                                     resultkey="wizard_countryrelationdf"))
    wizard_countryrelation_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="wizard_countryrelationdf",
                                                                                      filterConditions=[
                                                                                          "df = df.fillna(' ')",
                                                                                          "df[['FIC_MIS_DATE','V_ISO_COUNTRY_CODE','V_PARTY_ID','V_RELATIONSHIP_TYPE']] = df[['FIC_MIS_DATE','V_ISO_COUNTRY_CODE','V_PARTY_ID','V_RELATIONSHIP_TYPE']].astype(str)",
                                                                                          "df = df.fillna(' ')",
                                                                                          "df['V_RELATIONSHIP_TYPE'] = df['V_RELATIONSHIP_TYPE'].str.replace('[None]*',' ')",
                                                                                          # "print df",
                                                                                          "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",
                                                                                          "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"],
                                                                                      sourceTag="OFSAA_WIZARD_COUNTRYREL")],
                                                                        resultkey='wizard_country', writeToFile=True,
                                                                        reportName='WIZARD_COUNTRYRELATION'))
    wizardloader_customeridentify = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_CUSTOMERIDENTIFY",
                                                                     feedPattern="STG_CUSTOMER_IDENTIFCTN_dat.csv",
                                                                     feedParams=dict(skiprows=1,
                                                                                     feedformatFile='OFSA_CUSTOMER_IDENTIFICATION_Structure.csv'),
                                                                     feedFilters=[],disableCarryFwd = True,
                                                                     resultkey="wizard_customeridentifydf"))
    wizard_customeridentify_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="wizard_customeridentifydf",
                                                                                      filterConditions=[
                                                                                          "df = df.fillna(' ')",
                                                                                          "df[['FIC_MIS_DATE','V_PARTY_ID','V_CUSTOMER_TYPE_CODE','V_DOCUMENT_NAME','V_DOC_REF_NO','V_DOC_TYPE_CODE','V_DATA_JURISDICTION']] = df[['FIC_MIS_DATE','V_PARTY_ID','V_CUSTOMER_TYPE_CODE','V_DOCUMENT_NAME','V_DOC_REF_NO','V_DOC_TYPE_CODE','V_DATA_JURISDICTION']].astype(str)",
                                                                                          "df = df.fillna(' ')",

                                                                                          "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",

                                                                                          "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                                      ] ,
                                                                                      sourceTag="OFSAA_WIZARD_CUSTOMERIDENTIFY")],
                                                                        resultkey='wizard_customer_identidy', writeToFile=True,
                                                                        reportName='WIZARD_CUSTOMERIDENTIFY'))
    wizardloader_partyaddress = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_ADDRESS",
                                                                           feedPattern="STG_PARTY_ADDRESS_dat.csv",
                                                                           feedParams=dict(skiprows=1,
                                                                                           feedformatFile='OFSA_PARTY_ADDRESS_Structure.csv'),
                                                                           feedFilters=[],disableCarryFwd = True,
                                                                           resultkey="wizard_partyaddressdf"))
    wizard_partyaddress_report = dict(type="ReportGenerator",
                                          properties=dict(sources=[dict(source="wizard_partyaddressdf",
                                                                        filterConditions=[
                                                                            "df = df.fillna(' ')",

                                                                            "df =df[['FIC_MIS_DATE','V_PARTY_ID','N_SEQUENCE_NUMBER','V_ADDRESS_LINE1','V_ADDRESS_LINE2','V_ADDRESS_PURPOSE_TYPE_IND','V_CITY','V_ISO_COUNTRY_CODE','V_POSTAL_CODE','V_STATE','FEED_FILE_NAME']]",
                                                                            "df[['FIC_MIS_DATE','V_PARTY_ID','N_SEQUENCE_NUMBER','V_ADDRESS_LINE1','V_ADDRESS_LINE2','V_ADDRESS_PURPOSE_TYPE_IND','V_CITY','V_ISO_COUNTRY_CODE','V_POSTAL_CODE','V_STATE']] = df[['FIC_MIS_DATE','V_PARTY_ID','N_SEQUENCE_NUMBER','V_ADDRESS_LINE1','V_ADDRESS_LINE2','V_ADDRESS_PURPOSE_TYPE_IND','V_CITY','V_ISO_COUNTRY_CODE','V_POSTAL_CODE','V_STATE']].astype(str)",
                                                                            "df['V_POSTAL_CODE'] = df['V_POSTAL_CODE'].str.replace('[None]*',' ')",
                                                                                          "df['V_STATE'] = df['V_STATE'].str.replace('[None]*',' ')",
                                                                            "df = df.fillna(' ')",

                                                                                          # "print df.head()",
                                                                            "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in  [' ','']]),axis=1)",

                                                                            "df = df[['FIC_MIS_DATE','V_PARTY_ID','N_SEQUENCE_NUMBER','V_ADDRESS_LINE1','V_ADDRESS_LINE2','V_ADDRESS_PURPOSE_TYPE_IND','V_CITY','V_ISO_COUNTRY_CODE','V_POSTAL_CODE','V_STATE','FEED_FILE_NAME','Checkforempty']]",
                                                                                          # "print df.head()"

                                                                        ],
                                                                        sourceTag="OFSAA_WIZARD_PARTYADDRESS")],
                                                          resultkey='wizard_party_address', writeToFile=True,
                                                          reportName='WIZARD_PARTY_ADDRESS'))
    wizardloader_partydetails = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_DETAILS",
                                                                      feedPattern="STG_PARTY_DETAILS_dat.csv",
                                                                      feedParams=dict(skiprows=1,
                                                                                      feedformatFile='OFSA_PARTY_DETAILS_Structure.csv'),
                                                                      feedFilters=[],disableCarryFwd = True,
                                                                      resultkey="wizard_partydetailsdf"))
    wizard_partydetails_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[dict(source="wizard_partydetailsdf",
                                                                   filterConditions=[
                                                                       "df = df.fillna(' ')",
                                                                       "df[['FIC_MIS_DATE','V_PARTY_ID','V_PRIMARY_WEALTH_SOURCE','D_FIN_PROF_LAST_UPD_DATE']] = df[['FIC_MIS_DATE','V_PARTY_ID','V_PRIMARY_WEALTH_SOURCE','D_FIN_PROF_LAST_UPD_DATE']].astype(str)",
                                                                       "df = df.fillna(' ')",
                                                                       # "df['V_DOC_REF_NO'] = df['V_DOC_REF_NO'].str.replace('nan',' ')",
                                                                       # "print df",
                                                                       "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",

                                                                       "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                   ],
                                                                   sourceTag="OFSAA_WIZARD_PARTYDETAILS")],
                                                     resultkey='wizard_party_details', writeToFile=True,
                                                     reportName='WIZARD_PARTY_DETAILS'))

    wizardloader_partymaster = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_MASTER",
                                                                       feedPattern="STG_PARTY_MASTER_dat.csv",
                                                                       feedParams=dict(skiprows=1,
                                                                                       feedformatFile='OFSA_PARTY_MASTER_Structure.csv'),
                                                                       feedFilters=[],disableCarryFwd = True,
                                                                       resultkey="wizard_partymasterdf"))
    wizard_partymaster_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[dict(source="wizard_partymasterdf",
                                                                    filterConditions=[
                                                                        "df = df.fillna(' ')",
                                                                        "df[['FIC_MIS_DATE','V_BUSINESS_DOMAIN','V_ADDRESS_COUNTRY_CODE','V_TAX_COUNTRY_CODE','D_START_DATE','V_PARTY_ID','V_TYPE','D_DATE_OF_BIRTH','D_INCORPORATION_DATE','V_PARTY_NAME','V_INDUSTRY_CODE','V_DATA_JURISDICTION','V_BUSS_SEGMENT','V_LAST_NAME','V_MIDDLE_NAME','V_PROFESSION','V_ORG_CONSTITUENT_TYPE','V_NATIONALITY_COUNTRY_CODE','V_PUBLIC_PRIVATE_HELD','V_SEC_CITZN_COUNTRY_CODE','V_TAX_IDENTIFIER','F_PARTY_TAX_ID_FORMAT_IND','V_PARTY_ALIAS','V_NATIONALITY_COUNTRY','F_VIP_IND','V_LEGAL_ENTITY','V_PARTY_PARENT_GROUP_NAME','V_ACQUIRED_CHANNEL']] = df[['FIC_MIS_DATE','V_BUSINESS_DOMAIN','V_ADDRESS_COUNTRY_CODE','V_TAX_COUNTRY_CODE','D_START_DATE','V_PARTY_ID','V_TYPE','D_DATE_OF_BIRTH','D_INCORPORATION_DATE','V_PARTY_NAME','V_INDUSTRY_CODE','V_DATA_JURISDICTION','V_BUSS_SEGMENT','V_LAST_NAME','V_MIDDLE_NAME','V_PROFESSION','V_ORG_CONSTITUENT_TYPE','V_NATIONALITY_COUNTRY_CODE','V_PUBLIC_PRIVATE_HELD','V_SEC_CITZN_COUNTRY_CODE','V_TAX_IDENTIFIER','F_PARTY_TAX_ID_FORMAT_IND','V_PARTY_ALIAS','V_NATIONALITY_COUNTRY','F_VIP_IND','V_LEGAL_ENTITY','V_PARTY_PARENT_GROUP_NAME','V_ACQUIRED_CHANNEL']].astype(str)",
                                                                        "df = df.fillna(' ')",

                                                                        "df = df[['FIC_MIS_DATE','V_BUSINESS_DOMAIN','V_ADDRESS_COUNTRY_CODE','V_TAX_COUNTRY_CODE','D_START_DATE','V_PARTY_ID','V_TYPE','D_DATE_OF_BIRTH','D_INCORPORATION_DATE','V_PARTY_NAME','V_INDUSTRY_CODE','V_DATA_JURISDICTION','V_BUSS_SEGMENT','V_LAST_NAME','V_MIDDLE_NAME','V_PROFESSION','V_ORG_CONSTITUENT_TYPE','V_NATIONALITY_COUNTRY_CODE','V_PUBLIC_PRIVATE_HELD','V_SEC_CITZN_COUNTRY_CODE','V_TAX_IDENTIFIER','F_PARTY_TAX_ID_FORMAT_IND','V_PARTY_ALIAS','V_NATIONALITY_COUNTRY','F_VIP_IND','V_LEGAL_ENTITY','V_PARTY_PARENT_GROUP_NAME','V_ACQUIRED_CHANNEL','FEED_FILE_NAME']]",
                                                                        "df['V_PARTY_PARENT_GROUP_NAME'] = df['V_PARTY_PARENT_GROUP_NAME'].str.replace('[None]*',' ')",
                                                                        "df['V_ACQUIRED_CHANNEL'] = df['V_ACQUIRED_CHANNEL'].str.replace('[None]*',' ')",
                                                                        "df = df.fillna(' ')",
                                                                        "print df.dtypes",
                                                                        "df.to_csv('/home/varun/OFSAA/check_local.csv')",

                                                                        "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",

                                                                        "df = df[['FIC_MIS_DATE','V_BUSINESS_DOMAIN','V_ADDRESS_COUNTRY_CODE','V_TAX_COUNTRY_CODE','D_START_DATE','V_PARTY_ID','V_TYPE','D_DATE_OF_BIRTH','D_INCORPORATION_DATE','V_PARTY_NAME','V_INDUSTRY_CODE','V_DATA_JURISDICTION','V_BUSS_SEGMENT','V_LAST_NAME','V_MIDDLE_NAME','V_PROFESSION','V_ORG_CONSTITUENT_TYPE','V_NATIONALITY_COUNTRY_CODE','V_PUBLIC_PRIVATE_HELD','V_SEC_CITZN_COUNTRY_CODE','V_TAX_IDENTIFIER','F_PARTY_TAX_ID_FORMAT_IND','V_PARTY_ALIAS','V_NATIONALITY_COUNTRY','F_VIP_IND','V_LEGAL_ENTITY','V_PARTY_PARENT_GROUP_NAME','V_ACQUIRED_CHANNEL','FEED_FILE_NAME','Checkforempty']]"

                                                                    ],
                                                                    sourceTag="OFSAA_WIZARD_PARTYMASTER")],
                                                      resultkey='wizard_party_master', writeToFile=True,
                                                      reportName='WIZARD_PARTY_MASTER'))
    wizardloader_partyphone= dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_PHONE",
                                                                      feedPattern="STG_PARTY_PHONE_dat.csv",
                                                                      feedParams=dict(skiprows=1,
                                                                                      feedformatFile='OFSA_PARTY_PHONE_Structure.csv'),
                                                                      feedFilters=[],disableCarryFwd = True,
                                                                      resultkey="wizard_partyphonedf"))
    wizard_partyphone_report = dict(type="ReportGenerator",
                                     properties=dict(sources=[dict(source="wizard_partyphonedf",
                                                                   filterConditions=[
                                                                       "df = df.fillna(' ')",
                                                                       "df[['FIC_MIS_DATE','V_PARTY_ID','N_PHONE_NO','N_SEQUENCE_NUMBER','V_PHONE_PURPOSE_TYPE']] = df[['FIC_MIS_DATE','V_PARTY_ID','N_PHONE_NO','N_SEQUENCE_NUMBER','V_PHONE_PURPOSE_TYPE']].astype(str)",
                                                                       "df = df.fillna(' ')",
                                                                       # "df['V_DOC_REF_NO'] = df['V_DOC_REF_NO'].str.replace('nan',' ')",
                                                                       # "print df",
                                                                       "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",

                                                                       "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                   ],
                                                                   sourceTag="OFSAA_WIZARD_PARTYPHONE")],
                                                     resultkey='wizard_party_phone', writeToFile=True,
                                                     reportName='WIZARD_PARTY_PHONE'))
    wizardloader_partyrelationship = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_RELATIONSHIP",
                                                                     feedPattern="STG_PARTY_RELATIONSHIP_dat.csv",
                                                                     feedParams=dict(skiprows=1,
                                                                                     feedformatFile='OFSA_PARTY_RELATIONSHIP_Structure.csv'),
                                                                     feedFilters=[],disableCarryFwd = True,
                                                                     resultkey="wizard_partyrelationshipdf"))
    wizard_partyrelationship_report = dict(type="ReportGenerator",
                                    properties=dict(sources=[dict(source="wizard_partyrelationshipdf",
                                                                  filterConditions=[
                                                                      "df = df.fillna(' ')",
                                                                      "df[['FIC_MIS_DATE','V_PARTY_ID','V_RELATED_PARTY_ID','V_RELATIONSHIP_DESC','D_RELATIONSHIP_EFF_DATE','D_RELATIONSHIP_EXP_DATE']] = df[['FIC_MIS_DATE','V_PARTY_ID','V_RELATED_PARTY_ID','V_RELATIONSHIP_DESC','D_RELATIONSHIP_EFF_DATE','D_RELATIONSHIP_EXP_DATE']].astype(str)",
                                                                      "df = df.fillna(' ')",
                                                                      # "df['V_DOC_REF_NO'] = df['V_DOC_REF_NO'].str.replace('nan',' ')",
                                                                      # "print df",
                                                                      "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] in [' ','']]),axis=1)",

                                                                      "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                  ],
                                                                  sourceTag="OFSAA_WIZARD_PARTYRELATIONSHIP")],
                                                    resultkey='wizard_party_relationship', writeToFile=True,
                                                    reportName='wizard_PARTY_RELATION'))
    wizardloader_partyrolemap = dict(type="PreLoader", properties=dict(loadType="CSV", source="WIZARD_PAR_ROLE_MAP",
                                                                            feedPattern="STG_PARTY_ROLE_MAP_dat.csv",
                                                                            feedParams=dict(skiprows=1,
                                                                                            feedformatFile='OFSA_PARTY_ROLE_MAP_Structure.csv'),
                                                                            feedFilters=[],disableCarryFwd = True,
                                                                            resultkey="wizard_partyrolemapdf"))
    wizard_partyrolemap_report = dict(type="ReportGenerator",
                                           properties=dict(sources=[dict(source="wizard_partyrolemapdf",
                                                                         filterConditions=[
                                                                             "df = df.fillna(' ')",
                                                                             "df[['FIC_MIS_DATE','V_PARTY_ID','V_PARTY_STATUS','V_PARTY_ROLE']] = df[['FIC_MIS_DATE','V_PARTY_ID','V_PARTY_STATUS','V_PARTY_ROLE']].astype(str)",
                                                                             "df = df.fillna(' ')",
                                                                             # "df['V_DOC_REF_NO'] = df['V_DOC_REF_NO'].str.replace('nan',' ')",
                                                                             # "print df",
                                                                             "df['Checkforempty']=df.apply(lambda x : ','.join([col for col,val in x.to_dict().iteritems() if x[col] ==' ']),axis=1)",

                                                                             "df = df[['V_PARTY_ID','FIC_MIS_DATE','Checkforempty','FEED_FILE_NAME']]"

                                                                         ],
                                                                         sourceTag="OFSAA_WIZARD_PARTYROLEMAP")],
                                                           resultkey='wizard_party_rolemap', writeToFile=True,
                                                           reportName='WIZARD_PARTY_ROLEMAP'))
    concat = dict(type="SourceConcat", properties=dict(sourceList=['custom_reports.wizard_served','custom_reports.wizard_prodserved','custom_reports.wizard_country','custom_reports.wizard_customer_identify','custom_reports.wizard_party_address','custom_reports.wizard_party_details','custom_reports.wizard_party_master','custom_reports.wizard_party_phone','custom_reports.wizard_party_relationship','custom_reports.wizard_party_rolemap'], resultKey="wizard_all"))
    concat_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[dict(source="wizard_all",
                                                                    filterConditions=[

                                                                    ],
                                                                    sourceTag="OFSAA_WIZARD_ALL")],
                                                      resultkey='wizard_all', writeToFile=True,
                                                      reportName='wizard_all_files'))

    ofsaaloader_mktserved = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_MKT",
                                                                    feedPattern="STG_CORRES_MKT_SERVED_txt.csv",
                                                                    feedParams=dict(skiprows=1,
                                                                                    feedformatFile='OFSA_CORRES_MKT_Structure.csv'),
                                                                    feedFilters=[], disableCarryFwd=True,
                                                                    resultkey="ofsaa_mktserveddf"))
    ofsaaloader_prodserved = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_SERVED",
                                                                     feedPattern="STG_CORRES_PROD_SERVED_txt.csv",
                                                                     feedParams=dict(skiprows=1,
                                                                                     feedformatFile='OFSA_CORRES_PROD_Structure.csv'),
                                                                     feedFilters=[], disableCarryFwd=True,
                                                                     resultkey="ofsaa_mktproddf"))
    ofsaaloader_countryrelation = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_COUNTRY",
                                                                          feedPattern="STG_CUST_COUNTRY_RELATION_txt.csv",
                                                                          feedParams=dict(skiprows=1,
                                                                                          feedformatFile='OFSA_COUNTRY_RELATION_Structure.csv'),
                                                                          feedFilters=[], disableCarryFwd=True,
                                                                          resultkey="ofsaa_countryrelationdf"))
    ofsaaloader_customeridentify = dict(type="PreLoader",
                                         properties=dict(loadType="CSV", source="OFSAA_CUSTOMERIDENTIFY",
                                                         feedPattern="STG_CUSTOMER_IDENTIFCTN_txt.csv",
                                                         feedParams=dict(skiprows=1,
                                                                         feedformatFile='OFSA_CUSTOMER_IDENTIFICATION_Structure.csv'),
                                                         feedFilters=[], disableCarryFwd=True,
                                                         resultkey="ofsaa_customeridentifydf"))
    ofsaaloader_partyaddress = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_PAR_ADDRESS",
                                                                       feedPattern="STG_PARTY_ADDRESS_txt.csv",
                                                                       feedParams=dict(skiprows=1,
                                                                                       feedformatFile='OFSA_PARTY_ADDRESS_Structure.csv'),
                                                                       feedFilters=[], disableCarryFwd=True,
                                                                       resultkey="ofsaa_partyaddressdf"))

    ofsaaloader_partydetails = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_PAR_DETAILS",
                                                                       feedPattern="STG_PARTY_DETAILS_txt.csv",
                                                                       feedParams=dict(skiprows=1,
                                                                                       feedformatFile='OFSA_PARTY_DETAILS_Structure.csv'),
                                                                       feedFilters=[], disableCarryFwd=True,
                                                                       resultkey="ofsaa_partydetailsdf"))


    ofsaaloader_partymaster = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_PAR_MASTER",
                                                                      feedPattern="STG_PARTY_MASTER_txt.csv",
                                                                      feedParams=dict(skiprows=1,
                                                                                      feedformatFile='OFSA_PARTY_MASTER_Structure.csv'),
                                                                      feedFilters=[], disableCarryFwd=True,
                                                                      resultkey="ofsaa_partymasterdf"))

    ofsaaloader_partyphone = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_PAR_PHONE",
                                                                     feedPattern="STG_PARTY_PHONE_txt.csv",
                                                                     feedParams=dict(skiprows=1,
                                                                                     feedformatFile='OFSA_PARTY_PHONE_Structure.csv'),
                                                                     feedFilters=[], disableCarryFwd=True,
                                                                     resultkey="ofsaa_partyphonedf"))

    ofsaaloader_partyrelationship = dict(type="PreLoader",
                                          properties=dict(loadType="CSV", source="OFSAA_PAR_RELATIONSHIP",
                                                          feedPattern="STG_PARTY_RELATIONSHIP_txt.csv",
                                                          feedParams=dict(skiprows=1,
                                                                          feedformatFile='OFSA_PARTY_RELATIONSHIP_Structure.csv'),
                                                          feedFilters=[], disableCarryFwd=True,
                                                          resultkey="ofsaa_partyrelationshipdf"))

    ofsaaloader_partyrolemap = dict(type="PreLoader", properties=dict(loadType="CSV", source="OFSAA_PAR_ROLE_MAP",
                                                                       feedPattern="STG_PARTY_ROLE_MAP_txt.csv",
                                                                       feedParams=dict(skiprows=1,
                                                                                       feedformatFile='OFSA_PARTY_ROLE_MAP_Structure.csv'),
                                                                       feedFilters=[], disableCarryFwd=True,
                                                                       resultkey="ofsaa_partyrolemapdf"))
    ofsaa_summary = dict(type='DataFiller',
                         properties=dict(template='ofsaa.csv',
                                         alias={"wizard_partyrolemapdf": "df", },
                                         resultKey='ofsaaresult'))
    ofsaa_summary_report = dict(type='ReportGenerator',
                                properties=dict(sources=[dict(source='ofsaaresult',
                                                              filterConditions=[],
                                                              sourceTag="OFSAA")], resultKey='ofasaasummaryreport',
                                                writeToFile=True,
                                                reportName='OFSAA_Recon_Summary'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')
    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='OFSAA_RECON', matched='any'))
    elements = [elem1,ofsaavswizard_mktserved,ofsaavswizard_prodserved,
                ofsaavswizard_countryrelation,ofsaavswizard_customeridentification,
                ofsaavswizard_partyaddress,ofsaavswizard_partydetails,ofsaavswizard_partymaster,
                ofsaavswizard_partyphone,ofsaavswizard_partyrelationship,ofsaavswizard_partyrolemap,

                exp_mkt,updatecols_mkt,exp_mkt_report,
                exp_prod,updatecols_prod,exp_prod_report,
                exp_country_relation,updatecols_country_relation,exp_country_relation_report,
                exp_customer_identification,updatecols_customer_identification,exp_customer_identification_report,
                exp_party_address,updatecols_party_address,exp_party_address_report,
                exp_party_details,updatecols_party_details,exp_party_details_report,
                exp_party_master, updatecols_party_master, exp_party_master_report,
                exp_party_phone, updatecols_party_phone, exp_party_phone_report,
                exp_party_relationship, updatecols_party_relationship, exp_party_relationship_report,
                exp_party_rolemap, updatecols_party_rolemap, exp_party_rolemap_report,
                wizardloader_mktserved,wizard_served_report,
                wizardloader_prodserved,wizard_prodserved_report,
                wizardloader_countryrelation,wizard_countryrelation_report,
                wizardloader_customeridentify,wizard_customeridentify_report,
                wizardloader_partyaddress,wizard_partyaddress_report,
                wizardloader_partydetails,wizard_partydetails_report,
                wizardloader_partymaster,wizard_partymaster_report,
                wizardloader_partyphone,wizard_partyphone_report,
                wizardloader_partyrelationship,wizard_partyrelationship_report,
                wizardloader_partyrolemap,wizard_partyrolemap_report,
                # concat,concat_report,
                ofsaaloader_mktserved,ofsaaloader_prodserved,
                ofsaaloader_countryrelation,ofsaaloader_customeridentify,ofsaaloader_partyaddress,ofsaaloader_partydetails,ofsaaloader_partymaster,ofsaaloader_partyphone,
                ofsaaloader_partyrelationship,ofsaaloader_partyrolemap,
                ofsaa_summary, ofsaa_summary_report,
                gen_meta_info, elem19]
    # matcher,CBS,CRM,WIZARD,gen_meta_info,
    f = FlowRunner(elements)
    f.run()
