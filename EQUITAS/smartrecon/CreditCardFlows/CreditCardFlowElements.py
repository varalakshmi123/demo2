import sys
import pandas

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
#import SettlementReport

#import sys

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    # basePath = "/Users/shivashankar/Desktop/ReconData/AndhraBank/"
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, recontype="CreditCard", compressionType="zip",
                                 resultkey='',
                                 reconName="CreditCard"))


    params={}
    params["feedformatFile"] = "VisaFixedFormatTCR00.csv"

    visafilter = ['df=df[df["Source Amount"].fillna("").str.isdigit()]',
                  'df=df[(df["Transaction Code Qualifier"].fillna("")=="00")&(df["Transaction Code"].fillna("").isin(["05","06","07","15","20","25","27"]))]',
                  'df["Destination Amount"]=df["Destination Amount"].astype(np.float64)',
                  'df["Source Amount"]=df["Source Amount"].astype(np.float64)',
                  'df["Source Amount"]=df["Source Amount"]/100',
                  'df["Destination Amount"]=df["Destination Amount"]/100', 'df["Card Number"]',
                  'df["Card Number"]="`"+df["Card Number"].str[0:16]', 'df["BIN"]=df["Card Number"].str[1:7]',
                  'df["BIN"]=df["BIN"].astype(np.int64)']

    elem2 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISAIC",
                                                   feedPattern="^CTF.*\.TXT",
                                                   feedParams=params,
                                                   feedFilters=visafilter,
                                                   resultkey="visadf"))
    params = {}
    elem3 = dict(type="ReferenceLoader", properties=dict(loadType="CSV", source="BinMaster",
                                                         fileName=["Reference/BinMaster.csv"],
                                                         feedParams=params,
                                                         feedFilters=[],
                                                         resultkey="binmaster"))
    elem4 = dict(type="ReferenceLoader", properties=dict(loadType="CSV", source="CRDRMapping",
                                                         fileName=["Reference/TransCodetoDRCR.csv"],
                                                         feedParams={'feedformatFile': 'Transcodedrcrref.csv',
                                                                     'skiprows': 1},
                                                         feedFilters=[
                                                             'df["Transaction Code"]=df["Transaction Code"].astype(str)',
                                                             'df["Transaction Code"]=df["Transaction Code"].apply(lambda x:"0"+x if len(x)==1 else x)'],
                                                         resultkey="crdrmapping"))

    rupaybinmasterloader = dict(type="ReferenceLoader", properties=dict(loadType="Excel", source="RupayBins",
                                                                        fileName=["Reference/Rupay_Bins.xls"],
                                                                        feedParams=params,
                                                                        feedFilters=[],
                                                                        resultkey="rupaybins"))

    elem5 = dict(type="VLookup", properties=dict(data="visadf", lookup="binmaster",
                                                 dataFields=["BIN"],
                                                 lookupFields=["BIN"],
                                                 includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                 resultkey="visadf"))

    acqissmarker = ["df['Settlement Type']='ISSUER'", "df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'",
                    "df=df[df['Card Type']=='Credit Card']"]
    elem6 = dict(id=4, next=5, type="ExpressionEvaluator",
                 properties=dict(source="visadf", resultkey="visadf", expressions=acqissmarker))

    elem7 = dict(type="VLookup", properties=dict(data="visadf", lookup="crdrmapping",
                                                 dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                 lookupFields=["DataSource", "Settlement Type", "Transaction Code"],
                                                 includeCols=["DrCRIND", "Description"],
                                                 resultkey="visadf"))
    params = {}
    params["feedformatFile"] = "UnbilledFormatAndhraBank.csv"
    elem8 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="UnBilled",
                                                   feedPattern="^UNBILL.*\.xls",
                                                   feedParams=params,
                                                   feedFilters=['df["Amount"]=df["DRAMT"]+df["CRAMT"]',
                                                                'df["Amount"]=df["Amount"].fillna(0)','df.loc[df["DRCR"]=="D","DrCRIND"]="Dr"',
                                                                'df.loc[df["DRCR"]=="C","DrCRIND"]="Cr"'],
                                                   resultkey="unbilledf"))
    params = {}
    filters = ['df.loc[df["Transaction Type"]=="9","DrCRIND"]="Cr"',
               'df.loc[df["Transaction Type"]!="9","DrCRIND"]="Dr"',
               'df["Card Number"]="`"+df["Card Number"].str[0:16]',
               'df["Transaction Amount"]=df["Transaction Amount"].fillna(0).astype(np.float64)',
               'df["Transaction Amount"]=df["Transaction Amount"]/100', 'df["BIN"]=df["Card Number"].str[1:7]',
               'df["BIN"]=df["BIN"].astype(np.int64)']
    elem9 = dict(type="PreLoader", properties=dict(loadType="BTH", source="BTH",
                                                   feedPattern="^.*\.bth",
                                                   feedParams=params,
                                                   feedFilters=filters,
                                                   resultkey="bthdf"))

    bth_bin_update = dict(type="VLookup", properties=dict(data="bthdf", lookup="binmaster",
                                                          dataFields=["BIN"],
                                                          lookupFields=["BIN"],
                                                          includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                          resultkey="bthdf"))

    bth_bin_update_filters = ["df['Settlement Type']='ISSUER'",
                              "df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'"]

    bth_bin_update_eval = dict(type="ExpressionEvaluator",
                               properties=dict(source="bthdf", resultkey="bthdf", expressions=bth_bin_update_filters))

    claimfilter = ['df.loc[df["FEED_FILE_NAME"].str[-3:]=="CLM","DrCRIND"]="Dr"',
                   'df.loc[df["FEED_FILE_NAME"].str[-3:]=="STL","DrCRIND"]="Cr"']
    params = {}
    params["feedformatFile"] = "ATMCLMFormat.csv"
    elem10 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VisaDebitCardClaim",
                                                    feedPattern=".*\.(CLM|STL)",
                                                    feedParams=params,
                                                    feedFilters=claimfilter,
                                                    resultkey="visadcdf"))

    params = {}
    # params["feedformatFile"] = "Reference/ATMCLMFormat.csv"
    rupayfilter = ['df["nPAN"]',
                   'df["nPAN"]="`"+df["nPAN"].str[0:16]',
                   'df["BIN"]=df["nPAN"].str[1:7]',
                   'df["BIN"]=df["BIN"].astype(np.int64)',
                   'df.loc[df["nSetDCInd"]=="D","DrCRIND"]="Dr"',
                   'df.loc[df["nSetDCInd"]=="C","DrCRIND"]="Cr"', 'df["nAmtTxn"]=df["nAmtTxn"].astype(np.float64)',
                   'df["nAmtTxn"]=df["nAmtTxn"]/100',
                   "df['Transaction Code']=df['nMTI'].astype(str)+df['nFunCd'].astype(str)",
                   "df['BTHRRN']=df['nARD'].str[10:-1]"]
    rupayoutfilter = ['df["nPAN"]', 'df["nPAN"]="`"+df["nPAN"].str[0:16]', 'df["BIN"]=df["nPAN"].str[1:7]',
                      'df["BIN"]=df["BIN"].astype(np.int64)',
                      'df["nAmtTxn"]=df["nAmtTxn"].astype(np.float64)', 'df["nAmtTxn"]=df["nAmtTxn"]/100',
                      "df['Transaction Code']=df['nMTI'].astype(str)+df['nFunCd'].astype(str)",
                      "df['BTHRRN']=df['nARD'].str[10:-1]"]
    elem11 = dict(type="PreLoader", properties=dict(loadType="RupayXML", source="RUPAYOC",
                                                    feedPattern="^000AND.*\.xml",
                                                    feedParams=params,
                                                    feedFilters=rupayoutfilter,
                                                    resultkey="rupayoutdf"))

    params = {}
    # params["feedformatFile"] = "Reference/ATMCLMFormat.csv"
    # rupayfilter = {}
    elem12 = dict(type="PreLoader", properties=dict(loadType="RupayXML", source="RUPAYIC",
                                                    feedPattern="^0[1-9]1ANDB.*\.xml",
                                                    feedParams=params,
                                                    feedFilters=rupayfilter,
                                                    resultkey="rupayindf"))

    elem13 = dict(type="VLookup", properties=dict(data="rupayindf", lookup="binmaster",
                                                  dataFields=["BIN"],
                                                  lookupFields=["BIN"],
                                                  includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                  resultkey="rupayindf"))

    elem14 = dict(type="VLookup", properties=dict(data="rupayoutdf", lookup="binmaster",
                                                  dataFields=["BIN"],
                                                  lookupFields=["BIN"],
                                                  includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                  resultkey="rupayoutdf"))

    acqissmarker = ["df['Settlement Type']='ISSUER'", "df.loc[df['Card Type'] == '','Settlement Type']='ACQUIRER'"]
    rupayoutexp = dict(type="ExpressionEvaluator",
                       properties=dict(source="rupayoutdf", resultkey="rupayoutdf", expressions=acqissmarker))
    rupayinexp = dict(type="ExpressionEvaluator",
                       properties=dict(source="rupayindf", resultkey="rupayindf", expressions=acqissmarker))

    rupaylookup = dict(type="VLookup", properties=dict(data="rupayoutdf", lookup="crdrmapping",
                                                       dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                       lookupFields=["DataSource", "Settlement Type",
                                                                     "Transaction Code"],
                                                       includeCols=['DrCRIND',"Description"],
                                                       resultkey="rupayoutdf"))
    rupayinlookup=dict(type="VLookup", properties=dict(data="rupayindf", lookup="crdrmapping",
                                                       dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                       lookupFields=["DataSource", "Settlement Type",
                                                                     "Transaction Code"],
                                                       includeCols=['DrCRIND',"Description"],
                                                       resultkey="rupayindf"))

    params = {}
    params["feedformatFile"] = "VisaFixedFormatTCR00.csv"
    elem15 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISAOG",
                                                    feedPattern="^visaout.*.*",
                                                    feedParams=params,
                                                    feedFilters=visafilter + ["df['BTHRRN']=df['ARN '].str[10:-1]"],
                                                    resultkey="visaoutgoingdf"))

    elem16 = dict(type="VLookup", properties=dict(data="visaoutgoingdf", lookup="binmaster",
                                                  dataFields=["BIN"],
                                                  lookupFields=["BIN"],
                                                  includeCols=["Card Type", "Card Variant", "BIN GL"],
                                                  resultkey="visaoutgoingdf"))

    elem17 = dict(type="ExpressionEvaluator",
                  properties=dict(source="visaoutgoingdf", resultkey="visaoutgoingdf", expressions=acqissmarker))

    elem18 = dict(type="VLookup", properties=dict(data="visaoutgoingdf", lookup="crdrmapping",
                                                  dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                  lookupFields=["DataSource", "Settlement Type", "Transaction Code"],
                                                  includeCols=["DrCRIND", "Description"],
                                                  resultkey="visaoutgoingdf"))

    elem19 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="visadf",
                                                                        columns=dict(keyColumns=["ARN ", "Card Number",
                                                                                                 "DrCRIND"],
                                                                                     sumColumns=["Destination Amount"],
                                                                                     matchColumns=["ARN ",
                                                                                                   "Card Number",
                                                                                                   "DrCRIND",
                                                                                                   "Destination Amount"]),
                                                                        sourceTag="VISA Incoming"),
                                                                   dict(source="unbilledf",
                                                                        columns=dict(
                                                                            keyColumns=["REFNO",
                                                                                        "CARDNO", "DrCRIND"],
                                                                            sumColumns=["Amount"],
                                                                            matchColumns=[
                                                                                "REFNO",
                                                                                "CARDNO",
                                                                                "DrCRIND",
                                                                                "Amount"]),
                                                                        sourceTag="UNBILLED")], matchResult="results"))
    # nApprvlCd
    elem20 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="rupayindf",
                                                                        columns=dict(keyColumns=['nApprvlCd',
                                                                            # "nRRN",
                                                                                                 "nPAN"],
                                                                                     sumColumns=[],
                                                                                     matchColumns=['nApprvlCd',
                                                                                         # "nRRN",
                                                                                                   "nPAN"]),
                                                                        sourceTag="RUPAY_ATM_Incoming"),
                                                                   dict(source="results.UNBILLED",
                                                                        columns=dict(
                                                                            keyColumns=['APPCODE',
                                                                                # "RRN",
                                                                                "CARDNO"],
                                                                            sumColumns=[],
                                                                            matchColumns=['APPCODE',
                                                                                # "RRN",
                                                                                "CARDNO"]),
                                                                        sourceTag="UNBILLED")],
                                                          matchResult="results"))

    # bht_expression = dict(id=4, next=5, type="ExpressionEvaluator",
    #             properties=dict(source="bthdf", resultkey="bthdf", expressions=["df.head().to_csv('/Users/shivashankar/PycharmProjects/smartrecon/bthdf.csv', index = False)"]))

    # visa_expression = dict(id=4, next=5, type="ExpressionEvaluator",
    #                      properties=dict(source="visaoutgoingdf", resultkey="visaoutgoingdf", expressions=["df = df['Settlement Type'] == 'ACQUIRER']","df.head().to_csv('/Users/shivashankar/PycharmProjects/smartrecon/visaoutgoing.csv', index = False)"]))

    # nApprvlCd
    elem21 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="bthdf",
                                                                        columns=dict(keyColumns=["Card Number",
                                                                                                 "Approval Code",'Transaction Amount'],
                                                                                     matchColumns=[
                                                                                         "Card Number",
                                                                                         "Approval Code",'Transaction Amount']),
                                                                        sourceTag="BTH"),
                                                                   dict(source="visaoutgoingdf",
                                                                        columns=dict(
                                                                            keyColumns=[
                                                                                "Card Number", "Authorization Code",'Source Amount'],matchColumns=[
                                                                                "Card Number", "Authorization Code",'Source Amount'
                                                                                ]),
                                                                        sourceTag="VISAOUTGOING")],
                                                          matchResult="results"))


    # '''sumColumns=["nAmtTxn"]''',sumColumns=["Transaction Amount"],
    elem22 = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="results.BTH",
                                                                        columns=dict(keyColumns=["Card Number",
                                                                                                 "Retrieval reference number",
                                                                                                 "Approval Code"],

                                                                                     matchColumns=[
                                                                                         "Card Number",
                                                                                         "Retrieval reference number",
                                                                                         "Approval Code",
                                                                                     ]),
                                                                        sourceTag="BTH"),
                                                                   dict(source="rupayoutdf",
                                                                        columns=dict(
                                                                            keyColumns=["nPAN", "BTHRRN", "nApprvlCd"],matchColumns=[
                                                                                "nPAN", "BTHRRN", "nApprvlCd"]),
                                                                        sourceTag="RUPAY_ATM_OUTGOING")],
                                                          matchResult="results"))

    elem23 = dict(type="NWayMatch",
                  properties=dict(sources=[dict(source="results.BTH",
                                                columns=dict(
                                                    keyColumns=['Approval Code', 'Card Number'],
                                                    sumColumns=[],
                                                    matchColumns=['Approval Code', 'Card Number',
                                                                  # 'Transaction Amount'
                                                                  ]),
                                                subsetfilter=["df['UNBILLED Match'] = 'UNMATCHED'",
                                                              "df=df[(df['VISAOUTGOING Match']=='UNMATCHED')&(df['RUPAY_ATM_OUTGOING Match']=='UNMATCHED')&(df['Card Type']=='Credit Card')]"],
                                                sourceTag="BTH"),
                                           dict(source="results.UNBILLED", columns=dict(
                                               keyColumns=["APPCODE", 'CARDNO'], sumColumns=[],
                                               matchColumns=["APPCODE", 'CARDNO',
                                                             # 'Amount'
                                                             ]),
                                                subsetfilter=["df['BTH Match'] = 'UNMATCHED'",
                                                    "df=df[(df['VISA Incoming Match']=='UNMATCHED')&(df['RUPAY_ATM_Incoming Match']=='UNMATCHED')]"],
                                                sourceTag="UNBILLED")],
                                  matchResult="results"))

    bthamountcoladd = dict(type="VLookup", properties=dict(data="results.BTH", lookup="results.VISAOUTGOING",
                                                        dataFields=["Card Number", "Approval Code"],
                                                        lookupFields=["Card Number", "Authorization Code"],
                                                        includeCols=["Source Amount"],
                                                        resultkey="results.BTH"))
    bthunbillcoladd=dict(type="VLookup", properties=dict(data="results.BTH", lookup="results.UNBILLED",
                                                        dataFields=["Card Number", "Approval Code"],
                                                        lookupFields=['CARDNO',"APPCODE"],
                                                        includeCols=['Amount'],
                                                        resultkey="results.BTH"))
    bthamountdiff = dict(type="ExpressionEvaluator",
                      properties=dict(source="results.BTH", resultkey="results.BTH", expressions=
                      ["df['UNBILL AMOUNT DIFF']=pd.to_numeric(df['Amount'], errors='coerce')-df['Transaction Amount']",
                          "df['VISAOUT AMOUNT DIFF']=pd.to_numeric(df['Source Amount'], errors='coerce')-df['Transaction Amount']"]))
    #
    unbillamountcoladd=dict(type="VLookup", properties=dict(data="results.UNBILLED", lookup="results.RUPAY_ATM_Incoming",
                                                        dataFields=['APPCODE','CARDNO'],
                                                        lookupFields=['nApprvlCd','nPAN'],
                                                        includeCols=["nAmtTxn"],
                                                        resultkey="results.UNBILLED"))
    unbillamountdiff = dict(type="ExpressionEvaluator",
                         properties=dict(source="results.UNBILLED", resultkey="results.UNBILLED", expressions=
                         ["df['AMOUNT DIFF']=df['Amount']-pd.to_numeric(df['nAmtTxn'], errors='coerce')"]))


    #
    # elem24=dict(type="ReportGenerator", properties=dict(
    #     sources=[dict(source='results.BTH',
    #     filterConditions=["df = df[(df['VISAOUTGOING Match'] == 'UNMATCHED')&(df['RUPAY_ATM_OUTGOING Match'] == 'UNMATCHED')&(df['UNBILLED Match'] == 'UNMATCHED')]",
    #                       "df=df[df['Card Type'] == 'Debit card']"],
    #                   sourceTag="BTH")], writeToFile=True, reportName='BTH debit cards data Report'))

    bthgroupby=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.BTH',
                              filterConditions=[
                    "df = df.groupby(['SOURCE', 'Settlement Type', 'Card Type', 'Transaction Type', 'DrCRIND','VISAOUTGOING Match','RUPAY_ATM_OUTGOING Match','UNBILLED Match'])['Transaction Amount'].sum().reset_index()"
                               ],sourceTag="BTH")],writeToFile=True, reportName='BTH tran_type summary'))
    bthgroupby1=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.BTH',
                              filterConditions=[
                    "df=df.groupby(['SOURCE','Settlement Type','Card Type','BIN GL','BIN','DrCRIND','VISAOUTGOING Match','RUPAY_ATM_OUTGOING Match','UNBILLED Match'])['Transaction Amount'].sum().reset_index()"
                               ],sourceTag="BTH")],writeToFile=True, reportName='BTH bin summary'))
    unbilledgroupby=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.UNBILLED',
                              filterConditions=["print df.columns",
                    "df=df.groupby(['SOURCE','BIN','DrCRIND','VISA Incoming Match','RUPAY_ATM_Incoming Match','BTH Match'])['Amount'].sum().reset_index()"
                               ],sourceTag="UNBILLED")],writeToFile=True, reportName='unbilled bin summary'))

    unbilledgroupby1 = dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.UNBILLED',
                                         filterConditions=[
         "df=df.groupby(['SOURCE','TXN_TYPE','DrCRIND','VISA Incoming Match','RUPAY_ATM_Incoming Match','BTH Match'])['Amount'].sum().reset_index()"
                                         ], sourceTag="UNBILLED")], writeToFile=True, reportName='unbilled tran_type summary'))
    visaincoming=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.VISA Incoming',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','Transaction Code','Description','DrCRIND','UNBILLED Match'])['Destination Amount'].sum().reset_index()"
                                         ], sourceTag="VISA Incoming")], writeToFile=True, reportName='VisaIN tran_type summary'))
    visaincoming1=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.VISA Incoming',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','BIN','BIN GL','DrCRIND','UNBILLED Match'])['Destination Amount'].sum().reset_index()"
                                         ], sourceTag="VISA Incoming")], writeToFile=True, reportName='VisaIN bin summary'))
    visaoutgoing=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.VISAOUTGOING',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','BIN','BIN GL','DrCRIND','BTH Match'])['Source Amount'].sum().reset_index()"
                                         ], sourceTag="VISAOUTGOING")], writeToFile=True, reportName='VisaOG bin summary'))
    visaoutgoing1=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.VISAOUTGOING',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','Transaction Code','Description','DrCRIND','BTH Match'])['Source Amount'].sum().reset_index()"
                                         ], sourceTag="VISAOUTGOING")], writeToFile=True, reportName='VisaOG tran_type summary'))
    rupayincoming=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.RUPAY_ATM_Incoming',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','Transaction Code','Description','DrCRIND','UNBILLED Match'])['nAmtTxn'].sum().reset_index()"
                                         ], sourceTag="RUPAY_ATM_Incoming")], writeToFile=True, reportName='RupayIN tran_type summary'))

    rupayincoming1= dict(type="ReportGenerator",
                         properties=dict(
                             sources=[dict(source='results.RUPAY_ATM_Incoming',
                                           filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','BIN','BIN GL','DrCRIND','UNBILLED Match'])['nAmtTxn'].sum().reset_index()"
                                           ], sourceTag="RUPAY_ATM_Incoming")], writeToFile=True,
                             reportName='RupayIN BIN summary'))
    rupayoutgoing=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.RUPAY_ATM_OUTGOING',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','Transaction Code','Description','DrCRIND','BTH Match'])['nAmtTxn'].sum().reset_index()"
                                         ], sourceTag="RUPAY_ATM_OUTGOING")], writeToFile=True, reportName='RupayIN tran_type summary'))
    rupayoutgoing1=dict(type="ReportGenerator",
                       properties=dict(
                           sources=[dict(source='results.RUPAY_ATM_OUTGOING',
                                         filterConditions=[
                "df=df.groupby(['SOURCE','Settlement Type','Card Type','BIN','BIN GL','DrCRIND','BTH Match'])['nAmtTxn'].sum().reset_index()"
                                         ], sourceTag="RUPAY_ATM_OUTGOING")], writeToFile=True, reportName='RupayIN BIN summary'))





    elem25=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.BTH',
                              discardConditions=[
               "df=df[~((df['Card Type']=='Debit card')&(df['VISAOUTGOING Match']=='UNMATCHED')&(df['RUPAY_ATM_OUTGOING Match']=='UNMATCHED')&(df['UNBILLED Match']=='UNMATCHED'))]",
                               ],sourceTag="BTH")],discardRecord=True,writeToFile=True, reportName='BTH debit cards data Report'
                                                  ))
    rupaydis=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.RUPAY_ATM_OUTGOING',
                              discardConditions=[
                                      "df=df[~((df['Card Type']=='Debit card')&(df['BTH Match']=='UNMATCHED'))]",
                                  ],sourceTag="RUPAY_ATM_OUTGOING")],discardRecord=True,writeToFile=True, reportName='RUPAY_ATM_OUTGOING debit cards'
                                                  ))

    elem26 = dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.VISAOUTGOING',
                              discardConditions=[
                                  "df=df[~((df['Card Type']=='Debit card')&(df['BTH Match']=='UNMATCHED'))]"
                                  ],sourceTag="VISAOUTGOING")],discardRecord=True,writeToFile=True, reportName='VISAOUTGOING debit cards'
                                                  ))

    elem27 =dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.VISA Incoming',
                              discardConditions=[
                                  "df=df[~((df['Card Type']=='Debit card')&(df['UNBILLED Match']=='UNMATCHED'))]"
                                  ],sourceTag="Visa Incoming")],discardRecord=True,writeToFile=True, reportName='VISAIncoming debit cards'
                                                  ))
    elem28=dict(type="ReportGenerator",
                properties=dict(
                sources=[dict(source='results.RUPAY_ATM_Incoming',
                              discardConditions=[
                                  "df=df[~((df['Card Type']=='Debit card')&(df['UNBILLED Match']=='UNMATCHED'))]"
                                  ],sourceTag="RUPAY_ATM_Incoming")],discardRecord=True,writeToFile=True, reportName='RUPAY_ATM_Incoming debit cards'
                                                  ))
    summary = dict(type='GenerateReconSummary',
                  properties=dict(sources=[dict(resultKey="BTH", sourceTag='BTH', aggrCol=['Transaction Amount'],matched='any'),
                                           dict(resultKey="UNBILLED", sourceTag='UNBILLED',aggrCol=['Amount'],matched='any'),
                                           dict(resultKey="RUPAY_ATM_OUTGOING", sourceTag='RUPAY_ATM_OUTGOING',aggrCol=['nAmtTxn'],matched='any'),
                                           dict(resultKey="RUPAY_ATM_Incoming", sourceTag='RUPAY_ATM_Incoming',aggrCol=['nAmtTxn'],matched='any'),
                                           dict(resultKey="VISAOUTGOING", sourceTag='VISAOUTGOING',aggrCol=['Source Amount'],matched='any'),
                                           dict(resultKey="VISA Incoming", sourceTag='VISA Incoming',aggrCol=['Destination Amount'],matched='any')],
                                  groupbyCol=['STATEMENT_DATE']))


    elem30 = dict(type="DumpData", properties=dict(matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8, elem9, bth_bin_update,
                bth_bin_update_eval, elem10, elem11, elem12, elem13, elem14, rupayoutexp,rupayinexp, rupaylookup,
                rupayinlookup,elem15,
                elem16, elem17, elem18, elem19, elem20, elem21,elem22, elem23,bthamountcoladd,bthunbillcoladd,bthamountdiff,
                unbillamountcoladd,unbillamountdiff,
                bthgroupby,bthgroupby1,unbilledgroupby,unbilledgroupby1,visaincoming,visaincoming1,visaoutgoing,visaoutgoing1,
                rupayincoming,rupayincoming1,rupayoutgoing,rupayoutgoing1,
                elem25,rupaydis,elem26,elem27,elem28,
                summary,elem30,gen_meta_info]

    f = FlowRunner(elements)

    f.run()
    #    f.result['rupaybins'].to_csv("Output/rupaybins.csv")
    #     for x in f.result['results'].keys():
    #         for col in f.result['results'][x].columns:
    #             f.result['results'][x]=f.result['results'][x].astype(str)
    #         f.result['results'][x].to_csv("Output/" + x + ".csv")
    # for key in f.result.keys():
    #    print key
    #    if isinstance(f.result[key], pd.DataFrame):
    #        f.result[key].to_csv("Output/"+key+".csv")
    # f.result['visaoutgoingdf'].to_csv("Output/visaoutgoingparsed.csv")
    # f.result['unbilledf'].head().to_csv("Output/unbilled.csv")
    # f.result['visadf'].head().to_csv("Output/unbilled.csv")
    # f.result['rupayindf'].to_csv("Output/rupayincoming.csv")
    # print f.result['bthdf']["DrCRIND"].unique()
    # print df.head()
    # print df["Transaction Code"].unique()
    # print df[df['Card Type'] == '']
    # df.loc[df['Card Type'] == '', 'Settlement Type'] = 'ACQUIRER'
    # print df[df['Card Type'] == '']
    # f.result['unbilledf'].to_csv("Output/unbilledtransactions.csv", index=False)
    # df[(df["Transaction Code Qualifier"] == "00") & (df["Transaction Code"].isin(["05", "06", "07", "15", "20", "25", "27"]))]
