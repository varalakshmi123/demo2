import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft',
                                        outputPath="", recontype="ATM", compressionType="zip", reconName="BillDesk",
                                        resultkey='', uploadFileName=uploadFileName))

    gl = dict(type="PreLoader", properties=dict(loadType="Excel", source="GL_Dump",
                                                feedPattern="GL_DUMP2_*.*xlsx",
                                                feedParams=dict(feedformatFile='BillDesk_GL_Structure.csv', skiprows=1),
                                                feedFilters=["df = df[df['MN TXT TXN DESC'] != 'RTGS']",
                                                             "df = df[df['COD GL ACCT'] != '114210121']",
                                                             "df['Account Number'] = df['GL TXT TXN DESC'].str[:12]",
                                                             "df.loc[df['COD ACCT NO'].str.contains('99990208100106'), 'Atom Txn ID'] = df[df['COD ACCT NO'].str.contains('99990208100106')]['GL TXT TXN DESC'].str.split(expand = True)[3]"],
                                                resultkey="gldump_df"))

    # user portal payment
    upp_vendor = dict(type="PreLoader", properties=dict(loadType="Excel", source="UPP",
                                                        feedPattern="EQTS_*.",
                                                        feedParams=dict(feedformatFile='BillDesk_UPP_Structure.csv',
                                                                        skiprows=1),
                                                        feedFilters=["df = df[df['success/failure'] == 'Success']"],
                                                        disableCarryFwd=True,
                                                        resultkey="upp_df"))

    # payment gateway payment
    pgp_vendor = dict(type="PreLoader", properties=dict(loadType="Excel", source="PGP",
                                                        feedPattern="EQB Validation Report*.",
                                                        feedParams=dict(feedformatFile='BillDesk_PGP_Structure.csv',
                                                                        skiprows=1), disableCarryFwd=True,
                                                        feedFilters=[
                                                            "df['Txn Date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')"],
                                                        resultkey="pgp_df"))

    # ATOS vendor
    atos_vendor = dict(type="PreLoader",
                       properties=dict(loadType="CSV", source="ATOS", feedPattern="Worldline_Equitas Claim File_*.",
                                       feedParams=dict(feedformatFile='BillDesk_ATOS_Structure.csv',
                                                       sep=","), feedFilters=[], disableCarryFwd=True,
                                       resultkey="atos_df"))

    # ATOM vendor
    atom_vendor = dict(type="PreLoader",
                       properties=dict(loadType="Excel", source="ATOM", feedPattern="Equitas Bank Trxn*.",
                                       feedParams=dict(feedformatFile='BillDesk_ATOM_Structure.csv',
                                                       skiprows=1, skipfooter=3), disableCarryFwd=True,
                                       feedFilters=["df['Txn Date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')"],
                                       resultkey="atom_df"))

    gl_upp_lookup = dict(type='VLookup',
                         properties=dict(data='gldump_df', lookup='upp_df', dataFields=['REF TXN NO', 'AMT TXN LCY'],
                                         lookupFields=['Ref Txn No', 'Amt'], markers={"Vendor": "UPP"},
                                         resultkey="gldump_df"))

    gl_pgp_lookup = dict(type='VLookup',
                         properties=dict(data='gldump_df', lookup='pgp_df', dataFields=['REF TXN NO', 'AMT TXN LCY'],
                                         lookupFields=['CBS Ref#', 'Amount'], markers={"Vendor": "PGP"},
                                         restoreCols=['Vendor'], resultkey="gldump_df"))

    gl_pgp_account_lookup = dict(type='VLookup',
                                 properties=dict(data='pgp_df', lookup='gldump_df', dataFields=['CBS Ref#'],
                                                 lookupFields=['REF TXN NO'], includeCols=["Account Number"],
                                                 resultkey="pgp_df"))

    gl_atos_lookup = dict(type='VLookup',
                          properties=dict(data='gldump_df', lookup='atos_df', dataFields=['REF TXN NO', 'AMT TXN LCY'],
                                          lookupFields=['BankTransactionReferenceNo', 'TransactionAmount'],
                                          markers={"Vendor": "ATOS"},
                                          restoreCols=['Vendor'], resultkey="gldump_df"))

    gl_atom_lookup = dict(type='VLookup',
                          properties=dict(data='gldump_df', lookup='atom_df', dataFields=['Atom Txn ID', 'AMT TXN LCY'],
                                          lookupFields=['GatewayReferenceNumber', 'Txn Amount'],
                                          markers={"Vendor": "ATOM"},
                                          restoreCols=['Vendor'], resultkey="gldump_df"))

    gl_atom_account_lookup = dict(type='VLookup',
                                  properties=dict(data='atom_df', lookup='gldump_df',
                                                  dataFields=['GatewayReferenceNumber'],
                                                  lookupFields=['Atom Txn ID'],
                                                  includeCols=["Account Number", "REF TXN NO"], resultkey="atom_df"))

    gen_gl_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='gldump_df', filterConditions=[], sourceTag="GL_Dump")], writeToFile=True,
        reportName='GL_Dump'))

    gen_upp_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='upp_df', filterConditions=[
            "df = df[['#Ref','Txn id','Amt','AccountNumber','status','Ref Txn No','Bank']]"], sourceTag="UPP")],
        writeToFile=True,
        reportName='UPP-208100098'))

    gen_pgp_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='pgp_df',
                      filterConditions=["df.loc[df['Account Number'] != '','CBS Transaction Status'] = 'Success'",
                                        "df = df[['Ref#','CBS Ref#','Amount','CBS Transaction Status','Txn Date',"
                                        "'Account Number']]"],
                      sourceTag="PGP")], writeToFile=True,
        reportName='PGP-208100101'))

    gen_atom_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='atom_df', filterConditions=["df.loc[df['Account Number'] != '','Status'] = 'Success'",
                                                          "df = df[['GatewayReferenceNumber','REF TXN NO','Txn Amount',"
                                                          "'Status','Txn Date','Account Number']]"],
                      sourceTag="ATOM")], writeToFile=True, reportName='ATOM-208100106'))

    gen_atos_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='atos_df', filterConditions=[
            'df = df[["GatewayReferenceNumber","BankTransactionReferenceNo","TransactionAmount","Status","TRANSACTIONDATE","ACCNO"]]'],
                      sourceTag="ATOS")], writeToFile=True,
        reportName='ATOS-208100107'))

    gen_glsummary_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='gldump_df',
                      filterConditions=["df.loc[df['Vendor'].isnull(),'Vendor'] = 'Exception Amount'",
                                        "df = df.groupby(['Vendor'])['AMT TXN LCY'].sum().reset_index()"],
                      sourceTag="GL_Summary")], writeToFile=True, reportName='GL_Summary'))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='BillDesk', matched='all'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [inititalizer, gl, upp_vendor, pgp_vendor, atom_vendor, atos_vendor, gl_upp_lookup,
                gl_pgp_lookup, gl_pgp_account_lookup, gl_atom_lookup, gl_atom_account_lookup, gl_atos_lookup,
                gen_gl_report, gen_upp_report, gen_pgp_report, gen_atom_report, gen_atos_report, gen_glsummary_report,
                dumpData, gen_meta_info]

    f = FlowRunner(elements)
    f.run()
