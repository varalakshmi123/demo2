import sys

sys.path.append('/usr/share/nginx/smartrecon/')
import config
from FlowElements import FlowRunner

# Tips and Summary - All Disputes
# Refunds - All Disputes
# Total of Approved Transaction
# Switching fee - 0.60 * No of Approved Transactions
# Interchange income - From DSR summary
# Refund fee - From DSR summary

if __name__ == "__main__":
    stmtdate = '31-Jan-2019'
    uploadFileName = '04_Jan.zip'

    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}

    posswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                       'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                       'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                       'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                      'df["TXN_AMOUNT"] = df["TXN_AMOUNT"].fillna("0.0")',
                       'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                        reconName='ATM POS', uploadFileName=uploadFileName, recontype="ATM",
                                        compressionType="zip", resultkey=''))

    params = {"feedformatFile": "Equitas_ATM_SWITCH_SPreLoadertructure.csv"}
    switchdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                        feedPattern="^EANFY.*\.txt",
                                                        feedParams=params,
                                                        feedFilters=posswitchfilter,
                                                        resultkey="switchposdf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100034"]',
                  'df["TXN_AMOUNT"] = df["TXN_AMOUNT"].str.strip().replace("","0.0")',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0,"CR_DR_IND"]="D"',
                  'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df["_AMOUNT"] = df["TXN_AMOUNT"] / 100.00',
                  "df['_AMOUNT'] = df['_AMOUNT'].astype(str).str.replace('\.[0]*', '')",
                  "df['TRAN_PARTICULARS'] = df['TRAN_PERT'].str.extract('([0-9]+\s+[0-9]+)')",
                  "df['CUST_ACCOUNT_NUM'] = df['TRAN_PARTICULARS'].str.split(expand = True)[1].str[6:]",
                  "df['_DISPUTE_REF'] = df['TRAN_PARTICULARS'].str.split(expand = True)[0].str[-4:]",
                  "df['_DISPUTE_REF'] += df['RRN'].astype(str).str.strip()",
                  "df['_DISPUTE_REF'] += df['_AMOUNT'].astype(str)",
                  "df['_DISPUTE_REF'] = df['_DISPUTE_REF'].astype(str)", "del df['_AMOUNT']"]
    cbsf = ["df = df[df['TXT_TXN_DESC_2'] != 'MISC DEBIT']","df = df[df['TXT_TXN_DESC_2'] != 'MISC CREDIT']",
        "df['TXN_AMOUNT'] = df['TXN_AMOUNT'].fillna('0.0')",
            "df['TXN_AMOUNT'] = df['TXN_AMOUNT'].astype(np.float64)",
            "df.loc[df['TXN_AMOUNT'] <0.0,'CR_DR_IND'] = 'D' ",
            "df['CARD_NUMBER'] = df['TXT_TXN_DESC'].apply(lambda x:x[0:17].strip() if x != '' else '')",
            "df['req_col_cbs'] = df['CARD_NUMBER'].str[-4:].str.strip()+df['REF_DOC_NO']",
            "df['req_col_cbs'] = df['req_col_cbs'].astype(str)",
            "df['TXT_TXN_DESC'] = df['TXT_TXN_DESC'].astype(str)",
            "df['ACCOUNT_NUMBER'] = df['TXT_TXN_DESC'].apply(lambda x:x.split()[1][6:] if x != '' else '')",
            ]

    # params = {"feedformatFile": "CBS_POS_Structure.csv"}
    cbsdata = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                     feedPattern="GL_Dump_*.*",
                                                     feedParams=dict(feedformatFile="CBS_POS_Structure.csv",
                                                                     skiprows=1),
                                                     feedFilters=cbsf,
                                                     resultkey="cbsdf"))


    posdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="NPCIPOS",
                                                     feedPattern="861ES*.*.*|862ES*.*.*",
                                                     feedParams=dict(feedformatFile="861_NPCI_RUPAY_STMT_STRUCT.csv",
                                                                     skiprows=1, skipfooter=1),
                                                     feedFilters=[
                                                         'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)',
                                                         "df['AMOUNT'] = df['TXN_AMOUNT']/100.00",
                                                         'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                                                     "df['RRN'] = df['RRN'].astype(str)",
                                                     "df['req_col_npci'] = df['CARD_NUMBER'].str[-4:].str.strip()+df['RRN']",
                                                         "df['req_col_npci_org'] = df['req_col_npci']",
                                                     "df['req_col_npci'] = df['req_col_npci'].astype(np.int64).astype(str)"],

                                                     resultkey="npciposdf"))

    dsrdata = dict(type="PreLoader",
                   properties=dict(loadType="Excel", source="DSR Report", feedPattern="DSRSummaryReport_*.*xls",
                                   feedParams={}, resultkey="dsrsummarydf", disableCarryFwd=True,
                                   feedFilters=["df.fillna(method ='ffill', inplace = True)"]))
    vlookup_accnos = dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='npciposdf', dataFields=['req_col_cbs'],
                                            lookupFields=['req_col_npci'],
                                            includeCols=['ACQCODE'],
                                            resultkey="cbsdf"))
    cbs_cfData_exp = dict(type="ExpressionEvaluator",
                             properties=dict(source='cbsdf', resultkey='cbsdf',
                                             expressions=["df['req_col_cbs_org']=df['req_col_cbs']",
                                                          "df['req_col_cbs'] = pd.to_numeric(df['req_col_cbs'], errors='coerce')",
                                                 "df['req_col_cbs']=df['req_col_cbs'].astype(np.uint64).astype(str)" ,
                                                 # "df['req_col_cbs']=df['req_col_cbs'].astype(str).str.replace('\.(0)*','')",

                                                          ]))

    cbs_insert = dict(type='MasterDumpHandler',
                                 properties=dict(masterType="ATM_POS", operation="insert", source='cbsdf',
                                                 dropOnRollback=True, timedelta=300,
                                                 insertCols=['req_col_npci']))
    load_disputes = dict(type="PreLoader", properties=dict(loadType="CSV", source="Disputes",
                                                      feedPattern="^All_Disputes*.*",
                                                      feedParams={'skipfooter': 1}, disableCarryFwd=True,
                                                      feedFilters=[
                                                        ],
                                                      resultkey="disputesdf"))


    selfmatch = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                     columns=dict(
                                                                         keyColumns=["REF_DOC_NO",
                                                                                     "CARD_NUMBER"],
                                                                         amountColumns=[
                                                                             "TXN_AMOUNT"],
                                                                         crdrcolumn=[
                                                                             "CR_DR_IND"],
                                                                         CreditDebitSign=True),
                                                                     sourceTag="CBS")], matchResult="iiscbsdf"))


    twowaymatch_new = dict(type="NWayMatch", properties=dict(sources=[
        dict(source="iiscbsdf.CBS",
             columns=dict(
                 keyColumns=["req_col_cbs","TXN_AMOUNT"],
                 sumColumns=[
                     ],
                 matchColumns=[
                     "req_col_cbs",
                     "TXN_AMOUNT"]),
             sourceTag="CBS",
             subsetfilter = [
                             "del df['req_col_npci']",
                             "df['TXN_AMOUNT'] =df['TXN_AMOUNT'].astype(np.float64)"]),
        dict(source="npciposdf",
             columns=dict(
                 keyColumns=[
                             "req_col_npci","AMOUNT"],
                 sumColumns=[
                     ],
                 matchColumns=[
                     "req_col_npci",
                     "AMOUNT"]),
             sourceTag="NPCIPOS",
             subsetfilter = [
                             "df['AMOUNT'] = df['AMOUNT'].astype(np.float64)"])],
        matchResult="results"))

    cbs_match_remarks = dict(type="ExpressionEvaluator",
                           properties=dict(source='results.CBS', resultkey='results.CBS',
                                           expressions=[

                                               "df['Remarks'] = '' ","df['Credit_to'] = '' ",
                                           "df.loc[df['ACQCODE'] == 'A','Remarks'] = 'SUCCESSFUL' ",
                                           "df.loc[df['ACQCODE'] == 'D','Remarks'] = 'UNRECONCILED' ",
                                           "df.loc[df['ACQCODE'] == 'A','Credit_to'] = '208100038' ",
                                           "df.loc[df['ACQCODE'] == 'D','Credit_to'] = df['ACCOUNT_NUMBER']"]))


    disputes = dict(type="PreLoader", properties=dict(loadType="CSV", source="Disputes",
                                                      feedPattern="^All_Disputes*.*",
                                                      feedParams={'skipfooter': 1}, disableCarryFwd=True,
                                                      feedFilters=[
                                                          'df["Dispute Amount"]=df["Dispute Amount"].astype(np.float64)',
                                                          "df['_DISPUTE_REF'] = df['Primary Account Number'].str.split('-',expand = True)[3]",
                                                          "df['_DISPUTE_REF'] += df['Acquirer Reference Data - RRN'].astype(str).str.split('-',expand = True)[1].str.strip()",
                                                          "df['_DISPUTE_REF'] += df['Transaction Amount'].astype(str).str.replace('\.[0]*', '')",
                                                          "df['_DISPUTE_REF'] = df['_DISPUTE_REF'].astype(str)"],
                                                      resultkey="disputesdf"))

    twowaymatch = dict(type="NWayMatch", properties=dict(sources=[
                                                                    dict(source="iiscbsdf.CBS",
                                                                         columns=dict(
                                                                             keyColumns=["RRN",
                                                                                         "CARD_NUMBER"],
                                                                             sumColumns=[
                                                                                 "TXN_AMOUNT"],
                                                                             matchColumns=[
                                                                                 "RRN",
                                                                                 "CARD_NUMBER",
                                                                                 "TXN_AMOUNT"]),
                                                                         sourceTag="CBS"),
                                                                    dict(source="npciposdf",
                                                                         columns=dict(
                                                                             keyColumns=["RRN",
                                                                                         "CARD_NUMBER"],
                                                                             sumColumns=[
                                                                                 "TXN_AMOUNT"],
                                                                             matchColumns=[
                                                                                 "RRN",
                                                                                 "CARD_NUMBER",
                                                                                 "TXN_AMOUNT"]),
                                                                         sourceTag="NPCIPOS")],
                                                           matchResult="results"))

    load_disputes = dict(type='MasterDumpHandler',
                         properties=dict(masterType="ATM_POS", operation="load", resultKey='prev_disputes'))

    vlookup_disputes = dict(type='VLookup',
                            properties=dict(data='disputesdf', lookup='prev_disputes', dataFields=['_DISPUTE_REF'],
                                            lookupFields=['_DISPUTE_REF'],
                                            includeCols=['CUST_ACCOUNT_NUM', 'CR_DR_IND'],
                                            resultkey="gefu_entries"))

    format_gefu_entries = dict(type="ExpressionEvaluator",
                               properties=dict(source='gefu_entries', resultkey='gefu_entries',
                                               expressions=["df = df[df['CUST_ACCOUNT_NUM'].fillna('') != '']",
                                                            "df['Txn Desc'] = df['Card Acceptor Name'] + '/' "
                                                            "+ df['Primary Account Number'].str.split('-', expand=True)[3] + '/' "
                                                            "+ df['Transaction Date']",
                                                            "df.rename(columns = {'Dispute Amount':'Amount', "
                                                            "'CUST_ACCOUNT_NUM':'Account Number', "
                                                            "'CR_DR_IND':'Debit / Credit Code'}, inplace = True)"]))

    save_current_disputes = dict(type='MasterDumpHandler',
                                 properties=dict(masterType="ATM_POS", operation="insert", source='cbsdf',
                                                 dropOnRollback=True, timedelta=60,
                                                 insertCols=['_DISPUTE_REF', 'CUST_ACCOUNT_NUM', 'CR_DR_IND']))

    fill_gefu = dict(type='DataFiller',
                     properties=dict(template='disputes.csv', alias={"dsrsummarydf": "dsr", "disputesdf": 'dispute',
                                                                     'results.NPCIPOS': 'npcipos'},
                                     resultKey='disputes'))

    chargeback_entries = dict(type='ExpressionEvaluator',
                              properties=dict(source='disputesdf', resultkey='chargeback_entries',
                                              expressions=[
                                                  "df = df[df['Function Code and Description'].str.contains('450')]",
                                                  "df['Txn Desc'] = 'CHBK-' + df['Primary Account Number'].str.replace('-[xX]*', 'X') + '/' +"
                                                  "df['Acquirer Reference Data - RRN'].str.split('-', expand = True)[1].str[-4:]",
                                                  "df['Account Number'] = '208100039'",
                                                  "df['Debit / Credit Code'] = 'C'",
                                                  "df['Amount'] = df['Dispute Amount']",
                                                  "df.loc[len(df), ['Txn Desc', 'Account Number', 'Debit / Credit Code', 'Amount']] = "
                                                  "['POS CHBK ' + payload['statementDate'].strftime('%d-%m-%y'), "
                                                  "'208100038', 'D' , df['Amount'].sum()]"]))

    debit_repr_entries = dict(type='ExpressionEvaluator',
                              properties=dict(source='disputesdf', resultkey='debit_repr_entries',
                                              expressions=[
                                                  "df = df[df['Function Code and Description'].str.contains('205')]",
                                                  "df['Txn Desc'] = 'REP-' + df['Primary Account Number'].str.replace('-[xX]*','X') + '/' +"
                                                  "df['Acquirer Reference Data - RRN'].str.split('-', expand = True)[1] + '/' +"
                                                  "pd.to_datetime(df['Transaction Date'],format = '%d-%b-%Y').dt.strftime('%d-%m')",
                                                  "df['Account Number'] = '208100039'",
                                                  "df['Debit / Credit Code'] = 'D'",
                                                  "df['Amount'] = df['Dispute Amount']"]))

    credit_repr_entries = dict(type='ExpressionEvaluator',
                               properties=dict(source='disputesdf', resultkey='credit_repr_entries',
                                               expressions=[
                                                   "df = df[df['Function Code and Description'].str.contains('205')]",
                                                   "df['Txn Desc'] = 'POS REP ' + "
                                                   "pd.to_datetime(df['Dispute Raise Date'], format = '%d-%b-%Y').dt.strftime('%d-%m-%Y')",
                                                   "df['Account Number'] = '208100038'",
                                                   "df['Debit / Credit Code'] = 'C'",
                                                   "df['Amount'] = df['Dispute Amount']"]))

    vlookup_acqcode = dict(type='VLookup',
                           properties=dict(data='results.CBS', lookup='results.NPCIPOS', dataFields=['RRN'],
                                           lookupFields=['RRN'], includeCols=['ACQCODE'],
                                           resultkey="results.CBS"))

    npci_declined_cbscr_rev = dict(type='ExpressionEvaluator',
                                   properties=dict(source='results.CBS',
                                                   expressions=[
                                                       "df = df[(df['ACQCODE']=='D')&(df['NPCIPOS Match']=='MATCHED')]",
                                                       "df['Debit / Credit Code'] = 'C'",
                                                       "df['Txn Desc'] = df['CARD_NUMBER'] + '/' + df['RRN'] + '/REV'",
                                                       "df['Account Number'] = df['CUST_ACCOUNT_NUM']",
                                                       "df['Amount'] = df['TXN_AMOUNT'] / 100.00"],
                                                   resultkey="cbsdeclined_credit_reversals"))

    npci_declined_cbsdr_rev = dict(type='ExpressionEvaluator',
                                   properties=dict(source='results.CBS',
                                                   expressions=[
                                                       "df = df[(df['ACQCODE']=='D')&(df['NPCIPOS Match']=='MATCHED')]",
                                                       "df['Debit / Credit Code'] = 'D'",
                                                       "df['Txn Desc'] = df['CARD_NUMBER'] + '/' + df['RRN'] + '/REV'",
                                                       "df['Account Number'] = '208100034'",
                                                       "df['Amount'] = df['TXN_AMOUNT'] / 100.00"],
                                                   resultkey="cbsdeclined_debit_reversals"))

    npci_approved_cbsdr_rev = dict(type='ExpressionEvaluator',
                                   properties=dict(source='results.CBS',
                                                   expressions=[
                                                       "df=df[(df['ACQCODE'] == 'A') & (df['Self Matched']=='MATCHED')]",
                                                       "df.drop_duplicates(subset = ['RRN'], inplace = True)",
                                                       "df['Debit / Credit Code'] = 'D'",
                                                       "df['Txn Desc'] = df['CARD_NUMBER'] + '/' + df['RRN'] + '/REV'",
                                                       "df['Account Number'] = df['CUST_ACCOUNT_NUM']",
                                                       "df['Amount'] = df['TXN_AMOUNT'] / 100.00"],
                                                   resultkey="cbsapproved_debit_reversals"))

    npci_approved_cbscr_rev = dict(type='ExpressionEvaluator',
                                   properties=dict(source='results.CBS',
                                                   expressions=[
                                                       "df=df[(df['ACQCODE'] == 'A') & (df['Self Matched']=='MATCHED')]",
                                                       "df.drop_duplicates(subset = ['RRN'], inplace = True)",
                                                       "df['Debit / Credit Code'] = 'C'",
                                                       "df['Txn Desc'] = df['CARD_NUMBER'] + '/' + df['RRN'] + '/REV'",
                                                       "df['Account Number'] = '208100038'",
                                                       "df['Amount'] = df['TXN_AMOUNT'] / 100.00"],
                                                   resultkey="cbsapproved_credit_reversals"))

    concat_sources = dict(type="SourceConcat",
                          properties=dict(
                              sourceList=['disputes', 'chargeback_entries', 'gefu_entries', 'debit_repr_entries',
                                          'credit_repr_entries', 'cbsdeclined_credit_reversals',
                                          'cbsdeclined_debit_reversals', 'cbsapproved_debit_reversals',
                                          'cbsapproved_credit_reversals'], resultKey='gefu_entries'))

    generate_gefu = dict(type="GEFUGenerator", properties=dict(source='gefu_entries'))

    switch_post_remarks = dict(type="AddPostMatchRemarks",
                               properties=dict(source='results.Switch', remarkCol='Bank Remarks',
                                               remarks={"matchRemarks": "System Matched",
                                                        "selfRemarks": "Reverse Matched"}, how='all'))

    npci_post_remarks = dict(type="AddPostMatchRemarks",
                             properties=dict(source='results.NPCIPOS', remarkCol='Bank Remarks',
                                             remarks={"matchRemarks": "System Matched",
                                                      "selfRemarks": "Reverse Matched"}, how='all'))

    cbs_post_remarks = dict(type="AddPostMatchRemarks",
                            properties=dict(source='results.CBS', remarkCol='Bank Remarks',
                                            remarks={"matchRemarks": "System Matched",
                                                     "selfRemarks": "Reverse Matched"}, how='all'))

    cbs_exp_remarks = dict(type="ExpressionEvaluator",
                           properties=dict(source='results.CBS', resultkey='results.CBS',
                                           expressions=[
                                               "df.loc[df['TXN_DATE'] < payload['statementDate'], 'Exception Reason'] = 'PREVIOUS DAY TXN. WRONGLY CAPTURED TODAY'",
                                               "df['time_filter'] = pd.to_datetime(df['TXN_TIME'], format='%H:%M:%S')",
                                               "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['time_filter'].dt.hour >= 23), 'Exception Reason'] = 'Cut off transaction'",
                                               "del df['time_filter']",
                                               "exp_reason = df['Exception Reason']",
                                               "del df['Exception Reason']",
                                               "df.insert(len(df.columns),'Exception Reason', exp_reason)"]))

    npci_exp_remarks = dict(type="ExpressionEvaluator",
                            properties=dict(source='results.NPCIPOS', resultkey='results.NPCIPOS',
                                            expressions=["df['TXN_TIME'] = df['TXN_TIME'].fillna('0').astype(np.int64)",
                                                         "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['TXN_TIME'] >= 230000), 'Exception Reason'] = 'Cut off transaction'",
                                                         "exp_reason = df['Exception Reason']",
                                                         "del df['Exception Reason']",
                                                         "df.insert(len(df.columns),'Exception Reason', exp_reason)"]))

    switch_exp_remarks = dict(type="ExpressionEvaluator",
                              properties=dict(source='results.Switch', resultkey='results.Switch',
                                              expressions=["df['TXN_TIME'] = df['TXN_TIME'].astype(np.int64)",
                                                           "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['TXN_TIME'] >= 230000), 'Exception Reason'] = 'Cut off transaction'",
                                                           "exp_reason = df['Exception Reason']",
                                                           "del df['Exception Reason']",
                                                           "df.insert(len(df.columns),'Exception Reason', exp_reason)"]))

    declined_report = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='results.NPCIPOS', filterConditions=["df = df[df['ACQCODE'] == 'D']"], drop=True)],
        writeToFile=True, reportName='NPCI Declined Txn'))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATM POS', matched='all'))
    genMetaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [inititalizer,cbsdata,posdata,dsrdata,vlookup_accnos,cbs_cfData_exp,load_disputes,selfmatch,twowaymatch_new,cbs_match_remarks,dumpData,genMetaInfo]
    # elements = [inititalizer, cbsdata,posdata, dsrdata, selfmatch, twowaymatch, disputes, load_disputes,
    #             vlookup_disputes, format_gefu_entries, save_current_disputes, fill_gefu, chargeback_entries,
    #             debit_repr_entries, credit_repr_entries, vlookup_acqcode, npci_approved_cbscr_rev,
    #             npci_approved_cbsdr_rev, npci_declined_cbscr_rev, npci_declined_cbsdr_rev, concat_sources,
    #             # generate_gefu,
    #          npci_post_remarks, cbs_post_remarks, cbs_exp_remarks,
    #             npci_exp_remarks, declined_report, dumpData,genMetaInfo]

    f = FlowRunner(elements)

    f.run()
