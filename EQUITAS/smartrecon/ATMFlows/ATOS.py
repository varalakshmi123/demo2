import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft',
                                        outputPath="", recontype="ATM", compressionType="zip", reconName="ATOS",
                                        resultkey='', uploadFileName=uploadFileName))

    gl = dict(type="PreLoader", properties=dict(loadType="Excel", source="GL_Dump",
                                                feedPattern="GL_DUMP2_*.*xlsx",
                                                feedParams=dict(feedformatFile='BillDesk_GL_Structure.csv', skiprows=1),
                                                feedFilters=["df = df[df['MN TXT TXN DESC'] != 'RTGS']",
                                                             "df = df[df['COD GL ACCT'] == '208100107']",
                                                             "df['Account Number'] = df['GL TXT TXN DESC'].str[:12]"],
                                                resultkey="gldump_df"))

    # ATOS vendor
    atos_vendor = dict(type="PreLoader",
                       properties=dict(loadType="CSV", source="ATOS", feedPattern="Worldline_Equitas Claim File_*.",
                                       feedParams=dict(feedformatFile='BillDesk_ATOS_Structure.csv',
                                                       sep=","), feedFilters=[], disableCarryFwd=True,
                                       resultkey="atos_df"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="gldump_df",
                                                    columns=dict(
                                                        keyColumns=['REF TXN NO', 'AMT TXN LCY'],
                                                        sumColumns=[],
                                                        matchColumns=['REF TXN NO', 'AMT TXN LCY']),
                                                    sourceTag="GL_Dump"),
                                               dict(source="atos_df", columns=dict(
                                                   keyColumns=['BankTransactionReferenceNo', 'TransactionAmount'],
                                                   sumColumns=[],
                                                   matchColumns=['BankTransactionReferenceNo', 'TransactionAmount']),
                                                    sourceTag="ATOS")],
                                      matchResult="results"))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATOM', matched='all'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [inititalizer, gl, atos_vendor, nway_match, dumpData, gen_meta_info]

    f = FlowRunner(elements)
    f.run()
