import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    sfms_filters = []

    issuerswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                          'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                          'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                          'df["RRNISSUER"]=df["RRNISSUER"].astype(np.int64)',
                          'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                          'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']

    inititalizer = dict(id=1, next=2, type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft',
                                        outputPath="", recontype="ATM", compressionType="", reconName="ATM Issuer",
                                        resultkey='', uploadFileName=uploadFileName))

    params = {}
    params["feedformatFile"] = "Equitas_ATM_SWITCH_Structure.csv"

    switchdata = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                                      feedPattern="^EANFZ.*\.TXT",
                                                                      feedParams=params,
                                                                      feedFilters=issuerswitchfilter,
                                                                      resultkey="switchposdf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100036"]',
                  'df["RRN"] = df["RRN"].astype(np.int64)', 'df["TXN_AMOUNT"] = df["TXN_AMOUNT"].astype(np.float64)',
                  'df.loc[df["TXN_AMOUNT"] < 0, "CR_DR_IND"] = "D"']

    params = {}
    params["feedformatFile"] = "Equitas_ATM_HOST_Structure.csv"
    cbsdata = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                   feedPattern="^H2H.*\.txt",
                                                                   feedParams=params,
                                                                   feedFilters=cbsfilters,
                                                                   resultkey="cbsdf"))

    npciissuer = dict(id=5, next=6, type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                                      feedPattern="ISSR*.*mE.*",
                                                                      feedParams=dict(type="INWARD"),
                                                                      feedFilters=[
                                                                          'df["Ref Number"]=df["Ref Number"].astype(np.int64)'],
                                                                      resultkey="npciissdf"))

    switch_nonfin_txn = dict(type="ExpressionEvaluator",
                             properties=dict(source='switchposdf', resultkey='switchpos_nonfin_df',
                                             expressions=[
                                                 'df=df.loc[(df["TXN_AMOUNT"] ==0) & (df["RESPONSECODE"] =="00")]']))

    switch_rev_txn = dict(type="ExpressionEvaluator",
                          properties=dict(source='switchposdf', resultkey='switch_rev_df',
                                          expressions=['df=df.loc[(df["RESPONSECODE"]!="00")]']))

    switch_succ_txn = dict(type="ExpressionEvaluator",
                           properties=dict(source='switchposdf', resultkey='switchposdf',
                                           expressions=[
                                               'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]']))

    selfmatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                           columns=dict(
                                                                               keyColumns=["RRN",
                                                                                           "CARD_NUMBER"],
                                                                               amountColumns=[
                                                                                   "TXN_AMOUNT"],
                                                                               crdrcolumn=[
                                                                                   "CR_DR_IND"],
                                                                               CreditDebitSign=True),
                                                                           sourceTag="CBS")], matchResult="iiscbsdf"))

    threewaymatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="switchposdf",
                                                                               columns=dict(keyColumns=["RRNISSUER",
                                                                                                        "CARD_NUMBER"],
                                                                                            sumColumns=["TXN_AMOUNT"],
                                                                                            matchColumns=["RRNISSUER",
                                                                                                          "CARD_NUMBER",
                                                                                                          "TXN_AMOUNT"]),
                                                                               sourceTag="Switch"),
                                                                          dict(source="iiscbsdf.CBS",
                                                                               columns=dict(
                                                                                   keyColumns=["RRN",
                                                                                               "CARD_NUMBER"],
                                                                                   sumColumns=[
                                                                                       "TXN_AMOUNT"],
                                                                                   matchColumns=[
                                                                                       "RRN",
                                                                                       "CARD_NUMBER",
                                                                                       "TXN_AMOUNT"]),
                                                                               sourceTag="CBS"),
                                                                          dict(source="npciissdf",
                                                                               columns=dict(
                                                                                   keyColumns=["Ref Number",
                                                                                               "PAN Number"],
                                                                                   sumColumns=[
                                                                                       "Actual Transaction Amount"],
                                                                                   matchColumns=[
                                                                                       "Ref Number",
                                                                                       "PAN Number",
                                                                                       "Actual Transaction Amount"]),
                                                                               sourceTag="NPCIISSUER")],
                                                                 matchResult="results"))

    switch_post_remarks = dict(type="AddPostMatchRemarks",
                               properties=dict(source='results.Switch', remarkCol='Bank Remarks',
                                               remarks={"matchRemarks": "System Matched",
                                                        "selfRemarks": "Reverse Matched"}, how='all'))

    npci_post_remarks = dict(type="AddPostMatchRemarks",
                             properties=dict(source='results.NPCIISSUER', remarkCol='Bank Remarks',
                                             remarks={"matchRemarks": "System Matched",
                                                      "selfRemarks": "Reverse Matched"}, how='all'))

    cbs_post_remarks = dict(type="AddPostMatchRemarks",
                            properties=dict(source='results.CBS', remarkCol='Bank Remarks',
                                            remarks={"matchRemarks": "System Matched",
                                                     "selfRemarks": "Reverse Matched"}, how='all'))

    cbs_remarks = dict(type="ExpressionEvaluator",
                       properties=dict(source='results.CBS', resultkey='results.CBS',
                                       expressions=[
                                           "df.loc[df['TXN_DATE'] < payload['statementDate'], 'Exception Reason'] = 'PREVIOUS DAY TXN. WRONGLY CAPTURED TODAY'",
                                           "df['time_filter'] = pd.to_datetime(df['TXN_TIME'], format='%H:%M:%S')",
                                           "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['time_filter'].dt.hour >= 23), 'Exception Reason'] = 'Cut off transactions'",
                                           "del df['time_filter']"]))

    switch_remarks = dict(type="ExpressionEvaluator",
                          properties=dict(source='results.Switch', resultkey='results.Switch',
                                          expressions=["df['TXN_TIME'] = df['TXN_TIME'].astype(np.int64)",
                                                       "df['TXN_DATE'] = pd.to_datetime(df['TXN_DATE'], format='%y%m%d')",
                                                       "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['TXN_TIME'] >= 230000), 'Exception Reason'] = 'Cut off transaction'"]))

    switch_nonfin_report = dict(type="ReportGenerator",
                                properties=dict(sources=[dict(source='switchpos_nonfin_df', sourceTag="Switch Non-FIN",
                                                              filterConditions=[])], writeToFile=True, writeToDB=False,
                                                reportName='Switch_NON_FIN'))

    switch_rev_report = dict(type="ReportGenerator",
                             properties=dict(sources=[dict(source='switch_rev_df', sourceTag="Switch Reversal",
                                                           filterConditions=[])], writeToFile=True, writeToDB=False,
                                             reportName='Switch_Reversal'))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATM Issuer', matched='all'))

    genMetaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [inititalizer, switchdata, cbsdata, npciissuer, switch_nonfin_txn,
                switch_rev_txn, switch_succ_txn, selfmatch, threewaymatch, switch_post_remarks, npci_post_remarks,
                cbs_post_remarks, cbs_remarks, switch_remarks, switch_nonfin_report, switch_rev_report, dumpData,
                genMetaInfo]

    f = FlowRunner(elements)

    f.run()
