import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft',
                                        outputPath="", recontype="ATM", compressionType="zip", reconName="ATOM",
                                        resultkey='', uploadFileName=uploadFileName))

    gl = dict(type="PreLoader", properties=dict(loadType="Excel", source="GL_Dump",
                                                feedPattern="GL_DUMP2_*.*xlsx",
                                                feedParams=dict(feedformatFile='BillDesk_GL_Structure.csv', skiprows=1),
                                                feedFilters=["df = df[df['MN TXT TXN DESC'] != 'RTGS']",
                                                             "df = df[df['COD GL ACCT'] == '208100106']",
                                                             "df['Account Number'] = df['GL TXT TXN DESC'].str[:12]",
                                                             "df['Atom Txn ID'] = df['GL TXT TXN DESC'].str.split(expand = True)[3]"],
                                                resultkey="gldump_df"))

    # ATOM vendor
    atom_vendor = dict(type="PreLoader",
                       properties=dict(loadType="Excel", source="ATOM", feedPattern="Equitas Bank Trxn*.",
                                       feedParams=dict(feedformatFile='BillDesk_ATOM_Structure.csv',
                                                       skiprows=1, skipfooter=3), disableCarryFwd=True,
                                       feedFilters=["df['Txn Date'] = df['Txn Date'].dt.strftime('%d-%m-%Y')"],
                                       resultkey="atom_df"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="gldump_df",
                                                    columns=dict(
                                                        keyColumns=['Atom Txn ID', "AMT TXN LCY"],
                                                        sumColumns=[],
                                                        matchColumns=['Atom Txn ID', "AMT TXN LCY"]),
                                                    sourceTag="GL_Dump"),
                                               dict(source="atom_df", columns=dict(
                                                   keyColumns=['GatewayReferenceNumber', "Txn Amount"], sumColumns=[],
                                                   matchColumns=['GatewayReferenceNumber', "Txn Amount"]),
                                                    sourceTag="ATOM")],
                                      matchResult="results"))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATOM', matched='all'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [inititalizer, gl, atom_vendor, nway_match, dumpData, gen_meta_info]

    f = FlowRunner(elements)
    f.run()
