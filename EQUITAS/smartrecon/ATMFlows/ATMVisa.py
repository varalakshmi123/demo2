import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    sfms_filters = []

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath='/usr/share/nginx/smartrecon/mft',
                                        outputPath="", recontype="ATM", compressionType="", reconName="ATM VISA",
                                        resultkey='', uploadFileName=uploadFileName))

    visafilter = ['df=df[df["Source Amount"].fillna("").str.isdigit()]',
                  'df=df[(df["Transaction Code Qualifier"].fillna("")=="00")&(df["Transaction Code"].fillna("").isin(["05","06","07","15","20","25","27"]))]',
                  'df["Destination Amount"]=df["Destination Amount"].astype(np.float64)',
                  'df["Source Amount"]=df["Source Amount"].astype(np.float64)',
                  'df["Source Amount"]=df["Source Amount"]/100', 'df["Card Number"]',
                  'df["Card Number"]="`"+df["Card Number"].str[0:16]', 'df["BIN"]=df["Card Number"].str[1:7]',
                  'df["BIN"]=df["BIN"].astype(np.int64)', 'df["Settlement Type"]="ISSUER"']

    params = {}
    params["feedformatFile"] = "VisaFixedFormatTCR00.csv"
    visa = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISAIC",
                                                  feedPattern="^CTF.*\.TXT",
                                                  feedParams=params,
                                                  feedFilters=visafilter,
                                                  resultkey="visadf"))

    params = {}
    crdrreference = dict(type="ReferenceLoader", properties=dict(loadType="CSV", source="CRDRMapping",
                                                                 fileName=["Reference/TransCodetoDRCR.csv"],
                                                                 feedParams=params,
                                                                 feedFilters=[
                                                                     'df["Transaction Code"]=df["Transaction Code"].astype(str)',
                                                                     'df["Transaction Code"]=df["Transaction Code"].apply(lambda x:"0"+x if len(x)==1 else x)'],
                                                                 resultkey="crdrmapping"))

    crdrlookup = dict(type="VLookup", properties=dict(data="visadf", lookup="crdrmapping",
                                                      dataFields=["SOURCE", "Settlement Type", "Transaction Code"],
                                                      lookupFields=["DataSource", "Settlement Type",
                                                                    "Transaction Code"],
                                                      includeCols=["DrCRIND", "Description"],
                                                      resultkey="visadf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)',
                  'df=df[(df["GLCODE"]=="208100061")|(df["GLCODE"]=="208100063")|(df["GLCODE"]=="208100071")|(df["GLCODE"]=="208100072")|(df["GLCODE"]=="208100082")]',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df["TXN_AMOUNT"]=df["TXN_AMOUNT"]/100',
                  'df.loc[df["TXN_AMOUNT"]<0]["CR_DR_IND"]="D"', 'df["RRN"].fillna(0,inplace=True)',
                  'df["RRN"]=df["RRN"].astype(np.int64)',
                  'df["CARD_NUMBER"]="`"+df["CARD_NUMBER"].astype(str)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']

    params = {}
    params["feedformatFile"] = "Equitas_ATM_HOST_Structure.csv"
    cbsdata = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                   feedPattern="^H2H.*\.txt",
                                                                   feedParams=params,
                                                                   feedFilters=cbsfilters,
                                                                   resultkey="cbsdf"))

    selfmatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                           columns=dict(
                                                                               keyColumns=["RRN",
                                                                                           "CARD_NUMBER"],
                                                                               amountColumns=[
                                                                                   "TXN_AMOUNT"],
                                                                               crdrcolumn=[
                                                                                   "CR_DR_IND"],
                                                                               CreditDebitSign=True),
                                                                           sourceTag="CBS")], matchResult="iiscbsdf"))

    twowaymatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="iiscbsdf.CBS",
                                                                             columns=dict(
                                                                                 keyColumns=["CARD_NUMBER",
                                                                                             "TXN_AMOUNT"],

                                                                                 matchColumns=["CARD_NUMBER",
                                                                                               "TXN_AMOUNT"]),
                                                                             sourceTag="CBS"),
                                                                        dict(source="visadf",
                                                                             columns=dict(
                                                                                 keyColumns=["Card Number",
                                                                                             "Source Amount"],

                                                                                 matchColumns=["Card Number",
                                                                                               "Source Amount"]),
                                                                             sourceTag="VISA")], matchResult="results"))

    visa_post_remarks = dict(type="AddPostMatchRemarks",
                             properties=dict(source='results.VISA', remarkCol='Bank Remarks',
                                             remarks={"matchRemarks": "System Matched",
                                                      "selfRemarks": "Reverse Matched"}, how='all'))

    cbs_post_remarks = dict(type="AddPostMatchRemarks",
                            properties=dict(source='results.CBS', remarkCol='Bank Remarks',
                                            remarks={"matchRemarks": "System Matched",
                                                     "selfRemarks": "Reverse Matched"}, how='all'))

    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATM VISA', matched='all'))

    genMetaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [inititalizer, visa, crdrreference, crdrlookup, cbsdata, selfmatch, twowaymatch, visa_post_remarks,
                cbs_post_remarks, dumpData, genMetaInfo]
    f = FlowRunner(elements)
    f.run()
