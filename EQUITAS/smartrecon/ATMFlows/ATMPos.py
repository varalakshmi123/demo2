import sys

sys.path.append('/usr/share/nginx/smartrecon/')
import config
from FlowElements import FlowRunner

# Tips and Summary - All Disputes
# Refunds - All Disputes
# Total of Approved Transaction
# Switching fee - 0.60 * No of Approved Transactions
# Interchange income - From DSR summary
# Refund fee - From DSR summary

if __name__ == "__main__":
    stmtdate = '23-Jul-2019'
    uploadFileName = '01_Jan.zip'

    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}

    posswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                       'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                       'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                       'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                      'df["TXN_AMOUNT"] = df["TXN_AMOUNT"].fillna("0.0")',
                       'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']

    inititalizer = dict(type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                        reconName='ATM POS', uploadFileName=uploadFileName, recontype="ATM",
                                        compressionType="zip", resultkey=''))

    params = {"feedformatFile": "Equitas_ATM_SWITCH_SPreLoadertructure.csv"}
    switchdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                        feedPattern="^EANFY.*\.txt",
                                                        feedParams=params,
                                                        feedFilters=posswitchfilter,
                                                        resultkey="switchposdf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100034"]',
                  'df["TXN_AMOUNT"] = df["TXN_AMOUNT"].str.strip().replace("","0.0")',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0,"CR_DR_IND"]="D"',
                  'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df["_AMOUNT"] = df["TXN_AMOUNT"] / 100.00',
                  "df['_AMOUNT'] = df['_AMOUNT'].astype(str).str.replace('\.[0]*', '')",
                  "df['TRAN_PARTICULARS'] = df['TRAN_PERT'].str.extract('([0-9]+\s+[0-9]+)')",
                  "df['CUST_ACCOUNT_NUM'] = df['TRAN_PARTICULARS'].str.split(expand = True)[1].str[6:]",
                  "df['_DISPUTE_REF'] = df['TRAN_PARTICULARS'].str.split(expand = True)[0].str[-4:]",
                  "df['_DISPUTE_REF'] += df['RRN'].astype(str).str.strip()",
                  "df['_DISPUTE_REF'] += df['_AMOUNT'].astype(str)",
                  "df['_DISPUTE_REF'] = df['_DISPUTE_REF'].astype(str)", "del df['_AMOUNT']"]
    cbsf = ["df = df[df['TXT_TXN_DESC_2'] != 'MISC DEBIT']","df = df[df['TXT_TXN_DESC_2'] != 'MISC CREDIT']",
        "df['TXN_AMOUNT'] = df['TXN_AMOUNT'].fillna('0.0')",
            "df['TXN_AMOUNT'] = df['TXN_AMOUNT'].astype(np.float64)",
            "df.loc[df['TXN_AMOUNT'] <0.0,'CR_DR_IND'] = 'D' ",
            "df['CARD_NUMBER'] = df['TXT_TXN_DESC'].apply(lambda x:x[0:17].strip() if x != '' else '')",
            "df['req_col_match'] = (df['CARD_NUMBER'].str[-4:].str.strip()+df['REF_DOC_NO']).astype(str)",
            "df['req_col_match'] = df['req_col_match'].astype(str)",
            "df['TXT_TXN_DESC'] = df['TXT_TXN_DESC'].astype(str)",
            "df['ACCOUNT_NUMBER'] = df['TXT_TXN_DESC'].apply(lambda x:x.split()[1][6:] if x != '' else '')",
            ]

    # params = {"feedformatFile": "CBS_POS_Structure.csv"}
    cbsdata = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                     feedPattern="GL_Dump_*.*",
                                                     feedParams=dict(feedformatFile="CBS_POS_Structure.csv",
                                                                     skiprows=1),
                                                     feedFilters=cbsf,
                                                     resultkey="cbsdf"))


    posdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="NPCIPOS",
                                                     feedPattern="861ES*.*.*|862ES*.*.*",
                                                     feedParams=dict(feedformatFile="861_NPCI_RUPAY_STMT_STRUCT.csv",
                                                                     skiprows=1, skipfooter=1),
                                                     feedFilters=[
                                                         'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)',
                                                         "df['AMOUNT'] = df['TXN_AMOUNT']/100.00",
                                                         'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                                                     "df['RRN'] = df['RRN'].astype(str)",
                                                     "df['req_col_match_npci'] = df['CARD_NUMBER'].str[-4:].str.strip()+df['RRN']",
                                                         "df['req_col_npci_org'] = df['req_col_match_npci']",
                                                         "df['req_col_match_npci']=df['req_col_match_npci'].astype(str).str.replace('\.(0)*','')",
                                                         "df.to_csv('/tmp/npcipos.csv')"

                                                     # "df['req_col_match_npci'] = df['req_col_match_npci'].astype(np.int64).astype(str)"
                                                         ],

                                                     resultkey="npciposdf"))

    dsrdata = dict(type="PreLoader",
                   properties=dict(loadType="Excel", source="DSR Report", feedPattern="DSRSummaryReport_*.*-1.xls",
                                   feedParams={}, resultkey="dsrsummarydf", disableCarryFwd=True,
                                   feedFilters=["df['Transaction Cycle'] = df['Transaction Cycle'].replace('',np.nan)","df['Status(Approved/Declined)'] = df['Status(Approved/Declined)'].replace('',np.nan)",
                                                "df['Transaction Cycle'].fillna(method='ffill',inplace=True)",
                                                "df['Status(Approved/Declined)'].fillna(method='ffill',limit = 1,inplace=True)",
                                                "df['FILEDATE']=df['FEED_FILE_NAME'].str.split('.').str[0].str.rsplit('-',1).str[0].str.split('-',1).str[1]",
                                                "df['FILEDATE']=pd.to_datetime(df['FILEDATE'],format='%Y-%m-%d')",
                                                "df['FILEDATE'] = df['FILEDATE'].apply(lambda x:x-dt.timedelta(1))",
                                                "df['FILEDATE']  = df['FILEDATE'].astype(str)"]))


    dsrdata2 = dict(type="PreLoader",
                   properties=dict(loadType="Excel", source="DSR Report", feedPattern="DSRSummaryReport_*.*-2.xls",
                                   feedParams={}, resultkey="dsrsummarydf1", disableCarryFwd=True,
                                   feedFilters=["df['Transaction Cycle'] = df['Transaction Cycle'].replace('',np.nan)","df['Status(Approved/Declined)'] = df['Status(Approved/Declined)'].replace('',np.nan)",
                                                "df['Transaction Cycle'].fillna(method='ffill',inplace=True)",
                                                "df['Status(Approved/Declined)'].fillna(method='ffill',limit = 1,inplace=True)",
                                                "df['FILEDATE']=df['FEED_FILE_NAME'].str.split('.').str[0].str.rsplit('-',1).str[0].str.split('-',1).str[1]",
                                                "df['FILEDATE']=pd.to_datetime(df['FILEDATE'],format='%Y-%m-%d')",
                                                "df['FILEDATE']=df['FILEDATE'].astype(str)"]
                                   ))


    vlookup_accnos = dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='npciposdf', dataFields=['req_col_match'],
                                            lookupFieldsFilter = ["df = df[['req_col_match_npci','ACQCODE']]"],
                                            lookupFields=['req_col_match_npci'],
                                            includeCols=['ACQCODE'],
                                            resultkey="cbsdf"))
    cbs_cfData_exp = dict(type="ExpressionEvaluator",
                             properties=dict(source='cbsdf', resultkey='cbsdf',
                                             expressions=["df['req_col_cbs_org']=df['req_col_match']",
                                                          "df['req_col_match'] = pd.to_numeric(df['req_col_match'], errors='coerce')",
                                                 "df['req_col_match']=df['req_col_match'].astype(np.uint64).astype(str)" ,

                                                          ]))

    cbs_insert = dict(type='MasterDumpHandler',
                                 properties=dict(masterType="ATM_POS", operation="insert", source='cbsdf',
                                                 dropOnRollback=True, timedelta=300,
                                                 insertCols=['req_col_match','ACCOUNT_NUMBER','DAT_TXN_POSTING']))



    load_disputes_file = dict(type="PreLoader", properties=dict(loadType="CSV", source="Disputes",
                                                      feedPattern="^All_Disputes*.*",
                                                      feedParams={'skipfooter': 1}, disableCarryFwd=True,
                                                      feedFilters=["df['req_col_match'] = (df['Primary Account Number'].apply(lambda x:x.split('-')[-1] if '-' in x else '')+df['Acquirer Reference Data - RRN'].apply(lambda x:x.split('-')[1] if '-' in x else '')).str.replace(' ','')"

                                                                   ],
                                                      resultkey="disputesdf"))



    dispute_exp = dict(type="ExpressionEvaluator",
                          properties=dict(source='disputesdf', resultkey='disputesdf_master',
                                          expressions=["df=df[['req_col_match']]","df['req_col_match'] = df['req_col_match'].astype(str)" ]))

    get_accno_db =  dict(type='MasterDumpHandler',
                                  properties=dict(masterType="ATM_POS", operation="find", source='cbsdf',
                                                  inputdf='disputesdf_master',
                                                  dropOnRollback=True, resultKey='disputeresult'))


    masterresult_dispute_merge =  dict(type='VLookup',
                            properties=dict(data='disputesdf', lookup='disputeresult', dataFields=['req_col_match'],
                                            lookupFields=['req_col_match'],
                                            includeCols=['ACCOUNT_NUMBER','DAT_TXN_POSTING'],
                                            resultkey="master_result"))


    disputes_org_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source='master_result', filterConditions=["df = df[df['Function Code and Description'].str.contains('|'.join(['205-Re-presentment Raise','450-Chargeback Raise']))]",
                                                                 "df['Remarks_final'] = 'Moved To DIspute-GL' ","del df['ACCOUNT_NUMBER']",
                                                                 "del df['DAT_TXN_POSTING']"],drop=True)],writeToFile=True, resultKey = 'disputes_org',reportName='Disputes_'))



    declined_report_dispute = dict(type="ReportGenerator", properties=dict(
        sources=[dict(source='master_result', filterConditions=["df = df[df['Function Code and Description'].str.contains('|'.join(['262-Refund','265-SMS Tip & Surcharge Adjustment']))]",
                                                                "df['Transaction Date'] = df['Transaction Date'].astype(str)",
                                                                 "df['Remarks_final'] = df['Card Acceptor Name'] +'/'+df['Acquirer Reference Data - RRN'].str[-4:].str.strip()+'/'+df['Transaction Date'] "],
                      drop=True)],
        writeToFile=True, resultKey = 'disputes_org',reportName='Disputes_refund_'))


    fill_gefu = dict(type='DataFiller',
                     properties=dict(template='POS SETTLEMENT ENTRIS_1.csv', alias={"dsrsummarydf": "df"},
                                     resultKey='dsrReport1'))


    fill_gefu1 = dict(type='DataFiller',
                     properties=dict(template='POS SETTLEMENT ENTRIS_2.csv', alias={"dsrsummarydf1": "df"},
                                     resultKey='dsrReport2'))

    disputes_org_report1 = dict(type="ReportGenerator", properties=dict(sources=[dict(source='dsrReport1',drop=True)],writeToFile=True, resultKey = 'dsrReport1',reportName='DSR_1'))

    disputes_org_report2 = dict(type="ReportGenerator", properties=dict(sources=[dict(source='dsrReport2',drop=True)],writeToFile=True, resultKey = 'dsrReport2',reportName='DSR_2'))

    disputes_org_report = dict(type="ReportGenerator", properties=dict(sources=[dict(source='master_result', filterConditions=["df = df[df['Function Code and Description'].str.contains('|'.join(['205-Re-presentment Raise','450-Chargeback Raise']))]",
                                                                 "df['Remarks_final'] = 'Moved To DIspute-GL' ","del df['ACCOUNT_NUMBER']",
                                                                 "del df['DAT_TXN_POSTING']"],drop=True)],writeToFile=True, resultKey = 'disputes_org',reportName='Disputes_'))



    selfmatch = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                     columns=dict(
                                                                         keyColumns=["REF_DOC_NO",
                                                                                     "CARD_NUMBER"],
                                                                         amountColumns=[
                                                                             "TXN_AMOUNT"],
                                                                         crdrcolumn=[
                                                                             "CR_DR_IND"],
                                                                         CreditDebitSign=True),
                                                                     sourceTag="CBS")], matchResult="iiscbsdf"))


    twowaymatch_new = dict(type="NWayMatch", properties=dict(sources=[
        dict(source="iiscbsdf.CBS",
             columns=dict(
                 keyColumns=["req_col_cbs_org","TXN_AMOUNT"],
                 sumColumns=[
                     ],
                 matchColumns=[
                     "req_col_cbs_org",
                     "TXN_AMOUNT"]),
             sourceTag="CBS",
             subsetfilter = [
                             "del df['req_col_match_npci']",
                             "df['TXN_AMOUNT'] =df['TXN_AMOUNT'].astype(np.float64)"]),
        dict(source="npciposdf",
             columns=dict(
                 keyColumns=[
                             "req_col_match_npci","AMOUNT"],
                 sumColumns=[
                     ],
                 matchColumns=[
                     "req_col_match_npci",
                     "AMOUNT"]),
             sourceTag="NPCIPOS",
             subsetfilter = [
                             "df['AMOUNT'] = df['AMOUNT'].astype(np.float64)"])],
        matchResult="results"))

    cbs_match_remarks = dict(type="ExpressionEvaluator",
                           properties=dict(source='results.CBS', resultkey='results.CBS',
                                           expressions=[
                                               "df['Remarks'] = '' ","df['Credit_to'] = '' ",
                                           "df.loc[df['ACQCODE'] == 'A','Remarks'] = 'SUCCESSFUL' ",
                                           "df.loc[df['ACQCODE'] == 'D','Remarks'] = 'UNRECONCILED' ",
                                           "df.loc[df['ACQCODE'] == 'A','Credit_to'] = '208100038' ",
                                           "df.loc[df['ACQCODE'] == 'D','Credit_to'] = df['ACCOUNT_NUMBER']"]))

    switch_post_remarks = dict(type="AddPostMatchRemarks",
                               properties=dict(source='results.Switch', remarkCol='Bank Remarks',
                                               remarks={"matchRemarks": "System Matched",
                                                        "selfRemarks": "Reverse Matched"}, how='all'))

    switch_exp_remarks = dict(type="ExpressionEvaluator",
                              properties=dict(source='results.Switch', resultkey='results.Switch',
                                              expressions=["df['TXN_TIME'] = df['TXN_TIME'].astype(np.int64)",
                                                           "df.loc[(df['TXN_DATE'] == payload['statementDate']) & (df['TXN_TIME'] >= 230000), 'Exception Reason'] = 'Cut off transaction'",
                                                           "exp_reason = df['Exception Reason']",
                                                           "del df['Exception Reason']",
                                                           "df.insert(len(df.columns),'Exception Reason', exp_reason)"]))


    dumpData = dict(type="DumpData", properties=dict(dumpPath='ATM POS', matched='any'))

    elem23 = dict(type="GenerateReconSummary",
                  properties=dict(
                      sources=[dict(resultKey="NPCIPOS", sourceTag="NPCIPOS", aggrCol=['AMOUNT']),
                               dict(resultKey="CBS", sourceTag="CBS", aggrCol=['TXN_AMOUNT'])]))

    genMetaInfo = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [inititalizer, dsrdata, cbsdata, posdata, dsrdata2, vlookup_accnos, cbs_cfData_exp, cbs_insert,
                load_disputes_file, dispute_exp, get_accno_db, masterresult_dispute_merge, fill_gefu,
                fill_gefu1, disputes_org_report1, disputes_org_report2,
                disputes_org_report, declined_report_dispute, selfmatch, twowaymatch_new, cbs_match_remarks, dumpData,
                genMetaInfo]

    # elements = [inititalizer,dsrdata,dsrdata2,fill_gefu,fill_gefu1,disputes_org_report1,disputes_org_report2,dumpData]
    # elements = [inititalizer, cbsdata,posdata, dsrdata, selfmatch, twowaymatch, disputes, load_disputes,
    #             vlookup_disputes, format_gefu_entries, save_current_disputes, fill_gefu, chargeback_entries,
    #             debit_repr_entries, credit_repr_entries, vlookup_acqcode, npci_approved_cbscr_rev,
    #             npci_approved_cbsdr_rev, npci_declined_cbscr_rev, npci_declined_cbsdr_rev, concat_sources,
    #             # generate_gefu,
    #          npci_post_remarks, cbs_post_remarks, cbs_exp_remarks,
    #             npci_exp_remarks, declined_report, dumpData,genMetaInfo]

    f = FlowRunner(elements)

    f.run()
