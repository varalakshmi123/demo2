from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "21-Nov-2017"
    basePath = "/Users/shivashankar/Desktop/ReconData/Equitas/Raw Data"
    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    sfms_filters = []
    acqswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                    'df=df.loc[df["BENEFICIARYCODE"]!="EQU"]',
                    'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                    'df=df.loc[(df["TXN_TYPE"]=="04") | (df["TXN_TYPE"]=="88")]']

    inititalizer = dict(id=1, next=2, type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="ATM", compressionType="", resultkey='',ignoredb=True))
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_SWITCH_Structure.csv"
    switchdata = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                                      feedPattern="^EAEQU.*\.txt",
                                                                      feedParams=params,
                                                                      feedFilters=acqswitchfilter,
                                                                      resultkey="switchposdf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="114210042"]','df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']

    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_HOST_Structure.csv"
    cbsdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                     feedPattern="^H2H.*\.txt",
                                                     feedParams=params,
                                                     feedFilters=cbsfilters,
                                                     resultkey="cbsdf"))

    npciacq = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                     feedPattern="ACQRP*.*mE*.",
                                                     feedParams=dict(type="OUTWARD"),
                                                     feedFilters=[
                                                                     'df=df.loc[df["Actual Transaction Amount"]>0]'],
                                                     resultkey="npciacqdf"))

    threewaymatch = dict(id=6, type="NWayMatch", properties=dict(sources=[dict(source="switchposdf",
                                                                               columns=dict(keyColumns=["RRN", "CARD_NUMBER"],
                                                                                    sumColumns=["TXN_AMOUNT"],
                                                                                    matchColumns=["RRN", "CARD_NUMBER",
                                                                                                  "TXN_AMOUNT"]),
                                                                               sourceTag="Switch"), dict(source="cbsdf",
                                                                                                 columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     sumColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     matchColumns=[
                                                                                                         "RRN",
                                                                                                         "CARD_NUMBER",
                                                                                                         "TXN_AMOUNT"]),
                                                                                                 sourceTag="CBS",subsetfilter='df.loc[df["TXN_AMOUNT"]>0]'),
                                                                          dict(source="npciacqdf",
                                                                        columns=dict(
                                                                            keyColumns=["RRN",
                                                                                        "PAN Number"],
                                                                            sumColumns=[
                                                                                "Actual Transaction Amount"],
                                                                            matchColumns=[
                                                                                "RRN",
                                                                                "PAN Number",
                                                                                "Actual Transaction Amount"]),
                                                                        sourceTag="NPCIAcquirer")],
                                                         matchResult="results"))

    elements = [inititalizer, switchdata, cbsdata, npciacq, threewaymatch]
    f = FlowRunner(elements)
    f.run()
    f.result['switchposdf'].to_csv("Output/switchposdf.csv", index=False)
    f.result['cbsdf'].to_csv("Output/cbsdf.csv", index=False)
    f.result['npciacqdf'].to_csv("Output/npciacqdf.csv", index=False)
    for x in f.result['results'].keys():
        f.result['results'][x].to_csv("Output/" + x + ".csv")

