import pandas as pd
from Utilities import Utilities


class Matcher():
    def __init__(self):
        pass

    def match(self, left=None, right=None, leftkeyColumns=[], rightkeycolumns=[], leftsumColumns=[], rightsumColumns=[],
              leftmatchColumns=[], rightmatchColumns=[], outputColumns=[]):

        # No data found in either of frame return others as un-matched
        if left.empty or right.empty:
            left['Matched'], right['Matched'] = 'UNMATCHED', 'UNMATCHED'
            return left, right
	
        columns = []
        columns.extend(leftkeyColumns)
        columns.extend(leftsumColumns)

        right.reset_index(drop=True, inplace=True)
        left.reset_index(drop=True, inplace=True)

        if len(leftsumColumns) > 0:
            leftworking = left[columns].groupby(leftkeyColumns).sum().reset_index()
        else:
            leftworking = left[columns]

        columns = []
        columns.extend(rightkeycolumns)
        columns.extend(rightsumColumns)
        if len(rightsumColumns) > 0:
            rightworking = right[columns].groupby(rightkeycolumns).sum().reset_index()
        else:
            rightworking = right[columns]

        # for i in range(0, len(rightworking.columns)):
        #     if rightworking[rightworking.columns[i]].dtype != leftworking[leftworking.columns[i]].dtype:
        #         print "Column Mismatch in columns", leftworking.columns[i], rightworking.columns[i], rightworking[
        #             rightworking.columns[i]].dtype, leftworking[leftworking.columns[i]].dtype
        # for col in leftworking.columns:
        #    print col, leftworking[str(col)].dtype
        # print leftworking.columns
        # print rightworking.head()
        # print leftworking.head()

        # Incase of carry forward disable do not add carry forward indicator columns
        if 'CARRY_FORWARD' in left.columns:
            leftworking['left_carryforward'] = left['CARRY_FORWARD']
            left_cf_column = ['left_carryforward']
        else:
            left_cf_column = []

        if 'CARRY_FORWARD' in right.columns:
            rightworking['right_carryforward'] = right['CARRY_FORWARD']
            right_cf_column = ['right_carryforward']
        else:
            right_cf_column = []

        finalworking = leftworking.merge(rightworking, how="outer", left_on=leftmatchColumns,
                                         right_on=rightmatchColumns, indicator=True, suffixes=('', '_y'))

        for col in finalworking.columns.values:
            if col.endswith("_y"):
                del finalworking[col]

        matched = finalworking[finalworking["_merge"] == "both"]

        if '_merge' in matched.columns:
            del matched['_merge']

        if len(leftsumColumns) > 0:
            leftmatchColumns = list(set(leftmatchColumns) - set(leftsumColumns))

        if len(rightsumColumns) > 0:
            rightmatchColumns = list(set(rightmatchColumns) - set(rightsumColumns))


        left = left.merge(matched[leftmatchColumns + right_cf_column], how="left", on=leftmatchColumns,
                          indicator=True)
        right = right.merge(matched[rightmatchColumns + left_cf_column], how="left", on=rightmatchColumns,
                            indicator=True)

        left['Matched'] = 'UNMATCHED'

        left.loc[left['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del left["_merge"]

        right['Matched'] = 'UNMATCHED'
        right.loc[right['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del right['_merge']

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

        # left, right = self.add_match_remarks(left, right, leftkeyColumns, rightkeycolumns, leftsumColumns,
        #                                      rightsumColumns)

        return left, right

    def add_match_remarks(self, left, right, leftkeyColumns=[], rightkeycolumns=[], leftsumColumns=[],
                          rightsumColumns=[]):

        left_len, right_len = len(left), len(right)

        left_columns = leftkeyColumns
        right_columns = rightkeycolumns

        if 'Exception Remarks' not in right.columns:
            right['Exception Remarks'] = ''

        if 'Exception Remarks' not in left.columns:
            left['Exception Remarks'] = ''

        if 'Matching Remarks' not in right.columns:
            right['Matching Remarks'] = ''

        if 'Matching Remarks' not in left.columns:
            left['Matching Remarks'] = ''


        left.loc[left['Matched'] == 'MATCHED', ['Matching Remarks','Exception Remarks']] = '',''
        right.loc[right['Matched'] == 'MATCHED', ['Matching Remarks','Exception Remarks']] ='',''
        # left.loc[:,['Matching Remarks','Exception Remarks']]+='(','('
        # right.loc[:,['Matching Remarks','Exception Remarks']]+='(','('
        for idx in range(0, len(left_columns)):
            left_col = left_columns[idx]
            right_col = right_columns[idx]

            left_unique = left[[left_col]].drop_duplicates(keep='first')
            right_unique = right[[right_col]].drop_duplicates(keep='first')

            right = right.merge(left_unique, how='left', left_on=right_col, right_on=left_col,
                                indicator=True)
            left = left.merge(right_unique, how='left', left_on=left_col, right_on=right_col,
                              indicator=True)



            # r_idx = right[right_col] == Utilities().infer_dtypes(right[right_col])
            # l_idx = left[left_col] == Utilities().infer_dtypes(left[left_col])

            right[['Exception Remarks', 'Matching Remarks']] = right[['Exception Remarks', 'Matching Remarks']].fillna(
                '')
            left[['Exception Remarks', 'Matching Remarks']] = left[['Exception Remarks', 'Matching Remarks']].fillna('')

            # right.loc[r_idx, 'Exception Remarks'] += ',' + 'Blank %s' % right_col
            # left.loc[l_idx, 'Exception Remarks'] += ',' + 'Blank %s' % left_col

            right.loc[(right['_merge'] != 'both'), "Exception Remarks"] +=right_col+','
            left.loc[(left['_merge'] != 'both'), "Exception Remarks"] +=left_col+','

            right.loc[(right['_merge'] == 'both'), "Matching Remarks"] +=right_col+','
            left.loc[(left['_merge'] == 'both'), "Matching Remarks"] +=left_col+','

            # right.loc[(right['_merge'] == 'both') & (right['Matched'] == 'MATCHED'), "Exception Remarks"] = ''
            # left.loc[(left['_merge'] == 'both') & (left['Matched'] == 'MATCHED'), "Exception Remarks"] = ''


            right.drop(columns='_merge', inplace=True, errors='ignore')
            left.drop(columns='_merge', inplace=True, errors='ignore')

        left['Exception Remarks'] = left['Exception Remarks'].str.rstrip(',')
        right['Exception Remarks'] = right['Exception Remarks'].str.rstrip(',')

        left['Matching Remarks'] = left['Matching Remarks'].str.rstrip(',')
        right['Matching Remarks'] = right['Matching Remarks'].str.rstrip(',')

        # left.loc[:,['Matching Remarks','Exception Remarks']]+=')'
        # right.loc[:,['Matching Remarks','Exception Remarks']]+=')'

        # left[~left.index.isin(left.index), 'Matching Remarks'] += ','.join(left_columns)
        # right[~right.index.isin(right.index), 'Matching Remarks'] = ','.join(right_columns)
        #
        #
        # left[~left.index.isin(left.index), 'Exception Remarks'] += ','.join(left_columns)
        # right[~right.index.isin(right.index), 'Exception Remarks'] = ','.join(right_columns)


        # 
        # left = pd.concat([left[left['Matched'] == 'MATCHED'], left], join_axes=[left.columns])
        # right = pd.concat([right[right['Matched'] == 'MATCHED'], right], join_axes=[right.columns])

        if len(left) != left_len or len(right) != right_len:
            print 'Possible cartesian product while adding matchin remarks !!!'

        return left, right
