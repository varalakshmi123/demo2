import logger

logger = logger.Logger.getInstance("smartrecon").getLogger()


class ReconExecutor():
    def __init__(self, steps, initialdata):
        self.steps = steps
        self.initialdata = initialdata
        # Put job details here
        self.output = {}

    def run(self):
        for step in self.steps:
            try:
                self.output = step.run(self.initialdata, self.output)
            except Exception as inst:
                logger.error(inst)
