import xlrd

import FlowElements
from FlowElements import *
from prerecon.FeedLoader import FeedLoaderFactory

class FlowBuilder():
    def __init__(self):
        self.flowelements={}
        self.loaders={}

    def find_modules(self,directory, module_root=None):
        _module_root = os.path.basename(directory)
        if module_root:
            _module_root = module_root
        for module in os.listdir(directory):
            # skip this file and any other non-python file
            if module == '__init__.py' or module[-3:] != '.py':
                continue
            yield __import__('%s.%s' % (_module_root, module[:-3]),
                             locals(), globals(), ['object'], -1)



    def getFlowElements(self):
        if len(self.flowelements)==0:
            for name,val in Utilities().FindAllSubclasses(FlowElements, FlowElement):
                self.flowelements[name]=val().properties
        return self.flowelements

    def getLoaders(self):
        if len(self.loaders)==0:
            subclasses = [Utilities().FindAllSubclasses(module, FeedLoader.FeedLoader) for module in self.find_modules("prerecon")]
            for x in subclasses:
                for name,klass in x:
                    self.loaders[name]=klass
        return self.loaders

    def loadFile(self,name,type,filepath,params={}):
        fl=FeedLoaderFactory().getInstance(type)
        if type == "CSV" or type=="Excel" or type=="RupayXML" or type=="NPCI":
            df=fl.loadFile([filepath],params).head(50)
        elif len(params)==0:
            return "Please provide a feed definition to parse the data"
        return df.to_json(orient='records')

    def getExcelData(self,filepath):
        result=[]
        wb = xlrd.open_workbook(filepath)
        for i in range(0, wb.nsheets):
            sheetdata=""
            sheet = wb.sheet_by_index(i)
            for row_idx in range(0, 50):
                row = [str(cell.value) if isinstance(cell.value, float) else str(cell.value)
                       for cell in sheet.row(row_idx)]
                sheetdata+=",".join(row)+'\n'
            sheetdata=sheet.name+'\n\n'+sheetdata
            result.append(sheetdata)
        return result


    def getSampleData(self,type,filepath):
        if type=="RupayXML":
            fl = FeedLoaderFactory().getInstance(type)
            df = fl.loadFile([filepath], {}).head(50)
            returndata =[df.to_csv(index=False)]
        elif type=="Excel" or type=="SFMS":
            return self.getExcelData(filepath)
        else:
            with open(filepath) as f:
                content = f.readlines(50)
            # you may also want to remove whitespace characters like `\n` at the end of each line
            return content


    def getCurrentState(self,elements,filelocation):
        if type(elements) != list or len(elements)==0:
            return False, "Invalid Elements"
        if elements[0]["type"]!="Initializer":
            return False,"Missing initializer Element"
        elements[0]["properties"]["ignoreDb"]=True
        fr=FlowRunner(elements,True)
        try:
            fr.run()
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            fr.debugoutput["ExceptionType"]=str(exc_type)
            fr.debugoutput["ExceptionValue"] = str(exc_value)
            fr.debugoutput["ExceptionStack"] = str(exc_traceback)
        returnvars={}
        for key,value in fr.debugoutput.iteritems():
            returnvars[key]={}
            if type(value)==dict:
                for i in value.keys():
                    if type(value[i])==pd.DataFrame:
                        returnvars[key][i]=[]
                        for x in value[i].columns:
                            returnvars[key][i].append(x)
                    else:
                        returnvars[key][i]=value[i]
            else:
                returnvars[key]=value

        return returnvars



if __name__ == "__main__":
    #print FlowBuilder().getFlowElements()
    #print FlowBuilder().loadFile("rakunaka","Excel","/Users/ShivaShankar/Downloads/DynoLicenses.csv.xlsx")
    #print FlowBuilder().getSampleData("Excel","/Users/ShivaShankar/Downloads/DynoLicenses.csv.xlsx")
    #print FlowBuilder().getSampleData("BTH", "/Users/shivashankar/Desktop/ReconData/AndhraBank/CreditCard/21012018/AND1210118.bth")
    stmtdate = ""
    basePath = "/Users/shivashankar/Desktop/ReconData/Equitas/Raw Data/"
    elem1 = dict(name="First", type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath=basePath+"/temp",
                                 recontype="ATM", compressionType="", resultkey='', ignoredb=True))
    issuerswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                          'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                          'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                          # 'df=df.loc[(df["TXN_TYPE"]=="04") | (df["TXN_TYPE"]=="88")]',
                          'df["RRNISSUER"]=df["RRNISSUER"].astype(np.int64)',
                          'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                          'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_SWITCH_Structure.csv"
    elem2 = dict(name="EANYLoader", type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                                 feedPattern="^EANFY.*\.txt",
                                                                 feedParams=params,
                                                                 feedFilters=issuerswitchfilter,
                                                                 resultkey="switchposdf"))
    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)', 'df=df.loc[df["GLCODE"]=="208100034"]',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0]["CR_DR_IND"]="D"',
                  'df["RRN"].fillna(0,inplace=True)', 'df["RRN"]=df["RRN"].astype(np.int64)',
                  'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']
    params = {}
    params["feedformatFile"] = "Reference/Equitas_ATM_HOST_Structure.csv"
    elem3 = dict(name="Host2Host", type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                 feedPattern="^H2H.*\.txt",
                                                                 feedParams=params,
                                                                 feedFilters=cbsfilters,
                                                                 resultkey="cbsdf"))

    print FlowBuilder().getCurrentState([elem1,elem2,elem3],basePath)
