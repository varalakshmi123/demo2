import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                recontype="Ginie", compressionType="zip",
                                resultkey='', reconName="EQT_GINIE",uploadFileName = uploadFileName))
    load_cbs_file = dict(type="PreLoader", properties=dict(loadType='Excel', source="cbs", disableCarryFwd=True,
                                                            feedParams={"feedformatFile": 'CBS_GINIE_Structure.csv',
                                                                        "skiprows": 1},
                                                            feedPattern="CBS_FILE_*", resultkey="cbs_df",
                                                            feedFilters=[""]))
    load_gupshup_file = dict(type="PreLoader", properties=dict(loadType='CSV', source="gupshup", disableCarryFwd=True,
                                                           feedParams={"feedformatFile": 'GUPSHUP_GINIE_Structure.csv',
                                                                       "skiprows": 1},
                                                           feedPattern="Detailed*", resultkey="gupshup_df",
                                                           feedFilters=[]))
    nwaymatch_ginie= dict(type="NWayMatch",
                            properties=dict(sources=[dict(source="cbs_df",
                                                          columns=dict(
                                                              keyColumns=["MESSAGEID","PHONENUMBER"],
                                                              sumColumns=[],
                                                              matchColumns=["MESSAGEID","PHONENUMBER"]),
                                                          sourceTag="CBS_GINIE"),
                                                     dict(source="gupshup_df", columns=dict(
                                                         keyColumns=["MESSAGE ID","MOBILE NUMBER"],
                                                         sumColumns=[],
                                                         matchColumns=["MESSAGE ID","MOBILE NUMBER"]),
                                                          sourceTag="GUPSHUP_GINIE")],
                                            matchResult="results"))
    cbs_message_filters = dict(type="ExpressionEvaluator",
                                  properties=dict(source="results.CBS_GINIE",
                                                  expressions=[
                                                      "df = df[df['GUPSHUP_GINIE Match'] == 'UNMATCHED']",
                                                      "df = df[['MESSAGEID','PHONENUMBER','MESSAGETEXT','SUBMITTEDTIME','FREEFIELD1']]"],
                                                  resultkey='cbs_notin_gupshup'))

    cbs_message_filters_report= dict(type="ReportGenerator",
                                     properties=dict(sources=[
                                         dict(source='cbs_notin_gupshup', sourceTag="cbs_message_filters_report"
                                              )],
                                                     writeToFile=True, reportName='CBS_NOTIN_GUPSHUP'))
    gupshup_message_filters = dict(type="ExpressionEvaluator",
                               properties=dict(source="results.GUPSHUP_GINIE",
                                               expressions=[
                                                   "df.loc[df['STATUS'] == 'Not Sent to Operator','CAUSE'] = 'Number does not exist'",
                                               "df = df[['MESSAGE ID','MOBILE NUMBER','MASK','TRANSACTION ID','MESSAGE TEXT','STATUS','CAUSE']]"],
                                               resultkey='gupshup_nomsg'))

    gupshup_message_filters_report = dict(type="ReportGenerator",
                                      properties=dict(sources=[
                                          dict(source='gupshup_nomsg', sourceTag="gupshup_message_filters_report"
                                               )],
                                          writeToFile=True, reportName='GUPSHUP_NOMSG'))
    gupshup_summary_filters =dict(type="ExpressionEvaluator",
                               properties=dict(source="results.GUPSHUP_GINIE",
                                               expressions=[
                                                   "df['TOTAL'] = 1",
                                                   "df = df.groupby(['STATUS','CAUSE'],as_index=False)['TOTAL'].sum()",
                                                   "df.loc[len(df),'STATUS'] = 'GRAND TOTAL' ",
                                                   "df.loc[df['STATUS'] == 'GRAND TOTAL','TOTAL'] = df['TOTAL'].sum()",
                                                   ],
                                               resultkey='gupshup_summary_df'))
    gupshup_summary_filters_report = dict(type="ReportGenerator",
                                          properties=dict(sources=[
                                              dict(source='gupshup_summary_df', sourceTag="gupshup_summary_filters_report"
                                                   )],
                                              writeToFile=True, reportName='GUPSHUP_SUMMARY'))

    dump_data = dict(type='DumpData', properties={})
    meta = dict(type='GenerateReconMetaInfo', properties=dict())
    elements = [init,load_cbs_file,load_gupshup_file,nwaymatch_ginie,cbs_message_filters,cbs_message_filters_report,gupshup_message_filters,
                gupshup_message_filters_report,gupshup_summary_filters,gupshup_summary_filters_report,dump_data,meta]
    f = FlowRunner(elements)
    f.run()

