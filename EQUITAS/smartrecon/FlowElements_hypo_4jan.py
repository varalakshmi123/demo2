import copy
import datetime
import errno
import os
import sys
import traceback
import uuid
import zipfile

import pandas as pd
from pymongo import MongoClient
import json
import Matcher
import config
from Utilities import Utilities
from prerecon import FeedLoader as FeedLoader, FilterExecutor, FindFiles  # , CustomFunctionLoader

import logger
import numpy as np
import shutil
import inspect
import collections

import re
from GEFUHandler import GEFUHandler
from customscripts import PreLoaderScript
logger = logger.Logger.getInstance("smartrecon").getLogger()


class FlowElement:
    def __init__(self):
        self.type = None
        self.name = None
        self.properties = {}
        self.id = uuid.uuid4().hex

    def run(self, payload, debug):
        pass


class FlowRunner:
    def __init__(self, flow, debug=False):
        self.name = None
        self.elements = flow
        # self.lookup = {k['id']: k for k in self.elements}
        self.classMap = dict()
        self.debug = debug
        classes = self.FindAllSubclasses(FlowElement)
        for x in classes:
            self.classMap[x[1]] = x[0]
        self.result = None
        self.debugoutput = {}

    def FindAllSubclasses(self, classType):
        subclasses = []
        callers_module = sys._getframe(1).f_globals['__name__']
        classes = inspect.getmembers(sys.modules[callers_module], inspect.isclass)
        for name, obj in classes:
            if (obj is not classType) and (classType in inspect.getmro(obj)):
                subclasses.append((obj, name))
        return subclasses

    def run(self, payload=None):
        if payload is None:
            payload = dict()
        if len(self.elements) > 0:
            count = 0
            for x in self.elements:
                count += 1
                klass = self.classMap[x["type"]]
                instance = klass()
                instance.properties = x.get("properties", {})
                instance.nodeId = x.get("nodeId", str(count))
                instance.name = instance.properties.get("name", str(x["type"]) + str(count))
                instance.run(payload, self.debug)
                name = instance.name
                jobcompleted = "{0:.0f}%".format((float(count) / len(self.elements)) * 100)
                mClient = MongoClient(config.mongoHost)[config.databaseName]['recon_execution_details_log']
                if count==1:
                    stmtDate = datetime.datetime.strptime(instance.properties.get("statementDate", ""), '%d-%b-%Y')
                    mClient.insert(
                        {"statementDate": stmtDate, "jobStatus": 'JobRunning', "errorMsg": '',
                         "reconName": payload['reconName'], "reconExecutionId": Utilities().getNextSeq(),
                         "executionDateTime": datetime.datetime.now().strftime('%s'),"jobcompleted":jobcompleted,
                         "partnerId": "54619c820b1c8b1ff0166dfc"})

                else:
                    doc = mClient.find_one(
                        {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
                        sort=[('reconExecutionId', -1)])

                    mClient.update(
                        {"reconName": payload['reconName'], "reconExecutionId": doc['reconExecutionId']},
                        {'$set': {"jobcompleted": jobcompleted}})

                if not name:
                    name = "NoName"
                name += "@" + instance.type + str(count)
                # self.debugoutput[name] = copy.deepcopy(payload)
                if 'error' in payload:
                    print "Error in stage" + str(x)
                    print payload['error']
                    break
        status = 'Failed' if 'error' in payload.keys() else "Success"
        self.result = payload
        if "ignoredb" not in self.elements[0].get("properties", {}):
            self.updateJobStatus(status=status, payload=payload)

    def updateJobStatus(self, status='Success', payload={}):
        mongoClient = MongoClient(config.mongoHost)[config.databaseName]

        errorMsg = ''
        if status == 'Failed':
            for error in payload['error'].get('traceback', '').split('\\n'):
                print error
                logger.info(error)
            errorMsg = payload['error'].get('exception', 'Error Running Job, Contact Admin.')
            logger.info('Exception Message : %s' % errorMsg)

        # mongoClient['recon_execution_details_log'].insert(
        #     {"statementDate": payload['statementDate'], "jobStatus": status, "errorMsg": errorMsg,
        #      "reconName": payload['reconName'], "reconExecutionId": Utilities().getNextSeq(),
        #      "executionDateTime": datetime.datetime.now().strftime('%s'), "partnerId": "54619c820b1c8b1ff0166dfc"})

        doc = mongoClient['recon_execution_details_log'].find_one(
            {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
            sort=[('reconExecutionId', -1)])

        mongoClient['recon_execution_details_log'].update(
            {"reconName": payload['reconName'], "reconExecutionId": doc['reconExecutionId']},
            {'$set': {"jobStatus": status, 'errorMsg': errorMsg}})


class Initializer(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Initializer"
        self.properties = dict(statementDate="", basePath="", outputPath="", recontype="", compressionType="")

    def run(self, payload, debug):
        try:
            stmtDate = datetime.datetime.strptime(self.properties.get("statementDate", ""), '%d-%b-%Y')
            stmtDateFormat = stmtDate.strftime('%d%m%Y')
            baseDir = self.properties['basePath']
            compressionType = self.properties.get('compressionType', '') or ''
            extract_path = os.path.join(baseDir, self.properties.get('reconName', ''), stmtDate.strftime('%d%m%Y'))
            # take upload file name from user / default to statement date
            uploadFileName = ""
            if compressionType != "":
                uploadFileName = self.properties['uploadFileName'] if self.properties.get('uploadFileName',
                                                                                          '') else stmtDateFormat + '.' + compressionType

            source_path = os.path.join(baseDir, *[self.properties.get('reconName', ''), uploadFileName])

            payload['statementDate'] = stmtDate
            payload['reconName'] = self.properties.get('reconName', '')
            payload['reconType'] = self.properties.get('recontype', '')
            # Disable carry forward for all the sources being loaded
            payload['disableAllCarryFwd'] = self.properties.get('disableAllCarryFwd', False)
            payload['dropColumns'] = []
            if not "ignoredb" in self.properties:
                mClient = MongoClient(config.mongoHost)[config.databaseName]['recon_execution_details_log']
                # mClient.insert(
                #     {"statementDate": payload['statementDate'], "jobStatus": 'JobRunning', "errorMsg": '',
                #      "reconName": payload['reconName'], "reconExecutionId": Utilities().getNextSeq(),
                #      "executionDateTime": datetime.datetime.now().strftime('%s'),
                #      "partnerId": "54619c820b1c8b1ff0166dfc"})
                if self.properties.get('incremental', False):
                    payload['incremental'] = True
                elif mClient.find_one({"statementDate": stmtDate, "jobStatus": "Success",
                                       "reconName": self.properties.get('reconName', '')}):
                    payload['error'] = dict(elementName="Initializer", type="critical",
                                            exception="Statement date already exists :%s" % stmtDate.strftime(
                                                '%d-%m-%Y'))
                    return
                else:
                    pass

            if not os.path.exists(source_path):
                os.mkdir(source_path)

            if os.path.isfile(source_path) and compressionType != "":
                extractFile = zipfile.ZipFile(source_path, mode='a')
                if not os.path.exists(os.path.join(extract_path)):
                    os.mkdir(extract_path)

                # check for old files in the uploaded path from previous jobs, remove & over-write
                # TODO, how to handle the same on incremental run.
                if os.listdir(extract_path) != [] and not payload.get('incremental', False):
                    shutil.rmtree(extract_path)

                extractFile.extractall(extract_path)
            elif compressionType != "":
                payload['error'] = dict(elementName="Initializer", type="critical",
                                        exception="File %s not available in recon mft path" % uploadFileName)
                return

            output_path = self.properties.get('outputPath', '') or os.path.join(extract_path, 'OUTPUT')

            if not os.path.exists(output_path):
                os.mkdir(output_path)

            # check for pre execution's
            execution_doc = None
            if not "ignoredb" in self.properties:
                execution_doc = mClient.find_one(
                    {"reconName": self.properties['reconName'], "jobStatus": "Success"},
                    sort=[('reconExecutionId', -1)])
            if execution_doc:
                payload['prevStmtDate'] = execution_doc['statementDate'].strftime('%d%m%Y')

            payload['source_path'] = extract_path
            payload['output_path'] = output_path
        except Exception as e:
            payload['error'] = dict(elementName="Initializer", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class PreLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DataLoader"
        self.properties = dict(loadType="", source="", feedPattern="", feedParams=dict(), feedFilters=[],
                               resultkey="dataframe")

    def run(self, payload, debug):
        try:
            util=Utilities()
            feedfac = FeedLoader.FeedLoaderFactory()
            stmtDate = payload['statementDate']
            findFiles = FindFiles.FindFiles(stmtDate)
            inst = feedfac.getInstance(self.properties.get('loadType', ''))

            if self.properties.get('preloadScript',False):
                getattr(PreLoaderScript(stmtDate, payload['reconName']), self.properties.get('preLoadFunc'))()


            inst_files = findFiles.findFiles(feedPath=payload.get('source_path', ''),
                                             fpattern=self.properties.get('feedPattern', ''),delta = self.properties.get('delta',0),
                                             raiseError=self.properties.get('errors', True))

            # Use to move files to processed folder on incremental loading


            if 'filesLoaded' not in payload:
                payload['filesLoaded'] = []
            payload['filesLoaded'].extend(inst_files)

            feedParams = self.properties.get('feedParams', dict())
            feedParams['payload'] = payload
            inst = inst.loadFile(inst_files, self.properties.get('feedParams', dict()))
            inst['SOURCE'] = self.properties.get('source', '')
            inst['CARRY_FORWARD'] = ''
            inst['STATEMENT_DATE'] = stmtDate
            inst['Ageing'] = (datetime.datetime.now() - inst['STATEMENT_DATE']).dt.days
            inst['Ageing'] = inst['Ageing'].astype(str)

            for col in inst:
                inst[col] = inst[col].fillna(util.infer_dtypes(inst[col]))

            if self.properties.get('feedFilters', []):
                logger.info('-------------------------------------------------')
                logger.info('Applying Feed Filters')
                inst = FilterExecutor.FilterExecutor().run(inst, self.properties.get('feedFilters', []),
                                                           payload=payload)
                logger.info('Total records post filter %s: ' % len(inst))

            # load carry forward data
            if 'disableAllCarryFwd' in payload.keys() and payload['disableAllCarryFwd']:
                inst.drop(labels='CARRY_FORWARD', inplace=True, errors='ignore', axis='columns')
                logger.info("Warning : Carry forward disabled for All Sources")
            elif 'disableCarryFwd' in self.properties.keys() and self.properties['disableCarryFwd']:
                inst.drop(labels='CARRY_FORWARD', inplace=True, errors='ignore', axis='columns')
                logger.info("Warning : Carry forward disabled for source %s" % self.properties['source'])
            elif 'prevStmtDate' in payload.keys():
                # check if source data is initialized, if not load carry forward data
                # to avoid duplicate records being loaded
                if self.properties.get('resultkey', '') not in payload.keys():
                    cfInst = feedfac.getInstance('CarryForward')
                    cfData = cfInst.loadFile([], {"payload": payload, "properties": self.properties})
                    logger.info('-------------------------------------------------')
                    logger.info('Total Carry Forward records %s: ' % len(cfData))
                    cfData['CARRY_FORWARD'] = 'Y'
                    # set index on cfData to retain derived columns if any exists.
                    inst = pd.concat([inst, cfData], join_axes=[cfData.columns])
            else:
                pass

            # update ageing for all the transaction based on statement date
            if self.properties.get('resultkey', '') in payload.keys():
                inst = pd.concat([payload[self.properties["resultkey"]], inst], join_axes=[inst.columns])
                inst['SYS_TXN_ID'] = inst.reset_index(drop = True).index + 1
                payload[self.properties["resultkey"]] = inst
            else:
                inst['SYS_TXN_ID'] = inst.reset_index(drop = True).index + 1
                payload[self.properties["resultkey"]] = inst
 	              
            logger.info('Total data loaded in %s is %s' % (self.properties.get('source', ''), len(inst)))
        except Exception as e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="PreLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class FilterDuplicates(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "FilterDuplicates"
        self.name = "FilterDuplicates"
        self.properties = dict(source=None, duplicateStore=None, allowOneInMatch=True, keyColumns=[], sourceTag='',
                               inplace=True, markerCol='', resultKey='')

    def run(self, payload, debug):
        try:
            data = payload[self.properties["source"]]

            if 'allowOneInMatch' in self.properties and self.properties['allowOneInMatch']:
                keep = 'first'
            else:
                keep = False

            if not self.properties['duplicateStore'] in payload.keys():
                payload[self.properties['duplicateStore']] = {}

            if data.empty:
                pass
            else:
                duplicate_idx = data.duplicated(subset=self.properties["keyColumns"], keep=keep)

                if self.properties.get('inplace', False):
                    if 'markerCol' not in self.properties.keys():
                        raise KeyError("For inplace, markerCol is mandatory input.")
                    data[self.properties['markerCol']] = ''
                    data.loc[duplicate_idx, self.properties['markerCol']] = 'DUPLICATED'
                    payload['dropColumns'].append(self.properties['markerCol'])
                else:
                    payload[self.properties["duplicateStore"]][self.properties['sourceTag']] = data[duplicate_idx]
                    data = data.drop_duplicates(subset=self.properties["keyColumns"], keep=keep)

            payload[self.properties.get('resultKey', self.properties["source"])] = data
        except Exception as e:
            payload['error'] = dict(elementName="FilterDuplicates", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class NWayMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "NWayMatch"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="", subsetfilter=[])],
                               matchResult=None)

    def run(self, payload, debug):
        print("In N Way Match")
        result = {}
        remainder = []
        util = Utilities()
        for i in range(0, len(self.properties["sources"])):
            source = self.properties["sources"][i]
            data = Utilities().getValues(payload, source['source'], sep='.')
            data = data.reset_index(drop=True)

            sub_filters = self.properties["sources"][i].get("subsetfilter", [])

            # Reverse matched records should not be part of Nway Match
            if 'Self Matched' in data.columns:
                sub_filters.append("df = df[df['Self Matched'] != 'MATCHED']")

            filtered_data = FilterExecutor.FilterExecutor().run(data, sub_filters, payload, "Filter" + str(i))

            util.setValues(payload, source['source'], set_value=filtered_data)
            remaining_data = data[~data.index.isin(filtered_data.index)]
            remainder.append(remaining_data)

        if len(self.properties["sources"]) == 1:
            self.singlesidematch(payload, result, remainder)
            return

        for i in range(0, len(self.properties["sources"]) - 1):
            leftsource = self.properties["sources"][i]
            leftcolumns = leftsource["columns"]
            for j in range(i + 1, len(self.properties["sources"])):
                rightsource = self.properties["sources"][j]
                rightdata = util.getValues(payload, rightsource['source'], sep='.')
                rightcolumns = rightsource["columns"]

                leftdata = util.getValues(payload, leftsource['source'], sep='.')

                left, right = Matcher.Matcher().match(leftdata, rightdata,
                                                      leftkeyColumns=leftcolumns.get('keyColumns', []),
                                                      rightkeycolumns=rightcolumns.get('keyColumns', []),
                                                      leftsumColumns=leftcolumns.get('sumColumns', []),
                                                      rightsumColumns=rightcolumns.get('sumColumns', []),
                                                      leftmatchColumns=leftcolumns.get('matchColumns', []),
                                                      rightmatchColumns=rightcolumns.get('matchColumns', []))

                if rightsource["sourceTag"] + " Match" in left.columns:
                    del left[rightsource["sourceTag"] + " Match"]

                if leftsource["sourceTag"] + " Match" in right.columns:
                    del right[leftsource["sourceTag"] + " Match"]

                if rightsource["sourceTag"] + " CarryForward" in left.columns:
                    del left[rightsource["sourceTag"] + " CarryForward"]

                if leftsource["sourceTag"] + " CarryForward" in right.columns:
                    del right[leftsource["sourceTag"] + " CarryForward"]

                left = left.rename(columns={'Matched': rightsource["sourceTag"] + " Match"})
                right = right.rename(columns={'Matched': leftsource["sourceTag"] + " Match"})

                left = left.rename(columns={'Matched': rightsource["sourceTag"] + " Match",
                                            "right_carryforward": rightsource['sourceTag'] + ' CarryForward'})
                right = right.rename(columns={'Matched': leftsource["sourceTag"] + " Match",
                                              "left_carryforward": leftsource['sourceTag'] + ' CarryForward'})

                util.setValues(payload, ref=self.properties['sources'][i]['source'], set_value=left)
                util.setValues(payload, ref=self.properties['sources'][j]['source'], set_value=right)

        for i in range(0, len(self.properties["sources"])):
            result_df = util.getValues(payload, self.properties["sources"][i]["source"])
            result_df = pd.concat([result_df, remainder[i]], join_axes=[result_df.columns]).reset_index(drop=True)

            # force all blank status columns to unmatched
            match_cols = [col for col in result_df.columns if ' Match' in col]
            if match_cols:
                result_df[match_cols] = result_df[match_cols].fillna('UNMATCHED')

            util.setValues(result, self.properties["sources"][i]["sourceTag"], result_df)

        # Avoid over-writing of existing data
        if self.properties['matchResult'] in payload.keys():
            for key, value in result.iteritems():
                payload[self.properties['matchResult']][key] = value
        else:
            payload[self.properties['matchResult']] = result

    def singlesidematch(self, payload, result, remainder):
        source = self.properties["sources"][0]
        columns = source["columns"]
        data = Utilities().getValues(payload, source["source"])
        keyColumns = columns.get('keyColumns', [])
        amountColumns = columns.get('amountColumns', [])

        crdrindictorcolumn = columns.get("crdrcolumn", [])

        if columns["CreditDebitSign"] == True:
            # crdrindictorcolumn = columns.get("crdrcolumn", [])
            # if len(crdrindictorcolumn) == 1:
            for x in amountColumns:
                data.loc[data[x] < 0.0, x] *= -1
        credit = data[data[crdrindictorcolumn[0]] == source.get('crSign', 'C')]
        debit = data[data[crdrindictorcolumn[0]] == source.get('drSign', 'D')]
        left, right = Matcher.Matcher().match(credit, debit,
                                              leftkeyColumns=columns.get('keyColumns', []) + columns.get(
                                                  'amountColumns', []),
                                              rightkeycolumns=columns.get('keyColumns', []) + columns.get(
                                                  'amountColumns', []),
                                              leftmatchColumns=columns.get('keyColumns', []) + columns.get(
                                                  'amountColumns', []),
                                              rightmatchColumns=columns.get('keyColumns', []) + columns.get(
                                                  'amountColumns', []))
        selfmatched = pd.concat([left, right])
        # drop psuedo columns appended by matcher function
        selfmatched.drop(labels=['right_carryforward', 'left_carryforward'], axis='columns', errors='ignore',
                         inplace=True)

        selfmatched = selfmatched.rename(columns={'Matched': "Self Matched"})
        selfmatched.loc[selfmatched["Self Matched"] == 'UNMATCHED', "Self Matched"] = ''
        # selfmatched = matchdata[keyColumns + amountColumns].groupby(keyColumns).sum().reset_index()
        # condition = None
        # for x in amountColumns:
        #    if condition:
        #        condition + " & "
        #    else:
        #        condition = ""
        #    condition += "selfmatched[\"" + x + "\"]==0"
        # selfmatched.loc[eval(condition), 'Self Matched'] = "MATCHED"


        payload[self.properties["sources"][0]["source"]] = selfmatched
        result[self.properties["sources"][0]["sourceTag"]] = payload[self.properties["sources"][0]["source"]].append(
            remainder[0])
        data = payload[self.properties["sources"][0]["source"]].append(remainder[0])
        Utilities().setValues(payload,
                              self.properties['matchResult'] + "." + self.properties["sources"][0]["sourceTag"], data)
        # payload[self.properties['matchResult']] = result


class PerformMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Match"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(leftData=None, rightData=None, left=col, right=col, leftUnmatch=None,
                               rightUnmatch=None, matchResult=None)

    def run(self, payload, debug):
        print "In Perform Match"
        # try:

        leftdata = payload[self.properties['leftData']]
        rightdata = payload[self.properties['rightData']]

        leftcolumns = self.properties.get('left', {})
        rightcolumns = self.properties.get('right', {})
        left, right = Matcher.Matcher().match(leftdata, rightdata,
                                              leftkeyColumns=leftcolumns.get('keyColumns', []),
                                              rightkeycolumns=rightcolumns.get('keyColumns', []),
                                              leftsumColumns=leftcolumns.get('sumColumns', []),
                                              rightsumColumns=rightcolumns.get('sumColumns', []),
                                              leftmatchColumns=leftcolumns.get('matchColumns', []),
                                              rightmatchColumns=rightcolumns.get('matchColumns', []))

        payload[self.properties['leftUnmatch']] = left[left['Matched'] == 'UNMATCHED']

        # if left & right source are same (incase of reversal) append by checking leftUnmatch key & rightUnmatch key
        # to avoid data over-lapping.
        if self.properties['rightUnmatch'] == self.properties['leftUnmatch']:
            payload[self.properties['rightUnmatch']] = payload[self.properties['rightUnmatch']].append(
                right[right['Matched'] == 'UNMATCHED'], ignore_index=True)
        else:
            payload[self.properties['rightUnmatch']] = right[right['Matched'] == 'UNMATCHED']

        leftMatch = left[left['Matched'] == 'MATCHED']
        rightMatch = right[right['Matched'] == 'MATCHED']
        # totalMatch = leftMatch.merge(rightMatch, left_on=leftcolumns.get('matchColumns', []),
        #                              right_on=rightcolumns.get('matchColumns', []),
        #                              how='inner', suffixes=('', '_y'))
        totalMatch = leftMatch.append(rightMatch, ignore_index=True)
        for col in totalMatch.columns:
            if '_y' in col:
                del totalMatch[col]

        if self.properties['matchResult'] in payload.keys():
            payload[self.properties['matchResult']] = payload[self.properties['matchResult']].append(totalMatch,
                                                                                                     ignore_index=True)
        else:
            payload[self.properties['matchResult']] = totalMatch

            # except Exception as e:
            #    payload['error'] = dict(elementName="PreLoader", type="critical", exception=str(e),
            #                            traceback=traceback.format_exc())


class ExpressionEvaluator(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ExpressionEvaluator"

        self.properties = dict(expressions=[], source="dataframe", resultkey="dataframe")

    def run(self, payload, debug):
        try:
            expressions = self.properties.get('expressions', [])
            df = Utilities().getValues(payload, self.properties['source']).copy()

            df = FilterExecutor.FilterExecutor().run(df, expressions, payload=payload)

            Utilities().setValues(payload, self.properties['resultkey'], set_value=df)
	
	   # payload[self.properties['resultkey']].to_csv('/home/reconuser/fine.csv')
        except Exception as e:
            payload['error'] = dict(elementName="ExpressionEval", type="critical", exception=str(e),
                                    traceback=traceback.format_exc(), expression=expressions)


class NPCIReportsLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "NPCIReportsLoader"

        self.properties = dict()

    def run(self, payload, debug):
        try:
            findFiles = FindFiles.FindFiles(payload['statementDate'])
            timeOutReports = findFiles.findFiles(feedPath=payload.get('source_path', ''),
                                                 fpattern=self.properties.get('timeOutReport', ''))

            npciTimeOutCols = ['TXN UID', 'TXN Type', 'TXN Date', 'TXN Time', 'Settlement Date',
                               'Response Code', 'Ref Number', 'STAN', 'Remitter', 'Beneficiary',
                               'Beneficiary Mobile /Account/Aadhar Number', 'Remitter Number',
                               'Column1']
            npciTimeOutReport = pd.DataFrame()
            if len(timeOutReports) > 0:
                for report in timeOutReports:
                    tmp = pd.read_html(report, skiprows=1)
                    if tmp:
                        npciTimeOutReport = npciTimeOutReport.append(tmp[-1], ignore_index=True)
                    else:
                        pass
                npciTimeOutReport.columns = npciTimeOutCols
                npciTimeOutReport.loc[:, 'Ref Number'] = npciTimeOutReport.loc[:, 'Ref Number'].astype(str).str.lstrip(
                    "'")
            else:
                print 'Could not find any time reports,'
            payload["npciTimeOutReport"] = npciTimeOutReport

        except Exception as e:
            payload['error'] = dict(elementName="NPCIReportsLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class DumpData(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DumpData"

        self.properties = dict()

    def run(self, payload, debug):
        try:
            outputPath = payload['source_path'] + '/' + 'OUTPUT/'
            logger.info('Storing data in path %s' % outputPath)
            if not os.path.exists(os.path.dirname(outputPath)):
                try:
                    os.makedirs(os.path.dirname(outputPath))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            mode, header = 'w', True
            if payload.get('incremental', False) and os.listdir(outputPath):
                mode, header = 'a', False
	    columnDtype = []

            float_format = '%.{roundTo}f'.format(roundTo=self.properties.get('roundTo', 2))
            # segreate matched and un-matched reports
            if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                writer = pd.ExcelWriter(outputPath + 'ConsolidatedReport.xlsx')
            for key, data in payload.get('results', {}).iteritems():

                matchkey_cols = []
                match_cond = []
                un_match_cond = []
                original_len = len(data)

                # replace all non-ascii characters with spaces
                # data.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True, inplace=True)
		if not MongoClient(config.mongoHost)[config.databaseName]['columnDtypes'].find(
                        {"reconName": payload['reconName'], "source": key}).count() > 0:
                    for index, dtype in enumerate(data.dtypes):
                        columnDtype.append({'column': data.columns[index], 'dtype': self.typeMapper(str(dtype))})
                MongoClient(config.mongoHost)[config.databaseName]['columnDtypes'].insert(
                    {"reconName": payload['reconName'], "partnerId": "54619c820b1c8b1ff0166dfc", "source": key,
                     "dtypes": columnDtype})
                drop_cols = payload.get("dropColumns", [])
                if drop_cols:
                    data.drop(labels=drop_cols, axis='columns', errors='ignore', inplace=True)

                for col in data.columns:
                    if col.endswith(' Match') and 'Self Matched' != col:
                        matchkey_cols.append(col)
                        match_cond.append("(data['%s'] == 'MATCHED')" % col)
                        un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
                    else:
                        pass

                reversal_data = pd.DataFrame()
                if 'Self Matched' in data.columns:
                    data['Self Matched'] = data['Self Matched'].fillna('')
                    reversal_data = data[data['Self Matched'] == 'MATCHED']
                    data = data[data['Self Matched'] != 'MATCHED']

                    if not reversal_data.empty:
                        reversal_data.to_csv(outputPath + "%s_Self_Matched.csv" % key, index=False, mode=mode,
                                             header=header, date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,
                                             encoding='utf-8')
                        if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                            self.writeToExcel(writer,reversal_data, self.properties.get('exportType'), key+'Reversals')

                if self.properties.get('matched', '') == 'any':
                    matched_df = data[eval(" | ".join(match_cond))]
                    un_matched_df = data[eval(" & ".join(un_match_cond))]
                else:
                    matched_df = data[eval(" & ".join(match_cond))]
                    un_matched_df = data[eval(" | ".join(un_match_cond))]

                # Filter subset records back to matched queue.
                subset_records = pd.DataFrame()
                if self.properties.get('subsetfilters', {}) and self.properties['subsetfilters'].get(key, []):
                    subset_records = FilterExecutor.FilterExecutor().run(data,
                                                                         self.properties['subsetfilters'].get(key, []))
                    if not subset_records.empty:
                        matched_df = pd.concat([matched_df, subset_records], join_axes=[matched_df.columns])

                # validation to ensure there is no missing data post filters
                post_filter_count = len(matched_df) + len(un_matched_df) + len(reversal_data)
                if original_len != post_filter_count:
                    ref_file_path = '/tmp/%s_%s.csv' % (key, datetime.datetime.now().strftime('%d%m%y_%H%M%S'))
                    data.to_csv(ref_file_path, index=False)
                    logger.info('-------------------------------------------------')
                    logger.info("Mismatch in data length post filter in DumpData")
                    logger.info("Resolution: use subsetfilters to restore filtered records")
                    logger.info("Extract path : %s" % ref_file_path)
                    raise Exception(
                        "Mismatch in data length for source '%s' original length %s , post filter %s" % (
                            key, len(data), post_filter_count))

                matched_df.to_csv(outputPath + "%s.csv" % key, index=False, mode=mode, header=header,
                                  date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                un_matched_df.to_csv(outputPath + "%s_UnMatched.csv" % key, index=False,
                                     date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                    self.writeToExcel(writer, data, self.properties.get('exportType'), key)

            if 'duplicateStore' in payload.keys():
                for key, data in payload['duplicateStore'].iteritems():
                    data['Remarks'] = 'Duplicated'
                    data.to_csv(outputPath + '%s_Duplicated.csv' % key, index=False, mode=mode, header=header,
                                date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                    if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                        self.writeToExcel(writer, data, self.properties.get('exportType'), key+'Duplicates')

            if 'custom_reports' in payload.keys():
                for key, data in payload['custom_reports'].iteritems():
                    if len(data)>1000000:
			data.replace({r'[^\x00-\x7F]+': ''}, regex=True, inplace=True)
                        data_split = np.array_split(data,2)
                        for i in [0,1]:
			    data_split[i].replace({r'[^\x00-\x7F]+': ''}, regex=True, inplace=True)
                            data_split[i].to_csv((outputPath + '{report}_Report_'+str(i)+'.csv').format(report=key), mode=mode, header=header, index=False,
                                date_format='%d-%m-%Y %H:%M:%S', float_format=float_format, encoding='utf-8')
                    else:
			data.replace({r'[^\x00-\x7F]+': ''}, regex=True, inplace=True)
                        data.to_csv(outputPath + '%s_Report.csv' % key, mode=mode,header=header, index=False,
                                             date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,encoding='utf-8')
                    if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                            self.writeToExcel(writer, data, self.properties.get('exportType'), key)

            if 'reconSummary' in payload.keys():
                payload['reconSummary'].to_csv(outputPath + 'recon_summary.csv', mode=mode, header=header, index=False,
                                               date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,
                                               encoding='utf-8')

                self.writeToExcel(writer, payload['reconSummary'], self.properties.get('exportType'),
                                  payload['reconName'] + 'Summary')

            if payload.get('incremental', False):
                self.moveToProcessed(payload=payload)

            if payload['reconName'] not in config.ignoreConsolidateReportRecons:
                writer.save()

            logger.info('=====> Recon Execution Completed <=====')
        except Exception, e:
            payload['error'] = dict(elementName="DumpData", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    # Move files to processed folder to avoid duplicate data loading
    def moveToProcessed(self, payload={}):
        processed_path = payload['source_path'] + '/PROCESSED'
        if not os.path.exists(processed_path):
            os.mkdir(processed_path)

        for file in payload.get('filesLoaded'):
            exists = processed_path + os.sep + file.split('/')[-1]
            if os.path.exists(exists):
                os.remove(exists)

            if os.path.isfile(file):
                shutil.move(file, processed_path)
    def typeMapper(self, dtype):
        if dtype == 'object':
            return 'str'

        elif dtype == 'float64':
            return 'np.float64'

        elif dtype == 'int64':
            return 'np.int64'
        else:
            return 'np.datetime64'
    def writeToExcel(self, excelWriter=None, dataframe=None, exportType='', key=''):
        if exportType != 'singleSheet':
            dataframe.to_excel(excelWriter, sheet_name=key, index=False)
        else:
            row = 0
            spaces = 1
            header_row = pd.DataFrame([{"header": key}])
            header_row.to_excel(excelWriter, sheet_name=key, startrow=row, startcol=0, index=False,
                                header=None)
            row += 1
            dataframe.to_excel(excelWriter, sheet_name=key, startrow=row, startcol=0, index=False)
            row = row + len(dataframe.index) + spaces + 1


class GenerateReconMetaInfo(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "GenerateReconMetaInfo"

        self.properties = dict()

    def run(self, payload, debug):
        collection = MongoClient(config.mongoHost)[config.databaseName]['recon_details']
        reconDict = {"reconName": payload['reconName'], "sourcePath": payload['reconType'],
                     "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                     "reconProcess": payload['reconType'], "reconId": payload['reconType'] + ' RECON',
                     "partnerId": "54619c820b1c8b1ff0166dfc", "reportDetails": []}

        for key, data in payload.get('results', {}).iteritems():
            reconDict['reportDetails'].extend([{'reportName': 'Matched', 'reportPath': '%s.csv' % key, "source": key},
                                               {'reportName': 'Unmatched',
                                                'reportPath': "%s_UnMatched.csv" % key, "source": key}])

            if 'Self Matched' in payload['results'][key]:
                reconDict['reportDetails'].append(
                    {'reportName': key + ' Self Match', 'reportPath': '%s_Self_Matched.csv' % key, "source": key})

        if 'duplicateStore' in payload.keys():
            for key, data in payload['duplicateStore'].iteritems():
                reconDict['reportDetails'].extend(
                    [{'reportName': key + ' Duplicated', 'reportPath': '%s_Duplicated.csv' % key, "source": key}])

        if 'custom_reports' in payload.keys():
            for key, data in payload['custom_reports'].iteritems():
                reconDict['reportDetails'].extend(
                    [{'reportName': key + ' Report', 'reportPath': '%s_Report.csv' % key, "source": 'Reports'}])

        if 'reconSummary' in payload.keys():
            reconDict['reportDetails'].append(
                {'reportName': "Recon Summary", 'reportPath': 'recon_summary.csv', "source": 'Reports'})

        if 'reports' in payload.keys():
            for report in payload['reports']:
                reconDict['reportDetails'].append(payload['reports'][report])

        existingDict = collection.find_one({'reconName': payload['reconName']})

        if not existingDict:
            collection.insert(reconDict)

        else:
            delta = list({dict2['reportPath'] for dict2 in reconDict['reportDetails']} -
                         {dict1['reportPath'] for dict1 in existingDict['reportDetails']})

            delta_dict = [{'reportPath': value} for value in delta]
            if delta_dict:
                newlist = [report for report in reconDict['reportDetails'] for data in delta_dict if
                           report['reportPath'] in data.values()]
                collection.update({"reconName": payload['reconName']}, {'$push': {'reportDetails': {'$each': newlist}}})
        print '===' * 50
        print 'Recon Defination'
        print reconDict
        print 'Recon License Defination'
	''''print {"sourcePath": payload['reconName'], "reconId": payload['reconType'] + ' RECON',
               "reconName": payload['reconName'], "licenseCount": 1, "licensed": "Licensed",
               "reconProcess": payload['reconType'], "updated": datetime.datetime.now().strftime('%s'),
                   "partnerId": "54619c820b1c8b1ff0166dfc", "jobScript": main.__file__.split('smartrecon')[-1]}'''
        print '===' * 50

class VLookup(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "VLookup"
        self.properties = dict(data="", lookup="", dataFields=[], lookupFields=[], includeCols=[], restoreCols=[],
                               markers={'colName': 'value'}, resultkey='')

    def run(self, payload, debug):
        try:
	    print "came fst"
	    # print payload['dfadj'].head()
            util = Utilities()
            data = util.getValues(payload, self.properties['data']).copy().reset_index(drop=True)
            lookup = util.getValues(payload, self.properties['lookup']).copy().reset_index(drop=True)

            data_field = self.properties['dataFields']
            lookup_field = self.properties.get('lookupFields', [])
            include_cols = self.properties.get('includeCols', [])
            marker_cols = self.properties.get('markers', {})

            # incase if look_up data is empty, initialize original data.
            if lookup.empty:
                for key in marker_cols.keys():
                    data[key] = data.get(key, '')

                for col in include_cols:
                    if col not in data.columns:
                        data[col] = ''

                util.setValues(payload, self.properties['resultkey'], data)
                return

            if lookup_field:
                lookup.drop_duplicates(subset=lookup_field, keep='first', inplace=True)

                if self.properties.get('dataFieldsFilter', []):
                    data = FilterExecutor.FilterExecutor().run(data, self.properties.get('dataFieldsFilter', []),
                                                               payload=payload)
                if self.properties.get('lookupFieldsFilter', []):
                    lookup = FilterExecutor.FilterExecutor().run(lookup, self.properties.get('lookupFieldsFilter', []),
                                                                 payload=payload)

                # use retain column when doing multiple vlookups on single frame to retain previous values
                restore_frame = data[[col for col in self.properties.get('restoreCols', []) if col in data.columns]]
                # DataFrame.update() works only on nan values
                for col in restore_frame:
                    restore_frame[col].replace(util.infer_dtypes(restore_frame[col]),np.nan, inplace=True)

                # Remove existing marker columns if exists in lookup data & re-init with new values
                for column, value in marker_cols.iteritems():
                    if column in lookup.columns:
                        del lookup[column]
                    lookup[column] = value
                    include_cols.append(column)
		
                # Remove if look up columns already exists in loaded data, avoid duplicate columns on re-look up
                result_field = data.columns.tolist()
                for col in include_cols:
                    if col in data.columns:
                        del data[col]

                    if col not in result_field:
                        result_field.append(col)
		#print data.head()
                lookup = lookup[lookup_field + include_cols]
                lookup_data = data.merge(lookup, left_on=data_field, right_on=lookup_field, how='left',
                                         suffixes=('', '_y'))
                
		for col in lookup_data.columns:
                    if '_y' in col:
                        del lookup_data[col]

                # Modify DataFrame in place using non-NA values from passed DataFrame.
                if not restore_frame.empty:
                    lookup_data.update(restore_frame, overwrite=False)

                for col in result_field:
                    lookup_data[col].fillna(util.infer_dtypes(lookup_data[col]), inplace=True)

                util.setValues(payload, self.properties['resultkey'], lookup_data)

        except Exception as e:
            payload['error'] = dict(elementName="VLookup", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class IncrementalLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "IncrementalLoader"
        self.name = "IncrementalLoader"
        self.properties = dict(source='', KeyColumns='', converters={})

    def run(self, payload, debug):
        try:
            if 'prevStmtDate' in payload.keys():
                data = payload[self.properties['source']].copy()
		data['CARRY_FORWARD'] = data['CARRY_FORWARD'].fillna('')

                key_cols = self.properties['keycolumns']
                current_df = data.copy()
                converters = self.properties.get('converters', {})

                for key, value in converters.iteritems():
                    converters[key] = eval(value)

                prev_exec_path = config.mftpath + "/{reconName}/{stmtDate}/OUTPUT/".format(
                    stmtDate=payload['prevStmtDate'], reconName=payload['reconName'])

                matched_df = pd.read_csv(prev_exec_path + self.properties['sourceTag'] + '.csv',
                                         converters=converters)

                # check if reversal records exists
                reversal_path = prev_exec_path + 'Self Matched.csv'
                if os.path.isfile(reversal_path):
                    reversal_records = pd.read_csv(reversal_path, converters=converters)
                    matched_df = pd.concat([matched_df, reversal_records]).drop_duplicates(subset=key_cols,
                                                                                           keep='first')

                # drop duplicates to avoid cartesian products
                if len(current_df):
			current_df = current_df.drop_duplicates(subset=key_cols, keep='first')
                left_out_data = current_df.merge(matched_df[key_cols], on=key_cols, indicator=True,
                                                 suffixes=('', '_y'),
                                                 how='outer')

                left_out_data['CARRY_FORWARD'].fillna('', inplace=True)
                left_out_data = left_out_data[
                    (left_out_data['_merge'] == 'left_only') | (left_out_data['CARRY_FORWARD'] == 'Y')]
                del left_out_data['_merge']

                merge_cols = key_cols + ['CARRY_FORWARD']
                data = data.merge(left_out_data[merge_cols], on=merge_cols, indicator=True, how='inner')

                data = data[data['_merge'] == 'both']
                del data['_merge']

                payload[self.properties['resultkey']] = data
            else:
                pass
        except Exception as e:
            payload['error'] = dict(elementName="IncrementalLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ReportGenerator(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ReportGenerator"

        self.properties = dict(source=None, filterConditions=[], columnNameList=[], reportName="", resultKey="",
                               drop="")

    def run(self, payload, debug):
        try:
            report_df = pd.DataFrame()
            for source in self.properties['sources']:
                org_df = Utilities().getValues(payload, source['source'])
                org_df.reset_index(inplace=True, drop=True)
                df = org_df.copy()

                # if empty frame ignore computations
                if df.empty:
                    continue

                # Applying the Filter Conditions
                if source.get('filterConditions', []):
                    df = FilterExecutor.FilterExecutor().run(df, source['filterConditions'], payload=payload)

                # if drop is set, removes the filtered records from original source frame
                if source.get('drop', False):
                    remaining_df = org_df[~org_df.index.isin(df.index)]
                    Utilities().setValues(payload, source['source'], remaining_df)

                # if frame is empty then concat with join_axes converts integer values to float
                if report_df.empty:
                    report_df = df
                else:
                    # incase if filters results in empty dataframe, to avoid loss of column index during concat use keycolumn
                    join_axes = pd.Index(
                        self.properties['keycolumns']) if 'keycolumns' in self.properties.keys() else df.columns
                    report_df = pd.concat([report_df, df], join_axes=[join_axes])

                # TODO, duplicate need to eliminate below block of code (already handled by drop)
                if self.properties.get('discardRecord', False):
                    report_df = report_df.reset_index(drop=True)
                    undiscarded = FilterExecutor.FilterExecutor().run(report_df, source['discardConditions'],
                                                                      payload=payload)
                    Utilities().setValues(payload, source['source'], set_value=undiscarded)
                    report_df = report_df[~report_df.index.isin(undiscarded.index)]

                if self.properties.get('writeToDB', False):
                    collection = self.properties.get('collection', 'report_details')
                    # clean up dataframe before posting to DB
                    report_df.fillna('', inplace=True)
                    report_df.columns = [c.replace('.', '') for c in report_df.columns]
                    data = np.array_split(report_df, 3)
                    for i in range(0, 3):
                        MongoClient(config.mongoHost)[config.databaseName][collection].insert(
                            {'reportName': self.properties.get('reportName', ""),
                             'statementDate': payload['statementDate'],
                             'source': source['sourceTag'], 'reconName': payload['reconName'],
                             "created": Utilities().getUtcTime(), 'partnerId': '54619c820b1c8b1ff0166dfc',
                             'details': data[i].to_dict(orient='records')})
                    # MongoClient(config.mongoHost)[config.databaseName][collection].insert(
                    #     {'reportName': self.properties.get('reportName', ""), 'statementDate': payload['statementDate'],
                    #      'source': source['sourceTag'], 'reconName': payload['reconName'],
                    #      "created": Utilities().getUtcTime(), 'partnerId': '54619c820b1c8b1ff0166dfc',
                    #      'details': report_df.to_dict(orient='records')})

            # Apply post filters after loading all the sources
            df = report_df.copy()
            if 'postFilters' in self.properties.keys():
                df = FilterExecutor.FilterExecutor().run(df, self.properties.get('postFilters', []), payload=payload)

            if self.properties.get('writeToFile', False):
                if 'custom_reports' not in payload.keys():
                    payload['custom_reports'] = {self.properties.get('reportName', "Report"): df}
                else:
                    payload['custom_reports'][self.properties.get('reportName', 'Report')] = df

        except Exception as e:
            payload['error'] = dict(elementName="ReportGenerator", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class GenerateReconSummary(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "SummaryReport"

        self.properties = dict(
            sources=[dict(sourceTag="", aggrCol=[], matched='all/any')], groupbyCol=[])

    def run(self, payload, debug):
        try:
            summary_dict = dict(reconName=payload['reconName'], statementDate=payload['statementDate'],
                                reconType=payload['reconType'], reportName="ReconSummary", details=[])

            summary_list = []
            summary_source=[]
            totalInput = 0
            reorder_cols = ["Source"]
            reorder_cols.extend(self.properties.get('groupbyCol', []))
            summary_cols = collections.OrderedDict(
                [("Matched Amount", 0), ("Matched Count", 0.0), ("UnMatched Count", 0),
                 ("UnMatched Amount", 0.0), ("Reversal Amount", 0.0), ("Reversal Count", 0),
                 ("Carry-Forward Matched Amount", 0.0), ("Carry-Forward Matched Count", 0),
                 ("Carry-Forward UnMatched Amount", 0.0), ("Carry-Forward UnMatched Count", 0)])

            for source in self.properties['sources']:
                data = Utilities().getValues(payload, 'results.' + source['sourceTag']).copy()

                # check if data exists (incremental loads may generate empty dataframes)
                if data.empty:
                    pass
                else:
                    totalInput += len(data)

                    slist,summarysource=self.compute_stats(data, groupbyCol=["Source"] + self.properties.get('groupbyCol', ['STATEMENT_DATE']))

                    summary_list.extend(slist)
                    summary_source.extend(summarysource)

            if len(summary_list) == 0:
                return

            summary_df = pd.DataFrame(summary_list)
            source_df = pd.DataFrame(summary_source)

            # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
            rename_mapper = {}
            for col in summary_df.columns:
                if 'Amount#' in col or 'Count#' in col:
                    aggr, match_status = tuple(col.split('#'))
                    rename_mapper[col] = match_status + ' ' + aggr
                else:
                    rename_mapper[col] = col.rstrip('#')

            summary_df.rename(columns=rename_mapper, inplace=True)

            # add missing summary cols to dataframe to avoid column alignment issues

            summary_df = self.missingSummary(summary_df, summary_cols)
            summary_source = self.missingSummary(source_df, summary_cols)

            # for column, value in summary_cols.iteritems():
            #     if column in summary_df.columns:
            #         summary_df[column] = summary_df[column].fillna(value=value)
            #     else:
            #         summary_df[column] = value

            # merge closing balances on sources if exists
            if 'closingBalances' in payload.keys():
                closing_balances = pd.DataFrame(payload['closingBalances'])
                summary_df = summary_df.merge(closing_balances, on='Source', how='left')
            else:
                summary_df['Closing Balance'] = 0.0

            # Re-order source columns to first index
            source = summary_df[reorder_cols]
            summary_df.drop(labels=reorder_cols, axis=1, inplace=True)
            summary_df = summary_df[summary_cols.keys() + ['Closing Balance']]

            for idx, value in enumerate(reorder_cols):
                summary_df.insert(idx, value, source.loc[:, value])
            if 'STATEMENT_DATE' in self.properties.get('groupbyCol', []):
                pass
            else:
                # payload['statementDate']=summary_df['STATEMENT_DATE']
                # summary_df.drop(labels=['STATEMENT_DATE'],axis=1,inplace=True)
                summary_df.insert(len(reorder_cols), 'Statement Date', payload['statementDate'])
            summary_df.insert(len(reorder_cols) + 1, 'Execution Date Time', datetime.datetime.now())
            summary_df.sort_values(by='Execution Date Time', inplace=True, ascending=False)
            summary_df.fillna('', inplace=True)

            payload['reconSummary'] = summary_df
            MongoClient(config.mongoHost)[config.databaseName]['custom_reports'].insert(summary_dict)

            doc = MongoClient(config.mongoHost)[config.databaseName]['recon_execution_details_log'].find_one(
                {"reconName": payload['reconName'], "jobStatus": "JobRunning"},
                sort=[('reconExecutionId', -1)])

            # Over writing Same dict details and writing Source wise summary
            summary_dict['details'] = summary_source.to_dict(orient='records')
            summary_dict['reconExecutionId'] = doc['reconExecutionId']
            summary_dict['totalInput'] = totalInput
            summary_dict['totalMatched'] = summary_df['Matched Count'].sum() + summary_df['Reversal Count'].sum() + \
                                           summary_df['Carry-Forward Matched Count'].sum()
            summary_dict['totalUnmatched'] = summary_df['UnMatched Count'].sum() + summary_df[
                'Carry-Forward UnMatched Count'].sum()
            summary_dict['totalMatchedAmount'] = summary_df['Matched Amount'].sum() + summary_df[
                'Reversal Amount'].sum() + summary_df['Carry-Forward Matched Amount'].sum()
            summary_dict['totalUnmatchedAmount'] = summary_df['UnMatched Amount'].sum() + summary_df[
                'Carry-Forward UnMatched Amount'].sum()
            MongoClient(config.mongoHost)[config.databaseName]['recon_summary'].insert(summary_dict)



        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="GenerateReconSummary", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    def missingSummary(self, summary, summary_cols):
        # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
        rename_mapper = {}
        for col in summary.columns:
            if 'Amount#' in col or 'Count#' in col:
                aggr, match_status = tuple(col.split('#'))
                rename_mapper[col] = match_status + ' ' + aggr
            else:
                rename_mapper[col] = col.rstrip('#')

        summary.rename(columns=rename_mapper, inplace=True)

        # add missing summary cols to dataframe to avoid column alignment issues
        for column, value in summary_cols.iteritems():
            if column in summary.columns:
                summary[column] = summary[column].fillna(value=value)
            else:
                summary[column] = value
        return summary

    # def compute_stats(self, source={}, data=None, groupbyCol=[]):
    #     default_group = ['Source']
    #     default_group.extend(groupbyCol)
    #
    #     data['matched_status'] = ''
    #     data['Count'] = 1
    #     data['Amount'] = data[source['aggrCol']].fillna(0.0).sum(axis=1)
    #     data.rename(columns={"SOURCE": "Source"}, inplace=True)
    #
    #     # segregate matched and un-matched transactions
    #     match_cond = []
    #     un_match_cond = []
    #     for col in data.columns:
    #         if ' Match' in col and 'Self Matched' != col:
    #             match_cond.append("(data['%s'] == 'MATCHED')" % col)
    #             un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
    #         else:
    #             pass
    #
    #     # if matched == 'any', if either of the source is matched assume the records to be matched.
    #     if source.get('matched', '') == 'any':
    #         data.loc[eval(" | ".join(match_cond)), "matched_status"] = 'Matched'
    #         data.loc[eval(" & ".join(un_match_cond)), "matched_status"] = 'UnMatched'
    #     else:
    #         data.loc[eval(" & ".join(match_cond)), "matched_status"] = 'Matched'
    #         data.loc[eval(" | ".join(un_match_cond)), "matched_status"] = 'UnMatched'
    #
    #     if 'Self Matched' in data.columns:
    #         data['Self Matched'] = data['Self Matched'].fillna('')
    #         data.loc[data['Self Matched'] == 'MATCHED', 'matched_status'] = 'Reversal'
    #     else:
    #         pass
    #
    #     if 'CARRY_FORWARD' in data.columns:
    #         data.loc[(data['CARRY_FORWARD'] == 'Y') & (
    #             data['matched_status'] == 'Matched'), 'matched_status'] = 'Carry-Forward Matched'
    #         data.loc[(data['CARRY_FORWARD'] == 'Y') & (
    #             data['matched_status'] == 'UnMatched'), 'matched_status'] = 'Carry-Forward UnMatched'
    #     else:
    #         pass
    #
    #     pivot_df = pd.pivot_table(data, index=default_group, columns=['matched_status'], values=['Count', 'Amount'],
    #                               aggfunc=np.sum).reset_index()
    #     pivot_df.columns = pivot_df.columns.to_series().str.join('#')
    #     summary_list = pivot_df.to_dict(orient='records')
    #
    #     return summary_list


    def compute_stats(self, data=None, aggrCol=[], groupbyCol=[], matchType='all'):
        data['matched_status'] = ''
        data['Count'] = 1
        data['Amount'] = data[aggrCol].fillna(0.0).sum(axis=1)
        data.rename(columns={"SOURCE": "Source"}, inplace=True)

        # segregate matched and un-matched transactions
        match_cond = []
        un_match_cond = []
        for col in data.columns:
            if ' Match' in col and 'Self Matched' != col:
                match_cond.append("(data['%s'] == 'MATCHED')" % col)
                un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
            else:
                pass

        # if matched == 'any', if either of the source is matched assume the records to be matched.
        if matchType == 'any':
            data.loc[eval(" | ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" & ".join(un_match_cond)), "matched_status"] = 'UnMatched'
        else:
            data.loc[eval(" & ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" | ".join(un_match_cond)), "matched_status"] = 'UnMatched'

        if 'Self Matched' in data.columns:
            data['Self Matched'] = data['Self Matched'].fillna('')
            data.loc[data['Self Matched'] == 'MATCHED', 'matched_status'] = 'Reversal'
        else:
            pass

        if 'CARRY_FORWARD' in data.columns:
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                data['matched_status'] == 'Matched'), 'matched_status'] = 'Carry-Forward Matched'
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                data['matched_status'] == 'UnMatched'), 'matched_status'] = 'Carry-Forward UnMatched'
        else:
            pass

        # Recon Summary For Date Wise
        pivot_df = pd.pivot_table(data, index=groupbyCol, columns=['matched_status'], values=['Count', 'Amount'],
                                  aggfunc=np.sum).reset_index()

        # Recon Summary For source Wise
        pivot_source = pd.pivot_table(data, index=['Source'], columns=['matched_status'], values=['Count', 'Amount'],
                                      aggfunc=np.sum).reset_index()

        pivot_df.columns = pivot_df.columns.to_series().str.join('#')
        pivot_source.columns = pivot_source.columns.to_series().str.join('#')

        summary_list = pivot_df.to_dict(orient='records')
        source_summary = pivot_source.to_dict(orient='records')

        return summary_list, source_summary
class RenameColumns(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "RenameColumns"
        self.properties = dict(source="", columns={}, resultkey='', dropDuplicates='', dropCoulmns='')

    def run(self, payload, debug):
        try:
            df = Utilities().getValues(payload, self.properties.get('source', ''))
            df.rename(columns=self.properties.get('columns', {}), axis=1, inplace=True)

            if self.properties.get('dropDuplicates', False):
                df = df.columns[~df.columns.duplicated(keep=self.properties.get('keep', 'first'))]

            if self.properties.get('dropCoulmns', False):
                df = df[list(self.properties.get('columns', {}).values)]
                # df.drop(labels=list(self.properties.get('columns',{}).values),inplace=True,axis=1)

            payload[self.properties['resultkey']] = df

        except Exception as e:
            pass


class SourceConcat(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Concat"
        self.properties = dict(sourceList="", resultKey="dataframe")

    def run(self, payload, debug):
        try:

            dflist = []
            columns = []
            for source in self.properties['sourceList']:
                df = Utilities().getValues(payload, ref=source, default=pd.DataFrame())

                if not df.empty:
                    if len(df.columns) > len(columns):
                        columns = df.columns
                    dflist.append(df)

            if dflist:
                df = pd.concat([df for df in dflist], join_axes=[columns])
            else:
                df = pd.DataFrame()

            Utilities().setValues(payload, self.properties.get('resultKey', ''), set_value=df)

        except Exception as e:
            payload['error'] = dict(elementName="SourceConcat", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class MultiLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "MultiLoader"
        self.properties = dict(loadType="", source="", feedPattern="", feedParams=dict(), feedFilters=[],
                               resultkey="dataframe")

    def run(self, payload, debug):
        try:

            feedfac = FeedLoader.FeedLoaderFactory()
            stmtDate = payload['statementDate']
            findFiles = FindFiles.FindFiles(stmtDate)

            for property in self.properties['sources']:
                if property.get('ifPreloadScript', False):
                    getattr(PreLoaderScript(stmtDate, payload['reconName']), property.get('preLoadFunc'))()
                inst = feedfac.getInstance(property.get('loadType', ''))
                inst_files = findFiles.findFiles(feedPath=payload.get('source_path', ''),
                                                 fpattern=property.get('feedPattern', ''),
                                                 raiseError=property.get('errors', True))

                feedParams = property.get('feedParams', dict())
                feedParams['payload'] = payload
                inst = inst.loadFile(inst_files, property.get('feedParams', dict()))
                inst['SOURCE'] = property.get('source', '')
                inst['STATEMENT_DATE'] = stmtDate
                if property.get('feedFilters', []):
                    inst = FilterExecutor.FilterExecutor().run(inst, property.get('feedFilters', []), payload=payload)

                if 'disableCarryFwd' in property.keys() and property['disableCarryFwd']:
                    pass
                else:
                    if 'prevStmtDate' in payload.keys():
                        cfInst = feedfac.getInstance('CarryForward')
                        cfData = cfInst.loadFile([], {"payload": payload, "properties": property})
                        cfData['CARRY_FORWARD'] = 'Y'
                        inst = pd.concat([inst, cfData], join_axes=[cfData.columns])
                if property.get('resultkey', '') in payload.keys():

                    payload[property.get("resultkey", '')] = pd.concat([payload[property.get("resultkey", '')], inst],
                                                                       join_axes=[payload[property.get("resultkey",
                                                                                                       '')].columns])
                else:
                    payload[property.get("resultkey", '')] = inst

                #print "inst", len(payload[property.get("resultkey", '')])

        except Exception as e:
            payload['error'] = dict(elementName="MultiLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())
            print payload['error']


class Splitdf(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "Spliter"
        self.properties = dict(keycolumn='', source='')

    def run(self, payload, debug):
        try:
            df = payload[self.properties.get('source')]
            uniquelist = df[self.properties.get('keycolumn')].unique()

            for column in uniquelist:
                payload['source' + column] = df[df[self.properties.get('keycolumn')] == column]

        except Exception as e:
            pass


class AddPostMatchRemarks(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "AddMatchingRemarks"
        self.name = "AddMatchingRemarks"
        self.properties = dict(source='', remarkCol='',
                               remarks={"matchRemarks": "", "selfRemarks": ""},
                               how='all/any', resultKey='')

    def run(self, payload, debug):
        try:
            data = Utilities().getValues(payload, self.properties['source']).copy()
            remarks = self.properties.get('remarks', {})
            remarksCol = self.properties.get('remarkCol', 'Remarks')
            match_cols = []
            unmatch_cols = []
            psuedo_cols = []

            if data.empty:
                return

            for column in data.columns:
                if ' Match' in column and 'Self Matched' != column:
                    src_col = column.replace(' Match', '')
                    psuedo_col = src_col + "#post_remarks#"
                    psuedo_cols.append(psuedo_col)
                    data.loc[data[column].fillna('') == 'UNMATCHED', psuedo_col] = src_col
                    data.loc[pd.isna(data[psuedo_col]), psuedo_col] = ''

                    match_cols.append("(data['%s'] == 'MATCHED')" % column)
                    unmatch_cols.append("(data['%s'] == 'UNMATCHED')" % column)
                else:
                    pass

            data.drop(columns=remarksCol, inplace=True, errors='ignore')
            data[remarksCol] = ''
            if match_cols:
                if self.properties.get('how', 'all') == 'all':
                    data.loc[eval(" & ".join(match_cols)), remarksCol] = remarks.get('matchRemarks', 'Matched')
                    data.loc[eval(" | ".join(unmatch_cols)), remarksCol] = data[psuedo_cols].apply(
                        lambda x: 'Not in ' + ', '.join([ix for ix in x if ix]), axis='columns')
                else:
                    data.loc[eval(" | ".join(match_cols)), remarksCol] = remarks.get('matchRemarks', 'Matched')
                    data.loc[eval(" & ".join(unmatch_cols)), remarksCol] = data[psuedo_cols].apply(
                        lambda x: 'Not in ' + ', '.join([ix for ix in x if ix]), axis='columns')

            if 'Self Matched' in data.columns:
                data['Self Matched'] = data['Self Matched'].fillna('')
                data.loc[data['Self Matched'] == 'MATCHED', remarksCol] = remarks.get('selfRemarks', 'Self Matched')

            data.drop(labels=psuedo_cols, axis='columns', inplace=True, errors='ignore')
            Utilities().setValues(payload, self.properties.get('resultKey', self.properties['source']), data)
        except Exception as e:
            payload['error'] = dict(elementName="AddPostMatchRemarks", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ReferenceLoader(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ReferenceLoader"
        self.properties = dict(loadType="", source="", fileName="", feedParams=dict(), feedFilters=[],
                               resultkey="dataframe" ,prevstatementDate = '')

    def run(self, payload, debug):
        try:
            feedfac = FeedLoader.FeedLoaderFactory()
            instance = feedfac.getInstance(self.properties.get('loadType', ''))
            prevstatementDate = self.properties['prevstatementDate']
            if  prevstatementDate:
                if  'prevStmtDate' in payload.keys():
                    pathnow= config.basepath+'mft'+'/'+payload['reconName']+'/'+payload['prevStmtDate']+'/'+'OUTPUT'+'/'
                    file_names = [pathnow+file_name for file_name in self.properties["fileName"]]
                else:
                    file_names = []
            else:
                file_names = [config.basepath + file_name for file_name in self.properties["fileName"]]
            if len(file_names )>0:
                inst = instance.loadFile(file_names, self.properties.get('feedParams', dict()))
                if self.properties.get('feedFilters', []):
                    inst = FilterExecutor.FilterExecutor().run(inst, self.properties.get('feedFilters', []),
                                                               payload=payload)
                payload[self.properties["resultkey"]] = inst
        except Exception as e:
            payload['error'] = dict(elementName="ReferenceLoader", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ValidateMasterDump(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ValidateMasterDump"
        self.properties = dict(source="", masterType="", keyColumn=[], insertCols=[], resultKey='', filters=[])

    def run(self, payload, debug):
        try:
            util = Utilities()
            master_dump = MongoClient(config.mongoHost)[config.databaseName]['master_dumps']
            df = util.getValues(payload, self.properties['source']).copy()
            df['type'] = self.properties['masterType']
            df['reconName'] = payload['reconName']
            df['statementDate'] = payload['statementDate']
            df['dropOnRollback'] = self.properties.get('dropOnRollback', False)
            result_list = []

            # exclude transaction that need to participate in validation
            if 'filters' in self.properties.keys():
                df = FilterExecutor.FilterExecutor().run(df, expressions=self.properties.get('filters', []),
                                                         payload=payload)

            if df.empty:
                pass
            else:
                insert_list = df[
                    self.properties['insertCols'] + ['reconName', 'statementDate', 'type', 'dropOnRollback']].to_dict(
                    orient='records')

                for insert in insert_list:
                    cursor = master_dump.find({key: insert[key] for key in self.properties.get('keyColumn', [])})
                    if cursor.count() > 0:
                        result_list.append(insert)
                    else:
                        insert["partnerId"] = "54619c820b1c8b1ff0166dfc"
                        insert["created"] = util.getUtcTime()
                        master_dump.insert(insert)

                if result_list:
                    result = pd.DataFrame(result_list)[self.properties['insertCols']]
                else:
                    result = pd.DataFrame(columns=self.properties['insertCols'])

                util.setValues(payload, ref=self.properties['resultKey'], set_value=result)
        except Exception as e:
            payload['error'] = dict(elementName="UpdateMasterDump", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class LoadExternalReport(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "LoadFile"
        self.name = "LoadExternalReport"
        self.properties = dict(reconName='')

    def run(self, payload, debug):
        try:
            filelist=list()
            feedfac = FeedLoader.FeedLoaderFactory()
            inst = feedfac.getInstance(self.properties.get('loadType', ''))
            # feedParams = self.properties.get('feedParams', dict())
            # file=config.mftpath+os.sep+"{}".format(self.properties['reconType'])+payload['statementDate'].strftime('%d%m%Y')
            file="{mftpath}/{reconName}/{stmtDate}/{filepath}".format(mftpath=config.mftpath,reconName=self.properties['reconName'],stmtDate=payload['statementDate'].strftime('%d%m%Y'),filepath=self.properties['filepath'])
            filelist.append(file)
            inst = inst.loadFile(filelist, self.properties.get('feedParams', dict()))

            payload[self.properties['resultKey']]=inst

        except Exception as e:
            payload['error'] = dict(elementName="LoadExternalReport", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class LoadIncrementalData(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "LoadIncrementalData"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(filters=[], source='', sourceTag='', uniqCol='', dateColumn='', amountColumn='',
                               DrCrIndicator='')

    def run(self, payload, debug):
        try:
            df = Utilities().getValues(payload, self.properties['source']).copy()
            diff = pd.DataFrame()

            df.loc[df[self.properties.get('DrCrIndicator')] == 'Dr', self.properties['amountColumn']] *= -1
            if df[self.properties.get('dateColumn')].dtype is not np.datetime64:
                df[self.properties.get('dateColumn')] = pd.to_datetime(df[self.properties.get('dateColumn')],
                                                                       format='%d/%m/%Y')
            # stmtDate = datetime.datetime.strptime(self.properties.get("dateColumn", ""), '%d-%b-%Y')
            originaldf = df.copy()
            df = df.groupby(self.properties.get('dateColumn'))[self.properties['amountColumn']].sum().reset_index()

            # df['openingBalance'] = [payload['openingBal']] + df[self.properties.get('amountColumn')].tolist()[:-1]

            # df['ClosingBalance'] = df[self.properties['amountColumn']]

            # df = df.groupby(['dateColumn'])[self.properties['amountColumn']].sum().reset_index()

            df['ClosingBalance'] = df[self.properties['amountColumn']].cumsum()
            df['ClosingBalance']=df["ClosingBalance"]+float(payload['openingBal'])
            df['openingBalance']= pd.Series([payload['openingBal']]+df["ClosingBalance"].tolist()[:-1])
            #df['openingBalance'] = [payload['openingBal']] + [0.0] * (len(df) - 1)
            for index,row in df.iterrows():
                currdatedf = originaldf[originaldf[self.properties.get('dateColumn')] == row[self.properties["dateColumn"]]]
                record=MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].find_one(
                            {"reconName": payload['reconName'], "source": self.properties.get('sourceTag'),
                             'statementDate': row[self.properties["dateColumn"]]})
                if record:
                    if record["ClosingBalance"]!=row['ClosingBalance']:
                        diff = diff.append(
                            currdatedf[~currdatedf[self.properties.get('uniqCol')].isin(record['voucherlist'])],
                            ignore_index=True)
                        voucherlist = currdatedf[self.properties.get('uniqCol')].tolist()
                        doc=dict()
                        doc['ClosingBalance'] = row['ClosingBalance']
                        doc['openingBalance'] = row['openingBalance']
                        doc['voucherlist'] = voucherlist
                        MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].update(
                            {"_id":record["_id"]},doc)
                    else:
                        continue
                else:
                    doc=dict()
                    voucherlist = currdatedf[self.properties.get('uniqCol')].tolist()
                    doc['ClosingBalance'] = row['ClosingBalance']
                    doc['openingBalance'] = row['openingBalance']
                    doc['voucherlist'] = voucherlist
                    doc['reconName'] = payload['reconName']
                    doc['source'] = self.properties.get('sourceTag')
                    doc['statementDate']=row[self.properties.get('dateColumn')]
                    # data.columns = ['openingBalance', 'ClosingBalance', 'voucherlist', 'reconName', 'sourceTag']

                    MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].insert(
                        doc)

            originaldf = originaldf[(originaldf[self.properties.get('dateColumn')] == payload['statementDate']) | (originaldf.get('CARRY_FORWARD', '') == 'Y')]
            df = pd.concat([df, diff], join_axes=[df.columns])
            payload[self.properties.get('resultKey')] = df

        except Exception as e:
            payload['error'] = dict(elementName="LoadIncrementalData", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ValidateClosingBalance(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "ValidateClosingBalance"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(filters=[], source='', sourceTag='')

    def run(self, payload, debug):
        try:
            prev_closing_bal = -882954137.90
            df = Utilities().getValues(payload, self.properties['source'])
            df = df.reset_index(drop=True)
            if 'prevStmtDate' in payload.keys():
                prev_stmt_date = datetime.datetime.strptime(payload['prevStmtDate'], '%d%m%Y')
                doc = MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].find_one(
                    {"stmtDate": prev_stmt_date, "reconName": payload['reconName'],
                     "source": self.properties.get('sourceTag')})
                if doc:
                    prev_closing_bal = doc['closingBalance']
            if "filters" in self.properties and len(df) > 0:
                df = FilterExecutor.FilterExecutor().run(df, self.properties.get('filters'), payload)

            creditsum = eval(self.properties.get('creditexp'))

            debitsum = eval(self.properties.get('debitexp'))
            openingbal = float(eval(self.properties.get('openingBalExp')))
            closingbal = float(eval(self.properties.get('closingBalExp')))
            balance = openingbal - debitsum + creditsum - closingbal
            if balance != 0:
                raise ValueError(self.properties.get('sourceTag') + 'Balance Not Matching')
        except Exception as e:
            payload['error'] = dict(elementName="ValidateClosingBalance", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class DuplicateMatch(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "DuplicateMatch"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="", subsetfilter=[])],
                               matchResult=None)

    def run(self, payload, debug):
        try:
            print("In Duplicate Match")
            result = {}
            remainder = []
            for i in range(0, len(self.properties["sources"])):
                if "subsetfilter" in self.properties["sources"][i] and len(
                        self.properties["sources"][i]["subsetfilter"]) > 0:
                    source = self.properties["sources"][i]
                    data = Utilities().getValues(payload, source['source'])

                    data.reset_index(drop=True, inplace=True)
                    filtereddata = FilterExecutor.FilterExecutor().run(data,
                                                                       self.properties["sources"][i]["subsetfilter"],
                                                                       payload, "Filter" + str(i))

                    remainingdata = data[~data.index.isin(filtereddata.index)]
                    remainder.append(remainingdata)
                    Utilities().setValues(payload, ref=source["source"], set_value=filtereddata)
                else:
                    remainder.append(pd.DataFrame())
            if len(self.properties["sources"]) == 1:
                return

            for i in range(0, len(self.properties["sources"]) - 1):
                leftsource = self.properties["sources"][i]
                leftcolumns = leftsource["columns"]
                for j in range(i + 1, len(self.properties["sources"])):
                    rightsource = self.properties["sources"][j]
                    rightcolumns = rightsource["columns"]
                    rightdata = Utilities().getValues(payload, rightsource['source'])

                    leftdata = Utilities().getValues(payload, leftsource['source'])

                    leftmatchColumns = leftcolumns.get('matchColumns', [])
                    rightmatchColumns = rightcolumns.get('matchColumns', [])

                    leftjson = leftdata.to_dict(orient='records', into=collections.OrderedDict())
                    rightjson = rightdata.to_dict(orient='records', into=collections.OrderedDict())

                    for x in leftjson:
                        for y in rightjson:
                            if leftsource["sourceTag"] + " Match" in y and y[
                                        leftsource["sourceTag"] + " Match"] == "MATCHED":
                                continue

                            matched = True
                            for idx in range(0, len(leftmatchColumns)):
                                if x[leftmatchColumns[idx]] != y[rightmatchColumns[idx]]:
                                    matched = False
                                    break

                            if matched:
                                x[rightsource["sourceTag"] + " Match"] = "MATCHED"
                                y[leftsource["sourceTag"] + " Match"] = "MATCHED"

                    payload[self.properties["sources"][i]["source"]] = pd.DataFrame.from_dict(leftjson)
                    payload[self.properties["sources"][j]["source"]] = pd.DataFrame.from_dict(rightjson)

            for i in range(0, len(self.properties["sources"])):
                tmp_df = payload[self.properties["sources"][i]["source"]]

                # set nan values to un-matched
                for col in tmp_df:
                    if ' Match' in col:
                        tmp_df.loc[tmp_df[col].isnull(), col] = 'UNMATCHED'

                # tmp_df contains match columns(<src> Match), join_axes on tmp_df to retain columns
                # else if tmp_df is empty join_axes join on remainder else data frame becomes empty if done on tmp_df
                result[self.properties["sources"][i]["sourceTag"]] = pd.concat([tmp_df, remainder[i]],
                                                                               join_axes=[remainder[i].columns
                                                                                          if tmp_df.empty
                                                                                          else tmp_df.columns]
                                                                               ).reset_index(drop=True)

            # Avoid over-writing of existing data
            if self.properties['matchResult'] in payload.keys():
                for key, value in result.iteritems():
                    payload[self.properties['matchResult']][key] = value
            else:
                payload[self.properties['matchResult']] = result
        except Exception, e:
            payload['error'] = dict(elementName="DuplicateMatch", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class ComputeClosingBalance(FlowElement):
    def __init__(self):
        self.type = "Computation"
        self.name = "ComputeClosingBalance"
        self.properties = [dict(source='', resultKey='', filterConditions=[], addToResult=True)]

    def run(self, payload, debug):
        try:
            for source in self.properties.get('sources', ''):
                df = Utilities().getValues(payload, source['source'])
                prev_closing_bal = 0.0

                if 'prevStmtDate' in payload.keys():
                    prev_stmt_date = datetime.datetime.strptime(payload['prevStmtDate'], '%d%m%Y')
                    doc = MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].find_one(
                        {"stmtDate": prev_stmt_date, "reconName": payload['reconName'],
                         "source": source['sourceTag']})
                    if doc:
                        prev_closing_bal = doc['closingBalance']

                if source.get('filterConditions', []):
                    df = FilterExecutor.FilterExecutor().run(df, source['filterConditions'], payload=payload)

                closing_bal = prev_closing_bal + df[source['keyColumn']].sum()

                if 'closingBalances' not in payload.keys():
                    payload['closingBalances'] = []

                payload['closingBalances'].append({"Source": source['sourceTag'], "Closing Balance": closing_bal})

                if self.properties.get('addToResult', False):
                    df['ClosingBalance'] = closing_bal
                    Utilities().setValues(payload, source['resultkey'], set_value=df)

                MongoClient(config.mongoHost)[config.databaseName]['closingBalance'].insert(
                    {"closingBalance": closing_bal, "stmtDate": payload['statementDate'],
                     "reconName": payload['reconName'], "source": source['sourceTag']})

        except Exception, e:
            payload['error'] = dict(elementName="ComputeClosingBalance", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class MergeAndCompare(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "MergeAndCompare"
        col = dict(keyColumns=[], sumColumns=[], matchColumns=[])
        self.properties = dict(sources=[dict(source=None, columns=col, sourceTag="")],
                               matchResult=None)

    def run(self, payload, debug):
        print("In MergeAndCompare")
        result = {}
        remainder = []
        if len(self.properties["sources"]) == 1:
            return
        sourcedict={}
        duplicates={}
        for i in range(0, len(self.properties["sources"])):
            source = self.properties["sources"][i]
            columns = source["columns"]
            fields=columns.get("keyColumns",[])
            data = Utilities().getValues(payload, source['source'])
            data = data.fillna("")
            for x in data.columns:
                if x in ['STATEMENT_DATE']:
                    continue
                else:
                    data[x]=data[x].astype(str)

            datalist=data.to_dict(orient="records")
            sourcedict[source["source"]]={}
            duplicates[source["source"]]=[]
            for x in datalist:
                keyval = ""
                for y in fields:
                    if len(keyval) > 0:
                        keyval += ":" + str(x[y])
                    else:
                        keyval = str(x[y])
                if keyval in sourcedict[source["source"]]:
                    duplicates[source["source"]].append(x)
                else:
                    sourcedict[source["source"]][keyval]=x
        for i in range(0, len(self.properties["sources"]) - 1):
            leftsource = self.properties["sources"][i]
            leftcolumns = leftsource["columns"]
            leftdata=sourcedict[leftsource['source']]
            for j in range(i + 1, len(self.properties["sources"])):
                rightsource = self.properties["sources"][j]
                rightcolumns = rightsource["columns"]
                rightdata = sourcedict[rightsource['source']]
                leftmatchcolumns = leftcolumns.get("matchColumns", [])
                rightmatchcolumns= rightcolumns.get("matchColumns", [])

                for key in leftdata.keys():
                    if key in rightdata:
                        leftrow=leftdata[key]
                        if type(leftrow)==str:
                            continue
                        leftrow[rightsource['sourceTag']+ " Matched Columns"]=""
                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] = ""
                        rightrow=rightdata[key]
                        rightrow[leftsource['sourceTag'] + " Matched Columns"]=""
                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] = ""
                        for i in range(0,len(leftmatchcolumns)):
                            if leftmatchcolumns[i] == '' or rightmatchcolumns[i] == '':
                                continue
                            elif str(leftrow[leftmatchcolumns[i]]).strip()==str(rightrow[rightmatchcolumns[i]]).strip():
                                leftrow[rightsource['sourceTag'] + " Matched Columns"]+=","+rightmatchcolumns[i]
                                rightrow[leftsource['sourceTag'] + " Matched Columns"]+=","+leftmatchcolumns[i]
                            else:
                                if not str(leftrow[leftmatchcolumns[i]]).strip():
                                    if not str(rightrow[rightmatchcolumns[i]]).strip():
                                    # leftrow[leftmatchcolumns[i]] = '----'
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                i] + " with value " + \
                                                                                            '----' + " not same as " + \
                                                                                            leftmatchcolumns[
                                                                                                i] + "with value " + '----' + ","
                                    else:
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    rightrow[
                                                                                                        rightmatchcolumns[
                                                                                                            i]] + " not same as " + \
                                                                                                    leftmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                else:
                                    if not str(rightrow[rightmatchcolumns[i]]).strip():
                                        leftrow[rightsource['sourceTag'] + " UnMatched Columns"] += rightmatchcolumns[
                                                                                                    i] + " with value " + \
                                                                                            '----' + " not same as " + \
                                                                                                leftmatchcolumns[
                                                                                                    i] + "with value " + \
                                                                                                leftrow[
                                                                                                    leftmatchcolumns[
                                                                                                        i]] + ","

                                if not str(rightrow[rightmatchcolumns[i]]).strip():
                                    if not str(leftrow[leftmatchcolumns[i]]).strip():
                                    # rightrow[rightmatchcolumns[i]]='----'
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                    i] + " with value " + \
                                                                                                '----' + "not same as " + \
                                                                                                rightmatchcolumns[
                                                                                                    i] + "with value " + '----' + ","
                                    else:
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                        i] + " with value " + \
                                                                                                    leftrow[
                                                                                                        leftmatchcolumns[
                                                                                                            i]] + "not same as " + \
                                                                                                    rightmatchcolumns[
                                                                                                        i] + "with value " + '----' + ","
                                else:
                                    if not str(leftrow[leftmatchcolumns[i]]).strip():
                                        rightrow[leftsource['sourceTag'] + " UnMatched Columns"] += leftmatchcolumns[
                                                                                                    i] + " with value " + \
                                                                                                '----' + "not same as " + \
                                                                                                rightmatchcolumns[
                                                                                                    i] + "with value " + \
                                                                                                rightrow[
                                                                                                    rightmatchcolumns[
                                                                                                        i]] + ","


                                leftrow[rightsource['sourceTag'] + " UnMatched Columns"]+=rightmatchcolumns[i]+" with value "+ rightrow[rightmatchcolumns[i]]+" not same as "+leftmatchcolumns[i]+"with value "+leftrow[leftmatchcolumns[i]]+","
                                rightrow[leftsource['sourceTag'] + " UnMatched Columns"]+=leftmatchcolumns[i]+" with value "+ leftrow[leftmatchcolumns[i]]+"not same as "+rightmatchcolumns[i]+"with value "+rightrow[rightmatchcolumns[i]]+","
                    else:
                        leftdata[key][rightsource['source']+ " Missing"]="True"
                        # rightdata[key][leftsource['source']+ " Missing"]="True"

                sourcedict[leftsource['source']]=leftdata
                sourcedict[rightsource['source']] = rightdata

        for i in range(0, len(self.properties["sources"])):
            result[self.properties["sources"][i]["sourceTag"]] = pd.DataFrame(list(sourcedict[self.properties["sources"][i]["source"]].values()))
            payload['duplicateStore'] = {}
            for key in duplicates.keys():
                payload['duplicateStore'][key+" duplicates"] = pd.DataFrame(duplicates[key])

        payload[self.properties['matchResult']] = result


class MasterDumpHandler(FlowElement):
    def __init__(self):
        self.type = "MasterDumpHandler"
        self.name = "MasterDumpHandler"
        self.properties = dict(source="", masterType="", operation="insert / load", resultKey='', dropOnRollback=True,
                               insertCols=[], timedelta="in days")

    def run(self, payload, debug):
        try:
            master_dumps = MongoClient(config.mongoHost)[config.databaseName]['master_dumps']

            if self.properties.get('operation', None) is None:
                raise ValueError('operation is mandatory input "insert" / "find"')
            elif self.properties['operation'] == 'find':
                df = Utilities().getValues(payload, self.properties['inputdf']).copy()
                if 'SYS_TXN_ID' in df.columns:
                    del df['SYS_TXN_ID']
                df_json = json.loads(df.to_json(orient='records'))
                for col in df.columns.tolist():
                    if np.issubdtype(df[col].dtype, np.datetime64):
                        df_json=[{col:datetime.datetime.fromtimestamp(item[col]/1000).replace(hour=0,minute=0,second=0)} for item in df_json]


                master_data = []
                for i in df_json:
                    master_data.extend(list(master_dumps.find(i)))
                payload[self.properties['resultKey']] = pd.DataFrame(master_data)
            elif self.properties['operation'] == 'insert':
                source = Utilities().getValues(payload, self.properties['source']).copy()
                for col in source.columns.tolist():
                    source[col].fillna(Utilities().infer_dtypes(source[col]), inplace=True)

                if self.properties.get('insertCols', []):
                    source = source[self.properties['insertCols']]

                source['type'] = self.properties['masterType']
                source['reconName'] = payload['reconName']
                source['statementDate'] = payload['statementDate']
                source["created"] = datetime.datetime.strptime(datetime.datetime.now().strftime('%d-%b-%Y'), '%d-%b-%Y')
                source['dropOnRollback'] = self.properties.get('dropOnRollback', False)
                # clean dataframe values before saving to db
                # source.replace([np.nan, 'nan', 'NaT'], '', inplace=True)
                master_dumps.insert(source.to_dict(orient='records'))
            elif self.properties['operation'] == 'load':
                master_data = list(
                    master_dumps.find({'type': self.properties['masterType'], 'reconName': payload['reconName']}))
                payload[self.properties['resultKey']] = pd.DataFrame(master_data)
            else:
                raise ValueError("Invalid operation %s not implemented" % self.properties['operation'])

            # if timedelta is defined removed, records older than days specified in time delta
            delta_days = self.properties.get('timedelta', None)
            if delta_days:
                delta_date = datetime.datetime.strptime(datetime.datetime.now().strftime('%d-%b-%Y'), '%d-%b-%Y') \
                             - datetime.timedelta(delta_days)
                master_dumps.remove({"created": {'$lte': delta_date}})

        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="MasterDumpHandler", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())




class DataFiller(FlowElement):
    def __init__(self):
        self.type = "DataFiller"
        self.name = "DataFiller"
        self.properties = dict(template='', alias={'<payload_key>': "<alias_name>"}, resultKey='')

    def run(self, payload, debug):
        try:
            template = config.basepath + '/templates/' + self.properties.get('template', '')
            util = Utilities()
            if os.path.isfile(template):
                template_df = pd.read_csv(template, dtype=str).fillna('')
                template_df = template_df.replace(["\\n", "\\r"], " ", regex=True)

                # initialize alias if defined.
                for key, value in self.properties.get('alias', {}).iteritems():
                    exec "%s = util.getValues(payload, '%s')" % (value, key) in globals(), locals()

                for column in template_df.columns:
                    mask = template_df[column].str.match("{.*}")
                    index = template_df[mask].index.tolist()

                    if index:
                        template_df[column] = template_df[column].astype(str).str.replace("{|}", '')
                        for idx in index:
                            template_df.loc[idx, column] = eval(template_df.loc[idx, column])

                if self.properties.get('resultKey', '') == '':
                    logger.info("Result Key Not defined in element 'DataFiller ..'")
                else:
                    payload[self.properties['resultKey']] = template_df
            else:
                raise Exception("Template file not found define.")

        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="DataExtracter", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class GEFUGenerator(FlowElement):
    def __init__(self):
        self.type = "GEFUGenerator"
        self.name = "GEFUGenerator"
        self.properties = dict(source='')

    def run(self, payload, debug):
        try:
            getattr(GEFUHandler(payload['reconName'], payload['statementDate'].strftime('%d%m%Y')),
                    '_%s' % config.bank_name)(payload[self.properties['source']])
        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="GEFUGenerator", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())


class OuterMerge(FlowElement):
    def __init__(self):
        self.type = "OuterMerge"
        self.name = "OuterMerge"
        self.properties = dict(template='', resultKey='')

    def run(self, payload, debug):
        try:
            leftsource = self.properties.get('left')
            leftdata = Utilities().getValues(payload, leftsource['source'], sep='.')
            leftcolumns=leftdata.columns
            if leftsource.get('addSourceTag', True):
                leftdata.columns = [str(col) + '_' + leftsource['sourceTag'] for col in leftdata.columns]

            rightsource = self.properties.get('right')
            rightdata = Utilities().getValues(payload, rightsource['source'], sep='.')
            rightcolumns=rightdata.columns
            if rightsource.get('addSourceTag', True):
                rightdata.columns = [str(col) + '_' + rightsource['sourceTag'] for col in rightdata.columns]

            df = leftdata.merge(rightdata, how='outer', left_on=leftsource['columns']['keyColumns'],
                                right_on=rightsource['columns']['keyColumns'], indicator=True)
            # thirdsource=self.properties.get('third')
            # if thirdsource is not empty():
            #     thirddata=Utilities().getValues(payload, thirdsource['source'], sep='.')
            #     df=df.merge(thirddata,how='outer', left_on=leftsource['columns']['keyColumns'],
            #                     right_on=rightsource['columns']['keyColumns'], indicator=True)

            if self.properties.get('writeToFile', False):
                if 'custom_reports' not in payload.keys():
                    payload['custom_reports'] = {self.properties.get('reportName', "Report"): df}
                else:
                    payload['custom_reports'][self.properties.get('reportName', 'Report')] = df


        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="OuterMerge", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

class SimpleMerge(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "SimpleMerge"
        self.properties = dict(merge_data="", merge_lookup="", merge_dataFields=[], merge_lookupFields=[], merge_includeCols=[], merge_restoreCols=[],
                                merge_how = "",resultkey='')
    def run(self, payload, debug):
        try:
            util = Utilities()
            merge_data = util.getValues(payload, self.properties['merge_data']).copy().reset_index(drop=True)
            merge_lookup = util.getValues(payload, self.properties['merge_lookup']).copy().reset_index(drop=True)
            merge_data_field = self.properties['merge_dataFields']
            merge_lookup_field = self.properties.get('merge_lookupFields', [])
            merge_include_cols = self.properties.get('merge_includeCols', [])
            merge_how = self.properties.get('merge_how')

            if merge_lookup_field:
                if self.properties.get('merge_dataFieldsFilter', []):
                    merge_data = FilterExecutor.FilterExecutor().run(merge_data, self.properties.get('merge_dataFieldsFilter', []),
                                                               payload=payload)
                if self.properties.get('merge_lookupFieldsFilter', []):
                    merge_lookup = FilterExecutor.FilterExecutor().run(merge_lookup, self.properties.get('merge_lookupFieldsFilter', []),
                                                                 payload=payload)
               # print payload['T24orgfst'].head()
		result_field = merge_data.columns.tolist()
                for col in merge_include_cols:
                    if col in merge_data.columns:
                        del merge_data[col]

                    if col not in result_field:
                        result_field.append(col)

                merge_lookup = merge_lookup[merge_lookup_field + merge_include_cols]  
                merge_lookup_data = merge_data.merge(merge_lookup, left_on=merge_data_field, right_on=merge_lookup_field, how=merge_how,suffixes=('', '_y')
                                         )

                util.setValues(payload, self.properties['resultkey'], merge_lookup_data)

        except Exception as e:
            payload['error'] = dict(elementName="SimpleMerge", type="critical", exception=str(e),
                            traceback=traceback.format_exc())









class UpdateColumns(FlowElement):
    def __init__(self):
        self.type = "UpdateColumns"
        self.name = "UpdateColumns"
        self.properties = dict(template='', resultKey='')

    def run(self, payload, debug):
        try:
            source=self.properties.get('source')
            data=Utilities().getValues(payload,source, sep='.')
            columns = self.properties.get('columns')
            data = data.groupby(self.properties.get('KeyColumns')).apply(lambda x: self.updateColumn(x,columns))
            Utilities().setValues(payload, self.properties['resultkey'], data)
            if self.properties.get('writeToFile', False):
                if 'custom_reports' not in payload.keys():
                    payload['custom_reports'] = {self.properties.get('reportName', "Report"): data}
                else:
                    payload['custom_reports'][self.properties.get('reportName', 'Report')] = data


        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="OuterMerge", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())
    def updateColumn(self,frames,columns):
        try:
            frames['Matched'] = ''
            frames['UnMatched'] = ''
            matched = []
            unMatched = []

            if len(frames) > 1 and len(frames['SOURCE'].unique())>1:
                for col in columns:
                    if len(frames[col].unique()) == 1:
                        matched.append(col)
                    else:
                        unMatched.append(col)
                frames['Matched'] = ', '.join(matched)
                frames['UnMatched'] = ', '.join(unMatched)
            else:
                frames['Matched'] = "Missing"
                frames['UnMatched'] = "Missing"

            return frames
        except Exception as e:
            print e
            # payload['error'] = dict(elementName="OuterMerge", type="critical", exception=str(e),
            #                         traceback=traceback.format_exc())


