import sys

# from StyleFrame import StyleFrame, Styler
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas
import parseeonch
import parseokieomco
import parseokieonco

if __name__ == "__main__":
    stmtdate = '19-Dec-2018'
    uploadFileName = '19-12-2018_tran.zip'

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, reconName="EOMCH_TRANLEVEL", recontype='HITACHI',
                                 compressionType="zip", resultkey='', disableAllCarryFwd=True))

    eomchfilters = ["df['Card No_Match'] = df['Card No'].str[:6] + df['Card No'].str[-4:]","df.to_csv('/tmp/ej_data.csv')"]

    eomchload = dict(type="PreLoader", properties=dict(loadType='EomchTranwise', source="EOMCH",
                                                       feedParams={},
                                                       feedPattern="EOMCH[0-9]{3}.*",
                                                       resultkey="EjData", feedFilters=eomchfilters))
    cbsfilters = [
        "df = df[df['MN TXT TXN DESC'].isin(['ATM CASH WITHDRAWAL','ATM CASH DEPOSIT'])]",
        "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL', 'Card No'] = df['GL TXT TXN DESC'].str[:16]",
        "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT', 'Card No'] = df['GL TXT TXN DESC'].str[:12]"]

    cbsload = dict(type="PreLoader", properties=dict(loadType='Excel', source="GLDumpPresent",
                                                     feedParams={
                                                         "feedformatFile": 'GLDUMP_CASH_BALANCING_STRUCT.csv',
                                                         'skiprows': 1},
                                                     feedPattern="GL_DUMP1_as_on_#%d-%b-%Y#.xlsx",
                                                     resultkey="CbsData", feedFilters=cbsfilters))

    switch_Filters = [

        "df['Transaction Description'] =df['Transaction Description'].str.strip()",

        "df = df[df['Transaction Description'].isin(['WITHDRAWAL','Cash Deposit','CASH DEPOSIT  W/O CARDO CARD'])]"]

    switchload = dict(type="PreLoader", properties=dict(loadType='CSV', source="SWITCH",
                                                        feedParams={
                                                            "feedformatFile": 'Switch_CashBal_Tranlevel_Structure.csv',
                                                            'skiprows': 1},
                                                        feedPattern="Equitas_TRXNDUMP*.*",
                                                        resultkey="SwitchData", feedFilters=switch_Filters))

    EJ_CBS_withdraw = dict(type="NWayMatch", properties=dict(sources=[dict(source="EjData",
                                                                           columns=dict(
                                                                               keyColumns=['TRANSACTION MODE IN EJ',
                                                                                           'Amount in EJ',
                                                                                           'Card No_Match', 'RRN_Match',
                                                                                           'Account Number'],
                                                                               sumColumns=[],
                                                                               matchColumns=['TRANSACTION MODE IN EJ',
                                                                                             'Amount in EJ',
                                                                                             'Card No_Match',
                                                                                             'RRN_Match',
                                                                                             'Account Number']),
                                                                           sourceTag="EJ",
                                                                           subsetfilter=["df = df[df['TRANSACTION MODE IN EJ'].isin(['CASHWITHDRAWAL'])]",
                                                                         "df['Card No_Match'] = df['Card No'].str[:6] + df['Card No'].str[-4:]",
                                                                         "df.loc[df['TRANSACTION MODE IN EJ'].isin(['CASH DEPOSIT','CARD LESS CASH DEPOSIT']),'Card No_Match'] = '0' ",
                                                                         "df['RRN_Match'] = df['RRN'].copy()",
                                                                         "df['Card No'] = df['Card No'].fillna('0')",
                                                                         "df.loc[df['Card No'].str.startswith('508998') | df['Card No'].str.startswith('508999') |df['Card No'].str.startswith('472357'),'RRN_Match'] = df['RRN']",
                                                                         "df.loc[df['TRANSACTION MODE IN EJ'] == 'CASHWITHDRAWAL','TRANSACTION MODE IN EJ'] = 'ATM CASH WITHDRAWAL' ",
                                                                         "df.loc[df['TRANSACTION MODE IN EJ'].isin(['CASH DEPOSIT','CARD LESS CASH DEPOSIT']),'TRANSACTION MODE IN EJ'] = 'ATM CASH DEPOSIT' ",
                                                                                         "df['Amount in EJ'] = df['Amount in EJ'].fillna('0')",
                                                                                         "df['Amount in EJ'] = df['Amount in EJ'].astype(np.float64)"]),
                                                                      dict(source="CbsData", columns=dict(

                                                                          keyColumns=['AMT TXN LCY', 'REF DOC NO',
                                                                                      'Card No', 'Acc No',
                                                                                      'MN TXT TXN DESC'],
                                                                          sumColumns=[],
                                                                          matchColumns=['AMT TXN LCY', 'REF DOC NO',
                                                                                        'Card No', 'Acc No',
                                                                                        'MN TXT TXN DESC']),
                                                                           sourceTag="CBS",
                                                                           subsetfilter=[   "df =  df[df['MN TXT TXN DESC'].isin(['ATM CASH WITHDRAWAL','ATM CASH DEPOSIT','ATM. Off Us Notification'])]",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL','Card No'] = df['GL TXT TXN DESC'].str[:16].str.strip()",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH WITHDRAWAL','Acc No'] = df['GL TXT TXN DESC'].str[-12:].str.strip()",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Card No'] = '0' ",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM CASH DEPOSIT','Acc No'] = df['GL TXT TXN DESC'].str[:12].str.strip()",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM. Off Us Notification','Card No'] =  df['GL TXT TXN DESC'].str[:16].str.strip()",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM. Off Us Notification','Acc No'] = '000000000000' ",
                                                                         "df.loc[df['MN TXT TXN DESC'] == 'ATM. Off Us Notification','MN TXT TXN DESC'] = 'ATM CASH WITHDRAWAL' "])],
                                                             matchResult="results"))

    gen_meta_info = dict(type='GenerateReconMetaInfo', properties=dict())

    elem19 = dict(type="DumpData", properties=dict())

    elements = [elem1, eomchload, cbsload, switchload, EJ_CBS_withdraw, elem19, gen_meta_info]

    f = FlowRunner(elements)

    f.run()
