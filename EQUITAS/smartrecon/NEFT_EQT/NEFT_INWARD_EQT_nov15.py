import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner
if __name__ == "__main__":
    stmtdate = sys.argv[1]

    uploadFileName = sys.argv[2]
    # stmtdate="01-sep-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"

    sfms_filters = ['df["INDIACATOR"]="C"', "df['IFSC'] = df['Benf. IFSC'].str[-4:]"]

    cbs_filters = ["df = df[df['Sub MSGType'] == 'N02']"]
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 recontype="NEFT", compressionType="zip", resultkey='', reconName='NEFT_EQT_INWARD'))
    params = {}
    params["feedformatFile"] = "NEFT_INWARD_Structure.csv"
    # params[
    #     "columnNames"] = "S.No,Seq No,Transaction Ref,Related Reference,Sender IFSC,Sender A/c Type,Sender A/c No,Sender Name,Amount (Rs.),Value. Date,Remit. Date,Batch Id,FRESH / RETURN,Benf. IFSC,Status,Benf. A/c Type,Benf. A/c No.,Benf. Name,Return Code,Return Reason,Originator of Remittance,Sender To Receiver Information"
    params['FooterKey'] = "Incoming_Rejected_Transactions"

    elem2 = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                   feedPattern="NEFT_CONSOLIDATED_*.*",
                                                   feedParams=dict(feedformatFile='NEFT_EQT_CBS_Structure.csv', delimiter='|',
                                                                   skiprows=1), feedFilters=cbs_filters,
                                                   resultkey="cbsdf"))

    elem3 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="NEFT_INWARD",
                                                                 feedPattern="NEFT Inward *.*",
                                                                 feedParams=params,errors=False,
                                                                 feedFilters=[],
                                                                 resultkey="sfmsindf"))
    nwaymatch_inward = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   keyColumns=["Ref No","Remitter IFSC Code", "Benef IFSC code","Amount"],
                                                   sumColumns=[],
                                                   matchColumns=["Ref No","Remitter IFSC Code", "Benef IFSC code","Amount"]),
                                               sourceTag="CBS"),
                                          dict(source="sfmsindf", columns=dict(
                                              keyColumns=["Transaction Ref","Sender IFSC","Benf. IFSC","Amount"],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref","Sender IFSC", "Benf. IFSC","Amount"]),
                                               sourceTag="NEFT_Inward")],
                                 matchResult="results"))
    vlookup_inward = dict(type='VLookup',
                        properties=dict(data='sfmsindf', lookup='cbsdf', dataFields=['Transaction Ref'],
                                        lookupFields=['Ref No'], includeCols = ['Benef CustName'],
                                        resultkey="vlookupdf"))
    vlookup_report = dict(type="ReportGenerator",
                             properties=dict(sources=[dict(source='vlookupdf', sourceTag="VLOOK_REPORT",
                                                           filterConditions=["df = df[df['Benf. Name'] != df['Benef CustName']]"]
                                                           )],
                                             writeToFile=True, reportName='Name Mismatch Report'))
    dump_data = dict(type='DumpData', properties={})
    meta = dict(type='GenerateReconMetaInfo', properties=dict())
    elements = [elem1, elem2,elem3,nwaymatch_inward,vlookup_inward,vlookup_report,dump_data,meta]
    f = FlowRunner(elements)
    f.run()
