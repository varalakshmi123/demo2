import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner
if __name__ == "__main__":
    stmtdate = sys.argv[1]

    uploadFileName = sys.argv[2]
    # stmtdate="01-sep-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"
    cbs_filters = ["df = df[df['Sub MSGType'] == 'N07']"]
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 recontype="NEFT", compressionType="zip", resultkey='', reconName='NEFT_EQT_RETURN'))
    params = {}
    params["feedformatFile"] = "NEFT_Return_Structure.csv"
    params['FooterKey'] = "No of Transactions"
    params["columnNames"] = 'S.No,Seq No,Transaction Ref,Related Reference,Amount (Rs.),Value Date,Batch Id,Sending Branch,Sender A/c Type,Sender A/c No,Sender A/c Name,Benf. Branch,Benf A/c Type,Benf. A/c No,Benf. A/c Name,Txn. Status,Return Code,Return Resason,Originator of Remittance,Sender To Receiver Information'
    params['skiprows'] = 0

    elem2 = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                   feedPattern="NEFT_CONSOLIDATED_*.*",
                                                   feedParams=dict(feedformatFile='NEFT_EQT_CBS_Structure.csv',
                                                                   delimiter='|',
                                                                   skiprows=1), feedFilters=cbs_filters,
                                                   resultkey="cbsdf"))
    elem3 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                   feedPattern="NEFT Outward *.*", errors=False,
                                                   feedParams=params, feedFilters=[], resultkey="sfmsdf"))
    nwaymatch_return = dict(type="NWayMatch",
                            properties=dict(sources=[dict(source="cbsdf",
                                                          columns=dict(
                                                              keyColumns=["Ref No", "Remitter IFSC Code","Benef IFSC code", "Amount"],
                                                              sumColumns=[],
                                                              matchColumns=["Ref No", "Remitter IFSC Code","Benef IFSC code", "Amount"]),
                                                          sourceTag="CBS"),
                                                     dict(source="sfmsdf", columns=dict(
                                                         keyColumns=["Transaction Ref","Sending Branch", "Benf. Branch",
                                                                     "Amount (Rs.)"],
                                                         sumColumns=[],
                                                         matchColumns=["Transaction Ref", "Sending Branch", "Benf. Branch",
                                                                       "Amount (Rs.)"]),
                                                          sourceTag="NEFT_Outward")],
                                            matchResult="results"))
    vlookup_inward = dict(type='VLookup',
                          properties=dict(data='sfmsdf', lookup='cbsdf', dataFields=['Transaction Ref'],
                                          lookupFields=['Ref No'], includeCols=['Benef CustName'],
                                          resultkey="vlookupdf"))
    vlookup_report = dict(type="ReportGenerator",
                          properties=dict(sources=[dict(source='vlookupdf', sourceTag="VLOOK_REPORT",
                                                        filterConditions=[
                                                            "df = df[df['Benf. A/c Name'] != df['Benef CustName']]"]
                                                        )],
                                          writeToFile=True, reportName='Name Mismatch Report'))
    dump_data = dict(type='DumpData', properties={})
    meta = dict(type='GenerateReconMetaInfo', properties=dict())
    elements = [elem1, elem2, elem3,nwaymatch_return,vlookup_inward,vlookup_report,dump_data,meta]
    f = FlowRunner(elements)
    f.run()