import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner
if __name__ == "__main__":
    stmtdate = '01-Oct-2019'
    uploadFileName = '08012019.zip'
    # stmtdate="01-sep-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"

    sfms_filters = ['df["INDIACATOR"]="C"', "df['IFSC'] = df['Benf. IFSC'].str[-4:]"]

    cbs_filters = ["df = df[df['Sub MSGType'] == 'N02']"]
    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 recontype="NEFT", compressionType="zip", resultkey='', reconName='NEFT_EQT_INWARD'))
    params = {}
    params["feedformatFile"] = "NEFT_INWARD_Structure.csv"
    # params[
    #     "columnNames"] = "S.No,Seq No,Transaction Ref,Related Reference,Sender IFSC,Sender A/c Type,Sender A/c No,Sender Name,Amount (Rs.),Value. Date,Remit. Date,Batch Id,FRESH / RETURN,Benf. IFSC,Status,Benf. A/c Type,Benf. A/c No.,Benf. Name,Return Code,Return Reason,Originator of Remittance,Sender To Receiver Information"
    params['FooterKey'] = "Incoming_Rejected_Transactions"

    elem2 = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                   feedPattern="NEFT_CONSOLIDATED_*.*",
                                                   feedParams=dict(feedformatFile='NEFT_EQT_CBS_Structure.csv',
                                                                   delimiter='|',
                                                                   skiprows=1), feedFilters=cbs_filters,
                                                   resultkey="cbsdf"))

    elem3 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="NEFT Inward *.*",
                                                                 feedParams=params,errors=False,
                                                                 feedFilters=[],
                                                                 resultkey="sfmsindf"))
    cbsupper = dict(type="ExpressionEvaluator",
                                  properties=dict(source="cbsdf",
                                                  expressions=[
                                                      "df['Benef CustName'] = df['Benef CustName'].str.upper()",
                                                  "df['Benef CustName'] = df['Benef CustName'].apply(lambda x: str(x).strip())"],
                                                  resultkey='cbsdf'))
    sfmsupper = dict(type="ExpressionEvaluator",
                    properties=dict(source="sfmsindf",
                                    expressions=[
                                        "df['Benf. Name'] = df['Benf. Name'].str.upper()",
                                        "df['Benf. Name'] = df['Benf. Name'].apply(lambda x: str(x).strip())"
                                    ],
                                    resultkey='sfmsindf'))

    nwaymatch_inward = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   feedfilters=["df['Benef CustName'] = df['Benef CustName'].str.upper().strip()"],
                                                   keyColumns=["Ref No","Remitter IFSC Code", "Benef IFSC code","Amount"],
                                                   sumColumns=[],
                                                   matchColumns=["Ref No","Remitter IFSC Code", "Benef IFSC code","Amount"]),
                                               sourceTag="CBS"),
                                          dict(source="sfmsindf", columns=dict(
                                              feedfilters = ["df['Benf. Name'] = df['Benf. Name'].str.upper().strip()"],
                                              keyColumns=["Transaction Ref","Sender IFSC","Benf. IFSC","Amount"],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref","Sender IFSC", "Benf. IFSC","Amount"]),
                                               sourceTag="SFMS")],
                                 matchResult="results"))
    vlookup_inward = dict(type='VLookup',
                        properties=dict(data='sfmsindf', lookup='cbsdf', dataFields=['Transaction Ref'],
                                        dataFieldsFilter = ["df['Benf. Name'] = df['Benf. Name'].astype(str)",
                                                            "df['Benf. Name'] = df['Benf. Name'].apply(lambda x: x.replace(x[x.find(' ')],'') if  re.search(r'\s',x) else x)"],
                                        lookupFieldsFilter = ["df['Benef CustName'] = df['Benef CustName'].astype(str)",
                                                            "df['Benef CustName'] = df['Benef CustName'].apply(lambda x: x.replace(x[x.find(' ')],'') if  re.search(r'\s',x) else x)"],
                                        lookupFields=['Ref No'], includeCols = ['Benef CustName'],
                                        resultkey="vlookupdf"))
    vlookup_report = dict(type="ReportGenerator",
                             properties=dict(sources=[dict(source='vlookupdf', sourceTag="VLOOK_REPORT",
                                                           filterConditions=[
                                                               "df=df[df['CBS Match']!='UNMATCHED']",
                                                               "df = df[df['Benf. Name'] != df['Benef CustName']]",
                                                               "df=df.rename(columns={'Benf. Name':'SFMS Benf. Name','Benef CustName':'CBS Benf. Name'})",
                                                               "df=df.drop(labels=['FEED_FILE_NAME','SOURCE','CARRY_FORWARD','STATEMENT_DATE','Ageing','CBS Match'],axis=1)"
                                                           ]
                                                           )],
                                             writeToFile=True, reportName='Name Mismatch Report'))

    filter1 = ["df=df[df['CBS Match']=='MATCHED']",
               "df['Matched Count'] = 1",
               "df['Execution Date Time']=datetime.now().strftime('%d-%m-%Y %H:%M:%S')",
               "df = df.groupby(['SOURCE','Batch Id','Execution Date Time','STATEMENT_DATE'])['Amount','Matched Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Matched Amount','Statement Date':'STATEMENT_DATE','Batch Id':'Settlement Time'},inplace=True)"]

    filter2 = ["df=df[df['SFMS Match']=='MATCHED']",
               "df['Matched Count'] = 1",
               "df['Execution Date Time']=datetime.now().strftime('%d-%m-%Y %H:%M:%S')",
               "df = df.groupby(['SOURCE','BatchTime','Execution Date Time','STATEMENT_DATE'])['Amount','Matched Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Matched Amount','Statement Date':'STATEMENT_DATE','BatchTime':'Settlement Time'},inplace=True)"]

    elem11 = dict(type="ReportGenerator", properties=dict(sources=[dict(source="results.SFMS", sourceTag='SFMS',
                                                                            filterConditions=filter1
                                                                        ),
                                                                   dict(source="results.CBS", sourceTag='CBS',
                                                                        filterConditions=filter2
                                                                     )],
                                                          reportName='Settlement Wise Reconcile', writeToFile=True,
                                                          ))

    dump_data = dict(type='DumpData', properties={})
    meta = dict(type='GenerateReconMetaInfo', properties=dict())
    elements = [elem1, elem2,elem3,cbsupper,sfmsupper,nwaymatch_inward,vlookup_inward,vlookup_report,elem11,dump_data,meta]
    f = FlowRunner(elements)
    f.run()
