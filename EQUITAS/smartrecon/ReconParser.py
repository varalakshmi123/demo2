import inspect

import FlowElements


class ReconParser():
    def __init__(self):
        self.modules = ["FlowElements"]
        self.classes = {}
        self.classProperties = {}

    def getClasses(self):
        if len(self.classes.keys()) == 0:
            for m in self.modules:
                exec ("import " + m)
                for name, obj in inspect.getmembers(eval(m)):
                    # print name
                    if inspect.isclass(obj) and issubclass(obj, FlowElements.FlowElement):
                        if obj.__name__ != 'FlowElement':
                            self.classes[obj.__name__] = obj
                            self.classProperties[obj.__name__] = obj().properties
        return list(self.classes.keys())

    def getProperties(self, element):
        return self.classProperties.get(element, {})

    def getAllElements(self):
        return self.classProperties


if __name__ == "__main__":
    rp = ReconParser()
    classes = rp.getClasses()
    for klass in classes:
        print klass, rp.getProperties(klass)
    print rp.getAllElements()
