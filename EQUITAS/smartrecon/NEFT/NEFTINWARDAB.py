import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    # stmtdate="01-sep-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"

    sfms_filters = ['df["INDIACATOR"]="C"',"df['IFSC'] = df['Benf. IFSC'].str[-4:]"]
    # cbs_filters = ['df["V_TXN_DESC"]=df["V_TXN_DESC"].apply(lambda x: x.split(":")[-1])']XXXX1061657016
    cbs_filters = ["df=df[( (df['FORACID'].fillna('').str.lstrip('0').str.endswith('1061657016')) | (df['FORACID'].fillna('').str.lstrip('0').str.endswith('1025398006')))]",
                   "df = df[(df['TRAN_PARTICULARS'] != 'NEFT') & (df['TRAN_RMKS'] != 'NEFT SETTLEMENT') & (df['DEBIT_AMT'] != 0)]",
                   r"df['Transaction Ref']=df['TRAN_RMKS'].apply(lambda x:x.split('\\')[0])",
                   "df['Transaction Ref']=df['Transaction Ref'].str.strip(' ')","df['INDICATOR']='D'","df['IFSC']=df['TRAN_RMKS'].str[-4:]",
                   r"df['Sender Name']=df['TRAN_PARTICULARS'].apply(lambda x:''.join(x.split('/')[1:]))"]

    rectified_filters = ["df['INDICATOR']='D'",
                         #"df['Transaction Ref']=df['TRAN_RMKS'].apply(lambda x:x.split('/')[0])",
                         'df["TRAN_PARTICULARS"]=df["TRAN_PARTICULARS"].str.strip(" ")',
                         "df['Transaction Ref']=df['TRAN_PARTICULARS'].apply(lambda x:x.split(':')[1] if ':' in x else x)",
                         'df=df.dropna(how="all",subset=["Transaction Ref"])',
                         'df.rename(columns={"TRAN DATE": "RECTIFIED_TRAN_DATE", "TRAN_ID": "RECTIFIED_TRAN_ID"},inplace=True)',
                         'df["Transaction Ref"]=df["Transaction Ref"].str.strip(" ")']

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="NEFT",compressionType="zip", resultkey='',reconName='NEFTInward'))
    params = {}
    params["feedformatFile"] = "NEFT_INWARD_Structure.csv"
    # params[
    #     "columnNames"] = "S.No,Seq No,Transaction Ref,Related Reference,Sender IFSC,Sender A/c Type,Sender A/c No,Sender Name,Amount (Rs.),Value. Date,Remit. Date,Batch Id,FRESH / RETURN,Benf. IFSC,Status,Benf. A/c Type,Benf. A/c No.,Benf. Name,Return Code,Return Reason,Originator of Remittance,Sender To Receiver Information"
    params['FooterKey'] = "Incoming_Rejected_Transactions"

    elem2 = dict(type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern="sol_[0-9]{4}_[0-9]{4}_1061657016_#%d%m%Y#_[0-9]{3}.lst",
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',delimiter='|',skiprows=1),feedFilters=cbs_filters,resultkey="cbsdf"))

    elem3 = dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="INWARD_NEFT_*.*",
                                                                 feedParams=params,errors=False,
                                                                 feedFilters=sfms_filters,
                                                                 resultkey="sfmsdf"))

    rectifiedentries = dict( type="PreLoader", properties=dict(loadType="CSV", source="CBSRECTIFIED",
                                                                            feedPattern="sol_[0-9]{4}_[0-9]{4}_1025400017_#%d%m%Y#_[0-9]{3}.lst",
                                                                            disableCarryFwd=True,
                                                                            feedParams=dict(
                                                                                feedformatFile='CBS_ANDHRA.csv',
                                                                                delimiter='|', skiprows=1),
                                                                            errors=False, feedFilters=rectified_filters,
                                                                            resultkey="recdf"))

    elem4 = dict(type="FilterDuplicates",
                 properties=dict(source="sfmsdf", inplace=True, allowOneInMatch=True,duplicateStore='duplicateStore',
                                 keyColumns=["Transaction Ref","Amount"], sourceTag="SFMS",markerCol='Duplicate',resultKey="sfmsdf"))

    elem5 = dict(type="FilterDuplicates",
                 properties=dict(source="cbsdf", inplace=True, allowOneInMatch=True,duplicateStore='duplicateStore',
                           keyColumns = ["Transaction Ref","DEBIT_AMT"], sourceTag = "CBS",markerCol='Duplicate',resultKey = "cbsdf"))




    elem6 = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   keyColumns=["Transaction Ref","IFSC","DEBIT_AMT"],
                                                   sumColumns=[],
                                                   matchColumns=["Transaction Ref", "DEBIT_AMT","IFSC"]), sourceTag="CBS",
                                                 subsetfilter = ["df=df[(df['Duplicate']!='DUPLICATED')]"]),
                                          dict(source="sfmsdf", columns=dict(
                                              keyColumns=["Transaction Ref","IFSC","Amount"],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref", "Amount","IFSC"]),subsetfilter = ["df=df[(df['Duplicate']!='DUPLICATED')]"], sourceTag="SFMS")],
                                 matchResult="results"))

    # removeunmatched = dict(type="RemoveUnmatched",
    #                        properties=dict(sources=[dict(source="results.CBS_IFSC", resultkey='cbsdf_ifsc'),
    #                                                 dict(source="results.SFMS_IFSC", resultkey='sfmsdf_ifsc')
    #
    #                                                 ]))

    elem9 = dict( type="NWayMatch",
                 properties=dict(sources=[dict(source="results.CBS",
                                               columns=dict(
                                                   keyColumns=["Transaction Ref"],
                                                   sumColumns=["DEBIT_AMT"],
                                                   matchColumns=["Transaction Ref", "DEBIT_AMT"]),subsetfilter=["df=df[(df['Duplicate']!='DUPLICATED') &(df['SFMS Match']!='MATCHED')]"], sourceTag="CBS"),
                                          dict(source="results.SFMS", columns=dict(
                                              keyColumns=["Transaction Ref"],
                                              sumColumns=["Amount"],
                                              matchColumns=["Transaction Ref","Amount"]),subsetfilter=["df=df[(df['Duplicate']!='DUPLICATED') &(df['CBS Match']!='MATCHED')]"],sourceTag="SFMS")],
                                 matchResult="results"))

    duplicate = dict(type="DuplicateMatch",
                     properties=dict(sources=[dict(source="results.CBS",
                                                   columns=dict(
                                                       keyColumns=["Transaction Ref"],
                                                       sumColumns=["DEBIT_AMT"],
                                                       matchColumns=["Transaction Ref", "DEBIT_AMT"]),
                                                   subsetfilter=["df=df[(df['Duplicate']=='DUPLICATED') &(df['SFMS Match']!='MATCHED')]"],
                                                   sourceTag="CBS"),
                                              dict(source="results.SFMS", columns=dict(
                                                  keyColumns=["Transaction Ref"],
                                                  sumColumns=["Amount"],
                                                  matchColumns=["Transaction Ref", "Amount"]),
                                                   subsetfilter=["df=df[(df['Duplicate']=='DUPLICATED') &(df['CBS Match']!='MATCHED')]"],
                                                   sourceTag="SFMS")],
                                     matchResult="results"))

    # concat=dict(type="SourceConcat",properties=dict(sourceList=['results.CBS','results.CBS_IFSC'],resultkey='concatdf'))



    # elem5=dict(id=5,next=6,type='ReportGenerator',properties=dict(inputDataFrame='sfmsdf',columnNameList=["Transaction Ref", "Amount"],reportName='REPORT',resultkey='smdf'))

    vlookup = dict(type='VLookup',
                        properties=dict(data='results.SFMS', lookup='recdf', dataFields=["Transaction Ref", "Amount"],
                                        includeCols=["RECTIFIED_TRAN_DATE", "RECTIFIED_TRAN_ID"],
                                        lookupFields=["Transaction Ref", "Amount"],markers=dict(RectifiedEntries='MATCHED'),
                                        resultkey="results.SFMS"))


    exp2 = dict(type='ExpressionEvaluator', properties=dict(source='results.SFMS', expressions=[

        "df.loc[df.get('RectifiedEntries','')=='MATCHED','CBS Match']='MATCHED'",
        "payload['RectifiedEntries']=df[df.get('RectifiedEntries','')=='MATCHED']",
        "df.drop(['RectifiedEntries'],axis=1,inplace=True)" ],resultkey="results.SFMS"))

    filter1 = ["df = df[df['FORACID'].str.endswith('1061657016')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='NEFT-INWARD-Branch Settlements'",
               "df['Account Head']='XXXX1061657016'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    # filter2 = ["df = df[df['FORACID'].str.endswith('1061657016')]",
    #            "df = df[df['SFMS Match'] == 'UNMATCHED']",
    #            "df['Debit Count'] = 1", "df['Credit Count'] = 1",
    #            "df['Account Description']='NEET Oustanding Outward (Unmatched Amount)'",
    #            "df['Account Head']='XXXX1061657016'",
    #            "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
    #            "df.rename({'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    # filter2 = ["df = df[df['FORACID'].str.endswith('1061657016')]",
    #            "df = df[df['SFMS Match'] == 'UNMATCHED']",
    #            "df['Debit Count'] = 1", "df['Credit Count'] = 1",
    #            "df['Account Description']='NEFT Oustanding Inward(Unmatched Amount)'",
    #            "df['Account Head']='XXXX1061657016'",
    #            "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
    #            "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]





    elem10 = dict(type="ReportGenerator", properties=dict(sources=[dict(source="results.CBS", sourceTag='CBS',
                                                                             filterConditions=filter1)],writeToFile=True,keycolumns=['Account Description','Account Head','Debit Amount','Credit Amount','Debit Count','Credit Count'],reportName='Principle'))



    # elem11 = dict(type='GenerateReconSummary', properties=dict(
    #     sources=[dict(resultKeys=["CBS","CBS_IFSC"], sourceTag='CBS', aggrCol=['DEBIT_AMT'],matched='any'),
    #              dict(resultKeys=["SFMS","SFMS_IFSC"], sourceTag='SFMS', aggrCol=['Amount'],matched='any')]))



    RectifiedEntriesReport = dict(type='ReportGenerator',
                              properties=dict(sources=[dict(source='RectifiedEntries')],
                                              writeToMongo=False,
                                              writeToFile=True, reportName='RectifiedEntries'))


    elem11 = dict(type='GenerateReconSummary',
                  properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['DEBIT_AMT']),
                                           dict(resultKey="SFMS", sourceTag='SFMS',
                                                aggrCol=['Amount'])],groupbyCol=['STATEMENT_DATE']))

    elem12 = dict( type='DumpData', properties=dict(dumpPath='NEFTInward', matched='all'))


    meta=dict(type='GenerateReconMetaInfo',properties=dict())


    elements = [elem1,elem2,elem3,rectifiedentries,elem4,elem5,elem6,elem9,duplicate,vlookup,exp2,elem10,RectifiedEntriesReport,elem11,elem12,meta]
    f = FlowRunner(elements)
    f.run()

