import sys

sys.path.append('/usr/share/nginx/smartrecon')

from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    # stmtdate='01-sep-2017'

    basePath = "/usr/share/nginx/smartrecon/mft/"
    reconName = 'NEFTOUTWARD'
    # sfms_filters =
    cbs_filters = ['df = df[df["DEBIT_AMT"] == 0.0]',
                   'df["INDICATOR"]="CR"',
                   'df["TRAN_PARTICULARS"]=df["TRAN_PARTICULARS"].str.strip(" ")',
                   'df["Transaction Ref"]=df["TRAN_PARTICULARS"].fillna("").str.extract("(ANDB[A-Za-z][0-9]*)")',
                   'df=df.dropna(how="all",subset=["Transaction Ref"])',
                   'df["Transaction Ref"]=df["Transaction Ref"].str.strip(" ")',
                   'df = df[((df["FORACID"].str.endswith("1025399010")) & (df["FORACID"].str.lstrip("0") != "7551025399010")) | (df["FORACID"].str.lstrip("0") == "7551025398005")]',
                   'df["IFSC"]=df["SOL_ID"]']

    rectified_filters = ['df = df[df["DEBIT_AMT"] == 0.0]',
                   'df["INDICATOR"]="CR"',
                   'df["TRAN_PARTICULARS"]=df["TRAN_PARTICULARS"].str.strip(" ")',
                   #'df["Transaction Ref"]=df["TRAN_RMKS"].fillna("").str.extract("(ANDB[A-Za-z][0-9]*)")',
                   'df["Transaction Ref"]=df["TRAN_PARTICULARS"].apply(lambda x:x.split(":")[1] if ":" in x else x)',
                   'df=df.dropna(how="all",subset=["Transaction Ref"])',
                   'df.rename(columns={"TRAN DATE":"RECTIFIED_TRAN_DATE","TRAN_ID":"RECTIFIED_TRAN_ID"},inplace=True)',
                   'df["Transaction Ref"]=df["Transaction Ref"].str.strip(" ")']


    # c_fil=['df["Transaction Ref"]=df["TRAN_PARTICULARS"].apply(lambda x: x.split("/")[1] if "NEFT/" in x else x)']
    sfms_filters = ['df["INDICATOR"]="DR"', "df['IFSC'] = df['Sending Branch'].fillna('').str[-4:]"]
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="NEFT",compressionType="zip", resultkey='',reconName='NEFTOutward'))
    params = {}
    params["feedformatFile"] = "NEFT_OUTWARD_Structure.csv"
    params[
        "columnNames"] = 'S.No,Seq No,Transaction Ref,Amount (Rs.),Value Date,Batch Id,Sending Branch,Sender A/c Type,Sender A/c No,Sender A/c Name,Benf. Branch,Benf A/c Type,Benf. A/c No,Benf. A/c Name,Txn. Status,Originator of Remittance,Sender To Receiver Information'
    params['FooterKey'] = "Outgoing Return Transactions"

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern="sol_[0-9]{4}_[0-9]{4}_(1025399010|1025398005)_#%d%m%Y#_[0-9]{3}.lst",
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),
                                                                 errors=False, feedFilters=cbs_filters,
                                                                 resultkey="cbsdf"))





    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="OUTWARD_NEFT_.*?.xls",
                                                                 feedParams=params, errors=False,
                                                                 feedFilters=sfms_filters,
                                                                 resultkey="sfmsdf"))

    rectifiedentries= dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBSRECTIFIED",
                                                                 feedPattern="sol_[0-9]{4}_[0-9]{4}_1025400017_#%d%m%Y#_[0-9]{3}.lst",disableCarryFwd=True,
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),
                                                                 errors=False,feedFilters=rectified_filters,
                                                                 resultkey="recdf"))

    elem4 = dict(id=4, next=5, type='IncrementalLoader',
                 properties=dict(reconName='NEFTOutward', source='sfmsdf', keycolumns=["Transaction Ref", 'Amount (Rs.)'],
                                 sourceTag='SFMS', resultkey='sfmsdf'))


    elem5 = dict(type="FilterDuplicates",
                 properties=dict(source="sfmsdf", duplicateStore="duplicateStore", allowOneInMatch=True,inplace=True,markerCol='Duplicate',
                                 keyColumns=["Transaction Ref", 'Amount (Rs.)'], sourceTag="SFMS", resultKey="sfmsdf"))

    elem6 = dict(type="FilterDuplicates",
                 properties=dict(source="cbsdf", duplicateStore="duplicateStore", allowOneInMatch=True,markerCol='Duplicate',inplace=True,
                                 keyColumns=["Transaction Ref", 'Amount'], sourceTag="CBS", resultKey="cbsdf"))

    # elem5 = dict(id=5,next=6,type="ScriptEvaluator",properties=[dict(source='cbsexp', condition=cbscrfilter, resultkey='cbscr'),dict(source='cbsexp', condition=cbsdrfilter, resultkey='cbsdr')])


    elem7 = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   keyColumns=["Transaction Ref", 'Amount', 'IFSC'],
                                                   sumColumns=[],
                                                   matchColumns=["Transaction Ref", "Amount", "IFSC"]),
                                               subsetfilter=["df=df[(df['Duplicate']!='DUPLICATED')]"],

                                               sourceTag="CBS"),
                                          dict(source="sfmsdf", columns=dict(
                                              keyColumns=["Transaction Ref", 'Amount (Rs.)', 'IFSC'],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref", "Amount (Rs.)", "IFSC"]),
                                               subsetfilter=[
                                                   "df=df[(df['Duplicate']!='DUPLICATED')]"],
                                               sourceTag="SFMS")],
                                 matchResult="results"))

    # removeunmatched = dict(type="RemoveUnmatched",
    #                        properties=dict(sources=[dict(source="results.CBS_IFSC", resultkey='cbsdf_ifsc'),
    #                                                 dict(source="results.SFMS_IFSC", resultkey='sfmsdf_ifsc')
    #
    #                                                 ]))



    # filter3=[
    #          "df = df[df['FORACID'].str.endswith('7551025398005')]",
    #          "df['Debit Count'] = 1",
    #          "df['Credit Count'] = 1",
    #          "df['Account Description']='OUTWARD-CGGB'",
    #          "df['Account Head']='07551025398005'",
    #          "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
    #          "df.rename({'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]


    #
    # filter4 = ["df = df[df['FORACID'].str.endswith('1025399010')]",
    #            "df = df[df['SFMS Match'] == 'UNMATCHED']",
    #            "df['Debit Count'] = 1", "df['Credit Count'] = 1",
    #            "df['Account Description']='NEET Oustanding Outward (Unmatched Amount)'",
    #            "df['Account Head']='XXXX1025399010'",
    #            "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
    #            "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]


    #
    # elem8 = dict(type="ExpressionEvaluator", properties=dict(source="results.SFMS_IFSC",
    #                                                          expressions=["df=df[df['CBS_IFSC Match']=='UNMATCHED']",
    #                                                                       "df=df.drop(labels=['CBS_IFSC Match'],axis=1)"],
    #                                                          resultkey='sfmsdf_ifsc'))
    #
    # elem9 = dict(type="ExpressionEvaluator", properties=dict(source="results.CBS_IFSC",
    #                                                          expressions=["df=df[df['SFMS_IFSC Match']=='UNMATCHED']",
    #                                                                       "df=df.drop(labels=['SFMS_IFSC Match'],axis=1)"],
    #                                                          resultkey='cbsdf_ifsc'))

    elem10 =  dict( type="NWayMatch",
                 properties=dict(sources=[dict(source="results.CBS",
                                               columns=dict(
                                                   keyColumns=["Transaction Ref"],
                                                   sumColumns=["Amount"],
                                                   matchColumns=["Transaction Ref", "Amount"]),subsetfilter=["df=df[(df['Duplicate']!='DUPLICATED') &(df['SFMS Match']!='MATCHED')]"], sourceTag="CBS"),
                                          dict(source="results.SFMS", columns=dict(
                                              keyColumns=["Transaction Ref"],
                                              sumColumns=["Amount (Rs.)"],
                                              matchColumns=["Transaction Ref","Amount (Rs.)"]),subsetfilter=["df=df[(df['Duplicate']!='DUPLICATED') &(df['CBS Match']!='MATCHED')]"],sourceTag="SFMS")],
                                 matchResult="results"))

    duplicate = dict(type="DuplicateMatch",
                     properties=dict(sources=[dict(source="results.CBS",
                                                   columns=dict(
                                                       keyColumns=["Transaction Ref"],
                                                       sumColumns=["Amount"],
                                                       matchColumns=["Transaction Ref", "Amount"]),
                                                   subsetfilter=["df=df[(df['Duplicate']=='DUPLICATED') &(df['SFMS Match']!='MATCHED')]"],
                                                   sourceTag="CBS"),
                                              dict(source="results.SFMS", columns=dict(
                                                  keyColumns=["Transaction Ref"],
                                                  sumColumns=["Amount (Rs.)"],
                                                  matchColumns=["Transaction Ref","Amount (Rs.)"]),
                                                   subsetfilter=["df=df[(df['Duplicate']=='DUPLICATED') &(df['CBS Match']!='MATCHED')]"],
                                                   sourceTag="SFMS")],
                                     matchResult="results"))

    vlookup = dict(type='VLookup',
                        properties=dict(data='results.CBS', lookup='recdf', dataFields=["Transaction Ref", "Amount"],
                                        includeCols=["RECTIFIED_TRAN_DATE", "RECTIFIED_TRAN_ID"],
                                        lookupFields=["Transaction Ref", "Amount"],markers=dict(RectifiedEntries='MATCHED'),
                                        resultkey="results.CBS"))

    exp2 = dict(type='ExpressionEvaluator', properties=dict(source='results.CBS', expressions=[

        "df.loc[df.get('RectifiedEntries','')=='MATCHED','SFMS Match']='MATCHED'",
        "payload['RectifiedEntries']=df[df.get('RectifiedEntries','')=='MATCHED']",
        "df.drop(['RectifiedEntries'],axis=1,inplace=True)"

        ],resultkey="results.CBS"))

    filter1 = ["df = df[df['FORACID'].str.endswith('1025399010')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='OUTWARD Branch settlement'",
               "df['Account Head']='XXXX1025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    filter2 = ["df = df[df['FORACID'].str.endswith('07551025398005')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='OUTWARD-CGGB'",
               "df['Account Head']='07551025398005'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]


    # concat=dict(type="SourceConcat",properties=dict(sourceList=['results.CBS','results.CBS_IFSC'],resultkey='concatdf'))


    elem11 = dict(type="ReportGenerator", properties=dict(sources=[dict(source="results.CBS", sourceTag='CBS',
                                                                        filterConditions=filter1
                                                                        ,
                                                                        keycols=['Account Description', 'Account Head',
                                                                                 'Debit Amount', 'Credit Amount',
                                                                                 'Debit Count', 'Credit Count']),
                                                                   dict(source="results.CBS", sourceTag='CBS',
                                                                        filterConditions=filter2,
                                                                        keycols=['Account Description', 'Account Head',
                                                                                 'Debit Amount', 'Credit Amount',
                                                                                 'Debit Count', 'Credit Count'])],
                                                          reportName='Principle',writeToFile=True,keycolumns=['Account Description','Account Head','Debit Amount','Credit Amount','Debit Count','Credit Count']))

    RectifiedEntriesReport = dict(type='ReportGenerator',
                              properties=dict(sources=[dict(source='RectifiedEntries')],
                                              writeToMongo=False,
                                              writeToFile=True, reportName='RectifiedEntries'))


    elem12 = dict(type='GenerateReconSummary',
                  properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                           dict(resultKey="SFMS", sourceTag='SFMS',
                                                aggrCol=['Amount (Rs.)'])],groupbyCol=['STATEMENT_DATE']))
    elem13 = dict(type='DumpData', properties=dict(dumpPath='NEFTOutward', matched='all'))

    meta=dict(type='GenerateReconMetaInfo',properties=dict())


    elements = [elem1, elem2, elem3,rectifiedentries,elem4,elem5, elem6, elem7, elem10,duplicate,vlookup,exp2,elem11,RectifiedEntriesReport,elem12,elem13,meta]
    f = FlowRunner(elements)
    f.run()
