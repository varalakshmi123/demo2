import sys
sys.path.append('/usr/share/nginx/smartrecon')

from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    # stmtdate='22-dec-2017'
    basePath = "/usr/share/nginx/smartrecon/mft/"


    sfms_filters = ["df.dropna(how='all',subset=['Transaction Ref','Amount (Rs.)'],inplace=True)","df['INDICATOR'] = 'C'"]
    # cbs_filters = ['df["Tran Remarks"]=df["Tran Remarks"].str.strip(" ")','df=df[df["Tran Remarks"].str.contains("N07:")]','df["Transaction Ref"]=df["Tran Remarks"].apply(lambda x: x.split(":")[1])','df["ReconName"]="NEFTRETURN"']

    cbs_filters = [
        "df=df[( (df['FORACID'].fillna('').str.lstrip('0').str.endswith('7551061657016')) | (df['FORACID'].fillna('').str.lstrip('0').str.endswith('7551025398006')))]",
        "df = df[(df['TRAN_PARTICULARS'] != 'NEFT') & (df['TRAN_RMKS'] != 'NEFT SETTLEMENT') & (df['DEBIT_AMT'] != 0)]",
        r"df['Transaction Ref']=df['TRAN_RMKS'].apply(lambda x:x.split('\\')[0])",
        "df['Transaction Ref']=df['Transaction Ref'].str.strip(' ')", "df['INDICATOR']='D'"]

    cbstrn_filters=["df = df[df['Amount']!= 0.0]",
                    "df = df[(df['FORACID'].fillna('').str.lstrip('0').str.endswith('7551025398006'))]",
                    "df['INDICATOR'] = 'D'","df['Transaction Ref'] = df['TRAN_PARTICULARS'].str.replace('NEFT Reject For ','')"]

    rectified_filters = [
                         #"df['Transaction Ref']=df['TRAN_RMKS'].apply(lambda x:x.split('/')[0])",
                         'df["TRAN_PARTICULARS"]=df["TRAN_PARTICULARS"].str.strip(" ")',
                         "df['Transaction Ref']=df['TRAN_PARTICULARS'].apply(lambda x:x.split(':')[1] if ':' in x else x)",
                         'df=df.dropna(how="all",subset=["Transaction Ref"])',
                         'df["Transaction Ref"]=df["Transaction Ref"].str.strip(" ")',
                         'df.rename(columns={"TRAN DATE":"RECTIFIED_TRAN_DATE","TRAN_ID":"RECTIFIED_TRAN_ID"},inplace=True)'

                          ]

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="NEFT", compressionType="zip",resultkey='',reconName='NEFTReturn'))
    params = {}
    params["feedformatFile"] = "NEFT_Return_Structure.csv"
    params['FooterKey'] = "No of Transactions"
    params["columnNames"] ='S.No,Seq No,Transaction Ref,Related Reference,Amount (Rs.),Value Date,Batch Id,Sending Branch,Sender A/c Type,Sender A/c No,Sender A/c Name,Benf. Branch,Benf A/c Type,Benf. A/c No,Benf. A/c Name,Txn. Status,Return Code,Return Resason,Originator of Remittance,Sender To Receiver Information'

    params['skiprows']=0
    #
    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern="sol_0500_0999_1061657016_#%d%m%Y#_[0-9]{3}.lst",
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),feedFilters=cbs_filters,resultkey="cbsdf"))


    elem3 = dict(type="PreLoader", properties=dict(loadType="CSV", source="CBSTRANSIT",
                                                                 feedPattern="sol_0500_0999_1025398006_#%d%m%Y#_[0-9]{3}.lst",
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),feedFilters=cbstrn_filters,resultkey="cbstrdf"))



    elem4= dict(type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="OUTWARD_NEFT_*.*",errors=False,
                                                                 feedParams=params,feedFilters=sfms_filters,resultkey="sfmsdf"))


    incremental = dict(type='IncrementalLoader',
                 properties=dict(reconName='NEFTOutward', source='sfmsdf', keycolumns=["Transaction Ref", 'Amount (Rs.)'],
                                 sourceTag='SFMS', resultkey='sfmsdf', function='Summary'))


    rectifiedentries= dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBSRECTIFIED",
                                                                 feedPattern="sol_[0-9]{4}_[0-9]{4}_1025400017_#%d%m%Y#_[0-9]{3}.lst",disableCarryFwd=True,
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),
                                                                 errors=False,feedFilters=rectified_filters,
                                                                 resultkey="recdf"))



    elem5 = dict(id=5, next=6, type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   keyColumns=["Transaction Ref", "DEBIT_AMT"],
                                                   sumColumns=[],
                                                   matchColumns=["Transaction Ref", "DEBIT_AMT"]), sourceTag="CBS"),
                                          dict(source="sfmsdf", columns=dict(
                                              keyColumns=["Transaction Ref", "Amount (Rs.)"],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref", "Amount (Rs.)"]), sourceTag="SFMS"),dict(source="cbstrdf", columns=dict(
                                              keyColumns=["Transaction Ref", "Amount"],
                                              sumColumns=[],
                                              matchColumns=["Transaction Ref", "Amount"]), sourceTag="CBSTRANSIT")],
                                 matchResult="results"))

    vlookup1 = dict(type='VLookup',
                   properties=dict(data='results.CBS', lookup='recdf', dataFields=["Transaction Ref", "DEBIT_AMT"],
                                   includeCols=["RECTIFIED_TRAN_DATE", "RECTIFIED_TRAN_ID"],
                                   lookupFields=["Transaction Ref", "Amount"], markers=dict(RectifiedEntries='MATCHED'),
                                   resultkey="results.CBS"))

    exp2 = dict(type='ExpressionEvaluator', properties=dict(source='results.CBS', expressions=[

        "df.loc[df.get('RectifiedEntries','')=='MATCHED','SFMS Match']='MATCHED'",
        "df.loc[df.get('RectifiedEntries','')=='MATCHED','CBSTRANSIT Match']='MATCHED'",
        "payload['RectifiedEntries1']=df[df.get('RectifiedEntries','')=='MATCHED']",
        "df.drop(['RectifiedEntries'],axis=1,inplace=True)"

    ], resultkey="results.CBS"))

    vlookup2 = dict(type='VLookup',
                   properties=dict(data='results.CBSTRANSIT', lookup='recdf', dataFields=["Transaction Ref", "Amount"],
                                   includeCols=["RECTIFIED_TRAN_DATE","RECTIFIED_TRAN_ID"],
                                   lookupFields=["Transaction Ref", "Amount"], markers=dict(RectifiedEntries='MATCHED'),
                                   resultkey="results.CBSTRANSIT"))

    exp3 = dict(type='ExpressionEvaluator', properties=dict(source='results.CBSTRANSIT', expressions=[

        "df.loc[df.get('RectifiedEntries','')=='MATCHED','SFMS Match']='MATCHED'",
        "df.loc[df.get('RectifiedEntries','')=='MATCHED','CBS Match']='MATCHED'",
        "payload['RectifiedEntries2']=df[df.get('RectifiedEntries','')=='MATCHED']",
        "df.drop(['RectifiedEntries'],axis=1,inplace=True)"

    ], resultkey="results.CBSTRANSIT"))




    filter1 = ["df = df[df['FORACID'].str.endswith('7551061657016')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='NEFT-INWARD-REJECTED-ACCOUNT-0755(S.C.)'",
               "df['Account Head']='07551061657016'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    filter2 = ["df = df[df['FORACID'].str.endswith('7551025398006')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='INWARD-REJECTED-ACCOUNT-8006'",
               "df['Account Head']='07551025398006'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]
    # #
    elem6 = dict(id=6,next=7, type="ReportGenerator", properties=dict(sources=[dict(source="results.CBS", sourceTag='CBS',
                                                                             filterConditions=filter1),
                                                                        dict(source="results.CBSTRANSIT", sourceTag='CBSTRANSIT',
                                                                             filterConditions=filter2)],
                                                               reportName='Principle',writeToFile=True,keycolumns=['Account Description','Account Head','Debit Amount','Credit Amount','Debit Count','Credit Count']))
    #

    RectifiedEntriesReport1 = dict(type='ReportGenerator',
                                  properties=dict(sources=[dict(source='RectifiedEntries1')],
                                                  writeToMongo=False,
                                                  writeToFile=True, reportName='RectifiedEntriesCBS'))

    RectifiedEntriesReport2 = dict(type='ReportGenerator',
                                   properties=dict(sources=[dict(source='RectifiedEntries2')],
                                                   writeToMongo=False,
                                                   writeToFile=True, reportName='RectifiedEntriesCBSTRANSIT'))

    elem7 = dict(id=7, next=8, type='GenerateReconSummary', properties=dict(
        sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['DEBIT_AMT']),
                 dict(resultKey="SFMS", sourceTag='SFMS', aggrCol=['Amount (Rs.)']),dict(resultKey="CBSTRANSIT", sourceTag='CBSTRANSIT', aggrCol=['Amount'])],groupbyCol=['STATEMENT_DATE']))

    elem8 = dict(id=8, type='DumpData', properties=dict(dumpPath='NEFTReturn', matched='all'))

    meta=dict(type='GenerateReconMetaInfo',properties=dict())

    elements = [elem1,elem2,elem3,elem4,incremental,rectifiedentries,elem5,vlookup1,exp2,vlookup2,exp3,elem6,RectifiedEntriesReport1,RectifiedEntriesReport2,elem7,elem8,meta]
    f = FlowRunner(elements)
    f.run()
