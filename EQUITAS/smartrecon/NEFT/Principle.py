import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    #stmtdate = "02-Sep-2017"
    basePath = "/usr/share/nginx/smartrecon/mft/"
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]


    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="NEFT", compressionType="zip", resultkey='', reconName='Principle',reportType='single'))

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="PRINCIPLE",
                                                                 feedPattern="sol_0500_0999_1025399010_*.*",
                                                                 feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
                                                                                 delimiter='|', skiprows=1),disableCarryFwd=True,
                                                                 resultkey="pdf"))

    inward = ["df = df[df['FORACID'].str.endswith('7551025399010')]",
               "df['TRAN_ID']=df['TRAN_ID'].str.strip()","df=df[ df['TRAN_PARTICULARS'].str.contains('1061657016')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='PRINCIPLE ACCT Branch Settlements Inward'",
               "df['Account Head']='07551025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    outward = ["df = df[df['FORACID'].str.endswith('7551025399010')]",
               "df['TRAN_ID']=df['TRAN_ID'].str.strip()", "df=df[ df['TRAN_PARTICULARS'].str.contains('1025399010')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='PRINCIPLE ACCT Branch Settlements Outward'",
               "df['Account Head']='07551025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    N04 = ["df = df[df['FORACID'].str.endswith('7551025399010')]",
               "df['TRAN_ID']=df['TRAN_ID'].str.strip()",
               "df=df[(~df['TRAN_ID'].str.startswith('S')) & ((df['TRAN_PARTICULARS'].str.match(r'NEFT SETTLE*.*'))|(df['TRAN_PARTICULARS'].str.match(r'NEFT SETTLEMENT [0-9]{6}'))) & (df['TRAN_RMKS'].isnull())]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='PRINCIPLE ACCT-N04-RBI-NET-SETTLEMENT'",
               "df['Account Head']='07551025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    NEFTRBIN07= ["df = df[df['FORACID'].str.endswith('7551025399010')]",
               "df['TRAN_ID']=df['TRAN_ID'].str.strip()",
               "df=df[(df['TRAN_ID'].str.startswith('S')) & ((df['TRAN_PARTICULARS'].isin(['NEFT RBI'])))& df['TRAN_RMKS'].fillna('').str.match('NEFT RBI\/[0-9]{4}\/[A-Z0-9]{3}')]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='PRINCIPLE ACCT-N07-RBI-NET-SETTLEMENT'",
               "df['Account Head']='07551025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    cbsadjust= ["df = df[df['FORACID'].str.endswith('7551025399010')]",
               "df['TRAN_ID']=df['TRAN_ID'].str.strip()",
              "df=df[(df['TRAN_ID'].str.startswith('S')) & ~((df['TRAN_PARTICULARS'].str.contains('NEFT|NEFT RBI',regex=True))) & ~((df['TRAN_RMKS'].fillna('').str.match('NEFT RBI\/[0-9]{4}\/[A-Z0-9]{3}'))|(df['TRAN_RMKS'].fillna('').str.match('NEFT SETTLEMENT')))]",
               "df['Debit Count'] = 1", "df['Credit Count'] = 1",
               "df['Account Description']='PRINCIPLE ACCT CBS Adjustment '",
               "df['Account Head']='07551025399010'",
               "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
               "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    Manual = ["df = df[df['FORACID'].str.endswith('7551025399010')]",
                 "df['TRAN_ID']=df['TRAN_ID'].str.strip()",
                 "df=df[(~df['TRAN_ID'].str.startswith('S')) & ~(df['TRAN_PARTICULARS'].str.contains('NEFT')) & (df['TRAN_RMKS'].isnull())]",
                 "df['Debit Count'] = 1", "df['Credit Count'] = 1",
                 "df['Account Description']='PRINCIPLE ACCT Manual Postings'",
                 "df['Account Head']='07551025399010'",
                 "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
                 "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]


    TotalCreditDebit=["df = df[df['FORACID'].str.endswith('7551025399010')]",
                     "df['Debit Count'] = 1", "df['Credit Count'] = 1",
                     "df['Account Description']='TotalCreditDebitSum'",
                     "df['Account Head']='07551025399010'",
                     "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
                     "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]

    Others=["df = df[df['FORACID'].str.endswith('7551025399010')]",
                     "df['Debit Count'] = 1", "df['Credit Count'] = 1",
                     "df['Account Description']='PRINCIPLE Others'",
                     "df['Account Head']='07551025399010'",
                     "df.loc[df['TRAN_PARTICULARS'].str.contains('1061657016'),'Status']='inward'",
                     "df.loc[(~df['TRAN_ID'].str.startswith('S')) & ((df['TRAN_PARTICULARS'].str.match(r'NEFT SETTLE*.*'))|(df['TRAN_PARTICULARS'].str.match(r'NEFT SETTLEMENT [0-9]{6}'))) & (df['TRAN_RMKS'].isnull()),'Status']='N04'",
                     "df.loc[df['TRAN_PARTICULARS'].str.contains('1025399010'),'Status']='outward'",
                     "df.loc[(df['TRAN_ID'].str.startswith('S')) & ((df['TRAN_PARTICULARS'].isin(['NEFT RBI'])))& df['TRAN_RMKS'].fillna('').str.match('NEFT RBI\/[0-9]{4}\/[A-Z0-9]{3}'),'Status']='NEFTRBIN07'",
                     "df.loc[(df['TRAN_ID'].str.startswith('S')) & ~((df['TRAN_PARTICULARS'].str.contains('NEFT|NEFT RBI',regex=True))) & ~((df['TRAN_RMKS'].fillna('').str.match('NEFT RBI\/[0-9]{4}\/[A-Z0-9]{3}'))|(df['TRAN_RMKS'].fillna('').str.match('NEFT SETTLEMENT'))),'Status']='cbsadjust'",
                     "df.loc[(~df['TRAN_ID'].str.startswith('S')) & ~(df['TRAN_PARTICULARS'].str.contains('NEFT')) & (df['TRAN_RMKS'].isnull()),'Status']='Manual'",
                     "df=df[~df['Status'].isin(['inward','outward','NEFTRBIN07','cbsadjust','Manual','N04'])]",
                     "df = df.groupby(['Account Description','Account Head'])['DEBIT_AMT','Amount','Debit Count','Credit Count'].sum().reset_index()",
                     "df.rename(columns={'Amount':'Credit Amount','DEBIT_AMT':'Debit Amount'},inplace=True)"]


    LoadExternalReport1=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTReturn',filepath='OUTPUT/Principle_Report.csv',resultKey='returndf'))

    LoadExternalReport2=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTOutward',filepath='OUTPUT/Principle_Report.csv',resultKey='outwarddf'))

    LoadExternalReport3=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTInward',filepath='OUTPUT/Principle_Report.csv',resultKey='inwarddf'))

    LoadExternalReport4=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTInward',filepath='OUTPUT/recon_summary.csv',resultKey='inwardsummarydf'))

    LoadExternalReport5=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTOutward',filepath='OUTPUT/recon_summary.csv',resultKey='outwardsummarydf'))

    LoadExternalReport6=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTReturn',filepath='OUTPUT/recon_summary.csv',resultKey='returnsummarydf'))


    ReportGenerator = dict(id=3, type="ReportGenerator", properties=dict(sources=[dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=inward),dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=outward),dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=N04),dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=NEFTRBIN07),
                                                                        dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=cbsadjust),
                                                                        dict(source="pdf", sourceTag='CBS',
                                                                             filterConditions=Manual),
                                                                        dict(source="pdf", sourceTag='CBS',
                                                                                       filterConditions=TotalCreditDebit),
                                                                        dict(source="pdf", sourceTag='CBS',
                                                                                       filterConditions=Others)],keycolumns=['Account Description','Account Head','Debit Amount','Credit Amount','Debit Count','Credit Count'],
                                                               reportName='Principle',writeToMongo=True,writeToFile=True))

    ComputeClosingBalance=dict(type='ComputeClosingBalance',properties=dict(sources=[dict(source="custom_reports.Principle",sourceTag="principle",resultKey='principle',
                                                                            filterConditions=["df=df[df['Account Description'].str.contains('PRINCIPLE')]",
                                                                                              "df['Net Diff']=df['Credit Amount']-df['Debit Amount']"
                                                                                              ],keyColumn='Net Diff',resultkey="principle")],addToResult=True))


    SourceConcat=dict(type='SourceConcat',properties=dict(sourceList=['returndf','outwarddf','inwarddf','principle'],resultKey='principle'))



    ReportGenerator2=dict(type='ReportGenerator',properties=dict(sources=[dict(source='principle')],writeToMongo=False,writeToFile=True,reportName='Principle'))

    inwardSummary=dict(type='ReportGenerator',properties=dict(sources=[dict(source='inwardsummarydf')],writeToMongo=False,writeToFile=True,reportName='InwardSummary'))

    outwardSummary=dict(type='ReportGenerator',properties=dict(sources=[dict(source='outwardsummarydf')],writeToMongo=False,writeToFile=True,reportName='OutwardSummary'))

    returnSummary=dict(type='ReportGenerator',properties=dict(sources=[dict(source='returnsummarydf')],writeToMongo=False,writeToFile=True,reportName='ReturnSummary'))


    dumpdata = dict(type='DumpData', properties=dict(dumpPath='Principle', matched='all',exportType='singleSheet'))

    meta=dict(type='GenerateReconMetaInfo',properties=dict(exportType='singleSheet'))


    elements = [elem1,elem2,LoadExternalReport1,LoadExternalReport2,LoadExternalReport3,LoadExternalReport4,LoadExternalReport5,LoadExternalReport6,ReportGenerator,ComputeClosingBalance,SourceConcat,ReportGenerator2,inwardSummary,outwardSummary,returnSummary,dumpdata,meta]
    f = FlowRunner(elements)
    f.run()
