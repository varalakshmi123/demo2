import pandas as pd
import os
import subprocess
import re

fileproperties = {
    "CSV": {"types": ['txt', 'lst', 'csv'], "delimiter": [',', '|', ';']},
    "Excel": {"types": ['xlsx', 'xls']},
    "XML":{"types":['xml']},
    "FixedFormat":{"types":['txt','lst'],"delimiter":[' ']}
}

checkforDelimiter={
    "listOfNonDelimitedTypes":['xls','xlsx','xml']
}

def getDelimiter(sourcefile, delimiterlist):
    file = open(sourcefile, 'r')
    data = " ".join(file.readlines()[0:25])
    countlist = []
    for delimiter in delimiterlist:
        countlist.append(data.count(delimiter))
    max_value = max(countlist)
    max_index = countlist.index(max_value)
    return delimiterlist[max_index]


def getSourceProperties(sourcefile):
    delimiter=None
    fileName = os.path.basename(sourcefile)
    fileExtension = fileName.split('.')[1]
    filePattern=getfilePattern(fileName.split('.')[0])+'.'+fileExtension
    loadType,delimiter = fileTypeMapper(sourcefile,fileExtension)

    print fileName
    print delimiter
    print loadType
    print filePattern


def fileTypeMapper(sourcefile,fileExtension):
    delimiter=None
    delimiterList=getDelimiterList()
    if fileExtension not in checkforDelimiter['listOfNonDelimitedTypes']:
       delimiter=getDelimiter(sourcefile,delimiterList)
    loadType=getLoadType(fileExtension,delimiter)
    return loadType,delimiter
    # for key,val in fileproperties.iteritems():
    #     if delimiter in fileExtension val['delimiter']:
    #         LoadType=key

    return
def getfilePattern(filename):
    ext = "".join(['$' if x.isdigit() else x for x in filename])
    prev=ext[0]
    count=1
    for c in ext[1:]:
       if prev!=c:
           # if prev=='w':
           #   ext=ext.replace(prev*count,'[a-zA-z]{'+str(count)+'}',1)
           #   count=1
           if prev=='$':
               ext=ext.replace(prev*count, '[0-9]{' + str(count) + '}',1)
               count = 1
           else:
               count=1
       else:
           count+=1
       prev=c
    else:
        # if prev == 'w':
        #     ext = ext.replace(prev * count, '[a-zA-z]{' + str(count) + '}', 1)
        if prev == '$':
            ext = ext.replace(prev * count, '[0-9]{' + str(count) + '}', 1)
    return ext


def getDelimiterList():
    delimiterList=[]
    for key,property in fileproperties.iteritems():
        if 'delimiter' in property.keys():
          delimiterList.extend(property['delimiter'])

    return delimiterList


def getLoadType(fileExtension,delimiter):
    if delimiter is None:
        for key, property in fileproperties.iteritems():
            if fileExtension in property['types']:
                loadType=key
    else:
        for key, property in fileproperties.iteritems():
            if fileExtension in property['types'] and delimiter in property['delimiter']:
                loadType=key
    return loadType
if __name__ == '__main__':
    source = '/usr/share/nginx/smartrecon/mft/NEFT_EQT_OUTWARD/13072018/OUTPUT/CBS.csv'

    getSourceProperties(source)
    # filename='MOB_BBPS_23072018'

    # getfilePattern(filename)

