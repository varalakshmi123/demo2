import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import numpy as  np

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="ATM", compressionType="zip", resultkey='',
                                reconName="ATM_EQT_ACQUIRER"))
    gl_filters = ["df['REF_DOC_NO'] = df['REF_DOC_NO'].fillna('NA')", "df['COD_DRCR_USE'] = df['COD_DRCR'].copy()",
                  "df = df.loc[df['COD_GL_ACCT'] == '114210042']", "df.loc[df['AMT_TXN_LCY']<0,'COD_DRCR_USE'] = 'D' ",
                  "df['AMT_TXN_LCY_USE'] = df['AMT_TXN_LCY'].abs()"]
    acquirer_filters = ["df['Transaction Amount'] = df['Transaction Amount'].astype(float)/100"]
    ver_filters = ["df['RRN'] = df['RRN'].apply(lambda x:x[1:])", "df['RRN'] = df['RRN'].astype(str)"]
    load_acquirer = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI", feedPattern="ACQRPESF*.*",
                                                         feedParams=dict(feedformatFile='NPCI_OUTWARD_Structure.csv',
                                                                         type="OUTWARD"), feedFilters=acquirer_filters,
                                                         resultkey="acqrdf"))
    load_gl = dict(type="PreLoader", properties=dict(loadType="Excel", source="ACQUIRER", feedPattern="GL_*.*",
                                                     feedParams=dict(feedformatFile='GL_NFS_EQT_Structure.csv',
                                                                     type="GL", skiprows=1), feedFilters=gl_filters,
                                                     resultkey="gldf"))
    load_ver = dict(type="PreLoader",
                    properties=dict(loadType="HTML", source="VerifReversalfile", feedPattern="VerifReversal*.*",
                                    feedParams=dict(feedformatFile='ATM_NFS_Verification.csv', type="Ver",
                                                    parseIndex=[2], skiprows=[1]), feedFilters=ver_filters,
                                    resultkey="verdf"))
    nwayacquirer_match = dict(type="NWayMatch",
                            properties=dict(sources=[dict(source="acqrdf",
                                                          columns=dict(
                                                              keyColumns=['Ref Number', 'Transaction Amount'],
                                                              sumColumns=[],
                                                              matchColumns=['Ref Number', 'Transaction Amount']),
                                                          subsetfilter=[
                                                              "df['Ref Number'] =  df['Ref Number'].astype(str)",
                                                              "df['Transaction Amount'] = df['Transaction Amount'].astype(np.float64)"],
                                                          sourceTag="NFS_ACQUIRER"),
                                                     dict(source="gldf", columns=dict(
                                                         keyColumns=['REF_DOC_NO', 'AMT_TXN_LCY'], sumColumns=[],
                                                         matchColumns=['REF_DOC_NO', 'AMT_TXN_LCY']),
                                                          subsetfilter=["df = df[df['REF_DOC_NO']!='NA']",
                                                                        "df['REF_DOC_NO'] = df['REF_DOC_NO'].astype(str)",
                                                                        "df['AMT_TXN_LCY'] = df['AMT_TXN_LCY'].astype(np.float64)"],
                                                          sourceTag="GL_NFS")],
                                            matchResult="results"))
    gl_reversal = dict(type="NWayMatch",
                       properties=dict(sources=[dict(source="results.GL_NFS",
                                                     columns=dict(
                                                         keyColumns=['REF_DOC_NO'],
                                                         amountColumns=["AMT_TXN_LCY"],
                                                         matchColumns=['REF_DOC_NO'],
                                                         crdrcolumn=['COD_DRCR'], CreditDebitSign=False),
                                                     subsetfilter=["df = df.loc[df['NFS_ACQUIRER Match'] == 'UNMATCHED']",
                                                                   "df = df.loc[df['REF_DOC_NO'] != 'NA']"],
                                                     sourceTag="GL_NFS")], matchResult="results"))
    ver_vlookup = dict(type='VLookup',
                       properties=dict(data='results.GL_NFS', lookup='verdf', dataFields=['REF_DOC_NO'],
                                       lookupFields=['RRN'], markers={'REMARKS': 'YES'},
                                       resultkey="results.GL_NFS"))
    vlookup_report = dict(type="ReportGenerator",
                          properties=dict(sources=[dict(source='results.GL_NFS', sourceTag="VLOOK_REPORT")],
                                          writeToFile=True, reportName='Acquirer Verification Report'))
    evaluator = dict(type="ExpressionEvaluator", properties=dict(source="results.GL_NFS",
                                                             expressions=["df.loc[df['REF_DOC_NO'] == 'NA','REMARKS'] = 'MANUAL ENTRY' ",
                                                                          "df.loc[df['NFS_ACQUIRER Match'] == 'MATCHED','REMARKS'] = 'NPCI SETTLED ON 30-JUN-18' ",
                                                                          "df.loc[df['DAT_TXN_PROCESSING'].dt.hour >= 23,'REMARKS'] = 'POST CUTOFF' ",
                                                                          "df.loc[df['Self Matched'] == 'True','REMARKS'] = 'REVERSAL' "
                                                                 ], resultkey='results.GL_NFS'))

    dump_data = dict(type="DumpData", properties={})
    gen_meta_info = dict(type='GenerateReconMetaInfo')
    elements = [init, load_acquirer, load_gl,load_ver,nwayacquirer_match,gl_reversal,ver_vlookup,evaluator,dump_data,gen_meta_info]
    f = FlowRunner(elements)
    f.run()
