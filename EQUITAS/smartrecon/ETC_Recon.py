import os
import sys
import zipfile
from datetime import datetime
from shutil import rmtree
from traceback import format_exc

import pandas as pd
from pymongo import MongoClient

import config
import logger
from Matcher import Matcher
from Utilities import Utilities
from prerecon.FeedLoader import FeedLoaderFactory
from prerecon.FilterExecutor import FilterExecutor
from prerecon.FindFiles import FindFiles

logger = logger.Logger.getInstance("smartrecon").getLogger()


class ETCRecon():
    def __init__(self):
        pass

    def initEtcRecon(self, stmtDate):
        self.statementDate = stmtDate
        try:
            stmtDate = datetime.strptime(statementDate, '%d-%b-%Y')
        except:
            logger.info("Statement date provided is not in the required format 'DD-MMM-YYYY'")
            logger.info('----------------------------------------')
            logger.info('Recon job execution ABORTED')
            logger.info('----------------------------------------')
            exit(0)

        logger.info("----------------------------------------")
        logger.info("ETC Recon Job Initiated")
        logger.info("Statement Date : %s" % stmtDate.strftime('%d-%b-%Y'))

        self.mdb = MongoClient('localhost')['erecon']

        # check if statement date already exists
        execDoc = self.mdb['recon_execution_details_log'].find_one(
            {"STATEMENT_DATE": stmtDate, 'RECON_ID': "IMPS_RECON",
             'PROCESSING_STATE_STATUS': 'Matching Completed'})
        if execDoc:
            logger.info("Statement Date Already Exists")
            logger.info('----------------------------------------')
            logger.info('Recon job execution ABORTED')
            logger.info('----------------------------------------')
            raise ValueError("Statement %s Date Already Exists" % stmtDate.strftime("%d-%b-%Y"))

        jobID = Utilities().getNextSeq()

        self.prev_job_doc = Utilities().getPreviousExecutionId('ETC_RECON')

        jobSummary = dict()
        jobSummary['RECON_ID'] = "ETC_RECON"
        jobSummary['RECON_EXECUTION_ID'] = jobID
        jobSummary["partnerId"] = "54619c820b1c8b1ff0166dfc"
        jobSummary["machineId"] = "16af9057-69af-4c69-bbcf-70d99334e027"
        jobSummary['created'] = Utilities().getUtcTime()
        jobSummary['STATEMENT_DATE'] = stmtDate

        try:
            flFactory = FeedLoaderFactory()
            self.csvLoader = flFactory.getInstance("CSV")
            self.excelLoader = flFactory.getInstance("Excel")
            self.findFiles = FindFiles(stmtDate)
            self.source_path = config.basepath + "mft/ETC/" + stmtDate.strftime('%d%m%Y') + '/'
            self.ReportPath = self.source_path + 'OUTPUT/'

            # check if files exists in path else remove all over-write
            if os.path.isdir(config.basepath + "mft/ETC/" + stmtDate.strftime('%d%m%Y')):
                rmtree(config.basepath + "mft/ETC/" + stmtDate.strftime('%d%m%Y'), ignore_errors=True)

            if os.path.isfile(config.basepath + "mft/ETC/" + stmtDate.strftime('%d%m%Y') + '.zip'):
                extractFile = zipfile.ZipFile(config.basepath + "mft/ETC/" + stmtDate.strftime('%d%m%Y') + '.zip')
                extractFile.extractall(config.basepath + "mft/ETC/")

            try:
                if not os.path.exists(self.ReportPath):
                    os.mkdir(self.ReportPath)
            except OSError as exc:  # Guard against race condition
                import errno

                if exc.errno != errno.EEXIST:
                    raise

            self.carryForwardLoader = flFactory.getInstance('CarryForward')

            self.loadSources()
            self.perform_GI_NPCI_Match()
            self.perform_GI_CBSGI_Match()
            self.perform_CBSGI_CBS_Match()
            self.tag_issuance_recon()

            jobSummary['PROCESSING_STATE_STATUS'] = "Matching Completed"
            jobSummary['JOB_STATUS'] = "SUCCESS"
        except Exception:
            logger.info('----------------------------------------')
            logger.info('Recon job execution ABORTED')

            jobSummary['PROCESSING_STATE_STATUS'] = "Matching Failed"
            jobSummary['JOB_STATUS'] = "Failed"
            logger.info(format_exc())
            logger.info('----------------------------------------')

        os.system("cd %s ;zip -r ETC_Report.zip ." % self.ReportPath)
        outward_report = self.ReportPath + 'ETC_Report.zip'
        logger.info('ETC_Report output report path : ' + outward_report)

        self.mdb['recon_execution_details_log'].save(jobSummary)

    def loadSources(self):
        etcTxnFiles = self.findFiles.findFiles(feedPath=self.source_path, fpattern="ETC Transaction Report.xls")
        etcTxnReportDf = self.excelLoader.loadFile(etcTxnFiles,
                                                   {"feedformatFile": "ETC_TXN_Report_Structure.csv",
                                                    "skiprows": 1, 'skipfooter': 1})
        etcTxnReportFilter = ["df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
                              "df = df[df['TransactionType'] != 'NON_FIN']"]
        self.etcTxnReportDf = FilterExecutor().run(etcTxnReportDf, etcTxnReportFilter)
        self.etcTxnReportDf['SOURCE'] = 'ETC Txn Report'

        ### CBS Txn Repors
        cbsTxnFiles = self.findFiles.findFiles(feedPath=self.source_path, fpattern="CBS Transaction Report.xls")
        cbsTxnReportDf = self.excelLoader.loadFile(cbsTxnFiles,
                                                   {"feedformatFile": "CBS_TXN_REPORT_Structure.csv",
                                                    "skiprows": 1})
        cbsTxnReportFilter = ["df = df[df['Narration'].astype(str).str.startswith('ETCT')]",
                              "df['TransactionNo'] = df['TRANSACTIONID'].astype(str).str[:-12]",
                              "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
                              "df['Unique Ref No'] = df['Narration'].str[5:]"]
        self.cbsTxnReportDf = FilterExecutor().run(cbsTxnReportDf, cbsTxnReportFilter)
        self.cbsTxnReportDf['SOURCE'] = 'CBS Txn Report'

        ### Load CBS GL & All GL
        cbsAllGl = self.findFiles.findFiles(feedPath=self.source_path, fpattern="allgl.csv")
        cbsGl = self.findFiles.findFiles(feedPath=self.source_path, fpattern="GL*.*.csv")
        cbsAll = cbsGl + cbsAllGl

        cbsGlReportdf = pd.DataFrame()
        each = dict()
        print cbsAll
        for i in cbsAll:
            each = self.loadCombinedGL(i)
            cbsGlReportdf = cbsGlReportdf.append(each)
            print len(cbsGlReportdf)
        print cbsGlReportdf['FEED_FILE_NAME'].unique()

        cbsGlReportdf = cbsGlReportdf[~cbsGlReportdf['Description'].str.contains("ETCCB")]
        cbsGlReportdf['Unique Ref No'] = cbsGlReportdf['Description'].apply(lambda x: x.split(':')[-1])
        cbsGlReportdf['Unique Ref No'] = cbsGlReportdf['Unique Ref No'].str[5:]
        cbsGlReportdf['Unique Ref No'] = cbsGlReportdf['Unique Ref No'].str.strip()
        cbsGlReportdf = cbsGlReportdf[cbsGlReportdf['User ID (maker)'] == 'GIUSER']
        cbsGlReportdf['trn_date'] = pd.to_datetime(cbsGlReportdf['Transaction Initiation Date'],
                                                        format='%d-%m-%Y %H:%M:%S')
        cbsGlReportdf['SOURCE'] = 'CBS-GL'

        self.cbsGlReportdf = cbsGlReportdf.copy()

        ######
        selfRegFiles = self.findFiles.findFiles(feedPath=self.source_path, fpattern="Self Registration-CBS.xls")
        self.selfRegReportDf = self.excelLoader.loadFile(selfRegFiles,
                                                         {"feedformatFile": "Self_Registration_Structure.csv",
                                                          "skiprows": 1})
        self.selfRegReportDf['SOURCE'] = 'Self Reg Report'

        ######
        tagIssuanceFiles = self.findFiles.findFiles(feedPath=self.source_path, fpattern="Tag Issuance - CBS.xls")
        self.tagIssuanceReportDf = self.excelLoader.loadFile(tagIssuanceFiles,
                                                             {"feedformatFile": "Tag_Issuance_CBS_Structure.csv",
                                                              "skiprows": 1})
        self.tagIssuanceReportDf['SOURCE'] = 'Tag Issuance Report'

    def loadCombinedGL(self, glfile):
        columns = pd.read_csv("Reference/GL MAPPING.csv")
        if 'allgl.csv' in glfile:
            srccolumns = columns["All GL"].tolist()
            mappedcols = columns["Common Field GL"].tolist()
            renamedict = {}
            for i in range(0, len(srccolumns)):
                renamedict[srccolumns[i]] = mappedcols[i]
            gl = pd.read_csv(glfile)
            gl = gl[srccolumns]
            gl = gl.rename(columns=renamedict)
            gl['FEED_FILE_NAME'] = glfile.split('/')[-1]
        else:
            srccolumns = columns["GL Report"].tolist()
            mappedcols = columns["Common Field GL"].tolist()
            renamedict = {}
            for i in range(0, len(srccolumns)):
                renamedict[srccolumns[i]] = mappedcols[i]
            gl = pd.read_csv(glfile)
            gl = gl[srccolumns]
            gl = gl.rename(columns=renamedict)
            gl['FEED_FILE_NAME'] = glfile.split('/')[-1]
        return gl

    def perform_GI_NPCI_Match(self):
        npci_files = self.findFiles.findFiles(feedPath=self.source_path, fpattern="841ESFB756[0-9]{11}.csv")
        npci = self.csvLoader.loadFile(npci_files,
                                       {"delimiter": ",", "feedformatFile": "NPCI_POST_Settlement_Structure.csv",
                                        "skiprows": 1})
        npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                        "df['TransactionNo'] = df['TransactionNo'].str.lstrip(\"'\")",
                        "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
                        "df['Transaction Amount'] = df['Transaction Amount'].astype(np.float64) / 100.00"]

        npci = FilterExecutor().run(npci, npci_filters)

        # if self.prev_job_doc:
        #     cf_report = ''
        #     if 'OUTWARD_REPORT_PATH' in self.prev_job_doc.keys():
        #         logger.info("Loading Carry Foward Records for UPI-ACQUIRER")
        #         npci = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                            params={"source": "NPCI", "dest": npci})
        #         self.etcTxnReportDf = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                           params={"source": "CBS", "dest": self.etcTxnReportDf})

        npciReport, etcReport = Matcher().match(npci, self.etcTxnReportDf,
                                                leftkeyColumns=["TransactionNo", 'Transaction Amount'],
                                                rightkeycolumns=["TransactionNo", 'Transaction Amount'],
                                                leftsumColumns=[],
                                                rightsumColumns=[],
                                                leftmatchColumns=["TransactionNo", "Transaction Amount"],
                                                rightmatchColumns=["TransactionNo", "Transaction Amount"])

        concat_records = npciReport.append(etcReport, ignore_index=True)
        total_matched = concat_records[concat_records['Matched'] == 'MATCHED']

        if not os.path.exists(self.ReportPath + '/' + 'GI_vs_NPCI/'):
            os.mkdir(self.ReportPath + '/' + 'GI_vs_NPCI/')

        tmp_path = self.ReportPath + '/' + 'GI_vs_NPCI/'
        total_matched.to_csv(tmp_path + 'MatchedReport.csv', index=False)
        npciReport[npciReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + 'NPCI_UnMatchedReport.csv', index=False)
        etcReport[etcReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + "ETC_Txn_UnMatchedReport.csv", index=False)

    def perform_GI_CBSGI_Match(self):
        # gl_files = self.findFiles.findFiles(feedPath=self.source_path, fpattern="CBS Transaction Report.xls")
        # cbs_files = self.findFiles.findFiles(feedPath=self.source_path, fpattern="ETC Transaction Report.xls")
        #
        # # CBS_TXN_REPORT_Structure.csv
        # gl = self.excelLoader.loadFile(gl_files,
        #                                {"feedformatFile": "CBS_TXN_REPORT_Structure.csv",
        #                                 "skiprows": 1})
        # cbs = self.excelLoader.loadFile(cbs_files,
        #                                 {"feedformatFile": "ETC_TXN_Report_Structure.csv",
        #                                  "skiprows": 1, 'skipfooter': 1})
        #
        # gl_filters = ["df = df[df['Narration'].astype(str).str.startswith('ETCT')]",
        #               "df['TransactionNo'] = df['TRANSACTIONID'].astype(str).str[:-12]",
        #               "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')"]
        # gl = FilterExecutor().run(gl, gl_filters)
        #
        # cbs_filters = ["df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')",
        #                "df = df[df['TransactionType'] != 'NON_FIN']"]
        # cbs = FilterExecutor().run(cbs, cbs_filters)

        # if self.prev_job_doc:
        #     cf_report = ''
        #     if 'OUTWARD_REPORT_PATH' in self.prev_job_doc.keys():
        #         logger.info("Loading Carry Foward Records for UPI-ACQUIRER")
        #         self.cbsTxnReportDf = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                                 params={"source": "NPCI", "dest": self.cbsTxnReportDf })
        #         self.etcTxnReportDf = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                                                params={"source": "CBS",
        #                                                                        "dest": self.etcTxnReportDf})

        cbsReport, etcReport = Matcher().match(self.cbsTxnReportDf, self.etcTxnReportDf,
                                               leftkeyColumns=["TransactionNo", 'Transaction Amount'],
                                               rightkeycolumns=["TransactionNo", 'Transaction Amount'],
                                               leftsumColumns=[],
                                               rightsumColumns=[],
                                               leftmatchColumns=["TransactionNo", "Transaction Amount"],
                                               rightmatchColumns=["TransactionNo", "Transaction Amount"])

        concat_records = cbsReport.append(etcReport, ignore_index=True)
        total_matched = concat_records[concat_records['Matched'] == 'MATCHED']

        if not os.path.exists(self.ReportPath + '/' + 'GI_vs_CBSGI/'):
            os.mkdir(self.ReportPath + '/' + 'GI_vs_CBSGI/')

        tmp_path = self.ReportPath + '/' + 'GI_vs_CBSGI/'
        total_matched.to_csv(tmp_path + 'MatchedReport.csv', index=False)
        cbsReport[cbsReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + 'CBS_Txn_UnMatchedReport.csv', index=False)
        etcReport[etcReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + "ETC_Txn_UnMatchedReport.csv", index=False)

    def perform_CBSGI_CBS_Match(self):
        # allgl = self.findFiles.findFiles(feedPath=self.source_path, fpattern="allgl.csv")
        # Gl = self.findFiles.findFiles(feedPath=self.source_path, fpattern="GL.csv")
        # gl = pd.DataFrame()
        # for i, j in zip(allgl,Gl):
        #     each = self.loadCombinedGL(i, j)
        #     gl = gl.append(each)
        # print len(gl)
        #  cbs = self.excelLoader.loadFile(cbs_files,
        #                                  {"feedformatFile": "CBS_TXN_REPORT_Structure.csv",
        #                                   "skiprows": 1})
        # cbs = self.cbsTxnReportDf
        # cbs_filter = ["df = df[df['Narration'].astype(str).str.startswith('ETCT')]",
        #               "df['TransactionNo'] = df['TRANSACTIONID'].astype(str).str[:-12]",
        #               "df['TransactionNo'] = df['TransactionNo'].str.lstrip('0')"]
        # cbs = FilterExecutor().run(cbs, cbs_filter)

        # exit(0)
        # if self.prev_job_doc:
        #     cf_report = ''
        #     if 'OUTWARD_REPORT_PATH' in self.prev_job_doc.keys():
        #         logger.info("Loading Carry Foward Records for UPI-ACQUIRER")
        #         self.cbsTxnReportDf = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                                 params={"source": "NPCI", "dest": self.cbsTxnReportDf })
        #         self.etcTxnReportDf = self.carryForwardLoader.loadFile([self.prev_job_doc['OUTWARD_REPORT_PATH']],
        #                                                                params={"source": "CBS",
        #                                                                        "dest": self.etcTxnReportDf})

        # self.cbsTxnReportDf['Unique Ref No'] =self.cbsTxnReportDf['Narration'].str[5:]
        # gl = gl[~gl['Description'].str.contains("ETCCB")]
        # gl['Unique Ref No'] = gl['Description'].apply(lambda x: x.split(':')[-1])
        # gl['Unique Ref No'] = gl['Unique Ref No'].str[5:]
        # gl = gl[~gl['Account Number'].isin(['99990204020028', '99990204020027'])]
        # gl = gl[gl['User ID (maker)'] == 'GIUSER']
        #

        cbsGlReport = self.cbsGlReportdf.copy()
        cbsGlReport = cbsGlReport[
            ~cbsGlReport['Account Number'].isin(['99990204020028', '99990204020027'])]
        cbsGlReport['trn_date'] = pd.to_datetime(cbsGlReport['Transaction Initiation Date'], format='%d-%m-%Y %H:%M:%S')
        cbsGlReport = cbsGlReport[cbsGlReport['trn_date'].dt.strftime('%d%m%y') == "171217"]

        print cbsGlReport.head()
        cbsGlReport, cbsReport = Matcher().match(cbsGlReport, self.cbsTxnReportDf,
                                                 leftkeyColumns=["Unique Ref No"],
                                                 rightkeycolumns=["Unique Ref No"],
                                                 leftsumColumns=['Transaction Amount'],
                                                 rightsumColumns=['Transaction Amount'],
                                                 leftmatchColumns=["Unique Ref No", "Transaction Amount"],
                                                 rightmatchColumns=["Unique Ref No", "Transaction Amount"])

        concat_records = cbsReport.append(cbsGlReport, ignore_index=True)

        if not os.path.exists(self.ReportPath + '/' + 'CBSGI_vs_CBS/'):
            os.mkdir(self.ReportPath + '/' + 'CBSGI_vs_CBS/')

        tmp_path = self.ReportPath + '/' + 'CBSGI_vs_CBS/'
        concat_records[concat_records['Matched'] == 'MATCHED'].to_csv(tmp_path + 'MatchedReport.csv', index=False)
        cbsReport[cbsReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + 'CBS_Txn_UnMatchedReport.csv', index=False)
        cbsGlReport[cbsGlReport['Matched'] == 'UNMATCHED'].to_csv(tmp_path + "CBSGL_Txn_UnMatchedReport.csv",
                                                                  index=False)

    def tag_issuance_recon(self):
        creditFilter = ["df = df[df['Credit Account'] == '204020027']",
                        "df['Unique Ref No'] = df.loc[:, 'Narration'].astype(str).str[5:]",
                        "df['Unique Ref No'] = df['Unique Ref No'].str.strip()"]

        selfRegDf = self.selfRegReportDf.copy()
        selfRegDf = FilterExecutor().run(selfRegDf, creditFilter)

        cbsGlFilter = ["df['Account Number'] = df['Account Number'].astype(str)",
                       "df = df[df['Account Number'] == '99990204020027']"]

        print self.cbsGlReportdf['FEED_FILE_NAME'].unique()

        cbsGlReportdf = self.cbsGlReportdf.copy()
        cbsGlReportdf = FilterExecutor().run(cbsGlReportdf, cbsGlFilter)

        selfRegDf, right = Matcher().match(selfRegDf, cbsGlReportdf,
                                           leftkeyColumns=["Unique Ref No"],
                                           rightkeycolumns=["Unique Ref No"],
                                           leftmatchColumns=["Unique Ref No"],
                                           rightmatchColumns=["Unique Ref No"])

        tagIssuanceDf = self.tagIssuanceReportDf.copy()
        tagIssuanceDf = FilterExecutor().run(tagIssuanceDf, creditFilter)

        tagIssuanceDf, right = Matcher().match(tagIssuanceDf, cbsGlReportdf,
                                               leftkeyColumns=["Unique Ref No"],
                                               rightkeycolumns=["Unique Ref No"],
                                               leftmatchColumns=["Unique Ref No"],
                                               rightmatchColumns=["Unique Ref No"])

        if not os.path.exists(self.ReportPath + '/' + 'TAG_Issuance/'):
            os.mkdir(self.ReportPath + '/' + 'TAG_Issuance/')

        tmp_path = self.ReportPath + '/' + 'TAG_Issuance/'

        selfRegDf.to_csv(tmp_path + "SelfRegistration.csv", index=False)
        tagIssuanceDf.to_csv(tmp_path + 'TagIssuance.csv', index=False)


if __name__ == "__main__":
    statementDate = sys.argv[1]
    ETCRecon().initEtcRecon(statementDate)
