import sys
# from StyleFrame import StyleFrame, Styler
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas

if __name__ == "__main__":
    stmtdate = '22-Oct-2018'
    uploadFileName = '22102018.zip'

    df = pandas.read_csv(config.basepath + "Reference/AccountOpening.csv").fillna('')
    crmcolumns = df["CRM"].tolist()
    wizardcolumns = df["Wizard"].tolist()
    cbscolumns = df["CBS"].tolist()
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, reconName="AccountOpening", recontype='CustomerDetails',
                                 compressionType="zip", resultkey='',disableAllCarryFwd=True))

    cbsloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                       feedPattern="^.*\cbs.xls",
                                                       feedParams=dict(skiprows=1,
                                                                   feedformatFile='ACC_CBS.csv'),
                                                       feedFilters=[
                                                           "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                     "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                                                           "df['Deposit Amount']=df['Deposit Amount'].fillna(0)",
                                                       "df['Installment Amount']=df['Installment Amount'].fillna(0)",
                                                    "df['Nominee Phone Ext']=df['Nominee Phone Ext'].replace('91','')",
                                                   "df['Nominee Phone Ext']=df['Nominee Phone Ext'].fillna('')",
                                           "df['Nominee Phone Area Code']=df['Nominee Phone Area Code'].replace('91','')",
                                           "df['Nominee Phone Area Code']=df['Nominee Phone Area Code'].fillna('')",
                                                           "df=df.apply(lambda x : x.astype(str).str.lower() if (isinstance(x, str) or isinstance(x, object)) and not (np.issubdtype(x.dtype, np.datetime64)) else x)"],
                                                       resultkey="cbsdf"))

    wizardloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Wizard",
                                                          feedPattern="^.*\wizard.xls",
                                                          feedParams=dict(skiprows=1,
                                                                   feedformatFile='ACC_Wizard.csv'),
                                                          feedFilters=[
                                                              "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                     "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                                                              "df['Deposit Amount']=df['Deposit Amount'].fillna(0)",
                                                    "df['Nominee Phone Ext']=df['Nominee Phone Ext'].replace('91','')",
                                                    "df['Nominee Phone Ext']=df['Nominee Phone Ext'].fillna('')",
                                              "df['Nominee Phone Area Code']=df['Nominee Phone Area Code'].replace('91','')",
                                              "df['Nominee Phone Area Code']=df['Nominee Phone Area Code'].fillna('')",
                                                              "df['Installment Amount']=df['Installment Amount'].fillna(0)",
                           "df=df.apply(lambda x : x.astype(str).str.lower() if (isinstance(x, str) or isinstance(x, object)) and not (np.issubdtype(x.dtype, np.datetime64)) else x)"],
                                                          resultkey="wizarddf"))

    crmloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CRM",
                                                       feedPattern="^.*\crm.xls",
                                                       feedParams=dict(skiprows=1,
                                                                   feedformatFile='ACC_CRM.csv'),errors =False,
                                                       feedFilters=[
                                                           "df.columns = [col.strip() for col in df.columns.tolist()]",
                                                           "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                                                           "df['Deposit Amount']=df['Deposit Amount'].fillna(0)",
                                                           "df['Installment Amount']=df['Installment Amount'].fillna(0)",
                                                           "df=df.apply(lambda x : x.astype(str).str.lower() if (isinstance(x, str) or isinstance(x, object)) and not (np.issubdtype(x.dtype, np.datetime64)) else x)"],
                                                       resultkey="crmdf"))

    concat=dict(type="SourceConcat",properties=dict(sourceList=['cbsdf','crmdf'], resultKey="cbscrm"))
    exp1=dict(type="ExpressionEvaluator", properties=dict(source="cbscrm",
                     expressions=["df=df.sort_values(by=['Account Number'])"]
                                               , resultkey='CBSvsCRM'))
    updatecols=dict(type='UpdateColumns',
                  properties=dict(source='CBSvsCRM',columns=crmcolumns,
                                  KeyColumns=['Account Number']
                                               ,resultkey='final'))
    exp2=dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='final',
                                                filterConditions=[
                                    "df.loc[(df['SOURCE']=='cbs')&(df['Matched']=='Missing'),'Matched']='Missing in CRM'",
                                    "df.loc[(df['SOURCE']=='crm')&(df['Matched']=='Missing'),'Matched']='Missing in CBS'",
                                    "df.loc[(df['SOURCE']=='cbs')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CRM'",
                                    "df.loc[(df['SOURCE']=='crm')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CBS'"
                                                ],
                                                sourceTag="CBSvsCRM")], resultKey='cbsvscrm',
                                  writeToFile=True,
                                  reportName='CBSvsCRM'))
    concat1=dict(type="SourceConcat",properties=dict(sourceList=['cbsdf','wizarddf'], resultKey="cbswizard"))

    exp3 = dict(type="ExpressionEvaluator", properties=dict(source="cbswizard",
                                                            expressions=["df=df.sort_values(by=['Account Number'])"]
                                                            , resultkey='CBSvsWIZARD'))
    updatecols1 = dict(type='UpdateColumns',
                      properties=dict(source='CBSvsWIZARD', columns=crmcolumns,
                                      KeyColumns=['Account Number']
                                      , resultkey='final1'))
    exp4= dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='final1',
                                              filterConditions=[
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['Matched']=='Missing'),'Matched']='Missing in WIZARD'",
                                                  "df.loc[(df['SOURCE']=='wizard')&(df['Matched']=='Missing'),'Matched']='Missing in CBS'",
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in WIZARD'",
                                                  "df.loc[(df['SOURCE']=='wizard')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CBS'",
                                                  "df.loc[df['Nominee Address Line1']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee Address Line1,','')",
                                                  "df.loc[df['Nominee Address Line2']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee Address Line2,','')",
                                                  "df.loc[df['Nominee Address Line3']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee Address Line3,','')",
                                                  "df.loc[df['Nominee Phone Country Code']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee Phone Country Code,','')",
                                                  "df.loc[df['Nominee Pin code']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee Pin code,','')",
                                                  "df.loc[df['Nominee City']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee City,','')",
                                                  "df.loc[df['Nominee State']=='same as customer address','UnMatched']=df['UnMatched'].str.replace('Nominee State,','')",
                                                  "df.loc[df['Nominee Address Line1']=='same as customer address','Matched']=df['Matched']+',Nominee Address Line1'",
                                                  "df.loc[df['Nominee Address Line2']=='same as customer address','Matched']=df['Matched']+',Nominee Address Line2'",
                                                  "df.loc[df['Nominee Address Line3']=='same as customer address','Matched']=df['Matched']+',Nominee Address Line3'",
                                                  "df.loc[df['Nominee Phone Country Code']=='same as customer address','Matched']=df['Matched']+',Nominee Phone Country Code'",
                                                  "df.loc[df['Nominee Pin code']=='same as customer address','Matched']=df['Matched']+',Nominee Pin code'",
                                                  "df.loc[df['Nominee City']=='same as customer address','Matched']=df['Matched']+',Nominee City'",
                                                  "df.loc[df['Nominee State']=='same as customer address','Matched']=df['Matched']+',Nominee State'",

                                              ],
                                              sourceTag="CBSvsCRM")], resultKey='cbsvswizard',
                                writeToFile=True,
                                reportName='CBSvsWIZARD'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='AccountOpening', matched='any'))

    elements = [elem1, cbsloader, wizardloader, crmloader, concat,exp1,updatecols,exp2,concat1,exp3,updatecols1,exp4,gen_meta_info,elem19]
                # matcher, CBS, CRM, WIZARD, gen_meta_info, ]

    f = FlowRunner(elements)

    f.run()
