import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas

if __name__ == "__main__":
    stmtdate ='06-Aug-2019'
    uploadFileName ='CUST_ONBOARDING_RPT_WIZARD.zip'

    df = pandas.read_csv(config.basepath + "Reference/CustomerOnboardingMatchingColumns.csv")
    crmcolumns = df["CRM"].tolist()
    wizardcolumns = df["Wizard"].tolist()
    cbscolumns = df["CBS"].tolist()

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",uploadFileName=uploadFileName,
                                 reconName="CustomerOnboarding",recontype='CustomerDetails', compressionType="zip", resultkey=''))

    cbsloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CBS",
                                                                 feedPattern="^.*\cbs.xls",
                                                                 feedParams=dict(skiprows=1,
                                                                   feedformatFile='CUST_CBS.csv'),
                                                                 feedFilters=["df['Current IC No']=df['Current IC No'].astype(str).str.replace('\.(0)*','')",
                                                                              "df['Mobile Nummber']=df['Mobile Nummber'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['Postal Code']=df['Postal Code'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                               "df['Fax']=df['Fax'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                               "df['Aadhaar Number']=df['Aadhaar Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                               "df['Registered Number']=df['Registered Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                               "df['Alternate Mail']=df['Alternate Mail'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('')",
                                                                              "df['Phone(Res)']=df['Phone(Res)'].replace('nan','')",
                                                                              "df['Phone(Res)']=df['Phone(Res)'].str.replace('91','')",
                                                                              "df['Phone(Res)']=df['Phone(Res)'].apply(lambda x:x[-10:])",
                                                                              "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['Employee ID']=df['Employee ID'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['AOR']=df['AOR'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['Phone(office)']=df['Phone(office)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              "df['Marital Status']=df['Marital Status'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                              # "df['Phone(Res)']=df[df['Phone(Res)']=='91']['Phone(Res)'].str.replace('91','')",
                                                                              "df=df.apply(lambda x : x.astype(str).str.lower() if (isinstance(x, str) or isinstance(x, object)) and not (np.issubdtype(x.dtype, np.datetime64))  else x)",
                                                                              "df['Postal Code']=df['Postal Code'].astype(str).fillna('').str.replace('\.(0)*','')"
                                                                              ],
                                                                 resultkey="cbsdf"))

    wizardloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Wizard",
                                                       feedPattern="^.*\wizard.xls",
                                                       feedParams=dict(skiprows=1,
                                                                   feedformatFile='CUST_Wizard.csv'),
                                                       feedFilters=["df['Current IC No']=df['Current IC No'].astype(str).str.replace('\.(0)*','')",
                                                                    "df['Mobile Nummber']=df['Mobile Nummber'].astype(str).fillna('').str.replace('\.(0)*','')",

                                                                    "df['Fax']=df['Fax'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Aadhaar Number']=df['Aadhaar Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Registered Number']=df['Registered Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Alternate Mail']=df['Alternate Mail'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('')",
                                                                    "df['Phone(Res)']=df['Phone(Res)'].replace('nan','')",
                                                                    "df['Phone(Res)']=df['Phone(Res)'].str.replace('91','')",
                                                                    "df['Phone(Res)']=df['Phone(Res)'].apply(lambda x:x[-10:])",
                                                                    "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Employee ID']=df['Employee ID'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['AOR']=df['AOR'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Phone(office)']=df['Phone(office)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df['Marital Status']=df['Marital Status'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                                    "df=df.apply(lambda x : x.astype(str).str.lower() if (isinstance(x, str) or isinstance(x, object)) and not (np.issubdtype(x.dtype, np.datetime64))  else x)",
                                                                  "df['Postal Code']=df['Postal Code'].astype(str).fillna('').str.replace('\.(0)*','')"],
                                                       resultkey="wizarddf"))


    crmloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CRM",
                                                          feedPattern="^.*\crm.xls",disableCarryFwd=True,
                                                          feedParams=dict(skiprows=1,
                                                                   feedformatFile='CUST_CRM.csv'),errors = False,
                                                          feedFilters=["df['Current IC No']=df['Current IC No'].astype(str).str.replace('\.(0)*','')",
                                                                       "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('')",
                                                                       "df['Phone(Res)']=df['Phone(Res)'].replace('nan','')",
                                                                       "df['Phone(Res)']=df['Phone(Res)'].str.replace('91','')",
                                                                       "df['Phone(Res)']=df['Phone(Res)'].apply(lambda x:x[-10:])",
                                                                       "df=df.apply(lambda x : x.astype(str).str.lower() if isinstance(x, str) or isinstance(x, object) else x)",
                                                                       "df.loc[df['Current IC No'].fillna('')=='','Remarks']='Customer already existed'"],
                                                          resultkey="crmdf"))


    concat = dict(type="SourceConcat", properties=dict(sourceList=['cbsdf', 'crmdf'], resultKey="cbscrm"))
    exp1 = dict(type="ExpressionEvaluator", properties=dict(source="cbscrm",
                                                            expressions=["df=df.sort_values(by=['Customer ID'])"]
                                                            , resultkey='CBSvsCRM'))
    updatecols = dict(type='UpdateColumns',
                      properties=dict(source='CBSvsCRM', columns=crmcolumns,
                                      KeyColumns=['Customer ID']
                                      , resultkey='final'))
    exp2 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='final',
                                              filterConditions=[
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['Matched']=='Missing'),'Matched']='Missing in CRM'",
                                                  "df.loc[(df['SOURCE']=='crm')&(df['Matched']=='Missing'),'Matched']='Missing in CBS'",
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CRM'",
                                                  "df.loc[(df['SOURCE']=='crm')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CBS'",

                                              ],
                                              sourceTag="CBSvsCRM")], resultKey='cbsvscrm',
                                writeToFile=True,
                                reportName='CBSvsCRM'))

    concat1 = dict(type="SourceConcat", properties=dict(sourceList=['cbsdf', 'wizarddf'], resultKey="cbswizard"))

    exp3 = dict(type="ExpressionEvaluator", properties=dict(source="cbswizard",
                                                            expressions=["df=df.sort_values(by=['Customer ID'])"]
                                                            , resultkey='CBSvsWIZARD'))
    updatecols1 = dict(type='UpdateColumns',
                       properties=dict(source='CBSvsWIZARD', columns=crmcolumns,
                                       KeyColumns=['Customer ID']
                                       , resultkey='final1'))
    exp4 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='final1',
                                              filterConditions=[
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['Matched']=='Missing'),'Matched']='Missing in WIZARD'",
                                                  "df.loc[(df['SOURCE']=='wizard')&(df['Matched']=='Missing'),'Matched']='Missing in CBS'",
                                                  "df.loc[(df['SOURCE']=='cbs')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in WIZARD'",
                                                  "df.loc[(df['SOURCE']=='wizard')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CBS'",

                                              ],
                                              sourceTag="CBSvsWIZARD")], resultKey='cbsvswizard',
                                writeToFile=True,
                                reportName='CBSvsWIZARD'))

    exp5 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='custom_reports.CBSvsWIZARD',
                                              filterConditions=[
                                                  "df=df[df['UnMatched']=='Missing in CBS']",
                                                  "df=df.fillna('')",
                                                  "df=df[['STATEMENT_DATE','SYS_TXN_ID','SOURCE','Current IC No', 'Customer ID', 'Cust open Dt', 'Customer Category', 'Prefix', 'First Name', 'Middle Name', 'Last Name', 'Company Name', 'Short Name', 'Mobile Nummber', 'Phone(Res)', 'Phone(office)', 'Fax', 'Aadhaar Number', 'PAN', 'FORM 60 NO', 'Registered Number', 'Date of Birth', 'Date Registered', 'Business Type', 'Nationality', 'Language', 'Home Branch', 'Gender', 'Email', 'Alternate Mail', 'Mother Name', 'Marital Status', 'Staff Checkbox', 'Employee ID', 'Mailing Address1', 'Mailing Address2', 'Mailing Address3', 'Postal_Code', 'City', 'State', 'Country', 'Permanent Address1', 'Permanent Address2', 'Permanent Address3', 'Postal Code', 'PERMCITY', 'PERMSTATE', 'PERMCOUNTRY', 'AOR','Matched','UnMatched']]",
                                                  "df['Cust open Dt'] = df['Cust open Dt'].apply(lambda x: x if x != '' else pd.NaT)",
                                                  "df['Date of Birth'] = df['Date of Birth'].apply(lambda x: x if x != '' else pd.NaT)"
                                                 ],
                                              sourceTag="CBSvsWIZARD")], resultKey='MissinginCBS',
                                writeToFile=True,
                                reportName='Wizard_UnMatched'))

    exp6 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='custom_reports.CBSvsWIZARD',
                                              filterConditions=[
                                                  "df = df[df['UnMatched'] == 'Missing in WIZARD']",
                                                  "df=df.fillna('')",
                                                  "df=df[['STATEMENT_DATE','SYS_TXN_ID','SOURCE','Current IC No', 'Customer ID', 'Cust open Dt', 'Customer Category', 'Prefix', 'First Name', 'Middle Name', 'Last Name', 'Company Name', 'Short Name', 'Mobile Nummber', 'Phone(Res)', 'Phone(office)', 'Fax', 'Aadhaar Number', 'PAN', 'FORM 60 NO', 'Registered Number', 'Date of Birth', 'Date Registered', 'Business Type', 'Nationality', 'Language', 'Home Branch', 'Gender', 'Email', 'Alternate Mail', 'Mother Name', 'Marital Status', 'Staff Checkbox', 'Employee ID', 'Mailing Address1', 'Mailing Address2', 'Mailing Address3', 'Postal_Code', 'City', 'State', 'Country', 'Permanent Address1', 'Permanent Address2', 'Permanent Address3', 'Postal Code', 'PERMCITY', 'PERMSTATE', 'PERMCOUNTRY', 'AOR','Matched','UnMatched']]",
                                                  "df['Cust open Dt'] = df['Cust open Dt'].apply(lambda x: x if x != '' else pd.NaT)",
                                                  "df['Date of Birth'] = df['Date of Birth'].apply(lambda x: x if x != '' else pd.NaT)"
                                               ],
                                              sourceTag="CBSvsWIZARD")], resultKey='MissinginWizard',
                                writeToFile=True,
                                reportName='CBS_UnMatched'))

    exp7= dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='custom_reports.CBSvsWIZARD',
                                              filterConditions=[
                                                  "df=df[(df['CARRY_FORWARD']=='Y')&(df['UnMatched'].isin(['Missing in WIZARD','Missing in CBS']))]",
                                                      "df['Current IC No']=df['Current IC No'].astype(str).str.replace('\.(0)*','')",
                                                      "df['Mobile Nummber']=df['Mobile Nummber'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Customer ID']=df['Customer ID'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Fax']=df['Fax'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Aadhaar Number']=df['Aadhaar Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Registered Number']=df['Registered Number'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Alternate Mail']=df['Alternate Mail'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('')",
                                                      "df['Phone(Res)']=df['Phone(Res)'].replace('nan','')",
                                                      "df['Phone(Res)']=df['Phone(Res)'].str.replace('91','')",
                                                      "df['Phone(Res)']=df['Phone(Res)'].apply(lambda x:x[-10:])",
                                                      "df['Phone(Res)']=df['Phone(Res)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Employee ID']=df['Employee ID'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['AOR']=df['AOR'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Phone(office)']=df['Phone(office)'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Marital Status']=df['Marital Status'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Postal Code']=df['Postal Code'].astype(str).fillna('').str.replace('\.(0)*','')",
                                                      "df['Postal_Code']=df['Postal_Code'].astype(str).fillna('').str.replace('\.(0)*','')"
                                              ],
                                              sourceTag="CBSvsWIZARD")], resultKey='CarryForward',
                                writeToFile=True,
                                reportName='CarryForward'))

    #
    gen_meta_info = dict(type='GenerateReconMetaInfo')
    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='CustomerOnboarding', matched='any'))
    elements = [elem1, cbsloader, wizardloader,concat1,exp3,updatecols1,exp4,exp5,exp6,exp7,gen_meta_info,elem19]
                # matcher,CBS,CRM,WIZARD,gen_meta_info,
    f = FlowRunner(elements)
    f.run()

    # f.result['cbsdf'].to_csv("Output/cbsdf.csv", index=False)
    # f.result['wizarddf'].to_csv("Output/wizarddf.csv", index=False)
    # for x in f.result['results'].keys():
    #     if type(f.result['results'][x])!=pandas.DataFrame:
    #         print("Key has invalid data"+ x)
    #         continue
    #     f.result['results'][x].to_csv(f.result['output_path']+"/"+ x + ".csv")

