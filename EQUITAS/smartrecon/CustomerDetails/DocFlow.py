import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas
if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    df = pandas.read_csv("/usr/share/nginx/smartrecon/Reference/DocFlow.csv").fillna('')
    crmcolumns = df["CRM"].tolist()
    wizardcolumns = df["Wizard"].tolist()
    dmscolumns = df["DMS"].tolist()
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 reconName="DocFlow", recontype='CustomerDetails', compressionType="zip",disableAllCarryFwd=True,
                                 resultkey=''))

    DMSloader = dict(type="PreLoader", properties=dict(loadType="CSV", source="DMS",
                                                       feedPattern="^.*dms.csv",
                                                       feedParams=dict(delimiter=",", skiprows=1,
                                                                   feedformatFile='DMS_Structure.csv'),
                                                       feedFilters=[
                                                           "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                                                           "df=df.apply(lambda x : x.astype(str).str.lower() if isinstance(x, str) or isinstance(x, object) else x)"],
                                                       resultkey="dmsdf"))
    wizardloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Wizard",
                                                          feedPattern="^.*\wizard.xls",
                                                          feedParams=dict(skiprows=1,
                                                                   feedformatFile='DOC_Wizard.csv'),
                                                          feedFilters=[
                                                              "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                                                              "df=df.apply(lambda x : x.astype(str).str.lower() if isinstance(x, str) or isinstance(x, object) else x)"],
                                                          resultkey="wizarddf"))
    crmloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CRM",
                               feedPattern="^.*\crm.xls",
                               feedParams=dict(skiprows=1,feedformatFile='DOC_CRM.csv'),errors = False,
                               feedFilters=[
                               "df['Customer ID']=df['Customer ID'].astype(str).str.replace('\.(0)*','')",
                               "df=df.apply(lambda x : x.astype(str).str.lower() if isinstance(x, str) or isinstance(x, object) else x)",
                               "df['KYC Documents- Type']=df['KYCDOC1_TYPE']+','+df['KYCDOC2_TYPE']+','+df['KYCDOC3_TYPE']",
                               "df['KYC Documents- Name']=df['KYCDOC1_NAME']+','+df['KYCDOC2_NAME']+','+df['KYCDOC3_NAME']",
                               "df['KYC Documents - Document No']=df['KYCDOC1_NO']+','+df['KYCDOC2_NO']+','+df['KYCDOC3_NO']",
                               "df=df[['Lead Number','Customer ID','Account Number','Customer Branch','KYC Documents- Type','KYC Documents- Name','KYC Documents - Document No','SOURCE']]"],
                                 resultkey="crmdf"))

    concat = dict(type="SourceConcat", properties=dict(sourceList=['dmsdf', 'crmdf'], resultKey="dmscrm"))



    # exp1 = dict(type="ExpressionEvaluator", properties=dict(source="dmscrm",
    #                                                         expressions=["df=df.sort_values(by=['Customer ID'])"]
    #                                                         , resultkey='DMSvsCRM'))
    # updatecols = dict(type='UpdateColumns',
    #                   properties=dict(source='DMSvsCRM', columns=crmcolumns,
    #                                   KeyColumns=['Customer ID']
    #                                   , resultkey='final'))
    exp2 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='dmscrm',
                                              filterConditions=[
                      "df=df.groupby(['Lead Number','SOURCE','Customer ID','Account Number'])['KYC Documents- Type'].apply(lambda x : ','.join(x)).reset_index()",
                      "df=df.sort_values(by=['Customer ID'])"
                                                  # "df.loc[(df['SOURCE']=='dms')&(df['Matched']=='Missing'),'Matched']='Missing in CRM'",
                                                  # "df.loc[(df['SOURCE']=='crm')&(df['Matched']=='Missing'),'Matched']='Missing in DMS'",
                                                  # "df.loc[(df['SOURCE']=='dms')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in CRM'",
                                                  # "df.loc[(df['SOURCE']=='crm')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in DMS'"
                                              ],
                                              sourceTag="DMSvsCRM")], resultKey='dmsvscrm',
                                writeToFile=True,
                                reportName='DMSvsCRM'))
    concat1 = dict(type="SourceConcat", properties=dict(sourceList=['dmsdf', 'wizarddf'], resultKey="dmswizard"))
    #
    # exp3 = dict(type="ExpressionEvaluator", properties=dict(source="dmswizard",
    #                                                         expressions=[
    #                                                             # "df=df.sort_values(by=['KYC Documents - Document No'])",
    #                  "df=df.groupby(['Lead Number','SOURCE','Customer ID','Account Number'])['KYC Documents- Type'].apply(lambda x : '|'.join(x)).reset_index()"]
    #                                                         , resultkey='DMSvsWIZARD'))
    # updatecols1 = dict(type='UpdateColumns',
    #                    properties=dict(source='DMSvsWIZARD', columns=crmcolumns,
    #                                    KeyColumns=['Customer ID']
    #                                    , resultkey='final1'))
    exp4 = dict(type='ReportGenerator',
                properties=dict(sources=[dict(source='dmswizard',
                                              filterConditions=[
                "df=df.groupby(['Lead Number','SOURCE','Customer ID','Account Number'])['KYC Documents- Type'].apply(lambda x : ','.join(x)).reset_index()",
                                                  "df=df.sort_values(by=['Customer ID'])"

                                                  # "df.loc[(df['SOURCE']=='dms')&(df['Matched']=='Missing'),'Matched']='Missing in WIZARD'",
                                                  # "df.loc[(df['SOURCE']=='wizard')&(df['Matched']=='Missing'),'Matched']='Missing in DMS'",
                                                  # "df.loc[(df['SOURCE']=='dms')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in WIZARD'",
                                                  # "df.loc[(df['SOURCE']=='wizard')&(df['UnMatched']=='Missing'),'UnMatched']='Missing in DMS'"
                                              ],
                                              sourceTag="DMSvsWizard")], resultKey='dmsvswizard',
                                writeToFile=True,
                                reportName='DMSvsWIZARD'))

    gen_meta_info = dict(type='GenerateReconMetaInfo')
    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='DocFlow', matched='any'))
    elements = [elem1, DMSloader, wizardloader, crmloader,concat,exp2,concat1,exp4,gen_meta_info,elem19]
                # matcher, DMS, CRM, WIZARD, gen_meta_info, elem19]
    f = FlowRunner(elements)
    f.run()