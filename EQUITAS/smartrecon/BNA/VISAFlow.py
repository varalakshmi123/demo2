import sys
sys.path.append('/usr/share/nginx/bnarecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    stmtdate = "01-Dec-2017"
    basePath = "/usr/share/nginx/bnarecon/mft/"
    recontype = "VISA"
    reconName = 'VISARecon'
cbs_filters = ["df['Card Number'] = df['Card Number'].str.rstrip(' ')",
               "df['RR Number'] = df['RR Number'].str.rstrip(' ').str.lstrip(' ')",
               "df['A/C Number'] = df['A/C Number'].str.lstrip(' ')",
               "df['BNA_ATM_ID'] = df['BNA_ATM_ID'].str.lstrip(' ')","df=df[(df['Term_ID'].isin(['VISA']))]"
               ]

switch_filters = ["df.loc[df[(df['RRNO'] == 'null') | (df['RRNO'].isnull()) | (df['RRNO'].astype(str) == '000000000000')].index,'RRNO'] = df['SEQ_NUM']",
                  "df['RRNO'] = df['RRNO'].astype(str).str.rstrip(' ').str.lstrip(' ')",
                  "df = df[(df['TRAN_CODE'].str.startswith('10')) | (df['TRAN_CODE'].str.contains('W1',case=False))]",
                  "df = df[(df['RESP_CODE'].isin(['000','001'])) & (df['TERM_FIID'] == '5997')& (df['CARD_FIID']).isin(['VISA'])]",
                  "df['INDICATOR']=''",
                  "df['INDICATOR'] = df['TRAN_CODE'].str.slice(0,2).map({'10':'D','w1':'C','W1':'C'})",
                  "df['STATUS']='CASH_ WITHDRAWL'",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('10'))),['INDICATOR','STATUS']]=['C','CASH_WITHDRAWL_REV']",
                  "df.loc[((df['REVERSAL_REASON']=='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['C','CASH_DEPOSIT']",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['C','CASH_DEPOSIT_REV']"
                  ]

acq_fil=["df['Transaction Amount']=df['Transaction Amount']/100"]


elem1 = dict(id=1, next=2, type="Initializer",
             properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                             recontype="BNA", compressionType="zip", resultkey='', reconName='VISARecon'))



elem2 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="VISA",
                                                     feedPattern="TT464T0.2017-12-04-11-59-05.001",
                                                     feedParams=dict(feedformatFile="VisaFixedFormatTCR00.csv", skipTopRows=1,
                                                                     skipBottomRows=1),
                                                     resultkey="visadf"))

# elem2 = dict(id=2, next=3,type="PreLoader", properties=dict(loadType="Excel", source="Acquirer",
#                                                              feedPattern="VISA_ATM_ACQ_TXN_FILE.xls",
#                                                              feedParams=dict(feedformatFile="Visa.csv", skiprows=1),resultkey="visadf"))
reference = dict(type="ReferenceLoader", properties=dict(loadType="CSV",
                                                         fileName=[
                                                             "Reference/bna_master_dec17.csv"],
                                                         resultkey="refdf"))


elem3=dict(id=3,next=4,type="ExpressionEvaluator",properties=dict(source='visadf',resultkey='visadf',expressions=["df=df[df['TERM_ID'].isin(payload['refdf']['ATM_ID'].unique())]"]))





# elem4 = dict(id=4, next=5, type="PreLoader", properties=dict(loadType="Excel", source="SWITCH",
#                                                              feedPattern="TLFFILE#%d%m%Y#.xls",
#                                                              feedParams=dict(
#                                                                  feedformatFile='SWITCH.csv',skiprows=1),
#                                                              feedFilters=switch_filters,
#                                                              resultkey="switchdf"))
#
# elem5=dict(id=5,next=6,type='FilterDuplicates',properties=dict(source='switchdf',sourceTag='SWITCH',duplicateStore='duplicateStore',allowOneInMatch=True,keyColumns=["CARD_NUM", "ATM_ID","AMOUNT","RRNO"]))
#
# elem6 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
#                                                      feedPattern="BNAHST#%d%m%Y#.txt",
#                                                      feedParams=dict(feedformatFile="CBS.csv", skipTopRows=1,
#                                                                      skipBottomRows=1),
#                                                      feedFilters=cbs_filters,
#                                                      resultkey="cbsdf"))
#
#
# elem7=dict(id=7,next=8,type='FilterDuplicates',properties=dict(source='cbsdf',sourceTag='CBS',duplicateStore='duplicateStore',allowOneInMatch=True,keyColumns=["Card Number", "BNA_ATM_ID","Amount","RR Number"]))
#
#
#
# elem8 = dict(id=8,next=9,type="NWayMatch",
#              properties=dict(sources=[dict(source="switchdf",
#                                            columns=dict(
#                                                keyColumns=["CARD_NUM", "ATM_ID","AMOUNT"],
#                                                sumColumns=[],
#                                                matchColumns=["CARD_NUM", "ATM_ID","AMOUNT"]),
#                                            sourceTag="SWITCH"),
#                                       dict(source="cbsdf", columns=dict(
#                                           keyColumns=["Card Number", "BNA_ATM_ID","Amount"], sumColumns=[],
#                                           matchColumns=[ "Card Number", "BNA_ATM_ID","Amount"]), sourceTag="CBS"),  dict(source="visadf", columns=dict( keyColumns=["CARDNO", "TERM_ID","TRANAMT"], sumColumns=[], matchColumns=["CARDNO", "TERM_ID","TRANAMT"]), sourceTag="Acquirer")], matchResult="results"))
#
#
# elem9=dict(id=9,type='DumpData',properties=dict(dumpPath='VISARecon'))
#

# elements = [elem1,elem2,reference,elem3,elem4,elem5,elem6,elem7,elem8,elem9]
elements = [elem1,elem2]

f = FlowRunner(elements)
f.run()