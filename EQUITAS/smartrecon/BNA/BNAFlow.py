import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    basePath = "/usr/share/nginx/smartrecon/mft/"
    recontype = "BNA"
    reconName = 'BNARecon'

    cbs_filters = ["df['Card Number'] = df['Card Number'].str.rstrip(' ')",
                      "df['RR Number'] = df['RR Number'].str.rstrip(' ').str.lstrip(' ')",
                      "df['A/C Number'] = df['A/C Number'].str.lstrip(' ')",
                      "df['BNA_ATM_ID'] = df['BNA_ATM_ID'].str.lstrip(' ')","df=df[(df['Term_ID'].isin(['PRO1']))]"
                   ]

    switch_filters=["df.loc[df[(df['RRNO'] == 'null') | (df['RRNO'].isnull()) | (df['RRNO'].astype(str) == '000000000000')].index,'RRNO'] = df['SEQ_NUM']",
                    "df['RRNO'] = df['RRNO'].astype(str).str.rstrip(' ').str.lstrip(' ')",
                    "df = df[(df['TRAN_CODE'].str.startswith('10')) | (df['TRAN_CODE'].str.contains('W1',case=False))]",
                    "df = df[(df['RESP_CODE'].isin(['000','001'])) & (df['TERM_FIID'] == '5997')& (df['CARD_FIID']).isin(['6995','6997'])]",
                    "df['INDICATOR']=''",
                    "df['INDICATOR'] = df['TRAN_CODE'].str.slice(0,2).map({'10':'D','w1':'C','W1':'C'})",
                    "df['STATUS']='CASH_ WITHDRAWL'",
                   "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('10'))),['INDICATOR','STATUS']]=['C','CASH_WITHDRAWL_REV']",
                   "df.loc[((df['REVERSAL_REASON']=='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['C','CASH_DEPOSIT']",
                   "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['D','CASH_DEPOSIT_REV']"
                   ]


    # #
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="BNA",uploadFileName=uploadFileName,compressionType="zip", resultkey='',reconName='BNAONUS'))
    # #
    # # elem2 = dict(id=2,next=3,type="PreScript", properties=dict(loadType="Script",feedPattern="TLFFILE#%d%m%Y#.xlsx"))
    # #
    # #
    elem2 = dict(id=2, next=3,type="PreLoader", properties=dict(loadType="Excel", source="SWITCH",
                                                         feedPattern="TLFFILE#%d%m%Y#.xls",
                                                         feedParams=dict(
                                                             feedformatFile='SWITCH.csv',skiprows=1),feedFilters=switch_filters,
                                                         resultkey="switchdf"))

    # elem3=dict(id=3,next=4,type="ExpressionEvaluator",properties=dict(data='switchdf',resultkey='switchdf'))



    elem3 = dict(id=5, next=6, type='FilterDuplicates',
                 properties=dict(source='switchdf', sourceTag='SWITCH', duplicateStore='duplicateStore',
                                 resultKey="switchdf_unq", allowOneInMatch=True,
                                 keyColumns=["CARD_NUM", "ATM_ID", "AMOUNT", "RRNO", "INDICATOR"]))

    elem4 = dict(id=3,type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                 feedPattern="BNAHST#%d%m%Y#.txt",
                                                                 raiseError=False,
                                                                 feedParams=dict(feedformatFile="CBS.csv",skipTopRows= 1,skipBottomRows=1),
                                                                 feedFilters=cbs_filters,
                                                                 resultkey="cbsdf"))

    elem5 = dict(id=7, next=8, type='FilterDuplicates',
                 properties=dict(source='cbsdf', sourceTag='CBS', duplicateStore='duplicateStore',
                                 resultKey="cbsdf_unq", allowOneInMatch=True,
                                 keyColumns=["Card Number", "BNA_ATM_ID", "Amount", "RR Number", "Dr/CR Indicator"]))

    # elem6 = dict(type="NWayMatch",
    #              properties=dict(sources=[dict(source="switchdf_unq",
    #                                            columns=dict(keyColumns=["CARD_NUM", "ATM_ID", "RRNO"],
    #                                                         amountColumns=["AMOUNT"],
    #                                                         matchColumns=["CARD_NUM", "ATM_ID", "RRNO", "AMOUNT"],
    #                                                         crdrcolumn=['INDICATOR'],
    #                                                         CreditDebitSign=True), sourceTag="SWITCH")],
    #                              matchResult="reversal"))
    #
    #
    # elem7 = dict(id=6, next=7, type="NWayMatch",
    #               properties=dict(sources=[dict(source="cbsdf_unq",
    #                                             columns=dict(keyColumns=["Card Number", "BNA_ATM_ID", "RR Number"],
    #                                                          amountColumns=['Amount'],
    #                                                          matchColumns=["Card Number", "BNA_ATM_ID", "RR Number",
    #                                                                        'Amount'], crdrcolumn=['Dr/CR Indicator'],
    #                                                          CreditDebitSign=True), sourceTag="CBS")],
    #                               matchResult="reversal"))

    elem8=dict(type="NWayMatch",
         properties=dict(sources=[dict(source="switchdf_unq",
         columns=dict(keyColumns=["CARD_NUM", "ATM_ID", "RRNO", "TRAN_DATE", "INDICATOR"],sumColumns=['AMOUNT'],
         matchColumns=["CARD_NUM", "ATM_ID", "RRNO", "TRAN_DATE","INDICATOR",'AMOUNT']),sourceTag="SWITCH"),

         dict(source="cbsdf_unq", columns=dict(keyColumns=["Card Number", "BNA_ATM_ID", "RR Number", "Tran Date","Dr/CR Indicator"],sumColumns=['Amount'],
         matchColumns=["Card Number", "BNA_ATM_ID", "RR Number", "Tran Date","Dr/CR Indicator",'Amount']),sourceTag="CBS")], matchResult = "results"))



    summary = dict(type='GenerateReconSummary',
                  properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                           dict(resultKey="SWITCH", sourceTag='SWITCH',
                                                aggrCol=['AMOUNT'])],groupbyCol=['STATEMENT_DATE']))

    elem9=dict(id=9,type='DumpData',properties=dict(dumpPath='BNAONUS'))

    meta = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [elem1,elem2,elem3,elem4,elem5,elem8,summary,elem9,meta]

    f = FlowRunner(elements)
    f.run()
