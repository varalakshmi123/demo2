import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    # stmtdate = sys.argv[1]
    # uploadFileName = sys.argv[2]
    stmtdate='01-dec-2017'
    basePath = "/usr/share/nginx/smartrecon/mft/"
    recontype = "CCD"
    reconName = 'CCDRecon'
cbs_filters = ["df['Card Number'] = df['Card Number'].str.rstrip(' ')",
               "df['RR Number'] = df['RR Number'].str.rstrip(' ').str.lstrip(' ')",
               "df['A/C Number'] = df['A/C Number'].str.lstrip(' ')",
               "df['BNA_ATM_ID'] = df['BNA_ATM_ID'].str.lstrip(' ')","df=df[(df['Term_ID'].isin(['CCD']))]"
               ]

switch_filters = ["df.loc[df[(df['RRNO'] == 'null') | (df['RRNO'].isnull()) | (df['RRNO'].astype(str) == '000000000000')].index,'RRNO'] = df['SEQ_NUM']",
                  "df['RRNO'] = df['RRNO'].astype(str).str.rstrip(' ').str.lstrip(' ')",
                  "df = df[(df['TRAN_CODE'].str.startswith('10')) | (df['TRAN_CODE'].str.contains('W1',case=False))]",
                  "df = df[(df['RESP_CODE'].isin(['000','001'])) & (df['TERM_FIID'] == '5997')& (df['CARD_FIID']).isin(['CCD'])]",
                  "df['INDICATOR']=''",
                  "df['INDICATOR'] = df['TRAN_CODE'].str.slice(0,2).map({'10':'D','w1':'C','W1':'C'})",
                  "df['STATUS']='CASH_WITHDRAWL'",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('10'))),['INDICATOR','STATUS']]=['C','CASH_WITHDRAWL_REV']",
                  "df.loc[((df['REVERSAL_REASON']=='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['C','CASH_DEPOSIT']",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['D','CASH_DEPOSIT_REV']"
                  ]
# acq_fil=["df['Transaction Amount']=df['Transaction Amount']/100"]

ccd_fil=["df['RRN_NO']=df['RRN_NO'].str.strip()"]


elem1 = dict(id=1, next=2, type="Initializer",
             properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                             recontype="BNA",compressionType="zip", resultkey='', reconName='CCDRecon'))





elem2 = dict(id=2, next=3,type="PreLoader", properties=dict(loadType="Excel", source="CCD",
                                                             feedPattern="AB_CRCD_ATM_ACQUIRING*.*",
                                                                feedFilters=ccd_fil,
                                                             feedParams=dict(feedformatFile="CCD.csv", skiprows=1,
                                                                             ),resultkey="ccddf"))


reference = dict(type="ReferenceLoader", properties=dict(loadType="CSV",
                                                         fileName=[
                                                             "Reference/bna_master_dec17.csv"],
                                                         resultkey="refdf"))


elem3=dict(id=3,next=4,type="ExpressionEvaluator",properties=dict(source='ccddf',resultkey='ccddf',

expressions=["df=df[df['TERMINAL_ID'].isin(payload['refdf']['ATM_ID'].unique())]"]))



elem4 = dict(id=4, next=5, type="PreLoader", properties=dict(loadType="Excel", source="SWITCH",
                                                             feedPattern="TLFFILE#%d%m%Y#.xls",
                                                             feedParams=dict(
                                                                 feedformatFile='SWITCH.csv',skiprows=1),
                                                             feedFilters=switch_filters,
                                                             resultkey="switchdf"))

elem5=dict(type='FilterDuplicates',properties=dict(source='switchdf',sourceTag='SWITCH',duplicateStore='duplicateStore',allowOneInMatch=True,resultKey="switchdf_unq",keyColumns=["CARD_NUM", "ATM_ID","AMOUNT","RRNO","INDICATOR"]))


elem6 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                     feedPattern="BNAHST#%d%m%Y#.txt",
                                                     feedParams=dict(feedformatFile="CBS.csv", skipTopRows=1,
                                                                     skipBottomRows=1),
                                                     feedFilters=cbs_filters,
                                                     resultkey="cbsdf"))


elem7=dict(type='FilterDuplicates',properties=dict(source='cbsdf',sourceTag='CBS',duplicateStore='duplicateStore',allowOneInMatch=True,resultKey="cbsdf_unq",keyColumns=["Card Number", "BNA_ATM_ID","Amount","RR Number","Dr/CR Indicator"]))


#
# elem8 = dict(type="NWayMatch",
#              properties=dict(sources=[dict(source="switchdf_unq",
#                                            columns=dict(keyColumns=["CARD_NUM", "ATM_ID", "RRNO"],
#                                                         amountColumns=["AMOUNT"],
#                                                         matchColumns=[  "CARD_NUM", "ATM_ID", "RRNO","AMOUNT"], crdrcolumn=['INDICATOR'],
#                                                         CreditDebitSign=True), sourceTag="SWITCH")], matchResult="reversal"))
# elem9 = dict(id=6, next=7, type="NWayMatch",
#              properties=dict(sources=[dict(source="cbsdf_unq",
#                                            columns=dict(keyColumns=["Card Number", "BNA_ATM_ID", "RR Number"],
#                                                         amountColumns=['Amount'],
#                                                         matchColumns=["Card Number", "BNA_ATM_ID", "RR Number",'Amount'], crdrcolumn=['Dr/CR Indicator'],
#                                                         CreditDebitSign=True), sourceTag="CBS")],
#                              matchResult="reversal"))
#
# elem10 = dict(type="NWayMatch",
#              properties=dict(sources=[dict(source="ccddf",
#                                            columns=dict(keyColumns=["CARD_NUMBER", "TERMINAL_ID",'RRN_NO'],
#                                                         amountColumns=["TXN_AMOUNT"],
#                                                         matchColumns=["CARD_NUMBER", "TERMINAL_ID","TXN_AMOUNT",'RRN_NO'], crdrcolumn=['DR_CR_IND'],
#                                                         CreditDebitSign=True), sourceTag="CCD")],
#                              matchResult="reversal"))

elem11 = dict(type="NWayMatch",
             properties=dict(sources=[dict(source="switchdf_unq",
                                           columns=dict(
                                               keyColumns=["CARD_NUM", "ATM_ID", "RRNO","TRAN_DATE"],
                                               sumColumns=['AMOUNT'],
                                               matchColumns=[  "CARD_NUM", "ATM_ID", "RRNO","AMOUNT","TRAN_DATE"]),
                                           sourceTag="SWITCH"),
                                      dict(source="cbsdf_unq", columns=dict(
                                          keyColumns=["Card Number", "BNA_ATM_ID", "RR Number","Tran Date"], sumColumns=['Amount'],
                                          matchColumns=[ "Card Number", "BNA_ATM_ID", "RR Number","Amount","Tran Date"]), sourceTag="CBS"),

                                       dict(source="ccddf", columns=dict( keyColumns=["CARD_NUMBER", "TERMINAL_ID",'RRN_NO','TXN_DATE'], sumColumns=['TXN_AMOUNT'], matchColumns=["CARD_NUMBER", "TERMINAL_ID",'RRN_NO',"TXN_AMOUNT",'TXN_DATE']), sourceTag="CCD")], matchResult="results"))


summary = dict(type='GenerateReconSummary',
               properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                        dict(resultKey="SWITCH", sourceTag='SWITCH',
                                             aggrCol=['AMOUNT']),dict(resultKey="CCD", sourceTag='CCD',
                                             aggrCol=['TXN_AMOUNT'])],groupbyCol=['STATEMENT_DATE']))


elem12=dict(type='DumpData',properties=dict(dumpPath='CCDRecon',matched='all'))


meta=dict(type="GenerateReconMetaInfo",properties=dict())

elements = [elem1,elem2,reference,elem3,elem4,elem5,elem6,elem7,elem11,summary,elem12,meta]


f = FlowRunner(elements)
f.run()