import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]
    basePath = "/usr/share/nginx/smartrecon/mft/"
    recontype = "FNFA"
    reconName = 'FNFARecon'
cbs_filters = ["df['Card Number'] = df['Card Number'].str.rstrip(' ')",
               "df['RR Number'] = df['RR Number'].str.rstrip(' ').str.lstrip(' ')",
               "df['A/C Number'] = df['A/C Number'].str.lstrip(' ')",
               "df['BNA_ATM_ID'] = df['BNA_ATM_ID'].str.lstrip(' ')","df=df[(df['Term_ID'].isin(['FNFA','ABKT']))]"
               ]

switch_filters = ["df.loc[df[(df['RRNO'] == 'null') | (df['RRNO'].isnull()) | (df['RRNO'].astype(str) == '000000000000')].index,'RRNO'] = df['SEQ_NUM']",
                  "df['RRNO'] = df['RRNO'].astype(str).str.rstrip(' ').str.lstrip(' ')",
                  "df = df[(df['TRAN_CODE'].str.startswith('10')) | (df['TRAN_CODE'].str.contains('W1',case=False))]",
                  "df = df[(df['RESP_CODE'].isin(['000','001'])) & (df['TERM_FIID'] == '5997')& (df['CARD_FIID']).isin(['FNFA','ABKT'])]",
                  "df['INDICATOR']=''",
                  "df['INDICATOR'] = df['TRAN_CODE'].str.slice(0,2).map({'10':'D','w1':'C','W1':'C'})",
                  "df['STATUS']='CASH_ WITHDRAWL'",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('10'))),['INDICATOR','STATUS']]=['C','CASH_WITHDRAWL_REV']",
                  "df.loc[((df['REVERSAL_REASON']=='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['C','CASH_DEPOSIT']",
                  "df.loc[((df['REVERSAL_REASON']!='00') & (df['TRAN_CODE'].str.startswith('W1'))),['INDICATOR','STATUS']]=['D','CASH_DEPOSIT_REV']"
                  ]
acq_fil=["df['Transaction Amount']=df['Transaction Amount']/100","df=df[df['Response Code'].isin(['00','28'])]","df['INDICATOR']=''",
         
         "df.loc[(df['Response Code']=='00'),'INDICATOR']='C'","df.loc[(df['Response Code']=='28'),'INDICATOR']='D'"
         ]

elem1 = dict(id=1, next=2, type="Initializer",
             properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                             recontype="BNA",uploadFileName=uploadFileName,compressionType="zip", resultkey='', reconName='FNFARecon'))

elem2 = dict(id=2, next=3,type="PreLoader", properties=dict(loadType="FixedFormat", source="Acquirer",
                                                             feedPattern="ACQRPNMT*.*",
                                                             feedParams=dict(feedformatFile="NPCI_OUTWARD_Structure.csv", skipTopRows=0,
                                                                             skipBottomRows=0),feedFilters=acq_fil,resultkey="acqdf"))


reference = dict(type="ReferenceLoader", properties=dict(loadType="CSV",
                                                         fileName=[
                                                             "Reference/bna_master_dec17.csv"],
                                                         resultkey="refdf"))


elem3=dict(id=3,next=4,type="ExpressionEvaluator",properties=dict(source='acqdf',resultkey='acqdf',expressions=["df=df[df['Card Acceptor Terminal ID'].isin(payload['refdf']['ATM_ID'].unique())]"]))





elem4 = dict(id=4, next=5, type="PreLoader", properties=dict(loadType="Excel", source="SWITCH",
                                                             feedPattern="TLFFILE#%d%m%Y#.xls",
                                                             feedParams=dict(
                                                                 feedformatFile='SWITCH.csv',skiprows=1),
                                                             feedFilters=switch_filters,
                                                             resultkey="switchdf"))

elem5=dict(id=5,next=6,type='FilterDuplicates',properties=dict(source='switchdf',sourceTag='SWITCH',duplicateStore='duplicateStore',resultKey="switchdf_unq",allowOneInMatch=True,keyColumns=["CARD_NUM", "ATM_ID","AMOUNT","RRNO","INDICATOR"]))

elem6 = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                     feedPattern="BNAHST#%d%m%Y#.txt",
                                                     feedParams=dict(feedformatFile="CBS.csv", skipTopRows=1,
                                                                     skipBottomRows=1),
                                                     feedFilters=cbs_filters,
                                                     resultkey="cbsdf"))


elem7=dict(id=7,next=8,type='FilterDuplicates',properties=dict(source='cbsdf',sourceTag='CBS',duplicateStore='duplicateStore',resultKey="cbsdf_unq",allowOneInMatch=True,keyColumns=["Card Number", "BNA_ATM_ID","Amount","RR Number","Dr/CR Indicator"]))

#
# elem8 = dict(type="NWayMatch",
#              properties=dict(sources=[dict(source="switchdf_unq",
#                                            columns=dict(keyColumns=["CARD_NUM", "ATM_ID", "RRNO"],
#                                                         amountColumns=["AMOUNT"],
#                                                         matchColumns=[  "CARD_NUM", "ATM_ID", "RRNO","AMOUNT"], crdrcolumn=['INDICATOR'],
#                                                         CreditDebitSign=True), sourceTag="SWITCH")], matchResult="reversal"))
#
#
# elem9 = dict(id=6, next=7, type="NWayMatch",
#              properties=dict(sources=[dict(source="acqdf",
#                                            columns=dict(keyColumns=["PAN Number", "Card Acceptor ID", "Ref Number"],
#                                                         amountColumns=["Transaction Amount"],
#                                                         matchColumns=["PAN Number", "Card Acceptor ID", "Ref Number","Transaction Amount"], crdrcolumn=['INDICATOR'],
#                                                         CreditDebitSign=True), sourceTag="Acquirer")], matchResult="reversal"))
# elem10 = dict(id=6, next=7, type="NWayMatch",
#              properties=dict(sources=[dict(source="cbsdf_unq",
#                                            columns=dict(keyColumns=["Card Number", "BNA_ATM_ID", "RR Number"],
#                                                         amountColumns=['Amount'],
#                                                         matchColumns=["Card Number", "BNA_ATM_ID", "RR Number",'Amount'], crdrcolumn=['Dr/CR Indicator'],
#                                                         CreditDebitSign=True), sourceTag="CBS")],
#                              matchResult="reversal"))


elem11 = dict(type="NWayMatch",
             properties=dict(sources=[dict(source="switchdf_unq",
                                           columns=dict(
                                               keyColumns=["CARD_NUM", "ATM_ID", "RRNO","TRAN_DATE"],
                                               sumColumns=['AMOUNT'],
                                               matchColumns=[  "CARD_NUM", "ATM_ID", "RRNO","AMOUNT","TRAN_DATE"]),
                                           sourceTag="SWITCH"),

                                      dict(source="cbsdf_unq", columns=dict(
                                          keyColumns=["Card Number", "BNA_ATM_ID", "RR Number","Tran Date"], sumColumns=['Amount'],
                                          matchColumns=[ "Card Number", "BNA_ATM_ID", "RR Number","Amount","Tran Date"]), sourceTag="CBS"),
                                      dict(source="acqdf", columns=dict( keyColumns=["PAN Number", "Card Acceptor ID", "Ref Number","Transaction Date"], sumColumns=['Transaction Amount'], matchColumns=["PAN Number", "Card Acceptor ID", "Ref Number",'Transaction Amount',"Transaction Date"]), sourceTag="Acquirer")], matchResult="results"))

summary = dict(type='GenerateReconSummary',
               properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                        dict(resultKey="SWITCH", sourceTag='SWITCH',
                                             aggrCol=['AMOUNT']),dict(resultKey="Acquirer", sourceTag='Acquirer',
                                             aggrCol=['Transaction Amount'])],groupbyCol=['STATEMENT_DATE']))


elem12=dict(id=7,type='DumpData',properties=dict(dumpPath='FNFARecon'))

meta=dict(type="GenerateReconMetaInfo",properties=dict())


elements = [elem1,elem2,reference,elem3,elem4,elem5,elem6,elem7,elem11,summary,elem12,meta]


f = FlowRunner(elements)
f.run()