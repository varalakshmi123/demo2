import sys
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName =sys.argv[2]

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName, recontype="AEPS", compressionType="zip", resultkey='',
                                 reconName="AEPS_ONUS"))

    cbs_filters=[
                 "df=df[~(df['FROM ACCOUNT NUMBER'].str.startswith('8968'))]",
                 "df['RRN']=df.PARTICULARS.str.split(':').str[1]",
                 "df['RRN']=df['RRN'].str.strip()",
                 "df=df[~(df['TO ACCOUNT NUMBER'].str.startswith('8968'))]",
                 "df['DrCrIndicator'] = 'D'",
                 "df=df.dropna(axis=0,how='all',subset=['AMOUNT'])",
                 "df.loc[df['PARTICULARS'].str.contains('REV'),'DrCrIndicator'] = 'C'"
                 ]


    elem2= dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                       feedParams=dict(delimiter="|",
                                                                   feedformatFile='CORP_AEPS_CBS_Structure.csv'),
                                                   feedPattern="AEPS_ONUS_*.*",
                                                   resultkey="cbsdf", feedFilters=cbs_filters))
    drop_cbs_duplicates = dict(type="FilterDuplicates",
                               properties=dict(source="cbsdf", duplicateStore="duplicateStore",
                                               allowOneInMatch=True,
                                               keyColumns=["RRN", 'AMOUNT'], sourceTag="CBS",
                                               resultKey="cbsdf"))

    switch_filters = ["df=df[df['vendor_response']=='SUCCESS']"]

    elem3 = dict(type="PreLoader", properties=dict(loadType='Excel', source="SWITCH",
                                                   feedPattern="ALL_TXN_REPORT_*.*",
                                                   feedParams=dict(skiprows=1,
                                                                   feedformatFile='AEPS_ONUS_SWITCH_Structure.csv'),
                                                   resultkey="switchdf", feedFilters=switch_filters))
    drop_switch_duplicates = dict(type="FilterDuplicates",
                                  properties=dict(source="switchdf", duplicateStore="duplicateStore",
                                                  allowOneInMatch=True,
                                                  keyColumns=["rrn", 'trans_amt'], sourceTag="SWITCH",
                                                  resultKey="switchdf"))

    elem4 = dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=dict(
                                                   keyColumns=["RRN", 'AMOUNT'],
                                                   sumColumns=[],
                                                   matchColumns=["RRN", 'AMOUNT']),
                                               sourceTag="CBS"),
                                          dict(source="switchdf", columns=dict(
                                              keyColumns=["rrn", 'trans_amt'], sumColumns=[],
                                              matchColumns=["rrn", 'trans_amt']),
                                               sourceTag="SWITCH")],
                                 matchResult="results"))

    elem5 = dict(type="NWayMatch", properties=dict(sources=[dict(source="results.CBS",
                                                                 columns=dict(
                                                                     keyColumns=["STAN NUMBER", 'AMOUNT'],
                                                                     amountColumns=[],
                                                                     crdrcolumn=["DrCrIndicator"],
                                                                     CreditDebitSign=False),
                                                                 subsetfilter=["df=df[df['SWITCH Match']=='UNMATCHED']"],
                                                                 sourceTag="CBS")],
                                                   matchResult="results"))


    elem6 = dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
                                                             expressions=[
                                                                 "df=df[~df['TRANSACTION TIME'].str.startswith('23')]",
                                                                 "df=df[(df['Self Matched']=='')&(df['SWITCH Match']=='UNMATCHED')]",
                                                                 "df['No']='N'", "df['Yes']='Y'",
                                                                 "df['Blank1']=''", "df['Blank2']=''",
                                                                 "df['Blank3']=''", "df['Blank4']=''",
                                                                 "df['Blank5']=''",
                                                                 "df=df[['BRANCH CODE','DrCrIndicator','TO ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                 "df.rename(columns={'TO ACCOUNT NUMBER':'ACCOUNT NUMBER','DrCrIndicator':'DrCrInd'},inplace=True)",
                                                                 "df['PARTICULARS']=df['PARTICULARS'].str.replace('TRANSFER','REV')"],
                                                             resultkey='cbsunmatchoriginal'))

    elem7= dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
                                                              expressions=[
                                                                  "df=df[~df['TRANSACTION TIME'].str.startswith('23')]",
                                                                  "df=df[(df['Self Matched']=='')&(df['SWITCH Match']=='UNMATCHED')]",
                                                                  "df['No']='N'", "df['Yes']='Y'",
                                                                  "df['Blank1']=''", "df['Blank2']=''",
                                                                  "df['Blank3']=''", "df['Blank4']=''",
                                                                  "df['Blank5']=''",
                                                                  "df['DrCrInd'] = np.where(df['DrCrIndicator']=='C', 'D', 'C')",
                                                                  "df=df[['BRANCH CODE','DrCrInd','FROM ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                  "df.rename(columns={'FROM ACCOUNT NUMBER':'ACCOUNT NUMBER'},inplace=True)",
                                                                  "df['PARTICULARS']=df['PARTICULARS'].str.replace('TRANSFER','REV')"],
                                                              resultkey='cbsrevunmatch'))
    carryforward=dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
                                                             expressions=[
                                                                 # "df=df[~df['TRANSACTION TIME'].str.startswith('00')]",
                                                                 "df.loc[(df['Self Matched']=='')&(df['SWITCH Match']=='UNMATCHED')&(~df['TRANSACTION TIME'].str.startswith('23')),'Remarks']='ReverseEntryPosted'",
                                                                 "df.loc[(df['Self Matched']=='')&(df['SWITCH Match']=='UNMATCHED')&(~df['TRANSACTION TIME'].str.startswith('23')),'Self Matched']='MATCHED'"
                                                             ],resultkey = 'results.CBS'))

    elem8 = dict(type="SourceConcat",
                  properties=dict(sourceList=['cbsunmatchoriginal', 'cbsrevunmatch'], resultKey="cbsfinal"))

    elem9 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='cbsfinal', sourceTag="CBS")], resultKey='cbs_report',
                                  writeToFile=True,
                                  reportName='UnmatchReversal'))



    elem10 = dict(type="DumpData", properties=dict(dumpPath='AEPS ONUS Recon', matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2,drop_cbs_duplicates, elem3,drop_switch_duplicates, elem4, elem5, elem6,elem7,carryforward,elem8,elem9,elem10,gen_meta_info]

    f = FlowRunner(elements)
    f.run()
