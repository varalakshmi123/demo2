import sys
import pandas

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config


def Settelement():

    # elem1 = dict(type="Initializer",
    #             properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
    #                             uploadFileName=uploadFileName, recontype="AEPS", compressionType="zip", resultkey='',
    #                             reconName="AEPS"))

    npci_filters = [
        # "df = df[df['Transaction Amount'] > 0]",
        "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
    ]

    issnpci = dict(type="PreLoader", properties=dict(loadType="AEPS", source="NPCI",
                                                     feedPattern="ISSRPPKU*.*",
                                                     feedParams=dict(type="INWARD", skiprows=0),
                                                     feedFilters=npci_filters,
                                                     resultkey="issnpci"))

    acqnpci = dict(type="PreLoader", properties=dict(loadType="AEPS", source="NPCI",
                                                     feedPattern="ACQRPPKU*.*",
                                                     feedParams=dict(type="OUTWARD", skiprows=0),
                                                     feedFilters=npci_filters,
                                                     resultkey="acqnpci"))

    adjustmentload = dict(type="PreLoader", properties=dict(loadType="HTML", source="Adjustment",
                                                            feedPattern="adjust_*.*",
                                                            feedParams=dict(feedformatFile='PKGB_Adjustment_Structure.csv',parseIndex=2),
                                                            feedFilters=[],
                                                            resultkey="Adjustmentdf"))

    issfilter00 = dict(type="ExpressionEvaluator", properties=dict(source="issnpci",
                                   expressions=[
                                       "df = df[df['Response Code'] == '00']",
                                       "df['count']=1", "df['InterchangeFee']=0",
                                       "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']<100),'InterchangeFee']=0",
                                       "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']>=100),'InterchangeFee']=df['Transaction Amount']*0.005",
                                       "df.loc[df['Transaction Type']=='25','InterchangeFee']=df['Transaction Amount']*0.0005",
                                       "df.loc[df['InterchangeFee']>15,'InterchangeFee']=15",
                                       "df=df.groupby(['Transaction Type','Response Code'])['Transaction Amount','InterchangeFee','count'].sum().reset_index()",
                                       # "df['SwitchingFee']=1",
                                       "df.loc[(df['Transaction Type']=='KY')|(df['Transaction Type']=='25')|(df['Transaction Type']=='FC')|(df['Transaction Type']=='05')|(df['Transaction Type']=='01')|(df['Transaction Type']=='04'),'SwitchingFee']=df['count']*0.25",
                                       "df.loc[(df['Transaction Type']=='UA')|(df['Transaction Type']=='DM'),'SwitchingFee']=df['count']*0.10",
                                       # "df.loc[,'SwitchingFee']=df['count']*0",
                                       "df['Switching Fee-GST']=(df['SwitchingFee']*18)/100",
                                       "df['Approved Fee-GST']=(df['InterchangeFee']*18)/100"
                                   ], resultkey='issreport00'))

    issfilter = dict(type="ExpressionEvaluator", properties=dict(source="issnpci",
                                                                 expressions=["df = df[df['Response Code'] != '00']",
                                                                              "df['count']=1",
                                                                              "df=df.groupby(['Transaction Type','Response Code'])['count'].sum().reset_index()"],
                                                                 resultkey='issreport'))

    acqfilter00 = dict(type="ExpressionEvaluator", properties=dict(source="acqnpci",
                           expressions=[
                               "df = df[df['Response Code'] == '00']",
                               "df['count']=1", "df['InterchangeFee']=0",
                               "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']<100),'InterchangeFee']=0",
                               "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']>=100),'InterchangeFee']=df['Transaction Amount']*0.005",
                               "df.loc[df['Transaction Type']=='25','InterchangeFee']=df['Transaction Amount']*0.0005",
                               "df.loc[df['InterchangeFee']>15,'InterchangeFee']=15",
                               "df=df.groupby(['Transaction Type','Response Code'])['Transaction Amount','InterchangeFee','count'].sum().reset_index()",
                               "df.loc[(df['Transaction Type']=='KY')|(df['Transaction Type']=='25')|(df['Transaction Type']=='FC'),'SwitchingFee']=df['count']*0.25",
                               "df.loc[(df['Transaction Type']=='DM'),'SwitchingFee']=df['count']*0.10",
                               "df['Switching Fee-GST']=(df['SwitchingFee']*18)/100",
                               "df['Approved Fee-GST']=(df['InterchangeFee']*18)/100"],
                           resultkey='acqreport00'))

    acqfilter = dict(type="ExpressionEvaluator", properties=dict(source="acqnpci",
                                                                 expressions=["df = df[df['Response Code'] != '00']",
                                                                              "df['count']=1",
                                                                              "df=df.groupby(['Transaction Type','Response Code'])['count'].sum().reset_index()"],
                                                                 resultkey='acqreport'))

    sttelementexe = dict(type='DataFiller',
                         properties=dict(template='settlement_report.csv',
                                         alias={"issreport00": "df", "issreport": "rem", "acqreport00": 'df1',
                                                "acqreport": 'rem1'},
                                         resultKey='settlement'))

    settlementreport = dict(type='ReportGenerator',
                            properties=dict(sources=[dict(source='settlement',
                                                          filterConditions=[

                                                              "df.loc[len(df), ['Description','Debit','Credit']] = "
                                                              "['Issuer/Acquirer Sub Totals',"
                                                              "df['Debit'].replace('', np.nan).astype(np.float64).sum(),"
                                                              "df['Credit'].replace('', np.nan).astype(np.float64).sum()]"
                                                          ],
                                                          sourceTag="NPCI")], resultKey='issreport',
                                            writeToFile=True,
                                            reportName='settlement'))

    # elem7= dict(type='ReportGenerator',
    #               properties=dict(sources=[dict(source='issreport', sourceTag="NPCI")], resultKey='iss_npci_report',
    #                               writeToFile=True,
    #                               reportName='iss_npci_report'))

    adjustment = dict(type="ExpressionEvaluator", properties=dict(source="Adjustmentdf",
                                                                  expressions=[
                                                                      "df=df[df['Acquirer']!='PKU']", "df=df[['BankAdjRef']]",
                                                                      r"df['BankAdjRef']=df['BankAdjRef'].str.lstrip('\'')"
                                                                  ], resultkey='Adjustment_report'))

    save_current_disputes = dict(type='MasterDumpHandler',
                                 properties=dict(masterType="AEPS_OFFUS", operation="find", source='cbsdf',
                                                 inputdf='Adjustment_report',
                                                 dropOnRollback=True, resultKey='adjustment'))

    adjunmatchoriginal = dict(type="ExpressionEvaluator", properties=dict(source="adjustment",
                                                                          expressions=[
                                                                              "df['No']='N'", "df['Yes']='Y'",
                                                                              "df['Blank1']=''", "df['Blank2']=''",
                                                                              "df['Blank3']=''", "df['Blank4']=''",
                                                                              "df['Blank5']=''",
                                                                              "df=df[['BRANCH CODE','DrCrIndicator','TO ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                              "df.rename(columns={'TO ACCOUNT NUMBER':'ACCOUNT NUMBER','DrCrIndicator':'DrCrInd'},inplace=True)",
                                                                              "df['PARTICULARS']=df['PARTICULARS'].str.replace('TRANSFER','REV')"],
                                                                          resultkey='adjunmatchoriginal'))

    adjrevunmatch = dict(type="ExpressionEvaluator", properties=dict(source="adjustment",
                                                                     expressions=[
                                                                         "df['No']='N'", "df['Yes']='Y'",
                                                                         "df['Blank1']=''", "df['Blank2']=''",
                                                                         "df['Blank3']=''", "df['Blank4']=''",
                                                                         "df['Blank5']=''",
                                                                         "df['DrCrInd'] = np.where(df['DrCrIndicator']=='C', 'D', 'C')",
                                                                         "df=df[['BRANCH CODE','DrCrInd','FROM ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                         "df.rename(columns={'FROM ACCOUNT NUMBER':'ACCOUNT NUMBER'},inplace=True)",
                                                                         "df['PARTICULARS']=df['PARTICULARS'].str.replace('TRANSFER','REV')"],
                                                                     resultkey='adjrevunmatch'))

    adjustmentfinal = dict(type="SourceConcat",
                           properties=dict(sourceList=['adjunmatchoriginal', 'adjrevunmatch'], resultKey="adjfinal"))

    adjustmentreversal = dict(type='ReportGenerator',
                              properties=dict(sources=[dict(source='adjfinal', sourceTag="Adjustment")],
                                              resultKey='cbs_report',
                                              writeToFile=True,
                                              reportName='AdjustmentReversal'))

    elements = [issnpci, acqnpci, adjustmentload, issfilter00, issfilter, acqfilter00, acqfilter, sttelementexe,
                settlementreport, adjustment,
                save_current_disputes,
                adjunmatchoriginal, adjrevunmatch, adjustmentfinal, adjustmentreversal]

    return elements





