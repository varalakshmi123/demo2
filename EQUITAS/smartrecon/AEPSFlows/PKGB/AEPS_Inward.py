import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = '14-Feb-2019'
    uploadFileName = '14022019.zip'

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 recontype="AEPS", compressionType="zip", resultkey='', reconName="AEPS Inward",
                                 uploadFileName=uploadFileName))

    npci_filters = ["df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
                    "df = df[df['Response Code'] == '00']"]

    elem2 = dict(type="PreLoader", properties=dict(loadType="NPCI", source="NPCI",
                                                   feedPattern="ISSRPPKU*.*",
                                                   feedParams=dict(type="AEPS_INWARD"),
                                                   feedFilters=npci_filters,
                                                   resultkey="npcidf"))

    cbs_filters = ["df['TRN_ID_TMP'] = df['REMARKS'].str.split('/',expand = True)[0].fillna('')",
                   "df_acqr = df[df['REMARKS'].str.contains('ISSR/4')]",
                   "df_rev = df[df['TRN_ID_TMP'].isin(df_acqr['TranID'].unique().tolist())]",
                   "df_acqr['Dr/Cr Indicator'] = 'D'", "df_rev['Dr/Cr Indicator'] = 'C'",
                   "df = pd.concat([df_acqr,df_rev], join_axes=[df_acqr.columns])", "del df['TRN_ID_TMP']"]

    elem3 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                   feedParams=dict(delimiter="|", skiprows=1,
                                                                   feedformatFile='AEPS_CBS_Structure.csv'),
                                                   feedPattern="AEPS_CBS_*.*",
                                                   resultkey="cbsdf", feedFilters=cbs_filters))

    switch_filters = ["df = df[df['itgs_trans_type'] == '4']",
                      "tran_type={'10000':'Withdrawal', '210000':'Deposit','900000':'FUNDTRANSFER','0':'ONUS_AADHAARPAY'}",
                      "df['TRANSACTION_TYPE']=df['itgs_vendor_req'].map(tran_type)",
                      "df[['CBS_To_Terminal_Id','From Bank Code','To Bank Code']]=df['itgs_from_acct_no'].str.split(expand=True)",
                      "df[['CBS_From_Terminal_Id','From Branch Code','To Branch Code']]=df['itgs_to_acct_no'].str.split(expand=True)"]

    elem4 = dict(type="PreLoader", properties=dict(loadType='CSV', source="SWITCH",
                                                   feedPattern="OFFUS_SUCCESS_*.*",
                                                   feedParams=dict(delimiter="|", skiprows=1,
                                                                   feedformatFile='AEPS_SWITCH_Structure.csv'),
                                                   resultkey="switchdf", feedFilters=switch_filters))

    elem5 = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                 columns=dict(
                                                                     keyColumns=["RRN"],
                                                                     amountColumns=['AMOUNT'],
                                                                     crdrcolumn=["Dr/Cr Indicator"],
                                                                     CreditDebitSign=False),
                                                                 sourceTag="CBS")],
                                                   matchResult="results"))

    elem6 = dict(id=6, next=7, type="NWayMatch",
                 properties=dict(sources=[dict(source="npcidf",
                                               columns=dict(
                                                   keyColumns=["Ref Number", 'Transaction Amount'],
                                                   sumColumns=[],
                                                   matchColumns=["Ref Number", "Transaction Amount"]),

                                               sourceTag="NPCI"),
                                          dict(source="results.CBS", columns=dict(
                                              keyColumns=["RRN", 'AMOUNT'], sumColumns=[],
                                              matchColumns=["RRN", 'AMOUNT']),
                                               subsetfilter=["df=df[(df['Self Matched']!='MATCHED')]"
                                                   ],
                                               sourceTag="CBS"),
                                          dict(source="switchdf", columns=dict(
                                              keyColumns=["itgs_gateway_rrn", "itgs_trans_amt"], sumColumns=[],
                                              matchColumns=["itgs_gateway_rrn", "itgs_trans_amt"]),
                                               sourceTag="SWITCH")],
                                 matchResult="results"))

    report_filters = [
        "tran_type = {'05': 'Balance Inquiry', 'FC': 'Fund Transfer', '04': 'Withdrawal','01': 'Deposit', 'UA': 'Biometric Authentication', '25': 'Aadhaar Pay'}",
        "df['Transaction Desc'] = df['Transaction Type'].map(tran_type)", "df['COUNT'] = 0",
        "df=df[df['CARRY_FORWARD'] != 'Y']",
        "df = df.groupby('Transaction Desc').agg({'Transaction Amount': 'sum', 'COUNT': 'count'}).reset_index()",
        "df.loc[len(df), ['Transaction Desc', 'Transaction Amount', 'COUNT']] = ['Total',df['Transaction Amount'].sum(),df['COUNT'].sum()]"]

    elem7 = dict(id=8, type="ReportGenerator", properties=dict(
        sources=[dict(source='npcidf', filterConditions=report_filters,
                      sourceTag="NPCI")],writeToFile=True,reportName='AEPS_Inward_Summary'))

    elem8 = dict(type="DumpData", properties=dict(dumpPath='AEPS Inward', matched='all'))

    elem9 = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [elem1, elem2, elem3, elem4,elem5, elem6, elem7, elem8, elem9]
    f = FlowRunner(elements)
    f.run()
