from pymongo import MongoClient
from bson import ObjectId
import pandas as pd
import os
import re
client = MongoClient('localhost')

db=client['erecon']
basedir='/usr/share/nginx/smartrecon/NEFT/'
listofflows=os.listdir(basedir)
doc={}
print listofflows

lis = []
for flow in listofflows:
    print flow
    if 'NEFTOUTWARDAB.py' in flow:
        with open(basedir+flow) as f:
            doc = {}
            for line in f.readlines():

                if 'reconName' in line:
                    if doc.get('reconName'):
                      pass
                    else:
                        doc['reconName']=["".join(re.split("[^a-zA-Z]*",i.split('=')[1])) for i in line.split(',')if 'reconName' in i][0]
                        # lis.append(doc)
                if  'source' in line:
                    if doc.get('source'):
                        pass
                    else:
                        doc['source']=(["".join(re.split("[^a-zA-Z]*",i.split('=')[1])) for i in line.split(',')if 'source' in i][0])
                if 'feedformatFile' in line:
                    if doc.get('feedformatFile'):
                        pass
                    else:
                        doc['feedformatFile']=["".join(re.split("[^.a-zA-Z]*",i.split('=')[1])) for i in line.split(',')if 'feedformatFile' in i][0]

                if len(doc.keys())==3:
                    lis.append(doc)
                    break



            exit(0)



# ll=['NEFT_OUTWARD_Structure.csv','CBS_ANDHRA.csv','NEFT_INWARD_Structure.csv','NEFT_Return_Structure.csv']

# doc=[{"reconName":"NEFTOutward",{"feedformat":[],"source":""},{"feedformat":[],"source":""},
#     {"reconName":"NEFTInward","feedformat":[],"source":""},{"reconName":"NEFTInward","feedformat":[],"source":""}]
# l=['GL MAPPING.csv', 'CSC_TagIssuance.csv', 'SyndicateBank_NEFT_CBS_Structure.csv', 'Equitas_ATM_SWITCH_Structure.csv', 'PG_Chargeback_Structure.csv', 'Self_Registration_Structure(Tag_Iss).csv', 'ETC_ALLGL_Structure.csv', 'NPCI_Merchant_Structure.csv', 'Tag_Issuance_CBS_Structure(Tag_Iss).csv', 'CSC_Purchase.csv', 'UPI_CBS_OUTWARD.csv', 'CBS_JRI_Structure.csv', 'Self_Registration_Structure(Tag_Rech).csv', 'DOC_CRM.csv', 'CBS_PG_Structure.csv', 'CBS_ANDHRA.csv', 'AEPS_SWITCH_Structure.csv', 'NEFT_INWARD_Structure.csv', 'Tag_Recharge_CBS_Structure.csv', 'ETC_GL_Structure.csv', 'NPCI_INWARD_Structure.csv', 'UPI_CBS_INWARD.csv', 'NPCI_OUTWARD_Structure.csv', 'DocFlow.csv', 'IMPS_CBS_Structure.csv', 'UPI_OUTWARD_Structure.csv', 'BinMaster.csv', 'UPI_INWARD_Structure.csv', 'Equitas_ATM_HOST_Structure.csv', 'JRI_Structure.csv', 'IMPS_TIMEOUT_TXN_Structure.csv', 'AEPS_CBS_Structure.csv', 'NPCI_POST_Settlement_Structure.csv', 'VisaFixedFormatTCR00.csv', '861_NPCI_RUPAY_STMT_STRUCT.csv', 'CSC_Recharge.csv', 'Equitas_IMPS_CBS_Structure.csv', 'Tag_Issuance_CBS_Structure(Tag_Rech).csv', 'TXN_HEADER_FIELDS.csv', 'CBS_TXN_REPORT_Structure.csv', 'ETC_TXN_Report_Structure.csv', 'TransCodetoDRCR.csv', 'AEPS_OUTWARD_Structure.csv', 'AEPS_INWARD_Structure.csv', 'ATMCLMFormat.csv', 'CSC_MIS_Structure.csv', 'PG_Structure.csv', 'CSC_Settlement_Report.csv', 'UPI_INWARD_MERCHANT_Structure.csv', 'PG_Refund_Structure.csv', 'bna_master_dec17.xls', 'bna_master_dec17.csv', 'AcqCbs.csv', 'CORP_AEPS_SWITCH_Structure.csv', 'DMS_Structure.csv', 'Visa.csv', 'CCD.csv', 'SWITCH.csv', 'CBS.csv', 'Inward_23Jan2018_PMTable.csv', 'Outward_23Jan2018_PMTable.csv', 'PmTableStructure.csv', 'CustomerOnboardingMatchingColumns.csv', 'CBS_NEFT.csv', 'DOC_Wizard.csv', 'NEFTSyndicateInward.csv', 'NEFTSYNDICATEOutward.csv', 'NEFT_OUTWARD_Structure.csv', 'BillDesk_GL_Structure.csv', 'BillDesk_PGP_Structure.csv', 'CORP_AEPS_CBS_Structure.csv', 'CUST_CBS.csv', 'CUST_Wizard.csv', 'NEFT_Return_Structure.csv', 'IOB_UPI_CBS_Structure.csv', 'ACC_CBS.csv', 'ACC_CRM.csv', 'AccountOpening.csv', 'ACC_Wizard.csv', 'AEPS_ONUS_SWITCH_Structure.csv', 'BillDesk_ATOM_Structure.csv', 'BillDesk_ATOS_Structure.csv', 'BillDesk_UPP_Structure.csv', 'CUST_CRM.csv', 'MERCHANT_HEADER_FIELDS.csv', 'NPCI_ATM_DISPUTES_FORMAT.csv', 'PKGB_Adjustment_Structure.csv', 'Rupay_Bins.xls', 'settlement_report.csv', 'Transcodedrcrref.csv', 'UnbilledFormatAndhraBank.csv', 'UPI_TIMEOUT_Structure.csv', 'IOB_Timeout_Structure.csv', 'Feba_Structure.csv', 'Fincore_Structure.csv']



#
# for reference in ll:
#
#     df=pd.read_csv('/usr/share/nginx/smartrecon/Reference/'+reference)


# df=pd.read_csv('/usr/share/nginx/smartrecon/Reference/CBS_ANDHRA.csv')