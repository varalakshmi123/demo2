import sys
import pandas
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

def Settelement():

    npci_filters = [
    # "df = df[df['Transaction Amount'] > 0]",
    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
    ]

    issnpci = dict(type="PreLoader", properties=dict(loadType="AEPS", source="NPCI",
                                                 feedPattern="ISSRPCRU*.*",
                                                 feedParams=dict(type="INWARD", skiprows=0),
                                                 feedFilters=npci_filters,disableCarryFwd=True,
                                                 resultkey="issnpci"))

    acqnpci = dict(type="PreLoader", properties=dict(loadType="AEPS", source="NPCI",
                                                 feedPattern="ACQRPCRU*.*",
                                                 feedParams=dict(type="OUTWARD", skiprows=0),
                                                 feedFilters=npci_filters,disableCarryFwd=True,
                                                 resultkey="acqnpci"))

    adjustmentload = dict(type="PreLoader", properties=dict(loadType="HTML", source="Adjustment",
                                                        feedPattern="Cr_Adjust_rep_aeps.*",
                                                        feedParams=dict(feedformatFile='Corp_Adjustment_Structure.csv',parseIndex=[0],skiprows=[3]),disableCarryFwd=True,
                                                        feedFilters=[],errors=False,
                                                        resultkey="Adjustmentdf"))
    reportfilters = [
        "df=df.dropna(subset=['Debit','Credit'],how='all')",
                 "df=df[~df['Debit'].fillna('').str.isalpha()]",
                 "df=df[~df['Credit'].fillna('').str.isalpha()]",
                 "df['Debit'] = df['Debit'].str.replace('','0').astype(np.float64)",
                 "df['Credit'] = df['Credit'].str.replace('','0').astype(np.float64)",
                 "df['Debit'].fillna(0.0,inplace = True)",
                 "df['Credit'].fillna(0.0,inplace = True)"]

    aepsntsl = dict( type="PreLoader", properties=dict(loadType="HTML", source="aepsntsl",
                                                                            feedPattern="AEPSNTSLCRU*.*",
                                                                            disableCarryFwd=True,
                                                                            feedParams=dict(
                                                                                feedformatFile='AEPS_OFFUS_Structure.csv',
                                                                                parseIndex=[3,5] , skiprows=[0,1]),
                                                                            errors=False, feedFilters=reportfilters,
                                                                            resultkey="aepsntsldf"))
    htmlexe = dict(type='DataFiller',
                     properties=dict(template='html_report.csv',
                                     alias={"aepsntsldf": "df", },
                                     resultKey='htmlresult'))

    htmltemplatereport = dict(type='ReportGenerator',
                            properties=dict(sources=[dict(source='htmlresult',
                                                          filterConditions=["df['Expenses']=df['Expenses'].astype(np.float64)",
                                                              "df['GST Paid']=df['GST Paid'].astype(np.float64)",
                                                              "df['TotExp.']=df['TotExp.'].astype(np.float64)",
                                                              "df['Income']=df['Income'].astype(np.float64)",
                                                              "df['GST Recd']=df['GST Recd'].astype(np.float64)",
                                                              "df['TotIncom']=df['TotIncom'].astype(np.float64)",
                                                              "df['Dr Tran']=df['Dr Tran'].astype(np.float64)",
                                                              "df['Cr. Tran']=df['Cr. Tran'].astype(np.float64)",
                                                              "df['Net Amt.']=df['Net Amt.'].astype(np.float64)",
                                                              "df['NPCI TrnAmt']=df['NPCI TrnAmt'].astype(np.float64)",
                                                              "df=df.round(2)"],
                                                          sourceTag="AEPS")], resultKey='htmlreport',
                                            writeToFile=True,
                                            reportName='htmltempreport'))


    issfilter00 = dict(type="ExpressionEvaluator", properties=dict(source="issnpci",
                                                               expressions=[
                                                                   "df=df.reset_index()",
                                                                   "df = df[df['Response Code'] == '00']",
                                                                   "df['count']=1", "df['InterchangeFee']=0",
                                                                   "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']<100),'InterchangeFee']=0",
                                                                   "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']>=100),'InterchangeFee']=df['Transaction Amount']*0.005",
                                                                   "df.loc[df['Transaction Type']=='25','InterchangeFee']=df['Transaction Amount']*0.0005",
                                                                   "df.loc[df['InterchangeFee']>15,'InterchangeFee']=15",
                                                                   "df=df.groupby(['Transaction Type','Response Code'])['Transaction Amount','InterchangeFee','count'].sum().reset_index()",
                                                                   # "df['SwitchingFee']=1",
                                                                   "df.loc[(df['Transaction Type']=='KY')|(df['Transaction Type']=='25')|(df['Transaction Type']=='FC')|(df['Transaction Type']=='05')|(df['Transaction Type']=='01')|(df['Transaction Type']=='04'),'SwitchingFee']=df['count']*0.25",
                                                                   "df.loc[(df['Transaction Type']=='UA')|(df['Transaction Type']=='DM'),'SwitchingFee']=df['count']*0.10",
                                                                   # "df.loc[,'SwitchingFee']=df['count']*0",
                                                                   "df['Switching Fee-GST']=(df['SwitchingFee']*18)/100",
                                                                   "df['Approved Fee-GST']=(df['InterchangeFee']*18)/100"
                                                               ], resultkey='issreport00'))

    issfilter = dict(type="ExpressionEvaluator", properties=dict(source="issnpci",
                                                             expressions=["df = df[df['Response Code'] != '00']",
                                                                          "df['count']=1",
                                                                          "df=df.groupby(['Transaction Type','Response Code'])['count'].sum().reset_index()"],
                                                             resultkey='issreport'))

    acqfilter00 = dict(type="ExpressionEvaluator", properties=dict(source="acqnpci",
                                                               expressions=[
                                                                   "df=df.reset_index()",
                                                                   "df = df[df['Response Code'] == '00']",
                                                                   "df['count']=1", "df['InterchangeFee']=0",
                                                                   "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']<100),'InterchangeFee']=0",
                                                                   "df.loc[(df['Transaction Type'].isin(['01','FC','04'])) & (df['Transaction Amount']>=100),'InterchangeFee']=df['Transaction Amount']*0.005",
                                                                   "df.loc[df['Transaction Type']=='25','InterchangeFee']=df['Transaction Amount']*0.0005",
                                                                   "df.loc[df['InterchangeFee']>15,'InterchangeFee']=15",
                                                                   "df=df.groupby(['Transaction Type','Response Code'])['Transaction Amount','InterchangeFee','count'].sum().reset_index()",
                                                                   "df.loc[(df['Transaction Type']=='KY')|(df['Transaction Type']=='25')|(df['Transaction Type']=='FC'),'SwitchingFee']=df['count']*0.25",
                                                                   "df.loc[(df['Transaction Type']=='DM'),'SwitchingFee']=df['count']*0.10",
                                                                   "df['Switching Fee-GST']=(df['SwitchingFee']*18)/100",
                                                                   "df['Approved Fee-GST']=(df['InterchangeFee']*18)/100"],
                                                               resultkey='acqreport00'))

    acqfilter = dict(type="ExpressionEvaluator", properties=dict(source="acqnpci",
                                                             expressions=["df = df[df['Response Code'] != '00']",
                                                                          "df['count']=1",
                                                                          "df=df.groupby(['Transaction Type','Response Code'])['count'].sum().reset_index()"],
                                                             resultkey='acqreport'))

    sttelementexe = dict(type='DataFiller',
                     properties=dict(template='settlement_report.csv',
                                     alias={"issreport00": "df", "issreport": "rem", "acqreport00": 'df1',
                                            "acqreport": 'rem1'},
                                     resultKey='settlement'))

    settlementreport = dict(type='ReportGenerator',
                        properties=dict(sources=[dict(source='settlement',
                                                      filterConditions=[
                                          "df['Debit']=df['Debit'].replace('', np.nan).astype(np.float64)",
                                          "df['Credit']=df['Credit'].replace('', np.nan).astype(np.float64)",
                                          "df.loc[len(df), ['Description','Debit','Credit']] = "
                                          "['Issuer/Acquirer Sub Totals',"
                                          "df['Debit'].sum(),"
                                          "df['Credit'].sum()]",
                                          "df['No of transactions']=df['No of transactions'].replace(r'^\s*$',np.nan,regex=True).fillna(0).astype(np.int64)"
                                                      ],
                                                      sourceTag="NPCI")], resultKey='issreport',
                                        writeToFile=True,
                                        reportName='settlement'))

    save_settelementdump = dict(type='MasterDumpHandler',
                                 properties=dict(masterType="settlement Report", operation="insert", source='custom_reports.settlement',
                                                 insertCols=['Description','TypeofTransaction','DrCr','No of transactions','Debit','Credit'],
                                                 dropOnRollback=True, resultKey='settelementdump'))

    adjustment = dict(type="ExpressionEvaluator", properties=dict(source="Adjustmentdf",
                                                              expressions=[
                                                                  "df=df[df['Acquirer']!='CRU']", "df=df[['RRN']]",
                                                                  r"df['RRN']=df['RRN'].str.lstrip('\'')"
                                                              ], resultkey='Adjustment_report'))

    save_current_disputes = dict(type='MasterDumpHandler',
                             properties=dict(masterType="AEPS_OFFUS", operation="find", source='cbsdf',
                                             inputdf='Adjustment_report',
                                             dropOnRollback=True, resultKey='adjustment'))

    adjunmatchoriginal = dict(type="ExpressionEvaluator", properties=dict(source="adjustment",
                                                                      expressions=[
                                                                          "adf = payload['Adjustmentdf'].copy()",
                                                                          r"adf['RRN']=adf['RRN'].str.lstrip('\'')",
                                                                          "df=df.merge(adf[['RRN','Acquirer']],on=['RRN'],how='left')",
                                                                          "df['No']='N'", "df['Yes']='Y'",
                                                                          "df['Blank1']=''", "df['Blank2']=''",
                                                                          "df['Blank3']=''", "df['Blank4']=''",
                                                                          "df['Blank5']=''",
                                                                          "df['PARTICULARS']='FT REV RRNO'+df['PARTICULARS'].str.rsplit('RR NO',1).str[-1]+' DT '+df['PARTICULARS'].str.split(' ').str[2]+' CRADJ '+df['Acquirer']",
                                                                          "df['PARTICULARS']=df['PARTICULARS'].str.replace(' : ',':')",
                                                                          "df=df[['BRANCH CODE','DrCrIndicator','TO ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                          "df.rename(columns={'TO ACCOUNT NUMBER':'ACCOUNT NUMBER','DrCrIndicator':'DrCrInd'},inplace=True)"
                                                                          ],
                                                                      resultkey='adjunmatchoriginal'))

    adjrevunmatch = dict(type="ExpressionEvaluator", properties=dict(source="adjustment",
                                                                 expressions=[
                                                                     "adf = payload['Adjustmentdf'].copy()",
                                                                     r"adf['RRN']=adf['RRN'].str.lstrip('\'')",
                                                                     "df=df.merge(adf[['RRN','Acquirer']],on=['RRN'],how='left')",
                                                                     "df['No']='N'", "df['Yes']='Y'",
                                                                     "df['Blank1']=''", "df['Blank2']=''",
                                                                     "df['Blank3']=''", "df['Blank4']=''",
                                                                     "df['Blank5']=''",
                                                                     "df['DrCrInd'] = np.where(df['DrCrIndicator']=='C', 'D', 'C')",
                                                                     "df['PARTICULARS']='FT REV RRNO'+df['PARTICULARS'].str.rsplit('RR NO',1).str[-1]+' DT '+df['PARTICULARS'].str.split(' ').str[2]+' CRADJ '+df['Acquirer']",
                                                                     "df['PARTICULARS']=df['PARTICULARS'].str.replace(' : ',':')",
                                                                     "df=df[['BRANCH CODE','DrCrInd','FROM ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                                                                     "df.rename(columns={'FROM ACCOUNT NUMBER':'ACCOUNT NUMBER'},inplace=True)"
        ],
                                                                 resultkey='adjrevunmatch'))

    adjustmentfinal = dict(type="SourceConcat",
                       properties=dict(sourceList=['adjunmatchoriginal', 'adjrevunmatch'], resultKey="adjfinal"))
    

    adjustmentreversal = dict(type='ReportGenerator',
                          properties=dict(sources=[dict(source='adjfinal', sourceTag="Adjustment")],
                                          resultKey='cbs_report',
                                          writeToFile=True,
                                          reportName='AdjustmentReversal'))

    elements = [issnpci, acqnpci, adjustmentload,aepsntsl,htmlexe, htmltemplatereport,issfilter00, issfilter, acqfilter00, acqfilter, sttelementexe,
                settlementreport,save_settelementdump, adjustment,
                save_current_disputes,
                adjunmatchoriginal, adjrevunmatch, adjustmentfinal, adjustmentreversal]

    return elements





