import sys
import pandas
sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import SettlementReport

if __name__ == "__main__":
    # df = pandas.read_csv('/usr/share/nginx/smartrecon/Reference/settlement_report.csv')
    # stmtdate=sys.argv[1]
    # uploadFileName = sys.argv[2]
    stmtdate = '04-Jun-2019'
    uploadFileName = '04062019.zip'
    elem1 = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="AEPS", compressionType="zip", resultkey='',
                                reconName="AEPS_OFFUS"))

    npci_filters = ["df['Transaction Amount']=df['Transaction Amount'].fillna(0.0).astype(np.float64)",
                    "df = df[df['Transaction Amount'] > 0]",
                    "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100"]

    elem2 =dict(type="MultiLoader", properties=dict(sources=[dict(loadType="AEPS", source="NPCI",
                                                   feedPattern="ISSRPCRU*.*",
                                                   feedParams=dict(type="INWARD", skiprows=0),
                                                   feedFilters=npci_filters,
                                                   resultkey="npcitotal"),
                                        dict(loadType="AEPS", source="NPCI",
                                                   feedPattern="ACQRPCRU*.*",
                                                   feedParams=dict(type="OUTWARD", skiprows=0),
                                                   feedFilters=npci_filters,disableCarryFwd=True,
                                                   resultkey="npcitotal")]))

    # elem3 = dict(type="PreLoader", properties=dict(loadType="AEPS", source="NPCI",
    #                                                feedPattern="ACQRPCRU*.*",
    #                                                feedParams=dict(type="OUTWARD", skiprows=0),
    #                                                feedFilters=npci_filters,
    #                                                resultkey="npcitotal"))

    elem4 = dict(type="ExpressionEvaluator", properties=dict(source="npcitotal",
                     expressions=["df.loc[df['FEED_FILE_NAME'].str.startswith('ISS'),'SOURCE_TYPE'] = 'ISSUER'",
                                  "df.loc[df['FEED_FILE_NAME'].str.startswith('ACQ'),'SOURCE_TYPE'] = 'ACQUIRER'",
                                  "df = df[df['Response Code'] == '00']"
                                  ],resultkey='npcidf'))



    drop_npci_duplicates = dict(type="FilterDuplicates",
                                  properties=dict(source="npcidf", duplicateStore="duplicateStore",
                                                  allowOneInMatch=True,
                                                  keyColumns=["Ref Number", 'Transaction Amount'], sourceTag="NPCI",
                                                  resultKey="npcidf"))

    switch_filters = [
                      "dict1={'000000':'Merchant transaction','010000':'Withdrawal transaction','210000':'Deposit transaction','900000':'Fund transfer','310000':'Balance enquiry','910000':'Mini statement'}",
                      "df['Transaction Desc'] = df['VENDOR_REQUEST'].map(dict1)",
                      # "df=df[df['VENDOR_RESPONSE']=='00']",
                       "df['CBS_REQ_TYPE']=df['CBS_REQ_TYPE'].fillna(0).astype(np.int64).astype(str)",
                       "df['VENDOR_REQUEST']=df['VENDOR_REQUEST'].astype(str).fillna('').str.replace('\.(0)*','')",
                       "df['VENDOR_RESPONSE']=df['VENDOR_RESPONSE'].astype(str).fillna('').str.replace('\.(0)*','')",
                       "df=df[df['CBS_REQ_TYPE']=='400000']",
                      "df['CBS_RRN']=df['VENDOR RRN'].str[-6:]"]

    elem5 = dict(type="PreLoader", properties=dict(loadType='Excel', source="SWITCH",
                                                   feedPattern="AEPS_SWITCH_OFFUS_REPORT_*.*",
                                                   feedParams=dict(skiprows=1,
                                                                   feedformatFile='CORP_AEPS_SWITCH_Structure.csv'),
                                                   resultkey="switchtotal", feedFilters=switch_filters,disableCarryFwd=True))



    switchdf=dict(type="ExpressionEvaluator", properties=dict(source="switchtotal",
                     expressions=["df=df[df['VENDOR_RESPONSE']=='00']",
                                  ],resultkey='switchdf'))

    drop_switch_duplicates = dict(type="FilterDuplicates",
                                  properties=dict(source="switchdf", duplicateStore="duplicateStore",
                                                  allowOneInMatch=True,
                                                  keyColumns=['GATEWAY RRN', 'AMOUNT'], sourceTag="SWITCH",
                                                  resultKey="switch_duplicate"))

    cbs_filters = ["df['DrCrIndicator'] = 'D'",
                   "df.loc[df['PARTICULARS'].str.contains('REV'),'DrCrIndicator'] = 'C'",
                   "df['RRN']=df.PARTICULARS.str.split(':').str[1]",
                   "df['RRN']=df['RRN'].str.strip()"]
                   # "df['BLANK']=df['BLANK'].fillna('')"]

    elem6 = dict(type="PreLoader", properties=dict(loadType='CSV', source="CBS",
                                                   feedParams=dict(delimiter="|",
                                                                   feedformatFile='CORP_AEPS_CBS_Structure.csv'),
                                                   feedPattern="AEPS_OFFUS_*.*",
                                                   resultkey="cbsdf", feedFilters=cbs_filters))
    drop_cbs_duplicates = dict(type="FilterDuplicates",
                                properties=dict(source="cbsdf", duplicateStore="duplicateStore",
                                                allowOneInMatch=True,
                                                keyColumns=["RRN", 'AMOUNT'], sourceTag="CBS",
                                                resultKey="cbsdf"))

    save_current_disputes = dict(type='MasterDumpHandler',
                 properties=dict(masterType="AEPS_OFFUS", operation="insert", source='cbsdf',
                                 dropOnRollback=True, insertCols=['BRANCH CODE','DrCrIndicator','FROM ACCOUNT NUMBER',
                                                                  'TO ACCOUNT NUMBER','AMOUNT','PARTICULARS',
                                                                  'TRANSACTION DATE','RRN'],resultKey='cbsdb'))


    elem7= dict(type="NWayMatch",
                 properties=dict(sources=[dict(source="npcidf",
                                               columns=dict(
                                                   keyColumns=["Ref Number", 'Transaction Amount'],
                                                   sumColumns=[],
                                                   matchColumns=["Ref Number", "Transaction Amount"]),
                                               sourceTag="NPCI"),
                                          dict(source="cbsdf", columns=dict(
                                              keyColumns=["RRN", 'AMOUNT'], sumColumns=[],
                                              matchColumns=["RRN", 'AMOUNT']),
                                               sourceTag="CBS")],
                                 matchResult="results"))

    elem8 = dict(type="NWayMatch", properties=dict(sources=[dict(source="results.CBS",
                                                                 columns=dict(
                                                                     keyColumns=["STAN NUMBER",'AMOUNT'],
                                                                     amountColumns=[],
                                                                     crdrcolumn=["DrCrIndicator"],
                                                                     CreditDebitSign=False),subsetfilter=["df=df[df['NPCI Match']=='UNMATCHED']"],
                                                                 sourceTag="CBS")],
                                                   matchResult="results"))

    elem9=dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
            expressions=["df=df[~df['TRANSACTION TIME'].str.startswith('23')]",
                "df=df[(df['Self Matched']=='')&(df['NPCI Match']=='UNMATCHED')]","df['No']='N'","df['Yes']='Y'",
                         "df['Blank1']=''","df['Blank2']=''","df['Blank3']=''","df['Blank4']=''","df['Blank5']=''",
                         "df=df[['BRANCH CODE','DrCrIndicator','TO ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                          "df.rename(columns={'TO ACCOUNT NUMBER':'ACCOUNT NUMBER','DrCrIndicator':'DrCrInd'},inplace=True)",

                         # "df['PARTICULARS']=df['PARTICULARS'].str.replace('TRANSFER','REV')",
                         "df['PARTICULARS']='FT REV RRNO'+df['PARTICULARS'].str.rsplit('RR NO',1).str[-1]+' DT '+df['PARTICULARS'].str.split(' ').str[2]+' FAILED TXN'",
                         "df['PARTICULARS']=df['PARTICULARS'].str.replace(' : ',':')"
                         ],
                                                            resultkey='cbsunmatchoriginal'))

    elem10=dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
            expressions=[
                "df=df[~df['TRANSACTION TIME'].str.startswith('23')]",
                "df=df[(df['Self Matched']=='')&(df['NPCI Match']=='UNMATCHED')]","df['No']='N'","df['Yes']='Y'",
                         "df['Blank1']=''","df['Blank2']=''","df['Blank3']=''","df['Blank4']=''","df['Blank5']=''",
                         "df['DrCrInd'] = np.where(df['DrCrIndicator']=='C', 'D', 'C')",
                         "df=df[['BRANCH CODE','DrCrInd','FROM ACCOUNT NUMBER','AMOUNT','PARTICULARS','No','Blank1','Blank2','Blank3','Blank4','Blank5','Yes']]",
                          "df.rename(columns={'FROM ACCOUNT NUMBER':'ACCOUNT NUMBER'},inplace=True)",
                         "df['PARTICULARS']='FT REV RRNO'+df['PARTICULARS'].str.rsplit('RR NO',1).str[-1]+' DT '+df['PARTICULARS'].str.split(' ').str[2]+' FAILED TXN'",
                         "df['PARTICULARS']=df['PARTICULARS'].str.replace(' : ',':')" ],                                  resultkey='cbsrevunmatch'))

    elem11=dict(type="SourceConcat",properties=dict(sourceList=['cbsunmatchoriginal','cbsrevunmatch'], resultKey="cbsfinal"))

    elem12= dict(type='ReportGenerator',
                   properties=dict(sources=[dict(source='cbsfinal', sourceTag="CBS")], resultKey='cbs_report',
                                  writeToFile=True,
                                  reportName='CBSUnmatchReversal'))
    carryforward = dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
                                expressions=[
                                    # "df=df[~df['TRANSACTION TIME'].str.startswith('23')]",
                                    "df.loc[(df['Self Matched']=='')&(df['NPCI Match']=='UNMATCHED')&(~df['TRANSACTION TIME'].str.startswith('23')),'Remarks']='ReverseEntryPosted'",
                                    "df.loc[(df['Self Matched']=='')&(df['NPCI Match']=='UNMATCHED')&(~df['TRANSACTION TIME'].str.startswith('23')),'Self Matched']='MATCHED'"],
                                                                    resultkey='results.CBS'))

    drcload=dict(type="PreLoader", properties=dict(loadType='CSV', source="DRC",
                                                   feedParams=dict(delimiter=",",skiprows=1,
                                                                   feedformatFile='CORP_AEPS_DRC_Structure.csv'),
                                                   feedPattern="DRCDRAEPS*.*",preloadScript=True,preLoadFunc='OfsaaScript',disableCarryFwd = True,
                                                   resultkey="drcdf"))
    drclookupcbs=dict(type='VLookup',
                  properties=dict(data='drcdf', lookup='results.CBS', dataFields=['RRN'],
                                  lookupFields=['RRN'],includeCols=['Self Matched','NPCI Match','Remarks'], resultkey="drclookupdf"))

    drcoutput = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='drclookupdf',
                                                filterConditions=[
                                                    # "df=df[['RRN','Trx Date','Amount','PAN']]",
                                                        "df['Trx Date']=df['Trx Date'].dt.strftime('%Y-%m-%d')",
                                                        "df.loc[df['Self Matched']=='MATCHED','flag']='DRC-102'",
                                                        "df.loc[(df['Self Matched']=='MATCHED')&(df['NPCI Match']=='UNMATCHED')&(df['Remarks']=='ReverseEntryPosted'),'flag']='DRC-103'",
                                                        "df.loc[(df['Self Matched']=='')&(df['NPCI Match']==''),'flag']='DRC-104'",
                                                        "df['filename']='DRC'+df['Trx Date']",
                                                        "df.loc[df['flag']=='DRC-102','reason']='Customer account has been reversed online'",
                                                        "df.loc[df['flag']=='DRC-103','reason']='Customer account has been reversed manually'",
                                                        "df.loc[df['flag']=='DRC-104','reason']='Customer account has not been debited'",
                                                        "df['shser']=df['RRN']",
                                                        "df.rename(columns={'RRN':'Bankadjref','Trx Date':'shtdat','Amount':'adjamt','PAN':'shcrd'},inplace=True)",
                                                        "df=df[['Bankadjref','flag','shtdat','adjamt','shser','shcrd','filename','reason']]"

                                                ],
                                                sourceTag="DRC")], resultKey='drcoutput',
                                  writeToFile=True,
                                  reportName='DRC_Output'))


    # elem12 = dict(type="ExpressionEvaluator", properties=dict(source="results.NPCI",
    #                                                           expressions=["df['CBS Match']=='MATCHED'"],resultkey='switchNPCI'))
    # elem13 = dict(type="ExpressionEvaluator", properties=dict(source="results.CBS",
    #                                                           expressions=["df['NPCI Match']=='MATCHED'"],
    #                                                           resultkey='switchCBS'))



    elem13= dict(type='VLookup',
                  properties=dict(data='switch_duplicate', lookup='results.NPCI', dataFields=['GATEWAY RRN','AMOUNT'],
                                  lookupFields=['Ref Number','Transaction Amount'],
                                  # lookupFieldsFilter=["df=df[df['CBS Match']=='MATCHED']"],
                                  markers={'NPCI':'YES'}, resultkey="switchlook_up"))

    elem14= dict(type='VLookup',
                  properties=dict(data='switchlook_up', lookup='results.CBS', dataFields=['GATEWAY RRN','AMOUNT'],
                                  lookupFields=['RRN','AMOUNT'],
                                  # lookupFieldsFilter=["df=df[df['NPCI Match']=='MATCHED']"],
                                  markers={'CBS': 'YES'},
                                  resultkey="switchlook_up"))

    elem15 = dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='switchlook_up',
                                                filterConditions=[
                                                    # "df=df[(df['NPCI']=='YES')&(df['CBS']=='YES')]"
                                                ],
                                                sourceTag="SWITCH")], resultKey='switchlook_up',
                                  writeToFile=True,
                                  reportName='Switch status in NPCI and CBS'))

    # acqtimeout_filters = ["df = df[df['Transaction Amount'] > 0]",
    #                 "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
    #                 "df = df[df['Response Code'] == '50']"]

    elem16 = dict(type="ExpressionEvaluator", properties=dict(source="npcitotal",
                     expressions=["df = df[(df['Response Code'] == '50')& (df['FEED_FILE_NAME'].str.startswith('ISS'))]"
                                  ],resultkey='isstimeoutdf'))
    #
    # isstimeout_filters = ["df = df[df['Transaction Amount'] > 0]",
    #                       "df.loc[:,'Transaction Amount'] = df['Transaction Amount'].astype(float) / 100",
    #                       "df = df[df['Response Code'] == '08']"]

    elem17 = dict(type="ExpressionEvaluator", properties=dict(source="npcitotal",
                     expressions=["df = df[(df['Response Code'] == '08')& (df['FEED_FILE_NAME'].str.startswith('ACQ'))]"
                                  ],resultkey='acqtimeoutdf'))
    elem18 = dict(type="SourceConcat",
                  properties=dict(sourceList=['isstimeoutdf', 'acqtimeoutdf'], resultKey="npcitimeoutdf"))

    elem19=dict(type='VLookup',
                  properties=dict(data='npcitimeoutdf', lookup='results.CBS', dataFields=['Ref Number','Transaction Amount'],
                                  lookupFields=['RRN','AMOUNT'],
                                  lookupFieldsFilter=["df=df[(df['Self Matched']=='')&(df['NPCI Match']=='UNMATCHED')]"],markers={'CBS': 'YES'},
                                  resultkey="npcitimeoutdf"))

    elem20 = dict(type='VLookup',
                  properties=dict(data='npcitimeoutdf', lookup='results.CBS',
                                  dataFields=['Ref Number', 'Transaction Amount'],
                                  lookupFields=['RRN', 'AMOUNT'],
                                  lookupFieldsFilter=["df=df[(df['Self Matched']=='MATCHED')]"],markers={'Auto_Reversal': 'YES'},
                                  resultkey="npcitimeoutdf"))

    elem21= dict(type='ReportGenerator',
                  properties=dict(sources=[dict(source='npcitimeoutdf',sourceTag="NPCICBS")], resultKey='npcitimeout',
                                  writeToFile=True,
                                  reportName='TimeOut'))

    elem22= dict(type='ReportGenerator',
                  properties=dict(sources=[
                      dict(source='custom_reports.Switch status in NPCI and CBS', filterConditions=["df=df[df['VENDOR NAME']=='INTEGRA']",
                        "df=df[df['VENDOR_RESPONSE'].fillna('')=='']"],
                           sourceTag="SWITCH"),
                      dict(source='switchdf', filterConditions=["df=df[df['VENDOR NAME']=='INTEGRA']",
                     "df=df[(df['VENDOR_RESPONSE'].fillna('')=='00')&(df['VENDOR_RESPONSE_TIME'].fillna('')=='')]"],
                           sourceTag="SWITCH")
                  ], resultKey='switchfailed',
                      writeToFile=True,
                      reportName='TimeoutCreditAdjustment'))


    integrareport=dict(type='ReportGenerator',
                  properties=dict(sources=[
                      dict(source='switchtotal', filterConditions=[
                          # "df=df.merge(payload['npcidf'][['Ref Number']],left_on=['VENDOR RRN'],right_on=['Ref Number'],indicator=True)",
                          "df=df[(df['VENDOR NAME']=='INTEGRA')&(df['VENDOR_REQUEST']=='0')&(df['VENDOR_RESPONSE']=='00')]",
                          "df['InterchangeFee']=df['AMOUNT']*0.05/100",
                          "df['InterchangeFeeGST']=df['InterchangeFee']*0.18",
                          "df['SwitchingFee']=0.25",
                          "df['SwitchingFeeGST']=df['SwitchingFee']*0.18",

                        # "df=df[df['VENDOR_RESPONSE'].fillna('')=='']"
                      ],
                           sourceTag="SWITCH")
                  ], resultKey='integra',
                      writeToFile=True,
                      reportName='SwitchIntegra'))

    mbsreport = dict(type='ReportGenerator',
                         properties=dict(sources=[
                             dict(source='custom_reports.Switch status in NPCI and CBS', filterConditions=[
                                 # "df=df.merge(payload['npcidf'][['Ref Number']],left_on=['VENDOR RRN'],right_on=['Ref Number'],indicator=True)",
                                 "df=df[(df['VENDOR NAME']=='MBS')&(df['VENDOR_REQUEST']=='0')&(df['VENDOR_RESPONSE']=='00')]",
                                 "df['InterchangeFee']=df['AMOUNT']*0.05/100",
                                 "df['InterchangeFeeGST']=df['InterchangeFee']*0.18",
                                 "df['SwitchingFee']=0.25",
                                 "df['SwitchingFeeGST']=df['SwitchingFee']*0.18",


                                 # "df=df[df['VENDOR_RESPONSE'].fillna('')=='']"
                             ],
                                  sourceTag="SWITCH")
                         ], resultKey='integra',
                             writeToFile=True,
                             reportName='SwitchMBS'))



    elem23 = dict(type="GenerateReconSummary",
                  properties=dict(
                      sources=[dict(resultKey="NPCI", sourceTag="NPCI", aggrCol=['Transaction Amount']),
                               dict(resultKey="CBS", sourceTag="CBS", aggrCol=['AMOUNT'])]))

    elem24=dict(type='VLookup',
                  properties=dict(data='results.CBS', lookup='npcitotal', dataFields=['RRN','AMOUNT'],
                                  lookupFields=['Ref Number','Transaction Amount'],includeCols=['Response Code'],
                                  resultkey="results.CBS"))

    elem25= dict(type="DumpData", properties=dict(dumpPath='AEPS Inward Recon', matched='any'))
    gen_meta_info = dict(type='GenerateReconMetaInfo')

    elements = [elem1, elem2,elem4,drop_npci_duplicates, elem5,switchdf,drop_switch_duplicates,elem6,drop_cbs_duplicates,save_current_disputes,elem7,elem8,
                elem9,elem10,elem11,elem12,carryforward,drcload,drclookupcbs,drcoutput,
                elem13,elem14,elem15,elem16,elem17,elem18,elem19,elem20,elem21,elem22,integrareport,mbsreport,elem23,elem24]
    settlement = SettlementReport.Settelement()
    elements1=[elem25,gen_meta_info]
    elements.extend(settlement)
    elements.extend(elements1)
    f = FlowRunner(elements)
    f.run()
