from datetime import datetime
from pandas.api.types import *
from pymongo import MongoClient
import numpy as np
import pandas as pd
class Utilities:
    def __init__(self):
        pass

    def getNextSeq(self):
        mongo_db = MongoClient('localhost')['erecon']
        sequenceGenObj = mongo_db["sequenceGenerator"].find_one({"seqName": "reconExecutionSeq"})
        executionId = int(sequenceGenObj['seq'])
        mongo_db["sequenceGenerator"].update({"seqName": "reconExecutionSeq"},
                                             {"$set": {"seq": executionId + 1}})

        return str(executionId)

    def getUtcTime(self, convertDate=None, resetSeconds=False):
        if convertDate is None:
            convertDate = datetime.utcnow()
        if resetSeconds:
            convertDate = convertDate.replace(second=0)

        ts = int(datetime.now().replace(second=0).strftime("%s"))
        utc_offset = datetime.fromtimestamp(ts) - datetime.utcfromtimestamp(ts)
        return int(convertDate.now().strftime("%s")) - utc_offset.seconds

    def getPreviousJobDoc(self, reconId):
        mongo_db = MongoClient('localhost')['erecon']
        doc = mongo_db['recon_execution_details_log'].find({'RECON_ID': reconId,
                                                            'PROCESSING_STATE_STATUS': 'Matching Completed'}).sort(
            [('RECON_EXECUTION_ID', -1)])
        doc = list(doc)
        if doc is not None and len(doc):
            doc = doc[0]
        if len(doc) == 0:
            doc = None
        return doc

    def getValues(self, obj={}, ref='', sep='.', default=None):
        for key in ref.split(sep):
            if obj is not None and key in obj.keys():
                obj = obj[key]
            elif default is not None:
                return default
            else:
                raise KeyError('Key "%s" not found in input object' % key)
        return obj

    def setValues(self, obj={}, ref='', set_value='', sep='.'):
        keys = ref.split(sep)
        count =1
        for x in keys:
            if count<=len(keys)-1:
                if x in obj:
                    continue
                else:
                    obj[x]={}
                count+=1
        if keys:
            exec "obj['" + "']['".join(keys) + "'] = set_value"
        else:
            raise KeyError('Ref keys cannot be empty %s' % ref)

        return obj

    def infer_dtypes(self, series):
        if is_float_dtype(series):
            return 0.0
        elif is_integer_dtype(series):
            return 0
        elif is_string_dtype(series):
            return ''
        else:
            return pd.NaT
