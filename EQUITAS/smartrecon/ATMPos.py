from FlowElements import FlowRunner

# Tips and Summary - All Disputes
# Refunds - All Disputes
# Total of Approved Transaction
# Switching fee - 0.60 * No of Approved Transactions
# Interchange income - From DSR summary
# Refund fee - From DSR summary



if __name__ == "__main__":
    stmtdate = "08-Feb-2018"
    basePath = "/Users/shivashankar/Desktop/ReconData/Equitas/Raw Data"
    prefixes = {"EANFZ": "ISSUER", "EAEQU": "ACQUIRER", "EANFY": "POS"}
    posswitchfilter = ['df["TRANSACTIONTYPE"]=df["SOURCE"].apply(lambda x: x[:5],"")',
                    'df=df.loc[df["BENEFICIARYCODE"]=="EQU"]',
                    'df=df.loc[(df["TXN_AMOUNT"]!=0) & (df["RESPONSECODE"]=="00")]',
                    'df["RRNISSUER"]=df["RRNISSUER"].astype(np.int64)',
                    'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)',
                    'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)'
                     ]  # ,'df["TXN_AMOUNT"]=df["TXN_AMOUNT"]/100.0'
    inititalizer = dict(id=1, next=2, type="Initializer",
                        properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="ATM", compressionType="", resultkey='',ignoredb=True))
    params = {}
    params["feedformatFile"] = "Equitas_ATM_SWITCH_Structure.csv"

    switchdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="Switch",
                                                        feedPattern="^EANFY.*\.txt",
                                                        feedParams=params,
                                                        feedFilters=posswitchfilter,
                                                        resultkey="switchposdf"))

    cbsfilters = ['df.dropna(subset=["RRN"],inplace=True)','df=df.loc[df["GLCODE"]=="208100034"]',
                  'df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)', 'df.loc[df["TXN_AMOUNT"]<0,"CR_DR_IND"]="D"','df["RRN"].fillna(0,inplace=True)','df["RRN"]=df["RRN"].astype(np.int64)',
                    'df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)','df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)']
    params = {}
    params["feedformatFile"] = "Equitas_ATM_HOST_Structure.csv"
    cbsdata = dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="CBS",
                                                                   feedPattern="^H2H.*\.txt",
                                                                   feedParams=params,
                                                                   feedFilters=cbsfilters,
                                                                   resultkey="cbsdf"))



    posdata=dict(type="PreLoader", properties=dict(loadType="FixedFormat", source="861Pos",
                                                                 feedPattern="861ES*.*.*",
                                                                 feedParams=dict(feedformatFile="861_NPCI_RUPAY_STMT_STRUCT.csv",skiprows=1),
                                                                 feedFilters=['df["TXN_AMOUNT"]=df["TXN_AMOUNT"].astype(np.float64)','df["RRN"].fillna(0,inplace=True)','df["RRN"]=df["RRN"].astype(np.int64)','df["CARD_NUMBER"]=df["CARD_NUMBER"].astype(str)'],
                                                                 resultkey="npciposdf"))

    selfmatch =dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                          columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     amountColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     crdrcolumn=[
                                                                                                         "CR_DR_IND"],
                                                                                                     CreditDebitSign=True),
                                                                          sourceTag="CBS")], matchResult="iiscbsdf"))

    params = {}
    #params["feedformatFile"] = "NPCI_ATM_DISPUTES_FORMAT.csv"
    params["skipfooter"]=1
    disputes=dict(type="PreLoader", properties=dict(loadType="CSV", source="Disputes",
                                                                 feedPattern="^All_Disputes*.*",
                                                                 feedParams=params,
                                                                 feedFilters=['df["Dispute Amount"]=df["Dispute Amount"].astype(np.float64)'],
                                                                 resultkey="disputesdf"))


    threewaymatch = dict(type="NWayMatch", properties=dict(sources=[dict(source="switchposdf",
                                                                               columns=dict(keyColumns=["RRNISSUER", "CARD_NUMBER"],
                                                                                    sumColumns=["TXN_AMOUNT"],
                                                                                    matchColumns=["RRNISSUER", "CARD_NUMBER",
                                                                                                  "TXN_AMOUNT"]),
                                                                               sourceTag="Switch"), dict(source="iiscbsdf.CBS",
                                                                                                 columns=dict(
                                                                                                     keyColumns=["RRN",
                                                                                                                 "CARD_NUMBER"],
                                                                                                     sumColumns=[
                                                                                                         "TXN_AMOUNT"],
                                                                                                     matchColumns=[
                                                                                                         "RRN",
                                                                                                         "CARD_NUMBER",
                                                                                                         "TXN_AMOUNT"]),
                                                                                                 sourceTag="CBS"),
                                                                          dict(source="npciposdf",
                                                                       columns=dict(
                                                                           keyColumns=["RRN",
                                                                                       "CARD_NUMBER"],
                                                                           sumColumns=[
                                                                               "TXN_AMOUNT"],
                                                                           matchColumns=[
                                                                               "RRN",
                                                                               "CARD_NUMBER",
                                                                               "TXN_AMOUNT"]),
                                                                       sourceTag="NPCIPOS")],
                                                                 matchResult="results"))



    elements = [inititalizer, cbsdata,switchdata,posdata, selfmatch, threewaymatch,disputes]
    f = FlowRunner(elements)
    f.run()
    df=f.result['disputesdf'][["Function Code","Function Code and Description","Dispute Amount"]]
    df=df.groupby(["Function Code","Function Code and Description"]).sum().reset_index()
    print df
    npcipos=f.result['results']["NPCIPOS"]
    reversal=npcipos[(npcipos["ACQCODE"]=="D") & (npcipos['CBS Match']=="MATCHED")][["TXN_AMOUNT","CARD_NUMBER"]]
    print "Reversal Transactions"
    print reversal
    cbsamount=npcipos[(npcipos["ACQCODE"]=="A") & (npcipos["CBS Match"]=="MATCHED")]
    npciamount=npcipos[(npcipos["ACQCODE"]=="A")]
    print "Approved Transactions"
    print npciamount["TXN_AMOUNT"].sum(),npciamount["TXN_AMOUNT"].count()
    print "Mismatch NPCI to CBS", npciamount["TXN_AMOUNT"].sum()-cbsamount["TXN_AMOUNT"].sum(), npciamount["TXN_AMOUNT"].count()-cbsamount["TXN_AMOUNT"].count()

    postcutoff=npcipos[(npcipos["ACQCODE"]=="A") & (npcipos["CBS Match"]=="UNMATCHED")]
    postcutoff= postcutoff[["RRN","CARD_NUMBER","TXN_AMOUNT"]]
    postcutoff["TXN_AMOUNT"]=postcutoff["TXN_AMOUNT"]/100
    postcutoff["RRN"]=postcutoff["RRN"].astype(str)
    print postcutoff
    print postcutoff["TXN_AMOUNT"].sum()

    print "Switching Transaction Amount"
    print npciamount["TXN_AMOUNT"].count()*0.60

    declined=npcipos[(npcipos["ACQCODE"] == "D")]
    print "Declined Transactions"
    print declined["TXN_AMOUNT"].sum(), declined["TXN_AMOUNT"].count()





    f.result['switchposdf'].to_csv("Output/switchposdf.csv", index=False)
    f.result['cbsdf'].to_csv("Output/cbsdf.csv", index=False)
    f.result['npciposdf'].to_csv("Output/npciacqdf.csv", index=False)
    for x in f.result['results'].keys():
        f.result['results'][x].to_csv("Output/" + x + ".csv")