import json
import pymongo
from pymongo import MongoClient
import pandas
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class ReconStatusGenerator():
    def __init__(self):
        self.client = MongoClient()
        self.client = MongoClient('localhost', 27017)
        self.db = self.client['erecon']
        self.jobs = self.db['jobs']
        self.statementdate=""





    def generateReport(self):
        recons=[]
        for job in self.jobs.find({"stmtDate":self.statementdate}):
            x={}
            x["Recon ID"]=job["reconId"]
            x["Statement Date"]=job["stmtDate"]
            sourcesys=""
            filteredcount=""
            totalcount=""
            for source in job["sources"].values():
                if len(sourcesys)>0:
                    sourcesys+="/"
                    filteredcount+"/"
                    totalcount + "/"
                sourcesys+=source["sourceName"]
                filteredcount=str(source["filteredCount"])
                totalcount=str(source["rawInputCount"])
            x["Source Systems"]=sourcesys
            x["Records Loaded"]=totalcount
            x["Records Matched"] = source["matchedCount"]
            x["Records Unmatched"] = source["unmatchedCount"]
            x["Records Filtered"]=filteredcount
            x["Status"]=source["JobStatus"]
            x["Remark"]=source.get("errorMsg","")
            recons.append(x)
        df=pandas.from_dict(recons)
        html=df.to_html()









if __name__ =="__main__":
    f=open("output.json")
    data=json.loads(f.read())
    print type(data)