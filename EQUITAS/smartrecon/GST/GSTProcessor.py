import pandas as pd
import numpy as np
import os
import fnmatch
import csv
from dateutil.parser import *
import json
from copy import deepcopy
import datetime
import xlrd

class GSTProcessor():
    def __init__(self,path,statementDate):
        self.basepath="/Users/shivashankar/Downloads/GSTTest/"
        self.filepath=self.basepath+path+"/"+statementDate+"/"
        self.outputPath="/Users/shivashankar/Downloads/GSTTest/output"
        self.statementDate=statementDate
        self.outputdir = self.outputPath + self.statementDate + '/'
        if not os.path.exists(self.outputdir):
            os.makedirs(self.outputdir)
        self.sourceprefixes=["BSM","CMS","NOVOLOAN","RETAIL","TECH1","WBO","RSYS","MTF","INDUS","CBS","TECH","TIPLUS","NOVOPAYMENTS"]
        self.manualfiles={"TI+ CLF manual  ":{"filter":['df["STATECODE"]=df["Reason for Crdit/debit note"].str[0:2]']},
                          "tech excel":{"header":None,"filter":['df["STATECODE"]=df[9].str[0:2]','df["TAX VALUE"]=df[17]+df[18]+df[19]','df["MULTIPLIER"]=1','df["MULTIPLIER"][df[8]=="R"]=-1','df["TAX VALUE"]=df["TAX VALUE"]*df["MULTIPLIER"]','df["CHARGE VALUE"]=df[14]*df["MULTIPLIER"]','df["STATEMENT DATE"]=df[12]']},
                          "tech excel1":{"header":None,"filter":['df["STATECODE"]=df[5]','df["TAX VALUE"]=df[8]+df[9]+df[10]','df["MULTIPLIER"]=1','df["TAX VALUE"]=df["TAX VALUE"]*df["MULTIPLIER"]','df["CHARGE VALUE"]=df[6]*df["MULTIPLIER"]','df["STATEMENT DATE"]=self.statementDate']},
                          "wbo cms":{"filter":['df["STATECODE"]=df["Source Branch State "]']},
                          "BB onboarding":{"filter":['df["STATECODE"]=df["Source Branch State "]']},
                          "wbo trade": {"filter": ['df["STATECODE"]=df["Source Branch State "]']},
                          "wbo lending": {"filter": ['df["STATECODE"]=df["Source Branch State "]']},
                          #"branch control": {"filter": ['df["STATECODE"]=df["Source Branch State "]']},
                          "pos-fd and retail-retail npci": {"filter": ['df["STATECODE"]=df["Source Branch State "]']}}#,"dimts":"","tech excel":"","tech excel1":"","pos-fd and retail-retail npci":"","wbo trade":"","BB onboarding":"","branch control":"","wbo lending":"","wbo cms":""}
        self.feedstruct={}
        self.loadFeedStructures()
        self.data={}
        self.ogldata=None
        self.branchcodes = pd.read_csv("Reference/GeneralStateCode.csv")
        self.statecodes = self.branchcodes[["STATECODE", "STATENAME"]].drop_duplicates()
        self.tistatecode= pd.read_csv("Reference/TI+StateCode.csv")[["STATECODE", "STATENAME"]].drop_duplicates()
        self.sourceproductgroup = pd.read_csv("Reference/SourceProductGroup.csv", delimiter='|')
        self.columnnamechanges = pd.read_csv("Reference/MappedColumnlist.txt")
        self.sourceprefixes=self.columnnamechanges["SOURCE"].unique().tolist()
        print self.sourceproductgroup["SOURCE"].unique().tolist()
        print self.sourceprefixes
        self.finaldata={}
        self.totaldata={}
        self.template={"STATE CODE": 0, "TAX": 0.00, "STATE NAME": "", "SOURCE": "", "TRANSACTION COUNT": 0, "STATEMENT DATE": statementDate}
        self.globalframe=pd.DataFrame()

    def loadFeedStructures(self):
        with open('Reference/ProductFeedStruct.txt', 'r') as csvfile:
            cfile = csv.reader(csvfile, delimiter='|')
            for line in cfile:
                # print line
                self.feedstruct[line[0]] = line[3:]

    def recursive_glob(self, rootdir='.', pattern='*'):
        """Search recursively for files matching a specified pattern.

        Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
        """
        matches = []
        for root, dirnames, filenames in os.walk(rootdir):
            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
        return matches

    def loadManualFiles(self, fileName):
        for x in self.manualfiles.keys():
            if "header" in self.manualfiles[x]:
                header = self.manualfiles[x]["header"]
            else:
                header = 0
            df = pd.read_excel(self.filepath + "/" + fileName, x, squeeze=True, header=header)
            df["SOURCE NAME"] = "CBS"
            df = df.fillna(0)
            if "filter" in self.manualfiles[x]:
                for f in self.manualfiles[x]["filter"]:
                    # df["STATECODE"] = df["Reason for Crdit/debit note"].str[0:2]
                    exec (f)
            self.processFrame(df, self.tistatecode, x, "CBS")

    def processFrame(self, df, stateCode, source, destsource):
        df["STATECODE"] = df["STATECODE"].astype(int)
        df = df.merge(stateCode, how="left", on=["STATECODE"])
        columns = self.columnnamechanges[self.columnnamechanges["SOURCE"] == source.strip()]
        srccolumns = columns["RAWDATACOL"].tolist()
        mappedcols = columns["MAPPEDCOL"].tolist()
        renamedict = {}
        for i in range(0, len(srccolumns)):
            renamedict[srccolumns[i]] = mappedcols[i]
        # for x in srccolumns:
        print df.columns
        df = df[srccolumns]
        df = df.rename(columns=renamedict)
        df["STATEMENT DATE"] = df["STATEMENT DATE"].apply(lambda x: parse(str(x)).strftime("%Y%m%d"))
        df["SOURCE"] = source
        df["OGLSOURCE"] = destsource
        self.globalframe = self.globalframe.append(df, ignore_index=True)

    def loadFiles(self):
        files = self.recursive_glob(self.filepath, "*.txt")
        for selectedfile in files:
            print selectedfile
            if os.path.basename(selectedfile.lower()).startswith("egl_gl"):
                self.ogldata = pd.read_csv(selectedfile, delimiter='|')
                self.ogldata = self.ogldata.merge(self.branchcodes, how="left", on=["BRANCH"])
                print self.ogldata.columns
                accounts = pd.read_csv("Reference/Natural Accounts OGL Filter.csv")
                values = accounts["NATURAL_AC"].unique()
                self.ogldata = self.ogldata[self.ogldata["NATURAL_AC"].isin(values)]
                source = "OGL"
                columns = self.columnnamechanges[self.columnnamechanges["SOURCE"] == source]
                srccolumns = columns["RAWDATACOL"].tolist()
                mappedcols = columns["MAPPEDCOL"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                self.ogldata = self.ogldata[srccolumns]
                self.ogldata = self.ogldata.rename(columns=renamedict)
                self.ogldata.to_csv("OGLEorig.csv")
                continue
            source = None
            for x in self.sourceprefixes:
                if os.path.basename(selectedfile.lower()).startswith(x.lower()):
                    source = x
                    if source == "MTF":
                        source = "TIPLUS"
                    break
            # print source,selectedfile
            if source:
                df = pd.read_csv(selectedfile, names=self.feedstruct[source], delimiter='|', skiprows=1,
                                 skipfooter=1)
                df.to_csv(self.outputdir + source + "_orig.csv")

                df["SOURCE"] = source
                print(len(df["SOURCE"]))
                df = df.merge(self.sourceproductgroup, how="left", on=["SOURCE"])
                print(len(df["SOURCE"]))
                if source.lower().startswith("ti"):
                    df = df.rename(columns={'sourceState': 'STATECODE'})
                    self.processFrame(df, self.tistatecode, source, source)
                    # df = df.merge(self.tistatecode, how="left", on=["STATECODE"])
                elif "sourceState" in list(df.columns.values):
                    df = df.rename(columns={'sourceState': 'STATECODE'})
                    self.processFrame(df, self.statecodes, source, source)
                    # df = df.merge(self.statecodes, how="left", on=["STATECODE"])

    def computeGST(self):
        df = self.globalframe[
            ["STATE NAME", "STATEMENT DATE", "CHARGE VALUE", "TAX VALUE", "SOURCE", "OGLSOURCE"]].groupby(
            ["SOURCE", "OGLSOURCE", "STATEMENT DATE", "STATE NAME"]).agg(['sum', 'count']).reset_index()
        # df.rename(columns={"SOURCE NAME":"SOURCE"},inplace=True)
        print df.columns
        df["TAX"] = df["TAX VALUE"]['sum']
        df["CHARGE"] = df["CHARGE VALUE"]['sum']
        df["TRANSACTION COUNT"] = df["TAX VALUE"]['count']
        # df["SOURCE"] = source
        df = df[["STATE NAME", "STATEMENT DATE", "TAX", "CHARGE", "TRANSACTION COUNT", "SOURCE", "OGLSOURCE"]]
        df['TAX'] = df['TAX'].apply(lambda x: round(x, 2))
        df['CHARGE'] = df['CHARGE'].apply(lambda x: round(x, 2))
        df.columns = df.columns.droplevel(level=1)
        data = json.loads(df.to_json(orient='records'))
        for x in data:
            if x["STATEMENT DATE"] not in self.finaldata:
                self.finaldata[x["STATEMENT DATE"]]={}
            if x['STATE NAME'] not in self.finaldata[x["STATEMENT DATE"]]:
                self.finaldata[x["STATEMENT DATE"]][x['STATE NAME']] = []
            self.finaldata[x["STATEMENT DATE"]][x['STATE NAME']].append(x)
        # df=df["".groupby(["STATECODE"])
        # self.data[source] = df
        # print source
        print df.to_csv(self.outputdir + "finalvalues.csv")
        # print "_________________________________________________________________"

    def mapState(self):
        self.ogldata = self.ogldata[["SOURCECODE", "STATE CODE", "STATE NAME", "TAX VALUE", "STATEMENT DATE"]]
        self.data=self.sourceproductgroup[["SOURCECODE","SourceName"]].drop_duplicates()
        self.ogldata = self.ogldata.merge(self.data, how="left", on="SOURCECODE")
        self.ogldata.to_csv("Beforemerge.csv")
        self.ogldata = self.ogldata.rename(columns={"SourceName": "MATCHSOURCE"})
        self.ogldata["STATEMENT DATE"] = self.ogldata["STATEMENT DATE"].apply(
            lambda x: parse(str(x)).strftime("%Y%m%d"))
        self.ogldata = self.ogldata.groupby(["MATCHSOURCE", "STATE CODE", "STATE NAME", "STATEMENT DATE"]).agg(
            ['sum', 'count']).reset_index()
        # print self.ogldata.columns
        self.ogldata["TAX"] = self.ogldata["TAX VALUE"]['sum']
        self.ogldata["TRANSACTION COUNT"] = self.ogldata["TAX VALUE"]['count']
        self.ogldata = self.ogldata[["MATCHSOURCE", "STATE NAME", "TAX", "TRANSACTION COUNT", "STATEMENT DATE"]]

        self.ogldata["TAX"] = self.ogldata["TAX"].apply(lambda x: round(x, 2))
        self.ogldata.columns = self.ogldata.columns.droplevel(level=1)
        self.ogldata.to_csv(self.outputdir + "OGL.csv")
        self.ogldata["SOURCE"] = "OGL"
        # self.ogldata.drop_
        print self.ogldata.columns
        data = json.loads(self.ogldata.to_json(orient='records'))
        for x in data:
            if x["STATEMENT DATE"] not in self.finaldata:
                self.finaldata[x["STATEMENT DATE"]]={}
            if x['STATE NAME'] not in self.finaldata[x["STATEMENT DATE"]]:
                self.finaldata[x["STATEMENT DATE"]][x['STATE NAME']] = []
            self.finaldata[x["STATEMENT DATE"]][x['STATE NAME']].append(x)

        for x in self.finaldata.keys():
            if not x in self.totaldata:
                self.totaldata[x] = []
            for items in self.finaldata[x].values():
                for val in items:
                    autogen = deepcopy(self.sourceprefixes)
                    if val["SOURCE"] == 'OGL':
                        self.totaldata[val["STATEMENT DATE"]].append(val)
                        continue
                    if val["SOURCE"] in autogen:
                        autogen.remove(val["SOURCE"])
                        self.totaldata[val["STATEMENT DATE"]].append(val)
                for src in autogen:
                    if src == "OGL":
                        continue
                    data = deepcopy(self.template)
                    data["SOURCE"] = src
                    data["STATE NAME"] = val["STATE NAME"]
                self.totaldata[val["STATEMENT DATE"]].append(data)

        # update mongodb with this
        for key in self.totaldata.keys():
            df=pd.read_json(json.dumps(self.totaldata[key]),orient='records')
            df.to_csv(key+".csv")
            x = {"statementdate": key, "data": self.totaldata[key], "year": int(key[0:4])}
            print json.dumps(x)

    def filterData(self, allowedSources, data):
        filtereddata = []
        for x in data:
            if x["SOURCE"] == "OGL":
                if x["MATCHSOURCE"] in allowedSources:
                    filtereddata.append(x)
            elif x["SOURCE"] in allowedSources:
                filtereddata.append(x)
        return filtereddata

    def getYTD(self, allowedSources, date):
        now = datetime.datetime.now()
        # get all records for this year {"YEAR":now.year,"SOURCE":{$in:allowedSources}}
        records = []
        allrecords = []
        for x in records:
            allrecords.extend(x["data"])
        json.dumps(allrecords)
        df = pd.read_json(json.dumps(allrecords), orient="records")
        df = df["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT"].groupby(
            ["STATE NAME", "SOURCE"]).sum().reset_index()
        return json.loads(df.to_json(orient='records'))

if __name__ == "__main__":
    g = GSTProcessor("", "20171013")
    g.loadManualFiles("WORKING MANUAL.xlsx")
    print("------------------------------------------------------------")
    print g.globalframe
    print("------------------------------------------------------------")
    g.loadFiles()
    g.computeGST()
    g.mapState()
    print g.filterData(["CBS"], g.totaldata["20171013"])
    # print g.ogldata.head(20)





