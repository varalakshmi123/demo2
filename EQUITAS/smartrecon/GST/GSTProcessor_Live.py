import pandas as pd
import numpy as np
import os
import fnmatch
import csv
from dateutil.parser import *
import json
from copy import deepcopy
import datetime
import pandas


class GSTProcessor():
    def __init__(self, path, statementDate):
        self.basepath = config.flaskPath + config.gst_basepath
        self.filepath = self.basepath + path + "/" + statementDate + "/"
        self.outputPath = self.filepath + "output"
        self.statementDate = statementDate
        self.outputdir = self.outputPath + self.statementDate + '/'
        if not os.path.exists(self.outputdir):
            os.makedirs(self.outputdir)
        self.sourceprefixes = ["BSM", "CMS", "NOVOLOAN", "RETAIL", "TECH1", "WBO", "RSYS", "MTF", "INDUS", "CBS",
                               "TECH", "TIPLUS"]
        self.feedstruct = {}
        self.loadFeedStructures()
        self.data = {}
        self.ogldata = None
        self.branchcodes = pandas.read_csv(self.basepath + "/Reference/GeneralStateCode.csv")
        self.statecodes = self.branchcodes[["STATECODE", "STATENAME"]].drop_duplicates()
        self.tistatecode = pandas.read_csv(self.basepath + "/Reference/TI+StateCode.csv")[
            ["STATECODE", "STATENAME"]].drop_duplicates()
        self.sourceproductgroup = pandas.read_csv(self.basepath + "/Reference/SourceProductGroup.csv",
                                                  delimiter='|')
        self.columnnamechanges = pandas.read_csv(self.basepath + "/Reference/MappedColumnlist.txt")
        self.sourceprefixes = self.columnnamechanges["SOURCE"].unique().tolist()
        self.finaldata = {}
        self.totaldata = []
        self.template = {"STATE CODE": 0, "TAX": 0.00, "STATE NAME": "", "SOURCE": "", "TRANSACTION COUNT": 0,
                         "STATEMENT DATE": statementDate}
        self.sourcewise={}

    def loadFeedStructures(self):
        logger.info(self.basepath + '/Reference/ProductFeedStruct.txt')
        with open(self.basepath + '/Reference/ProductFeedStruct.txt', 'r') as csvfile:
            cfile = csv.reader(csvfile, delimiter='|')
            for line in cfile:
                # print line
                self.feedstruct[line[0]] = line[3:]

    def recursive_glob(self, rootdir='.', pattern='*'):
        """Search recursively for files matching a specified pattern.

        Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
        """
        matches = []
        for root, dirnames, filenames in os.walk(rootdir):
            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
        return matches

    def loadFiles(self):
        files = self.recursive_glob(self.filepath, "*.txt")
        for selectedfile in files:
            logger.info(selectedfile)
            if os.path.basename(selectedfile.lower()).startswith("egl_gl"):
                self.ogldata = pandas.read_csv(selectedfile, delimiter='|')
                self.ogldata = self.ogldata.merge(self.branchcodes, how="left", on=["BRANCH"])
                print self.ogldata.columns
                accounts = pandas.read_csv(self.basepath + "/Reference/Natural Accounts OGL Filter.csv")
                values = accounts["NATURAL_AC"].unique()
                self.ogldata = self.ogldata[self.ogldata["NATURAL_AC"].isin(values)]
                source = "OGL"
                columns = self.columnnamechanges[self.columnnamechanges["SOURCE"] == source]
                srccolumns = columns["RAWDATACOL"].tolist()
                mappedcols = columns["MAPPEDCOL"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                self.ogldata = self.ogldata[srccolumns]
                self.ogldata = self.ogldata.rename(columns=renamedict)
                continue
            source = None
            for x in self.sourceprefixes:
                if os.path.basename(selectedfile.lower()).startswith(x.lower()):
                    source = x
                    break
            # print source,selectedfile
            if source:
                df = pandas.read_csv(selectedfile, names=self.feedstruct[source], delimiter='|', skiprows=1, skipfooter=1)

                df["SOURCE"] = source
                print(len(df["SOURCE"]))
                df = df.merge(self.sourceproductgroup, how="left", on=["SOURCE"])
                print(len(df["SOURCE"]))
                if source.startswith("TI"):
                    df = df.rename(columns={'sourceState': 'STATECODE'})
                    df = df.merge(self.tistatecode, how="left", on=["STATECODE"])
                elif "sourceState" in list(df.columns.values):
                    df = df.rename(columns={'sourceState': 'STATECODE'})
                    df = df.merge(self.statecodes, how="left", on=["STATECODE"])
                print(len(df["SOURCE"]))
                columns = self.columnnamechanges[self.columnnamechanges["SOURCE"] == source]
                srccolumns = columns["RAWDATACOL"].tolist()
                mappedcols = columns["MAPPEDCOL"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                # for x in srccolumns:
                df = df[srccolumns]
                df = df.rename(columns=renamedict)
                df["STATEMENT DATE"] = df["STATEMENT DATE"].apply(lambda x: parse(str(x)).strftime("%Y%m%d"))
                # df["YEAR"]=df["STATEMENT DATE"].apply(lambda x:parse(str(x)).year)
                df = df[["STATE NAME", "STATEMENT DATE", "CHARGE VALUE", "TAX VALUE"]].groupby(
                    ["STATEMENT DATE", "STATE NAME"]).agg(['sum', 'count']).reset_index()
                print df.columns
                df["TAX"] = df["TAX VALUE"]['sum']
                df["CHARGE"] = df["CHARGE VALUE"]['sum']
                df["TRANSACTION COUNT"] = df["TAX VALUE"]['count']
                df["SOURCE"] = source
                df = df[["STATE NAME", "STATEMENT DATE", "TAX", "CHARGE", "TRANSACTION COUNT", "SOURCE"]]
                df['TAX'] = df['TAX'].apply(lambda x: round(x, 2))
                df['CHARGE'] = df['CHARGE'].apply(lambda x: round(x, 2))
                df.columns = df.columns.droplevel(level=1)
                data = json.loads(df.to_json(orient='records'))
                for x in data:
                    if x['STATE NAME'] not in self.finaldata:
                        self.finaldata[x['STATE NAME']] = []
                    self.finaldata[x['STATE NAME']].append(x)
                # df=df["".groupby(["STATECODE"])
                # self.data[source] = df
                # print source
                # print df.to_csv(self.outputdir+source+".csv")
                # print "_________________________________________________________________"

            # for key,val in self.data.items():
            #    print "GST",key, val.head(5)

    def mapState(self):
        self.ogldata = self.ogldata[["SOURCECODE", "STATE CODE", "STATE NAME", "TAX VALUE", "STATEMENT DATE"]]
        self.ogldata = self.ogldata.merge(self.sourceproductgroup, how="left", on="SOURCECODE")
        print self.ogldata.columns
        self.ogldata = self.ogldata.rename(columns={"SOURCE": "MATCHSOURCE"})
        self.ogldata["STATEMENT DATE"] = self.ogldata["STATEMENT DATE"].apply(
            lambda x: parse(str(x)).strftime("%Y%m%d"))
        self.ogldata = self.ogldata.groupby(["MATCHSOURCE", "STATE CODE", "STATE NAME", "STATEMENT DATE"]).agg(
            ['sum', 'count']).reset_index()
        # print self.ogldata.columns
        self.ogldata["TAX"] = self.ogldata["TAX VALUE"]['sum']
        self.ogldata["TRANSACTION COUNT"] = self.ogldata["TAX VALUE"]['count']
        self.ogldata["SOURCE"] = "OGL"
        self.ogldata = self.ogldata[
            ["MATCHSOURCE", "STATE NAME", "TAX", "TRANSACTION COUNT", "SOURCE", "STATEMENT DATE"]]
        self.ogldata["TAX"] = self.ogldata["TAX"].apply(lambda x: round(x, 2))
        self.ogldata.columns = self.ogldata.columns.droplevel(level=1)
        data = json.loads(self.ogldata.to_json(orient='records'))

        for x in data:
            if x['STATE NAME'] not in self.finaldata:
                self.finaldata[x['STATE NAME']] = []

            self.finaldata[x['STATE NAME']].append(x)


        for x in self.finaldata.keys():
            autogen = deepcopy(self.sourceprefixes)
            for val in self.finaldata[x]:
                if val["SOURCE"] == 'OGL':
                    self.totaldata.append(val)
                    continue
                if val["SOURCE"] in autogen:
                    autogen.remove(val["SOURCE"])
                    self.totaldata.append(val)
            for src in autogen:
                if src == "OGL":
                    continue
                data = deepcopy(self.template)
                data["SOURCE"] = src
                data["STATE NAME"] = val["STATE NAME"]
                self.totaldata.append(data)



        # update mongodb with this
        #doc = {"statementdate": datetime.datetime.strptime(self.statementDate, '%Y%m%d'),
        #       "data": self.totaldata, "year": self.statementDate[:4]}
        #status, data = GstReports().create(doc)
        #return doc

    def filterData(self, allowedSources, data):
        filtereddata = []
        for x in data:
            if x["SOURCE"] == "OGL":
                if x["MATCHSOURCE"] in allowedSources:
                    filtereddata.append(x)
            elif x["SOURCE"] in allowedSources:
                filtereddata.append(x)
        return filtereddata

    def queryYTDData(self, statementDate):
        records = []
        condition = {"year": str(datetime.datetime.now().year),
                     "statementdate":{"$lte":statementDate}}
        records = GstReports().find(condition)

        logger.info(records.count())
        allrecords = []
        if records.count() > 0:
            for record in records:
                allrecords.extend(record['data'])
        else:
            pass
        return allrecords

    def getYTD(self, allowedSources, stmtDate):
        allrecords = self.queryYTDData(stmtDate)
        if allrecords:
            allrecords = self.filterData(allowedSources, allrecords)
            df = pandas.read_json(json.dumps(allrecords), orient="records")
            df = df[["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT"]].groupby(
                ["STATE NAME", "SOURCE"]).sum().reset_index()

            return df.to_dict(orient='records')
        else:
            return []

    def getStateWiseGst(self, gstReportData):
        df_all = pandas.DataFrame(gstReportData)
        df_ogl_data = df_all[(df_all['SOURCE'] == 'OGL')]
        matchSrcList = df_ogl_data['MATCHSOURCE'].unique().tolist()
        sourceBrkList = []
        for src in matchSrcList:
            obj = dict()
            obj['reportName'] = 'OGL v/s ' + src
            df_ogl = df_ogl_data[df_ogl_data['MATCHSOURCE'] == src]
            df_oth = df_all[df_all['SOURCE'] == src]
            df_merge = df_ogl.merge(df_oth, on='STATE NAME', suffixes=('', '_'))
            df_merge['DIFFERENCE'] = df_merge['TAX'] + df_merge['TAX_']
            df_merge = df_merge[['STATE NAME', 'TAX', 'TAX_', 'DIFFERENCE']]
            df_merge.rename(columns = {"STATE NAME":"AGG_COLUMN"}, inplace = True)

            df_total = pandas.DataFrame(
                [{"AGG_COLUMN": "Total", 'TAX': df_merge['TAX'].sum(), 'TAX_': df_merge['TAX_'].sum(),
                  'DIFFERENCE': df_merge['DIFFERENCE'].sum()}])
            df_merge = df_merge.append(df_total)
            df_merge = df_merge.round(2)

            obj['subcolumns'] = ['State Name', 'OGL Tax', src + ' Tax', 'Difference']
            obj['data'] = df_merge.to_dict(orient='records')
            sourceBrkList.append(obj)
        return sourceBrkList

    def groupGstByState(self, gstReportData):
        obj = dict()
        resp = []
        gstReportData = pandas.DataFrame(gstReportData)
        df_ogl = gstReportData[(gstReportData['SOURCE'] == 'OGL')]
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]
        df_other = df_other.groupby('STATE NAME').sum()['TAX'].reset_index()
        df_ogl = df_ogl.groupby('STATE NAME').sum()['TAX'].reset_index()
        df_merge = df_ogl.merge(df_other, on='STATE NAME', suffixes=('', '_'), how='outer')
        df_merge['DIFFERENCE'] = df_merge['TAX'] - df_merge['TAX_']
        df_merge.rename(columns = {"STATE NAME":"AGG_COLUMN"}, inplace = True)
        df_merge[['TAX', 'TAX_']] = df_merge[['TAX', 'TAX_']].fillna(0.0)
        df_merge['DIFFERENCE'] = df_merge['TAX'] - df_merge['TAX_']

        df_total = pandas.DataFrame(
            [{"AGG_COLUMN": "Total", 'TAX': df_merge['TAX'].sum(), 'TAX_': df_merge['TAX_'].sum(),
              'DIFFERENCE': df_merge['DIFFERENCE'].sum()}])
        df_merge = df_merge.append(df_total)
        df_merge = df_merge.round(2)

        obj['reportName'] = 'State wise GST Balances'
        obj['subcolumns'] = ['State Name', 'OGL Balance', 'Extract Balance', 'Difference']
        obj['data'] = df_merge.to_dict(orient='records')
        resp.append(obj)
        return resp

    def groupGstBySystem(self, gstReportData):
        obj = dict()
        resp = []
        gstReportData = pandas.DataFrame(gstReportData)
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]
        df_ogl = gstReportData[(gstReportData['SOURCE'] == 'OGL')]
        df_other = df_other.groupby('SOURCE').sum()['TAX'].reset_index()
        df_ogl = df_ogl.groupby('MATCHSOURCE').sum()['TAX'].reset_index()

        df_merge = df_ogl.merge(df_other, left_on='MATCHSOURCE', right_on = 'SOURCE', suffixes=('', '_'), how='outer')
        df_merge = df_merge[['SOURCE', 'TAX', 'TAX_']]
        df_merge.rename(columns={"SOURCE": "AGG_COLUMN"}, inplace=True)
        df_merge[['TAX', 'TAX_']] = df_merge[['TAX', 'TAX_']].fillna(0.0)
        df_merge['DIFFERENCE'] = df_merge['TAX'] - df_merge['TAX_']

        df_total = pandas.DataFrame([{"AGG_COLUMN":"Total", 'TAX':df_merge['TAX'].sum(), 'TAX_':df_merge['TAX_'].sum(),
                                     'DIFFERENCE': df_merge['DIFFERENCE'].sum()}])
        df_merge = df_merge.append(df_total)
        df_merge = df_merge.round(2)

        obj['reportName'] = 'System wise GST Balances'
        obj['subcolumns'] = ['System', 'OGL Balance', 'Extract Balance', 'Difference']
        obj['data'] = df_merge.to_dict(orient='records')
        resp.append(obj)
        return resp

    def gstReportByTotal(self, gstReportData):
        gstReportData = pandas.DataFrame(gstReportData)
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]
        df_group = df_other[["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT", "CHARGE"]].groupby(
            ["STATE NAME"]).sum().reset_index().round(2)
        df_group['SOURCE'] = 'IDFC Bank Total'
        df_total = df_other.append(df_group, ignore_index = True)
        df_total.fillna('', inplace = True)
        return df_total.to_dict(orient = 'records')