from FlowElements import FlowRunner
import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner

if __name__ == "__main__":
    # stmtdate = "24-Jan-2018"
    stmtdate=sys.argv[1]
    uploadFileName=sys.argv[2]
    basePath = "/usr/share/nginx/smartrecon/mft/"

    sfms_filters = ['df=df.dropna(how="all",subset=["S.No"])', 'df["ReconName"]="NEFTOUTWARD"']
    cbs_filters = ['df["Tran Remarks"]=df["Tran Remarks"].str.strip(" ")',
                   'df=df[df["Tran Remarks"].str.contains("N06:|N07:",regex=True)]',
                   'df["Transaction Ref"]=df["Tran Remarks"].apply(lambda x: x.split(":")[1])',
                   "df.loc[df['Tran Remarks'].str.count(':')>2,'HasReversal']='Reversal'",
                   'df["S_TransactionRef"]=df["Tran Remarks"].apply(lambda x: x.split(":")[2] if x.count(":")>1 else "")',
                   'df["ReconName"]="NEFTOUTWARD"',
                   'df["CR_DR"]=df["DR-CR Ind"]',
                   'df["Amount1"]=df["Amount"]',
                   'df["Amount1"]=df.apply(lambda x:x["Amount"]*-1 if x["CR_DR"]=="D" else x["Amount"],axis=1)',
                   'df["CR_DR"]=df.apply(lambda x:"D" if (x["Amount1"]<0 )else "C",axis=1)',
                   'df["Amount1"]=df["Amount"].abs()']

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",uploadFileName=uploadFileName,
                                 recontype="NEFT_SYNDICATE", compressionType="zip", resultkey='',
                                 reconName='NEFTOutwardSyndicate'))
    params = {}
    params["feedformatFile"] = "NEFTSYNDICATEOutward.csv"
    # params["columnNames"] ='S.No,Seq No,Transaction Ref,Amount (Rs.),Value Date,Batch Id,Sending Branch,Sender A/c Type,Sender A/c No,Sender A/c Name,Benf. Branch,Benf A/c Type,Benf. A/c No,Benf. A/c Name,Txn. Status,Originator of Remittance,Sender To Receiver Information'
    params["columnNames"] = 'IFSC,Name,A/C Number,IFSC,Name,A/C Number'

    params['FooterKey'] = "STR: Sent To RBI       RAR : Rescheduled At RBI       RAS : Rescheduled At SC       RVD: Rescheduled Value Date       RBT: Rescheduled Batch Time"

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern=".*?_Complete.csv",
                                                                 feedParams=dict(feedformatFile='CBS_NEFT.csv',
                                                                                 skiprows=1),
                                                                 resultkey="allcbsdf"))


    exp1=dict(type='ExpressionEvaluator',properties=dict(source='allcbsdf',expressions=cbs_filters,resultkey='cbsdf'))


    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="OW_Complete_Transaction_Report_SFMS.xls",
                                                                 feedParams=params,
                                                                 feedFilters=sfms_filters,
                                                                 resultkey="sfmsdf"))

    SFMSDuplicates = dict(type="FilterDuplicates",
                          properties=dict(source="sfmsdf", inplace=True, allowOneInMatch=True,
                                          duplicateStore='duplicateStore',
                                          keyColumns=["Transaction Ref"], sourceTag="SFMS", markerCol='Duplicate',
                                          resultKey="sfmsdf"))

    CBSDuplicates = dict(type="FilterDuplicates",
                         properties=dict(source="cbsdf", inplace=True, allowOneInMatch=True,
                                         duplicateStore='duplicateStore',
                                         keyColumns=["Transaction Ref"], sourceTag="CBS", markerCol='Duplicate',
                                         resultKey="cbsdf"))

    pmtable = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="PMTABLE",
                                                                   feedPattern="Outward*",disableCarryFwd=True,
                                                                   feedParams=dict(
                                                                       feedformatFile='PmTableStructure.csv',
                                                                       skiprows=1),
                                                                   resultkey="pmtdf"))

    vlookup_reversal = dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='allcbsdf', dataFields=['Transaction Ref'],
                                            lookupFields=['Transaction Ref'], markers=dict(MarkMacthed='MATCHED'),
                                            dataFieldsFilter=["df=df[df['HasReversal']=='Reversal']","df=df[df['Tran Remarks'].str.contains('N07:',regex=True)]"],
                                            lookupFieldsFilter=[
                                                "df=df[df['Tran Remarks'].str.contains('N02:',regex=True)]",
                                                "df['Transaction Ref']=df['Tran Remarks'].apply(lambda x: x.split(':')[2] if x.count(':')>1 else '')"],
                                            resultkey="markdf"))

    vlookup_ifsc = dict(type='VLookup',
                        properties=dict(data='cbsdf', lookup='pmtdf', dataFields=['Transaction Ref'],
                                        lookupFields=['TRAN_REF_NO'],
                                        includeCols=['COD_BENF_INST', 'BENF_CUST_NAM', 'COD_BENF_ACCT', 'ORD_INST',
                                                     'COD_ACCT_NAM', 'COD_DR_ACNO', 'TXT_PROCESS_STATUS'],
                                        resultkey="cbsdf"))

    exp2 = dict(type='ExpressionEvaluator', properties=dict(source='cbsdf', expressions=[
                "payload['markdf']=payload['markdf'][payload['markdf']['MarkMacthed']=='MATCHED']",
                "payload['markdf']['S_TransactionRef']=payload['markdf']['Tran Remarks'].apply(lambda x: x.split(':')[2] if x.count(':')>1 else '')",
                "df=df[~df['S_TransactionRef'].isin(payload['markdf']['S_TransactionRef'].unique())]",
                "df['BranchSuspense']=''",
                "payload['markdf']['SFMS Match']='MATCHED'",
                "payload['markdf'].drop(['MarkMacthed'], axis=1, inplace=True)",
                "df.drop(['HasReversal'],axis=1,inplace=True)",
                "payload['markdf'].drop(['HasReversal'],axis=1,inplace=True)",
                "payload['markdf']['BranchSuspense']='N07Reversal-BranchSuspense'"
                ], resultkey='cbsdf'))

    # rename = dict(type='RenameColumns', properties=dict(source='cbsdf', columns={"COD_BENF_INST": "Benf_Branches",
    #                                                                              'ORD_INST': "Sending_Branch",
    #                                                                              'COD_ACCT_NAM': "Sender_A/c_Name",
    #                                                                              'COD_DR_ACNO': "Sender_A/c_No",
    #                                                                              'BENF_CUST_NAM': "Benf_A/c_Name",
    #                                                                              'COD_BENF_ACCT': "Benf_A/c_No"},
    #                                                     resultkey="cbsdf"))

    reversal = dict(keyColumns=["Transaction Ref"], amountColumns=["Amount1"],
                    matchColumns=["Transaction Ref"], crdrcolumn=['CR_DR'], CreditDebitSign=True)

    elem7 = dict(id=7, next=8, type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=reversal,
                                               sourceTag="CBS")],
                                 matchResult="results"))


    leftcol = dict(keyColumns=["Transaction Ref", "Amount1", 'COD_BENF_INST','ORD_INST'], sumColumns=[],
                   matchColumns=["Transaction Ref", "Amount1", 'COD_BENF_INST','ORD_INST'])

    rightcol = dict(keyColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'], sumColumns=[],
                    matchColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'])




    elem4 = dict(id=4, next=7, type="NWayMatch",
                 properties=dict(sources=[dict(source="results.CBS",
                                               columns=leftcol,
                                               sourceTag="CBS", subsetfilter=["df=df[(df['Self Matched']!='MATCHED')]"]),
                                          dict(source="sfmsdf",
                                               columns=rightcol, sourceTag="SFMS")],
                                 matchResult="results"))


    SourceConcat=dict(type='SourceConcat',properties=dict(sourceList=['markdf','results.CBS'],resultKey='results.CBS'))



    # LoadExternalReport=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTInwardSyndicate',filepath='OUTPUT/CBS_UnMatched.csv',resultKey='cbsunmatchdf'))
    #
    # removeunwantedcolumns=dict(type='ExpressionEvaluator',properties=dict(source='cbsunmatchdf',expressions=['df.drop(["SFMS CarryForward","SFMS Match","Self Matched"], axis=1, inplace=True)'],resultkey='cbsunmatchdf'))

    leftcol = dict(keyColumns=["S_TransactionRef", "Amount1"], sumColumns=[],
                   matchColumns=["S_TransactionRef", "Amount1"])

    rightcol = dict(keyColumns=["Transaction Ref", "Amount"], sumColumns=[],
                   matchColumns=["Transaction Ref", "Amount"])

    # N02_N07=dict(type="NWayMatch",
    #              properties=dict(sources=[dict(source="results.CBS",
    #                                            columns=leftcol,
    #                                            sourceTag="CBS", subsetfilter=["df=df[(df['SFMS Match']!='MATCHED') & (df['Self Matched']!='MATCHED')]"]),
    #                                       dict(source="cbsunmatchdf",
    #                                            columns=rightcol, sourceTag="FLEX")],
    #                              matchResult="results"))

    # flexfilter=dict(type='ExpressionEvaluator',properties=dict(source='results.FLEX',expressions=["df.loc[df['CBS Match']=='MATCHED','SFMS Match']='MATCHED'"],resultkey='results.FLEX'))
    #
    # cbsfilter=dict(type='ExpressionEvaluator',properties=dict(source='results.CBS',expressions=["df.loc[df['FLEX Match']=='MATCHED','SFMS Match']='MATCHED'"],resultkey='results.CBS'))

    filter1 = ["df['Count'] =1",
               'df = df[df["Tran Remarks"].str.contains("N06:")]',
               "df['Account Description']='Outward'",
               "df=df.groupby(['Account Description','CR_DR'])['Count','Amount1'].sum().reset_index()",
               "df.rename(columns={'Amount1':'Amount'},inplace=True)"
               ]

    filter2 = ["df['Count'] =1",
               'df = df[df["Tran Remarks"].str.contains("N07:")]',
               "df['Account Description']='Return'",
               "df=df.groupby(['Account Description','CR_DR'])['Count','Amount1'].sum().reset_index()",
               "df.rename(columns={'Amount1':'Amount'},inplace=True)"
               ]

    report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="results.CBS", sourceTag='CBS',
                                                                        filterConditions=filter1),
                                                                   dict(source="results.CBS", sourceTag='CBS',
                                                                        filterConditions=filter2)
                                                                   ],
                                                          reportName='SyndicateReport',writeToMongo=True,writeToFile=True))

    BranchSuspense = dict(type='ReportGenerator',
                              properties=dict(sources=[dict(source='markdf')],
                                              writeToMongo=False,
                                              writeToFile=True, reportName='BranchSuspense'))

    elem8 = dict(type='GenerateReconSummary',
                  properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount1'],matched='any'),
                                          dict(resultKey="SFMS", sourceTag='SFMS',
                                               aggrCol=['Amount'])]))


    elem9 = dict(id=9, type='DumpData', properties=dict(dumpPath='NEFTOutwardSyndicate', matched='any'))

    metainfo=dict(type='GenerateReconMetaInfo',properties=dict())

    elements = [elem1, elem2,exp1,elem3,SFMSDuplicates,CBSDuplicates,pmtable,vlookup_reversal,vlookup_ifsc,exp2,elem7,elem4,SourceConcat,report,BranchSuspense,elem8,elem9,metainfo]
    # elements = [elem1,elem3]

    f = FlowRunner(elements)
    f.run()
    # for x in f.result['results'].keys():
    #     f.result['results'][x].to_csv("/home/kedarnath/Desktop" + x + ".csv")
