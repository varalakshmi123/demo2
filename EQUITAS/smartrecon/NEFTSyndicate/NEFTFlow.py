import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    # stmtdate = "24-Jan-2018"
    stmtdate=sys.argv[1]
    uploadFileName=sys.argv[2]
    basePath = "/usr/share/nginx/smartrecon/mft/"

    sfms_filters = ['df=df.dropna(how="all",subset=["S.No"])','df["ReconName"]="NEFTINWARD"']
    # cbs_filters = ['df["V_TXN_DESC"]=df["V_TXN_DESC"].apply(lambda x: x.split(":")[-1])']
    cbs_filters = ['df["Tran Remarks"]=df["Tran Remarks"].str.strip(" ")',
                   'df=df[df["Tran Remarks"].str.contains("N02:")]',
                   "df.loc[df['Tran Remarks'].str.count(':')>1,'HasReversal']='Reversal'",
                   'df["Transaction Ref"]=df["Tran Remarks"].apply(lambda x: x.split(":")[1])',
                   'df["S_TransactionRef"]=df["Tran Remarks"].apply(lambda x: x.split(":")[2] if x.count(":")>1 else "")',
                   'df["ReconName"]="NEFTINWARD"']

    # cbs_fil = [
    #     "df=df[( (df['FORACID'].fillna('').str.lstrip('0').str.endswith('1061657016')) | (df['FORACID'].fillna('').str.lstrip('0').str.endswith('1025398006')))]",
    #     "df = df[(df['TRAN_PARTICULARS'] != 'NEFT') & (df['TRAN_RMKS'] != 'NEFT SETTLEMENT') & (df['DEBIT_AMT'] != 0)]",
    #     r"df['Transaction Ref']=df['TRAN_RMKS'].apply(lambda x:x.split('\\')[0])",
    #     "df['Transaction Ref']=df['Transaction Ref'].str.strip(' ')", "df['INDICATOR']='D'"]

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="NEFT_SYNDICATE", compressionType="zip",uploadFileName=uploadFileName, resultkey='',reconName='NEFTInwardSyndicate'))

    params = {}
    params["feedformatFile"] = "NEFTSyndicateInward.csv"
    params["columnNames"] = "IFSC,Name,A/C Number,IFSC,Name,A/C Number"
    params['FooterKey'] = "STB:  Sent To Branch"


    elem2 = dict(id=2,next=3,type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern=".*?_Complete.csv",
                                                                 feedParams=dict(
                                                                     feedformatFile='CBS_NEFT.csv',skiprows=1),
                                                                 resultkey="allcbsdf"))


    exp1=dict(type='ExpressionEvaluator',properties=dict(source='allcbsdf',expressions=cbs_filters,resultkey='cbsdf'))


    # elem3=dict(id=3,type='Splitdf',properties=dict(keycolumn='INDICATOR',source='cbsdf1'))
    #
    # elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
    #                                                              feedPattern=".*?_Complete.csv",
    #                                                              feedParams=dict(
    #                                                                  feedformatFile='CBS_NEFT.csv', skiprows=1),
    #                                                              feedFilters=cbs_filters,
    #                                                              resultkey="cbsdf2"))


    # elem4=dict(id=4,type='SourceConcat',properties=dict(sourceList=['cbsdf1','cbsdf2'],resultkey='cbsdf'))
    #
    # elem2=dict(id=2,type='MultiLoader',properties=[dict(loadType="CSV", source="CBS",
    #                                                              feedPattern=".*?_Complete.csv",
    #                                                              feedParams=dict(
    #                                                                  feedformatFile='CBS_NEFT.csv',skiprows=1),
    #                                                              feedFilters=cbs_filters,
    #                                                              resultkey="cbsdf"),
    #                                                       dict(loadType="CSV", source="CBS",
    #                                                            feedPattern="sol_[0-9]{4}_[0-9]{4}_1061657016_#%d%m%Y#_[0-9]{3}.lst",
    #                                                            feedParams=dict(feedformatFile='CBS_ANDHRA.csv',
    #                                                                            delimiter='|', skiprows=1),feedFilters=cbs_fil,resultkey="cbsdf")])
    elem3 = dict(id=3, next=4, type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="IW_Complete_Transaction_Report_SFMS.xls",
                                                                 feedParams=params,
                                                                 feedFilters=sfms_filters,
                                                                  resultkey="sfmsdf"))

    SFMSDuplicates = dict(type="FilterDuplicates",
                 properties=dict(source="sfmsdf", inplace=True, allowOneInMatch=True,duplicateStore='duplicateStore',
                                 keyColumns=["Transaction Ref"], sourceTag="SFMS",markerCol='Duplicate',resultKey="sfmsdf"))

    CBSDuplicates = dict(type="FilterDuplicates",
                 properties=dict(source="cbsdf", inplace=True, allowOneInMatch=True,duplicateStore='duplicateStore',
                           keyColumns = ["Transaction Ref"], sourceTag = "CBS",markerCol='Duplicate',resultKey = "cbsdf"))

    # reference=dict(type="ReferenceLoader",properties=dict(loadType="CSV",
    #                                                       fileName=["Reference/Inward_23Jan2018_PMTable.csv"],
    #                                                              resultkey="pmtdf"))



    pmtable = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="PMTABLE",
                                                                 feedPattern="Inward_*",disableCarryFwd=True,
                                                                   feedParams=dict(
                                                                       feedformatFile='PmTableStructure.csv', skiprows=1),
                                                                 resultkey="pmtdf"))


    vlookup_reversal=dict(type='VLookup',
                            properties=dict(data='cbsdf', lookup='allcbsdf', dataFields=['Transaction Ref'],
                                            lookupFields=['Transaction Ref'],markers=dict(MarkMacthed='MATCHED'),
                                            dataFieldsFilter=["df=df[df['HasReversal']=='Reversal']"],
                                            lookupFieldsFilter=["df=df[df['Tran Remarks'].str.contains('N07:',regex=True)]","df['Transaction Ref']=df['Tran Remarks'].apply(lambda x: x.split(':')[2] if x.count(':')>1 else '')"],
                                            resultkey="markdf"))



    vlookup_ifsc = dict(type='VLookup',
                        properties=dict(data='cbsdf', lookup='pmtdf', dataFields=['Transaction Ref'],
                                        lookupFields=['TRAN_REF_NO'],
                                        includeCols=['COD_BENF_INST', 'BENF_CUST_NAM', 'COD_BENF_ACCT', 'ORD_INST',
                                                     'COD_ACCT_NAM', 'COD_DR_ACNO', 'TXT_PROCESS_STATUS'],
                                        resultkey="cbsdf"))


    # rename=dict(type='RenameColumns',properties=dict(source='cbsdf',columns={"COD_BENF_INST":"Benf_Branches",'ORD_INST':"Sending_Branch",'COD_ACCT_NAM':"Sender_A/c_Name",'COD_DR_ACNO':"Sender_A/c_No",'BENF_CUST_NAM':"Benf_A/c_Name",'COD_BENF_ACCT':"Benf_A/c_No"},resultkey="cbsdf"))

    exp2=dict(type='ExpressionEvaluator',properties=dict(source='cbsdf',expressions=["payload['markdf']=payload['markdf'][payload['markdf']['MarkMacthed']=='MATCHED']",
                                                                                     "payload['markdf']['S_TransactionRef']=payload['markdf']['Tran Remarks'].apply(lambda x: x.split(':')[2] if x.count(':')>1 else '')",
                                                                                     "df=df[~df['S_TransactionRef'].isin(payload['markdf']['S_TransactionRef'].unique())]",
                                                                                     "df['BranchSuspense']=''",
                                                                                     "payload['markdf']['SFMS Match']='MATCHED'",
                                                                                     "payload['markdf'].drop(['MarkMacthed'], axis=1, inplace=True)",
                                                                                     "df.drop(['HasReversal'],axis=1,inplace=True)",
                                                                                     "payload['markdf'].drop(['HasReversal'],axis=1,inplace=True)",
                                                                                     "payload['markdf']['BranchSuspense']='N07Reversal-BranchSuspense'"
                                                                                     ],resultkey='cbsdf'))

    reversal = dict(keyColumns=["Transaction Ref"], amountColumns=["Amount"],
                    matchColumns=["Transaction Ref"], crdrcolumn=['DR-CR Ind'], CreditDebitSign=True)


    elem7 = dict(id=7, next=8, type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=reversal,
                                               sourceTag="CBS")],
                                 matchResult="results"))


    leftcol = dict(keyColumns=["Transaction Ref", "Amount", 'COD_BENF_INST','ORD_INST'], sumColumns=[],
                   matchColumns=["Transaction Ref", "Amount", 'COD_BENF_INST','ORD_INST'])

    rightcol = dict(keyColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'], sumColumns=[],
                    matchColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'])


    elem4 = dict(id=4, next=7, type="NWayMatch",
                 properties=dict(sources=[dict(source="results.CBS",
                                               columns=leftcol,
                                               sourceTag="CBS", subsetfilter=["df=df[(df['Self Matched']!='MATCHED')]"]),
                                          dict(source="sfmsdf",
                                               columns=rightcol, sourceTag="SFMS")],
                                 matchResult="results"))

    duplicate = dict(type="DuplicateMatch",
                     properties=dict(sources=[dict(source="results.CBS",
                                                   columns=dict(
                                                       keyColumns=["Transaction Ref"],
                                                       sumColumns=["Amount"],
                                                       matchColumns=["Transaction Ref", "Amount"]),
                                                   subsetfilter=["df=df[df['Duplicate']=='DUPLICATED']"],
                                                   sourceTag="CBS"),
                                              dict(source="results.SFMS", columns=dict(
                                                  keyColumns=["Transaction Ref"],
                                                  sumColumns=["Amount"],
                                                  matchColumns=["Transaction Ref", "Amount"]),
                                                   subsetfilter=["df=df[df['Duplicate']=='DUPLICATED']"],
                                                   sourceTag="SFMS")],
                                     matchResult="results"))

    leftcol = dict(keyColumns=["Transaction Ref", "Amount", 'COD_BENF_INST','ORD_INST'], sumColumns=[],
                   matchColumns=["Transaction Ref", "Amount", 'COD_BENF_INST','ORD_INST'])

    rightcol = dict(keyColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'], sumColumns=[],
                    matchColumns=["Transaction Ref", "Amount", 'Benf. Branch','Sending Branch'])

    SourceConcat=dict(type='SourceConcat',properties=dict(sourceList=['markdf','results.CBS'],resultKey='results.CBS'))

   



   
    filter1=[
             "df['Count']=1",
             "df['Account Description']='Inward'",
             "df=df.groupby(['Account Description','DR-CR Ind'])['Count','Amount'].sum().reset_index()",
             "df.rename(columns={'DR-CR Ind':'CR_DR'},inplace=True)"
            ]

    report = dict(type="ReportGenerator", properties=dict(sources=[dict(source="results.CBS", sourceTag='CBS',
                                                                             filterConditions=filter1)],reportName='SyndicateReport',writeToMongo=True,writeToFile=True))

    BranchSuspense = dict(type='ReportGenerator',
                          properties=dict(sources=[dict(source='markdf')],
                                          writeToMongo=False,
                                          writeToFile=True, reportName='BranchSuspense'))

    elem8 = dict(type='GenerateReconSummary',
                 properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                          dict(resultKey="SFMS", sourceTag='SFMS',
                                               aggrCol=['Amount'])]))



    elem9 = dict(id=9, type='DumpData', properties=dict(dumpPath='NEFTInwardSyndicate', matched='all'))

    metainfo=dict(type='GenerateReconMetaInfo',properties=dict())


    elements = [elem1, elem2,exp1,elem3,SFMSDuplicates,CBSDuplicates,pmtable,vlookup_reversal,vlookup_ifsc,exp2,elem7,elem4,duplicate,SourceConcat,report,BranchSuspense,elem8,elem9,metainfo]
    # elements = [elem1,reference]

    f = FlowRunner(elements)
    f.run()

