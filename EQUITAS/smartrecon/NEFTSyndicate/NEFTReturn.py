from FlowElements import FlowRunner

if __name__ == "__main__":
    stmtdate = "05-Dec-2017"
    basePath = "/usr/share/nginx/bnarecon/mft/"

    sfms_filters = ['df=df.dropna(how="all",subset=["S.No"])','df["ReconName"]="NEFTRETURN"']
    cbs_filters = ['df["Tran Remarks"]=df["Tran Remarks"].str.strip(" ")','df=df[df["Tran Remarks"].str.contains("N07:")]','df["Transaction Ref"]=df["Tran Remarks"].apply(lambda x: x.split(":")[1])','df["ReconName"]="NEFTRETURN"']

    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="NEFT", compressionType="zip", resultkey='',reconName='NEFTReturnSyndicate'))
    params = {}
    params["feedformatFile"] = "NEFT_Return_Structure.csv"
    params['FooterKey'] = "No of Transactions"
    params["columnNames"] ='S.No,Seq No,Transaction Ref,Related Reference,Amount (Rs.),Value Date,Batch Id,Sending Branch,Sender A/c Type,Sender A/c No,Sender A/c Name,Benf. Branch,Benf A/c Type,Benf. A/c No,Benf. A/c Name,Txn. Status,Return Code,Return Resason,Originator of Remittance,Sender To Receiver Information'
    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="CBS",
                                                                 feedPattern=".*?_Complete.csv",
                                                                 feedParams=dict(
                                                                     feedformatFile='CBS_NEFT.csv',skiprows=1),
                                                                 feedFilters=cbs_filters,
                                                                 resultkey="cbsdf"))

    elem3 = dict(id=3,next=4,type="PreLoader", properties=dict(loadType="SFMSMultiSheet", source="SFMS",
                                                                 feedPattern="SFMS_Outward.xls",
                                                                 feedParams=params,
                                                                 feedFilters=sfms_filters,
                                                                 resultkey="sfmsdf"))

    leftcol = dict(keyColumns=["Transaction Ref","Amount"], sumColumns=[],
                   matchColumns=["Transaction Ref", "Amount"])

    rightcol = dict(keyColumns=["Transaction Ref","Amount"], sumColumns=[],
                    matchColumns=["Transaction Ref", "Amount"])

    elem4 = dict(id=4, next=7, type="NWayMatch",
                 properties=dict(sources=[dict(source="cbsdf",
                                               columns=leftcol,
                                               sourceTag="CBS"),
                                          dict(source="sfmsdf",
                                               columns=rightcol, sourceTag="SFMS")],
                                 matchResult="results"))

    elem7 = dict(type='GenerateReconSummary',
                 properties=dict(sources=[dict(resultKey="CBS", sourceTag='CBS', aggrCol=['Amount']),
                                          dict(resultKey="SFMS", sourceTag='SFMS',
                                               aggrCol=['Amount'])]))

    elem8 = dict(id=8, type='DumpData', properties=dict(dumpPath='NEFTReturnSyndicate'))

    elements = [elem1, elem2, elem3, elem4, elem7, elem8]
    f = FlowRunner(elements)
    f.run()
