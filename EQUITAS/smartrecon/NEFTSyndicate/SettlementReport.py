import sys
sys.path.append('/usr/share/nginx/smartrecon')
from FlowElements import FlowRunner


if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName=sys.argv[2]

    basePath = "/usr/share/nginx/smartrecon/mft/"

    elem1 = dict(type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=basePath, outputPath="",
                                 recontype="NEFT_SYNDICATE", compressionType="zip", resultkey='',
                                 reconName='SettlementReport'))

    elem2 = dict(id=2, next=3, type="PreLoader", properties=dict(loadType="CSV", source="PRINCIPLE",
                                                                 feedPattern="170800300_Complete.csv",uploadFileName=uploadFileName,
                                                                 feedParams=dict(feedformatFile='CBS_NEFT.csv',skiprows=1),
                                                                 disableCarryFwd=True,
                                                                 resultkey="pdf"))

    inward = ["df=df[df['Tran Remarks'].str.contains('N02:')]",
        "df['Count']=1",
        "df['Account Description']='InwardSettlement'",
        "df=df.groupby(['Account Description','DR-CR Ind'])['Count','Amount'].sum().reset_index()",
        "df.rename(columns={'DR-CR Ind':'CR_DR'},inplace=True)"
              ]

    outward=["df=df[df['Tran Remarks'].str.contains('N06:')]",
        "df['Count']=1",
        "df['Account Description']='OutwardSettlement'",
        "df=df.groupby(['Account Description','DR-CR Ind'])['Count','Amount'].sum().reset_index()",
        "df.rename(columns={'DR-CR Ind':'CR_DR'},inplace=True)"
             ]

    ret=["df=df[df['Tran Remarks'].str.contains('N07:')]",
        "df['Count']=1",
        "df['Account Description']='ReturnSettlement'",
        "df=df.groupby(['Account Description','DR-CR Ind'])['Count','Amount'].sum().reset_index()",
         "df.rename(columns={'DR-CR Ind':'CR_DR'},inplace=True)"
         ]

    others=["df=df[~df['Tran Remarks'].str.contains('N07:|N06:|N02:')]",
        "df['Count']=1",
        "df['Account Description']='Others'",
        "df=df.groupby(['Account Description','DR-CR Ind'])['Count','Amount'].sum().reset_index()",
        "df.rename(columns={'DR-CR Ind':'CR_DR'},inplace=True)"
            ]

    manualfilters=["df=df[~df['Tran Remarks'].str.contains('N07:|N06:|N02:')]"]



    LoadExternalReport2=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTOutwardSyndicate',filepath='OUTPUT/SyndicateReport_Report.csv',resultKey='outwarddf'))

    LoadExternalReport3=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTInwardSyndicate',filepath='OUTPUT/SyndicateReport_Report.csv',resultKey='inwarddf'))

    LoadExternalReport4=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTInwardSyndicate',filepath='OUTPUT/recon_summary.csv',resultKey='inwardsummarydf'))

    LoadExternalReport5=dict(type='LoadExternalReport',properties=dict(loadType='CSV',reconName='NEFTOutwardSyndicate',filepath='OUTPUT/recon_summary.csv',resultKey='outwardsummarydf'))



    ReportGenerator = dict(type="ReportGenerator", properties=dict(sources=[dict(source="pdf", sourceTag='CBS',
                                                                                       filterConditions=inward),
                                                                                  dict(source="pdf", sourceTag='CBS',
                                                                                       filterConditions=outward),
                                                                                   dict(source="pdf", sourceTag='CBS',
                                                                                       filterConditions=ret),
                                                                            dict(source="pdf", sourceTag='CBS',
                                                                                 filterConditions=others)
                                                                            ],reportName='SettlementReport',keycolumns=['Account Description','CR_DR','Count','Amount'],writeToMongo=True,writeToFile=True))

    SourceConcat=dict(type='SourceConcat',properties=dict(sourceList=['outwarddf','inwarddf','custom_reports.SettlementReport'],resultKey='SettlementReport'))

    ReportGenerator2 = dict(type='ReportGenerator',
                            properties=dict(sources=[dict(source='SettlementReport')], writeToMongo=False, writeToFile=True,
                                            reportName='SettlementReport'))

    inwardSummary = dict(type='ReportGenerator',
                         properties=dict(sources=[dict(source='inwardsummarydf')], writeToMongo=False, writeToFile=True,
                                         reportName='InwardSummary'))

    outwardSummary = dict(type='ReportGenerator',
                          properties=dict(sources=[dict(source='outwardsummarydf')], writeToMongo=False,
                                          writeToFile=True, reportName='OutwardSummary'))

    ManualTransactions = dict(type='ReportGenerator',
                          properties=dict(sources=[dict(source='pdf',filterConditions=manualfilters)], writeToMongo=False,
                                          writeToFile=True, reportName='ManualTransactions'))

    dumpdata = dict(type='DumpData', properties=dict(dumpPath='SettlementReport', matched='all',exportType='singleSheet'))


    elements = [elem1, elem2,LoadExternalReport2,LoadExternalReport3,LoadExternalReport4,LoadExternalReport5,ReportGenerator,SourceConcat,ReportGenerator2,inwardSummary,outwardSummary,ManualTransactions,dumpdata]

    f = FlowRunner(elements)
    f.run()


