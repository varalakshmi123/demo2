import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config

if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    init = dict(type="Initializer",
                properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                uploadFileName=uploadFileName, recontype="JRI", compressionType="zip", resultkey='',
                                reconName="JRI Recon"))

    cbs_filters = ["df = df[df['COD_GL_ACCT'] == '208100096']", "df['REF TXN NO'] = df['REF TXN NO'].str.strip()",
                   "df.loc[df['COD_DRCR'] == 'D','REF TXN NO'] = df['REF TXN NO'].str[:-1]"]

    jri_filters = ["df['System Reference'] = df['System Reference'].str.lstrip(\"'\")"]

    load_cbs = dict(type="PreLoader", properties=dict(loadType='Excel', source="CBS",
                                                      feedParams={"feedformatFile": 'CBS_JRI_Structure.csv',
                                                                  "skiprows": 1},
                                                      feedPattern="GL_Dump*.*xls",
                                                      resultkey="cbsdf", feedFilters=cbs_filters))

    load_jri = dict(type="PreLoader", properties=dict(loadType='HTML', source="JRI",
                                                      feedPattern="TransactionReport*.*xls",
                                                      feedParams=dict(ParseIndex=-1, skiprows=1,
                                                                      feedformatFile='JRI_Structure.csv'),
                                                      feedFilters=jri_filters, resultkey="jridf"))

    cbs_jri_lookup = dict(type='VLookup',
                          properties=dict(data='cbsdf', lookup='jridf', dataFields=['REF TXN NO'],
                                          lookupFields=['System Reference'], includeCols=['Transaction Status'],
                                          resultkey="cbsdf"))

    load_jri_succ = dict(type="ExpressionEvaluator",
                         properties=dict(source="jridf", resultkey="jridf",
                                         expressions=["df = df[df['Transaction Status'] == 'Successful']"]))

    rev_match = dict(type="NWayMatch", properties=dict(sources=[dict(source="cbsdf",
                                                                     columns=dict(
                                                                         keyColumns=["REF TXN NO"],
                                                                         amountColumns=['AMT_TXN_LCY'],
                                                                         crdrcolumn=["COD_DRCR"],
                                                                         CreditDebitSign=False),
                                                                     sourceTag="CBS")], matchResult="cbsresults"))

    nway_match = dict(type="NWayMatch",
                      properties=dict(sources=[dict(source="cbsresults.CBS",
                                                    columns=dict(
                                                        keyColumns=['REF TXN NO', "AMT_TXN_LCY"],
                                                        sumColumns=[],
                                                        matchColumns=['REF TXN NO', "AMT_TXN_LCY"]),
                                                    sourceTag="CBS"),
                                               dict(source="jridf", columns=dict(
                                                   keyColumns=['System Reference', "Amount"], sumColumns=[],
                                                   matchColumns=['System Reference', "Amount"]),
                                                    sourceTag="JRI")],
                                      matchResult="results"))

    jri_remarks = dict(type="AddPostMatchRemarks",
                       properties=dict(source='results.JRI', remarkCol='Recon Remarks',
                                       remarks={"matchRemarks": "System Matched", "selfRemarks": "Reversal"},
                                       how='all'))

    cbs_remarks = dict(type="AddPostMatchRemarks",
                       properties=dict(source='results.CBS', remarkCol='Recon Remarks',
                                       remarks={"matchRemarks": "System Matched", "selfRemarks": "Reversal"},
                                       how='all'))

    unsucc_remarks = dict(type="ExpressionEvaluator",
                          properties=dict(source="results.CBS",
                                          expressions=[
                                              "df.loc[(df['Transaction Status'] == 'Unsuccessful') & "
                                              "(df['Recon Remarks'] != 'Reversal'), 'Recon Remarks'] = 'Unsuccessful'",
                                              "df.loc[(df['Transaction Status'] == 'Unsuccessful') & "
                                              "(df['Recon Remarks'] == 'Reversal'), 'Recon Remarks'] = "
                                              "'Unsuccessful & Auto Reversed'"],
                                          resultkey='results.CBS'))

    gen_unsucc_report = dict(type="ReportGenerator",
                             properties=dict(sources=[dict(source='results.CBS', sourceTag="Transaction Reversed",
                                                           filterConditions=[
                                                               "df = df[df['Transaction Status'] == 'Unsuccessful']"])],
                                             writeToFile=True, reportName='Transaction Reversed'))

    jri_cutoff_marker = dict(type="ExpressionEvaluator",
                             properties=dict(source="results.JRI",
                                             expressions=[
                                                 "df.loc[(df['Order Date'].dt.hour >= 23) & (df['Recon Remarks'] != 'System Matched'), 'Recon Remarks'] = 'Post Cutoff'"],
                                             resultkey='results.JRI'))

    cbs_cutoff_marker = dict(type="ExpressionEvaluator",
                             properties=dict(source="results.CBS",
                                             expressions=[
                                                 "df.loc[(df['DAT_TXN_PROCESSING'].dt.hour >= 23) & (df['Recon Remarks'] != 'System Matched'), 'Recon Remarks'] = 'Post Cutoff'"],
                                             resultkey='results.CBS'))

    jri_summary = dict(type='ReportGenerator', properties=dict(sources=[dict(source='results.JRI', filterConditions=[
        "df['Count'] = 1",
        "df = df[['Recon Remarks','Count','Amount']].groupby(['Recon Remarks'], as_index=False).sum()"])],
                                                               writeToFile=True,
                                                               reportName='JRI Summary'))

    cbs_summary = dict(type='ReportGenerator', properties=dict(sources=[dict(source='results.CBS', filterConditions=[
        "df['Count'] = 1", "df['Amount'] = df['AMT_TXN_LCY']",
        "df = df[['Recon Remarks','Count','Amount']].groupby(['Recon Remarks'], as_index=False).sum()"])],
                                                               writeToFile=True, reportName='CBS Summary'))

    # Discard un-successful transcation from CBS
    load_cbs_succ = dict(type="ExpressionEvaluator",
                         properties=dict(source="results.CBS", resultkey="results.CBS",
                                         expressions=["df = df[df['Transaction Status'] != 'Unsuccessful']"]))

    dump_data = dict(type="DumpData", properties=dict(dumpPath='JRI Recon'))

    gen_meta_info = dict(type="GenerateReconMetaInfo", properties=dict())

    elements = [init, load_jri, load_cbs, cbs_jri_lookup, load_jri_succ, rev_match, nway_match, jri_remarks,
                cbs_remarks, unsucc_remarks, gen_unsucc_report, jri_cutoff_marker, cbs_cutoff_marker, jri_summary,
                cbs_summary, load_cbs_succ, dump_data, gen_meta_info]

    f = FlowRunner(elements)

    f.run()
