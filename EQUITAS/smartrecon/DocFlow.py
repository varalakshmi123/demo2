import sys

sys.path.append('/usr/share/nginx/smartrecon/')
from FlowElements import FlowRunner
import config
import pandas
if __name__ == "__main__":
    stmtdate = sys.argv[1]
    uploadFileName = sys.argv[2]

    df = pandas.read_csv("Reference/DocFlow.csv").fillna('')
    crmcolumns = df["CRM"].tolist()
    wizardcolumns = df["Wizard"].tolist()
    dmscolumns = df["DMS"].tolist()
    elem1 = dict(id=1, next=2, type="Initializer",
                 properties=dict(statementDate=stmtdate, basePath=config.mftpath, outputPath="",
                                 uploadFileName=uploadFileName,
                                 reconName="DocFlow", recontype='DocFlow', compressionType="zip",
                                 resultkey=''))

    DMSloader = dict(type="PreLoader", properties=dict(loadType="CSV", source="DMS",
                                                       feedPattern="^.*DMS.csv",
                                                       feedParams=dict(delimiter=",", skiprows=1,
                                                                   feedformatFile='DMS_Structure.csv'),
                                                       feedFilters=[
                                                           "df['Customer_ID']=df['Customer_ID'].astype(str).str.replace('\.(0)*','')"],
                                                       resultkey="dmsdf"))
    wizardloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="Wizard",
                                                          feedPattern="^.*\wizard.xls",
                                                          feedParams={},
                                                          feedFilters=[
                                                              "df[' Customer ID']=df[' Customer ID'].astype(str).str.replace('\.(0)*','')"],
                                                          resultkey="wizarddf"))
    crmloader = dict(type="PreLoader", properties=dict(loadType="Excel", source="CRM",
                                                       feedPattern="^.*\crm.xls",
                                                       feedParams={},
                                                       feedFilters=[
                                                           "df['CUSTID']=df['CUSTID'].astype(str).str.replace('\.(0)*','')"],
                                                       resultkey="crmdf"))

    matcher = dict(type="MergeAndCompare", properties=dict(sources=[dict(source="dmsdf",
                                                                         columns=dict(
                                                                             keyColumns=["Customer_ID"],
                                                                             matchColumns=dmscolumns),
                                                                         sourceTag="DMS"),
                                                                    dict(source="crmdf",
                                                                         columns=dict(
                                                                             keyColumns=["CUSTID"],
                                                                             matchColumns=crmcolumns),
                                                                         sourceTag="CRM"),
                                                                    dict(source="wizarddf",
                                                                         columns=dict(
                                                                             keyColumns=[" Customer ID"],
                                                                             matchColumns=wizardcolumns),
                                                                         sourceTag="WIZARD")],
                                                           matchResult="results"))

    DMS = dict(type="ExpressionEvaluator", properties=dict(source="results.DMS",
                                                           expressions=[
                                                               "df.loc[(df['CRM Matched Columns'].fillna('') ==''),'CRM Match']='UNMATCHED'",
                                                               "df.loc[(df['CRM Matched Columns'].fillna('') !=''),'CRM Match']='MATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('') ==''),'WIZARD Match']='UNMATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')  !=''),'WIZARD Match']='MATCHED'"
                                                           ], resultkey='results.DMS'))

    CRM = dict(type="ExpressionEvaluator", properties=dict(source="results.CRM",
                                                           expressions=[
                                                               "df.loc[(df['DMS Matched Columns'].fillna('')==''),'DMS Match']='UNMATCHED'",
                                                               "df.loc[(df['DMS Matched Columns'].fillna('')!='') ,'DMS Match']='MATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')==''),'WIZARD Match']='UNMATCHED'",
                                                               "df.loc[(df['WIZARD Matched Columns'].fillna('')!='') ,'WIZARD Match']='MATCHED'"
                                                           ], resultkey='results.CRM'))
    WIZARD = dict(type="ExpressionEvaluator", properties=dict(source="results.WIZARD",
                                                              expressions=[
                                                                  "df.loc[(df['DMS Matched Columns'].fillna('')==''),'DMS Match']='UNMATCHED'",
                                                                  "df.loc[(df['DMS Matched Columns'].fillna('')!='') ,'DMS Match']='MATCHED'",
                                                                  "df.loc[(df['CRM Matched Columns'].fillna('')==''),'CRM Match']='UNMATCHED'",
                                                                  "df.loc[(df['CRM Matched Columns'].fillna('')!=''),'CRM Match']='MATCHED'",
                                                              ], resultkey='results.WIZARD'))

    #
    #
    #
    gen_meta_info = dict(type='GenerateReconMetaInfo')
    elem19 = dict(type="DumpData",
                  properties=dict(dumpPath='DocFlow', matched='any'))
    elements = [elem1, DMSloader, wizardloader, crmloader, matcher, DMS, CRM, WIZARD, gen_meta_info, elem19]
    f = FlowRunner(elements)
    f.run()