#! /bin/bash
echo "please do git clone while doing git clone"
# Installing the nginx, check the status
sudo apt install nginx
sudo apt-get update
service nginx start
service nginx status
# Installing the 3.6 mongodb
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
service mongod start
service mongod status
# Installing the redis-serve
sudo apt-get install redis-server
sudo apt-get install php-redis
sudo apt-get update
# Virutal Env Creation
cd /opt/
mkdir recon_env
chmod -R 777 /opt/recon_env/
cd /opt/recon_env/
# Installing python-pip virtual env
sudo apt install python-pip
pip install virtualenv
cd /opt/recon_env/
# Creating env for the ngerecon/smartrecon(_V2)
virtualenv env
source /opt/recon_env/env/bin/activate
cd /opt/repository/demo2/installationscripts/
cp -r reconpackages.txt /opt/
pip install -r /opt/reconpackages.txt
deactivate
# Creating env for the ereconIdfc/rbeappv5.0
cd /opt/recon_env/
virtualenv erecon_idfc
source /opt/recon_env/erecon_idfc/bin/activate
cd /opt/repository/demo2/installationscripts/
cp -r idfc.txt /opt/
pip install -r /opt/idfc.txt
deactivate
# installing of hashframe package
cd /opt/repository/demo2/
sudo apt-get install unzip
unzip hashframe.zip
cd hashframe
python setup.py install
