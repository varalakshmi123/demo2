#!/bin/bash

cd /usr/share/nginx/

mkdir www
chmod -R 777 .

banks='EQUITAS,SURYO,IDFC,CORP'
folders='ngerecon,erecon,erecon_corp,smartrecon,smartrecon_V2,v8,flowelements,reconbuilder,ngerecon_new'


Field_Separator=$IFS
IFS=,

for bank in $banks                                # here code will move to particular paths like www and nginx
do
   for folder in $folders                                  
   do
     if [ "$bank" == "SURYO" ] && [ "$folder" == "ngerecon" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/ngerecon_suryo/

     elif [ "$bank" == "SURYO" ] && [ "$folder" == "smartrecon_V2" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/
     
     elif [ "$bank" == "SURYO" ] && [ "$folder" == "flowelements" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/
     fi

     if [ "$bank" == "CORP" ] && [ "$folder" == "erecon_corp" ]
     then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/erecon_corp/
     elif [ "$bank" == "CORP" ] && [ "$folder" == "v8" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/
     fi

     if [ "$bank" == "IDFC" ] && [ "$folder" == "erecon" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/erecon_idfc/
     elif [ "$bank" == "IDFC" ] && [ "$folder" == "reconbuilder" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/
     fi
     
     if [ "$bank" == "EQUITAS" ] && [ "$folder" == "ngerecon" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/www/ngerecon_equitas/
     elif [ "$bank" == "EQUITAS" ] && [ "$folder" == "smartrecon" ]
      then cp -r /opt/repository/demo2/$bank'/'$folder /usr/share/nginx/
     fi

     if [ "$folder" == "ngerecon_new" ]
      then cp -r /opt/repository/demo2/$folder /usr/share/nginx/www/
     fi
 

 
   done
done


 
IFS=$Field_Separator

echo "127.0.0.1 ngereconsuryo.com" >> /etc/hosts   #host entery
echo "127.0.0.1 ereconidfc.com" >> /etc/hosts
echo "127.0.0.1 ngereconequitas.com" >> /etc/hosts
echo "127.0.0.1 ngereconcorp.com" >> /etc/hosts
echo "127.0.0.1 flowelements.com" >> /etc/hosts
echo "127.0.0.1 reconbuilder.com" >> /etc/hosts
echo "127.0.0.1 ngereconnew.com" >> /etc/hosts

cd /opt/repository/demo2/configuration        # copy all configuartion ,service ,sh files to particular paths        
cp -r *.conf /etc/nginx/conf.d/
cd /opt/repository/demo2/servicesh/
cp -r *.service /etc/systemd/system/
cd /opt/
mkdir systemdScripts
cd /opt/repository/demo2/servicesh/
cp -r *.sh /opt/systemdScripts/

cd /opt/repository/demo2/servicesh/

sudo systemctl daemon-reload                   

systemctl status ngereconsuryo.service            # checking the status of all service files and start services             
systemctl start ngereconsuryo.service
systemctl status ngereconsuryo.service

systemctl status ngereconequitas.service
systemctl start ngereconequitas.service
systemctl status ngereconequitas.service

systemctl status reconbuilder.service
systemctl start  reconbuilder.service
systemctl status reconbuilder.service

systemctl status erecon_idfc.service
systemctl start  erecon_idfc.service
systemctl status erecon_idfc.service





