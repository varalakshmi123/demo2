/**
 * Created by hash on 9/8/16.
 */

function reconlicController($scope, $rootScope, $interval, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, ReconLicenseService, BusinessService, JobStatusService) {

    $scope.chosenValue = '/app/partials/license.html'
    $scope.reconLicenseOptions = {};
    $scope.reconLicenseOptions.init = false;
    //$scope.reconLicenseOptions.isLoading = true;
    $scope.reconLicenseOptions.searchFields = {}
    $scope.reconLicenseOptions.service = ReconLicenseService
    $scope.failureLogPath = ''
    $scope.reconLicenseOptions.headers = [
        [
            {name: "Recon Name", searchable: true, sortable: true, dataField: "reconName", colIndex: 0},
            {
                name: "Recon Id",
                searchable: true,
                sortable: true,
                dataField: "reconId",
                colIndex: 1
            },
            // {
            //     name: "WorkPool Name",
            //     searchable: true,
            //     sortable: true,
            //     dataField: "workpool_name",
            //     colIndex: 1
            // },
            {
                name: "BusinessProcess Name",
                searchable: true,
                sortable: true,
                dataField: "businessProcessName",
                colIndex: 1
            },
            // {
            //     name: "Business ContextId",
            //     searchable: false,
            //     sortable: true,
            //     dataField: "businessContextId",
            //     colIndex: 1
            // },
            // {
            //     name: "Currency",
            //     searchable: true,
            //     sortable: true,
            //     dataField: "currency",
            //     colIndex: 1
            // },
            {
                name: "Statement Date",
                searchable: true,
                sortable: true,
                dataField: "statementDate",
                colIndex: 1
            },
            {
                name: "Last Execution Date",
                searchable: true,
                sortable: true,
                dataField: "executionDT", filter: 'formatTimeStamp',
                colIndex: 1
            },
            {
                name: "Job Status",
                searchable: true,
                sortable: true,
                dataField: "jobStatus", filter: 'jobStatus',
                isCompile: true,
                isHtml: true,
                colIndex: 1
            },
            {
                name: "Sources",
                searchable: true,
                sortable: true,
                dataField: "sources",
                colIndex: 1
            }, {
            name: "License Count",
            searchable: true,
            sortable: true,
            dataField: "licenseCount",
            filter: 'badge',
            isCompile: true,
            isHtml: true,
            colIndex: 1
        }, {
            name: "Licensed",
            dataField: "licensed",
            imgMap: {"Licensed": "app/img/completed.png", 'Non-Licensed': "app/img/Failed.png"},
            colIndex: 1
        }, {
            name: "Actions",
            colIndex: 4,
            datawidth: "5%",
            actions: [{
                toolTip: "Run Recon",
                action: "runRecon",
                posticon: "fa fa-play fa-lg",
                "disabledFn": "disableRun"
            }, {
                toolTip: "Job History",
                action: "jobhistory",
                posticon: "fa fa-info-circle fa-lg",
                "disabledFn": "disableJobSummary"
            }, {
                toolTip: "Job Log File",
                // action: "jobLog",
                onclickDownloadUrl: true,
                disabledFn: 'jobLog'
            }]
        }
        ]
    ];

    if ($rootScope.credentials.role == 'admin' || $rootScope.credentials.role == 'Administrator') {
        console.log($scope.reconLicenseOptions.headers[0][$scope.reconLicenseOptions.headers[0].length - 1])
        $scope.reconLicenseOptions.headers[0].push({
            name: "Export Recon Configuration",
            colIndex: 4,
            datawidth: "5%",
            actions: [{
                toolTip: "Export Recon",
                action: "exportReconDetails",
                posticon: "fa fa-sign-out fa-lg",
                "disabledFn": "disableRun",
                customMigrationUrl: true
            }]
        })
    }

    $scope.reconLicenseOptions.jobLog = function (recon) {
        if (angular.isDefined(recon['log_path']) & recon['log_path'] != '') {
            return false
        }
        return true;
    }


    $scope.reconLicenseOptions.exportReconDetails = function (recon) {
        console.log(recon)
        ReconLicenseService.exportReconDetails('dummy', {'reconId': recon['reconId']}, function (data) {
            var filePath = window.location.origin + '/app/files/' + data
            if (data != '') {
                $window.open(filePath);
                console.log(filePath,window.location.origin + filePath)
            }
            // var link = document.createElement('a');
            // var blob = new Blob([ content ], { type : 'text/plain' });
            // link.attr('href',window.URL.createObjectURL(blob));
            // link.attr('download', filePath);
            // link[0].click();
            // document.body.appendChild(link);
            // link.href = filePath;
            // link.click();
        })
    }

    $scope.jobHistoryOptions = {};
    $scope.jobHistoryOptions.init = false;
    $scope.jobHistoryOptions.extpaging = true;
    $scope.jobHistoryOptions.init = false;
    $scope.getJobHistory = function (data) {
        $scope.jobHistoryOptions.searchFields = {}
        $scope.jobHistoryOptions.datamodel = []
        //$scope.jobHistoryOptions.service = ReconLicenseService
        $scope.jobHistoryOptions.headers = [
            [
                {name: "Recon Name", dataField: "reconName", colIndex: 0},
                {
                    name: "Recon Id",
                    dataField: "reconId",
                    colIndex: 1
                },
                {
                    name: "Statement Date",
                    dataField: "stmtDate",
                    colIndex: 1
                },
                {
                    name: "Job Start",
                    dataField: "jobStart",
                    colIndex: 1
                },
                {
                    name: "Job End",
                    dataField: "jobEnd",
                    colIndex: 1
                },
                {
                    name: "Total Input",
                    dataField: "totalInput",
                    colIndex: 1
                },
                {
                    name: "Total Output",
                    dataField: "totalOutput",
                    colIndex: 1
                }, {
                name: "MatchedCount",
                dataField: "matchedCount",
                colIndex: 1
            }, {
                name: "Un-MatchedCount",
                dataField: "unmatchedCount",
                colIndex: 1
            }
            ]
        ];
        $scope.jobHistoryOptions.datamodel = data
    }
    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };
    $scope.reconLicenseOptions.jobhistory = function (recon) {
        $rootScope.openModalPopupOpen()
        ReconLicenseService.getJobSummary('dummy', {'recon_id': recon['reconId']}, function (data) {
            console.log(data)
            $rootScope.openModalPopupClose()
            if (data['total'] > 0) {
                $scope.chosenValue = '/app/partials/jobDetails.html'
                $scope.getJobHistory(data)
            }
            else {
                $rootScope.showAlertMsg('No Jobs Executed for this recon')
            }
        })

    }
    $scope.reconLicenseOptions.runRecon = function (recon) {
        $modal.open({
            templateUrl: 'runRecon.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, ReconLicenseService) {
                $rootScope.showAlert('', false, '')
                $scope.rec = recon
                if ($rootScope.credentials.role == 'Recon Administrator') {
                    $scope.rec['statementDate'] = new Date().setDate(new Date().getDate() - 1);
                } else {
                    $scope.rec['statementDate'] = 0
                }

                $scope.runRecon = function (rec) {
                    $scope.reconData = rec
                    //rec['statementDate'] = 0
                    //$scope.reconData['statementDate'] = $scope.toEpoch($scope.reconData['statementDate'])
                    var query = {}
                    query['recon_id'] = $scope.reconData['reconId']
                    console.log($scope.reconData)
                    query['statementDate'] = $filter('date')($scope.reconData['statementDate'], 'dd-MMM-yyyy')
                    perColConditions = {'reconId': query['reconId']}
                    console.log(query['reconId'])
                    JobStatusService.get(undefined, {"query": {"condition": {'reconId': query['recon_id']}}}, function (reconObj) {
                        console.log(reconObj)
                        if (reconObj['total'] == 0) {
                            reconObj = {
                                'reconId': query['recon_id'],
                                'jobStatus': 'Job Running',
                                'executionDT': Date.now() / 1000,
                                'statementDate': query['statementDate'],
                                'ExecutedBy': $rootScope.credentials.userName,
                                'log_path': ''
                            }
                            console.log(reconObj)
                            JobStatusService.post(reconObj, function (data) {
                                $scope.reconLicenseOptions.reQuery = true
                                ReconLicenseService.runRecon('dummy', query, function (data) {
                                    // console.log(data)
                                })
                                $scope.cancel()
                                $scope.recursitionFunc(query)
                            })


                        } else {
                            if (reconObj['data'][0]['jobStatus'] == 'Job Running') {
                                $scope.msgNotify('warning', 'Job is already running. Please wait untill it completes...');
                                $scope.cancel()
                            }
                            else {
                                reconObj = reconObj['data'][0]
                                reconObj['jobStatus'] = 'Job Running'
                                reconObj['executionDT'] = Date.now() / 1000
                                reconObj['statementDate'] = query['statementDate']
                                console.log(reconObj)
                                JobStatusService.put(reconObj['_id'], reconObj, function (data) {
                                    $scope.reconLicenseOptions.reQuery = true
                                    ReconLicenseService.runRecon('dummy', query, function (data) {
                                    })
                                    $scope.cancel()
                                    $scope.recursitionFunc(query)
                                })
                                // JobStatus().modify(reconObj['_id'], reconObj)

                            }

                        }
                    })
                }
                $scope.recursitionFunc = function (query) {
                    var stop;
                    $scope.fight = function () {
                        queryCond = {
                            "query": {
                                "condition": {
                                    'jobStatus': 'Job Running'
                                },
                            }
                        }
                        if (angular.isDefined(stop)) return;
                        stop = $interval(function () {
                            ReconLicenseService.get(undefined, queryCond, function (data) {
                                $scope.reconLicenseOptions.reQuery = true
                            })
                        }, 60000);
                    };

                    $scope.fight();
                }
                $scope.stopFight = function (stop) {
                    if (angular.isDefined(stop)) {
                        $interval.cancel(stop);
                        stop = undefined;
                    }
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel')
                }
            }
        })
    }
    $scope.reconLicenseOptions.disableRun = function (recon) {
        if ($rootScope.credentials.role == 'Recon Manager' || $rootScope.credentials.role == 'Recon Administrator') {
            return false
        } else {
            return false
        }
    }

    $scope.reconLicenseOptions.disableJobSummary = function (recon) {
        return false
    }

    $scope.syncReconLicense = function () {
        $rootScope.openModalPopupOpen()
        ReconLicenseService.getReconLicense('dummy', function (data) {
            $rootScope.openModalPopupClose()
            //$scope.getLicensedRecons()
            $scope.reconLicenseOptions.reQuery = true
        })
    }

    $scope.getLicensedRecons = function () {
        $scope.chosenValue = '/app/partials/license.html'
        $scope.reconLicenseOptions.reQuery = true
    }
    $scope.recursitionFunc = function (query) {
        $scope.stop;
        $scope.fight = function () {
            queryCond = {
                "query": {
                    "condition": {
                        'jobStatus': 'Job Running'
                    },
                }
            }
            if (angular.isDefined($scope.stop)) return;
            $scope.stop = $interval(function () {
                ReconLicenseService.get(undefined, queryCond, function (data) {
                    console.log(data, '<----------')
                    if (data.total == 0) {
                        $scope.reconLicenseOptions.reQuery = true
                        $scope.stopFight($scope.stop);
                    }
                    $scope.reconLicenseOptions.reQuery = true

                })
            }, 5000);
        };

        $scope.fight();
    }
    $scope.stopFight = function (stop) {
        if (angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
    };

    $scope.$on('$destroy', function () {
        $scope.stopFight($scope.stop)
    });

    $scope.recursitionFunc()


}
