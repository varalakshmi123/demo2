
var app = angular.module("webApp.services",[]);

app.service("baseService", ["$http","$rootScope", function($http,$rootScope){
    var baseService = {};
    var apiUrl = "/api/"

    baseService.handleError = function(data, status, headers, config) {
        console.log("error: "+data+":"+status);
    }

    baseService.handleData = function(data, dataCallback,errorCallBack) {
        if(data["status"] === "ok")
        {
            if(dataCallback)
            {
                dataCallback(data["msg"]);
            }
        }
/*        else
        {
            //alert("Received Error: "+angular.toJson(data))
		errorCallBack(data);	    
        }*/
	else {
                $rootScope.openModalPopupClose();
                if (angular.isDefined(errorCallBack))
                {
                    errorCallBack(data);
                }
                else
                {
                    $rootScope.handleErrorMessages(data);
                }
            }
    }

    baseService.getSnakeCaseName = function(camelCase) {
        return camelCase.replace( /([A-Z])/g, "_$1").toLowerCase().replace(/^_(.*)/g,"$1");
    }

    baseService.get = function(collection, id, filter, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        if(id)
        {
            url = url+"/"+id;
        }

        return $http.get(url,{params:filter}).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    }

    baseService.put = function(collection, id, data, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        url = url+"/"+id;
        $http.put(url, data).error(baseService.handleError)	
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    }

    baseService.post = function(collection, data, dataCallback,errorCallBack){
    console.log(apiUrl)
        console.log(baseService.getSnakeCaseName(collection))

        var url = apiUrl+baseService.getSnakeCaseName(collection);
        console.log(url)
        $http.post(url, data).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    }

    baseService.deleteData = function(collection, id, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection);
        url = url+"/"+id;
        /*$http.delete(url).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });*/
        $http({method: 'DELETE', url: url}).error(baseService.handleError)
                    .success(function (data, status, headers, config) {
                        baseService.handleData(data, dataCallback, errorCallBack);
                    });
    }

    baseService.action = function(collection, id, action, data, dataCallback,errorCallBack){
        var url = apiUrl+baseService.getSnakeCaseName(collection)+"/"+id+"/"+action;
        $http.post(url, data).error(baseService.handleError)
            .success(function(data, status, headers, config){
                baseService.handleData(data, dataCallback,errorCallBack);
            });
    }

    return baseService;
}]);
