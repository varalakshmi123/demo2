var app = angular.module("angularBootstrap", []);

app.directive('topnav', ['$location', function ($location) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            'brand': "@"
        },
        link: function (scope, element, attrs) {
            scope.isCollapsed = "false";
            //Monitor URL and update the selection
            scope.$on('$locationChangeSuccess', function (ev) {
                element.find('li').removeClass('active');
                //For dropdown in menu we need to set "active" on 2 li's so using ".parents()"
                element.find("[href='" + $location.path() + "']").parents("li").addClass('active');
            });
        },
        template: '<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">' +
            '    <div class="navbar-header">' +
            '        <button type="button" class="navbar-toggle" ng-click="isCollapsed = !isCollapsed">' +
            '            <span class="sr-only">Toggle navigation</span>' +
            '            <span class="icon-bar"></span>' +
            '            <span class="icon-bar"></span>' +
            '            <span class="icon-bar"></span>' +
            '        </button>' +
            '        <a class="navbar-brand" href="/migratedSystems">{{brand}}</a>' +
            '    </div>' +
            '    <div class="collapse navbar-collapse" collapse="isCollapsed">' +
            '        <div ng-transclude></div>' +
            '    </div>' +
            '</nav>'
    };
}]);
app.directive('sidenav', ['$location', function ($location) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            'brand': "@"
        },
        link: function (scope, element, attrs) {
            scope.isCollapsed = "false";
            //Monitor URL and update the selection
            scope.$on('$locationChangeSuccess', function (ev) {
                element.find('li').removeClass('active');
                //For dropdown in menu we need to set "active" on 2 li's so using ".parents()"
                element.find("[href='" + $location.path() + "']").parents("li").addClass('active');
            });
        },
        template: '<div ng-transclude></div>'
    };
}]);
app.directive('sidetopnav', ['$location', function ($location) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            'brand': "@"
        },
        link: function (scope, element, attrs) {
            scope.isCollapsed = "false";
            //Monitor URL and update the selection
            scope.$on('$locationChangeSuccess', function (ev) {
                element.find('li').removeClass('active');
                //For dropdown in menu we need to set "active" on 2 li's so using ".parents()"
                element.find("[href='" + $location.path() + "']").parents("li").addClass('active');
            });
        },            	
        template: '<div class="navbar navbar-fixed-top navbarFixedTopHeader row">' +
				'	<div class="col-lg-9 text-center" style="margin-top:1%;font-size: 1.3em;">' +
				'		<span id="titlename">{{brand}}</span>&nbsp;<b>' +
				'	</div>' +
				'    <div class="collapse navbar-collapse" collapse="isCollapsed">' +
				'        <div ng-transclude></div>' +
				'    </div>' +
				'	<div style="clear:both"></div>' +
				'</div>'
    };
}]);

/*********************************************************************************
 * <alert type="danger" close="close('test')">
 *     <strong>Danger!</strong> Best check yo self, you're not looking too good. <a href="#" class="alert-link">Hello</a>
 * </alert>
 *********************************************************************************/
app.directive("alert", [function () {
    return {
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {
            type: "@",
            close: "&"
        },
        link: function (scope, element, attrs) {
            scope.closeButton = "close" in attrs;
            scope.closeAlert = function () {
                scope.close();
                element.remove();
            };
        },
        template: '<div class="alert" ng-class="\'alert-\'+type">' +
            '    <button type="button" class="close" ng-click="closeAlert()" ng-show="closeButton">&times;</button>' +
            '    <div ng-transclude></div>' +
            '</div>'
    };
}]);

/*********************************************************************************
 * <div>
 *     <button class="btn btn-default" ng-click="toggle()">Click</button>
 *     <div class="well" collapse="isCollapsed">
 *         <p>Hello</p>
 *     </div>
 * </div>
 *********************************************************************************/
app.directive("collapse", [function () {
    return {
        restrict: 'A',
        scope: {
            collapse: "="
        },
        link: function (scope, element, attrs) {
            scope.show = function () {
                if (!element.hasClass("in")) {
                    element.removeClass('collapse');
                    element.addClass('collapsing');
                    element.height(0);
                    element.removeClass('collapsing');
                    element.addClass('in');
                    element.height('auto');
                }
            }
            scope.hide = function () {
                if (element.hasClass("in")) {
                    element.height(element.height());
                    element.offsetHeight;

                    element.addClass('collapsing');
                    element.removeClass('collapse');
                    element.removeClass('in');

                    element.removeClass('collapsing');
                    element.addClass('collapse');
                }
            }
            scope.$watch('collapse', function (value) {
                if (value) {
                    scope.hide();
                }
                else {
                    scope.show();
                }
            });
            //First time trigger manually
            if (scope.collapse) {
                element.addClass('in');
                scope.hide();
            }
            else {
                element.removeClass('in');
                scope.show();
            }
        }
    };
}]);

/*********************************************************************************
 * <accordion multi="isMulti">
 *     <accordion-item title="state1">
 *         <p>State 1 message</p>
 *     </accordion-item>
 *     <accordion-item title="state2">
 *         <p>State 2 message</p>
 *     </accordion-item>
 * </accordion>
 *
 * Note: 'isMulti' defined as true or false in controller
 *********************************************************************************/
app.directive("accordion", [function () {
    return {
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {
            multi: "="
        },
        controller: function ($scope, $element, $attrs) {
            this.items = [];
            this.addItem = function (itemScope) {
                this.items.push(itemScope);
                itemScope.$on('$destroy', function (event) {
                    if(this.items)
                    {
                        var index = this.items.indexOf(itemScope);
                        if (index !== -1) {
                            this.items.splice(index, 1);
                        }
                    }
                });
            }
            this.handleOpen = function (nowOpened) {
                if (!$scope.multi) {
                    angular.forEach(this.items, function (item, index) {
                        if (item != nowOpened && item.isCollapsed === false) {
                            item.isCollapsed = true;
                        }
                    });
                }
            }
        },
        template: '<div class="panel-group"><div ng-transclude></div> </div>'
    };
}]);

app.directive("accordionItem", [function () {
    return {
        restrict: "E",
        transclude: true,
        replace: true,
        require: '^accordion',
        scope: {
            title: "@"
        },
        link: function (scope, element, attrs, accordionCtrl) {
            scope.isCollapsed = false;
            accordionCtrl.addItem(scope);

            scope.$watch('isCollapsed', function (value) {
                if (!value) {
                    accordionCtrl.handleOpen(scope);
                }
            })
            
	    scope.toggle = function () {
                scope.isCollapsed = !scope.isCollapsed;
            }
        },
        template: '<div class="panel panel-default">' +
            '    <div class="panel-heading">' +
            '        <h4 class="panel-title">' +
            '            <button class="btn btn-link" ng-click="toggle()"> {{ title }}</button>' +
            '        </h4>' +
            '    </div>' +
            '    <div class="panel-body" collapse="isCollapsed">' +
            '        <div ng-transclude></div>' +
            '    </div>' +
            '</div>'
    };
}]);

/*********************************************************************************
 *  <modal visible="modalVisible" close="closeFunc()">
 *      <div class="modal-header">
 *          <button type="button" class="close" data-dismiss="modal">×</button>
 *          <h4 class="modal-title">Heading</h4>
 *      </div>
 *      <div class="modal-body">
 *          <p>Body message</p>
 *      </div>
 *      <div class="modal-footer">
 *          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 *          <button type="button" class="btn btn-primary">Save</button>
 *      </div>
 *  </modal>
 *********************************************************************************/
app.directive("modal", ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            visible: "=",
            autoclose: "=",
            close: "&"
        },
        link: function (scope, element, attrs) {
            scope.backdrop = undefined;

            scope.showModal = function () {
                scope.backdrop = angular.element('<div class="modal-backdrop fade in" />').appendTo(document.body);
                angular.element(document.body).addClass("modal-open");
                element.addClass("in");
                element.show();
                element.on('click', '[data-dismiss="modal"]', function () {
                    $timeout(function(){scope.visible = false;}, 0)
                });

                element.on('click', function (e) {
                    if (e.target !== e.currentTarget) return;
                    $timeout(function(){scope.visible = false;}, 0);
                });

                if(angular.isDefined(scope.autoclose) && scope.autoclose >= 0)
                {
                    scope.autocloseTimer = $timeout(function(){scope.visible = false;}, scope.autoclose);
                }

            }

            scope.hideModal = function () {
                if (scope.backdrop) {
                    angular.element(document.body).removeClass("modal-open")
                    scope.backdrop.remove();
                    scope.backdrop = undefined;
                    element.removeClass("in");
                    scope.close();
                    element.hide();
                    if(angular.isDefined(scope.autocloseTimer))
                    {
                        $timeout.cancel(scope.autocloseTimer);
                    }
                }
            }

            scope.$watch('visible', function (value) {
                if (value) {
                    scope.showModal();
                }
                else {
                    scope.hideModal();
                }
            });
        },
        template: '<div class="modal fade" tabindex="-1">' +
                  '    <div class="modal-dialog">' +
                  '        <div class="modal-content">' +
                  '            <div ng-transclude></div>' +
                  '        </div>' +
                  '     </div>' +
                  '</div>'
    };
}]);

/*********************************************************************************
 * <tabset>
 *     <tab heading="Tab1"> Tab 1 content <i class="icon-truck"></i></tab>
 *     <tab heading="Tab2"> Tab 2 content</tab>
 *     <tab heading="Tab3"> Tab 3 content</tab>
 *     <tab heading="Tab4"> Tab 4 content</tab>
 * </tabset>
 *********************************************************************************/
app.directive("tabset", ['$timeout', function($timeout){
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
        },
        controller: function($scope, $element, $attrs){
            $scope.tabs = [];
            this.addTab = function(tabScope) {
                $scope.tabs.push(tabScope);
            }
            $scope.activate = function(index) {
                $scope.activeIndex = index;
                angular.forEach($scope.tabs, function(tabContent, index){
                    if(index === $scope.activeIndex)
                    {
                        tabContent.active = true;
                    }
                    else
                    {
                        tabContent.active = false;
                    }
                });
            }
            $timeout(function(){$scope.activate(0)},0);
        },
        template: '<div><ul class="nav nav-tabs"><li ng-repeat="tab in tabs" ng-class="{active: $index === activeIndex}"><a href="" ng-click="activate($index)">{{tab.heading}}</a></li></ul><div ng-transclude class="tab-content"></div></div>'
    }
}]);

app.directive("tab", [function(){
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        require: "^tabset",
        scope: {
            heading: '@'
        },
        link: function (scope, element, attrs, tabsetCtrl) {
            scope.active = false;
            tabsetCtrl.addTab(scope);
        },
        template: '<div class="tab-pane" ng-class="{active: active}"><div ng-transclude></div></div>'
    };
}]);

/*********************************************************************************
 *<div class="dropdown">
 *    <!-- Link or button to toggle dropdown -->
 *    <a class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
 *    <ul class="dropdown-menu">
 *        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
 *        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
 *        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
 *        <li role="presentation" class="divider"></li>
 *        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
 *    </ul>
 *</div>
 *
 * Note: class="dropdown-toggle" is mandatory!!
 *********************************************************************************/
app.directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
    var openElement = null,
        closeMenu   = angular.noop;
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {
            scope.$watch('$location.path', function() { closeMenu(); });
            element.parent().bind('click', function() { closeMenu(); });
            element.bind('click', function (event) {

                var elementWasOpen = (element === openElement);

                event.preventDefault();
                event.stopPropagation();

                if (!!openElement) {
                    closeMenu();
                }

                if (!elementWasOpen) {
                    element.closest(".dropdown").addClass('open');
                    openElement = element;
                    closeMenu = function (event) {
                        if (event) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        $document.unbind('click', closeMenu);
                        element.closest(".dropdown").removeClass('open');
                        closeMenu = angular.noop;
                        openElement = null;
                    };
                    $document.bind('click', closeMenu);
                }
            });
        }
    };
}]);



// Copy from http://angular-ui.github.io/bootstrap/ for tooltip and popover. One day will make this simpler for the framework to be maintainable
app.factory('$position', ['$document', '$window', function ($document, $window) {

    function getStyle(el, cssprop) {
        if (el.currentStyle) { //IE
            return el.currentStyle[cssprop];
        } else if ($window.getComputedStyle) {
            return $window.getComputedStyle(el)[cssprop];
        }
        // finally try and get inline style
        return el.style[cssprop];
    }

    /**
     * Checks if a given element is statically positioned
     * @param element - raw DOM element
     */
    function isStaticPositioned(element) {
        return (getStyle(element, "position") || 'static' ) === 'static';
    }

    /**
     * returns the closest, non-statically positioned parentOffset of a given element
     * @param element
     */
    var parentOffsetEl = function (element) {
        var docDomEl = $document[0];
        var offsetParent = element.offsetParent || docDomEl;
        while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent) ) {
            offsetParent = offsetParent.offsetParent;
        }
        return offsetParent || docDomEl;
    };

    return {
        /**
         * Provides read-only equivalent of jQuery's position function:
         * http://api.jquery.com/position/
         */
        position: function (element) {
            var elBCR = this.offset(element);
            var offsetParentBCR = { top: 0, left: 0 };
            var offsetParentEl = parentOffsetEl(element[0]);
            if (offsetParentEl != $document[0]) {
                offsetParentBCR = this.offset(angular.element(offsetParentEl));
                offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
                offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
            }

            return {
                width: element.prop('offsetWidth'),
                height: element.prop('offsetHeight'),
                top: elBCR.top - offsetParentBCR.top,
                left: elBCR.left - offsetParentBCR.left
            };
        },

        /**
         * Provides read-only equivalent of jQuery's offset function:
         * http://api.jquery.com/offset/
         */
        offset: function (element) {
            var boundingClientRect = element[0].getBoundingClientRect();
            return {
                width: element.prop('offsetWidth'),
                height: element.prop('offsetHeight'),
                top: boundingClientRect.top + ($window.pageYOffset || $document[0].body.scrollTop),
                left: boundingClientRect.left + ($window.pageXOffset || $document[0].body.scrollLeft)
            };
        }
    };
}]);

app.provider( '$tooltip', function () {
    // The default options tooltip and popover.
    var defaultOptions = {
        placement: 'top',
        animation: true,
        popupDelay: 0
    };

    // Default hide triggers for each show trigger
    var triggerMap = {
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur'
    };

    // The options specified to the provider globally.
    var globalOptions = {};

    /**
     * `options({})` allows global configuration of all tooltips in the
     * application.
     *
     *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
   *     // place tooltips left instead of top by default
   *     $tooltipProvider.options( { placement: 'left' } );
   *   });
     */
    this.options = function( value ) {
        angular.extend( globalOptions, value );
    };

    /**
     * This allows you to extend the set of trigger mappings available. E.g.:
     *
     *   $tooltipProvider.setTriggers( 'openTrigger': 'closeTrigger' );
     */
    this.setTriggers = function setTriggers ( triggers ) {
        angular.extend( triggerMap, triggers );
    };

    /**
     * This is a helper function for translating camel-case to snake-case.
     */
    function snake_case(name){
        var regexp = /[A-Z]/g;
        var separator = '-';
        return name.replace(regexp, function(letter, pos) {
            return (pos ? separator : '') + letter.toLowerCase();
        });
    }

    /**
     * Returns the actual instance of the $tooltip service.
     * TODO support multiple triggers
     */
    this.$get = [ '$window', '$compile', '$timeout', '$parse', '$document', '$position', '$interpolate', function ( $window, $compile, $timeout, $parse, $document, $position, $interpolate ) {
        return function $tooltip ( type, prefix, defaultTriggerShow ) {
            var options = angular.extend( {}, defaultOptions, globalOptions );

            /**
             * Returns an object of show and hide triggers.
             *
             * If a trigger is supplied,
             * it is used to show the tooltip; otherwise, it will use the `trigger`
             * option passed to the `$tooltipProvider.options` method; else it will
             * default to the trigger supplied to this directive factory.
             *
             * The hide trigger is based on the show trigger. If the `trigger` option
             * was passed to the `$tooltipProvider.options` method, it will use the
             * mapped trigger from `triggerMap` or the passed trigger if the map is
             * undefined; otherwise, it uses the `triggerMap` value of the show
             * trigger; else it will just use the show trigger.
             */
            function getTriggers ( trigger ) {
                var show = trigger || options.trigger || defaultTriggerShow;
                var hide = triggerMap[show] || show;
                return {
                    show: show,
                    hide: hide
                };
            }

            var directiveName = snake_case( type );

            var startSym = $interpolate.startSymbol();
            var endSym = $interpolate.endSymbol();
            var template =
                '<'+ directiveName +'-popup '+
                    'title="'+startSym+'tt_title'+endSym+'" '+
                    'content="'+startSym+'tt_content'+endSym+'" '+
                    'placement="'+startSym+'tt_placement'+endSym+'" '+
                    'animation="tt_animation()" '+
                    'is-open="tt_isOpen"'+
                    '>'+
                    '</'+ directiveName +'-popup>';

            return {
                restrict: 'EA',
                scope: true,
                link: function link ( scope, element, attrs ) {
                    var tooltip = $compile( template )( scope );
                    var transitionTimeout;
                    var popupTimeout;
                    var $body;
                    var appendToBody = angular.isDefined( options.appendToBody ) ? options.appendToBody : false;
                    var triggers = getTriggers( undefined );
                    var hasRegisteredTriggers = false;

                    // By default, the tooltip is not open.
                    // TODO add ability to start tooltip opened
                    scope.tt_isOpen = false;

                    function toggleTooltipBind () {
                        if ( ! scope.tt_isOpen ) {
                            showTooltipBind();
                        } else {
                            hideTooltipBind();
                        }
                    }

                    // Show the tooltip with delay if specified, otherwise show it immediately
                    function showTooltipBind() {
                        if ( scope.tt_popupDelay ) {
                            popupTimeout = $timeout( show, scope.tt_popupDelay );
                        } else {
                            scope.$apply( show );
                        }
                    }

                    function hideTooltipBind () {
                        scope.$apply(function () {
                            hide();
                        });
                    }

                    // Show the tooltip popup element.
                    function show() {
                        var position,
                            ttWidth,
                            ttHeight,
                            ttPosition;

                        // Don't show empty tooltips.
                        if ( ! scope.tt_content ) {
                            return;
                        }

                        // If there is a pending remove transition, we must cancel it, lest the
                        // tooltip be mysteriously removed.
                        if ( transitionTimeout ) {
                            $timeout.cancel( transitionTimeout );
                        }

                        // Set the initial positioning.
                        tooltip.css({ top: 0, left: 0, display: 'block' });

                        // Now we add it to the DOM because need some info about it. But it's not
                        // visible yet anyway.
                        if ( appendToBody ) {
                            $body = $body || $document.find( 'body' );
                            $body.append( tooltip );
                        } else {
                            element.after( tooltip );
                        }

                        // Get the position of the directive element.
                        position = appendToBody ? $position.offset( element ) : $position.position( element );

                        // Get the height and width of the tooltip so we can center it.
                        ttWidth = tooltip.prop( 'offsetWidth' );
                        ttHeight = tooltip.prop( 'offsetHeight' );

                        // Calculate the tooltip's top and left coordinates to center it with
                        // this directive.
                        switch ( scope.tt_placement ) {
                            case 'right':
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left + position.width
                                };
                                break;
                            case 'bottom':
                                ttPosition = {
                                    top: position.top + position.height,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;
                            case 'left':
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left - ttWidth
                                };
                                break;
                            default:
                                ttPosition = {
                                    top: position.top - ttHeight,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;
                        }

                        ttPosition.top += 'px';
                        ttPosition.left += 'px';

                        // Now set the calculated positioning.
                        tooltip.css( ttPosition );

                        // And show the tooltip.
                        scope.tt_isOpen = true;
                    }

                    // Hide the tooltip popup element.
                    function hide() {
                        // First things first: we don't show it anymore.
                        scope.tt_isOpen = false;

                        //if tooltip is going to be shown after delay, we must cancel this
                        $timeout.cancel( popupTimeout );

                        // And now we remove it from the DOM. However, if we have animation, we
                        // need to wait for it to expire beforehand.
                        // FIXME: this is a placeholder for a port of the transitions library.
                        if ( angular.isDefined( scope.tt_animation ) && scope.tt_animation() ) {
                            transitionTimeout = $timeout( function () { tooltip.remove(); }, 500 );
                        } else {
                            tooltip.remove();
                        }
                    }

                    /**
                     * Observe the relevant attributes.
                     */
                    attrs.$observe( type, function ( val ) {
                        scope.tt_content = val;
                    });

                    attrs.$observe( prefix+'Title', function ( val ) {
                        scope.tt_title = val;
                    });

                    attrs.$observe( prefix+'Placement', function ( val ) {
                        scope.tt_placement = angular.isDefined( val ) ? val : options.placement;
                    });

                    attrs.$observe( prefix+'Animation', function ( val ) {
                        scope.tt_animation = angular.isDefined( val ) ? $parse( val ) : function(){ return options.animation; };
                    });

                    attrs.$observe( prefix+'PopupDelay', function ( val ) {
                        var delay = parseInt( val, 10 );
                        scope.tt_popupDelay = ! isNaN(delay) ? delay : options.popupDelay;
                    });

                    attrs.$observe( prefix+'Trigger', function ( val ) {

                        if (hasRegisteredTriggers) {
                            element.unbind( triggers.show, showTooltipBind );
                            element.unbind( triggers.hide, hideTooltipBind );
                        }

                        triggers = getTriggers( val );

                        if ( triggers.show === triggers.hide ) {
                            element.bind( triggers.show, toggleTooltipBind );
                        } else {
                            element.bind( triggers.show, showTooltipBind );
                            element.bind( triggers.hide, hideTooltipBind );
                        }

                        hasRegisteredTriggers = true;
                    });

                    attrs.$observe( prefix+'AppendToBody', function ( val ) {
                        appendToBody = angular.isDefined( val ) ? $parse( val )( scope ) : appendToBody;
                    });

                    // if a tooltip is attached to <body> we need to remove it on
                    // location change as its parent scope will probably not be destroyed
                    // by the change.
                    if ( appendToBody ) {
                        scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess () {
                            if ( scope.tt_isOpen ) {
                                hide();
                            }
                        });
                    }

                    // Make sure tooltip is destroyed and removed.
                    scope.$on('$destroy', function onDestroyTooltip() {
                        if ( scope.tt_isOpen ) {
                            hide();
                        } else {
                            tooltip.remove();
                        }
                    });
                }
            };
        };
    }];
});

app.directive( 'tooltipPopup', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
        template: '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">' +
                  '  <div class="tooltip-arrow"></div>' +
                  '  <div class="tooltip-inner" ng-bind="content"></div>' +
                  '</div>'
    };
});

app.directive( 'tooltip', [ '$tooltip', function ( $tooltip ) {
    return $tooltip( 'tooltip', 'tooltip', 'mouseenter' );
}]);

app.directive( 'popoverPopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
        template: '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">' +
                  '  <div class="arrow"></div>' +
                  '  <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>' +
                  '  <div class="popover-content" ng-bind="content"></div>' +
                  '</div>'
    };
});

app.directive( 'popover', [ '$compile', '$timeout', '$parse', '$window', '$tooltip', function ( $compile, $timeout, $parse, $window, $tooltip ) {
        return $tooltip( 'popover', 'popover', 'click' );
}]);

app.directive( 'popoverHtmlPopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
        template: '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">' +
            '  <div class="arrow"></div>' +
            '  <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>' +
            '  <div class="popover-content" ng-bind-html="content"></div>' +
            '</div>'
    };
});

app.directive( 'popoverHtml', [ '$compile', '$timeout', '$parse', '$window', '$tooltip', function ( $compile, $timeout, $parse, $window, $tooltip ) {
    return $tooltip( 'popoverHtml', 'popover', 'click' );
}]);
