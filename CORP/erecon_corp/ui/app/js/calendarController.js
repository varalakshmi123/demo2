/**
 * Created by swami.
 */
function calendarController($scope, $rootScope, $interval, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, ReconLicenseService, BusinessService, JobStatusService, ReconExecutionDetailsLogService, EventLogService) {

    $scope.eventDetails = []
    $scope.monthTracker = 0
    $scope.modalHeader = 'Add'
    $('#calendar').fullCalendar({
        height: 550,
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: $scope.eventDetails,
        editable: false,
        customButtons: {
            addCalEvnt: {
                text: 'Add Event',
                click: function () {
                    console.log('coming here...!\nAdd event func')
                }
            }
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        dayClick: function (date, jsEvent, view) {
            console.log('add event in calendar')
            $scope.modalHeader = 'Add'
            $scope.openModalWindow(date)
        },
        eventClick: function (event, element) {
            // event.title = "CLICKED!";
            // $('#calendar').fullCalendar('updateEvent', event);
            console.log('edit event')
            $scope.modalHeader = 'Edit'
            $scope.openModalWindow(event, true)
        }
    });

    $('#calendar').find('.fc-prev-button').click(function () {
        $scope.monthTracker = $scope.monthTracker - 1
        $scope.refreshCalendarEvent()
    });

    $('#calendar').find('.fc-next-button').click(function () {
        $scope.monthTracker = $scope.monthTracker + 1
        $scope.refreshCalendarEvent()
    });

    $('#calendar').find('.fc-today-button').click(function () {
        $scope.monthTracker = 0
        $scope.refreshCalendarEvent()
    });

    $scope.refreshCalendarEvent = function () {
        queryDates = $scope.getQueryDate()
        $scope.eventDetails = []
        EventLogService.queryEvents('dummy', queryDates, function (data) {
            console.log(data)
            $scope.eventDetails = data
            if ($scope.eventDetails.length > 0) {
                $('#calendar').fullCalendar('removeEvents');
                // remove events and re-add event source to reflect search/non-search
                $('#calendar').fullCalendar('addEventSource', $scope.eventDetails);
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.getQueryDate = function () {
        queryParam = {}
        var date = new Date(), year = date.getFullYear(), month = date.getMonth();
        var firstDay = new Date(year, month + $scope.monthTracker - 1, 1);
        var lastDay = new Date(year, (month + $scope.monthTracker) + 2, 0);
        queryParam['start'] = {
            '$gte': $filter('date')(firstDay, 'yyyy-MM-dd'),
            '$lte': $filter('date')(lastDay, 'yyyy-MM-dd')
        }
        return JSON.stringify(queryParam)
    }

    $scope.openModalWindow = function (eventObj, edit) {
        $modal.open({
            templateUrl: 'addEvent.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, EventLogService) {
                $rootScope.showAlert('', false, '')
                $scope.reconDetails = []
                $scope.eventDescription = {'selectedRecons':[]}
                BusinessService.get(undefined, undefined, function (data) {
                    angular.forEach(data.data, function (val, key) {
                        if (angular.isDefined(edit) && edit && eventObj['reconID'].indexOf(val['RECON_ID']) != -1) {
                            $scope.reconDetails.push({'name': val.RECON_NAME, 'ticked': true, 'id': val.RECON_ID})
                            $scope.eventDescription['selectedRecons'].push({'name': val.RECON_NAME, 'ticked': true, 'id': val.RECON_ID})
                        } else {
                            $scope.reconDetails.push({'name': val.RECON_NAME, 'ticked': false, 'id': val.RECON_ID})
                        }
                    })
                    if (angular.isDefined(edit) && edit) {
                        $scope.eventDescription['title'] = eventObj['title']
                        $scope.eventDescription['selectedRecons'] = $scope.reconDetails
                    }
                }, function (err) {
                    $rootScope.showAlertMsg(err.msg)
                })

                $scope.addEvent = function (event) {
                    var selectedRecons = []
                    // get selected recons list
                    angular.forEach(event['selectedRecons'], function (val, key) {
                        selectedRecons.push(val['id'])
                    })
                    event['reconID'] = selectedRecons
                    delete event['selectedRecons']

                    if ($scope.modalHeader == 'Edit') {
                        EventLogService.put(eventObj['_id'], event, function (data) {
                            $rootScope.msgNotify('success', 'Event Updated Successfully');
                            $scope.refreshCalendarEvent()
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    } else {
                        event['start'] = $filter('date')(new Date(eventObj['_d']), 'yyyy-MM-dd')
                        EventLogService.post(event, function (data) {
                            $rootScope.msgNotify('success', 'Event Added Successfully');
                            $scope.refreshCalendarEvent()
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }
                }

                $scope.deleteEvent = function () {
                    EventLogService.deleteData(eventObj['_id'],function (data) {
                        $rootScope.msgNotify('success', 'Event Deleted Successfully');
                        $('#calendar').fullCalendar('removeEvents');
                        $scope.refreshCalendarEvent()
                        $scope.cancel()
                    }, function (err) {
                        $rootScope.showAlertMsg(err.msg)
                    })
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel')
                }
            }
        })
    }

    $scope.refreshCalendarEvent()
}