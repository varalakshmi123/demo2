/**
 * Created by ubuntu on 24/11/16.
 */

function reportsController($scope, $window, $upload, $rootScope, $http, $filter, CustomReportService, ReportInterfaceService, $timeout, UpiFailedTransactionReportService,UpiAcqFailedTransactionReportService,  NoAuthService, ReconLicenseService,ChargeBackService) {

    $scope.reportDetails = {}
    $scope.selReportData = {}
    $scope.csvExtractName = ''
    $scope.reportHeaders = []
    $scope.filename = "dummfile"
    $scope.separator = ","
    $scope.nostroReportExtract = {}

    $scope.reportLocMap = {}
    $scope.reportDwnType = 'CSV'
    $scope.chargebackvalues=["Reconciled","ChargeBack","Potential charge back"]

    // TODO migrate to mongo DB is required
    $scope.nostroReportBusinessName = ['Treasury']
    $scope.nostroReportName = {}
    $scope.nostroReportName['Treasury'] = ['Nostro Account Closing Balance']
    $scope.multiDropDownStng = {enableSearch: true, scrollableHeight: '230px', scrollable: true}
    $scope.nostroAdvSearch = {'currency': []}
    $scope.nostroCurrencies = []
    $scope.amtOperator = ''

    //RB Transactions
    $scope.flags =[{"name": "RET"},{"name":"TCC"}];
    $scope.flags_ = {"TCC": [{"name":"Beneficiary account credited manually post reconciliation", "id": "103"},
       {"name":"Beneficiary account has beed credited online", "id": "102"}], "RET":[{"name":"Account closed", "id": "114"},
       {"name":"Account does not exist", "id": "115"},
       {"name":"Party instructions", "id": "116"},
       {"name":"NRI Account", "id": "117"},
       {"name":"Credit freezed", "id": "118"},
       {"name":"Invalid beneficiary details", "id": "119"},
       {"name":"Any other reason", "id": "120"}]}
    $scope.flagDatas = [
        {"name":"Beneficiary account credited manually post reconciliation", "id": "103", "target":"TCC"},
        {"name":"Beneficiary account has beed credited online", "id": "102", "target":"TCC"},
        {"name":"Account closed", "id": "114", "target":"RET"},
        {"name":"Account does not exist", "id": "115", "target":"RET"},
        {"name":"Party instructions", "id": "116", "target":"RET"},
        {"name":"NRI Account", "id": "117", "target":"RET"},
        {"name":"Credit freezed", "id": "118", "target":"RET"},
        {"name":"Invalid beneficiary details", "id": "119", "target":"RET"},
        {"name":"Any other reason", "id": "120", "target":"RET"}
    ]
    // UPI FAILED TRANACTIONS
    $scope.rbRecords =[];
    $scope.getRBDatas = function(date){
	 date = $filter('date')(date,'ddMMyyyy');
	 $rootScope.openModalPopupOpen();
	UpiFailedTransactionReportService.get(undefined, {'query':{'perColConditions':{'statementdate':date}}},function(data){
	    $rootScope.openModalPopupClose();
                angular.forEach(data.data, function(v,k){
		    if(v.flag == ""){
			v.$$flags_x = $scope.flags_['TCC'];
		    }else{
			v.$$flags_x = $scope.flags_[v.flag];
		    }	
		});
                $scope.rbRecords = data.data;
	    if(data.data.length > 0){
                    $scope.rbRecords = data.data;
            }else{
                $rootScope.msgNotify('warning', 'No Data Found!')
            }
	    $scope.processUPICsv();
        });
	/* NoAuthService.getRBData('dummy', date, function(data){
	    $rootScope.openModalPopupClose();
	    if(data != 'No data found' && data.length > 0){
		    $scope.rbRecords = data;
	    }else{
                $rootScope.msgNotify('warning', data)
	    }
	
	},function(data){
	    console.log('Error Loading reconrds');
	    $rootScope.openModalPopupClose();
	});	*/

    }
    $scope.rbRecordsCsv = [];
    $scope.processUPICsv=function(){
	$scope.rbRecordsCsv = [];
        angular.forEach($scope.rbRecords, function(a,b){
           var rbObj = {};
           rbObj['bankadjref'] = a.bankadjref ;
           rbObj['flag'] = a.flag ;
           rbObj['shtdat'] = a.shtdat ;
           rbObj['adjamt'] = a.adjamt ;
           rbObj['shser'] = a.shser ;
           rbObj['shcrd'] = a.shcrd ;
           rbObj['filename'] = a.filename ;
           rbObj['reason'] = a.reason ;
           rbObj['specifyother'] = a.specifyother;
	   angular.forEach($scope.flagDatas, function(c,d){
		if(c.id == a.reason){
           	    rbObj['specifyother'] = c.name;
		    a.flag = c.target;
           	    rbObj['flag'] = a.flag ;
		}
	   })
           $scope.rbRecordsCsv.push(rbObj);
        });
    };
    $scope.updateFlagRecordChange = function(data, index){
	if(angular.isDefined(data._id)){
	    angular.forEach($scope.flagDatas, function(a,b){
		if(data.flag == a.target){
		    $scope.rbRecords[index]['$$flags_x'] = $scope.flags_[data.flag]
		    $scope.rbRecords[index].reason = a.id;
		    $scope.rbRecords[index].specifyother = a.name;
		    return false;
		}
	    });
	    UpiFailedTransactionReportService.put(data._id, data);
	}
	$scope.processUPICsv();
    }
    $scope.updateRecordChange = function(data, index){
	if(angular.isDefined(data._id)){
	    angular.forEach($scope.flagDatas, function(a,b){
		if(data.reason == a.id){
		    $scope.rbRecords[index].specifyother = a.name;
		    $scope.rbRecords[index]['$$flags_x'] = $scope.flags_[data.flag]
		}
	    });
	    UpiFailedTransactionReportService.put(data._id, data);
	}
	$scope.processUPICsv();
    }
    $scope.rbDatasTableOptions_ = function(){
	UpiFailedTransactionReportService.get(undefined, {'query':{}},function(data){
	
		$scope.rbRecords = data.data;
	
	});
        $scope.rbRecords_ = [
            {"bankadjref":"720511805939",
            "flag":"TCC",
            "shtdat":"2017-07-24",
            "adjamt":5000,
            "shser":"720511805939",
            "shcrd":"9017001000101008419",
            "filename":"corp_upload_20170724.csv",
            "reason":"103",
            "specifyother":"Beneficiary account credited manually post reconciliation"
            }
        ];
    };    
    
    
    
    $scope.aflags =[{"name": "DRC"}];
    $scope.aflagDatas = [
        {"name":"Customer A/c Reversed Online", "id": "102", "target":"DRC"},
        {"name":"Customer A/c Reversed Manually", "id": "103", "target":"DRC"},
        {"name":"Customer A/c Not Debited", "id": "104", "target":"DRC"},
    ]
    //UPI ACQ TRANSACTIONS
    $scope.rbAcqRecords =[];
    $scope.getRBAcqDatas = function(date){
	 date = $filter('date')(date,'ddMMyyyy');
	 $rootScope.openModalPopupOpen();
	UpiAcqFailedTransactionReportService.get(undefined, {'query':{'perColConditions':{'statementdate':date}}},function(data){
	    $rootScope.openModalPopupClose();
                $scope.rbAcqRecords = data.data;
	    if(data.data.length > 0){
                    $scope.rbAcqRecords = data.data;
            }else{
                $rootScope.msgNotify('warning', 'No Data Found!')
            }
	    $scope.processAcqUPICsv();
        });
    }
    $scope.rbAcqRecords = [];
    // Charge backup
    $scope.chargeRecords =[];
    $scope.showtable=false
    $scope.getchargeRecords = function(type){
  	$scope.showtable=true
        $rootScope.openModalPopupOpen();
        ChargeBackService.get(undefined, {'query':{'perColConditions':{"status":type}}},function(data){
            $rootScope.openModalPopupClose();
                $scope.chargeRecords = data.data;
            if(data.data.length > 0){
                    $scope.chargeRecords = data.data;
            }else{
                $rootScope.msgNotify('warning', 'No Data Found!')
            }
            //$scope.processAcqUPICsv();
        });
    }
    $scope.chargeRecords = [];

    $scope.processAcqUPICsv=function(){
	$scope.rbAcqRecordsCsv = [];
			angular.forEach($scope.rbAcqRecords, function(a,b){
			   var rbObj = {};
			   rbObj['bankadjref'] = a.bankadjref ;
			   rbObj['flag'] = 'DRC' ;a.flag = 'DRC';
			   rbObj['shtdat'] = a.shtdat ;
			   rbObj['adjamt'] = a.adjamt ;
			   rbObj['shser'] = a.shser ;
			   rbObj['shcrd'] = a.shcrd ;
			   rbObj['filename'] = a.filename ;
			   rbObj['reason'] = a.reason ;
			   rbObj['specifyother'] = a.specifyother;
		   angular.forEach($scope.aflagDatas, function(c,d){
			if(c.id == a.reason){
					rbObj['specifyother'] = c.name;
			}
		   })
           $scope.rbAcqRecordsCsv.push(rbObj);
        });
    };
    $scope.updateAcqRecordChange = function(data){
		if(angular.isDefined(data._id)){
			angular.forEach($scope.flagDatas, function(a,b){
			if(data.reason == a.id){
				data.specifyother = a.name;
			}
			});
			UpiFailedTransactionReportService.put(data._id, data);
		}
		$scope.processAcqUPICsv();
    }
    $scope.rbAcqDatasTableOptions_ = function(){
		UpiAcqFailedTransactionReportService.get(undefined, {'query':{}},function(data){		
			$scope.rbAcqRecords = data.data;		
		});
    };  
    
    
   // Upload Files
	$scope.upload = function (file) {
		$rootScope.openModalPopupOpen()
		$scope.fileData = {}
		$scope.fileData['file_name'] = file[0]['name']
		$upload.upload({
			url: '/api/utilities/' + $scope.fileData['file_name'] + '/upload?fileName=' + $scope.fileData['file_name'],
			method: 'POST',
			file: file[0],
			data: {'fileName': "test", 'description': "TESTING"}
		}).progress(function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		}).success(function (data, status, headers, config) {
			if (data.status == "error") {
				$rootScope.showAlertMsg(data.msg)
			}
			else {
				$rootScope.showSuccessMsg("File Uploaded Succesfully")
				$rootScope.openModalPopupClose()
			}
		})
	} 
 //-----------------------------------------------   
    
    $scope.genReport = function(statementdate){
        sdate = $filter('date')(statementdate, 'ddMMyyyy')
        $rootScope.openModalPopupOpen()
        ReconLicenseService.runReport('dummy', sdate, function (data) {
            $rootScope.openModalPopupClose();
            $window.open('/app/files/recon_reports/'+sdate+'.zip');
        })
    }

//ChargeBack
 $scope.chargeback= function(statementdate){
        $scope.showtable=false
	console.log("////////////////////")
        sdate = $filter('date')(statementdate, 'ddMMyyyy')
        $rootScope.openModalPopupOpen()
        ReconLicenseService.chargeback('dummy', sdate, function (data) {
            $rootScope.openModalPopupClose();
        $rootScope.showSuccessMsg("ChargeBack applied Succesfully")
        }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                console.log(err)
	})
        
    }


    // ag-grid for custom reports
    $scope.reportGridOptions = {
        columnDefs: [],
        rowData: [],
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        cellClicked: true,
        onCellClicked: function (event) {
            console.log(event)
        },
        enableFilter: true,
        groupSuppressAutoColumn: true,
        groupUseEntireRow: true,
        rememberGroupStateWhenNewData: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        suppressRowClickSelection: true
    };

    var filterCount = 0;
    $scope.onFilterChanged = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.reportGridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }


    $scope.generateReport = function () {
        console.log($scope.selReportData)
        $scope.cs_report_status = false
        $scope.reportReconId = $scope.selReportData['reconID']
        $scope.reportName = $scope.selReportData['reportName']
        var statementDate = $scope.toEpoch($scope.statementDate)
        var searchParam = {}
        searchParam['reconId'] = $scope.reportReconId
        searchParam['statementDate'] = $filter('date')(statementDate * 1000, 'dd/MM/yy')
        searchParam['functionName'] = $scope.selReportData['functionName']
        $scope.neftRecons = ['EPYMTS_NEFT_INWARD_APAC_0009', 'EPYMTS_NEFT_INWARD_APAC_0011',
            'EPYMTS_NEFT_OUTWARD_APAC_0010']
        if ($scope.reportReconId == 'CB_CASH_APAC_61171' && $scope.reportName != 'ChargesBack Report') {
            filePath = window.location.origin
            // console.log('here', filePath + '/app/files/report_files/' + $filter('date')(statementDate * 1000, 'ddMMyyyy')+'/' +$scope.selReportData['fileName'])
            // console.log(filePath + '/app/files/report_files/' + '')
            $window.open(filePath + '/app/files/recon_reports/' + $filter('date')(statementDate * 1000, 'ddMMyyyy') + '/' + $scope.selReportData['fileName'])
        } else {
            $rootScope.openModalPopupOpen()

            ReportInterfaceService.generateCustomReports('dummy', searchParam, function (data) {
                $rootScope.openModalPopupClose()
                console.log(data)
                $scope.cs_report_status = false
                if ($scope.reportReconId == 'CB_CASH_APAC_61171') {
                    if (data != 'No Records.' && data != 'File does not exits') {
                        filePath = window.location.origin
                        console.log(filePath + '/app/files/report_files/' + data)
                        $window.open(filePath + '/app/files/recon_reports/' + data)
                    } else {
                        $rootScope.msgNotify('warning', data)
                    }

                }
                else if ($scope.reportReconId == 'EPYMTS_CASH_APAC_0012' && angular.isDefined(data['prnpalData'])) {
                    if (!angular.isDefined(data['prnpalData']) && !angular.isDefined(data['prnpalData'])) {
                        $scope.principalAccData = []
                        $scope.settlementAccData = []
                    } else {
                        $scope.principalAccData = JSON.parse(data['prnpalData'])
                        $scope.settlementAccData = data['settlementData']
                        $scope.reportGridOptions['rowData'] = [12]

                    }
                } else if ($scope.neftRecons.indexOf($scope.reportReconId) != -1) {
                    $scope.neftReportData = JSON.parse(data)
                    if (JSON.parse(data).length == 0) {
                        $rootScope.showAlertMsg("No data found for the selected date")
                    } else {
                        $scope.colHeader = Object.keys($scope.neftReportData[0])
                    }
                }
                else {
                    $scope.cs_report_status = true
                    $scope.reportGridOptions.columnDefs = [];
                    $scope.reportGridOptions.rowData = [];
                    if (angular.isDefined($scope.reportGridOptions.api)) {
                        $scope.reportGridOptions.api.setColumnDefs([])
                        $scope.reportGridOptions.api.setRowData([])
                    }

                    if (angular.isDefined(data) && data != {} && data != null) {
                        if (JSON.parse(data).length == 0) {
                            $rootScope.openModalPopupClose()
                            $rootScope.showAlertMsg("No data found for this selection")
                        }
                        else {
                            //console.log(JSON.parse(data))
                            $scope.headers = []
                            $scope.columnDefs = []
                            angular.forEach(JSON.parse(data), function (v, k) {
                                $scope.headers = Object.keys(v)
                            })
                            angular.forEach($scope.headers, function (val, key) {
                                $scope.columnDefs.push({
                                    'headerName': val,
                                    'suppressAggregation': true,
                                    'field': val,
                                    'width': 302
                                })


                            })
                            console.log($scope.columnDefs)


                            $rootScope.openModalPopupClose()
                            $scope.reconsData = JSON.parse(data)
                            $scope.reportGridOptions.columnDefs = $scope.columnDefs;
                            $scope.reportGridOptions.rowData = $scope.reconsData;
                            $scope.reportGridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.reportGridOptions.api.setRowData($scope.reconsData)
                        }
                    } else {
                        $rootScope.openModalPopupClose()
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                }

            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                console.log(err)
            })
        }

    }
    // ag-grid for custom reports
    $scope.onReportExport = function(statementdate){
	  sdate = $filter('date')(statementdate, 'ddMMyyyy')
          $window.open('/app/files/recon_reports/'+sdate+'.zip');
    }
    $scope.onBtExport = function () {
        $scope.exportFlag = true
        console.log($scope.reportReconId)
        if ($scope.reportReconId == 'EPYMTS_CASH_APAC_0012') {
            console.log($scope.downloadFilePath)
            filePath = window.location.origin
            console.log(filePath)
            $window.open(filePath + '/app/files/P_ACC_REPORT.xlsx')
            return
        }
        if ($scope.reportGridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("No data to export")
        }


        $scope.customHeader = "Recon_Name :," + $scope.reportName + '\n' + "Recon_Id :," + $scope.reportReconId + '\n' + "Date-Time :," + new Date() + '\n' + "User :," + $rootScope.credentials.userName + '\n' + '\n'

        var params = {
            processCellCallback: function (params) {
                if (params.value != null) {
                    if (typeof params.value == "number") {
                        if (params.value.toString().length == 13 && params['column']['colId'].indexOf("AMOUNT") == -1) {
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else {
                            return params.value
                        }
                    }
                    else {
                        return params.value
                    }
                }
                //console.log(params.column.getColumnDef())
            },
            customHeader: $scope.customHeader,
            fileName: $scope.reportName + '-' + $filter('date')(new Date(Date.now())) + '.csv',
        };

        if ($scope.exportFlag) {
            //console.log($scope.gridOptions)
            $scope.reportGridOptions.api.exportDataAsCsv(params);
        }
    }

    

    $scope.exportCSVReport = function (exportData) {
        $scope.csvReportData = [];
        $scope.csvHeader = ''
        $scope.csvHeader = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        $scope.csvReportData.push({
            "GL_ACC_NUM": "GL Account Number",
            "GL_NAME": "GL Name",
            "CLOS_BALS1": "Closing Balance of " + $scope.source1,
            "CLOS_BALS2": "Closing Balance of " + $scope.source2,
            "DIFF": "Difference b/w " + $scope.source1 + " - " + $scope.source2,
            "VALUE_GRE": "Value Greater in " + $scope.source1 + '/' + $scope.source2
        })
        $scope.filename = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        for (i = 0; i < exportData.length; i++) {
            $scope.csvReportData.push({
                "GL_ACC_NUM": exportData[i]['C40'],
                "GL_NAME": exportData[i]['ACCOUNT_NAME'],
                "CLOS_BALS1": $filter('number')(exportData[i]['CLOSING_BALS1'], 2),
                "CLOS_BALS2": $filter('number')(exportData[i]['CLOSING_BALS2'], 2),
                "DIFF": $filter('number')(exportData[i]['DIFF'], 2),
                "VALUE_GRE": exportData[i]['VALUE_GRE']
            })
        }
        //angular.element('#csvReport').triggerHandler('click');
        //$("#csvReport").trigger('click');
        $timeout(function () {
            angular.element("#csvReport").triggerHandler('click');
        }, 0);
        return false;

    }

    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };

    function retrieveCustomReportDef() {
        reponse = {}
        console.log('cdcacs')
        CustomReportService.get(undefined, undefined, function (data) {
            if (data['total'] > 0) {
                $scope.reportDetails = data['data']
            }
        }, function (error) {
            console.log(error)
        })
    }

    $scope.statementDate = 0
    $scope.chargeBackDate = 0
    $scope.rbTransDate = 0;
    $scope.rbAcqTransDate = 0;
    
    $scope.rbDateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,
    };
    
    $scope.rbAcqDateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,
    };

    $scope.rbChargeBackDateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.getConsolidateReport = function () {
        query = {}
        query['reconId'] = ['EPYMTS_CASH_APAC_0012', "EPYMTS_NEFT_INWARD_APAC_0011", "EPYMTS_NEFT_INWARD_APAC_0009"]
        // query['reconId'] = ['EPYMTS_NEFT_INWARD_APAC_0009','EPYMTS_NEFT_OUTWARD_APAC_0010','EPYMTS_NEFT_INWARD_APAC_0011']
        ReportInterfaceService.getConsolidatedReport('dummy', query, function (data) {
            $scope.colLabel = data['distinct_src']
            $scope.subCols = []
            $scope.rowIndex = []
            $scope.consolidatedReport = JSON.parse(data['report_data'])
            console.log($scope.consolidatedReport)

            $scope.colLabel.forEach(function (column) {
                $scope.subCols = $scope.subCols.concat(['AMOUNT', 'COUNT'])
                $scope.rowIndex = $scope.rowIndex.concat(['sum_AMOUNT_' + column, 'sum_COUNT_' + column])
            })

        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
            console.log(err)

        })
    }

    $scope.exportreport = function (rowData, colData) {
        console.log(rowData[colData], colData)
        query['reconId'] = rowData['RECON_ID']
        query['filters'] = {'CBS': ""}
        query['recon_name'] = ''
        query['source'] = colData.split('_')[2]
        if (rowData[colData] > 0) {
            ReportInterfaceService.exportStatus('dummy', query, function (data) {
                var filePath = window.location.origin + '/app/files/recon_reports/'
                console.log(filePath + data)
                $window.open(filePath + data)
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                console.log(err)
            })
        }
    }

    retrieveCustomReportDef()
}

