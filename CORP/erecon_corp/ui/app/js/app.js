var app = angular.module("webApp");
app.config(["$routeProvider", function($routeProvider) {
    //Configure application routes
    $routeProvider.when('/login', {
        templateUrl: '/app/partials/login.html'
    }).when('/dashboard', {
        templateUrl:'app/partials/dashboard.html',
        controller: dashboardController
    })
//    .when('/users', {
//        templateUrl:'app/partials/migratedSystems.html',
//        controller: migratedSystemsController
//    })
    .when('/userpool', {
        templateUrl:'app/partials/userpool.html',
        controller: userpoolController
    }).when('/accountpool', {
        templateUrl:'app/partials/accountpool.html',
        controller: accountpoolController
    }).when('/reconassignments', {
        templateUrl:'app/partials/reconassignments.html',
        controller: reconAssignController
    }).when('/users', {
            templateUrl:'/app/partials/users.html',
            controller: usersController
    }).when('/recons', {
            templateUrl:'/app/partials/recons.html',
            controller: reconsController
    }).when('/summary', {
            templateUrl:'/app/partials/summary.html',
            controller: summaryController
    }).when('/reconslicense', {
            templateUrl:'/app/partials/reconlicense.html',
            controller: reconlicController
    }).when('/rollback', {
            templateUrl:'/app/partials/rollback.html',
            controller: rollbackController
    }).when('/feedloading', {
            templateUrl:'/app/partials/feedloading.html',
            controller: feedloadingController
    }).when('/reports', {
        templateUrl: '/app/partials/customReports.html',
        controller: reportsController
    }).when('/reports_', {
        templateUrl: '/app/partials/reports_.html',
        controller: reportsController
    }).when('/reconmigration', {
    	templateUrl: '/app/partials/reconmigration.html',
    	controller: reconsMigrationController
    }).when('/calendar', {
    	templateUrl: '/app/partials/calendar.html',
    	controller: calendarController
    })
}]);

