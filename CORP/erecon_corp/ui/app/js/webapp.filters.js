
var app = angular.module("webApp.filters",[]);

app.filter("tolocal", function(){
    return function(input) {
        var offset = new Date().getTimezoneOffset() * 60;
        var d = new Date((input-offset)*1000);
        return d.toString();
    }
});

app.filter("arraytocsv", function(){

    return function(input, maxlength) {
        if(!angular.isDefined(maxlength))
        {
            maxlength = 10;
        }
        var resp = "";
        if(angular.isArray(input))
        {
            resp = input.join(", ");
            if(angular.isDefined(maxlength) && resp.length > maxlength)
            {
                resp = resp.substr(0, maxlength-4);
                resp += " ...";
            }
        }
        else
        {
            //console.log("arraytocsv: Input not array!!!");
            resp = input;
        }
        return resp;
    }
});

app.filter("showForRole", ['$rootScope', function($rootScope){
    return function(input) {
        if(input)
        {
            return ($rootScope.credentials && input.indexOf($rootScope.credentials["role"]) >= 0);
        }
        return true;
    }
}]);
