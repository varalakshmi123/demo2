/**
 * Created by hash on 9/8/16.
 */
/**
 * Created by swami on 13/4/15.
 */
function reconschController($scope, $rootScope, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter) {
    $scope.recon = {}
    $scope.recon.statementDate = 0
    $scope.getTimes = function () {
        $scope.hours = [
            {key: "12AM", value: 0},
            {key: "1AM", value: 1},
            {key: "2AM", value: 2},
            {key: "3AM", value: 3},
            {key: "4AM", value: 4},
            {key: "5AM", value: 5},
            {key: "6AM", value: 6},
            {key: "7AM", value: 7},
            {key: "8AM", value: 8},
            {key: "9AM", value: 9},
            {key: "10AM", value: 10},
            {key: "11AM", value: 11},
            {key: "12PM", value: 12},
            {key: "1PM", value: 13},
            {key: "2PM", value: 14},
            {key: "3PM", value: 15},
            {key: "4PM", value: 16},
            {key: "5PM", value: 17},
            {key: "6PM", value: 18},
            {key: "7PM", value: 19},
            {key: "8PM", value: 20},
            {key: "9PM", value: 21},
            {key: "10PM", value: 22},
            {key: "11PM", value: 23}
        ];
        $scope.mins = [
            {key: "00", value: 0},
            {key: "15", value: 15},
            {key: "30", value: 30},
            {key: "45", value: 45}
        ];
    }

    $scope.getRecons = function () {
        $scope.reconList = [];
        // BusinessService.get(undefined, undefined, function (data) {
        //
        //     if (data.data.length > 0) {
        //         angular.forEach(data.data, function (v, k) {
        //             $scope.reconList.push({
        //                 'id': v.RECON_ID,
        //                 'name': v.RECON_NAME
        //             })
        //         });
        //     }
        // })
    }
    // $scope.getRecons()
    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };
    $scope.runRecon = function (recon) {
        console.log(recon)
    }
    $scope.scheduleRecon = function (recon) {
        $modal.open({
            templateUrl: 'scheduleRecon.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, ReconJobService) {
                $scope.getTimes()
                $scope.toEpoch = function (date) {
                    return Math.round(new Date(date) / 1000.0);
                };
                var data = {}
                data['statementDate'] = $scope.toEpoch(recon['statementDate'])
                data['reconName'] = recon['selectedRecon']
                console.log(recon)
                $scope.schedule = function (rec) {
                    data['hour'] = rec['hour']
                    data['min'] = rec['min']
                    $scope.timezone = (data['hour'] <= 12) ? data['hour'] : data['hour'] - 12
                    $scope.meridian = (data['hour'] <= 11) ? "AM" : "PM"
                    data['time'] = $scope.timezone + ":" + rec['min'] + " " + $scope.meridian
                    ReconJobService.post(data, function (data) {
                        console.log(data)
                        $modalInstance.dismiss('cancel');
                        $rootScope.showSuccessMsg('Recon Scheduled Succesfully')
                        $scope.reconScheduleOptions.reQuery = true
                    })
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }
    $scope.reconScheduleOptions = {};
    $scope.reconScheduleOptions.init = false;
    //$scope.reconScheduleOptions.isLoading = true;
    $scope.getScheduledRecons = function () {

        $scope.userList = [];
        $scope.reconScheduleOptions.searchFields = {}
        $scope.reconScheduleOptions.service = ReconJobService;
        $scope.reconScheduleOptions.headers = [
            [
                {name: "Recon Name", searchable: true, sortable: true, dataField: "reconName", colIndex: 0},
                {
                    name: "StatementDate",
                    searchable: true,
                    sortable: true,
                    dataField: "statementDate",
                    filter: 'formatTimeStamp',
                    colIndex: 1
                },
                {
                    name: "Scheduled Start Time",
                    searchable: true,
                    sortable: true,
                    dataField: "time",
                    colIndex: 1
                },
                {
                    name: "Actions",
                    colIndex: 4,
                    width: '10%',
                    actions: [{
                        toolTip: "Reschedule",
                        action: "reschedule",
                        posticon: "fa fa-clock-o fa-lg"
                    }, {
                        toolTip: "Delete",
                        action: "deleteFunc",
                        posticon: "fa fa-trash-o fa-lg"
                    }]
                }
            ]
        ];
        //ReconJobService.get(undefined,undefined, function (data) {
        //    if (data.data.length > 0){
        //        $scope.reconScheduleOptions.dat
        //    }
        //})
        $scope.reconScheduleOptions.reschedule = function (row) {
            console.log(row)
            $scope.getTimes()
            $scope.rec = {}
            $scope.rec['hour'] = row['hour']
            $scope.rec['min'] = row['min']
            $modal.open({
                templateUrl: 'rescheduleRecon.html',
                windowClass: 'modal addUser',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance, ReconJobService) {
                    var data = row
                    $scope.schedule = function (rec) {
                        data['hour'] = rec['hour']
                        data['min'] = rec['min']
                        $scope.timezone = (data['hour'] <= 12) ? data['hour'] : data['hour'] - 12
                        $scope.meridian = (data['hour'] <= 11) ? "AM" : "PM"
                        data['time'] = $scope.timezone + ":" + rec['min'] + " " + $scope.meridian
                        ReconJobService.put(data['_id'], data, function (data) {
                            console.log(data)
                            $modalInstance.dismiss('cancel');
                            $rootScope.showSuccessMsg('Recon Rescheduled Succesfully')
                            $scope.reconScheduleOptions.reQuery = true
                        })
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }
        $scope.reconScheduleOptions.deleteFunc = function (data) {
            $modal.open({
                templateUrl: 'deleteschedule.html',
                windowClass: 'modal alert alert-error popupLarge',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance, ReconJobService) {
                    $scope.confirmDelete = function () {
                        $rootScope.openModalPopupOpen();
                        ReconJobService.deleteData(data["_id"], function (data) {
                            $rootScope.openModalPopupClose();
                            $scope.reconScheduleOptions.reQuery = true;
                            $modalInstance.dismiss('cancel');
                        }, function (errorData) {
                            $scope.cancel();
                            $rootScope.openModalPopupClose();
                            $scope.showErrormsg(errorData.msg);
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }

    }
    $scope.getScheduledRecons()


}