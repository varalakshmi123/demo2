function accountpoolController($scope, $rootScope, $routeParams, $location, $timeout, $route, $window, $modal, UsersService, UserPoolService, BusinessService, AccountPoolService) {
//        if($rootScope.credentials.role != 'admin'){
//            $rootScope.msgNotify('error','Access restricted to view schedules.');
//            $location.path("/migratedSystems");
//            return false;
//        }
    $scope.accountpoolOptions = {};
    $scope.accountpoolOptions.init = false;
    $scope.accountpoolOptions.isLoading = true;
    $scope.accountpoolOptions.isTableSorting = "name";
    $scope.accountpoolOptions.sortField = "name";
    //      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.accountpoolOptions.searchFields = {}
        $scope.accountpoolOptions.service = AccountPoolService;
        $scope.accountpoolOptions.headers = [
            [
                {name: "Name", searchable: true, sortable: true, dataField: "name", colIndex: 0},
//                    {name: "Users", searchable: true, sortable: true, dataField: "users", colIndex: 1},
                {name: "Accounts", searchable: true, sortable: true, dataField: "accounts", colIndex: 2},
                //                {name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
                {
                    name: "Actions",
                    colIndex: 4,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addUser",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.accountpoolOptions.disableEdit = function (data, index, action) {
            if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
                return true;
            }
            return false;
        }


        $scope.accountpoolOptions.hideButton = function (data, index, action) {
            if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
                return true;
            }
            return false;
        }
        $scope.accountpoolOptions.reQuery = true;
        //          $scope.init();
        //          console.log($scope.accountpoolOptions)
        //            $scope.init=function(){
        UserPoolService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.accountpoolOptions.checkRole = function (data, index, action) {
        if (data.role == "admin") {
            return false;
        }
        return true;
    }
    $scope.accountpoolOptions.addUser = function (userData) {
        $modal.open({
            templateUrl: 'addUser.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                $scope.mode = "Add";
                $scope.userData = {};
                $scope.tmpData = {};
                $scope.userData["role"] = "customer";
                $rootScope.isDisable = false;
                $scope.tagData = []
                $scope.arraycheck = []
                $scope.business_Data = []
                UsersService.get(undefined, undefined, function (data) {
                    console.log(data)
                    angular.forEach(data.data, function (val, key) {
                        $scope.tagData.push({'name': val.userName, 'id': val.userName})
                    })
                })
                BusinessService.get(undefined, undefined, function (data) {
                    console.log(data)
                    angular.forEach(data.data, function (val, key) {
                        if ($scope.arraycheck.indexOf(val.WORKPOOL_NAME) == -1) {
                            $scope.arraycheck.push(val.WORKPOOL_NAME)
                            $scope.business_Data.push({'name': val.WORKPOOL_NAME, 'id': val.BUSINESS_CONTEXT_ID})

                        }

                    })
                })
                $scope.tagAllOptions = new Select2Tagging($scope.tagData, 'name');
                $scope.tagAllOptionsbusiness = new Select2Tagging($scope.business_Data, 'name');
                function Select2Tagging(items, formatProperty) {
                    if (!formatProperty) {
                        formatProperty = "text";
                    }

                    console.log("Select2Tagging init");

                    var f = {};
                    f.formatProperty = formatProperty;

                    f.data = items;

                    var itemMap = {};
                    $.each(items, function (index, value) {
                        itemMap[value.id] = value;
                    });

                    f.initSelection = function (element, callback) {
                        var data = [];
                        var eleVal = element.val();
                        console.log("initSelection " + eleVal);
                        //debugger
                        var ids = eleVal.split(",");
                        $(ids).each(function () {
                            var id = this;
                            $(f.data).each(function () {
                                if (id.localeCompare("" + this.id) == 0) data.push(this);
                            });
                        });
                        callback(data);
                    };

                    f.createSearchChoice = function (term, data) {
                        console.log("createSearchChoice " + term + " data " + data);
                        if ($(data).filter(function () {
                                    return this[f.formatProperty].localeCompare(term) === 0;
                                    //return this.text.localeCompare(term) === 0;
                                }
                            ).length === 0) {
                            var result = {
                                id: term,
                                //text: term
                            };
                            result[f.formatProperty] = term;
                            return result;
                        }
                    };

                    formatResult = function (item) {
                        console.log("formatResult " + item);
                        if (item[f.formatProperty]) {
                            return item[f.formatProperty];
                        } else {
                            return item;
                        }

                    };

                    formatSelection = function (item) {
                        if (item[f.formatProperty]) {
                            return item[f.formatProperty];
                        } else {
                            return item;
                        }

                    };

                    f.formatResult = formatResult;
                    f.formatSelection = formatSelection;
                    f.multiple = true;
                    return f;
                }

                if (angular.isDefined(userData)) {
                    $scope.mode = "Edit";
                    $scope.userData = userData;
                }

                $scope.saveUser = function (userData) {
                    $rootScope.isDisable = true;
                    console.log(userData)
                    if (angular.isDefined($scope.userData._id)) {
                        console.log(userData)
                        $scope.business_context_data = []
                        $scope.users_data = []
                        if (typeof userData['business_context'] != "string") {
                            angular.forEach(userData.business_context, function (va, ke) {
                                console.log(va)
                                $scope.business_context_data.push(va.id)
                            })
                        }
                        if (typeof userData.users != "string") {
                            angular.forEach(userData.users, function (v, k) {
                                console.log(v)
                                $scope.users_data.push(v.id)
                            })
                        }

                        if (typeof userData['business_context'] != "string") {
                            userData['business_context'] = $scope.business_context_data.join(',')
                        }
                        if (typeof userData['users'] != "string") {
                            userData['users'] = $scope.users_data.join(',')
                        }
                        console.log(userData)
                        $rootScope.openModalPopupOpen();
                        UserPoolService.put(userData["_id"], $scope.userData, function (data) {
                            $rootScope.msgNotify('success','UserPool Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.accountpoolOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    } else {
                        $rootScope.openModalPopupOpen();
                        UserPoolService.post($scope.userData, function (data) {
                            $rootScope.msgNotify('success','UserPool Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.accountpoolOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.updatePassword = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.password)) {
                        if ($scope.userData.password == $scope.tmpData.pw2) {
                            $rootScope.openModalPopupOpen();
                            UserPoolService.changePassword(userData["_id"], $scope.userData.password, function (data) {
                                $rootScope.msgNotify('success','Successfully updated');
                                $rootScope.openModalPopupClose();
                                $timeout(function () {
                                    $scope.cancel();
                                }, 3000);
                            });
                        } else {
                            $rootScope.handleErrorMessages({'msg': 'Password mismatch.'})
                        }
                    }
                };
                $scope.updateQuota = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.quota)) {
                        $rootScope.openModalPopupOpen();
                        UserPoolService.setQuota(userData["_id"], $scope.userData.quota, function (data) {
                            $rootScope.msgNotify('success','Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.updateEmail = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.email)) {
                        $rootScope.openModalPopupOpen();
                        UserPoolService.setEmail(userData["_id"], $scope.userData.email, function (data) {
                            $rootScope.msgNotify('success','Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                    if (angular.isDefined(userData) && angular.isDefined(userData.fromEmail)) {
                        UserPoolService.setFromEmail(userData["_id"], $scope.userData.fromEmail, function (data) {
                            $rootScope.msgNotify('success','Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }

                };
                $scope.updateStatus = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.isActive)) {
                        userData.isActive = !userData.isActive;
                        $rootScope.openModalPopupOpen();
                        UserPoolService.updateStatus(userData["_id"], userData.isActive, function (data) {
                            $rootScope.msgNotify('success','Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.accountpoolOptions.reQuery = true;
                };
            }
        });
    };
    $scope.confirmDelete = angular.noop();

    $scope.accountpoolOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    UserPoolService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.accountpoolOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    //      }
    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }
    $scope.getUsers();
    /*      $rootScope.maxQuotaLimit=2000;
     $scope.calMaxLimitForUser = function(data,rQuota)
     {
     $rootScope.maxQuotaLimit=0;
     $scope.totalUsedQuota=0;
     angular.forEach(data, function(value,index){
     if(value.role=="customer")
     {
     $scope.totalUsedQuota+=value.quota;
     }
     });
     $rootScope.maxQuotaLimit=rQuota-$scope.totalUsedQuota;
     $scope.accountpoolOptions.reQuery = true;

     }
     function init(){
     $rootScope.maxQuotaLimit=2000;
     UserPoolService.get(undefined, undefined, function (data) {
     $scope.users = data.data;
     angular.forEach(data.data, function(value,index){
     if(value.role=="reseller")
     {
     $scope.calMaxLimitForUser($scope.users,value.quota);
     }
     });
     });
     }
     init();*/


//         $scope.init = function(){
//         UserPoolService.get(undefined, undefined, function (data) {
//            $scope.users = data;
//            $scope.available=0;
//            angular.forEach($scope.users.data, function (value, key) {
//                if($rootScope.credentials.role=='reseller' && value.role=='reseller')
//                {
//                    $rootScope.maxQuotaLimit=value.quota;
//                }
//                else if($rootScope.credentials.role=='reseller' && value.role!='reseller')
//                {
//                    $scope.available=$scope.available+value.quota;
//                }
//            });UserManagementService
//                $rootScope.availableQuotaForReseller=$rootScope.maxQuotaLimit-$scope.available;
//
//            });
//         }
//      init();
    $scope.usersType = ["Administrator", "Operations Manager", "Reconciler", "Dept User", "Configurer", "Enquirer", "Authorizer"]
//    AclsService.get(undefined, undefined, function (data) {
//        for (var i = 0; i < data.data.length; i++) {
//            if (data.data[i].role == 'admin') {
//                data.data.splice(i, 1);
//            }
//        }
//        $scope.acls = data.data;
//        $scope.usersType = [];
//        angular.forEach($scope.acls, function (valData, index) {
//            var tempData = {};
//            tempData['key'] = valData.role;
//            tempData['val'] = $filter('toUser')(valData.role);
//            $scope.usersType.push(tempData);
//        });
//    });

}
