function reconsController($scope, $rootScope, $http, $upload, $routeParams, $filter, $location, $timeout, $route, $window, $modal, UsersService, ReconAssignmentService, BusinessService, UserPoolService, FileUploadService, AccountPoolService, ReconJobPost) {
    $scope.showDateFilter = false
    console.log($rootScope.credentials)
    $rootScope.userpoolRole = ''
    $scope.business_context_data = []
    $scope.allocated_recons = []
    $scope.businessContexts = []
    $scope.reconData = {};
    $scope.businessContext = ''
    $scope.singleSide = false
    $scope.showGrid = false
    $scope.auditGridOptionsFlag = false
    $scope.ignoreTransaction = ['GLBAL_CASH_APAC_20461']
    $scope.amountFrmtCols = ['CLOSING_BALANCE_AMOUNT', 'AMOUNT', 'OPENING_BALANCE_AMOUNT', 'AMOUNT_1', 'AMOUNT_2']
    $scope.txnCount = 0
    // validation params
    $scope.currencyListselectionChangedFunc = []
    $scope.txnAmount = 0
    //$scope.reconId = ''
    $scope.clearDates = function () {
        $scope.matchedTabFilter = {}
        $scope.date = {}
        $scope.date.executionStartDate = 0
        $scope.date.executionEndDate = 0
        $scope.date.statementStartDate = 0
        $scope.date.statementEndDate = 0
    }
    $scope.date = {}
    $scope.matchedTabFilter = {}
    $scope.date.executionStartDate = 0
    $scope.date.executionEndDate = 0
    $scope.date.statementStartDate = 0
    $scope.date.statementEndDate = 0

    $scope.inlineOptions = {
        maxDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.maxDate = $scope.inlineOptions.maxDate ? null : new Date();
        $scope.dateOptions.maxDate = $scope.inlineOptions.maxDate;
    };

    $scope.toggleMin();


    $scope.allFlag = false
    $scope.getUserpools = function () {
        $scope.userpools = []
        var perColConditions = {};
        perColConditions["role"] = "Authorizer"
        perColConditions["business_context"] = $scope.businessContext
        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            //console.log(data)
            $scope.userpools = data.data
        })
    }

    $scope.getDeptUserpools = function () {
        $scope.deptuserpools = []
        var perColConditions = {};
        perColConditions["role"] = "Dept User"
        perColConditions["business_context"] = $scope.businessContext
        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            //console.log(data)
            $scope.deptuserpools = data.data
        })
    }
    $scope.getReconsData = function () {
        $scope.businessArrayCheck = []
        $scope.businessContexts = []
        if (angular.isDefined($rootScope.credentials) && $rootScope.credentials != null && $rootScope.credentials != '') {
            angular.forEach($rootScope.allBusinessData, function (a, b) {
                angular.forEach($rootScope.business_context_data, function (val, key) {
                    if (a['BUSINESS_CONTEXT_ID'] == val) {
                        if ($scope.businessArrayCheck.indexOf(a['BUSINESS_CONTEXT_ID']) == -1) {
                            $scope.businessArrayCheck.push(a['BUSINESS_CONTEXT_ID'])
                            $scope.businessContexts.push({
                                'name': a.WORKPOOL_NAME,
                                'id': a.BUSINESS_CONTEXT_ID,
                                'process': a.BUSINESS_PROCESS_NAME
                            })
                        }
                    }
                })
            })
        }
        console.log($rootScope.business_context_data)
        console.log($scope.businessContexts)
        if ($rootScope.allocated_recons.length > 0) {
            $scope.reconBusinessData = []
            $scope.reconBusinessCheck = []
            $scope.showBusTable = true
            angular.forEach($rootScope.allocated_recons, function (val, key) {
                angular.forEach($rootScope.allBusinessData, function (v, k) {
                    if (v.RECON_ID == val.recon) {
                        if ($scope.reconBusinessCheck.indexOf(v.RECON_ID) == -1) {
                            $scope.reconBusinessCheck.push(v.RECON_ID)
                            $scope.reconBusinessData.push({
                                'id': v.RECON_ID,
                                'name': v.RECON_NAME,
                                'business': val.business,
                                'userpool': val.userpool
                            })
                        }

                    }
                })
            })
            if (angular.isDefined($scope.businessContext) && $scope.reconBusinessData.length > 0) {
                $scope.getRecons($scope.businessContext)
            }
            console.log($scope.reconBusinessData)

        }

//            //console.log($scope.business_context_data)
        $scope.gridOptions = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,

            //onRowSelected: rowSelectedFunc,
            onSelectionChanged: selectionChangedFunc,
            onRowClicked: function (row) {

                if ($scope.selectedFilter != 'UnMatched') {   //'waitingForAuthorization'
                    if (angular.isDefined(row['node']) && row['node'] != null) {
                        console.log(row['node'])
                        console.log($scope.gridOptions.api)
                        $scope.clearAllRowSelection();
                        $scope.gridOptions.api.forEachNode(function (node) {
                            if (node.expanded != false) {
                                if (angular.isDefined(node.parent)) {
                                    if (node.expanded || node.parent.expanded)
                                        node.setSelected(true);
                                }

                            } else {
                                node.setSelected(false);
                            }
                        });
                    }
                }
                var lastSelectedRow = this.appSpecific.lastSelectedRow;
                var shiftKey = row.event.shiftKey,
                    ctrlKey = row.event.ctrlKey;

                // If modifier keys aren't pressed then only select the row that was clicked
                if (!shiftKey && !ctrlKey) {
                    //this.api.deselectAll();
                    if (angular.isUndefined(row.node.expanded) || row.node.expanded)
                        this.api.selectNode(row.node, true);
                }
                // If modifier keys are used and there was a previously selected row
                else if (lastSelectedRow !== undefined) {
                    // Select a block of rows
                    if (shiftKey) {
                        var startIndex, endIndex;
                        // Get our start and end indexes correct
                        if (row.rowIndex < lastSelectedRow.rowIndex) {
                            startIndex = row.rowIndex;
                            endIndex = lastSelectedRow.rowIndex;
                        }
                        else {
                            startIndex = lastSelectedRow.rowIndex;
                            endIndex = row.rowIndex;
                        }
                        // Select all the rows between the previously selected row and the
                        // newly clicked row
                        for (var i = startIndex; i <= endIndex; i++) {
                            this.api.selectIndex(i, true, true);
                        }
                    }
                    // Select one more row
                    if (ctrlKey) {
                        this.api.selectIndex(row.rowIndex, true);
                    }
                }
                // Store the recently clicked row for future use
                this.appSpecific.lastSelectedRow = row;
                selectionChangedFunc();
            },
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            groupColumnDef: null,
            //groupHeaders: true,
            //rowHeight: 22,
            groupSelectsChildren: false,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            suppressRowClickSelection: true,
        }

        $scope.gridOptionsAll = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,
            //onRowSelected: rowSelectedFunc,
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            groupColumnDef: null,
            //groupHeaders: true,
            //rowHeight: 22,
            groupSelectsChildren: false,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            pagination: true,
            suppressRowClickSelection: true
        };
        $scope.auditGridOptions = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,
            //onRowSelected: rowSelectedFunc,
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            groupColumnDef: null,
            //groupHeaders: true,
            //rowHeight: 22,
            groupSelectsChildren: false,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            suppressRowClickSelection: true
        };
        $scope.allocationGridOptions = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,
            //onRowSelected: rowSelectedFunc,
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            groupColumnDef: null,
            //groupHeaders: true,
            //rowHeight: 22,
            groupSelectsChildren: false,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            suppressRowClickSelection: true
        };
        $scope.inboxGridOptions = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,
            //onRowSelected: rowSelectedFunc,
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            groupColumnDef: null,
            //groupHeaders: true,
            //rowHeight: 22,
            groupSelectsChildren: false,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            suppressRowClickSelection: true
        };

    }
    //$scope.getReconsData()
    $scope.clearAllRowSelection = function () {
        $scope.gridOptions.api.deselectAll();
    }
    $scope.sizeToFit = function () {
        $scope.gridOptions.api.sizeColumnsToFit();
    }
    $scope.selectAllRows = function () {
        if ($scope.gridOptions.api.isAnyFilterPresent()) {
            $scope.gridOptions.api.forEachNodeAfterFilter(function (node) {
                node.setSelected(true);
            });
        } else {
            $scope.gridOptions.api.selectAll();
        }
    }
    function rowSelectedFunc(event) {
        console.log(event)
        //window.alert("row " + event.node.data.athlete + " selected = " + event.node.selected);
    }

    Array.prototype.unique = function () {
        var a = [];
        for (i = 0; i < this.length; i++) {
            var current = this[i];
            if (a.indexOf(current) < 0) a.push(current);
        }

        this.length = 0;
        for (i = 0; i < a.length; i++) {
            this.push(a[i]);
        }

        return this;
    }
    $scope.amountTotal = 0;
    function selectionChangedFunc() {
        $scope.auditGridOptionsFlag = false
        $scope.showGrid = false
        $scope.linkList = []
        $scope.currencyList = []
        if (angular.isDefined($scope.gridOptions)) {
            //$scope.gridOptions.api.showLoadingOverlay()
            //$rootScope.openModalPopupClose()
            $scope.totalS1 = []
            $scope.totalS2 = []
            $scope.totalS3 = []
            $scope.amountTotal = 0;
            $scope.total_trans1 = 0;
            $scope.total_debit1 = 0;
            $scope.total_credit1 = 0;
            $scope.total_trans2 = 0;
            $scope.total_debit2 = 0;
            $scope.total_credit2 = 0;
            $scope.total_trans3 = 0;
            $scope.total_debit3 = 0;
            $scope.total_credit3 = 0;
            angular.forEach($scope.gridOptions.api.getSelectedNodes(), function (a, b) {
                //console.log(a)
                if (angular.isDefined(a['data']) && a['data'] != null) {
                    if ($scope.linkList.indexOf(a['data']['LINK_ID']) == -1) {
                        $scope.linkList.push(a['data']['LINK_ID'])
                    }
                }
                if (a.data != null) {
                    if (a.data.SOURCE_TYPE == "S1") {
                        $scope.total_trans1 += 1
                        if ($scope.totalS1.length == 0) {
                            $scope.totalS1.push({
                                'source': a.data.SOURCE_TYPE,
                                'source_name': a.data.SOURCE_NAME,
                                'credit_amount': ($scope.totalS1.credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1.credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : 0,
                                'debit_amount': ($scope.totalS1.debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1.debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : 0,
                                'debit_count': ($scope.totalS1.debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1.debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : 0,
                                'credit_count': ($scope.totalS1.credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1.credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : 0,
                                'totalTransactions': $scope.total_trans1,
                                'outstanding_amount': a.data.AMOUNT
                            })
                        }
                        else {
                            if (angular.isDefined(a.data.DEBIT_CREDIT_INDICATOR) && a.data.DEBIT_CREDIT_INDICATOR != null) {
                                $scope.totalS1 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS1[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS1[0].credit_amount,
                                    'debit_amount': ($scope.totalS1[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS1[0].debit_amount,
                                    'debit_count': ($scope.totalS1[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS1[0].debit_count,
                                    'credit_count': ($scope.totalS1[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS1[0].credit_count,
                                    'totalTransactions': $scope.total_trans1,
                                    'outstanding_amount': $scope.totalS1[0].debit_amount - $scope.totalS1[0].credit_amount
                                }]
                            }
                            else {
                                $scope.totalS1 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS1[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS1[0].credit_amount,
                                    'debit_amount': ($scope.totalS1[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS1[0].debit_amount,
                                    'debit_count': ($scope.totalS1[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS1[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS1[0].debit_count,
                                    'credit_count': ($scope.totalS1[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS1[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS1[0].credit_count,
                                    'totalTransactions': $scope.total_trans1,
                                    'outstanding_amount': a.data.AMOUNT + $scope.totalS1[0]['outstanding_amount']
                                }]
                            }
                        }
                    }
                    if (a.data.SOURCE_TYPE == "S2") {
                        $scope.total_trans2 += 1
                        if ($scope.totalS2.length == 0) {
                            $scope.totalS2.push({
                                'source': a.data.SOURCE_TYPE,
                                'source_name': a.data.SOURCE_NAME,
                                'credit_amount': ($scope.totalS2.credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2.credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : 0,
                                'debit_amount': ($scope.totalS2.debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2.debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : 0,
                                'debit_count': ($scope.totalS2.debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2.debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : 0,
                                'credit_count': ($scope.totalS2.credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2.credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : 0,
                                'totalTransactions': $scope.total_trans2,
                                'outstanding_amount': a.data.AMOUNT
                            })
                        }
                        else {
                            if (angular.isDefined(a.data.DEBIT_CREDIT_INDICATOR) && a.data.DEBIT_CREDIT_INDICATOR != null) {
                                $scope.totalS2 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS2[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS2[0].credit_amount,
                                    'debit_amount': ($scope.totalS2[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS2[0].debit_amount,
                                    'debit_count': ($scope.totalS2[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS2[0].debit_count,
                                    'credit_count': ($scope.totalS2[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS2[0].credit_count,
                                    'totalTransactions': $scope.total_trans2,
                                    'outstanding_amount': $scope.totalS2[0].debit_amount - $scope.totalS2[0].credit_amount
                                }]
                            }
                            else {
                                $scope.totalS2 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS2[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS2[0].credit_amount,
                                    'debit_amount': ($scope.totalS2[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS2[0].debit_amount,
                                    'debit_count': ($scope.totalS2[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS2[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS2[0].debit_count,
                                    'credit_count': ($scope.totalS2[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS2[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS2[0].credit_count,
                                    'totalTransactions': $scope.total_trans2,
                                    'outstanding_amount': a.data.AMOUNT + $scope.totalS2[0]['outstanding_amount']
                                }]
                            }
                        }
                    }
                    if (a.data.SOURCE_TYPE == "S3") {
                        $scope.total_trans3 += 1
                        if ($scope.totalS3.length == 0) {
                            $scope.totalS3.push({
                                'source': a.data.SOURCE_TYPE,
                                'source_name': a.data.SOURCE_NAME,
                                'credit_amount': ($scope.totalS3.credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3.credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : 0,
                                'debit_amount': ($scope.totalS3.debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3.debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : 0,
                                'debit_count': ($scope.totalS3.debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3.debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : 0,
                                'credit_count': ($scope.totalS3.credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3.credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : 0,
                                'totalTransactions': $scope.total_trans3,
                                'outstanding_amount': a.data.AMOUNT
                            })
                        }
                        else {
                            if (angular.isDefined(a.data.DEBIT_CREDIT_INDICATOR) && a.data.DEBIT_CREDIT_INDICATOR != null) {
                                $scope.totalS3 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS3[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS3[0].credit_amount,
                                    'debit_amount': ($scope.totalS3[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS3[0].debit_amount,
                                    'debit_count': ($scope.totalS3[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS3[0].debit_count,
                                    'credit_count': ($scope.totalS3[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS3[0].credit_count,
                                    'totalTransactions': $scope.total_trans3,
                                    'outstanding_amount': $scope.totalS3[0].debit_amount - $scope.totalS3[0].credit_amount
                                }]
                            }
                            else {
                                $scope.totalS3 = [{
                                    'source': a.data.SOURCE_TYPE,
                                    'source_name': a.data.SOURCE_NAME,
                                    'credit_amount': ($scope.totalS3[0].credit_amount && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3[0].credit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? a.data.AMOUNT : $scope.totalS3[0].credit_amount,
                                    'debit_amount': ($scope.totalS3[0].debit_amount && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3[0].debit_amount += a.data.AMOUNT : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? a.data.AMOUNT : $scope.totalS3[0].debit_amount,
                                    'debit_count': ($scope.totalS3[0].debit_count && a.data.DEBIT_CREDIT_INDICATOR == "D") ? $scope.totalS3[0].debit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "D") ? 1 : $scope.totalS3[0].debit_count,
                                    'credit_count': ($scope.totalS3[0].credit_count && a.data.DEBIT_CREDIT_INDICATOR == "C") ? $scope.totalS3[0].credit_count += 1 : (a.data.DEBIT_CREDIT_INDICATOR == "C") ? 1 : $scope.totalS3[0].credit_count,
                                    'totalTransactions': $scope.total_trans3,
                                    'outstanding_amount': a.data.AMOUNT + $scope.totalS3[0]['outstanding_amount']
                                }]
                            }
                        }
                    }
                }
            })

            $scope.total = $scope.totalS1.concat($scope.totalS2.concat($scope.totalS3))

            if ($scope.linkList.length == 1) {
                $scope.auditGridOptionsFlag = true
            }
        }

    }

    $scope.showHistory = function () {
        $scope.linkList = []
        angular.forEach($scope.gridOptions.api.getSelectedNodes(), function (a, b) {
            //console.log(a)
            if (angular.isDefined(a['data']) && a['data'] != null) {
                if ($scope.linkList.indexOf(a['data']['LINK_ID']) == -1) {
                    $scope.linkList.push(a['data']['LINK_ID'])
                }
            }
        })
        if ($scope.linkList.length == 1) {
            $scope.auditGridOptionsFlag = true
            $scope.auditGridOptions.api.setColumnDefs([])
            $scope.auditGridOptions.api.setRowData([])
            var dataR = {}
            dataR['recon_id'] = $scope.reconId
            dataR['link_id'] = $scope.gridOptions.api.getSelectedNodes()[0]['data']['LINK_ID']
            dataR['raw_link'] = $scope.gridOptions.api.getSelectedNodes()[0]['data']['RAW_LINK']
            console.log(dataR)
            console.log($scope.gridOptions.api.getSelectedNodes())
            //$rootScope.openModalPopupOpen()
            ReconAssignmentService.getAuditTrail('dummy', dataR, function (data) {
                //$rootScope.openModalPopupClose()
                console.log(data)
                if (JSON.parse(data['data']).length > 0) {
                    var dataRec = {}
                    var uniqueUICols = []
                    dataRec['recon_id'] = $scope.reconId
                    dataRec['record_status'] = "ACTIVE"
                    ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                        $scope.headers = []
                        $scope.columnDefs = []
                        angular.forEach(JSON.parse(datarec), function (val, key) {
                            //console.log(val)
                            $scope.headers.push({
                                'field': val.UI_DISPLAY_NAME,
                                "name": val.MDL_FIELD_ID,
                                "mandatory": val.MANDATORY,
                                "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                            })
                            uniqueUICols.push(val.MDL_FIELD_ID)
                        })
                        angular.forEach($scope.staticCols, function (v, k) {
                            if (uniqueUICols.indexOf(v) == -1) {
                                $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                            }
                        })
                        //$scope.headers.sort()
                        angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                            if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                $scope.columnDefs.push({
                                    'headerName': val.field/*,'aggFunc': $scope.sumFunction()*/,
                                    'suppressAggregation': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    'cellStyle': {'text-align': 'right'},
                                    'cellRenderer': function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            return params.value.toLocaleString()
                                        } else {
                                            return ''
                                        }
                                    }
                                    //'filter': 'number'
                                })
                            }
                            else if (angular.isDefined(val.mdl_field_data_type)) {
                                if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'suppressMenu': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name, 'width': 302,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(val.mdl_field_data_type)) {
                                                if (val.mdl_field_data_type == "DATE") {
                                                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                        if (params.value.toString().length == 13) {
                                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                        } else {
                                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                        }
                                                        //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                        return date
                                                    } else {
                                                        return "";
                                                    }
                                                } else {
                                                    return params.value;
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name, 'width': 302,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }
                            }
                            else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                })
                            }

                        })
                        //console.log($scope.columnDefs)
                        $scope.reconsData = JSON.parse(data['data'])
                        $scope.auditGridOptions.columnDefs = $scope.columnDefs;
                        $scope.auditGridOptions.rowData = $scope.reconsData;
                        $scope.auditGridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.auditGridOptions.api.setRowData($scope.reconsData)
                    })
                }

            })
            var dataB = {}
            dataB['link_id'] = $scope.gridOptions.api.getSelectedNodes()[0]['data']['LINK_ID']
            /*ReconAssignmentService.getAllocationDetails('dummy', dataB, function (data) {
             console.log(JSON.parse(data))
             $scope.staticColsAllocation = ["ALLOCATED_TO", "ALLOCATED_BY", "ALLOCATION_TIME", "EXCEPTION_ID", "EXCEPTION_RESOLUTION_STATUS"]
             if (JSON.parse(data).length > 0) {
             console.log(JSON.parse(data))
             $scope.headers = []
             $scope.columnDefs = []
             angular.forEach($scope.staticColsAllocation, function (v, k) {
             $scope.headers.push({'field': v, "name": v})
             })
             //$scope.headers.sort()
             angular.forEach($scope.headers, function (val, key) {

             if (val.field == "ALLOCATION_TIME") {
             $scope.columnDefs.push({
             'headerName': val.field,
             'suppressAggregation': true,
             'suppressMenu': true,
             'field': val.name, 'width': 302,
             cellRenderer: function (params) {
             if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
             if (params.value.toString().length == 13) {
             if (val.field == "ALLOCATION_TIME") {
             var date = $filter('dateTimeFormat')(params.value);
             }
             else {
             var date = $filter('dateTimeFormat')(params.value);
             }
             } else {
             if (val.field == "ALLOCATION_TIME") {
             var date = $filter('dateTimeFormat')(params.value);
             }
             else {
             var date = $filter('dateTimeFormat')(params.value);
             }
             }
             //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
             return date
             } else {
             return "";
             }
             }
             })

             }
             else {
             $scope.columnDefs.push({
             'headerName': val.field,
             'suppressAggregation': true,
             'field': val.name, 'width': 302
             })
             }
             })
             //console.log($scope.columnDefs)
             $scope.reconsData = JSON.parse(data)
             $scope.allocationGridOptions.columnDefs = $scope.columnDefs;
             $scope.allocationGridOptions.rowData = $scope.reconsData;
             $scope.allocationGridOptions.api.setColumnDefs($scope.columnDefs)
             $scope.allocationGridOptions.api.setRowData($scope.reconsData)
             }
             })*/
        }
    }

    $scope.$watch('total', function (newvalue, oldvalue) {
        return $scope.total = newvalue
    })
    $scope.getTotal = function (data) {
        if (data == 'debit') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                total += product.debit_amount;
            }
            return total;
        }
        if (data == 'credit') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                total += product.credit_amount;
            }
            return total;
        }
        if (data == 'creditcount') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                total += product.credit_count;
            }
            return total;
        }
        if (data == 'debitcount') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                total += product.debit_count;
            }
            return total;
        }
        if (data == 'outstanding') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                if (product['debit_amount'] == 0 && product['credit_amount'] == 0 && product['outstanding_amount'] != 0) {
                    total += (product.outstanding_amount);
                }
                else {
                    total += (product.debit_amount - product.credit_amount);
                }
            }
            return total;
        }
        if (data == 'total') {
            var total = 0;
            for (var i = 0; i < $scope.total.length; i++) {
                var product = $scope.total[i];
                total += product.totalTransactions;
            }
            return total;
        }

    }
    var filterCount = 0;
    $scope.onFilterChanged = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.gridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }


    $scope.filterDoubleClicked = function (event) {
        setInterval(function () {
            $scope.gridOptions.api.ensureIndexVisible(Math.floor(Math.random() * 100000));
        }, 1000);
    }
    $scope.showBusTable = false
    $scope.process = undefined;
    $scope.reconId = undefined;
    $scope.getProcess = function (business) {
        $scope.busProcess = [];
        var perColConditions = {};
        perColConditions["BUSINESS_CONTEXT_ID"] = business
        BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            if (data.data.length > 0) {
                $scope.busProcess.push(data.data[0]['BUSINESS_PROCESS_NAME'])
                //angular.forEach(businessProcess, function (val, key) {
                //    if (data.data[0]['BUSINESS_PROCESS_ID'] == val['BUSINESS_PROCESS_ID']) {
                //        $scope.busProcess.push(val.BUSINESS_PROCESS_NAME)
                //    }
                //})
            }

        })
    }
    $scope.getBusiness = function (process) {
        $scope.getBusinessContext = []
        if (process != null || process != undefined || process != '') {
            //console.log(business)
            angular.forEach($scope.businessContexts, function (val, key) {
                if (process == val.process) {
                    $scope.getBusinessContext.push({'id': val.id, 'name': val.name})
                }
            })
        }
    }
    $scope.getRecons = function (business) {
        $scope.recon_ids = [];
        $scope.arraycheck = []
        $scope.getBusiness($scope.process)
        //$scope.getProcess(business)
//            //console.log($scope.reconData.businessContext)
        if (business != null || business != undefined || business != '') {
            //console.log(business)
            angular.forEach($scope.reconBusinessData, function (v, k) {
                if (business == v.business) {
                    if ($scope.arraycheck.indexOf(v.name) == -1) {
                        $scope.arraycheck.push(v.name)
                        $scope.recon_ids.push({'id': v.id, 'name': v.name, 'userpool': v.userpool})
                    }
                }
            })
            if ($scope.recon_ids.length > 0) {
                if (angular.isDefined($scope.reconId)) {
                    angular.forEach($scope.recon_ids, function (val, key) {
                        if (val.id == $scope.reconId) {
                            console.log(val.userpool)
                            var perColConditions = {};
                            perColConditions["name"] = val.userpool
                            UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                console.log(data)
                                if (data.data.length > 0) {
                                    $rootScope.userpoolRole = data.data[0]['role']
                                    console.log($rootScope.userpoolRole)
                                }
                            })
                        }
                    })
                }
            }
            //console.log($scope.reconBusinessData)
        }
    }
    function headerCellRendererFunc(params) {

        var cb = document.createElement('input');

        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');

        var eTitle = document.createTextNode(params.colDef.name);

        eHeader.appendChild(cb);

        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {

            if ($(this)[0].checked) {

                $scope.gridOptions.api.selectAll();

            } else {

                $scope.gridOptions.api.deselectAll();

            }

        });

        return eHeader;

    }

    $scope.allUserpools = function () {
        UserPoolService.get(undefined, undefined, function (data) {
            console.log(data)
            $rootScope.allUserPoolData = data.data
        })
    }
    $scope.allUserpools()
    $scope.reconFlag = false
    $scope.authorizeFlag = false
    $scope.matchedFlag = false
    $scope.reconDataFlag = false
//         $scope.columnDefs = [
//        ];
    $scope.reconsData = []
    $scope.selectedFilter = "UnMatched"
//         $scope.gridOptions = {};
    $scope.staticCols = ['SOURCE_TYPE', 'SOURCE_NAME', 'ACCOUNT_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME', 'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE', 'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS', 'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'INVESTIGATED_BY', 'RESOLUTION_COMMENTS', 'INVESTIGATOR_COMMENTS', 'UPDATED_BY']

    $scope.accountNumber = ''
    $scope.accountPoolName = ''
    $scope.exportAllReport = ''
    //unmatched data from below function
    $scope.getSelectedRecon = function (recon) {
        $scope.clearDates()
        console.log($scope.pastGrid)
        console.log($scope.colsOrder)
        $scope.showGrid = false
        $scope.auditGridOptionsFlag = false
        $scope.data = {}
        $scope.exportAllReport = ''

        angular.forEach($scope.recon_ids, function (v, k) {
            if (v.id == recon) {
                $scope.reconName = v.name
            }
        })
        angular.forEach($scope.recon_ids, function (val, key) {
            if (val.id == recon) {
                console.log(val.userpool)
                angular.forEach($rootScope.allUserPoolData, function (v, k) {
                    if (val.userpool == v.name) {
                        $rootScope.userpoolRole = v['role']
                        $rootScope.userpoolName = v['name']
                    }
                })
            }
        })
        if ($scope.selectedFilter == "UnMatched" && recon != null && recon != undefined && recon != '') {
            $scope.gridOptions.columnDefs = [];
            $scope.gridOptions.rowData = [];
            $scope.gridOptionsAll.columnDefs = [];
            $scope.gridOptionsAll.rowData = [];
            if (angular.isDefined($scope.gridOptions.api)) {
                $scope.gridOptions.api.setColumnDefs([])
                $scope.gridOptions.api.setRowData([])
                $scope.gridOptionsAll.api.setColumnDefs([])
                $scope.gridOptionsAll.api.setRowData([])
            }
            $scope.reconId = recon
            $rootScope.reconID = recon
            $rootScope.businessContextId = $scope.businessContext
            if (recon != null || recon != undefined || recon != '') {
//                 //console.log(business)
                var perColConditions = {};
                perColConditions["RECON_ID"] = recon
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    //console.log(data)
                    if (data.data.length > 0) {
                        $scope.showRecTable = true
                        $scope.selectedReconData = data.data[0]
                    }
                })
            }


            var dataR = {}
            dataR['business_context_id'] = $scope.businessContext
            dataR['recon_id'] = recon
            dataR['record_status'] = 'ACTIVE'
            dataR['suspenseFlag'] = $scope.suspenseFlag
            dataR['accountNumber'] = $scope.accountNumber
            dataR['userPools'] = $rootScope.userpoolName
            dataR['userRole'] = $rootScope.userpoolRole

            //console.log(dataR)
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.getReconUnmatched('dummy', dataR, function (data) {

                var uniqueUICols = []
                $scope.getUserpools()
                $scope.getDeptUserpools()
                var dataRec = {}
                dataRec['recon_id'] = recon
                dataRec['record_status'] = "ACTIVE"
                $rootScope.openModalPopupClose()
                $scope.txnCount = data['txn_count']
                if (data['txn_count'] == 0) {
                    if (!$scope.successFlag) {
                        $rootScope.showAlertMsg("No Data Found.")
                    }
                    $scope.successFlag = false
                }
                else {
                    $scope.successFlag = false
                    ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                        //console.log(datarec)

                        $scope.headers = []
                        $scope.columnDefs = [
                            {
                                headerName: '',
                                'width': 30,
                                checkboxSelection: true,
                                'suppressAggregation': true,
                                suppressSorting: true,
                                suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                pinned: true
                            }
                        ]
                        angular.forEach(JSON.parse(datarec), function (val, key) {
                            console.log(val)
                            $scope.headers.push({
                                'field': val.UI_DISPLAY_NAME,
                                "name": val.MDL_FIELD_ID,
                                "mandatory": val.MANDATORY,
                                "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                            })
                            uniqueUICols.push(val.MDL_FIELD_ID)
                        })
                        angular.forEach($scope.staticCols, function (v, k) {
                            if (uniqueUICols.indexOf(v) == -1) {
                                $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                            }
                        })
                        // ageing column only for unmatched tab
                        $scope.headers.push({'field': 'AGEING', 'name': 'AGEING', 'mandatory': "M"})
                        //$scope.headers.sort()

                        $scope.reconFlag = true
                        $scope.reconDataFlag = true
                        $scope.sumFunction = function (values) {
                            var result = 0;
                            values.forEach(function (value) {
                                if (typeof value === 'number') {
                                    result += value;
                                }
                            });
                            return result;
                        }
                        angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                            if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                $scope.columnDefs.push({
                                    'headerName': val.field/*,'aggFunc': $scope.sumFunction()*/,
                                    'suppressAggregation': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    'cellStyle': {'text-align': 'right'},
                                    'cellRenderer': function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            return params.value.toLocaleString()
                                        } else {
                                            return ''
                                        }
                                    }
                                    //'filter': 'number'
                                })
                            }
                            else if (angular.isDefined(val.mdl_field_data_type)) {
                                if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'suppressMenu': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name, 'width': 302,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(val.mdl_field_data_type)) {
                                                if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {

                                                        if (params.value.toString().length == 13) {
                                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                        } else {
                                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                        }
                                                        //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                        return date
                                                    } else {
                                                        return "";
                                                    }
                                                } else {
                                                    return params.value;
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name, 'width': 302,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }
                            }
                            else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name, 'width': 302,
                                    'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                })
                            }

                        })
                        //console.log($scope.columnDefs)
                        $scope.reconsData = JSON.parse(data['data'])
                        if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                            angular.forEach($scope.pastGrid, function (val, key) {
                                angular.forEach($scope.columnDefs, function (v, k) {
                                    if (val.colId == v.field) {
                                        $scope.columnDefs[k]['rowGroupIndex'] = key
                                    }
                                })
                            })
                        }

                        console.log($scope.gridOptions)
                        if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                            $scope.columnDefs = $scope.colsOrder
                        }
                        $scope.gridOptions.columnDefs = $scope.columnDefs;
                        $scope.gridOptions.rowData = $scope.reconsData;
                        $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.gridOptions.api.setRowData($scope.reconsData)
                        if (angular.isDefined($scope.savedSort)) {
                            $scope.gridOptions.api.setSortModel($scope.savedSort)
                        }
                        if (angular.isDefined($scope.savedFilter)) {
                            $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                            $scope.gridOptions.api.onFilterChanged();
                        }
                        $rootScope.openModalPopupClose()
                        //console.log($scope.gridOptions)
                    })
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
        else {
            $scope.reconId = recon
            $rootScope.reconID = recon
            $scope.changeGrid($scope.selectedFilter)
        }

    }

    //export whole data for selected recon
    $scope.getAll = function (selectedFilter) {

        var dataR = {}
        dataR['business_context_id'] = $scope.businessContext
        dataR['recon_id'] = $scope.reconId
        if (angular.isDefined($rootScope.reconID)) {
            dataR['recon_id'] = $rootScope.reconID
        }

        var filePath = window.location.origin + '/app/files/recon_reports/'

        if (!angular.isDefined(selectedFilter) && $scope.exportAllReport != '') {
            $window.open(filePath + $scope.exportAllReport)
        } else {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.getData('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                data = JSON.parse(data)
                $window.open(filePath + data['fileName'])
                /*if (data['txn_count'] > 0) {
                 $window.open(filePath + data['fileName'])
                 } else {
                 $rootScope.showAlertMsg('No data found.')
                 }*/
            }, function (err) {
                $rootScope.openModalPopupClose()
                $rootScope.showErrormsg(err.msg)
            })
        }

    }

    //matched and other tabs data
    $scope.changeGrid = function (selectedFilter) {

        $scope.data = {}
        $scope.txnCount = 0

        if (angular.isDefined($scope.reconId)) {
            $scope.matchedFlag = false
            if (angular.isDefined(selectedFilter)) {
                $scope.selectedFilter = selectedFilter
            }

            var dataR = {}
            dataR['business_context_id'] = $scope.businessContext
            dataR['recon_id'] = $scope.reconId
            dataR['record_status'] = 'ACTIVE'
            dataR['suspenseFlag'] = $scope.suspenseFlag
            dataR['accountNumber'] = $scope.accountNumber
            dataR['userPools'] = $rootScope.userpoolName
            dataR['userRole'] = $rootScope.userpoolRole
            dataR['recon_name'] = $scope.reconName
            ////console.log(dataR)
            if (angular.isDefined($rootScope.reconID)) {
                dataR['recon_id'] = $rootScope.reconID
            }

            if ($scope.selectedFilter == "UnMatched") {
                $scope.reconFlag = true
                $scope.reconDataFlag = true
                if (angular.isDefined($scope.gridOptions.api)) {
                    $scope.gridOptions.columnDefs = [];
                    $scope.gridOptions.rowData = [];
                    $scope.gridOptions.api.setColumnDefs([])
                    $scope.gridOptions.api.setRowData([])
                }
                $scope.getSelectedRecon($scope.reconId)

            }
            $rootScope.openModalPopupOpen()
            if ($scope.selectedFilter == "Matched") {
                $scope.gridOptions.columnDefs = [];
                $scope.gridOptions.rowData = [];
                $scope.gridOptions.api.setColumnDefs([])
                $scope.gridOptions.api.setRowData([])
                $scope.reconFlag = false
                $scope.reconDataFlag = true
                $scope.matchedFlag = true
                $scope.authorizeFlag = false
                $scope.showDateFilter = true

                $scope.gridOptions['rowModelType'] = 'pagination'


                ReconAssignmentService.getReconMatched('dummy', dataR, function (data) {
                    $rootScope.openModalPopupClose()
                    $scope.getUserpools()
                    dataRec = {}
                    var uniqueUICols = []
                    dataRec['recon_id'] = $scope.reconId
                    if (angular.isDefined($rootScope.reconID)) {
                        dataRec['recon_id'] = $rootScope.reconID
                    }

                    if (angular.isDefined(data['background_process']) & data['background_process']) {
                        $rootScope.showAlertMsg("Background Job is in progess,please wait")
                        return
                    }
                    dataRec['record_status'] = "ACTIVE"
                    $scope.txnCount = data['totalMatchedCount']
                    $scope.totalItems = data['totalMatchedCount']
                    $scope.maxSize = parseInt($scope.totalItems / $scope.viewby)
                    if ($scope.txnCount == 0) {
                        $rootScope.showAlertMsg("No Data Found.")
                    } else if (data['ignore_load']) {
                        $rootScope.showAlertMsg("Please use export all to retrieve the records")
                    }
                    else {
                        $scope.successFlag = false
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)

                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                                uniqueUICols.push(val.MDL_FIELD_ID)
                            })
                            angular.forEach($scope.staticCols, function (v, k) {
                                if (uniqueUICols.indexOf(v) == -1) {
                                    $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                                }
                            })
                            //$scope.headers.sort()


//                        $scope.reconFlag = true

                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'width': 302,
                                        'cellStyle': {'text-align': 'right'},
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'suppressMenu': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return "";
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'suppressMenu': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })

                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }

                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = JSON.parse(data['data'])
                            if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                                angular.forEach($scope.pastGrid, function (val, key) {
                                    angular.forEach($scope.columnDefs, function (v, k) {
                                        if (val.colId == v.field) {
                                            $scope.columnDefs[k]['rowGroupIndex'] = key
                                        }
                                    })
                                })
                            }
                            $scope.gridOptions.groupSelectsChildren = false;
                            if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                                $scope.columnDefs = $scope.colsOrder
                            }
                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            if (angular.isDefined($scope.savedSort)) {
                                $scope.gridOptions.api.setSortModel($scope.savedSort)
                            }
                            if (angular.isDefined($scope.savedFilter)) {
                                $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                                $scope.gridOptions.api.onFilterChanged();
                            }
                            //console.log($scope.gridOptions)
                        })
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            }

            if ($scope.selectedFilter == "rollBack") {
                $scope.reconFlag = false
                $scope.gridOptions.columnDefs = [];
                $scope.gridOptions.rowData = [];
                $scope.gridOptions.api.setColumnDefs([])
                $scope.gridOptions.api.setRowData([])
                ReconAssignmentService.getReconRollBack('dummy', dataR, function (data) {
                    $rootScope.openModalPopupClose()
                    $scope.getUserpools()
                    dataRec = {}
                    dataRec['recon_id'] = $scope.reconId
                    if (angular.isDefined($rootScope.reconID)) {
                        dataRec['recon_id'] = $rootScope.reconID
                    }
                    dataRec['record_status'] = "ACTIVE"

                    $scope.txnCount = data['txn_count']
                    if (data['txn_count'] == 0) {
                        $rootScope.showAlertMsg("No Data Found.")
                    }
                    else {
                        $scope.successFlag = true
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)
                            uniqueUICols = []
                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                                uniqueUICols.push(val.MDL_FIELD_ID)
                            })
                            angular.forEach($scope.staticCols, function (v, k) {
                                if (uniqueUICols.indexOf(v) == -1) {
                                    $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                                }
                            })
                            //$scope.headers.sort()


//                        $scope.reconFlag = true
                            $scope.reconDataFlag = true
                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'cellStyle': {'text-align': 'right'},
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'suppressMenu': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return ""
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'suppressMenu': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })

                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }
                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = JSON.parse(data['data'])
                            if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                                angular.forEach($scope.pastGrid, function (val, key) {
                                    angular.forEach($scope.columnDefs, function (v, k) {
                                        if (val.colId == v.field) {
                                            $scope.columnDefs[k]['rowGroupIndex'] = key
                                        }
                                    })
                                })
                            }
                            $scope.gridOptions.groupSelectsChildren = true
                            if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                                $scope.columnDefs = $scope.colsOrder
                            }
                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            if (angular.isDefined($scope.savedSort)) {
                                $scope.gridOptions.api.setSortModel($scope.savedSort)
                            }
                            if (angular.isDefined($scope.savedFilter)) {
                                $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                                $scope.gridOptions.api.onFilterChanged();
                            }
                            //console.log($scope.gridOptions)
                        })
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            }
            if ($scope.selectedFilter == "pendingForSubmission") {
                $scope.reconFlag = true
                $scope.gridOptions.columnDefs = [];
                $scope.gridOptions.rowData = [];
                $scope.gridOptions.api.setColumnDefs([])
                $scope.gridOptions.api.setRowData([])
                ReconAssignmentService.getReconPendingForSubmission('dummy', dataR, function (data) {
                    $scope.getUserpools()
                    dataRec = {}
                    var uniqueUICols = []
                    dataRec['recon_id'] = $scope.reconId
                    if (angular.isDefined($rootScope.reconID)) {
                        dataRec['recon_id'] = $rootScope.reconID
                    }
                    dataRec['record_status'] = "ACTIVE"
                    $rootScope.openModalPopupClose()
                    $scope.txnCount = data['txn_count']
                    if (data['txn_count'] == 0) {
                        if (!$scope.successFlag) {
                            $rootScope.showAlertMsg("No Data Found.")
                        }
                        $scope.successFlag = false
                    }
                    else {
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)

                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                                uniqueUICols.push(val.MDL_FIELD_ID)
                            })

                            angular.forEach($scope.staticCols, function (v, k) {
                                if (uniqueUICols.indexOf(v) == -1) {
                                    $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                                }
                            })
                            //$scope.headers.sort()

//                        $scope.reconFlag = true
                            $scope.reconDataFlag = true
                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'cellStyle': {'text-align': 'right'},
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'suppressMenu': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return "";
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'suppressMenu': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })

                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }

                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = JSON.parse(data['data'])
                            if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                                angular.forEach($scope.pastGrid, function (val, key) {
                                    angular.forEach($scope.columnDefs, function (v, k) {
                                        if (val.colId == v.field) {
                                            $scope.columnDefs[k]['rowGroupIndex'] = key
                                        }
                                    })
                                })
                            }
                            $scope.gridOptions.groupSelectsChildren = true
                            if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                                $scope.columnDefs = $scope.colsOrder
                            }
                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            if (angular.isDefined($scope.savedSort)) {
                                $scope.gridOptions.api.setSortModel($scope.savedSort)
                            }
                            if (angular.isDefined($scope.savedFilter)) {
                                $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                                $scope.gridOptions.api.onFilterChanged();
                            }
                            //console.log($scope.gridOptions)
                        })
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            }
            if ($scope.selectedFilter == "waitingForAuthorization") {
                $scope.reconFlag = false
                $scope.gridOptions.columnDefs = [];
                $scope.gridOptions.rowData = [];
                $scope.gridOptions.api.setColumnDefs([])
                $scope.gridOptions.api.setRowData([])
                dataR['authorizer'] = $rootScope.credentials.userName
                dataR['recon_id'] = $scope.reconId
                var uniqueUICols = []
                ReconAssignmentService.getReconWaitingForAuthorization('dummy', dataR, function (data) {

                    $scope.getUserpools()
                    dataRec = {}
                    dataRec['recon_id'] = $scope.reconId
                    if (angular.isDefined($rootScope.reconID)) {
                        dataRec['recon_id'] = $rootScope.reconID
                    }
                    dataRec['record_status'] = "ACTIVE"
                    $rootScope.openModalPopupClose()

                    $scope.txnCount = data['txn_count']
                    if (data['txn_count'] == 0) {
                        if (!$scope.successFlag) {
                            $scope.successFlag = false
                            $rootScope.showAlertMsg("No Data Found.")
                        }
                    }
                    else {
                        $scope.successFlag = false
                        $scope.authorizeFlag = true
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)

                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                                uniqueUICols.push(val.MDL_FIELD_ID)
                            })
                            angular.forEach($scope.staticCols, function (v, k) {
                                if (uniqueUICols.indexOf(v) == -1) {
                                    $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                                }
                            })
                            //$scope.headers.sort()


//                        $scope.reconFlag = true
                            $scope.reconDataFlag = true
                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'cellStyle': {'text-align': 'right'},
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'suppressMenu': true,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'rowGroupIndex': (val.field == 'LINK_ID') ? 0 : null,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return "";
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'rowGroupIndex': (val.field == 'LINK_ID') ? 0 : null,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'suppressMenu': true,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'rowGroupIndex': (val.field == 'LINK_ID') ? 0 : null,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                // date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })

                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'rowGroupIndex': (val.field == 'LINK_ID') ? 0 : null,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }

                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = JSON.parse(data['data'])
                            console.log($scope.gridOptions)
                            if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                                angular.forEach($scope.pastGrid, function (val, key) {
                                    angular.forEach($scope.columnDefs, function (v, k) {
                                        if (val.colId == v.field) {
                                            $scope.columnDefs[k]['rowGroupIndex'] = key
                                        }
                                    })
                                })
                            }
                            $scope.gridOptions.groupSelectsChildren = true
                            if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                                $scope.columnDefs = $scope.colsOrder
                            }
                            angular.forEach($scope.columnDefs, function (val, key) {
                                if (val['field'] == "LINK_ID") {
                                    $scope.columnDefs[key]['rowGroupIndex'] = 0
                                }
                            })

                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            if (angular.isDefined($scope.savedSort)) {
                                $scope.gridOptions.api.setSortModel($scope.savedSort)
                            }
                            if (angular.isDefined($scope.savedFilter)) {
                                $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                                $scope.gridOptions.api.onFilterChanged();
                            }
                            //console.log($scope.gridOptions)
                        })
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            }
        }

    }
    //console.log($rootScope.credentials)
    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };
    $scope.getUsers = function (userpool) {
        //console.log(userpool)
        if (userpool == "" || userpool == null || userpool == undefined) {
            $scope.users = []
        }
        else {
            var perColConditions = {};
            perColConditions["name"] = userpool
            UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                //console.log(data)
                if (data.data.length > 0) {
                    $scope.users = data.data[0].users.split(",")
                }
            })
        }
    }

    //filtering matched data with statement and execution dates
    $scope.getFilteredDataByDates = function (date, skip, limit) {
        //Check date validation
        console.log(skip, limit)

        var currentdate = new Date();
        if (date.statementStartDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid statement start date");
            return false;
        } else if (date.statementEndDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid statement end date");
            return false;
        } else if (date.executionStartDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid execution start date");
            return false;
        } else if (date.executionEndDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid executiion end date");
            return false;
        }
        if ((date.executionStartDate == 0 || date.executionStartDate == null) && (date.executionEndDate == 0 || date.executionEndDate == null) && (date.statementStartDate == 0 || date.statementStartDate == null) && (date.statementEndDate == 0 || date.statementEndDate == null)) {
            $rootScope.showAlertMsg('Please Select Dates to filter')
            return false;
        }


        var dataD = {}
        dataD['recon_id'] = $scope.reconId
        dataD['suspenseFlag'] = $scope.suspenseFlag
        dataD['accountNumber'] = $scope.accountNumber
        dataD['executionStartDate'] = (date.executionStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.executionStartDate) * 1000), 'dd/MM/yy') : 0;
        dataD['executionEndDate'] = (date.executionEndDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.executionEndDate) * 1000), 'dd/MM/yy') : 0;
        dataD['statementStartDate'] = (date.statementStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.statementStartDate) * 1000), 'dd/MM/yy') : 0;
        dataD['statementEndDate'] = (date.statementEndDate != 0 ) ? $filter('date')(new Date($scope.toEpoch($scope.date.statementEndDate) * 1000), 'dd/MM/yy') : 0;
        dataD['recon_name'] = $scope.reconName
        $scope.exportQuery = dataD
        $scope.matchedTabFilter = date
        if (angular.isDefined(skip))
            dataD['skip'] = skip
        if (angular.isDefined(limit))
            dataD['limit'] = limit
        $scope.exportAllReport = ''
        if ((dataD['statementStartDate'] != 0 && dataD['statementEndDate'] != 0) && (dataD['executionStartDate'] == 0 && dataD['executionEndDate'] == 0)) {
            $rootScope.openModalPopupOpen()
            $scope.gridOptions.columnDefs = [];
            $scope.gridOptions.rowData = [];
            $scope.gridOptions.api.setColumnDefs([])
            $scope.gridOptions.api.setRowData([])

            ReconAssignmentService.getReconMatchedStatementDate('dummy', dataD, function (responseData) {

                $rootScope.openModalPopupClose()

                if (angular.isDefined(responseData['background_process']) & responseData['background_process']) {
                    $rootScope.showAlertMsg("Background Job is in progess,please wait")
                    return
                }
                $scope.txnCount = responseData['totalMatchedCount']
                $scope.totalItems = responseData['totalMatchedCount']
                $scope.maxSize = parseInt($scope.totalItems / $scope.viewby)
                $scope.executionIds = responseData['RECON_EXECUTION_ID']
                if (angular.isDefined(responseData['download']) & responseData['download']) {
                    // $rootScope.showAlertMsg("Transactions contains more than one statment date. Please do export and check.")
                    $modal.open({
                        templateUrl: 'exportMultiExecData.html',
                        windowClass: 'modal addUser',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                            $scope.exportMultiExecData = function () {
                                $rootScope.openModalPopupOpen()
                                dataD['responseData'] = responseData
                                ReconAssignmentService.getReconMatchedExportAll('dummy', dataD, function (res) {
                                    $rootScope.openModalPopupClose()

                                    if (angular.isDefined(res['background_process']) & res['background_process']) {
                                        $rootScope.showAlertMsg("Background Job is in progess,please wait")
                                    }
                                    else {
                                        var filePath = window.location.origin + '/app/files/recon_reports/'
                                        $window.open(filePath + res['fileName'])
                                        $scope.cancel()
                                    }
                                })
                            }
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            }
                        }
                    })
                }
                if (responseData['total_count'] == 0) {
                    $rootScope.showAlertMsg("No Data Found.")
                } else if (responseData['ignore_load']) {
                    $rootScope.showAlertMsg("Please use export all to retrieve the records")
                }
                else {
                    ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                        //console.log(datarec)

                        $scope.headers = []
                        $scope.columnDefs = [
                            {
                                headerName: '',
                                'width': 30,
                                checkboxSelection: true,
                                'suppressAggregation': true,
                                suppressSorting: true,
                                suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                pinned: true
                            }
                        ]
                        angular.forEach(JSON.parse(datarec), function (val, key) {
                            //console.log(val)
                            $scope.headers.push({
                                'field': val.UI_DISPLAY_NAME,
                                "name": val.MDL_FIELD_ID,
                                "mandatory": val.MANDATORY,
                                "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                            })
                        })

                        angular.forEach($scope.staticCols, function (v, k) {
                            $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                        })
                        //$scope.headers.sort()

//                        $scope.reconFlag = true

                        angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                            if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'width': 302,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    'cellStyle': {'text-align': 'right'},
                                    'cellRenderer': function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            return params.value.toLocaleString()
                                        } else {
                                            return ''
                                        }
                                    }
                                    //'filter': 'number'
                                })
                            }
                            else if (angular.isDefined(val.mdl_field_data_type)) {
                                if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(val.mdl_field_data_type)) {
                                                if (val.mdl_field_data_type == "DATE") {
                                                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                        if (params.value.toString().length == 13) {
                                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                        } else {
                                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                        }
                                                        //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                        return date
                                                    } else {
                                                        return "";
                                                    }
                                                } else {
                                                    return params.value;
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }
                            }
                            else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'width': 302,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })
                            }
                            else {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'width': 302,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                })
                            }

                        })
                        //console.log($scope.columnDefs)
                        $scope.reconsData = JSON.parse(responseData['data'])
                        $scope.gridOptions.columnDefs = $scope.columnDefs;
                        $scope.gridOptions.rowData = $scope.reconsData;
                        $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.gridOptions.api.setRowData($scope.reconsData)
                        //console.log($scope.gridOptions)
                    })
                }

            }, function (err) {
                $rootScope.showErrormsg(err.msg)
                console.log(err.msg)
            })
        }
        else if ((dataD['statementStartDate'] == 0 && dataD['statementEndDate'] == 0) && (dataD['executionStartDate'] != 0 && dataD['executionEndDate'] != 0)) {
            $rootScope.openModalPopupOpen()
            $scope.gridOptions.columnDefs = [];
            $scope.gridOptions.rowData = [];
            $scope.gridOptions.api.setColumnDefs([])
            $scope.gridOptions.api.setRowData([])

            ReconAssignmentService.getReconMatchedExecutionDate('dummy', dataD, function (responseData) {
                //console.log(data)
                $rootScope.openModalPopupClose()
                $scope.txnCount = responseData['totalMatchedCount']
                $scope.totalItems = responseData['totalMatchedCount']
                $scope.maxSize = parseInt($scope.totalItems / $scope.viewby)

                if (angular.isDefined(responseData['background_process']) & responseData['background_process']) {
                    $rootScope.showAlertMsg("Background Job is in progess,please wait")
                    return
                }
                if (angular.isDefined(responseData['download']) & responseData['download']) {
                    $modal.open({
                        templateUrl: 'exportMultiExecData.html',
                        windowClass: 'modal addUser',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                            $scope.exportMultiExecData = function () {
                                $rootScope.openModalPopupOpen()
                                dataD['responseData'] = responseData
                                ReconAssignmentService.getReconMatchedExportAll('dummy', dataD, function (res) {
                                    $rootScope.openModalPopupClose()
                                    if (angular.isDefined(res['background_process']) & res['background_process']) {
                                        $rootScope.showAlertMsg("Background Job is in progess,please wait")
                                    }
                                    else {
                                        var filePath = window.location.origin + '/app/files/recon_reports/'
                                        $window.open(filePath + res['fileName'])
                                        $scope.cancel()
                                    }
                                })
                            }
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            }
                        }
                    })
                }
                // for recons black listed as ignore transaction,don't display
                // mathched txn instead provide export all
                if ($scope.ignoreTransaction.indexOf($scope.reconId) != -1) {
                    if (responseData['total'] == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    } else {
                        $scope.exportAllReport = responseData['fileName']
                        $rootScope.showAlertMsg("Please use export all to retrieve the records")
                    }
                } else {
                    data = JSON.parse(responseData)
                    if (data.length == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                    if (data.length > 0) {
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)

                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                            })
                            angular.forEach($scope.staticCols, function (v, k) {
                                $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                            })
                            //$scope.headers.sort()


//                        $scope.reconFlag = true

                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return "";
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })

                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }

                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = data
                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            //console.log($scope.gridOptions)
                        })
                    }
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
        else if ((dataD['statementStartDate'] != 0 && dataD['statementEndDate'] != 0) && (dataD['executionStartDate'] != 0 && dataD['executionEndDate'] != 0)) {
            $rootScope.openModalPopupOpen()
            $scope.gridOptions.columnDefs = [];
            $scope.gridOptions.rowData = [];
            $scope.gridOptions.api.setColumnDefs([])
            $scope.gridOptions.api.setRowData([])

            ReconAssignmentService.getReconMatched_St_Ex_Date('dummy', dataD, function (responseData) {
                //console.log(data)
                $rootScope.openModalPopupClose()

                $scope.totalItems = responseData['totalMatchedCount']
                $scope.maxSize = parseInt($scope.totalItems / $scope.viewby)
                if (angular.isDefined(responseData['download']) & responseData['download']) {
                    $modal.open({
                        templateUrl: 'exportMultiExecData.html',
                        windowClass: 'modal addUser',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                            $scope.exportMultiExecData = function () {
                                $rootScope.openModalPopupOpen()
                                dataD['responseData'] = responseData
                                ReconAssignmentService.getReconMatchedExportAll('dummy', dataD, function (res) {
                                    $rootScope.openModalPopupClose()
                                    if (angular.isDefined(res['background_process']) & res['background_process']) {
                                        $rootScope.showAlertMsg("Background Job is in progess,please wait")
                                    }
                                    else {
                                        var filePath = window.location.origin + '/app/files/recon_reports/'
                                        $window.open(filePath + res['fileName'])
                                        $scope.cancel()
                                    }

                                    console.log('hee', res)
                                })
                            }
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            }
                        }
                    })
                }

                if (angular.isDefined(responseData['background_process']) & responseData['background_process']) {
                    $rootScope.showAlertMsg("Background Job is in progess,please wait")
                    return
                }
                // for recons black listed as ignore transaction,don't display
                // mathched txn instead provide export all
                if ($scope.ignoreTransaction.indexOf($scope.reconId) != -1) {
                    if (responseData['total'] == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    } else {
                        $scope.exportAllReport = responseData['fileName']
                        $rootScope.showAlertMsg("Please use export all to retrieve the records")
                    }
                } else {
                    data = JSON.parse(responseData)
                    if (data.length == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                    if (data.length > 0) {
                        ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                            //console.log(datarec)

                            $scope.headers = []
                            $scope.columnDefs = [
                                {
                                    headerName: '',
                                    'width': 30,
                                    checkboxSelection: true,
                                    'suppressAggregation': true,
                                    suppressSorting: true,
                                    suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                    pinned: true
                                }
                            ]
                            angular.forEach(JSON.parse(datarec), function (val, key) {
                                //console.log(val)
                                $scope.headers.push({
                                    'field': val.UI_DISPLAY_NAME,
                                    "name": val.MDL_FIELD_ID,
                                    "mandatory": val.MANDATORY,
                                    "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                                })
                            })
                            angular.forEach($scope.staticCols, function (v, k) {
                                $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                            })
                            //$scope.headers.sort()


//                        $scope.reconFlag = true

                            angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                                if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'cellStyle': {'text-align': 'right'},
                                        'cellRenderer': function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                return params.value.toLocaleString()
                                            } else {
                                                return ''
                                            }
                                        }
                                        //'filter': 'number'
                                    })
                                }
                                else if (angular.isDefined(val.mdl_field_data_type)) {
                                    if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            cellRenderer: function (params) {
                                                if (angular.isDefined(val.mdl_field_data_type)) {
                                                    if (val.mdl_field_data_type == "DATE") {
                                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                            if (params.value.toString().length == 13) {
                                                                var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                            } else {
                                                                var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                            }
                                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                            return date
                                                        } else {
                                                            return "";
                                                        }
                                                    } else {
                                                        return params.value;
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        $scope.columnDefs.push({
                                            'headerName': val.field,
                                            'suppressAggregation': true,
                                            'width': 302,
                                            'hide': (val.mandatory == "M") ? false : true,
                                            'field': val.name,
                                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                        })
                                    }
                                }
                                else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                    }
                                                } else {
                                                    if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                        var date = $filter('dateTimeFormat')(params.value);
                                                    }
                                                    else {
                                                        var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                    }
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        }
                                    })
                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }

                            })
                            //console.log($scope.columnDefs)
                            $scope.reconsData = data
                            $scope.gridOptions.columnDefs = $scope.columnDefs;
                            $scope.gridOptions.rowData = $scope.reconsData;
                            $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                            $scope.gridOptions.api.setRowData($scope.reconsData)
                            //console.log($scope.gridOptions)
                        })
                    }
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
        else {
            $rootScope.showAlertMsg("Please select start and end dates properly")
        }

        ////console.log($scope.date.executionStartDate)
    }
    $scope.getBooleanValue = function (cssSelector) {
        //return document.querySelector(cssSelector).checked === true;
    }
    //$scope.getFileteredValue = function (params) {
    //    console.log(params)
    //}

    //export to csv
    $scope.onBtExport = function () {
        $scope.exportFlag = true
        if ($scope.gridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("no data to export")
        }
        if ((angular.isDefined($scope.accountPoolName) && $scope.accountPoolName != '') && (angular.isDefined($scope.accountNumber) && $scope.accountNumber != '')) {
            $scope.customHeader = "Recon_Name :," + $scope.reconName + '\n' + "Recon_Id :," + $scope.reconId + '\n' + "Account name :," + $scope.accountPoolName + '\n' + "Account Number :," + $scope.accountNumber + '\n' + "Date-Time :," + new Date() + '\n' + "User :," + $rootScope.credentials.userName + '\n' + "Status :," + $scope.selectedFilter + '\n' + '\n'
        }
        else {
            $scope.customHeader = "Recon_Name :," + $scope.reconName + '\n' + "Recon_Id :," + $scope.reconId + '\n' + "Date-Time :," + new Date() + '\n' + "User :," + $rootScope.credentials.userName + '\n' + "Status :," + $scope.selectedFilter + '\n' + '\n'
        }
        var params = {
            skipHeader: $scope.getBooleanValue('#skipHeader'),
            skipFooters: $scope.getBooleanValue('#skipFooters'),
            skipGroups: $scope.getBooleanValue('#skipGroups'),
            allColumns: $scope.getBooleanValue('#allColumns'),
            onlySelected: $scope.getBooleanValue('#onlySelected'),
            processCellCallback: function (params) {
                if (params.value != null) {
                    if (typeof params.value == "number") {
                        if (params.value.toString().length == 13 && params['column']['colId'].indexOf("AMOUNT") == -1) {
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else {
                            return params.value
                        }
                    }
                    else {
                        return params.value
                    }
                }
                //console.log(params.column.getColumnDef())
            },
            customHeader: $scope.customHeader,
            fileName: $scope.reconName + '-' + $scope.reconId + '-' + $filter('date')(new Date(Date.now())) + '-' + $scope.selectedFilter + '.csv',
            //columnSeparator: document.querySelector('#columnSeparator').value
        };

        if ($scope.getBooleanValue('#useCellCallback')) {
            params.processCellCallback = function (params) {
                if (params.value && params.value.toUpperCase) {
                    return params.value.toUpperCase();
                } else {
                    return params.value;
                }
            };
        }

        if ($scope.getBooleanValue('#customHeader')) {
            params.customHeader = '[[[ This ia s sample custom header - so meta data maybe?? ]]]\n';
        }
        if ($scope.getBooleanValue('#customFooter')) {
            params.customFooter = '[[[ This ia s sample custom footer - maybe a summary line here?? ]]]\n';
        }
        if ($scope.exportFlag) {
            //console.log($scope.gridOptions)
            $scope.gridOptions.api.exportDataAsCsv(params);
        }
    }


    $scope.clearFilters = function () {
        $scope.openModalPopupOpen();
        $scope.gridOptions.api.setFilterModel(null);
        $scope.gridOptions.api.onFilterChanged();
        $scope.openModalPopupClose();
    }
    $scope.reason_code = ['Inward Remittance not realized', 'Delay in Outward Remittance', 'Commissions Not Paid', 'Delayed Payments', 'Delayed in Nostro Inward Payments', 'Delayed in Nostro Outward Payments', 'Duplicate Entries', 'Operational Manual error', 'Incorrect Resolution', 'Delay in Clearing', 'Amount: Rounding-off Difference', 'Incorrect Debit or Credit Transaction Posting', 'Transaction not from authorized source', 'Transaction Date is a Holiday', 'Reference: Error in manual input reference', 'Entry to go into different account', 'Customer Reference & Transaction Reference interchanged', 'Relates to incorrect force matching done earlier', 'Mismatch in Forward Booking', 'Posted for Future Clearing Date', 'Matching entry expected in days in future', 'Matching entry awaiting authorization', 'In Sufficient Balance', 'Dorment Account', 'Funding Not Happened', 'Originating entry later than responding entry', 'Responding entry earlier than originating entry', 'Matching entry delayed due to sudden declaration of holiday', 'Matching entry diverted due to Court Order', 'Original transaction reversed', 'Matching transaction reversed post match', 'Difference due to direct entry into EGL', 'Incorrect Branch Code', 'Duplicate entry', 'Duplicate matching entry', 'Trade not booked', 'Late bookings', 'STP issue', 'Source issue', 'IT issues', 'Margin not booked', 'Reference data issue', 'Buy/sell error', 'Duplicate trade booked', 'Counter party mismatch', 'Internal trade booking error', 'Others'];

    //force matching records

    $scope.forceMatching = function (data) {
        console.log($scope.gridOptions)
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        console.log($scope.pastGrid)
        var dataR = {}
        $scope.doGrouping = true
        dataR['groupAndForce'] = true
        dataR['groupForceMatchAuth'] = false
        dataR['selected_rec'] = []

        $scope.checkSource = []
        var sourceTypeList = []
        var sourceCount = 0
        $scope.selectedRecords = $scope.gridOptions.api.getSelectedRows()
        angular.forEach($scope.selectedRecords, function (val, key) {
            if (val != null) {
                $scope.checkSource.push(val.SOURCE_TYPE)
                dataR['selected_rec'].push(val)

                if (sourceTypeList.indexOf(val.SOURCE_TYPE) == -1) {
                    sourceTypeList.push(val.SOURCE_TYPE)
                }
            }
        })

        if (dataR['selected_rec'].length > 0) {
            sourceCount = dataR['selected_rec'][0]['SRC_COUNT']
        }

        $scope.arrayFlag = $scope.checkSource.allValuesSame()
        console.log($scope.arrayFlag)

        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['singleSideMatch'] = (sourceCount > 2) ? false : $scope.singleSide
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("please select more than one record.")
         $scope.doGrouping = false
         }
         else*/
        if ($scope.arrayFlag && !$scope.singleSide && sourceCount <= 2) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("If single record match please select checkbox")
        }
        else if (!$scope.arrayFlag && $scope.singleSide && sourceCount <= 2) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("If not single record match, please unselect single record checkbox")
        }
        else if (sourceCount > 2 && sourceCount !== sourceTypeList.length) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("Please select transactions across all the sources to proceed")
        }
        //console.log($scope.gridOptions.api.getSelectedRows())

        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.groupColumns('dummy', dataR, function (data) {
                //console.log(data)
                $rootScope.openModalPopupClose()

                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $rootScope.showSuccessMsg('Force Match Done Successfully');
                    $scope.clearDates()
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data);
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }


    //grouping records code
    $scope.singleSide = false
    $scope.successFlag = false
    $scope.grouping = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        if (dataR['selected_rec'].length < 2) {
            $rootScope.showAlertMsg("please select more than one records")
            $scope.doGrouping = false
        }
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['groupAndForce'] = false
        dataR['groupForceMatchAuth'] = false
        console.log($scope.reconId)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {

            $rootScope.openModalPopupOpen()
            ReconAssignmentService.groupColumns('dummy', dataR, function (data) {
                //console.log(data)
                $rootScope.openModalPopupClose()

                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Grouping Done Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }

    //ungrouping records code
    $scope.Ungrouping = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        if (dataR['selected_rec'].length < 2) {
            $rootScope.showAlertMsg("please select more than one record to un-group")
            $scope.doGrouping = false
        }
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['operationType'] = 'UNGROUP'
        dataR['recon_id'] = $scope.reconId
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {

            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    //$rootScope.showSuccessMsg('Job added to queue, please check the status in job post');
                    $rootScope.showSuccessMsg('Un-Grouping Done Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
            /*ReconAssignmentService.unGroupingColumns('dummy', dataR, function (data) {
             //console.log(data)

             $rootScope.openModalPopupClose()
             if (data == "Success") {
             $scope.data = {}
             $scope.successFlag = true
             $scope.clearDates()
             $rootScope.showSuccessMsg('Un-Grouping Done Successfully');
             $scope.getSelectedRecon($scope.reconId);
             } else {
             $rootScope.showAlertMsg(data)
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }

    Array.prototype.allValuesSame = function () {
        for (var i = 1; i < this.length; i++) {
            if (this[i] !== this[0])
                return false;
        }
        return true;
    }

    //group and forcematching code
    $scope.groupForceMatch = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.checkSource = []
        $scope.doGrouping = true
        dataR['selected_rec'] = []

        var sourceTypeList = []
        var srcCount = 0
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                $scope.checkSource.push(val.SOURCE_TYPE)
                dataR['selected_rec'].push(val)

                if (sourceTypeList.indexOf(val.SOURCE_TYPE) == -1) {
                    sourceTypeList.push(val.SOURCE_TYPE)
                }
            }
        })

        if (dataR['selected_rec'].length > 0) {
            srcCount = dataR['selected_rec'][0]['SRC_COUNT']
        }

        $scope.arrayFlag = $scope.checkSource.allValuesSame()
        console.log($scope.arrayFlag)
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("please select more than one records")
         $scope.doGrouping = false
         }
         else*/
        if ($scope.arrayFlag && !$scope.singleSide && srcCount <= 2) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("If single record match please select checkbox")
        }
        else if (!$scope.arrayFlag && $scope.singleSide && srcCount <= 2) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("If not single record match, please unselect single record checkbox")
        }
        else if (srcCount > 2 && srcCount !== sourceTypeList.length) {
            $scope.doGrouping = false
            $rootScope.showAlertMsg("please select transactions across all the sources to proceed")
        }

        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['authorizer'] = data['userpool']
        dataR['groupAndForce'] = true
        dataR['groupForceMatchAuth'] = true
        dataR["singleSideMatch"] = (srcCount > 2) ? false : $scope.singleSide
        dataR['operationType'] = 'SUBMIT_FOR_AUTH'
        dataR['userName'] = $rootScope.credentials.userName
        dataR['recon_id'] = $scope.reconId

        //$scope.doGrouping = false
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Force Matched and Submitted for Authorization Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                }
                else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
            /*ReconAssignmentService.groupColumns('dummy', dataR, function (data) {
             //console.log(data)

             $rootScope.openModalPopupClose()
             if (data == "Success") {
             $scope.data = {}
             $scope.successFlag = true
             $scope.clearDates()
             $rootScope.showSuccessMsg('Force Matching and Submitted for Authorization Successfully');
             $scope.getSelectedRecon($scope.reconId);
             }
             else {
             $rootScope.showAlertMsg(data)
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }


    //authorize code
    $scope.authorize = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("Please select more than one record for Authorisation")
         $scope.doGrouping = false
         }*/
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['operationType'] = 'AUTH_RECORD'
        dataR['recon_id'] = $scope.reconId
        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                //console.log(data)

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    //$rootScope.showSuccessMsg('Job added to queue, please check the status in job post');
                    $rootScope.showSuccessMsg('Records Authorized Successfully')
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data);
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }

    //reject code
    $scope.reject = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        //$rootScope.msgNotify('warning',"yet to come")
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("Please select more than one record for Rejection")
         $scope.doGrouping = false
         }*/
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['operationType'] = 'REJ_RECORDS'
        dataR['recon_id'] = $scope.reconId
        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                //console.log(data)

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Records Rejected Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }

    //rollback code
    $scope.rollbackMatched = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        //console.log(data)
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("Please select one link id / transactions to perform to  Rollback Operation")
         $scope.doGrouping = false
         }*/
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['authorizer'] = data['userpool']
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['skip'] = 5000 * $scope.currentPage
        dataR['limit'] = dataR['skip'] + 5000
        dataR['operationType'] = 'ROLLBACK_RECORDS'
        dataR['userName'] = $rootScope.credentials.userName
        dataR['recon_id'] = $scope.reconId

        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    //$rootScope.showSuccessMsg('Job added to queue, please check the status in job post');
                    $rootScope.showSuccessMsg('RollBack Done Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                }
                else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })

            /*ReconAssignmentService.rollBack('dummy', dataR, function (data) {
             //console.log(data)

             $rootScope.openModalPopupClose()
             if (data == "Success") {
             $scope.data = {}
             $scope.successFlag = true
             $scope.clearDates()
             $rootScope.showSuccessMsg('RollBack Done Successfully');
             $scope.getSelectedRecon($scope.reconId);
             }
             else {
             $rootScope.showAlertMsg(data)
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }

    //authorize rollback records code
    $scope.rollbackRoll = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        //console.log(data)
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })

        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['authorizer'] = data['userpool']
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['recon_id'] = $scope.reconId
        dataR['operationType'] = 'ROLLBACK_AUTH'

        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    //$rootScope.showSuccessMsg('Job added to queue, please check the status in job post');
                    $rootScope.showSuccessMsg('RollBack Done Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
            /*ReconAssignmentService.authorizeRollBackRecords('dummy', dataR, function (data) {
             //console.log(data)

             $rootScope.openModalPopupClose()
             if (data == "Success") {
             $scope.data = {}
             $scope.successFlag = true
             $scope.clearDates()
             $rootScope.showSuccessMsg('RollBack Done Successfully');
             $scope.getSelectedRecon($scope.reconId);
             } else {
             $rootScope.showAlertMsg(data)
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }

    //reject rollback records code
    $scope.rejectRoll = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        //console.log(data)
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })

        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['authorizer'] = data['userpool']
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['recon_id'] = $scope.reconId
        dataR['operationType'] = 'ROLLBACK_REJ'
        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.addToQueue('dummy', dataR, function (data) {
                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    //$rootScope.showSuccessMsg('Job added to queue, please check the status in job post');
                    $rootScope.showSuccessMsg('Rejection Done Successfully');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
            /*ReconAssignmentService.rejectRollBackRecords('dummy', dataR, function (data) {
             //console.log(data)

             $rootScope.openModalPopupClose()
             if (data == "Success") {
             console.log("rollback success")
             $scope.data = {}
             $scope.clearDates()
             $scope.successFlag = true
             $rootScope.showSuccessMsg('Rejection Done Successfully');
             $scope.getSelectedRecon($scope.reconId);
             } else {
             $rootScope.showAlertMsg(data)
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }

    //submit for auth code
    $scope.submitForAuthorization = function (data) {
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        //console.log(data)
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })

        console.log(fun)

        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['authorizer'] = data['userpool']
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        dataR['recon_id'] = $scope.reconId

        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.submitForAuthorization('dummy', dataR, function (data) {
                //console.log(data)

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Submitted records for authorizer sucessfully.');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }

    //send to dept user
    $scope.sendToDept = function (data) {
        console.log(data)
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        //console.log(data)
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("please select more than one records")
         $scope.doGrouping = false
         }*/
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        dataR['deptUserPool'] = data['deptuserpool']
        dataR['reason_code'] = (angular.isDefined(data['reasoncode'])) ? data['reasoncode'] : ''
        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.submitForInvestigator('dummy', dataR, function (data) {
                //console.log(data)

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Submitted records for Dept user sucessfully.');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }
    $scope.sendToReconciler = function (data) {
        console.log(data)
        $scope.colsOrder = []
        if (angular.isDefined($scope.gridOptions['columnApi'])) {
            angular.forEach($scope.gridOptions['columnApi']['_columnController']['allColumns'], function (v, k) {
                $scope.colsOrder.push(v['colDef'])
            })
        }
        $scope.pastGrid = (angular.isDefined($scope.gridOptions['columnApi'])) ? $scope.gridOptions['columnApi']['_columnController']['rowGroupColumns'] : ''
        $scope.savedSort = $scope.gridOptions.api.getSortModel()
        $scope.savedFilter = $scope.gridOptions.api.getFilterModel();
        //console.log(data)
        var dataR = {}
        $scope.doGrouping = true
        dataR['selected_rec'] = []
        angular.forEach($scope.gridOptions.api.getSelectedRows(), function (val, key) {
            if (val != null) {
                dataR['selected_rec'].push(val)
            }
        })
        /*if (dataR['selected_rec'].length < 2) {
         $rootScope.showAlertMsg("please select one more than one records")
         $scope.doGrouping = false
         }*/
        //console.log($scope.gridOptions.api.getSelectedRows())
        dataR['comments'] = (angular.isDefined(data['comments'])) ? data['comments'] : ''
        //console.log(dataR)
        if ($scope.doGrouping && angular.isDefined(dataR['selected_rec'])) {
            $rootScope.openModalPopupOpen()
            ReconAssignmentService.investigateRecords('dummy', dataR, function (data) {
                //console.log(data)

                $rootScope.openModalPopupClose()
                if (data == "Success") {
                    $scope.data = {}
                    $scope.successFlag = true
                    $scope.clearDates()
                    $rootScope.showSuccessMsg('Submitted records back to Reconciler sucessfully.');
                    $scope.getSelectedRecon($scope.reconId);
                } else {
                    $rootScope.showAlertMsg(data)
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }
    }

    //upload code
    $scope.uploadFile = function () {
        $modal.open({
            templateUrl: 'uploadfile.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                $scope.upload = function (files) {
                    console.log(files[0]);
                    $scope.fileName = files[0].name
                    if (files[0].size > 20971520) {
                        $scope.msgNotify('warning', 'Uploaded File was more than 20 MB.Please choose less than 20MB file');
                        files = [];
                    }
                    else {
                        $scope.saveFile = function (label) {
                            console.log(label)
                            console.log($scope.fileName)
                            $scope.name = files[0].name.split('.')
                            $scope.array = [label, $scope.name[1].toLowerCase()]
                            $scope.id = $scope.array.join('.')
                            console.log($scope.id)

                            $upload.upload({
                                url: '/api/utilities/' + label['fileName'] + '.' + $scope.name[1].toLowerCase() + '/upload?fileName=' + label['fileName'] + '&description=' + label['description'] + '&recon=' + $scope.reconId,
                                method: 'POST',
                                file: files[0],
                                data: {'fileName': "test", 'description': "TESTING"}
                            }).progress(function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

                            }).success(function (data, status, headers, config) {
                                if (data.status == "error") {
                                    $rootScope.showAlertMsg(data.msg)
                                }
                                else {
                                    $rootScope.showSuccessMsg('File Uploaded Succesfully')
                                    $modalInstance.dismiss('cancel');
                                    $scope.getUploadedFiles()
                                }

                            })
                        };

                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    $scope.getUploadedFiles = function () {
        $rootScope.openModalPopupOpen()
        var perColConditions = {};
        perColConditions["recon"] = $scope.reconId
        FileUploadService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            $rootScope.openModalPopupClose()
            $scope.listOfData = data.data
        })
    }

    //inbox

    $scope.getInboxTransactions = function () {
        $scope.inboxGridOptions.columnDefs = [];
        $scope.inboxGridOptions.rowData = [];
        if (angular.isDefined($scope.inboxGridOptions.api)) {
            $scope.inboxGridOptions.api.setColumnDefs([])
            $scope.inboxGridOptions.api.setRowData([])
        }
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.get_Inbox_transaction('dummy', {}, function (data) {
            console.log(data)
            $rootScope.openModalPopupClose()
            if (JSON.parse(data).length == 0) {
                $rootScope.showAlertMsg("No data found for this selection")
            }
            if (JSON.parse(data).length > 0) {
                console.log(JSON.parse(data))
                console.log(Object.keys(JSON.parse(data)[0]))
                $scope.reconsData = JSON.parse(data)
                $scope.headers = []
                $scope.columnDefs = []
                angular.forEach(Object.keys(JSON.parse(data)[0]), function (v, k) {
                    $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                })
                //$scope.headers.sort()
                angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                    if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                        $scope.columnDefs.push({
                            'headerName': val.field/*,'aggFunc': $scope.sumFunction()*/,
                            'suppressAggregation': true,
                            'hide': (val.mandatory == "M") ? false : true,
                            'field': val.name, 'width': 302,
                            'cellStyle': {'text-align': 'right'},
                            'cellRenderer': function (params) {
                                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                    return params.value.toLocaleString()
                                } else {
                                    return ''
                                }
                            }
                            //'filter': 'number'
                        })
                    }
                    else if (angular.isDefined(val.mdl_field_data_type)) {
                        if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                            $scope.columnDefs.push({
                                'headerName': val.field,
                                'suppressAggregation': true,
                                'suppressMenu': true,
                                'hide': (val.mandatory == "M") ? false : true,
                                'field': val.name, 'width': 302,
                                cellRenderer: function (params) {
                                    if (angular.isDefined(val.mdl_field_data_type)) {
                                        if (val.mdl_field_data_type == "DATE") {
                                            if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                if (params.value.toString().length == 13) {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                } else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                                //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                return date
                                            } else {
                                                return "";
                                            }
                                        } else {
                                            return params.value;
                                        }
                                    }
                                }
                            })
                        }
                        else {
                            $scope.columnDefs.push({
                                'headerName': val.field,
                                'suppressAggregation': true,
                                'hide': (val.mandatory == "M") ? false : true,
                                'field': val.name, 'width': 302,
                                'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                            })
                        }
                    }
                    else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                        $scope.columnDefs.push({
                            'headerName': val.field,
                            'suppressAggregation': true,
                            'suppressMenu': true,
                            'hide': (val.mandatory == "M") ? false : true,
                            'field': val.name, 'width': 302,
                            cellRenderer: function (params) {
                                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                    if (params.value.toString().length == 13) {
                                        if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                            var date = $filter('dateTimeFormat')(params.value);
                                        }
                                        else {
                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                        }
                                    } else {
                                        if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                            var date = $filter('dateTimeFormat')(params.value);
                                        }
                                        else {
                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                        }
                                    }
                                    //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                    return date
                                } else {
                                    return "";
                                }
                            }
                        })

                    }
                    else {
                        $scope.columnDefs.push({
                            'headerName': val.field,
                            'suppressAggregation': true,
                            'hide': (val.mandatory == "M") ? false : true,
                            'field': val.name, 'width': 302,
                            'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                        })
                    }

                })
                $scope.reconName = $scope.columnDefs[$scope.columnDefs.length - 1]
                $scope.columnDefs.splice($scope.columnDefs.length - 1, 1)
                console.log($scope.reconName)
                $scope.columnDefs.unshift($scope.reconName)
                $scope.inboxGridOptions.columnDefs = $scope.columnDefs;
                $scope.inboxGridOptions.rowData = $scope.reconsData;
                $scope.inboxGridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.inboxGridOptions.api.setRowData($scope.reconsData)
            }
        })
    }


    //redirection from other tabs code
    $scope.suspenseFlag = false
    var localstorageValues = {}
    var localstorageValuesSummary = {}
    var localstorageValuesSuspense = {}
    localstorageValues = JSON.parse(localStorage.getItem("saved"))
    localstorageValuesSummary = JSON.parse(localStorage.getItem("summary"))
    localstorageValuesSuspense = JSON.parse(localStorage.getItem("suspense"))
    console.log(localstorageValues)
    console.log(localstorageValuesSummary)
    console.log(localstorageValuesSuspense)
    $scope.exportAllReport = ''
    if ($location.path() == '/recons') {
        $scope.suspenseFlag = false
        $scope.selectedFilter = "UnMatched"
        $scope.getReconsData()
        //from dashboard
        if (angular.isDefined(localstorageValues) && localstorageValues != null) {
            if (localstorageValues['from'] == 'dashboard') {
                $rootScope.userpoolRole = localstorageValues['userRole']
                $rootScope.userpoolName = localstorageValues['userPoolName']
                $scope.process = localstorageValues['businessProcess']
                $scope.getBusiness($scope.process)
                $scope.businessContext = localstorageValues['businessId']
                $scope.getRecons($scope.businessContext)
                $scope.reconId = localstorageValues['reconId']
                $scope.changeGrid('UnMatched')
                if (localstorageValues != null) {
                    if (Object.keys(localstorageValues).length > 0) {
                        localStorage.removeItem('saved')
                    }
                }
                if (localstorageValuesSummary != null) {
                    if (Object.keys(localstorageValuesSummary).length > 0) {
                        localStorage.removeItem('summary')
                    }
                }
                if (localstorageValuesSuspense != null) {
                    if (Object.keys(localstorageValuesSuspense).length > 0) {
                        localStorage.removeItem('suspense')
                    }
                }

            }
        }
        //from suspense
        if (angular.isDefined(localstorageValuesSuspense) && localstorageValuesSuspense != null) {
            if (localstorageValuesSuspense['from'] == 'suspense') {
                $scope.suspenseFlag = true
                $rootScope.userpoolRole = localstorageValuesSuspense['userRole']
                $rootScope.userpoolName = localstorageValuesSuspense['userPoolName']
                $scope.process = localstorageValuesSuspense['businessProcess']
                $scope.getBusiness($scope.process)
                $scope.businessContext = localstorageValuesSuspense['businessId']
                $scope.accountPoolName = localstorageValuesSuspense['account_pool_name']
                $scope.accountNumber = localstorageValuesSuspense['account_number']
                $scope.getRecons($scope.businessContext)
                $scope.reconId = localstorageValuesSuspense['reconId']
                $scope.changeGrid('UnMatched')
                if (localstorageValues != null) {
                    if (Object.keys(localstorageValues).length > 0) {
                        localStorage.removeItem('saved')
                    }
                }
                if (localstorageValuesSummary != null) {
                    if (Object.keys(localstorageValuesSummary).length > 0) {
                        localStorage.removeItem('summary')
                    }
                }
                if (localstorageValuesSuspense != null) {
                    if (Object.keys(localstorageValuesSuspense).length > 0) {
                        localStorage.removeItem('suspense')
                    }
                }

            }
        }
        //from summary both from dashboard and summary
        if (angular.isDefined(localstorageValuesSummary) && localstorageValuesSummary != null) {
            if (localstorageValuesSummary['from'] == 'summary') {
                $scope.suspenseFlag = false
                $rootScope.userpoolRole = localstorageValuesSummary['userRole']
                $rootScope.userpoolName = localstorageValuesSummary['userPoolName']
                $scope.process = localstorageValuesSummary['businessProcess']
                $scope.getBusiness($scope.process)
                $scope.businessContext = localstorageValuesSummary['businessId']
                $scope.getRecons($scope.businessContext)
                $scope.reconId = localstorageValuesSummary['reconId']
                $scope.changeGrid('UnMatched')
                if (localstorageValues != null) {
                    if (Object.keys(localstorageValues).length > 0) {
                        localStorage.removeItem('saved')
                    }
                }
                if (localstorageValuesSummary != null) {
                    if (Object.keys(localstorageValuesSummary).length > 0) {
                        localStorage.removeItem('summary')
                    }
                }
                if (localstorageValuesSuspense != null) {
                    if (Object.keys(localstorageValuesSuspense).length > 0) {
                        localStorage.removeItem('suspense')
                    }
                }
            }
        }
    }

    //Job Post Tab code starts here
    $scope.jobPostOptions = {};
    $scope.jobPostOptions.init = false;
    $scope.jobPostOptions.isLoading = true;
    $scope.jobPostOptions.isTableSorting = "created_date";
    $scope.jobPostOptions.sortField = "created_date";
    $scope.jobPostOptions.sortBy = -1

    $scope.viewby = 10000;
    $scope.totalItems = 10000;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 1; //Number of pager buttons to show

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function (page) {
        $scope.currentPage = page - 1
        console.log(page)
        console.log('Page changed to: ' + $scope.currentPage);
        dataR = {}
        dataR['business_context_id'] = $scope.businessContext
        dataR['recon_id'] = $scope.reconId
        dataR['record_status'] = 'ACTIVE'
        dataR['suspenseFlag'] = $scope.suspenseFlag
        dataR['accountNumber'] = $scope.accountNumber
        dataR['userPools'] = $rootScope.userpoolName
        dataR['userRole'] = $rootScope.userpoolRole
        dataR['recon_name'] = $scope.reconName
        dataR['skip'] = $scope.viewby * $scope.currentPage
        dataR['limit'] = dataR['skip'] + $scope.viewby
        console.log(Object.keys($scope.matchedTabFilter).length > 0)
        if (Object.keys($scope.matchedTabFilter).length > 0) {
            $scope.getFilteredDataByDates($scope.matchedTabFilter, $scope.viewby * $scope.currentPage, dataR['skip'] + $scope.viewby)
        }
        else if (angular.isDefined($scope.reconId) & angular.isDefined($scope.businessContext) &
            angular.isDefined($scope.reconId) & angular.isDefined($scope.reconId)) {

            ReconAssignmentService.getReconMatched('dummy', dataR, function (data) {
                console.log('ssssssssssssssssssssssss------------->', data)
                $rootScope.openModalPopupClose()
                $scope.getUserpools()
                dataRec = {}
                var uniqueUICols = []
                dataRec['recon_id'] = $scope.reconId
                if (angular.isDefined($rootScope.reconID)) {
                    dataRec['recon_id'] = $rootScope.reconID
                }
                if (angular.isDefined(data['background_process']) & data['background_process']) {
                    $rootScope.showAlertMsg("Background Job is in progess,please wait")
                    return
                }
                dataRec['record_status'] = "ACTIVE"
                $scope.txnCount = data['totalMatchedCount']
                $scope.totalItems = data['totalMatchedCount']
                $scope.maxSize = parseInt($scope.totalItems / $scope.viewby)
                if (data['total_count'] == 0) {
                    $rootScope.showAlertMsg("No Data Found.")
                } else if (data['ignore_load']) {
                    $rootScope.showAlertMsg("Please use export all to retrieve the records")
                }
                else {
                    $scope.successFlag = false
                    ReconAssignmentService.getReconColData('dummy', dataRec, function (datarec) {
                        //console.log(datarec)

                        $scope.headers = []
                        $scope.columnDefs = [
                            {
                                headerName: '',
                                'width': 30,
                                checkboxSelection: true,
                                'suppressAggregation': true,
                                suppressSorting: true,
                                suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                                pinned: true
                            }
                        ]
                        angular.forEach(JSON.parse(datarec), function (val, key) {
                            //console.log(val)
                            $scope.headers.push({
                                'field': val.UI_DISPLAY_NAME,
                                "name": val.MDL_FIELD_ID,
                                "mandatory": val.MANDATORY,
                                "mdl_field_data_type": val.MDL_FIELD_DATA_TYPE
                            })
                            uniqueUICols.push(val.MDL_FIELD_ID)
                        })
                        angular.forEach($scope.staticCols, function (v, k) {
                            if (uniqueUICols.indexOf(v) == -1) {
                                $scope.headers.push({'field': v, "name": v, 'mandatory': "M"})
                            }
                        })
                        //$scope.headers.sort()
                        angular.forEach($scope.headers, function (val, key) {
//                            if (val.name == "mdl_field_data_type"){
//                                $scope.columnDefs.push({'headerName':val.field,'suppressAggregation':true,'hide':true,'field':val.name})
//                            }
                            if ($scope.amountFrmtCols.indexOf(val.name) != -1) {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    'width': 302,
                                    'cellStyle': {'text-align': 'right'},
                                    'cellRenderer': function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            return params.value.toLocaleString()
                                        } else {
                                            return ''
                                        }
                                    }
                                    //'filter': 'number'
                                })
                            }
                            else if (angular.isDefined(val.mdl_field_data_type)) {
                                if (val.mdl_field_data_type == "DATE" || val.mdl_field_data_type == "TIMESTAMP") {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'suppressMenu': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        cellRenderer: function (params) {
                                            if (angular.isDefined(val.mdl_field_data_type)) {
                                                if (val.mdl_field_data_type == "DATE") {
                                                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                                        if (params.value.toString().length == 13) {
                                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                        } else {
                                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                        }
                                                        //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                                        return date
                                                    } else {
                                                        return "";
                                                    }
                                                } else {
                                                    return params.value;
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    $scope.columnDefs.push({
                                        'headerName': val.field,
                                        'suppressAggregation': true,
                                        'width': 302,
                                        'hide': (val.mandatory == "M") ? false : true,
                                        'field': val.name,
                                        'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                    })
                                }
                            }
                            else if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE" || val.field == "STATEMENT_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'width': 302,
                                    'suppressMenu': true,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val.field == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({
                                    'headerName': val.field,
                                    'suppressAggregation': true,
                                    'width': 302,
                                    'hide': (val.mandatory == "M") ? false : true,
                                    'field': val.name,
                                    'filter': (val.mdl_field_data_type == "DATE") ? 'epochtodate' : 'text'
                                })
                            }

                        })
                        //console.log($scope.columnDefs)
                        $scope.reconsData = JSON.parse(data['data'])
                        if (angular.isDefined($scope.pastGrid) && $scope.pastGrid != '') {
                            angular.forEach($scope.pastGrid, function (val, key) {
                                angular.forEach($scope.columnDefs, function (v, k) {
                                    if (val.colId == v.field) {
                                        $scope.columnDefs[k]['rowGroupIndex'] = key
                                    }
                                })
                            })
                        }
                        $scope.gridOptions.groupSelectsChildren = false;
                        if (angular.isDefined($scope.colsOrder) && $scope.colsOrder.length > 0) {
                            $scope.columnDefs = $scope.colsOrder
                        }
                        $scope.gridOptions.columnDefs = $scope.columnDefs;
                        $scope.gridOptions.rowData = $scope.reconsData;
                        $scope.gridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.gridOptions.api.setRowData($scope.reconsData)
                        if (angular.isDefined($scope.savedSort)) {
                            $scope.gridOptions.api.setSortModel($scope.savedSort)
                        }
                        if (angular.isDefined($scope.savedFilter)) {
                            $scope.gridOptions.api.setFilterModel($scope.savedFilter)
                            $scope.gridOptions.api.onFilterChanged();
                        }
                        //console.log($scope.gridOptions)
                    })
                }
            }, function (err) {
                $rootScope.showErrormsg(err.msg)
            })
        }


    };

    $scope.setItemsPerPage = function (num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1; //reset to first paghe
    }
    var perColConditions = {};
    perColConditions["logged_user"] = $rootScope.credentials.userName
    $scope.jobPostOptions.perColConditions = perColConditions
    $scope.getJobPost = function () {
        $scope.jobPostOptions.searchFields = {}
        $scope.jobPostOptions.service = ReconJobPost;
        $scope.jobPostOptions.headers = [
            [
                {name: "Recon ID", searchable: true, sortable: true, dataField: "recon_id", colIndex: 0},
                {name: "Recon Name", searchable: true, sortable: true, dataField: "recon_name", colIndex: 1},
                {name: "Work Pool Name", searchable: true, sortable: true, dataField: "work_pool_name", colIndex: 2},
                {
                    name: "Created Date",
                    searchable: true,
                    sortable: true,
                    dataField: "created_date",
                    colIndex: 3
                },
                {name: "Description", searchable: true, sortable: true, dataField: "job_msg", colIndex: 4},
                {name: "Error Msg", searchable: true, sortable: true, dataField: "err_msg", colIndex: 5},
                {
                    name: "Job Status",
                    searchable: true,
                    sortable: true,
                    dataField: "job_status",
                    filter: "jobStatus", isCompile: true,
                    isHtml: true,
                    colIndex: 6
                }
            ]
        ];
        $scope.jobPostOptions.reQuery = true;
    }

    $scope.getJobPost()

}

