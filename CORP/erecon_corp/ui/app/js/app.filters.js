var app = angular.module("webApp.filters");
app.filter("epochtodate", [
    function () {
        return function (utcSeconds) {
            var d = new Date()
            var n = d.getTimezoneOffset(); //Offset in minutes
            var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
            d.setUTCSeconds(utcSeconds - (n * 60));
            return d;
        }

    }]);
app.filter("tolocal", function () {
    return function (input) {
        var offset = new Date().getTimezoneOffset() * 60;
        var d = new Date((input - offset) * 1000);
        return d.toString();
    }
});
app.filter('formatNumber', function() {
  return function(input, decimalPlaces) {
   if(isNaN(input))
        return input;
     else {
       decimalPlaces = (angular.isDefined(decimalPlaces)) ? decimalPlaces : 2;
      return input.toFixed(decimalPlaces);
    }
  };
});
app.filter("arraytocsv", function () {

    return function (input, maxlength) {
        if (!angular.isDefined(maxlength))
        {
            maxlength = 10;
        }
        var resp = "";
        if (angular.isArray(input))
        {
            resp = input.join(", ");
            if (angular.isDefined(maxlength) && resp.length > maxlength)
            {
                resp = resp.substr(0, maxlength - 4);
                resp += " ...";
            }
        }
        else
        {
            //console.log("arraytocsv: Input not array!!!");
            resp = input;
        }
        return resp;
    }
});

app.filter("showForRole", ['$rootScope', function ($rootScope) {
        return function (input) {
            if (input)
            {
                return ($rootScope.credentials && input.indexOf($rootScope.credentials["role"]) >= 0);
            }
            return true;
        }
    }]);
app.filter('bprofileUsedCompanies', function () {
    return function (input) {
        var retValue = "";
        if (input != undefined) {
            angular.forEach(input, function (value, key) {
                retValue += value + '. <br/>';
            });
        } else {
            retValue = "";
        }
        return retValue;
    }
});
app.filter('bprofileContent', function () {
    return function (input) {
        var retValue = "";
        angular.forEach(input, function (value, key) {
            //retValue += value.productName+' from agreement '+ value.agreementType+'. ';
            retValue += value.productName + '. <br/>';
        });
        return retValue;
    }
});
app.filter('capitalize', function () {
    return function (input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});
app.filter("contactAction", function () {
    return function (input, data) {
        if (angular.isDefined(data)) {
            var ret = [{"name": "Ignore"}];
            if (data.isADUser && data.isCWContact) {
                if (data.status == "True") {
                    ret.push({"name": "Activate"});
                } else {
                    ret.push({"name": "Deactivate"});
                }
            } else if (data.isADUser && !data.isCWContact) {
                ret.push({"name": "Add"});
            } else if (!data.isADUser && data.isCWContact) {
                if (data.status == "True") {
                    ret.push({"name": "Activate"});
                } else {
                    ret.push({"name": "Deactivate"});
                }
            }
            return ret;
        }
    }
});
app.filter("requiredFieldFilter", function () {
    return function (input) {
        if (input) {
            return '*';
        } else {

            return '';
        }
    }
});
app.filter("approvalStatusFilter", function () {
    return function (input) {
        if (input == undefined) {
            return 'Agent Not Installed';
        } else {
            //var lookup = {'PendingApproval': '<img title="Pending Approval" src="/app/img/busy_25.png">', 'Approved': '<img title="Approved" src="/app/img/available_25.png">'}
            var lookup = {PendingApproval: 'Pending Approval', Approved: 'Approved', AgentNotInstalled: "Agent Not Installed", Installed: "Installed", AgentDisconnected: "Agent Disconnected"}
            return lookup[input]
        }
        //{PendingApproval: "/app/img/busy_25.png", Approved:"/app/img/available_25.png", null: "/app/img/offline_25.png", undefined: "/app/img/offline_25.png"},
    }
});

app.filter("syncTypeFilter", function () {
    return function (input) {
        if (input != undefined) {
            var lookup = {manualsync: 'Manual Sync', globalsync: 'Global Sync', autosync: 'Schedule Sync'}
            return lookup[input]
        }
    }
});
app.filter("updateDestMail", function () {
    return function (input) {
        var ret = input;
        if (input == null || input == "") {
            ret = 'UPDATE';
        }
        return ret;
    }
});
app.filter("fullNumber", function () {
    return function (input) {
        var ret = "0"
        if (input == null) {
            ret = '0';
        } else {
            ret = Math.floor(input);
        }
        return ret;
    };
});
app.filter("percentageFilter", function () {
    return function (input) {
        var ret = "0"
        if (input == null) {
            ret = '0 %';
        } else {
            ret = input + " %";

        }
        return ret;
    };
});
app.filter("counterCheck", function () {
    return function (input) {
        var ret = ""
        if (input == 0) {
            ret = '';
        } else {
            ret = input;
        }
        return ret;
    };
});
app.filter("countFilter", function () {
    return function (input) {
        var ret = "0"
        if (input == null) {
            ret = '0';
        } else {
            ret = input;
        }
        return ret;
    };
});
app.filter("defaultQueueCheck", function () {
    return function (input) {
        var ret = "<p></p>"
        if (input) {
            ret = '<i class="fa fa-check-square fa-lg"></i>';
        } else {
            ret = '<i class="fa fa-times-circle fa-lg"></i>';
        }
        return ret;
    };
});
app.filter("formatOwner", function () {
    return function (input) {
        if (angular.isDefined(input["location"]))
        {
            return input["userName"] + " / " + input["location"];
        }
        else
        {
            return input["userName"];
        }
    }
});

app.filter('statusImg', function () {
    return function (input) {
        var img = "";
        return img;
    }
});

app.filter("formatDate", function () {
    return function (input) {
        if (input == undefined || input == "") {
            return "-";
        }
        var d;
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if (input == undefined)
            return ' ';
        if (input == 'Invalid Date')
            return ' ';
        d = new Date(input * 1000);
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        var dd = d.getDate()
        dd = (dd < 10) ? '0' + dd : dd;
        var mm = d.getMonth() + 1;
        mm = (mm < 10) ? '0' + mm : mm;
        var yy = d.getFullYear() % 100
        yy = (yy < 10) ? '0' + yy : yy;
        var hours = d.getHours()
        var minutes = d.getMinutes();
        var stime = hours + ":" + minutes + " ";
        if (hours > 11) {
            if (hours == 12) {
                stime = hours + ":" + minutes + " " + "PM";
            } else {
                stime = (hours - 12) + ":" + minutes + " " + "PM";
            }
        } else {
            if (hours == 0) {
                hours = 12;
            }
            stime = hours + ":" + minutes + " " + "AM";
        }
        var hrfix = stime.split(":");
        var minfix = hrfix[1].split(" ");
        var ap = minfix[1];
        hrfix = (parseInt(hrfix[0]) < 10) ? "0" + hrfix[0] : hrfix[0];
        minfix = (parseInt(minfix[0]) < 10) ? "0" + minfix[0] : minfix[0];
        stime = hrfix + ":" + minfix + " " + ap;
        return days[ d.getDay() ] + ' ' + mm + '-' + dd + '-' + yy + ' ' + stime;
    }
});

app.filter("formatDateInfo", function () {
    return function (input) {
        if (input == undefined || input == "") {
            return "-";
        }
        var res = input.split(" ");
        if(isNaN(parseFloat(res[1])))
        {
            return input;
        }
        var input=res[1];
        var d;
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if (input == undefined)
            return ' ';
        if (input == 'Invalid Date')
            return ' ';
        d = new Date(input * 1000);
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        var dd = d.getDate()
        dd = (dd < 10) ? '0' + dd : dd;
        var mm = d.getMonth() + 1;
        mm = (mm < 10) ? '0' + mm : mm;
        var yy = d.getFullYear() % 100
        yy = (yy < 10) ? '0' + yy : yy;
        var hours = d.getHours()
        var minutes = d.getMinutes();
        var stime = hours + ":" + minutes + " ";
        if (hours > 11) {
            if (hours == 12) {
                stime = hours + ":" + minutes + " " + "PM";
            } else {
                stime = (hours - 12) + ":" + minutes + " " + "PM";
            }
        } else {
            if (hours == 0) {
                hours = 12;
            }
            stime = hours + ":" + minutes + " " + "AM";
        }
        var hrfix = stime.split(":");
        var minfix = hrfix[1].split(" ");
        var ap = minfix[1];
        hrfix = (parseInt(hrfix[0]) < 10) ? "0" + hrfix[0] : hrfix[0];
        minfix = (parseInt(minfix[0]) < 10) ? "0" + minfix[0] : minfix[0];
        stime = hrfix + ":" + minfix + " " + ap;
        var finalTime= days[ d.getDay() ] + ' ' + mm + '-' + dd + '-' + yy + ' ' + stime;
        res[1]=finalTime;
        var fullText="";
        for(i=0;i<res.length;i++)
        {
            fullText+=res[i]+" ";
        }
        return fullText;
    }
});

app.filter('repeatTime', function () {
    return function (input) {
        if (input == undefined || input == "") {
            return "-";
        }
        var hrs = input.hour;
        var mins = input.minute;
        if (hrs != undefined) {
            var hval = hrs.split("/");
            return  hval[1] + ' Hr(s)';
        } else {
            var mval = mins.split("/");
            return  mval[1] + ' Min(s)';
        }
    }
});

app.filter('memorySize', function () {
    return function (input) {
        var space = (parseInt(input) / 1073741824);
        if (Math.round(space) == 0) {
            return  uspace = Math.abs(space).toFixed(2) + ' MB';
        } else {
            return uspace = Math.abs(space).toFixed(2) + ' GB';
        }
    }
});

app.filter('backupType', function () {
    return function (input) {
        if (input == 'DiskStor') {
            return  "Image";
        } else if (input=='FileStor') {
            return "File";
        } else if (input=='vmWare') {
            return "VM";
        }

	else{
		return "";
	}
    }
});

app.filter('snapshotStatus', function () {
    return function (input) {
        if (input == 'testvm' || input == 'livevm') {
            return  "Virtualized";
        } else if (input == 'mount') {
            return "Mounted";
        } else if (input == 'iscsi') {
            return "iSCSI Started";
        } else {
            return input;
        }
    }
});

app.filter('bytes', function () {
    return function (bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes))
            return '-';
        if (typeof precision === 'undefined')
            precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
});

app.filter('getSize', function () {
    return function (input) {
        if (isNaN(parseInt(input)))
            return '';
        var gb = (parseInt(input) / (1024 * 1024 * 1024));
        var fspace = gb.toFixed(1);
        return fspace;
    }
});

app.filter('volUuid', function () {
    return function (input) {
        var re = new RegExp('[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}');
        var m = re.exec(input);
        if (m == null) {
            return "";
        }
        var s = "";
        var uid = "";
        s = s + m[0];
        return uid = s.replace(/-/g, "_");
    }
});

app.filter("formatSTime", function () {
    return function (input) {
        if (input == undefined || input == "") {
            return "-";
        }
        var h = input.hour;
        if (input.minute == undefined) {
            input.minute = 0;
        }
        var m = input.minute;
        var stime = h + ":" + m;
        if (h > 11) {
            if (h == 12) {
                stime = h + ":" + m + " PM";
            } else {
                stime = (h - 12) + ":" + m + " PM";
            }
        } else {
            if (h == 0) {
                h = 12;
            }
            stime = h + ":" + m + " AM";
        }
        var hrfix = stime.split(":");
        var minfix = hrfix[1].split(" ");
        var ap = minfix[1];
        hrfix = (parseInt(hrfix[0]) < 10) ? "0" + hrfix[0] : hrfix[0];
        minfix = (parseInt(minfix[0]) < 10) ? "0" + minfix[0] : minfix[0];
        stime = hrfix + ":" + minfix + " " + ap;
        return stime;
    }
});

app.filter('IpAdd', function () {
    return function (input) {
        var ipadd = JSON.stringify(input);
        ipadd = ipadd.replace('["', "");
        ipadd = ipadd.replace('"]', "");
        return ipadd;
    }
});

app.filter('IqnsFormat', function () {
    return function (input) {
        var iqnTable = '';
        try {
            //var input = JSON.parse(input);
            angular.forEach(input, function (i, vval) {
                iqnTable += "<b> " + vval + ': </b>' + i + "<br>";
            });
        } catch (e) {
            console.log("in err");
            return input;
        }
        return iqnTable;
    }
});



app.filter('status', function () {
    return function (input) {
        var valueStatus = (input) ? 'yes' : 'no';
        return  valueStatus;
    }
});

app.filter('Disk', function () {
    return function (input) {
        var isBoot = (input) ? 'lightGreen' : 'lightGray';
        return  isBoot;
    }
});

app.filter('rowStyle', function () {
    return function (input) {
        var isBoot = (input) ? 'lightGreen' : '';
        return  isBoot;
    }
});

app.filter('MountName', function () {
    return function (input) {
        var mntPoint = JSON.stringify(input);
        mntPoint = mntPoint.replace('["', "");
        mntPoint = mntPoint.replace('"]', "");
        mntPoint = mntPoint.replace('\\\\', "");
        return  mntPoint;

    }
});
app.filter("volumetodisks", function() {
    return function(input){
        var disks = []
        angular.forEach(input, function(a,b){
            var disk = '';
            var mntPoint = JSON.stringify(a.mountPointNames);
            mntPoint = mntPoint.replace('["', "");
            mntPoint = mntPoint.replace('"]', "");
            mntPoint = mntPoint.replace('\\\\', "");
            disk = ' '+mntPoint+' '+a.label;
            disks.push(disk);
        })
        return disks.join(',');
    }
});
app.filter("networktoips", function() {
    return function(input){
        var networks = []
        angular.forEach(input, function(a,b){
            var ipaddress = JSON.stringify(a.ipAddress);
            ipaddress = ipaddress.replace('["', "");
            ipaddress = ipaddress.replace('"]', "");
            ipaddress = ipaddress.replace('\\\\', "");
            networks.push(' '+ipaddress);
        })
        return networks.join(',');
    }
});
app.filter('mapVal', function () {
    return function (input) {

    }
});

app.filter("serverStatus", function () {
    return function (input) {
        return (input["totalServers"] - input["freeServers"]) + " / " + input["totalServers"];
    }
});

app.filter("workstationStatus", function () {
    return function (input) {
        return (input["totalWorkstations"] - input["freeWorkstations"]) + " / " + input["totalWorkstations"];
    }
});

app.filter("vaultStatus", function () {
    return function (input) {
        return (input["totalServers"] - input["freeServers"]) + " / " + input["totalServers"];
    }
});

app.filter("psIp", function () {
    return function (input) {
        resp = [];
        angular.forEach(input, function (value, key) {
            angular.forEach(value["ipAddress"], function (v, k) {
                resp.push(v);
            });
        });
        return resp.join();
    }
});

app.filter("psBackupStatus", function () {
    return function (input) {
        return input["Completed"] + "/" + input["Running"] + "/" + input["Failed"] + "/" + input["Missed"]
    }
});

app.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length - end.length) + end;
        }

    };
});

app.filter('formatTimeStamp',['$filter', function ($filter) {
    return function (input) {
        if (input != null && input != "" && input != undefined) {
            var date = new Date(input * 1000);

            return $filter('date')(new Date(date), 'dd/MM/yyyy hh:mm a');
        } else {
            return ""
        }

    }
}]);

app.filter('clusterState', function () {
    return function (input) {
        if (input != null && input != "" && input != undefined) {
            var clusterMap = {
                0: "STARTUP",
                1: "PRIMARY",
                2: "SECONDARY",
                3: "RECOVERING",
                4: "FATAL",
                5: "STARTUP2",
                6: "UNKNOWN",
                7: "ARBITER",
                8: "DOWN",
                9: "ROLLBACK",
                10: "SHUNNED"
            }
            return clusterMap[input];
        } else {
            return ""
        }

    }
});


app.filter('changeValue', function () {
    return function (input) {
        if (input) {
            return  "Virtualized";
        } else if(input==false) {
            return "Not Virtualized";
        }else{
		return "";
	}
    }
});

app.filter('mapStatus', function () {
    return function (input) {
        if (input) {
            return  "Enabled";
        } else {
            return "Disabled";
        }
    }
});

app.filter('mapValue', function () {
    return function (input) {
        if (input) {
            return  "Yes";
        } else {
            return "No";
        }
    }
});

app.filter('capitalizeFirstLetter', function () {
    return function (input) {
        if (input=="auto") {
            return  "Auto Clear";
        } else {
            return input.charAt(0).toUpperCase() + input.slice(1);
        }
    }
});

app.filter('addGB', function () {
    return function (input) {
	if(input==null || input==undefined)
	{
	}
	else if (input.charAt(input.length-1)=="T") {
            return  input.substring(0, input.length - 1)+" TB";
        }
        else if (input.charAt(input.length-1)=="G") {
            return  input.substring(0, input.length - 1)+" GB";
        }
        else if(input.charAt(input.length-1)=="M"){
            return  input.substring(0, input.length - 1)+" MB";
        }
        else if(input.charAt(input.length-1)=="K"){
            return input.substring(0, input.length - 1)+" KB";
        }
	else{
            return input;
        }
    }
});

app.filter('toUser', function () {
    return function (input) {
        if (input=="customer") {
            return  "user";
        }
        else {
            return  input;
        }

    }
});

app.filter("otherFormatDate", function () {
    return function (input) {
        if (input == undefined || input == "") {
            return "-";
        }
        var d;
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if (input == undefined)
            return ' ';
        if (input == 'Invalid Date')
            return ' ';
        d = new Date(input * 1000);
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        var dd = d.getDate()
        dd = (dd < 10) ? '0' + dd : dd;
        var mm = d.getMonth() + 1;
        mm = (mm < 10) ? '0' + mm : mm;
        if(mm==1)
        {
            mm="Jan";
        }
        else if(mm==2)
        {
            mm="Feb";
        }
        else if(mm==3)
        {
            mm="Mar";
        }
        else if(mm==4)
        {
            mm="Apr";
        }
        else if(mm==5)
        {
            mm="May";
        }
        else if(mm==6)
        {
            mm="Jun";
        }
        else if(mm==7)
        {
            mm="Jul";
        }
        else if(mm==8)
        {
            mm="Aug";
        }
        else if(mm==9)
        {
            mm="Sep";
        }
        else if(mm==10)
        {
            mm="Oct";
        }
        else if(mm==11)
        {
            mm="Nov";
        }
        else
        {
            mm="Dec";
        }
        var yy = d.getFullYear() % 100
        yy = (yy < 10) ? '0' + yy : yy;
        var hours = d.getHours()
        var minutes = d.getMinutes();
        var stime = hours + ":" + minutes + " ";
        if (hours > 11) {
            if (hours == 12) {
                stime = hours + ":" + minutes + " " + "PM";
            } else {
                stime = (hours - 12) + ":" + minutes + " " + "PM";
            }
        } else {
            if (hours == 0) {
                hours = 12;
            }
            stime = hours + ":" + minutes + " " + "AM";
        }
        var hrfix = stime.split(":");
        var minfix = hrfix[1].split(" ");
        var ap = minfix[1];
        hrfix = (parseInt(hrfix[0]) < 10) ? "0" + hrfix[0] : hrfix[0];
        minfix = (parseInt(minfix[0]) < 10) ? "0" + minfix[0] : minfix[0];
        stime = hrfix + ":" + minfix + " " + ap;
        return days[ d.getDay() ] + ' ' + dd + '-' + mm + '-' + yy + ' ' + stime;
    }
});

app.filter('filterSnapMountStatus', function () {
    return function (input) {
        if (input) {
            return  "Successful";
        } else if(input==false) {
            return "Failed";
        }else{
		return "";
	}
    }
});

app.filter('backedupORnot', function () {
    return function (input) {
        if (input) {
            return  "Added to backup";
        }
        else {
            return  "Not added to backup";
        }

    }
});
app.filter('badge', function ($sce) {
    return function (input) {
   	var retValue = "";
	if (angular.isDefined(input)){
	    retValue += '<span class="badge" >'+input+'</span>'
	}
	return $sce.trustAsHtml(retValue);

    }
});

app.filter('business',['BusinessService', function (BusinessService) {
    return function (input) {
        if (input) {
            var data = input.split(',')
            var returnOutput = []
            angular.forEach(data, function (val,key) {
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = val
                BusinessService.get(undefined,{"query": {"perColConditions": perColConditions}}, function (data) {
                    console.log(data)
                    returnOutput.push(data.WORKPOOL_NAME)

                })
            })

            return  returnOutput.join(',')
        }
        else {
//            return  "Not added to backup";
        }

    }
}]);

app.filter('dateFormat', function () {
    return function (input) {
        if (angular.isDefined(input) && input != null && input != undefined) {
            if (input.toString().length == 13) {
                var date = new Date(input)
            } else {
                var date = new Date(input * 1000);
            }
            date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
            return date.toLocaleDateString();
        } else {
            return "";
        }
    }
});
app.filter('dateTimeFormat', function () {
    return function (input) {
        function isValidDate(d){if(Object.prototype.toString.call(d)!=="[object Date]")return false;return!isNaN(d.getTime());}
        if (angular.isDefined(input) && input != null && input != undefined) {
            var d;
            d = new Date(input);
            d = new Date(d.getTime()+d.getTimezoneOffset()*60*1000);
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var stime = hours + ":" + minutes + " ";
            if(hours > 11){
                if(hours==12){
                    stime = hours+":" + minutes + " " +"PM";
                }else{
                    stime = (hours-12)+ ":" + minutes + " " +"PM";
                }
            } else {
                if (hours==0){
                    hours = 12;
                }
            stime = hours+":" + minutes + " " +"AM";
            }
            var hrfix = stime.split(":");
            var minfix = hrfix[1].split(" ");
            var ap = minfix[1];
            hrfix = (parseInt(hrfix[0]) < 10) ? "0"+hrfix[0] : hrfix[0];
            minfix = (parseInt(minfix[0]) < 10) ? "0"+minfix[0] : minfix[0];
            stime = hrfix+":"+minfix+" "+ap;
            return d.toLocaleDateString()+ " " +stime;
        } else {
            return "";
        }
    }
});
app.filter('memSize', ['$rootScope', function ($rootScope) {
return function (input) {
    if($rootScope.systemType=="vmWare")
    {
        return "NaN";
    }
    else
    {

        var space = (parseInt(input) / 1073741824);
        if (Math.round(space) == 0) {
            return  uspace = Math.abs(space).toFixed(2) + ' MB';
        } else {
            return uspace = Math.abs(space).toFixed(2) + ' GB';
        }
    }
    }
}]);

/*app.filter('inrCurrencyFormat', function () {
    return function (input) {
        if (angular.isDefined(input) && input != '') {
            var frmtCurrency = ''
            frmtCurrency = input.toLocaleString('en-IN', {maximumFractionDigits: 2})
            frmtCurrency = frmtCurrency.indexOf('.') !== -1 ? frmtCurrency : frmtCurrency + '.00'
            return frmtCurrency
        }else{
            return '0.0'
        }
    }
});*/

app.filter('inrCurrencyFormat', function () {
    return function (input, addSymbol) {
        if (angular.isDefined(input) && input != '') {
            var frmtCurrency = ''
            frmtCurrency = input.toLocaleString('en-IN', {maximumFractionDigits: 2})
            frmtCurrency = frmtCurrency.indexOf('.') !== -1 ? frmtCurrency : frmtCurrency + '.00'
            return frmtCurrency
        } else {
            return '0.0'
        }
    }
});

app.filter('jobStatus', function ($sce, $filter) {
    return function (input) {
        var retValue = '';
        if (angular.isDefined(input)) {
            if (input == 'SUCCESS') {

                retValue += '<span class="label label-success">' + input + '</span>'
            }
            else if (input == 'FAILURE') {

                retValue += '<span class="label label-danger">' + input + '</span>'
            }
            else {
                retValue += '<span class="label label-info">' + input + '</span>'
            }
        }
        return $sce.trustAsHtml(retValue);
    }
});

app.filter('sumOfValue', function () {
   return function (data, key) {
       if (angular.isUndefined(data) && angular.isUndefined(key))
           return 0;
       var sum = 0;
       angular.forEach(data,function(value){
           sum = sum + value[key];
       });
       return sum;
   }
})


