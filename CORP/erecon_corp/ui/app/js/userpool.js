function userpoolController($scope, $rootScope,$http, $routeParams, $location, $timeout, $route, $window, $modal, UsersService, UserPoolService, BusinessService) {
//        if($rootScope.credentials.role != 'admin'){
//            $rootScope.msgNotify('error','Access restricted to view schedules.');
//            $location.path("/migratedSystems");
//            return false;
//        }
    $scope.userpoolOptions = {};
    $scope.userpoolOptions.init = false;
    $scope.userpoolOptions.isLoading = true;
    $scope.userpoolOptions.isTableSorting = "name";
    $scope.userpoolOptions.sortField = "name";
    //      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    console.log($rootScope.credentials)
    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.userpoolOptions.searchFields = {}
        $scope.userpoolOptions.service = UserPoolService;
        $scope.userpoolOptions.headers = [
            [
                {
                    name: "",
                    dataField: "SelectedUserPool",
                    checkbox: true,
                    selectAll: false,
                    colIndex: 0,
                    datawidth: "5%"
                },
                {name: "Name", searchable: true, sortable: true, dataField: "name", colIndex: 0, datawidth: "5%"},
                {name: "Role", searchable: true, sortable: true, dataField: "role", colIndex: 0, datawidth: "5%"},
                {name: "Users", searchable: true, sortable: true, dataField: "users", colIndex: 2, datawidth: "40%"},
                {
                    name: "Business Contexts",
                    searchable: true,
                    sortable: true,
                    dataField: "business_context",
                    colIndex: 2,
                    datawidth: "40%"
                },
                //                {name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
                {
                    name: "Actions",
                    colIndex: 4,
                    datawidth: "5%",
                    actions: [{
                        toolTip: "Edit",
                        action: "addUser",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.userpoolOptions.disableEdit = function (data, index, action) {
            //if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
            //    return true;
            //}
            //return false;
        }


        $scope.userpoolOptions.hideButton = function (data, index, action) {
            //if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
            //    return true;
            //}
            //return false;
        }
        $scope.userpoolOptions.reQuery = true;
        //          $scope.init();
        //          console.log($scope.userpoolOptions)
        //            $scope.init=function(){
        UserPoolService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.userpoolOptions.checkRole = function (data, index, action) {
        if (data.role == "admin") {
            return false;
        }
        return true;
    }
    $scope.business_Data = []
    $scope.arraycheck = []
    $scope.userData = {}
    $scope.selectedUserPoolId = [];
    $scope.approveDelete = true;
    BusinessService.get(undefined, undefined, function (data) {
        console.log(data)
        $rootScope.openModalPopupClose()
        angular.forEach(data.data, function (val, key) {
            if ($scope.arraycheck.indexOf(val.WORKPOOL_NAME) == -1) {
                $scope.arraycheck.push(val.WORKPOOL_NAME)
                $scope.business_Data.push({
                    'name': val.WORKPOOL_NAME,
                    'id': val.BUSINESS_CONTEXT_ID,
                    'ticked': false
                })
            }
        })
    })
    $scope.tagData = []
    UsersService.get(undefined, undefined, function (data) {
        console.log(data)
        angular.forEach(data.data, function (val, key) {
            $scope.tagData.push({'name': val.userName, 'ticked': false})
        })
    })

    $scope.userpoolOptions.addUser = function (selUserData) {
        $rootScope.openModalPopupOpen()
        $modal.open({
            templateUrl: 'addUserPool.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                $rootScope.isDisable = false;
                if (angular.isDefined(selUserData)) {
                    $rootScope.openModalPopupOpen()
                    $scope.userData = selUserData
                    console.log($scope.userData)
                    $scope.users_data = $scope.userData['users'].split(',')
                    $scope.mode = "Edit";
                    $scope.businessData = $scope.userData.business_context.split(',')
                    angular.forEach($scope.businessData, function (v, k) {
                        angular.forEach($scope.business_Data, function (val, key) {
                            if (v == val.id) {
                                $scope.business_Data[key] = {'name': val.name, 'id': val.id, 'ticked': true}
                            }
                        })
                    })

                    $timeout(function () {
                        $rootScope.openModalPopupClose()
                    }, 5000)
                    console.log($scope.userData)
                }
                else {
                    $scope.mode = "Add";
                    $scope.userData = {};
                    $scope.userData["role"] = "";
                }

                $scope.saveUserpool = function (userData) {
                    console.log(userData)
                    $rootScope.isDisable = true;
                    if (angular.isDefined($scope.userData._id)) {
                        console.log(userData)
                        $scope.business_context_data = []
                        // $scope.users_data = []
                        if (typeof userData['business_context'] != "string") {
                            angular.forEach(userData.business_context, function (va, ke) {
                                console.log(va)
                                $scope.business_context_data.push(va.id)
                            })
                        }
                        if (typeof userData['business_context'] != "string") {
                            userData['business_context'] = $scope.business_context_data.join(',')
                        }
                        userData['users'] = $scope.users_data.join(',')
                        console.log(userData)
                        $rootScope.openModalPopupOpen();
                        UserPoolService.put(userData["_id"], $scope.userData, function (data) {

                            $rootScope.msgNotify('success', 'UserPool Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.userpoolOptions.reQuery = true;
                            $timeout(function () {
                                $scope.userData = {};
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                    else {
                        $scope.business_context_data = []
                        $scope.users_data = []
                        if (typeof userData['business_context'] != "string") {
                            angular.forEach(userData.business_context, function (va, ke) {
                                console.log(va)
                                $scope.business_context_data.push(va.id)
                            })
                        }
                        if (typeof userData.users != "string") {
                            angular.forEach(userData.users, function (v, k) {
                                console.log(v)
                                $scope.users_data.push(v.name)
                            })
                        }

                        if (typeof userData['business_context'] != "string") {
                            $scope.userData['business_context'] = $scope.business_context_data.join(',')
                        }
                        if (typeof userData['users'] != "string") {
                            $scope.userData['users'] = $scope.users_data.join(',')
                        }
                        $rootScope.openModalPopupOpen();
                        UserPoolService.post($scope.userData, function (data) {

                            $rootScope.msgNotify('success', 'UserPool Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.userpoolOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }


                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.userpoolOptions.reQuery = true;
                };
            }
        });


    };
    $scope.confirmDelete = angular.noop();
    $scope.addUsers = function () {
        //$scope.selectedUserpools = []
        //angular.forEach($scope.selectedUserPoolId, function (val,key) {
        //    UserPoolService.get(val,undefined, function (data) {
        //        console.log(data)
        //        $scope.selectedUserpools.push(data['name'])
        //    })
        //})
        $modal.open({
            templateUrl: 'addUsers.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                $scope.mode = "Add"
                $scope.tagData = []
                UsersService.get(undefined, undefined, function (data) {
                    //console.log(data)
                    angular.forEach(data.data, function (val, key) {
                        $scope.tagData.push({'name': val.userName, 'id': val._id, 'ticked': false})
                    })
                })
                $scope.saveUsers = function (userData) {
                    console.log(userData)
                    var dataU = {}
                    dataU['selectedUserPool_ids'] = $scope.selectedUserPoolId
                    dataU['selectedUsers'] = userData['users']
                    UserPoolService.addUsers('dummy', dataU, function (data) {
                        console.log(data)
                    })
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.approveDelete = true
                    $scope.userpoolOptions.reQuery = true;
                };
            }
        });
    }
    $scope.userpoolOptions.Change = function (data) {
        if ($scope.userpoolOptions.selectedAll) {
            $scope.userpoolOptions.selectedAll = false;
            $scope.selectedUserPoolId = [];
            angular.forEach($scope.userpoolOptions.datamodel.data, function (v, k) {
                if (v.SelectedUserPool && data._id != v._id) {
                    $scope.selectedUserPoolId.push(v._id);
                }
            });
        }
        else {
            angular.forEach($scope.userpoolOptions.datamodel.data, function (v, k) {
                if (data._id == v._id) {
                    if (angular.isDefined(v.SelectedUserPool)) {
                        if ($scope.selectedUserPoolId.indexOf(data._id) > -1) {
                            v['SelectedUserPool'] = false;
                        }
                        else {
                            v['SelectedUserPool'] = true;
                        }
                    }
                    else {
                        v['SelectedUserPool'] = true;
                    }
                    if (v.SelectedUserPool && $scope.selectedUserPoolId.indexOf(data._id) == -1) {
                        $scope.selectedUserPoolId.push(v._id);
                    }
                    else {
                        $scope.selectedUserPoolId.splice($scope.selectedUserPoolId.indexOf(data._id), 1);
                    }
                    console.log($scope.selectedUserPoolId);
                }
            });
            $scope.selectedUserPoolId.length == 0 ? $scope.approveDelete = true : $scope.approveDelete = false;
            //$scope.selectedUserPoolId.length == 1 ? $scope.viewButton = false : $scope.viewButton = true;
            $scope.selectedUserPoolId.length == $scope.userpoolOptions.datamodel.data.length ? $scope.userpoolOptions.selectedAll = true : $scope.userpoolOptions.selectedAll = false;
        }


        console.log($scope.selectedUserPoolId);
    };
    $scope.userpoolOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    UserPoolService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.userpoolOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    //      }
    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }
    $scope.getUsers();
    $scope.usersType = ["Administrator", "Operations Manager", "Reconciler", "Dept User", "Investigator", "Configurer", "Enquirer", "Authorizer"]

}
