/**
 * Created by swami on 13/4/15.
 */
function usersController($scope, $rootScope, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, UsersService, BusinessService) {
//    if($rootScope.credentials.role != 'admin'){
//        $rootScope.msgNotify('error','Access restricted to view users.');
//        $location.path("/migratedSystems");
//        return false;
//    }
//    console.log($rootScope.businessData)
//    angular.forEach($rootScope.businessData,function(v,k){
//        AccountPoolService.post(v,function(data){
//            console.log(data)
//        })
//    })
    $scope.usersOptions = {};
    $scope.usersOptions.init = false;
    $scope.usersOptions.isLoading = true;
    $scope.usersOptions.isTableSorting = "userName";
    $scope.usersOptions.sortField = "userName";
//      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.usersOptions.searchFields = {}
        $scope.usersOptions.service = UsersService;
        $scope.usersOptions.headers = [
            [
                {name: "Name", searchable: true, sortable: true, dataField: "userName", colIndex: 0},
                {name: "Role", searchable: true, sortable: true, dataField: "role", filter: "toUser", colIndex: 1},
                {name: "Email", searchable: true, sortable: true, dataField: "email", colIndex: 2},
                {
                    name: "Business Context",
                    searchable: true,
                    sortable: true,
                    dataField: "businessContextIds",
                    colIndex: 3
                },
//                {name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
                {
                    name: "Actions",
                    colIndex: 4,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addUser",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }, {
                            toolTip: "Unlock",
                            action: "Unlock",
                            posticon: "fa fa-unlock-alt fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.usersOptions.disableEdit = function (data, index, action) {
//            if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                return true;
//            }
//            return false;
        }


        $scope.usersOptions.hideButton = function (data, index, action) {
//            if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                return true;
//            }
//            return false;
        }
        $scope.usersOptions.reQuery = true;
//          $scope.init();
//          console.log($scope.usersOptions)
//            $scope.init=function(){
        UsersService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.usersOptions.checkRole = function (data, index, action) {
        if (data.role == "Super Admin") {
            return false;
        }
        return true;
    }
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

    $scope.showMsg = true
    $scope.analyze = function (value) {
        if (strongRegex.test(value)) {
            $scope.showMsg = true
        } else if (mediumRegex.test(value)) {
            $scope.showMsg = false
        } else {
            $scope.showMsg = false
        }
    };
    $scope.usersOptions.addUser = function (userData) {
        $modal.open({
            templateUrl: 'addUser.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                if ($rootScope.credentials.role == 'Super Admin') {
                    $scope.usersType = ["admin", "Administrator", "Operations Manager", "Reconciler", "Dept User", "Investigator", "Configurer", "Enquirer", "Authorizer", "Recon Manager", "Recon Executor", "Recon Assignment"]
                }
                $scope.mode = "Add";
                $scope.userData = {};
                $scope.userData.role = ""
                $scope.tmpData = {};
//                $scope.userData["role"] = "customer";
                console.log(userData)
                if (angular.isDefined(userData)) {
                    $scope.mode = "Edit";
                    $scope.userData = userData;
                    $rootScope.availableQuotaForReseller = $rootScope.availableQuotaForReseller + userData.quota;
                    if (userData.role == "reseller") {
                    }
                    if ($rootScope.credentials.role == "Super Admin") {
                        angular.forEach($scope.business_Data, function (k, v) {
                            k['ticked'] = userData['businessContextIds'].indexOf(k['id']) >= 0 ? true : false
                        })
                    }
                }
                console.log(userData)
                $rootScope.isDisable = false;
                $scope.saveUser = function () {
                    $rootScope.isDisable = true;
                    if ($rootScope.credentials.role == 'Super Admin' && angular.isDefined($scope.userData['bName'])) {
                        $scope.userData['businessContextIds'] = []
                        angular.forEach($scope.userData['bName'], function (k, v) {
                            $scope.userData['businessContextIds'].push(k['id'])
                        })
                        delete $scope.userData['bName']
                    } else if ($rootScope.credentials.role == 'admin' && angular.isDefined($rootScope.credentials['businessContextIds'])) {
                        $scope.userData['businessContextIds'] = $rootScope.credentials['businessContextIds']
                        delete $scope.userData['bName']
                    }

                    if (angular.isDefined(userData)) {
                        $rootScope.openModalPopupOpen();
                        $scope.userData['userName'] = $scope.userData['userName'].toLowerCase()
                        UsersService.put(userData["_id"], $scope.userData, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.usersOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    } else {
                        $rootScope.openModalPopupOpen();
                        $scope.userData['userName'] = $scope.userData['userName'].toLowerCase()
                        console.log($scope.userData)
                        $scope.todayDate = Math.floor((new Date().getTime()) / 1000)
                        $scope.userData['lastPwdUpdt'] = $scope.todayDate
                        $scope.userData['approvalAction'] = 'create'
                        $scope.userData['isApproved'] = true
                        //if ($scope.userData.role == "Administrator" || $scope.userData.role == "admin" || $scope.userData.role == "Approver")
                        //   $scope.userData['isApproved'] = true
                        $scope.userData['createdBy'] = $rootScope.credentials.userName
                        $scope.userData['operator'] = $rootScope.credentials.userName
                        UsersService.post($scope.userData, function (data) {
                            $rootScope.msgNotify('success', 'Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.usersOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        }, function (err) {
                            $rootScope.msgNotify('warning', err.msg);

                        });
                    }
                };
                $scope.updatePassword = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.password)) {
                        if ($scope.userData.password == $scope.tmpData.pw2) {
                            $rootScope.openModalPopupOpen();
                            console.log($scope.userData)
                            if (angular.isDefined($scope.userData['lastPwdUpdt'])) {
                                $scope.userData['lastPwdUpdt'] = Math.floor((new Date().getTime()) / 1000)
                                $scope.userData['password'] = $scope.userData.password
                            } else {
                                console.log('no')
                                $scope.todayDate = Math.floor((new Date().getTime()) / 1000)
                                $scope.userData['lastPwdUpdt'] = $scope.todayDate
                                $scope.userData['password'] = $scope.userData.password
                                console.log($scope.userData['lastPwdUpdt'])
                                console.log($scope.userData)
                            }

                            UsersService.changePassword(userData["_id"], $scope.userData.password, function (data) {
                                $rootScope.msgNotify('success', 'Successfully updated');
                                $rootScope.openModalPopupClose();
                                $timeout(function () {
                                    $scope.cancel();
                                }, 3000);
                            }, function (err) {
                                $rootScope.isDisable = false;
                                $rootScope.msgNotify('warning', err.msg);
                                $rootScope.openModalPopupClose();
                            });
                        } else {
                            $rootScope.handleErrorMessages({'msg': 'Password mismatch.'})
                        }
                    }
                };
                $scope.updateQuota = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.quota)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.setQuota(userData["_id"], $scope.userData.quota, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.updateEmail = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.email)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.setEmail(userData["_id"], $scope.userData.email, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                    if (angular.isDefined(userData) && angular.isDefined(userData.fromEmail)) {
                        UsersService.setFromEmail(userData["_id"], $scope.userData.fromEmail, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }

                };
                $scope.updateBzCtxIds = function () {
                    $scope.userData['businessContextIds'] = []
                    angular.forEach($scope.userData['bName'], function (k, v) {
                        $scope.userData['businessContextIds'].push(k['id'])
                    })
                    console.log($scope.userData)
                    if (angular.isDefined($scope.userData) && angular.isDefined($scope.userData.businessContextIds)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.put($scope.userData["_id"], $scope.userData, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }


                }
                $scope.updateStatus = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.isActive)) {
                        userData.isActive = !userData.isActive;
                        $rootScope.openModalPopupOpen();
                        UsersService.updateStatus(userData["_id"], userData.isActive, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.usersOptions.reQuery = true;
                };
            }
        });
    };
    $scope.confirmDelete = angular.noop();

    $scope.usersOptions.Unlock = function (data) {
        $modal.open({
            templateUrl: 'UnlockUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    data['accountLocked'] = false
                    data['loginCount'] = 0
                    delete data['password']
                    UsersService.put(data["_id"], data, function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.usersOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.usersOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    UsersService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.usersOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }
    BusinessService.get(undefined, undefined, function (data) {
        $scope.arraycheck = []
        $scope.business_Data = []

        angular.forEach(data.data, function (val, key) {
            if ($scope.arraycheck.indexOf(val.WORKPOOL_NAME) == -1) {
                $scope.arraycheck.push(val.WORKPOOL_NAME)
                $scope.business_Data.push({
                    'name': val.WORKPOOL_NAME,
                    'id': val.BUSINESS_CONTEXT_ID,
                    'ticked': false
                })
            }
        })
    })
    //      }
    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }
    $scope.getUsers();
    $scope.usersType = ["Administrator", "Operations Manager", "Reconciler", "Dept User", "Investigator", "Configurer", "Enquirer", "Authorizer", "Recon Manager", "Recon Executor", "Recon Assignment"]
    console.log($rootScope.credentials.role)
    if ($rootScope.credentials.role == 'Super Admin') {
        $scope.usersType = ["admin", "Administrator", "Operations Manager", "Reconciler", "Dept User", "Investigator", "Configurer", "Enquirer", "Authorizer", "Recon Manager", "Recon Executor", "Recon Assignment"]
    }

};
