/**
 * Created by ubuntu on 24/11/16.
 */


function reconsMigrationController($scope, $rootScope, $interval, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, ReconLicenseService, BusinessService, JobStatusService) {
    $scope.initImportFunc = function () {
        $rootScope.openModalPopupOpen()
        ReconLicenseService.getMigrationList('dummy', {'search': 'importRecon'}, function (data) {
            $rootScope.openModalPopupClose()
            console.log(Object.keys(data))
            // var files = []
            $scope.reconNames = []
            $scope.reconToMigrationDetails = {}
            if (Object.keys(data).length != 0) {

                $scope.reconToMigrationDetails = data
                $scope.reconNames = Object.keys(data)
            }
            else {
                $rootScope.showAlertMsg('No Recons to migrate')
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }
    $scope.initRollBackFunc = function () {
        $rootScope.openModalPopupOpen()
        ReconLicenseService.getMigrationList('dummy', {'search': 'rollBackRecon'}, function (data) {
            // console.log(Object.keys(data))
            $rootScope.openModalPopupClose()
            $scope.reconRollBackNames = []
            $scope.reconToMigrationDetails = {}
            if (Object.keys(data).length != 0) {


                $scope.reconToMigrationDetails = data
                $scope.reconRollBackNames = Object.keys(data)
            }
            else {
                $rootScope.showAlertMsg('No Recons to migrate')
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }
    $scope.initImportFunc()
    // $scope.initRollBackFunc()

    $scope.importReconDetails = function (selectedRecon) {
        console.log(selectedRecon)
        $scope.reconID = selectedRecon
        $scope.selectedMigrationTs = $scope.reconToMigrationDetails[selectedRecon]['fileTS']
        $scope.selectedMigrationTs.sort(function (a, b) {
            console.log(a, b)
        });
        console.log($scope.selectedMigrationTs)
        $modal.open({
            templateUrl: 'importRecon.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, ReconLicenseService) {
                $scope.selectTs = function (selectTs) {
                    $scope.fileMigrationTs = selectTs
                }


                $scope.importRecon = function () {
                    queryObj = {
                        'reconId': selectedRecon,
                        'fileName': selectedRecon + '_' + $scope.fileMigrationTs + '.tar.gz',
                        'exportFileName': selectedRecon + '_' + $scope.fileMigrationTs + '.tar.gz'
                    }
                    $rootScope.openModalPopupOpen()
                    ReconLicenseService.importReconDetails('dummy', queryObj, function (data) {
				console.log(data)
                        $rootScope.showAlertMsg(data)
                        $rootScope.openModalPopupClose()
                        $scope.initImportFunc()
                        $scope.cancel()
                    }, function (err) {
                        $rootScope.showAlertMsg(err.msg)
                    })

                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel')
                }
            }
        })


    }

    $scope.rollBackReconDetails = function (selectedRecon) {
        console.log(selectedRecon)
        $scope.rollBkStatus = false;
        $scope.reconID = selectedRecon
        $scope.selectedMigrationTs = $scope.reconToMigrationDetails[selectedRecon]['fileTS']
        $scope.selectedMigrationTs.sort(function (a, b) {
            return (a[a.length - 1] - b[b.length - 1])
        });
        $scope.fileMigrationTs = $scope.selectedMigrationTs[0]
        console.log($scope.selectedMigrationTs)
        $modal.open({
            templateUrl: 'rollBackRecon.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, ReconLicenseService) {
                $scope.selectTs = function (selectTs) {
                    $scope.fileMigrationTs = selectTs
                }
                $scope.selectCheckTs = function (selectTs) {
                    $scope.rollBkStatus = selectTs
                }

                $scope.confirmRollback = function () {
                    $modal.open({
                        templateUrl: 'confirmRollback.html',
                        windowClass: 'modal addUser',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        controller: function ($scope, $modalInstance, ReconLicenseService) {

                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel')
                            }
                            $scope.rollBackRecon = function () {
                                console.log($scope.rollBkStatus)
                                queryObj = {
                                    'reconId': selectedRecon,
                                    'fileName': selectedRecon + '_' + $scope.fileMigrationTs + '.tar.gz',
                                    'exportFileName': selectedRecon + '_' + $scope.fileMigrationTs + '.tar.gz',
                                    'rollBkStatus': $scope.rollBkStatus
                                }
                                $rootScope.openModalPopupOpen()
                                if ($scope.rollBkStatus && angular.isDefined($scope.fileMigrationTs) && $scope.fileMigrationTs != '') {
                                    ReconLicenseService.rollBackReconDetails('dummy', queryObj, function (data) {
                                        $rootScope.openModalPopupClose()
                                        $scope.initRollBackFunc()
                                        console.log($scope.rollBkStatus)
                                        // console.log(data)
                                        $scope.cancel()
                                    }, function (err) {
                                        $rootScope.showAlertMsg(err.msg)
                                    })
                                } else if ($scope.rollBkStatus != true) {
                                    ReconLicenseService.rollBackReconDetails('dummy', queryObj, function (data) {
                                        $rootScope.openModalPopupClose()
                                        $scope.initRollBackFunc()
                                        console.log($scope.rollBkStatus)
                                        // console.log(data)
                                        $scope.cancel()
                                        $scope.cancel()
                                    }, function (err) {
                                        $rootScope.showAlertMsg(err.msg)
                                    })
                                }

                            }
                        }
                    })
                    $scope.cancel()

                }


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel')
                }
            }
        })


    }

}