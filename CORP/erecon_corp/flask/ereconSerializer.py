# *************************************************************************
#
# ERECON CONFIDENTIAL
# ________________________
#
#  [2016] - [2020] HashInclude Computech Private Limited
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of HashInclude Computech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to HashInclude Computech
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from HashInclude Computech Private Limited.
# *************************************************************************
from logger import Logger

logger = Logger.getInstance("application").getLogger()

from redis import Redis
from rq import Queue
import ereconWorker
import time
import traceback
import sys
from Hdf5ZfsUtil import Hdf5ZfsUtil


class ReconWorker:
    def __init__(self,reconId):
        self.zfsQueue = Queue(name=reconId,connection=Redis())
        print reconId
        print self.zfsQueue.get_job_ids()
    def submitJob(self, functionName, *arg):
        recon_worker = ereconWorker.workerThread()
        try:
            # recon_worker.createHDFSnapShot(self.recon_id, self.uuid)
            job = self.zfsQueue.enqueue(eval("recon_worker." + functionName), *arg)
            return job.id
        except Exception, e:
            return None

    def getJobStatus(self, jobId):
        job = self.zfsQueue.fetch_job(jobId)
        if job:
            return job.get_status()
        return ""

    def getJob(self, jobId):
        return self.zfsQueue.fetch_job(jobId)

    def completeJob(self, jobId):
        job = self.getJob(jobId)
        job.delete()

    def runJob(self, jobId):
        job = self.getJob(jobId)
        status = job.perform()
        return status, job.result

    def getLastJob(self):
        jobs = self.zfsQueue.get_job_ids()
        if len(jobs):
            return jobs[0]
        return ""


if __name__ == "__main__":
    reconWorker = ReconWorker(sys.argv[1])
    while (True):
        jobId = reconWorker.getLastJob()
        ReconWorker(sys.argv[1]).getJob(jobId)
        if not len(jobId):
            time.sleep(3)
            continue
        try:
            # create current hdf snap shot as backup & restore on error
            snapSht_status = Hdf5ZfsUtil().createHDFSnapShot(jobId)
            if snapSht_status:
                result = reconWorker.runJob(jobId)
                Hdf5ZfsUtil().updateJobStatus(job_id=jobId)
            else:
                Hdf5ZfsUtil().updateJobStatus(job_id=jobId, job_status='FAILURE',
                                       error_msg='Error creating job restore snapshot.',
                                       restore_snapshot=False)
        except Exception, e:
            logger.info('Job_ID %s failed.' % jobId)
            logger.info(traceback.format_exc())
            Hdf5ZfsUtil().updateJobStatus(job_id=jobId, job_status='FAILURE',
                                                        error_msg='Internal Error, Contact Admin')
            raise
        finally:
            logger.info('Job %s removed from queue successfully' % jobId)
            reconWorker.completeJob(jobId)

        time.sleep(1)
