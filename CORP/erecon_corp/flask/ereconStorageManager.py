import os
import sys
import time
import redis
import subprocess
from redis import Redis
from copy import deepcopy


diskSupportTypes = "scsi\|ata\|cciss\|virtio"

storageNodes = ["admin"]

poolNodeAttributes = ["autoexpand=on"]
storageNodeAttributes = ["recordsize=64K", "compression=on", "xattr=sa", "atime=off"]


def systemCommand(command, waitForCompletion=True):
    child = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    if waitForCompletion:
        out, err = child.communicate()
        status = child.returncode
        return status, out, err
    else:
        return child


def get_disk_size(diskname):
    # "Get the file size by seeking at end"
    if not os.path.exists(diskname):
        return 0
    fd = os.open(diskname, os.O_RDONLY | os.O_LARGEFILE)
    size = 0
    try:
        size = os.lseek(fd, 0, os.SEEK_END)
    except:
        pass
    os.close(fd)
    return size


def checkIsMountPoint(path):
    (status, out, err) = systemCommand('mountpoint "%s"' % path)
    if status and ('input/output' in err.lower()):
        return True
    pathExists = os.path.exists(path)
    if not pathExists:
        return False
    elif pathExists and status:
        return False
    return True


class storageManager():
    @classmethod
    def getPoolDiskInfo(cls, diskMap):
        diskMap = os.path.basename(diskMap)
        (status, out, err) = systemCommand("zpool status | grep %s" % diskMap)
        if not status:
            data = out.strip().split()
            if len(data) > 2:
                return data[1].strip()
        return "OFFLINE"

    @classmethod
    def getPoolStatus(cls):
        try:
            errorStates = ["DEGRADED", "FAULTED", "OFFLINE", "UNAVAIL", "REMOVED", "FAIL", "DESTROYED", "corrupt",
                           "cannot", "unrecover"]
            (status, out, err) = systemCommand("zpool status erecon")
            if status:
                return False, err
            else:
                data = out.split("\n")
            state = ""
            for line in data:
                if line.strip().startswith("state:"):
                    state = line.strip().split("state:")[1].strip()
                    break
            if state.lower() in errorStates or state.upper() in errorStates:
                return False, state
            else:
                return True, state
        except Exception, e:
            print("Exception getting pool Status: %s" % e.message)
            return False, e.message

    @classmethod
    def getDiskStatus(cls, diskMap):
        diskMap = os.path.basename(diskMap)
        status, msg = storageManager.getPoolStatus()
        if not status:
            return "UNAVILABLE"
        (status, out, err) = systemCommand("zpool status | grep %s" % diskMap)
        if status:
            return "UNAVILABLE"
        data = out.strip().split()
        if diskMap in data:
            if len(data) > 3:
                return data[1]
        return "UNAVILABLE"

    @classmethod
    def getOsDisk(cls):
        (status, out, err) = systemCommand("df -h / | tail -n+2")
        if not status:
            osDisk = out.strip().split()[0]
            # if "cciss" in out:
            #    return True, os.path.realpath(osDisk)[:-2]
            return True, os.path.realpath(osDisk)[:-1]
        else:
            return False, ""

    @classmethod
    def getDiskId(cls, disk):
        return disk
        # (status, out, err) = systemCommand("ls /dev/disk/by-id | grep \"%s\"" % diskSupportTypes)
        # if not status:
        #     data = out.strip().split()
        #     for k in data:
        #         if os.path.realpath("/dev/disk/by-id/%s" % k) == disk:
        #             return "/dev/disk/by-id/%s" % k
        # else:
        #     return ""

    @classmethod
    def getMountedDisks(cls):
        usedDisks = []
        status, osDisk = storageManager.getOsDisk()
        usedDisks.append(osDisk)
        (status, out, err) = systemCommand("mount | egrep -v 'erecon|cgroup|proc|udev|sysfs|none|devpts|tmpfs|configfs")
        if status:
            return usedDisks

        for data in out.strip().split("\n"):
            mountInfo = data.strip().split()
            disk = ''.join([i for i in mountInfo[0] if not i.isdigit()])
            if disk not in usedDisks:
                usedDisks.append(disk)
        return usedDisks

    @classmethod
    def getStorageDisks(cls):
        r = Redis()
        storageDisks = []
        cryptDisks = r.lrange("cryptDisks", 0, -1)
        if not cryptDisks:
            storageDisks = []
        else:
            for disk in cryptDisks:
                storageDisks.append(os.path.realpath(disk))
        return storageDisks

    @classmethod
    def getStorageEncryptedDisks(cls):
        r = Redis()
        storageDisks = []
        cryptDisks = r.lrange("encryptedPaths", 0, -1)
        if not cryptDisks:
            storageDisks = []
        else:
            for disk in cryptDisks:
                storageDisks.append(os.path.realpath(disk))
        return storageDisks

    @classmethod
    def getRaidConfigration(cls):
        r = redis.Redis()
        data = {"raidlevel": r.get("raidlevel"), "disksetsize": r.get("raidsetCount")}
        try:
            if int(data["raidlevel"]) == 0:
                data["disksetsize"] = 1
        except Exception, e:
            pass
        return data

    @classmethod
    def loadereconStorage(cls, cryptDisks=None, recovery=False):
        # r = redis.Redis()
        # cryptPass = r.get('cryptPass')
        # if not cryptPass or len(cryptPass) == 0:
        #     cryptPass = "8b836efd35e04f798381e85cf5afd03e"
        # if not cryptDisks:
        #     disks = r.lrange("cryptDisks", 0, -1)
        # else:
        #     disks = cryptDisks
        # for disk in disks:
        #     if not os.path.exists(os.path.join("/dev/mapper", os.path.basename(disk))):
        #         (status, output, err) = systemCommand(
        #             "echo %s | cryptsetup luksOpen %s %s" % (cryptPass, disk, os.path.basename(disk)))
        #         if status:
        #             print "loadFileSystem failed for disk: %s with error: %d %s" % (disk, status, err)
        if os.path.exists("/erecon") and not checkIsMountPoint("/erecon"):
            systemCommand("rm -rf /erecon")
        time.sleep(10)

        if recovery:
            (status, out, err) = systemCommand("zpool import | grep erecon")
            if status or err.strip() != "":
                return False, err
            if status or out.strip() == "":
                return False, "No erecon Pools Available For Import"
            (status, out, err) = systemCommand("zpool import -f erecon")
            if status or err.strip() != "":
                return False, err

        (status, out, err) = systemCommand("zfs mount -a")
        if cryptDisks and len(cryptDisks):
            if not status and checkIsMountPoint('/erecon'):
                return True, "Success"

        # if checkIsMountPoint('/erecon') and r.get('expandStorage') and int(r.get('expandStorage')) == 1:
        #     time.sleep(5)
        #     (status, out, error) = systemCommand("zpool scrub erecon")
        #     print "Expand scrub: ", status, out, error
        #     time.sleep(2)
        #     (status, out, error) = systemCommand("zpool export erecon")
        #     print "Expand export erecon: ", status, out, error
        #     time.sleep(1)
        #     (status, out, error) = systemCommand("zpool import erecon")
        #     print "Expand import erecon: ", status, out, error
        #     r.delete('expandPool')
        #     r.save()
        #     time.sleep(2)
        #     print 'loadFileSystem expanded pool'
        return True, "Success"

    # @classmethod
    # def intializeDisk(cls, diskId):
    #     if os.path.exists(os.path.join("/dev/mapper", os.path.basename(diskId))):
    #         return None
    #     r = redis.Redis()
    #     cryptPass = r.get('cryptPass')
    #     if not cryptPass or len(cryptPass) == 0:
    #         cryptPass = "8b836efd35e04f798381e85cf5afd03e"
    #     (status, output, err) = systemCommand("echo %s | cryptsetup luksFormat %s" % (cryptPass, diskId))  # Password generated with os.urandom(16).encode('hex')
    #     if status:
    #         return None
    #     (status, output, err) = systemCommand(
    #         "echo %s | cryptsetup luksOpen %s %s" % (cryptPass, diskId, os.path.basename(diskId)))
    #     if status:
    #         return None
    #     return "/dev/mapper/" + os.path.basename(diskId)

    @classmethod
    def getEncryptedDisks(cls, disks, raid_level, diskById=False):
        print disks
        r = Redis()
        if raid_level > 0:
            if len(disks) < raid_level - 2:
                print "Need minimum %s disks for this raid level" % (raid_level - 2), " "
                return False, "Need minimum %s disks for this raid level" % (raid_level - 2), " "
        diskMap = list()
        for disk in disks:
            if diskById:
                diskById = storageManager.getDiskId(disk)
                if diskById and len(diskById) <= 0:
                    return False, 'Unable to found mapped drive...', ""
                diskMap.append(diskById)
            else:
                diskMap.append(disk)

        encryptedDisks = []
        # for disk in diskMap:
        #     cryptPath = storageManager.intializeDisk(disk)
        #     if cryptPath:
        #         encryptedDisks.append(cryptPath)
        #         r.rpush("cryptDisks", disk)
        #         r.rpush("encryptedPaths", cryptPath)
        # r.save()

        zfsRaidLevel = ""
        if raid_level == 5:
            zfsRaidLevel = "raidz"
        elif raid_level == 6:
            zfsRaidLevel = "raidz2"
        #return True, encryptedDisks, zfsRaidLevel
        return True, diskMap, zfsRaidLevel

    @classmethod
    def poolCreated(cls):
        (status, out, err) = systemCommand("zpool list erecon")
        if not status:
            return True
        return False

    @classmethod
    def createEreconStorage(cls, disks, raid_level=5, compressionType="gzip-4", diskById=False):
        r = redis.Redis()
        if storageManager.poolCreated():
            return False, "Storage Already Created..."

        if os.path.exists("/erecon"):
            (status, out, err) = systemCommand("mount | grep /erecon")
            if not status:
                return False, "Unable to create storage, has old bindings please restart appliance and try"
            systemCommand("rm -rf /erecon")

        status, encryptedDisks, zfsRaidLevel = storageManager.getEncryptedDisks(disks, raid_level, diskById=diskById)
        print status, encryptedDisks, zfsRaidLevel
        if not status:
            return status, encryptedDisks
        r.set("raidlevel", raid_level)
        r.set("raidsetCount", len(encryptedDisks))
        r.save()

        (status, output, err) = systemCommand("zpool create -o ashift=12 erecon %s %s" % (zfsRaidLevel, " ".join(encryptedDisks)))
        print "Create erecon: %s %s %s " % (status, output, err)
        if status:
            return False, err

        time.sleep(2)

        fissionNodeAttributes = storageNodeAttributes
        fissionNodeAttributes.append("compression=%s" % compressionType.lower())

        for fissionAttribute in fissionNodeAttributes:
            (status, output, err) = systemCommand("zfs set %s erecon" % fissionAttribute)
            if status:
                print "Error in setting attribute: %s " % fissionAttribute
                print "ERROR: %s %s %s" % (status, output, err)
                return False, err

        r.set("compressionAlgorithm", compressionType.lower().strip())
        r.save()

        for nodeattr in poolNodeAttributes:
            (status, output, err) = systemCommand("zpool set %s erecon" % nodeattr)
            if status:
                print "Error in setting attribute: %s " % nodeattr
                print "ERROR: %s %s %s" % (status, output, err)
                return False, err

        for fissionNode in storageNodes:
            (status, output, err) = systemCommand("zfs create erecon/%s" % fissionNode)
            if status:
                print("Error in create node: %s with error: %s" % (fissionNode, err))
                print "Error in create node: %s " % fissionNode
                return False, err

        return True, "Success"

    @classmethod
    def expandStorage(cls, newDiskList, diskById=True):
        (status, data) = storageManager.getPoolStatus()
        if status is False:
            print "Storage not yet up,storage expansion  not allowed"
            return False, "Storage not yet up,storage expansion  not allowed"
        r = Redis()
        raid_level = int(r.get("raidlevel"))
        #TODO:- Need to change redis variable names
        raidsetCount = int(r.get("raidsetCount"))
        status, encryptedDisks, zfsRaidLevel = storageManager.getEncryptedDisks(newDiskList, raid_level, diskById=diskById)
        if not status:
            return status, encryptedDisks

        (status, output, err) = systemCommand("zpool add erecon %s %s" % (zfsRaidLevel, " ".join(encryptedDisks)))
        print "add disks: ", status, output, err
        return True, ""

    @classmethod
    def destroyEreconStorage(cls, forceDestroy=False):
        (status, data) = storageManager.getPoolStatus()
        if status is False:
            if not forceDestroy:
                print "Storage not yet up,storage destroy  not allowed"
                return False, "Storage not yet up,storage destroy  not allowed"
            else:
                print "Storage not yet up, destroying storage forcefully"
        # r = redis.Redis()
        # encryptedDisks = r.lrange("cryptDisks", 0, -1)

        (status, output, err) = systemCommand("zpool list erecon")
        if not status:
            print "erecon storage destroying... ", systemCommand("zpool destroy -f erecon")
        # # for disk in encryptedDisks:
        # #     (status, output, err) = systemCommand("cryptsetup luksClose %s" % (os.path.basename(disk)))
        # r.delete("encryptedPaths")
        # r.delete("cryptDisks")
        # r.delete("raidlevel")
        # r.delete("raidsetCount")
        # r.save()
        # (status, output, err) = systemCommand("rm -rf /erecon")
        print "Delete pool cache: ", systemCommand("rm -rf /etc/zfs/zpool.cache")

    @classmethod
    def replaceStorageDisk(cls, existingDisk, replacingDisk):
        if not existingDisk.startswith("/dev/disk/") and not existingDisk.startswith("/dev/"):
            existingDisk = "/dev/disk/by-id/%s" % (os.path.basename(existingDisk))
        if not replacingDisk.startswith("/dev/disk/") and not replacingDisk.startswith("/dev/"):
            replacingDisk = "/dev/disk/by-id/%s" % (os.path.basename(replacingDisk))
        '''
        (status, data) = storageIface.getPoolStatus()
        if status is False:
            print "Storage not yet up,disk replacing not allowed"
            return False, "Storage not yet up,disk replacing not allowed"
        '''
        resp = storageManager.getRaidConfigration()
        # data = {"raidLevel": r.get("raidlevel"), "disksetsize": r.get("raidsetCount")}
        try:
            if int(resp['raidlevel']) == 0:
                print "Raid level is zero, why calling replacedisk ........."
                return False, "For present raid level disk replacement not allowed..."
        except Exception, e:
            return False, "Error getting raidlevel"
        r = redis.Redis()
        # diskById = storageManager.getDiskId(replacingDisk)
        # cryptPath = storageManager.intializeDisk(replacingDisk)
        cryptPath = replacingDisk
        if cryptPath:
            # Replace disk in erecon
            (status, output, err) = systemCommand("zpool replace -f erecon %s %s" % (os.path.basename(existingDisk), cryptPath))
            if status:
                return False, err
            r.rpush("cryptDisks", replacingDisk)
            r.rpush("encryptedPaths", cryptPath)
            r.lrem("cryptDisks", existingDisk)
            r.save()
            return True, "Success"
        else:
            return False, "Error initilizing disk"


def getOsDisk():
    (status, out, err) = systemCommand("df -h / | tail -n+2")
    if not status:
        osDisk = out.strip().split()[0]
        return True, os.path.realpath(osDisk)[:-1]
    else:
        return False, ""

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "USAGE: %s <FILENAME/DISKNAME>" % sys.argv[0]
        sys.exit(1)
    diskName = sys.argv[1]
    if not os.path.exists(diskName):
        print "Disk/File %s not available" % diskName
        sys.exit(1)
    status, osDisk = getOsDisk()
    if status and diskName.startswith(osDisk):
        print "\n###################################################################\n"
        print "   OS Disk Not Allowed to Create Storage, Please check the Disk"
        print "   OS WILL GET CRASH PLEASE BE CAREFULL WHILE CREATING STORAGE"
        print "\n###################################################################\n"
        sys.exit(1)
    status, resp = storageManager.createEreconStorage([diskName], raid_level=0, diskById=False)
    if status:
        print "Storage erecon created successfully"
    else:
        print "Error in creating storage erecon: %s" % resp

