import subprocess
import os

def systemCommand(command):
    childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    out, err = childProcess.communicate()
    status = childProcess.returncode
    return status, out, err

def checkUpstartJobStatus(upstartName):
    (status, out, error) = systemCommand("initctl status %s" % upstartName)
    if not status and 'start/running' in out.lower():
        return True
    return False


def stopUpstartJob(upstartName, forceRemove=False):
    if checkUpstartJobStatus(upstartName):
        (status, out, error) = systemCommand("initctl stop %s" % upstartName)
    if forceRemove and os.path.exists("/etc/init/%s.conf" % upstartName):
        systemCommand("rm -rf /etc/init/%s.conf" % upstartName)


def startUpstartJob(upstartName):
    if not checkUpstartJobStatus(upstartName):
        (status, out, err) = systemCommand("initctl start %s" % upstartName)
        if status:
            return False, err
    return True, ""