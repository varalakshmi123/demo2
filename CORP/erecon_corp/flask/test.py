import pandas as pd
import pymongo
import json

# import numpy as np
# df=pd.read_csv('/home/prasanna/Downloads/CorpBank/output/24092017/ChargebackTransactions.csv')
#
# adjustment=pd.read_csv('UPI Adjustment Report.csv')
#
# # adjustment=adjustment.rename(columns={"Txndate":"TransactionDate","RRN":"UPI RRN"})
# print df.dtypes
# df['UPI RRN']=df['UPI RRN'].astype('str')
# df['UPI RRN'] = pd.Series(df['UPI RRN'].values).str.replace("'",'')
# adjustment['RRN'] = adjustment['RRN'].str[1:]
# #RECONCILED
# df['status']=np.where(df['UPI RRN'].isin(adjustment['RRN'].values),'Reconciled','Potential charge back')
# print df
# #CHARGE BACK
# print adjustment.columns
# adjustment['Txndate']= pd.to_datetime(adjustment['Txndate'])
# adjustment['Txndate'] = adjustment['Txndate'].dt.strftime('%d-%m-%Y')
# df['TransactionDate']=pd.to_datetime(df['TransactionDate'])
# df['TransactionDate'] = df['TransactionDate'].dt.strftime('%d-%m-%Y')
# print df.dtypes
# df1=df[df['status']=='Potential charge back']
# for i in adjustment['Txndate'].values:
#     for j in df1['TransactionDate'].values:
#         if j<i:
#             df1['status']='ChargeBack'

# print adjustment.head()
# print df



# print reconciler
# #
# reconcile=df.merge(adjustment,on=['TransactionDate','UPI RRN'],how='inner',indicator=True)
# print reconcile['_merge'].values
# print reconcile.loc[reconcile['_merge']=='both']
#     # reconcile['status']='Reconciled'



def chargeback(filePath,outputdirpath):
    mongoHost = "localhost"
    mongoclient = pymongo.MongoClient(host=mongoHost)
    db = mongoclient["erecon"]
    acquirerrbtransactions=pd.read_csv(outputdirpath + '/ChargebackTransactions.csv')
    acquirerrbtransactions['status'] = 'Potential charge back'
    acquirerrbtransactions['UPI RRN'] = acquirerrbtransactions['UPI RRN'].astype('str')
    acquirerrbtransactions['UPI RRN'] = pd.Series(acquirerrbtransactions['UPI RRN'].values).str.replace("'", '')
    acquirerrbtransactions = acquirerrbtransactions.rename(columns={"A/C NO.": "ACCNO","ACCOUNT NUMBER ":"ACCOUNT NUMBER"})
    acquirerrbtransactions['TransactionDate'] = pd.to_datetime(acquirerrbtransactions['TransactionDate'])
    acquirerrbtransactions['TransactionDate'] = acquirerrbtransactions['TransactionDate'].dt.strftime('%d-%m-%Y')
    print acquirerrbtransactions.columns
    acquirerrbtransactions['ACCOUNT NUMBER'] = acquirerrbtransactions['ACCOUNT NUMBER'].astype(str)
    acquirerrbtransactions['ACCNO']=acquirerrbtransactions['ACCNO'].astype(str)

    print acquirerrbtransactions.dtypes
    acquirerrbjson=acquirerrbtransactions.to_json(orient='records')

    adjustment = pd.read_csv(filePath+'/UPI Adjustment Report.csv')
    adjustment['RRN'] = pd.Series(adjustment['RRN'].values).str.replace("'", '')
    adjustment['Txndate'] = pd.to_datetime(adjustment['Txndate'])
    adjustment['Txndate'] = adjustment['Txndate'].dt.strftime('%d-%m-%Y')
    collection = db['chargeback']
    for item in json.loads(acquirerrbjson):
        collection.insert(item)
    # print adjustment['RRN'].values
    for rrn in adjustment['RRN'].values:
        # print collection.find({"UPI RRN": rrn})
        # print rrn
        for post in collection.find({"UPI RRN": rrn}):
            print "%%%%%%%%%%%%%%%%%%%%%%%%%",post
            collection.update({"UPI RRN": rrn},{'$set':{"status": "Reconciled"}})

    # df1=df[df['status']=='Potential charge back']
    for i in adjustment['Txndate'].values:
        for j in acquirerrbtransactions['TransactionDate'].values:
            if j < i:
                print j<i
                print collection.find({"TransactionDate": j, "status": "Potential charge back"})
                collection.update({"TransactionDate":j,"status":"Potential charge back"},{'$set':{"status":"ChargeBack"}})
        else:
            print "NO records are lesser than the given date"

chargeback('/home/prasanna/Downloads/CorpBank/27092017','/home/prasanna/Downloads/CorpBank/output/27092017')



