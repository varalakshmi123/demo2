import pymongo
from pymongo import MongoClient
import logger

logger = logger.Logger.getInstance("application").getLogger()


class ReconBuilder():
    def __init__(self):
        self.mongo_client = MongoClient('mongodb://localhost:27017/')
        self.mongo_collection = self.mongo_client.reconbuilder

    def getReconContextDetails(self,id):
        reconJson = []
        cc_col = self.mongo_collection['recon_context_details']
        for val in cc_col.find():
            reconObj = {}
            reconObj['reconName'] = val['reconName'] if 'reconName' in val else ''
            reconObj['reconId'] = val['reconId'] if 'reconId' in val else ''
            reconObj['sources'] = val['processingCount'] if 'processingCount' in val else '0'
            reconObj['currency'] = val['currency'] if 'currency' in val else ''
            if 'businessContext' in val and val['businessContext']:
                reconObj['businessContextId'] = str(val['businessContext']['BUSINESS_CONTEXT_ID']) if 'BUSINESS_CONTEXT_ID' in val['businessContext'] else str(val['businessContext']['business_context_id'])
                reconObj['businessProcessName'] = val['businessContext']['BUSINESS_PROCESS_NAME'] if 'BUSINESS_PROCESS_NAME' in val['businessContext'] else val['businessContext']['businessProcess']
                reconObj['workpool_name'] = val['businessContext']['WORKPOOL_NAME'] if 'WORKPOOL_NAME' in val['businessContext'] else val['businessContext']['workPoolName']
            else:
                reconObj['businessContextId'] = ''
                reconObj['businessProcessName'] = ''
                reconObj['workpool_name'] = ''
            if 'businessContextId' in val and 'reconId' in val:
                if val['businessContextId'] == 1111131119 and val['reconId'] == 'SUSP_CASH_APAC_18646':
                    reconObj['licenseCount'] = 37
                else:
                    reconObj['licenseCount'] = 1
            else:
                reconObj['licenseCount'] = 1
            reconObj['licensed'] = 'Licensed'
            reconJson.append(reconObj)
        return True,reconJson

    def getJobSummaryReconBuilder(self,id,query):
        reconJobData = []
        cc_col_Jon = self.mongo_collection['jobSummary']
        for val in cc_col_Jon.find({'reconId':query['recon_id']}):
            obj = {}
            for key in ['reconName','stmtDate','jobEnd','reconId','jobStatus','failureReason','jobStart',
                         'totalInput','totalOutput', 'matchedCount','unmatchedCount','jobEnd']:
                if key in val.keys():
                    obj[key] = val[key]
            reconJobData.append(obj)
        return True,reconJobData

if __name__=='__main__':
    rb = ReconBuilder()
    rb.getJobSummaryReconBuilder('dummy',{'recon_id':'FIC_CASH_APAC_18663'})
