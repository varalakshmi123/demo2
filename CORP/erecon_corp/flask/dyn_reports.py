import json
import config
import pandas
from os import sep
import datetime
import numpy as np
import logger
import sys
from bson import ObjectId

from pymongo import MongoClient, DESCENDING
logger = logger.Logger.getInstance("application").getLogger()

class Reports(object):
    def __init__(self, reconId, execId):
        self.mongoclient = MongoClient('mongodb://localhost:27017')
        self.mongo_db = self.mongoclient['erecon']
        self.exectnColObj = self.mongo_db['recon_execution_details_log']
        self.dashboard_reports = self.mongo_db['dashboard_reports']
        self.businessCol = self.mongo_db['business']
        self.reconId = reconId
        self.execId = execId
        clonePath = self.getLatestExeDetails(reconId)['READ_CLONE_PATH']
        self.store_loc = sep + clonePath + sep + str(execId) + '.h5'
        iterator = self.fetch_all()
        data = pandas.DataFrame()
        for df in iterator:
            data = pandas.concat([data, df[df['ENTRY_TYPE'] == 'T']]).reset_index(drop=True)
        self.reconData = data

    # Method to fetch the data for the given key without limit in chunks
    def fetch_all(self,storeKey=config.store_key, where_clause="'RECORD_STATUS' == 'ACTIVE'"):
        # store_loc: /usr/share/nginx/reconid/executionid.h5,storeKey: 'TXN_DATA/REPORT_DATA'
        try:
            iterator = pandas.read_hdf(self.store_loc, storeKey, chunksize=config.hdf5_chunk_size,where=where_clause)
        except KeyError, e:
            return []
        logger.info('Data reading in chunk completed.')
        return iterator

    def getLatestExeDetails(self, reconId):
        doc = self.exectnColObj.find_one({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                                   'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                   'RECORD_STATUS': 'ACTIVE'
                                                   }, sort=[('RECON_EXECUTION_ID', DESCENDING)])
        return doc

    def genDashBoards(self):
        self.getAgingReport()
        self.getTransactionSummaryByDate()
        self.customProcessOutStanding()
        self.customProcess()
        self.ab_reports()

    def get_business_details(self):
        business_df = pandas.DataFrame()
        business_data = list(self.businessCol.find())
        business_df = pandas.DataFrame(business_data)
        business_df['BUSINESS_CONTEXT_ID'] = business_df['BUSINESS_CONTEXT_ID'].astype(str)
        business_df.columns = business_df.columns.str.upper()
        return business_df

    def checkAndSave(self,frame,reportName):
        print '    ','-'*30
        print '    ',reportName
        doc = {"RECON_EXECUTION_ID": self.execId, 'reportName': reportName,'ReportData': frame.T.to_dict().values()}
        doc["partnerId"] = "54619c820b1c8b1ff0166dfc"
        doc['RECON_ID'] = self.reconId
        qry = {"RECON_EXECUTION_ID": self.execId, 'reportName': reportName,
                 'RECON_ID': self.reconId}
        if self.dashboard_reports.find_one(qry):
            doc = self.dashboard_reports.find_one(qry)
            doc['ReportData'] = frame.T.to_dict().values()
            doc['RECON_ID'] = self.reconId
            self.dashboard_reports.update({'_id':doc['_id']}, {"$set": {'ReportData': frame.T.to_dict().values()}})
            logger.info('Modified the report data')
        else:
            self.dashboard_reports.save(doc)
            logger.info('Saved the report data')

    def getAgingReport(self):
        statesNewdf = pandas.read_csv(config.flaskPath+"meta/statesNew.csv")
        df = self.reconData[self.reconData['TXN_MATCHING_STATUS'] == 'UNMATCHED']

        if len(df) == 0:
            return False, 'No Data Found'

        execStatusDf = statesNewdf[['TXN_MATCHING_STATUS', 'EXCEPTION_STATUS']].drop_duplicates(
            subset=['TXN_MATCHING_STATUS'])
        df = pandas.merge(df, execStatusDf, on=['TXN_MATCHING_STATUS'], how="left")
        records = df[["RECON_ID", "BUSINESS_CONTEXT_ID", "STATEMENT_DATE", "AMOUNT", "EXCEPTION_STATUS"]].dropna(
            subset=['STATEMENT_DATE'])
        records = records[pandas.notnull(records['STATEMENT_DATE'])]
        records["TRANSACTIONAGE"] = records["STATEMENT_DATE"].apply(
            lambda x: (datetime.datetime.now() - x).days if x.year != 0001  else 0)
        records["COUNT"] = 1
        records["RECON_EXECUTION_ID"] = self.execId
        business_df = self.get_business_details()
        df2 = business_df[business_df['RECON_ID'].isin([self.reconId])]
        df2 = df2[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
        if len(df2):
            records = pandas.merge(records, df2, on=["RECON_ID"], how="left")
        else:
            records.loc[records.index,['WORKPOOL_NAME', 'RECON_NAME']] = '',''
        cols = ['WORKPOOL_NAME', 'RECON_NAME', "RECON_EXECUTION_ID", "RECON_ID", "BUSINESS_CONTEXT_ID",
                "TRANSACTIONAGE",
                "EXCEPTION_STATUS", "COUNT", "AMOUNT"]
        # Remove all matched transactions if any exists
        records.loc[records.loc[:, 'EXCEPTION_STATUS'] == 'CLOSED', 'EXCEPTION_STATUS'] = None
        records = records[cols].groupby(cols[:-2]).sum().reset_index()
        self.checkAndSave(records, 'AgingReport')
        return records

    def getTransactionSummaryByDate(self):
        indexColumns = ["BUSINESS_CONTEXT_ID", "RECON_ID", "STATEMENT_DATE", "TXN_MATCHING_STATUS",
                        "DEBIT_CREDIT_INDICATOR", "AMOUNT","COUNT"]
        df = self.reconData.copy()
        df.loc[df.index, "COUNT"] = 1
        if len(df) == 0:
            return False, 'No Data Found'
        for col in indexColumns:
            if col not in df.columns:
                return False,'Cannot Generate Report'

        summarizer = df[indexColumns]
        summarizer = summarizer[pandas.notnull(summarizer['STATEMENT_DATE'])]
        summarizer = summarizer[summarizer['STATEMENT_DATE'].apply(lambda x: True if x.year != 0001 else False)]
        dfOutstanding = pandas.pivot_table(summarizer, index=indexColumns[0:-2],
                                           values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len},fill_value=0.0).reset_index()
        if 'RECON_ID' in dfOutstanding.columns:
            business_df = self.get_business_details()
            df2 = business_df[business_df['RECON_ID'].isin([self.reconId])]
            df2 = df2[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
            if len(df2):
                dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
            else:
                dfOutstanding.loc[dfOutstanding.index, ['WORKPOOL_NAME', 'RECON_NAME']] = '', ''

        self.checkAndSave(dfOutstanding, 'TransactionSummaryByDate')

    def customProcessOutStanding(self):
        # [self.reconData['TXN_MATCHING_STATUS'].isin(['ROLLBACK_AUTHORIZATION_PENDING', 'FORCE_MATCHED', 'AUTOMATCHED', 'MATCHED']) == False]
        df = self.reconData
        if len(df) == 0:
            return False, 'No Data Found'
        for col in ['DEBIT_CREDIT_INDICATOR']:
            if col not in df.columns:
                return False,'Cannot Generate Report'
        dfOutstanding = df.copy()
        dfOutstanding["COUNT"] = 1

        if 'DEBIT_CREDIT_INDICATOR' not in dfOutstanding.columns:
            dfOutstanding['DEBIT_CREDIT_INDICATOR'] = 'Not Specified'
        if 'EXCEPTION_STATUS' in dfOutstanding.columns:
            dfOutstanding.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'
        if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding[
            'DEBIT_CREDIT_INDICATOR'] is not None:
            if 'ACCOUNT_NAME' not in dfOutstanding.columns:
                dfOutstanding['ACCOUNT_NAME'] = ''
            dfOutstanding = dfOutstanding[
                ["BUSINESS_CONTEXT_ID", "RECON_ID", "SOURCE_NAME", "ACCOUNT_NAME", "TXN_MATCHING_STATUS",
                 'DEBIT_CREDIT_INDICATOR',"CARRY_FORWARD_FLAG", 'AMOUNT', "COUNT"]]
            pivotFrame = pandas.pivot_table(dfOutstanding,
                                            index=["BUSINESS_CONTEXT_ID", "RECON_ID", "SOURCE_NAME", "ACCOUNT_NAME",
                                                   "TXN_MATCHING_STATUS", 'DEBIT_CREDIT_INDICATOR',"CARRY_FORWARD_FLAG"],
                                            values=['AMOUNT', "COUNT"],
                                            aggfunc={"AMOUNT": np.sum, "COUNT": len})
            pivotFrame.reset_index(inplace=True)
            self.checkAndSave(pivotFrame,'OutStandingAmount')

    def customProcess(self):
        df = self.reconData.copy()
        if len(df) == 0:
            return False, 'No Data Found'
        business_df = self.get_business_details()
        df.loc[df.index, "COUNT"] = 1
        idxColumns = ["BUSINESS_CONTEXT_ID", "RECON_ID", "TXN_MATCHING_STATUS","DEBIT_CREDIT_INDICATOR", "AMOUNT", "COUNT"]
        for col in idxColumns:
            if col not in df.columns:
                return False,'Cannot Generate Report'
        dfOutstanding = df.copy()
        dfOutstanding = dfOutstanding[idxColumns]

        if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding['DEBIT_CREDIT_INDICATOR'] is not None:
            dfOutstanding = pandas.pivot_table(dfOutstanding, index=idxColumns[0:-2],
                                               values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0.0).reset_index()
        df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
        df2 = df2[df2['RECON_ID'] == self.reconId]
        if len(df2):
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        else:
            dfOutstanding.loc[dfOutstanding.index, ['WORKPOOL_NAME', 'RECON_NAME']] = '', ''

        self.checkAndSave(dfOutstanding, 'TransactionStatus')

    def ab_reports(self):
        if self.reconId in ['EPYMTS_NEFT_INWARD_APAC_0009','EPYMTS_NEFT_OUTWARD_APAC_0010','EPYMTS_NEFT_INWARD_APAC_0011']:
            df = self.reconData[self.reconData['TXN_MATCHING_STATUS'].isin(
                ['ROLLBACK_AUTHORIZATION_PENDING', 'FORCE_MATCHED', 'AUTOMATCHED', 'MATCHED']) == False]
            df = df[df['SOURCE_TYPE'] == 'S2']
            if len(df) == 0:
                return False, 'No Data Found'
            cols = ['AMOUNT','AMOUNT_1','RECON_ID']
            report = df[cols].pivot_table(index=['RECON_ID'],aggfunc={'AMOUNT':np.sum,'AMOUNT_1':np.sum}).reset_index()
            self.checkAndSave(report, 'AB_Reports')








# if '__main__':
#     #     print 'EPYMTS_NEFT_OUTWARD_APAC_0010','26842'
#     Reports(sys.argv[1],sys.argv[2]).genDashBoards()
