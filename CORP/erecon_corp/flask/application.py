import logger
import pandas as pd
logger = logger.Logger.getInstance("application").getLogger()
import dbinterface
import re
import flask
import pandas
import numpy as np
import config
import os
from bson import json_util
import datetime
from flask import request
import json
import sql
import uuid
import csv
import datatypes
import noauthhandler
from dateutil.parser import parse
import pymongo
from pymongo import MongoClient, ReadPreference, DESCENDING
from reconBuilder import ReconBuilder
# from engine import scripts
import glob
# from jinja2 import Environment, FileSystemLoader
# from weasyprint import HTML
import time
import paramiko
import exportReconDetails
from hdf5interface import HDF5Interface
from ereconSerializer import ReconWorker
# import importReconDetails
# import recon_deployer
from pandas import HDFStore
from mdfieldSizes import modelFieldColSize as colSizes
import checkUpstart as upstat
from ZFSApi import ZFSApi
from Hdf5ZfsUtil import Hdf5ZfsUtil
import subprocess
import utilities
import zipfile

def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)


def getPartnerFilePath():
    return os.path.join(config.commonFilesPath, "theme_templates") + "/"


class Business(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Business, self).__init__("business", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(Business, self).create(doc)
        return status, businessDoc


class UsersGraph(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UsersGraph, self).__init__("usersgraph", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(UsersGraph, self).create(doc)
        return status, businessDoc


class FileUpload(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(FileUpload, self).__init__("fileupload", hideFields)

    def create(self, doc):
        if self.exists({'fileName': {"$regex": "^" + doc['fileName'] + "$", "$options": 'i'}}):
            return False, "FileName already exists."
        (status, businessDoc) = super(FileUpload, self).create(doc)
        return status, businessDoc


class JobStatus(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(JobStatus, self).__init__("jobstatus", hideFields)


class ReconAudit(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAudit, self).__init__("reconaudit", hideFields)


class ReconLicense(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconLicense, self).__init__("reconlicense", hideFields)

    def getAll(self, query={}):
        skip, limit = 0, 10
        if 'skip' in query:
            skip = query['skip']
            del query['skip']
        if 'limit' in query:
            limit = query['limit'] + skip
            del query['limit']
        stats, resp = super(ReconLicense, self).getAll(query)
        bizIds = flask.session['sessionData']['businessContextIds']
        df = pandas.DataFrame(resp['data'])
        if 'businessContextId' in df.columns:
            df['businessContextId'] = df['businessContextId'].astype(str)
            df['businessContextId'] = df['businessContextId'].str.replace('.0','')
            df = df.loc[df[df['businessContextId'].isin(bizIds)].index]
            df.index = range(1, len(df) + 1)
            resp['total'] = len(df)
            df = df[skip:limit]
            resp['current'] = len(df)
            resp['data'] = df.T.to_dict().values()

        stats, jobStatusObj = JobStatus().getAll({})
        jStatusdf = pandas.DataFrame(jobStatusObj['data'])
        if len(jStatusdf) and len(df):
            jStatusdf = jStatusdf[jStatusdf['reconId'].isin(df['reconId'].unique())]
            jStatusdf.sort(columns=['updated'], inplace=True)
            jStatusdf.drop_duplicates(subset=['reconId'], keep='last')
            jStatusdf = jStatusdf[['reconId', 'executionDT', 'jobStatus', 'ExecutedBy', 'log_path', 'statementDate']]
            if 'jobStatus' in df.columns: df.drop(['jobStatus'], axis=1, inplace=True)
            if 'executionDT' in df.columns: df.drop(['executionDT'], axis=1, inplace=True)
            if 'log_path' in df.columns: df.drop(['log_path'], axis=1, inplace=True)
            jStatusdf.drop_duplicates(subset=['reconId'], inplace=True)
            logger.info(len(df))
            response = pandas.merge(df, jStatusdf, on=['reconId'], how='left')
            logger.info(len(response))
            response = response.where((pandas.notnull(response)), '')
            resp['data'] = response.T.to_dict().values()
        return True, resp

    def getReconLicense(self, id):
        (status_builder, data_builder) = ReconBuilder().getReconContextDetails('dummy')
        (status_license, licenseData) = super(ReconLicense, self).getAll(query={})
        logger.info(licenseData)
        if licenseData['total'] == 0:
            for val in data_builder:
                (status, reconLicenseDoc) = super(ReconLicense, self).create(val)
        if len(data_builder) != licenseData['total']:
            existingData = []
            for y in licenseData['data']:
                existingData.append(y['reconId'])
            for x in data_builder:
                if x['reconId'] in existingData:
                    pass
                else:
                    # x['licensed'] = 'Non-Licensed'
                    (status, reconLicenseDoc) = super(ReconLicense, self).create(x)
        return True, {}

    def execution(self, command):
        childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        close_fds=True)
        out, err = childProcess.communicate()
        status = childProcess.returncode
        return status, out,err


    def chargeback(self,id, date):
	print "/////////////////////////////",date
    	mongoHost = "localhost"

        mongoclient = pymongo.MongoClient(host=mongoHost)
    	db = mongoclient["erecon"]
	filepath='/usr/share/nginx/v8/mft/CORP/'
	outputdirpath='/usr/share/nginx/www/erecon/ui/app/files/recon_reports/'
	if os.path.exists(filepath+date+'/UPI Adjustment Report.csv'):
    		acquirerrbtransactions=pd.read_csv(outputdirpath+ date + '/ChargebackTransactions.csv')
    		acquirerrbtransactions['status'] = 'Potential charge back'
    		acquirerrbtransactions['UPI RRN'] = acquirerrbtransactions['UPI RRN'].astype('str')
    		acquirerrbtransactions['UPI RRN'] = pd.Series(acquirerrbtransactions['UPI RRN'].values).str.replace("'", '')
    		acquirerrbtransactions = acquirerrbtransactions.rename(columns={"A/C NO.": "ACCNO","ACCOUNT NUMBER ":"ACCOUNT NUMBER"})
   		acquirerrbtransactions['TransactionDate'] = pd.to_datetime(acquirerrbtransactions['TransactionDate'])
	    	acquirerrbtransactions['TransactionDate'] = acquirerrbtransactions['TransactionDate'].dt.strftime('%d-%m-%Y')
   	 	print acquirerrbtransactions.columns
    		acquirerrbtransactions['ACCOUNT NUMBER'] = acquirerrbtransactions['ACCOUNT NUMBER'].astype(str)
    		acquirerrbtransactions['ACCNO']=acquirerrbtransactions['ACCNO'].astype(str)
		acquirerrbtransactions['AMOUNT']= acquirerrbtransactions['AMOUNT'].abs()

	    	print acquirerrbtransactions.dtypes
    		acquirerrbjson=acquirerrbtransactions.to_json(orient='records')

	    	adjustment = pd.read_csv(filepath+date+'/UPI Adjustment Report.csv')
	    	adjustment['RRN'] = pd.Series(adjustment['RRN'].values).str.replace("'", '')
                adjustment=adjustment[adjustment['Adjtype'].isin(['TCC','RET','Credit Adjustment'])]
	    	adjustment['Txndate'] = pd.to_datetime(adjustment['Txndate'])
	    	adjustment['Txndate'] = adjustment['Txndate'].dt.strftime('%d-%m-%Y')
    		collection = db['chargeback']
	    	for item in json.loads(acquirerrbjson):
                        item['partnerId'] = "54619c820b1c8b1ff0166dfc"
        		output = collection.find({"UPI RRN":item['UPI RRN']})
                        if output and len(list(output)) > 0:
				print "already exists"
			else:
				print ":INSERTING"
				collection.insert(item)
    	# print adjustment['RRN'].values
    		for rrn in adjustment['RRN'].values:
	        # print collection.find({"UPI RRN": rrn})
        	# print rrn
        		for post in collection.find({"UPI RRN": rrn}):
            			print "%%%%%%%%%%%%%%%%%%%%%%%%%",post
	           		collection.update({"UPI RRN": rrn},{'$set':{"status": "Reconciled"}})

    # df1=df[df['status']=='Potential charge back']
    		lowest=min(adjustment['Txndate'].values)

		for i in acquirerrbtransactions['TransactionDate'].values:
	        	if lowest > i:
        	        	print lowest>i
	                	print collection.find({"TransactionDate": i, "status": "Potential charge back"})
        	        	collection.update({"TransactionDate":i,"status":"Potential charge back"},{'$set':{"status":"ChargeBack"}})
	        	else:
        	    		print "NO records are lesser than the given date"
	
		return True,"SUCCESS"
	else:
		return False,"FILE not exists"
		


    def runReport(self, id, date):
	jobStatus = {}
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(config.engineIp, username=config.engineUserName, password=config.enginePassword)
        except Exception, e:
            logger.info(str(e))
            status, reconObj = JobStatus().get({'reconId': 'CB_CASH_APAC_61171'})
            reconObj['jobStatus'] = 'FAILURE'
            reconObj['executionDT'] = int(time.time())
            reconObj['log_path'] = ''
            st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)
            return False, 'Cannot connect to ssh'

        if JobStatus().exists({'reconId': 'CB_CASH_APAC_61171'}):
            status, reconObj = JobStatus().get({'reconId': 'CB_CASH_APAC_61171'})
            reconObj['jobStatus'] = 'Job Running'
            reconObj['executionDT'] = int(time.time())
            st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)

        logger.info('Recon Job Initiated')
        print('Recon Job Initiated')
        runcmd = 'sudo /usr/share/nginx/www/erecon/flask/erecon/bin/python2 ' + str(
            config.enginePath) + os.sep + 'scripts' + os.sep + 'ParseInputs.py' + ' ' + date
        #stdin, stdout, stderr = ssh.exec_command(runcmd)	
        stdin, stdout, stderr =  utilities.execute(runcmd, True)
	logger.info(stdin)
	logger.info(stdout)
	logger.info(stderr)
	time.sleep(5)
        statusSign = True
	# Custom Functions are called in engine, which dont write to log file, stderr is not valid here
	#log_entries = list(stderr)
	'''log_entries = []
        for val in log_entries:
            logger.info(val)
        if log_entries and 'SUCCESS' in log_entries[len(log_entries) - 2]:
            jobStatus['status'] = 'SUCCESS'
            reconObj['jobStatus'] = 'SUCCESS'
            reconObj['log_path'] = ''
            statusSign = True
	else:
	    statusSign = True
        if len(log_entries) > 3:
            for logmsge in log_entries[-4:]:
                # print logmsge
                if 'SUCCESS' in logmsge:
                    statusSign = True
                    break
         '''
        logger.info('break point 1')

        if statusSign:
            jobStatus['status'] = 'SUCCESS'
            reconObj['jobStatus'] = 'SUCCESS'
            reconObj['log_path'] = ''
        else:
            jobStatus['status'] = 'FAILURE'
            reconObj['jobStatus'] = 'FAILURE'
            log_path = 'CB_CASH_APAC_61171/failure_logs_' + datetime.datetime.now().strftime(
                '%d%m%Y%I%M%S') + '.txt'

            if not os.path.exists(os.path.join(config.contentDir, "engine_error_logs/CB_CASH_APAC_61171")):
                os.makedirs(os.path.join(config.contentDir, "engine_error_logs/CB_CASH_APAC_61171"))

            files_list = os.listdir(os.path.join(config.contentDir, "engine_error_logs/CB_CASH_APAC_61171"))
            if len(files_list) == 3:
                for file in files_list:
                    os.remove(os.path.join(config.contentDir, "engine_error_logs/{}/".format('CB_CASH_APAC_61171')) + file)

            with open(os.path.join(config.contentDir, "engine_error_logs/") + log_path, 'w') as log_file:
                for entry in log_entries:
                    log_file.write(entry)
            jobStatus['log_path'] = log_path
            reconObj['log_path'] = log_path
        st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)
        logger.info(reconObj)
        ssh.close()
        return True, jobStatus


    def runRecon(self, id, query):
        print query
        print '*' * 100
        jobStatus = {}
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print config.engineIp, config.engineUserName, config.enginePassword
        try:
            ssh.connect(config.engineIp, username=config.engineUserName, password=config.enginePassword)
        except Exception, e:
            logger.info(str(e))
            status, reconObj = JobStatus().get({'reconId': query['recon_id']})
            reconObj['jobStatus'] = 'FAILURE'
            reconObj['executionDT'] = int(time.time())
            reconObj['log_path'] = ''
            st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)
            return False, 'Cannot connect to ssh'

        # (stdin, stdout, stderr) = ssh.exec_command(
        #        'sudo python /usr/share/nginx/erecon/engine/scripts/JobExecutor.py {} {}'.
        #            format(query['recon_id'], query['statementDate']))

        if JobStatus().exists({'reconId': query['recon_id']}):
            status, reconObj = JobStatus().get({'reconId': query['recon_id']})
            reconObj['jobStatus'] = 'Job Running'
            reconObj['executionDT'] = int(time.time())
            st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)

        logger.info('Recon Job Initiated')
        print ('Recon Job Initiated')
        runcmd = 'sudo /usr/share/nginx/www/erecon/flask/erecon/bin/python2 ' + str(
            config.enginePath) + os.sep + 'scripts' + os.sep + 'JobExecutor.py' + ' ' + query[
                     'recon_id'] + ' ' + query['statementDate']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        print stdin, stdout, stderr

        # update recon job status
        # check for job success statusi
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)
            print val
        statusSign = False
        if 'SUCCESS' in log_entries[len(log_entries) - 2]:
            jobStatus['status'] = 'SUCCESS'
            reconObj['jobStatus'] = 'SUCCESS'
            reconObj['log_path'] = ''
            statusSign = True
        # print log_entries[:-4]

        if len(log_entries) > 3:
            for logmsge in log_entries[-4:]:
                # print logmsge
                if 'SUCCESS' in logmsge:
                    statusSign = True
                    break

        if statusSign:
            jobStatus['status'] = 'SUCCESS'
            reconObj['jobStatus'] = 'SUCCESS'
            reconObj['log_path'] = ''
        else:
            jobStatus['status'] = 'FAILURE'
            reconObj['jobStatus'] = 'FAILURE'
            log_path = query['recon_id'] + '/failure_logs_' + datetime.datetime.now().strftime(
                '%d%m%Y%I%M%S') + '.txt'

            if not os.path.exists(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id'])):
                os.makedirs(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id']))

            files_list = os.listdir(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id']))
            if len(files_list) == 3:
                for file in files_list:
                    os.remove(os.path.join(config.contentDir, "engine_error_logs/{}/".format(query['recon_id'])) + file)

            with open(os.path.join(config.contentDir, "engine_error_logs/") + log_path, 'w') as log_file:
                for entry in log_entries:
                    log_file.write(entry)
            jobStatus['log_path'] = log_path
            reconObj['log_path'] = log_path
        st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)
        # print reconObj
        ssh.close()
        return True, jobStatus

    def exportReconDetails(self, id, query):
        status, exportPath = exportReconDetails.exportReconMigration(query['reconId'])
        tarFileName = query['reconId'] + '_' + datetime.datetime.now().strftime('%d%b%Y%H%M') + '.tar.gz'
        if status:
            cmd = 'tar -czf ' + tarFileName + ' ' + exportPath
            os.system(cmd)
            os.system('sudo rm -r ' + config.flaskPath + exportPath)
            cmd = 'cp ' + tarFileName + ' ' + config.contentDir + os.sep
            os.system(cmd)
            os.system('sudo rm -r ' + config.flaskPath + tarFileName)
        return status, tarFileName

    def getMigrationList(self, id, query):
        files = {}
        # print query
        path = config.reconMigrationFolder
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(config.engineIp, username=config.engineUserName, password=config.enginePassword)
        runcmd = 'sudo python ' + config.reconMigrationFolder + 'getFiles.py ' + query['search']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stdout)
        f = []
        # print repr(log_entries[0])
        if str(log_entries[0]) == '[]' or str(log_entries[0]) == '[]\n':
            return True, files

        for val in log_entries[0].split('[')[1].split(']')[0].split(','):
            reconId = val.strip().replace("'", '').rsplit('_', 1)[0]
            arryName = val.strip().replace("'", '').rsplit('_', 1)[1]
            ts = arryName.replace('.tar.gz', '')
            print val, f, reconId
            if reconId not in f:
                f.append(reconId)
                files[reconId] = {}
                files[reconId]['fileTS'] = [ts]
                files[reconId]['fileNames'] = [val.strip().replace("'", '')]
            else:

                files[reconId]['fileTS'].append(ts)
                files[reconId]['fileNames'].append(val.strip().replace("'", ''))
        print files

        return True, files

    def importReconDetails(self, id, query):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(config.engineIp, username=config.engineUserName, password=config.enginePassword)
        reconId = str(query['reconId'])
        runcmd = 'sudo python ' + config.reconMigrationFolder + 'importReconDetails.py ' + query['reconId'] + ' ' + \
                 query['fileName']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)

        print '*' * 100
        log_entries = list(stdout)
        if log_entries[len(log_entries) - 1] == 'Recon Successfully Imported\n':
            return True, 'Recon Successfully Imported'
        for val in log_entries:
            logger.info(val)

        # runcmd = 'sudo python ' + config.reconMigrationFolder + 'recon_deployer.py ' + query['reconId'] + ' ' + \
        #          query['fileName']
        # stdin, stdout, stderr = ssh.exec_command(runcmd)
        # print runcmd
        # log_entries = list(stdout)
        # for val in log_entries:
        #     print val
        # log_entries = list(stderr)
        # for val in log_entries:
        #     print val

        # reconJob = recon_deployer.ReconMigration(reconId, query['fileName'])

        auditObj = {
            'importBy': flask.session['sessionData']['userName'],
            'importDetails': query,
            'reconId': query['reconId'],
            'operation': 'implement'
        }
        status, auditObj = ReconAudit().create(auditObj)

        return True, ''

    def rollBackReconDetails(self, id, query):
        print query
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(config.engineIp, username=config.engineUserName, password=config.enginePassword)
        reconId = str(query['reconId'])
        # if '#V0.tar.gz' in
        if query['rollBkStatus']:
            runcmd = 'sudo python ' + config.reconMigrationFolder + 'backups/' + 'importReconDetails.py ' + query[
                'reconId'] + ' ' + \
                     query['fileName']
        else:
            runcmd = 'sudo python ' + config.reconMigrationFolder + 'rollback.py ' + query[
                'reconId']
        print runcmd
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)

        log_entries = list(stdout)
        for val in log_entries:
            logger.info(val)

        auditObj = {
            'importBy': flask.session['sessionData']['userName'],
            'importDetails': query,
            'reconId': query['reconId'], 'operation': 'rollback'
        }
        status, auditObj = ReconAudit().create(auditObj)

        return True, ''

    def rollbackRecon(self, id, query):
        print query
        print '*' * 100
        prevExecDoc = ReconExecutionDetailsLog().find(
            {'RECON_ID': str(query['recon_id']), 'EXECUTION_STATUS': 'Completed',
             'RECORD_STATUS': 'ACTIVE', 'PROCESSING_STATE_STATUS': 'Matching Completed'}).sort(
            [('RECON_EXECUTION_ID', -1)])
        prevExecDoc = list(prevExecDoc)
        if len(prevExecDoc) == 0:
            return True, 'No Execution details Found.'
        else:
            rec = prevExecDoc[0]
            rec['RECORD_STATUS'] = 'INACTIVE'
            rec['UPDATED_BY'] = flask.session['sessionData']['userName']
            rec['UPDATED_DATE'] = int(time.time())
            rec['EXECUTION_STATUS'] = 'Rollback'
            ReconExecutionDetailsLog().modify({'_id': rec['_id']}, rec)
            if self.reconID == 'TRSC_CASH_APAC_20181':
                NostroReport().remove({"execID": str(currExecID)})
            jobSummary = {}
            jobSummary['reconId'] = query['recon_id']
            jobSummary["jobStatus"] = 'ROLLBACK'
            jobSummary["stmtDate"] = rec['STATEMENT_DATE']
            # TODO Post Job Summary in jobSummary collection of reconbuilder DB
        return True, 'Rollback Successful'

    def getJobSummary(self, id, query):
        status_job, jobData = ReconBuilder().getJobSummaryReconBuilder(id='dummy', query=query)
        resp = {}
        resp["total"] = len(jobData)
        resp["current"] = len(jobData)
        resp["data"] = list(jobData)
        return status_job, resp


class AccountPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(AccountPool, self).__init__("accountpool", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(AccountPool, self).create(doc)
        return status, businessDoc

    def getAccountPoolDetails(self, id):
        (status, recon_con) = sql.getConnection()
        if status:
            (status_ac, ac_data) = sql.getAcPools({}, connection=recon_con)
            return ac_data.to_json()


class MhadaReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(MhadaReport, self).__init__("mhada_report", hideFields)

    def create(self, doc):
        (status, mhadaDoc) = super(MhadaReport, self).create(doc)
        return status, mhadaDoc

class UpiFailedTransactionReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
	super(UpiFailedTransactionReport, self).__init__("upifailedtransactionreport",hideFields)

class UpiAcqFailedTransactionReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
	super(UpiAcqFailedTransactionReport, self).__init__("upiacquirerfailedtransactionreport",hideFields)

class NoAuth(noauthhandler.noAuthHandler):
    def getRBData(self, id, date):
	logger.info("Getting all the date record" +str(date))
        status, records = UpiFailedTransactionReport().get({"statementdate": str(date)})     
	return True, records

class NostroReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NostroReport, self).__init__("nostro_reports", hideFields)

    def create(self, doc):
        (status, nostroReport) = super(NostroReport, self).create(doc)
        return status, nostroReport


class UserPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UserPool, self).__init__("userpool", hideFields)

    def create(self, doc):
        if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
            return False, "UserPool already exists."
        (status, userpoolDoc) = super(UserPool, self).create(doc)
        return status, userpoolDoc

    # def deleteData(self,condition):
    #     logger.info(condition)
    #     (status,found) = UserPool(hideFields=False).get({'_id':condition})
    #     eventDoc = {'type':'userpool deletion','actionOn':found['name'],'createdDate':utilities.getUtcTime(),'createdBy':flask.session["sessionData"]['userName']}
    #     Audits().create(eventDoc)
    #     return super(UserPool, self).delete(condition)

    def updatePool(self, condition):
        logger.info('@' * 200)
        logger.info(condition)
        (status, found) = UserPool(hideFields=False).get({'_id': condition})
        logger.info(found)
        eventDoc = {'type': 'userpool updation', 'actionOn': found['name'], 'createdDate': utilities.getUtcTime(),
                    'createdBy': flask.session["sessionData"]['userName']}
        return Audits().create(eventDoc)
        # return Audits().create(eventDoc)

    def addUsers(self, id, query):
        logger.info(query)
        users = []
        if query is not None:
            for r in query['selectedUsers']:
                users.append(r['name'])
            for i in query['selectedUserPool_ids']:
                (status, userpoolData) = self.get({'_id': i})
                userpool_users = userpoolData['users'].split(',')
                usersArray = users + userpool_users
                logger.info(usersArray)
        return True, {}

    def getAll(self, query={}):
        skip, limit = 0, 10
        if 'skip' in query:
            skip = query['skip']
            del query['skip']
        if 'limit' in query:
            limit = query['limit'] + skip
            del query['limit']
        stats, resp = super(UserPool, self).getAll(query)
        bizIds = flask.session['sessionData']['businessContextIds']
        df = pandas.DataFrame(resp['data'])
        response = pandas.DataFrame()
        if 'business_context' in df.columns:
            df['business_context'] = df['business_context'].astype(str)
            df.loc[df.index, 'bizId'] = ''
            for bzid in bizIds:
                df.loc[df[df['business_context'].str.contains(str(bzid), case=False)].index, 'bizId'] = bzid
            response = df[df['bizId'].isin(bizIds)]
            response.index = range(1, len(response) + 1)
            resp['total'] = len(response)
            response = response[skip:limit]
            print response
            resp['current'] = len(response)
            resp['data'] = response.T.to_dict().values()
        return stats, resp


class Utilities(object):
    def upload_(self, id):
        data = dict(request.args)
        contentDir = os.path.join(config.contentDir)
        if not os.path.exists(contentDir):
            os.makedirs(contentDir)
        type = id.split('.')
        doc = {}
        doc['fileName'] = data['fileName'][0]
        doc['type'] = type[1].lower()
        doc['recon'] = data['recon'][0]
        doc['raw_link'] = data['raw_link'][0]
        if 'description' in data and data['description'][0] is not None:
            doc['description'] = data['description'][0]
        doc['uploadedBy'] = flask.session['sessionData']['userName']
        (status, data) = FileUpload().create(doc)
        if status:
            request.files['file'].save(os.path.join(contentDir, id))
            # request.files['sendfile'].save('/tmp/'+dummy)
            return True, id
        else:
            return False, data
            

    def upload(self, id):
        data = dict(request.args) 
        print "*"*100
        if data['fileName'][0]:
            request.files['file'].save(os.path.join(config.mftpath+'/CORP/', data['fileName'][0]))
            zip_ref = zipfile.ZipFile(os.path.join(config.mftpath+'/CORP/', data['fileName'][0]), 'r')
            zip_ref.extractall(config.mftpath+'/CORP/')
            zip_ref.close()
            time.sleep(2)
            os.system('rm %s' % (os.path.join(config.mftpath+'/CORP/', data['fileName'][0])))
            time.sleep(1)
            return True, id
        else:
            return False, data


class ReconAssignment(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        logger.info('coming herer')
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "UserPool already exists."
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def deleteData(self, condition):
        logger.info(condition)
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        logger.info(found)
        # eventDoc = {'type':'ReconAssignment deletion','actionOn':found['userpool'],'createdDate':utilities.getUtcTime(),'createdBy':flask.session["sessionData"]['userName']}
        # Audits().create(eventDoc)
        return super(ReconAssignment, self).delete(condition)

    def getAll(self, query={}):
        skip, limit = 0, 10
        if 'skip' in query:
            skip = query['skip']
            del query['skip']
        if 'limit' in query:
            limit = query['limit'] + skip
            del query['limit']
        stats, resp = super(ReconAssignment, self).getAll(query)
        bizIds = flask.session['sessionData']['businessContextIds']
        df = pandas.DataFrame(resp['data'])
        response = pandas.DataFrame()
        if 'businessContext' in df.columns:
            df['businessContext'] = df['businessContext'].astype(str)
            for bzid in bizIds:
                response = pandas.concat([response, df[df['businessContext'].str.contains(str(bzid), case=False)]],
                                         ignore_index=True)
            response.index = range(1, len(response) + 1)
            resp['total'] = len(response)
            response = response[skip:limit]
            resp['current'] = len(response)
            resp['data'] = response.T.to_dict().values()
        return stats, resp

    def getData(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                start_time = time.time()
                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                iterator = HDF5Interface(query['recon_id'], exec_id, clonePath).fetch_all()
                filter_data = pandas.DataFrame()
                for frame in iterator:
                    filter_data = pandas.concat(
                        [filter_data, frame[(frame['RECORD_STATUS'] == 'ACTIVE') & (frame['ENTRY_TYPE'] == 'T')]],
                        ignore_index=True)
                status, bus_doc = Business().get({"RECON_ID": query['recon_id']})
                json_out = {'fileName': self.export_to_csv(query['recon_id'], bus_doc['RECON_NAME'], filter_data),
                            'txn_count': len(filter_data)}
                logger.info('Export All record generation time ' + str(time.time() - start_time))
                return status, json.dumps(json_out)
            else:
                return True, "[]"

    # export all the records for the selected recon to csv file
    def export_to_csv(self, recon_id, recon_name, recon_data):

        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/' + recon_id + '*.csv'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        to_date = datetime.datetime.now().strftime("%b-%d-%Y")
        export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
        fileName = recon_id + '-' + to_date + '-All.csv'

        # update mdl field values with ui display names
        status, columns = self.getReconColData('', {'recon_id': recon_id})
        if status:
            recon_cols = pandas.read_json(columns)
            recon_data = recon_data.loc[:, recon_cols['MDL_FIELD_ID'].tolist() + config.reportCols]
            recon_cols.index = recon_cols['MDL_FIELD_ID']
            recon_data.index = range(1, len(recon_data) + 1)
            # recon_data = recon_data.applymap(lambda x: str(x).strip())
            # recon_data = recon_data.replace('nan', np.NaN)
            for col in recon_data.columns:
                if not col.startswith('AMOUNT') and 'DATE' not in col:
                    recon_data.loc[recon_data[(~recon_data[col].isnull())].index, col] = "'" + recon_data[col].astype(
                        str)
            recon_data.rename(columns=recon_cols['UI_DISPLAY_NAME'].to_dict(), inplace=True)
        else:
            logger.info("UI Column names not mapped to model field columns, check ??")

        with open(os.path.join(config.contentDir, "recon_reports/" + "/" + object_id + "/" + fileName), 'w') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerows(
                [['Recon_Name :', recon_name], ['Recon_ID', recon_id],
                 ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']], ['Status', 'All'],
                 ['', '']])
            recon_data.to_csv(outcsv, index=False, date_format='%d/%m/%Y')

        return object_id + '/' + fileName

    def addMultiple(self, id, query):
        if query is not None:
            logger.info(query)
            for u in query['user']:
                for b in query['businessContext']:
                    recons = []
                    acPools = []
                    for rec in query['recons']:
                        if b['id'] == rec['business_id']:
                            recons.append(rec['id'])
                    newData = dict()
                    newData['user'] = u['name']
                    newData['businessContext'] = b['id']
                    newData['userpool'] = query['userpool']
                    newData['recons'] = ",".join(recons)
                    if b['id'] == "1111131119":
                        if len(query['accountPools']) > 0:
                            for ac in query['accountPools']:
                                if 'id' in ac:
                                    acPools.append(ac['id'])
                            newData['accountPools'] = ",".join(acPools)
                        else:
                            newData['accountPools'] = ""
                    else:
                        newData['accountPools'] = ""
                    logger.info(newData)
                    (status, data) = self.create(newData)
            return True, ""
        else:
            return True, "No data Found"

    # apply group by on the cash_output_txn data
    def groupByLinkID(self, txn_dataframe):
        if len(txn_dataframe) > 0 and txn_dataframe is not None:
            df = pandas.DataFrame()
            # To avoid [Python int too large to convert to C long] error
            df["LINK_ID"] = txn_dataframe["LINK_ID"].astype("str")
            grouperDataFrame = df.groupby("LINK_ID", as_index=False)

            # count of link_Id's (link_id, count)
            df_cash_aggr = pandas.DataFrame({'count': grouperDataFrame.size()}).reset_index()
            reconData_merge = txn_dataframe.merge(df_cash_aggr, on="LINK_ID")

            return reconData_merge
        else:
            return pandas.DataFrame()

    def convert_to_json(self, data_frame):
        json_out = dict()
        json_out['txn_count'] = len(data_frame)
        if not data_frame.empty:
            # count aggregate by link_id
            data_frame = self.groupByLinkID(data_frame)
            data_frame.loc[:, "SRC_COUNT"] = len(data_frame["SOURCE_TYPE"].unique())
            reconData = data_frame.replace('None', '')
            for i in reconData.columns:
                if reconData[i].dtype == np.float64 and i != 'AMOUNT':
                    reconData = reconData.round({i: 2})
            json_out['data'] = reconData.to_json(orient='records')
        else:
            json_out['data'] = '[]'
        return json_out

    # Method to check if data exists for selected recon or not
    def check_data_exists(self, reconID):
        exec_id = ReconExecutionDetailsLog().getLatestExeID(reconID)
        if exec_id == '':
            return False, ''
        else:
            return True, exec_id

    def getReconUnmatched(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                logger.info(repr(jobsList) + '----------->' + query['recon_id'])
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}
                if not Hdf5ZfsUtil().sync_read_snap_shot(query['recon_id'], exec_id):
                    logger.info("User may be viewing stale data, check why snap shot not updated ??")
                # clonePath = self.init_zfs_clone(query['recon_id'])
                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                (status_rec, df) = HDF5Interface(query['recon_id'], exec_id, clonePath).read_col_query_unmatched()
                # ZFSApi().destroyClone(clonePath)
                json_out = self.convert_to_json(df)
                return status_rec, json_out
            else:
                return True, {'txn_count': 0, 'data': []}
        else:
            return False, "No Data Found."

    def getReconMatched(self, id, query):
        if query is not None:

            exportFlag = False
            if 'exportData' in query and query['exportData']:
                exportFlag = True
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                query['recon_execution_id'] = exec_id
                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}

                if not Hdf5ZfsUtil().sync_read_snap_shot(query['recon_id'], exec_id):
                    logger.info("User may be viewing stale data, check why snap shot not updated ??")

                # clonePath = self.init_zfs_clone(query['recon_id'])
                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                print clonePath, exec_id, query['recon_id']
                (status_rec, reconData) = HDF5Interface(query['recon_id'],
                                                        exec_id, clonePath).read_col_query_matched(query['recon_id'],
                                                                                                   exec_id,
                                                                                                   skip=skipFrom,
                                                                                                   limit=limitTo)
                # ZFSApi().destroyClone(clonePath)
                recExecDoc = list(
                    ReconExecutionDetailsLog().find(
                        {'RECON_ID': query['recon_id'], 'EXECUTION_STATUS': 'Completed',
                         'PROCESSING_STATE_STATUS': 'Matching Completed',
                         'RECORD_STATUS': 'ACTIVE', 'RECON_EXECUTION_ID': exec_id
                         }))
                totalMatchedItems = 0
                if len(recExecDoc) > 0:
                    totalMatchedItems = int(recExecDoc[0]['PERFECT_MATCHED_RECORDS_COUNT']) + int(
                        recExecDoc[0]['FORCE_MATCHED'])
                json_out = dict()
                if len(reconData) > config.threshold:
                    status, business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], reconData)
                    json_out['txn_count'] = len(reconData)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                    return True, json_out
                else:
                    json_out = self.convert_to_json(reconData)
                    json_out['ignore_load'] = False
                    json_out['totalMatchedCount'] = totalMatchedItems
                    return status_rec, json_out
            else:
                return True, {'txn_count': 0, 'data': [], 'totalMatchedCount': 0}
        else:
            return False, "No Data Found."

    def getReconRollBack(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}

                # clonePath = self.init_zfs_clone(query['recon_id'])
                if not Hdf5ZfsUtil().sync_read_snap_shot(query['recon_id'], exec_id):
                    logger.info("User may be viewing stale data, check why snap shot not updated ??")

                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                (status_rec, reconData) = HDF5Interface(query['recon_id'], exec_id, clonePath).read_col_query_rollback()
                # ZFSApi().destroyClone(clonePath)
                json_out = self.convert_to_json(reconData)
                return True, json_out
            else:
                return True, {'txn_count': 0, 'data': []}
        else:
            return False, "No Data Found."

    def getReconPendingForSubmission(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}
                query['recon_execution_id'] = exec_id
                (status_rec, reconData) = HDF5Interface(query['recon_id'], exec_id).read_col_query_pending(query)
                json_out = self.convert_to_json(reconData)
                return True, json_out
            else:
                return True, {'txn_count': 0, 'data': []}
        else:
            return False, "No Data Found."

    def getReconWaitingForAuthorization(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}

                if not Hdf5ZfsUtil().sync_read_snap_shot(query['recon_id'], exec_id):
                    logger.info("User may be viewing stale data, check why snap shot not updated ??")

                # clonePath = self.init_zfs_clone(query['recon_id'])
                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                (status, reconData) = HDF5Interface(query['recon_id'], exec_id,
                                                    clonePath).read_col_query_waitingForAuthorization(
                    query['userRole'])
                # ZFSApi().destroyClone(clonePath)
                json_out = self.convert_to_json(reconData)
                return True, json_out
            else:
                return True, {'txn_count': 0, 'data': []}
        else:
            return False, "No Data Found."

    def getAuditTrail(self, id, query):
        if query is not None:
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}

                if not Hdf5ZfsUtil().sync_read_snap_shot(query['recon_id'], exec_id):
                    logger.info("User may be viewing stale data, check why snap shot not updated ??")

                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                (status_rec, df) = HDF5Interface(query['recon_id'], exec_id, clonePath).read_col_query_auditTrail()

                json_out = self.convert_to_json(reconData)
                return True, json_out
            else:
                return True, {'txn_count': 0, 'data': []}
        else:
            return False, "No Data Found."

    def getAllocationDetails(self, id, query):
        pass
        # if query is not None:
        #     (status,recon_con) = sql.getConnection()
        #     if status:
        #         (status_rec, reconData) = sql.read_col_query_allocation_history(query= query, connection= recon_con)
        #         if status_rec:
        #             json_out = reconData.to_json(orient='records')
        #             return True,json_out
        #         else:
        #             return True, "[]"
        #     else:
        #         return False, "No Data Found."
        # else:
        #     return False, "No Data Found."

    def generateAdfReport(self, id):
        with open("hello.csv", 'a') as csvfile:
            csv_file = csv.writer(csvfile, delimiter=';')
            to_date = datetime.datetime.now().strftime("%d/%m/%Y")

            column_order = ["ACCOUNT_NO", "ACCOUNT_CCY", "BANK_TYPE", "DEBIT_CREDIT_FLAG", "TXN_AMT",
                            "PENDING_SINCE", "LOSS_PROVISION_AMT", "GL_NO", "SOURCE_SYS", "Effective_from_Dttm",
                            "Effective_To_Dttm",
                            "EXTRACT_DATE", "BATCH_ID", "BATCH_DATE"]

            adf_report_query = "select trunc(statement_date) as Extract_date,account_number as ACCOUNT_NO,currency as ACCOUNT_CCY ,debit_credit_indicator as DEBIT_CREDIT_FLAG, amount as TXN_AMT from cash_output_txn " \
                               "where record_status = 'ACTIVE' and entry_type = 'T' and matching_status = 'UNMATCHED' and recon_id = 'TRSC_CASH_APAC_20181'"

            adf_report_df = pandas.read_sql(adf_report_query, sql.getConnection()[1])

            for i in range(0, len(column_order)):
                # add static columns
                if column_order[i] not in adf_report_df:
                    adf_report_df[column_order[i]] = np.nan

            adf_report_df["BANK_TYPE"] = "I"
            adf_report_df["SOURCE_SYS"] = "ERECON"
            # calculating ageing
            adf_report_df['PENDING_SINCE'] = (
                datetime.datetime.now().date() - adf_report_df['EXTRACT_DATE']).datetime.days
            # re-ordering columns
            adf_report_df = adf_report_df[column_order]
            record_count = len(adf_report_df.index)

            data = [["ADF Report"], ['REPORT_DATE', to_date], ["TXN_COUNT", record_count]]
            csv_file.writerows(data)
            adf_report_df.to_csv(csvfile, index=False)

    def getReconColData(self, id, query):
        if query is not None:
            status, data = ReconDynamicDataModel().getAll({"RECON_ID": query['recon_id']})

            if data['total'] > 0:
                df = pandas.DataFrame(data['data'])
                df = df.loc[df[df['RECON_ID'] == query['recon_id']].index]
                if 'COL_STATUS' in df.columns:
                    df = df.loc[df[df['COL_STATUS'] == 'ACTIVE'].index]
                df = df[['MDL_FIELD_ID', 'UI_DISPLAY_NAME', 'ORDER_SEQ_NO', 'MANDATORY', 'MDL_FIELD_DATA_TYPE']]
                df.loc[df['MDL_FIELD_DATA_TYPE'] == 'np.datetime64', 'MDL_FIELD_DATA_TYPE'] = 'DATE'

                df = pandas.concat([df, pandas.DataFrame(
                    {'MDL_FIELD_ID': ['SOURCE_NAME'], 'UI_DISPLAY_NAME': ['SOURCE_NAME'], 'ORDER_SEQ_NO': [0],
                     'MANDATORY': 'M', 'MDL_FIELD_DATA_TYPE': ['str']})], ignore_index=True)

                df = df.sort_values(by=['ORDER_SEQ_NO']).reset_index()
                json_out = df.to_json(orient='records')
                return True, json_out
            else:
                return False, "Recon Column not configured"
        else:
            return False, ''

    def validateTransaction(self, selectedTxn):
        logger.info("Validation initiated ..")

        # Added gl_account_number as a part of custom validation changes
        df_cash = pandas.read_json(json.dumps(selectedTxn, default=json_util.default), orient='columns',
                                   dtype={'GL_ACCOUNT_NUMBER': 'str'})

        # df_validate = df_cash.loc[:,
        #              ['ACCOUNT_NUMBER', 'CURRENCY', 'AMOUNT', 'DEBIT_CREDIT_INDICATOR', 'SOURCE_TYPE', 'RECON_ID']]
        reconID = df_cash['RECON_ID'].iloc[0]

        validation_msg = "Unable to forcematch records "
        validation_status = True

        # Currency Validation
        if reconID in config.currency_validation and 'CURRENCY' in df_cash.columns:
            if len(pandas.unique(df_cash.loc[:, "CURRENCY"])) > 1:
                logger.info("mismatch in currency")
                validation_status = False
                validation_msg += ",mismatch in currency"
                # return False,"Could not forcematch the records due to mismatch in currency"

        # Amount Validation (Need to be upgraded)
        # checking for number of sources
        # incase of three src recons (Vald Rule --> {S1 - S2} == {S1 - S3} == 0)

        if 'AMOUNT' in df_cash.columns:
            if 'DEBIT_CREDIT_INDICATOR' in df_cash.columns:
                df_cash.loc[df_cash['DEBIT_CREDIT_INDICATOR'].isnull(), 'DEBIT_CREDIT_INDICATOR'] = ''
            else:
                df_cash['DEBIT_CREDIT_INDICATOR'] = ''

            pivoted_frame = pandas.pivot_table(df_cash, columns=['SOURCE_TYPE', 'DEBIT_CREDIT_INDICATOR'],
                                               values=['AMOUNT'], aggfunc={"AMOUNT": np.sum})
            pivoted_frame = pivoted_frame.unstack(level=2)
            logger.info(pivoted_frame)
            pivoted_frame.columns = [''.join(list(col)) for col in pivoted_frame.columns.values]
            pivoted_frame.reset_index().drop(axis=1, labels="level_0")
            logger.info(pivoted_frame)
            if reconID not in config.three_src_recon:
                # pivoted_frame = pivoted_frame.unstack(level=2)
                # pivoted_frame.columns = [''.join(list(col)) for col in pivoted_frame.columns.values]
                # pivoted_frame.reset_index().drop(axis=1, labels="level_0")
                pivoted_frame['OutStanding_Amount'] = 0
                pivoted_frame = pivoted_frame.fillna(0)

                if 'D' in pivoted_frame.columns and 'C' in pivoted_frame.columns:
                    pivoted_frame["OutStanding_Amount"] = (-pivoted_frame.loc[:, 'D']) + pivoted_frame.loc[:, 'C']
                    pivoted_frame = pivoted_frame.reset_index()
                    diff_amount = pivoted_frame.loc[:, "OutStanding_Amount"].sum()
                else:
                    tranposed_frame = pivoted_frame.T
                    # the above transpose return multi-level index, get required index level
                    tranposed_frame.columns = tranposed_frame.columns.get_level_values(1)
                    diff_amount = tranposed_frame.loc[:, "S1"].sum() - tranposed_frame.loc[:, "S2"].sum()

            else:
                tranposed_frame = pivoted_frame.T
                tranposed_frame.columns = tranposed_frame.columns.get_level_values(1)
                diff_amount = ((tranposed_frame["S1"].sum() - tranposed_frame["S2"].sum()) + (
                    tranposed_frame["S1"].sum() - tranposed_frame["S3"].sum()))
                logger.info(diff_amount)

            if round(diff_amount, 2) != 0 or round(diff_amount, 2) != 0.0:
                logger.info("could not forcematch records due to mismatch in amount")
                validation_status = False
                validation_msg += ",outstanding amount is not zero"
                # return False, "could not forcematch records, due to mismatch in amount."

        # Account number validation
        if reconID in config.account_number_validation:
            logger.info("Before ")
            logger.info(df_cash["ACCOUNT_NUMBER"])
            df_cash["ACCOUNT_NUMBER"] = df_cash["ACCOUNT_NUMBER"].apply(lambda x: str(x).lstrip("0"))
            logger.info("After")
            logger.info(df_cash["ACCOUNT_NUMBER"])
            if len(pandas.unique((df_cash.loc[:, "ACCOUNT_NUMBER"]))) > 1:
                logger.info("mismatch in account number")
                validation_status = False
                validation_msg += ",found different Account Numbers in selected transactions"
                # return False, "could not forcematch records, due to mismatch in account numbers."
                # if len(pandas.unique(str(df_validate.loc[:, "ACCOUNT_NUMBER"]).lstrip("0"))) > 1:
                #     logger.info("mismatch in account number")
                #     return False, "could not forcematch records, due to mismatch in account numbers."

        # check for custom validation if exists
        if reconID in config.customValidation:
            # Get custom validation function

            # Return True if validation fails else False
            funcName = config.customValidation[reconID]
            status, msg = eval('CustomValidation().' + funcName + '(df_cash)')
            if status:
                validation_status = False
                validation_msg += msg

        return validation_status, "Success" if validation_status else validation_msg

    # validation to check for partial records in the selected queue
    def checkForPartialTxnSelection(self, selectedTxn):

        # To avoid [Python int too large to convert to C long] error
        part_match_frame = pandas.read_json(json.dumps(selectedTxn, default=json_util.default), orient='columns',
                                            dtype={'LINK_ID': 'int64'})
        part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
        df_cash_aggr = pandas.DataFrame({'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()
        reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
        reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]

        return True if np.count_nonzero(reconData_merge["count_res"]) > 0 else False

    def parse_dates(self, value):
        if value != value or value is None:
            return np.nan
        elif isinstance(value, pandas.tslib.Timestamp):
            return value
        elif isinstance(value, (np.int64, np.float64, int, float)):
            return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(value / 1000))
        else:
            return parse(value).strftime("%Y-%m-%d %H:%M:%S")

    def update_audit_data(self, data=pandas.DataFrame(), logged_user=''):
        data.loc[:, 'CREATED_BY'] = str(logged_user)
        data.loc[:, 'CREATED_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        data.loc[:, "RECORD_VERSION"] = data["RECORD_VERSION"].astype(int) + 1
        data.loc[:, 'RECORD_STATUS'] = "ACTIVE"

    # Method to inactivate the transactions based on the specific column and values
    def inactive_txn(self, dataframe=None, column='OBJECT_OID', values=[], logged_user=''):
        mask = dataframe[column].isin(values)
        dataframe.loc[mask, 'RECORD_STATUS'] = 'INACTIVE'
        dataframe.loc[mask, 'UPDATED_BY'] = str(logged_user)
        dataframe.loc[mask, 'UPDATED_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Method to operations to the queue
    def addToQueue(self, id, query):

        if 'selected_rec' in query and len(query['selected_rec']) == 0:
            return True, 'Please select the transactions to proceed.'
        elif 'operationType' not in query:
            return True, 'Operation type not defined for the selected operation'
        elif query['operationType'] == 'SUBMIT_FOR_AUTH':
            fun_name = 'submitForAuthorization'
            job_type = 'Manual ForceMatch Transactions'
        elif query['operationType'] == 'AUTH_RECORD':
            fun_name = 'authorizeRecords'
            job_type = 'Authorize ForceMatch Transactions'
        elif query['operationType'] == 'REJ_RECORDS':
            fun_name = 'rejectRecords'
            job_type = 'Reject ForceMatch Transactions'
        elif query['operationType'] == 'ROLLBACK_RECORDS':
            fun_name = 'rollBack'
            job_type = 'Rollback Matched Transactions'
        elif query['operationType'] == 'ROLLBACK_AUTH':
            fun_name = 'authorizeRollBackRecords'
            job_type = 'Auth Rollback Transactions'
        elif query['operationType'] == 'ROLLBACK_REJ':
            fun_name = 'rejectRollBackRecords'
            job_type = 'Reject Rollback Transactions'
        elif query['operationType'] == 'UNGROUP':
            fun_name = 'unGrouping'
            job_type = 'Ungrouping Transactions'
        else:
            return True, "Invalid operation, operation not defined."

        # Check if partial records exists in the selected queue, if exists abort and raise
        if self.checkForPartialTxnSelection(query['selected_rec']):
            return True, 'Partial records found in the selected transaction queue.'

        # apply standard validations on the selected transactions if not a single side match
        if query['operationType'] == 'SUBMIT_FOR_AUTH' and not (query['singleSideMatch']):
            (status, valdation_msg) = self.validateTransaction(query['selected_rec'])
            if not status:
                return True, valdation_msg
            else:
                pass

        # Unique ID to identify the job
        unique_id = str(uuid.uuid4()).replace('-', '_')
        query['logged_user'] = flask.session["sessionData"]['userName']
        query['uuid'] = unique_id
        query['fun_name'] = fun_name
        # job_id = ReconWorker().submitJob(fun_name, query)
        if upstat.checkUpstartJobStatus(query['recon_id']):
            job_id = ReconWorker(str(query['recon_id'])).submitJob(fun_name, query)
        else:
            upstat.startUpstartJob('reconwatcher RECONID=%s' % str(query['recon_id']))
            job_id = ReconWorker(str(query['recon_id'])).submitJob(fun_name, query)

        logger.info('Adding job to queue for user %s' % str(flask.session["sessionData"]['userName']))
        logger.info('JOB ID %s' % str(job_id))
        # update job_post collection
        doc = dict()
        status, business_doc = Business().get({'RECON_ID': query['recon_id']})

        doc['job_id'] = job_id
        doc['uuid'] = unique_id
        doc['logged_user'] = flask.session["sessionData"]['userName']
        doc['job_msg'] = query['comments']
        doc['job_status'] = 'INPROGRESS'
        doc['recon_execution_id'] = ReconExecutionDetailsLog().getLatestExeID(query['recon_id'])
        doc['recon_id'] = query['recon_id']
        doc['recon_name'] = business_doc['RECON_NAME']
        doc['work_pool_name'] = business_doc['WORKPOOL_NAME']
        doc['created_date'] = datetime.datetime.now().strftime('%d/%m/%Y %H:%M %p')
        ReconJobPost().create(doc)

        # waiting
        waiting_status = True
        while (waiting_status):
            status, data = ReconJobPost().get({'job_id': job_id})
            if data['job_status'] == 'INPROGRESS':
                time.sleep(2)
                continue
            elif data['job_status'] == 'SUCCESS':
                waiting_status = False
                return True, 'Success'
            elif data['job_status'] == 'FAILURE':
                waiting_status = False
                msg = 'Internal Error, Contact Admin'
                # check for error message in reconJobPost collection
                if 'err_msg' in data and data['err_msg'] != '':
                    msg = data['err_msg']
                return True, msg
            else:
                pass
        logger.info('addtoqueue exit !!')

        return True, 'Success'

    def update_stale_data(self, uuid):
        status, job_post = ReconJobPost().get({'uuid': uuid})
        job_post['job_status'] = 'FAILURE'
        job_post['err_msg'] = "Stale data found in the selected queue, please refresh browser tab."
        ReconJobPost().modify(job_post['_id'], job_post)

    def updateReportData(self, reconId='', exeId=''):
        start_time = datetime.datetime.now()
        reports = DashBrdReports(reconId, exeId)
        reports.genDashBoards()
        logger.info('Dash Board Summary details updated in %s' % str(datetime.datetime.now() - start_time))

    def submitForAuthorization(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'OBJECT_OID': 'str', 'LINK_ID': 'str'})

            obj_id = sel_rec_df['OBJECT_OID'].unique()

            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf5Interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            auth_txn = dict()
            auth_txn['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
            auth_txn['TXN_MATCHING_STATUS'] = "AUTHORIZATION_PENDING"
            auth_txn['TXN_MATCHING_STATE'] = "SINGLE_SOURCE_WAITING_FOR_AUTH" \
                if 'singleSideMatch' in query and query['singleSideMatch'] else "WAITING_FOR_AUTHORIZATION"
            auth_txn['COMMENTS'] = str(query['comments'])
            auth_txn['AUTHORIZED_BY'] = str(query['authorizer'])
            auth_txn['logged_user'] = query['logged_user']
            auth_txn['column'] = 'OBJECT_OID'
            auth_txn['values'] = obj_id
            auth_txn['status'] = 'UNMATCHED'

            upd_cnt = hdf_interface.update_txn_status(query=auth_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['EXCEPTION_RECORDS_COUNT'] = int(doc['EXCEPTION_RECORDS_COUNT']) - len(sel_rec_df)
            doc['AUTHORIZATION_PENDING'] = int(doc['AUTHORIZATION_PENDING']) + len(sel_rec_df)
            ReconExecutionDetailsLog().modify(doc['_id'], doc)
            return True, 'Success'
        else:
            return True, 'No data found to process'

    def unGroupingColumns(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'OBJECT_OID': 'str', 'LINK_ID': 'str'})
            link_id = sel_rec_df['LINK_ID'].unique()
            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf5Interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            ungrp_txn = dict()
            ungrp_txn['TXN_MATCHING_STATUS'] = "UNMATCHED"
            ungrp_txn['TXN_MATCHING_STATE'] = "UNGROUPED"
            ungrp_txn['logged_user'] = query['logged_user']
            ungrp_txn['COMMENTS'] = query['comments']
            ungrp_txn['column'] = 'LINK_ID'
            ungrp_txn['values'] = link_id
            ungrp_txn['status'] = 'UNMATCHED'

            upd_cnt = hdf_interface.update_txn_status(query=ungrp_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            return True, 'Success'
        else:
            return True, 'No data found to process'

    def authorizeRecords(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'OBJECT_OID': 'str', 'LINK_ID': 'str'})
            link_ids = sel_rec_df['LINK_ID'].unique()

            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf5Interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            auth_txn = dict()
            auth_txn['TXN_MATCHING_STATUS'] = "MATCHED"
            auth_txn['COMMENTS'] = str(query['comments'])
            auth_txn['logged_user'] = query['logged_user']
            auth_txn['column'] = 'LINK_ID'
            auth_txn['values'] = link_ids
            auth_txn['status'] = 'AUTHORIZATION_PENDING'

            upd_cnt = hdf_interface.update_txn_status(query=auth_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['AUTHORIZATION_PENDING'] = int(doc['AUTHORIZATION_PENDING']) - len(sel_rec_df)
            doc['FORCE_MATCHED'] = int(doc['FORCE_MATCHED']) + len(sel_rec_df)
            ReconExecutionDetailsLog().modify(doc['_id'], doc)
            return True, 'Success'
        else:
            return False, "No records found."

    def rejectRecords(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'OBJECT_OID': 'str', 'LINK_ID': 'str'})
            link_ids = sel_rec_df['LINK_ID'].unique()
            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            reject_txn = dict()
            reject_txn['TXN_MATCHING_STATUS'] = "UNMATCHED"
            reject_txn['TXN_MATCHING_STATE'] = "AUTHORIZATION_REJECTED"
            reject_txn['COMMENTS'] = str(query['comments'])
            reject_txn['logged_user'] = query['logged_user']
            reject_txn['column'] = 'LINK_ID'
            reject_txn['values'] = link_ids
            reject_txn['status'] = 'AUTHORIZATION_PENDING'

            upd_cnt = hdf_interface.update_txn_status(query=reject_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['AUTHORIZATION_PENDING'] = int(doc['AUTHORIZATION_PENDING']) - upd_cnt
            doc['EXCEPTION_RECORDS_COUNT'] = int(doc['EXCEPTION_RECORDS_COUNT']) + upd_cnt
            ReconExecutionDetailsLog().modify(doc['_id'], doc)

            return True, 'Success'
        else:
            return False, "No Records Found."

    def rollBack(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:

            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'OBJECT_OID': 'str', 'LINK_ID': 'str'})

            link_ids = sel_rec_df['LINK_ID'].unique()
            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf5_interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            # Compute counts to update the recon summary
            fm_txn_count = len(sel_rec_df[sel_rec_df['TXN_MATCHING_STATE'].isin(
                ['FORCE_MATCHED', 'SINGLE_SOURCE_AUTHORIZED', 'ROLLBACK_REJECTED'])])
            am_txn_count = len(sel_rec_df[sel_rec_df['TXN_MATCHING_STATE'] == 'SYSTEM_MATCHED'])

            rollback_txn = dict()
            rollback_txn['TXN_MATCHING_STATUS'] = "ROLLBACK_AUTHORIZATION_PENDING"
            rollback_txn['TXN_MATCHING_STATE'] = None
            rollback_txn["COMMENTS"] = str(query["comments"])
            rollback_txn['AUTHORIZED_BY'] = str(query['authorizer'])
            rollback_txn['logged_user'] = query['logged_user']
            rollback_txn['column'] = 'LINK_ID'
            rollback_txn['values'] = link_ids
            rollback_txn['status'] = 'MATCHED'

            upd_cnt = hdf_interface.update_txn_status(query=rollback_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['ROLLBACK_AUTHORIZATION_PENDING'] = int(doc['ROLLBACK_AUTHORIZATION_PENDING']) + len(sel_rec_df)
            doc['FORCE_MATCHED'] = int(doc['FORCE_MATCHED']) - fm_txn_count
            doc['PERFECT_MATCHED_RECORDS_COUNT'] = int(doc['PERFECT_MATCHED_RECORDS_COUNT']) - am_txn_count
            ReconExecutionDetailsLog().modify(doc['_id'], doc)

            return True, 'Success'
        else:
            return False, "No Records Found."

    def authorizeRollBackRecords(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'LINK_ID': 'str'})
            link_ids = sel_rec_df['LINK_ID'].unique()
            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            auth_rollback_txn = dict()
            auth_rollback_txn['TXN_MATCHING_STATUS'] = "UNMATCHED"
            auth_rollback_txn['TXN_MATCHING_STATE'] = "ROLLBACK_AUTHORIZED"
            auth_rollback_txn['COMMENTS'] = str(query['comments'])
            auth_rollback_txn['logged_user'] = str(query['logged_user'])
            auth_rollback_txn['column'] = 'LINK_ID'
            auth_rollback_txn['values'] = link_ids
            auth_rollback_txn['status'] = 'ROLLBACK_AUTHORIZATION_PENDING'

            upd_cnt = hdf_interface.update_txn_status(auth_rollback_txn)
            logger.info('selected record count %s' % str(len(sel_rec_df)))
            logger.info('updated record count %s' % str(upd_cnt))

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['ROLLBACK_AUTHORIZATION_PENDING'] = int(doc['ROLLBACK_AUTHORIZATION_PENDING']) - upd_cnt
            doc['EXCEPTION_RECORDS_COUNT'] = int(doc['EXCEPTION_RECORDS_COUNT']) + upd_cnt
            ReconExecutionDetailsLog().modify(doc['_id'], doc)

            return True, 'Success'
        else:
            return False, 'No records Found.'

    def rejectRollBackRecords(self, id, query):
        cloned_val = query['selected_rec']

        if query is not None and query['selected_rec'] is not None:
            sel_rec_df = pandas.read_json(json.dumps(query['selected_rec'], default=json_util.default),
                                          orient='columns',
                                          dtype={'LINK_ID': 'str'})
            link_ids = sel_rec_df['LINK_ID'].unique()
            recon_id = sel_rec_df.iloc[0]['RECON_ID']
            exec_id = ReconExecutionDetailsLog().getLatestExeID(recon_id)

            # init hdf interface
            store_path = config.zfsBase + str(recon_id) + '/' + str(exec_id)
            hdf_interface = HDF5Interface(recon_id, exec_id, store_path)

            rej_rollback_txn = dict()
            rej_rollback_txn['TXN_MATCHING_STATUS'] = "MATCHED"
            rej_rollback_txn['TXN_MATCHING_STATE'] = None
            rej_rollback_txn["COMMENTS"] = str(query["comments"])
            rej_rollback_txn['logged_user'] = str(query['logged_user'])
            rej_rollback_txn['column'] = 'LINK_ID'
            rej_rollback_txn['values'] = link_ids
            rej_rollback_txn['status'] = 'ROLLBACK_AUTHORIZATION_PENDING'

            upd_cnt = hdf_interface.update_txn_status(query=rej_rollback_txn)

            if len(sel_rec_df) != upd_cnt:
                self.update_stale_data(query['uuid'])
                return False, 'Failure'

            self.updateReportData(recon_id, exec_id)

            # get forceMatched count & autoMatched count, to update recon summary
            fm_txn_count = len(sel_rec_df[sel_rec_df['TXN_MATCHING_STATE'].isin(
                ['FORCE_MATCHED', 'SINGLE_SOURCE_AUTHORIZED', 'ROLLBACK_REJECTED'])])
            am_txn_count = len(sel_rec_df[sel_rec_df['TXN_MATCHING_STATE'] == 'SYSTEM_MATCHED'])

            # update recon_execution_details_log collection count for recon summary
            status, doc = ReconExecutionDetailsLog().get({"RECON_EXECUTION_ID": str(exec_id), "RECORD_STATUS": "ACTIVE",
                                                          "PROCESSING_STATE_STATUS": "Matching Completed"})
            doc['ROLLBACK_AUTHORIZATION_PENDING'] = int(doc['ROLLBACK_AUTHORIZATION_PENDING']) - upd_cnt
            doc['FORCE_MATCHED'] = int(doc['FORCE_MATCHED']) + fm_txn_count
            doc['PERFECT_MATCHED_RECORDS_COUNT'] = int(doc['PERFECT_MATCHED_RECORDS_COUNT']) + am_txn_count
            ReconExecutionDetailsLog().modify(doc['_id'], doc)
            return True, 'Success'
        else:
            return False, 'No records Found.'

    def submitForInvestigator(self, id, query):
        # print query
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # validation to avoid partial selection of transactions
            # if self.checkForPartialTxnSelection(cloned_val):
            #     return True,"Partial records found in the selected queue."

            # Get Unique transactions from the selected list
            values = set()
            for items in cloned_val:
                values.add(items["LINK_ID"])
            unique_txndata = dict()
            unique_txndata["LINK_ID"] = values

            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]

            (status_exception, exmaster_data) = sql.getExceptionMasDetails(query=unique_txndata, connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = json.loads(exmaster_data.to_json(orient='records'))
            (status_allocation, exallocation_data) = sql.getExceptionAlloDetails(query=unique_txndata,
                                                                                 connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = json.loads(exallocation_data.to_json(orient='records'))

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)

                for rec_old in cloned_val:
                    # if 'format' in flask.session['sessionData']:
                    #     for format_type in flask.session['sessionData']['format']:
                    #         for k, v in format_type.items():
                    #             for key, val in rec_old.items():
                    #                 if k == key and (v == "DATE" or v == "TIMESTAMP"):
                    #                     if val is not None:
                    #                         print "key" + str(key)
                    #                         # print "val" + str(val)
                    #                         if type(rec_old[key]) != str:
                    #                             rec_old[key] = datetime.datetime.fromtimestamp(
                    #                                 (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                    #                         else:
                    #                             rec_old[key] = rec_old[key]
                    #                         print rec_old[key]
                    # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "UNMATCHED_INVESTIGATION_PENDING"
                    # rec_old['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # rec_old["COMMENTS"] = query['comments']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    # if (rec_old["STATEMENT_DATE"] is not None) and (type(rec_old["STATEMENT_DATE"]) == int) and (rec_old["STATEMENT_DATE"] is not 0):
                    #     logger.info(type(rec_old["STATEMENT_DATE"]))
                    #     rec_old['STATEMENT_DATE'] = datetime.datetime.fromtimestamp(rec_old['STATEMENT_DATE'] / 1000).strftime("%Y-%m-%d %H:%M:%S")
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 24
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED_INVESTIGATION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "UNMATCHED_INVESTIGATION_PENDING"
                    # authorization_by
                    rec_old["AUTHORIZATION_BY"] = query["deptUserPool"]
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["FORCEMATCH_AUTHORIZATION"] = None
                    rec_old["AUTHORIZATION_STATUS"] = "SYSTEM_AUTHORIZED"

                    rec_old["RECONCILIATION_STATUS"] = "EXCEPTION"
                    rec_old["MATCHING_STATUS"] = "UNMATCHED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    # utilized only for dept user management
                    rec_old["ACTION_BY"] = flask.session["sessionData"]['userName']

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                        # print json.dumps(oldData)
                        # print pandas.read_json(json.dumps(oldData),orient='records')
                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                for r in exmaster_data:
                    upd_exmaster_data = dict()
                    # if type(exmaster_data) == dict:
                    #    exmaster_data = exmaster_data
                    # else:
                    #    exmaster_data = json.loads(exmaster_data)[0]
                    # exmaster_data = json.loads(exmaster_data)[0]
                    upd_exmaster_data["COMMENTS"] = query['comments']
                    upd_exmaster_data["LINK_ID"] = r["LINK_ID"]
                    upd_exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exmaster_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                   '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                             '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # logger.info(exmaster_data['EXCEPTION_OID'])
                    upd_exmaster_data["EXCEPTION_ID"] = r["EXCEPTION_ID"]
                    upd_exmaster_data["EXCEPTION_STATUS"] = "INPROGESS"
                    upd_exmaster_data["IPADDRESS"] = "192.168.2.248"
                    upd_exmaster_data["REASON_CODE"] = query['reason_code']
                    upd_exmaster_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exmaster_data["RECORD_VERSION"] = r['RECORD_VERSION'] + 1
                    upd_exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    upd_exmaster_data["BUSINESS_CONTEXT_ID"] = r['BUSINESS_CONTEXT_ID']
                    upd_exmaster_data["RECON_EXECUTION_ID"] = r['RECON_EXECUTION_ID']
                    upd_exmaster_data["RECON_ID"] = r["RECON_ID"]
                    upd_exmaster_data["PRODUCTLINE_ID"] = r["PRODUCTLINE_ID"]
                    upd_exmaster_data["BUSINESS_PROCESS_ID"] = r["BUSINESS_PROCESS_ID"]
                    upd_exmaster_data["ASSET_CLASS_ID"] = r["ASSET_CLASS_ID"]
                    oldData_ex.append(upd_exmaster_data)

                    logger.info(oldData_ex)
                parseDates = ["EXCEPTION_CREATED_DATE", "CLEARING_DATE", "CREATED_DATE", "TRADE_DATE", "UPDATED_DATE",
                              "RECORD_END_DATE", "EXCEPTION_COMPLETION_DATE", "EXCEPTION_PROCESSING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                                 table_name="exception_master", connection=recon_con)

                for i in exallocation_data:
                    upd_exallocation_data = dict()
                    # exallocation_data = json.loads(exallocation_data)[0]
                    upd_exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                    upd_exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exallocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                       '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["EXCEPTION_ALLOCATION_ID"] = i["EXCEPTION_ALLOCATION_ID"]
                    upd_exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    upd_exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                    upd_exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                    upd_exallocation_data["IPADDRESS"] = "192.168.2.248"
                    upd_exallocation_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exallocation_data["RECORD_VERSION"] = i["RECORD_VERSION"] + 1
                    upd_exallocation_data["EXCEPTION_ID"] = i["EXCEPTION_ID"]
                    upd_exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    upd_exallocation_data["RECON_ID"] = i["RECON_ID"]
                    upd_exallocation_data["BUSINESS_CONTEXT_ID"] = i["BUSINESS_CONTEXT_ID"]
                    upd_exallocation_data["ALLOCATION_STATUS"] = i["ALLOCATION_STATUS"]
                    upd_exallocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                          '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["ALLOCATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exallocation_data["ACTION_BY"] = query["deptUserPool"]
                    upd_exallocation_data["ALLOCATION_HISTORY_FLAG"] = "Y"

                    oldData_allocation.append(upd_exallocation_data)

                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                                       table_name="exception_allocation",
                                                                       connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records to found"

    def investigateRecords(self, id, query):
        # print query
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # validation to avoid partial selection of transactions
            # if self.checkForPartialTxnSelection(cloned_val):
            #     return True,"Partial records found in the selected queue."

            # Get Unique transactions from the selected list
            values = set()
            for items in cloned_val:
                values.add(items["LINK_ID"])
            unique_txndata = dict()
            unique_txndata["LINK_ID"] = values

            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]

            (status_exception, exmaster_data) = sql.getExceptionMasDetails(query=unique_txndata, connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = json.loads(exmaster_data.to_json(orient='records'))
            (status_allocation, exallocation_data) = sql.getExceptionAlloDetails(query=unique_txndata,
                                                                                 connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = json.loads(exallocation_data.to_json(orient='records'))

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)

                for rec_old in cloned_val:
                    # if 'format' in flask.session['sessionData']:
                    #     for format_type in flask.session['sessionData']['format']:
                    #         for k, v in format_type.items():
                    #             for key, val in rec_old.items():
                    #                 if k == key and (v == "DATE" or v == "TIMESTAMP"):
                    #                     if val is not None:
                    #                         print "key" + str(key)
                    #                         # print "val" + str(val)
                    #                         if type(rec_old[key]) != str:
                    #                             rec_old[key] = datetime.datetime.fromtimestamp(
                    #                                 (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                    #                         else:
                    #                             rec_old[key] = rec_old[key]
                    #                         print rec_old[key]
                    # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "SYSTEM_UNMATCHED"
                    # rec_old['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # rec_old["COMMENTS"] = query['comments']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "UNMATCHED"
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old["INVESTIGATOR_COMMENTS"] = query["comments"]

                    # if (rec_old["STATEMENT_DATE"] is not None) and (type(rec_old["STATEMENT_DATE"]) == int) and (rec_old["STATEMENT_DATE"] is not 0):
                    #     logger.info(type(rec_old["STATEMENT_DATE"]))
                    #     rec_old['STATEMENT_DATE'] = datetime.datetime.fromtimestamp(rec_old['STATEMENT_DATE'] / 1000).strftime("%Y-%m-%d %H:%M:%S")
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 24
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED "
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED "
                    rec_old['INVESTIGATED_BY'] = flask.session["sessionData"]['userName']

                    # rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    # rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old['UPDATED_BY'] = None
                    logger.info(rec_old["ACTION_BY"])

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                        # print json.dumps(oldData)
                        # print pandas.read_json(json.dumps(oldData),orient='records')
                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                for r in exmaster_data:
                    upd_exmaster_data = dict()
                    # if type(exmaster_data) == dict:
                    #    exmaster_data = exmaster_data
                    # else:
                    #    exmaster_data = json.loads(exmaster_data)[0]
                    # exmaster_data = json.loads(exmaster_data)[0]
                    upd_exmaster_data["COMMENTS"] = query['comments']
                    upd_exmaster_data["LINK_ID"] = r["LINK_ID"]
                    upd_exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exmaster_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                   '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                             '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # logger.info(exmaster_data['EXCEPTION_OID'])
                    upd_exmaster_data["EXCEPTION_ID"] = r["EXCEPTION_ID"]
                    upd_exmaster_data["EXCEPTION_STATUS"] = "INPROGESS"
                    upd_exmaster_data["IPADDRESS"] = "192.168.2.248"

                    upd_exmaster_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exmaster_data["RECORD_VERSION"] = r['RECORD_VERSION'] + 1
                    upd_exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    upd_exmaster_data["BUSINESS_CONTEXT_ID"] = r['BUSINESS_CONTEXT_ID']
                    upd_exmaster_data["RECON_EXECUTION_ID"] = r['RECON_EXECUTION_ID']
                    upd_exmaster_data["RECON_ID"] = r["RECON_ID"]
                    upd_exmaster_data["PRODUCTLINE_ID"] = r["PRODUCTLINE_ID"]
                    upd_exmaster_data["BUSINESS_PROCESS_ID"] = r["BUSINESS_PROCESS_ID"]
                    upd_exmaster_data["ASSET_CLASS_ID"] = r["ASSET_CLASS_ID"]
                    oldData_ex.append(upd_exmaster_data)

                    logger.info(oldData_ex)
                parseDates = ["EXCEPTION_CREATED_DATE", "CLEARING_DATE", "CREATED_DATE", "TRADE_DATE", "UPDATED_DATE",
                              "RECORD_END_DATE", "EXCEPTION_COMPLETION_DATE", "EXCEPTION_PROCESSING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                                 table_name="exception_master", connection=recon_con)

                for i in exallocation_data:
                    upd_exallocation_data = dict()
                    # exallocation_data = json.loads(exallocation_data)[0]
                    upd_exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                    upd_exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exallocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                       '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["EXCEPTION_ALLOCATION_ID"] = i["EXCEPTION_ALLOCATION_ID"]
                    upd_exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    upd_exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"

                    upd_exallocation_data["IPADDRESS"] = "192.168.2.248"
                    upd_exallocation_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exallocation_data["RECORD_VERSION"] = i["RECORD_VERSION"] + 1
                    upd_exallocation_data["EXCEPTION_ID"] = i["EXCEPTION_ID"]
                    upd_exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    upd_exallocation_data["RECON_ID"] = i["RECON_ID"]
                    upd_exallocation_data["BUSINESS_CONTEXT_ID"] = i["BUSINESS_CONTEXT_ID"]
                    upd_exallocation_data["ALLOCATION_STATUS"] = i["ALLOCATION_STATUS"]
                    upd_exallocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                          '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["ALLOCATED_BY"] = flask.session["sessionData"]['userName']
                    upd_exallocation_data["ACTION_BY"] = self.getUserPoolDetails(recon_id=i["RECON_ID"],
                                                                                 user_id=i["ALLOCATED_BY"])
                    upd_exallocation_data["ALLOCATION_HISTORY_FLAG"] = "Y"

                    oldData_allocation.append(upd_exallocation_data)

                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                                       table_name="exception_allocation",
                                                                       connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records found."

    def getUserPoolDetails(self, recon_id, user_id):
        (status_b, data_d) = ReconAssignment().getAll({'perColConditions': {'user': user_id, 'recons': recon_id}})
        if data_d["total"] > 0:
            return data_d["data"][0]["userpool"]
        else:
            return ""

    def getReconMatchedExecutionDate(self, id, query):
        if query is not None:
            start_time = time.time()

            exportFlag = False
            if 'exportData' in query and query['exportData']:
                exportFlag = True
            query['execution_log'] = list(
                ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': query['recon_id'],
                                                 'EXECUTION_STATUS': 'Completed',
                                                 'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                 'EXECUTION_DATE': {
                                                     '$gte': datetime.datetime.strptime(
                                                         query['executionStartDate'],
                                                         '%d/%m/%y'),
                                                     '$lte': datetime.datetime.strptime(
                                                         query['executionEndDate'],
                                                         '%d/%m/%y') + datetime.timedelta(days=1)}}))

            if len(query['execution_log']) == 0:
                return True, {'txn_count': 0, 'data': '[]'}
            elif len(query['execution_log']) > 1 and not exportFlag:
                df = pandas.DataFrame(query['execution_log'])
                totalMatchedItems = int(
                    df['PERFECT_MATCHED_RECORDS_COUNT'].astype(long).sum() + df['FORCE_MATCHED'].astype(long).sum())

                return True, {'txn_count': totalMatchedItems, 'data': [], 'download': True,
                              'totalMatchedCount': totalMatchedItems, 'stateDT': False,
                              'execDT': True,
                              'RECON_EXECUTION_ID': df['RECON_EXECUTION_ID'].unique().tolist()}
            else:
                query['executionStartDate'] = datetime.datetime.strptime(query['executionStartDate'], '%d/%m/%y').date()
                query['executionEndDate'] = datetime.datetime.strptime(query['executionEndDate'], '%d/%m/%y').date()
                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}
                status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(query, execDT=True,
                                                                                             skip=skipFrom,
                                                                                             limit=limitTo)
                json_out = dict()
                totalMatchedItems = 0
                if len(data) > 0:
                    totalMatchedItems = len(data)
                else:
                    return True, {'txn_count': 0, 'data': '[]'}

                if 'columnName' in query and 'operation' in query and 'value' in query:
                    if query['operation'] in ['==', '>=', '<=']:
                        data = data[data[query['columnName']] == query['value']]
                    if query['operation'] in ['contains']:
                        data = data[data[query['columnName']].str.contains(query['value'], case=False)]

                if exportFlag:
                    fileName = self.export_to_csv(query['recon_id'], query['recon_name'], data)
                    return True, {'txn_count': 0, 'data': '[]', 'exportNow': True, 'fileName': fileName}

                data = data[skipFrom:limitTo - 1]
                # if data exceeds configured threshold limit provide export all to retrieve data
                if len(data) > config.threshold:
                    business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], data)
                    json_out['total'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                else:
                    json_out = self.convert_to_json(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = False
                    logger.info('matched record search Completed in ' + str(time.time() - start_time))
                return True, json_out
        else:
            return False, "No Data Found."

    def getReconMatchedStatementDate(self, id, query):
        if query is not None:
            start_time = time.time()

            exportFlag = False
            if 'exportData' in query and query['exportData']:
                exportFlag = True
            query['execution_log'] = list(
                ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': query['recon_id'],
                                                 'EXECUTION_STATUS': 'Completed',
                                                 'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                 'STATEMENT_DATE': {
                                                     '$gte': datetime.datetime.strptime(query['statementStartDate'],
                                                                                        '%d/%m/%y'),
                                                     '$lte': datetime.datetime.strptime(query['statementEndDate'],
                                                                                        '%d/%m/%y')}}))
            if len(query['execution_log']) == 0:
                return True, {'txn_count': 0, 'data': '[]'}
            elif len(query['execution_log']) > 1 and not exportFlag:
                df = pandas.DataFrame(query['execution_log'])
                totalMatchedItems = int(
                    df['PERFECT_MATCHED_RECORDS_COUNT'].astype(long).sum() + df['FORCE_MATCHED'].astype(long).sum())
                return True, {'txn_count': totalMatchedItems, 'data': [], 'totalMatchedCount': totalMatchedItems,
                              'download': True, 'stateDT': True,
                              'execDT': False,
                              'RECON_EXECUTION_ID': df['RECON_EXECUTION_ID'].unique().tolist()}
            else:
                query['statementStartDate'] = datetime.datetime.strptime(query['statementStartDate'], '%d/%m/%y')
                query['statementEndDate'] = datetime.datetime.strptime(query['statementEndDate'], '%d/%m/%y')
                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}
                status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(query, stateDT=True,
                                                                                             skip=skipFrom,
                                                                                             limit=limitTo)
                totalMatchedItems = 0
                if len(data) > 0:
                    totalMatchedItems = len(data)
                else:
                    return True, {'txn_count': 0, 'data': '[]'}

                if 'columnName' in query and 'operation' in query and 'value' in query:
                    if query['operation'] in ['==', '>=', '<=']:
                        data = data[data[query['columnName']] == query['value']]
                    if query['operation'] in ['contains']:
                        data = data[data[query['columnName']].str.contains(query['value'], case=False)]
                json_out = dict()
                if exportFlag:
                    fileName = self.export_to_csv(query['recon_id'], query['recon_name'], data)
                    return True, {'txn_count': 0, 'data': '[]', 'exportNow': True, 'fileName': fileName}

                data = data[skipFrom:limitTo - 1]

                # if data exceeds configured threshold limit provide export all to retrieve data
                if len(data) > config.threshold:
                    status, business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                else:
                    json_out = self.convert_to_json(data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = False
                logger.info('matched record search on stmt date Completed in ' + str(time.time() - start_time))
                return True, json_out
        else:
            return False, "No data found."

    def getReconMatched_St_Ex_Date(self, id, query):
        if query is not None:
            start_time = time.time()
            exportFlag = False
            if 'exportData' in query and query['exportData']:
                exportFlag = True
            query['execution_log'] = list(
                ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': query['recon_id'],
                                                 'EXECUTION_STATUS': 'Completed',
                                                 'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                 'STATEMENT_DATE': {
                                                     '$gte': datetime.datetime.strptime(
                                                         query['statementStartDate'],
                                                         '%d/%m/%y'),
                                                     '$lte': datetime.datetime.strptime(
                                                         query['statementEndDate'],
                                                         '%d/%m/%y')},
                                                 'EXECUTION_DATE': {
                                                     '$gte': datetime.datetime.strptime(
                                                         query['executionStartDate'],
                                                         '%d/%m/%y'),
                                                     '$lt': datetime.datetime.strptime(
                                                         query['executionEndDate'],
                                                         '%d/%m/%y') + datetime.timedelta(days=1)}}))
            if len(query['execution_log']) == 0:
                return True, {'txn_count': 0, 'data': '[]'}
            elif len(query['execution_log']) > 1 and not exportFlag:
                df = pandas.DataFrame(query['execution_log'])

                totalMatchedItems = int(
                    df['PERFECT_MATCHED_RECORDS_COUNT'].astype(long).sum() + df['FORCE_MATCHED'].astype(long).sum())

                return True, {'txn_count': totalMatchedItems, 'data': [], 'totalMatchedCount': totalMatchedItems,
                              'download': True, 'stateDT': True,
                              'execDT': True,
                              'RECON_EXECUTION_ID': df['RECON_EXECUTION_ID'].unique().tolist()}
            else:
                query['statementStartDate'] = datetime.datetime.strptime(query['statementStartDate'], '%d/%m/%y')
                query['statementEndDate'] = datetime.datetime.strptime(query['statementEndDate'], '%d/%m/%y')

                query['executionStartDate'] = datetime.datetime.strptime(query['executionStartDate'], '%d/%m/%y').date()
                query['executionEndDate'] = datetime.datetime.strptime(query['executionEndDate'], '%d/%m/%y').date()

                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if jobsList is not None and len(jobsList) != 0:
                    return True, {'background_process': True}
                status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(query, execDT=True,
                                                                                             stateDT=True)
                totalMatchedItems = 0
                if len(data) > 0:
                    totalMatchedItems = len(data)
                else:
                    return True, {'txn_count': 0, 'data': '[]'}

                if 'columnName' in query and 'operation' in query and 'value' in query:
                    if query['operation'] in ['==', '>=', '<=']:
                        data = data[data[query['columnName']] == query['value']]
                    if query['operation'] in ['contains']:
                        data = data[data[query['columnName']].str.contains(query['value'], case=False)]

                if exportFlag:
                    fileName = self.export_to_csv(query['recon_id'], query['recon_name'], data)
                    return True, {'txn_count': len(data), 'data': '[]', 'exportNow': True, 'fileName': fileName}

                data = data[skipFrom:limitTo - 1]
                json_out = dict()
                # if data exceeds configured threshold limit provide export all to retrieve data
                if len(data) > config.threshold:
                    status, business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                else:
                    json_out = self.convert_to_json(data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = False
                logger.info(
                    'matched record search on stmt date & execution date Completed in ' + str(time.time() - start_time))
                return True, json_out
        else:
            return False, "No Data Found."

    def getReconMatchedColLevel(self, id, query):
        if query is not None and 'columnName' in query and 'operation' in query and 'value' in query:
            exportFlag = False
            if 'exportData' in query and query['exportData']:
                exportFlag = True
            status, exec_id = self.check_data_exists(query['recon_id'])
            if status:
                clonePath = ReconExecutionDetailsLog().getLatestExeDetails(query['recon_id'])['READ_CLONE_PATH']
                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                query['execution_log'] = [{'RECON_EXECUTION_ID': exec_id}]

                status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(query)

                if query['operation'] in ['==', '>=', '<=']:
                    data = data[data[query['columnName']] == query['value']]
                if query['operation'] in ['contains']:
                    data = data[data[query['columnName']].str.contains(query['value'], case=False)]

                totalMatchedItems = 0
                if len(data) > 0:
                    totalMatchedItems = len(data)
                else:
                    return True, {'txn_count': 0, 'data': '[]'}
                if exportFlag:
                    fileName = self.export_to_csv(query['recon_id'], query['recon_name'], data)
                    return True, {'txn_count': totalMatchedItems, 'data': '[]', 'exportNow': True, 'fileName': fileName}

                data = data[skipFrom:limitTo - 1]
                json_out = dict()
                # if data exceeds configured threshold limit provide export all to retrieve data
                if len(data) > config.threshold:
                    status, business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                else:
                    json_out = self.convert_to_json(data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = False
                return True, json_out
        else:
            return False, "No Data Found."

    def getReconMatchedExportAll(self, id, query):
        if query is not None:
            start_time = time.time()
            execData = list(
                ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': query['recon_id'],
                                                 'EXECUTION_STATUS': 'Completed',
                                                 'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                 }))
            if len(execData) == 0:
                return False, "No data found."
            elif len(execData) > 1:
                df = pandas.DataFrame(execData)
                df = df[['RECON_EXECUTION_ID']][
                    df['RECON_EXECUTION_ID'].isin(query['responseData']['RECON_EXECUTION_ID'])]
                business_df = ReconUtility().get_business_details()
                df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']][
                    business_df['RECON_ID'] == query['recon_id']]
                reconName = df2['RECON_NAME'].max()
                # df = pandas.merge(df, df2, on=["RECON_ID"], how="left")
                # df.fillna('', inplace=True)
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if not jobsList and len(jobsList) != 0:
                    return True, {'background_process': True}
                if len(df):
                    frame = pandas.DataFrame()
                    for excec_id in df['RECON_EXECUTION_ID'].unique().tolist():
                        innerQuery = {'execution_log': [{'RECON_EXECUTION_ID': excec_id}],
                                      'recon_id': query['recon_id'],
                                      'execDT': False, 'stateDT': False}
                        if query['responseData']['stateDT']:
                            innerQuery['statementStartDate'] = datetime.datetime.strptime(query['statementStartDate'],
                                                                                          '%d/%m/%y')
                            innerQuery['statementEndDate'] = datetime.datetime.strptime(query['statementEndDate'],
                                                                                        '%d/%m/%y')
                            innerQuery['stateDT'] = True
                        if query['responseData']['execDT']:
                            innerQuery['executionStartDate'] = datetime.datetime.strptime(query['executionStartDate'],
                                                                                          '%d/%m/%y')
                            innerQuery['executionEndDate'] = datetime.datetime.strptime(query['executionEndDate'],
                                                                                        '%d/%m/%y')
                            innerQuery['execDT'] = True
                        status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(innerQuery,
                                                                                                     stateDT=innerQuery[
                                                                                                         'stateDT'],
                                                                                                     execDT=innerQuery[
                                                                                                         'execDT'])
                        frame = pandas.concat([frame, data], ignore_index=True)
                    json_out = {}
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], reconName, frame)
                    return True, json_out
                else:
                    return False, "No data found."
            else:
                query['statementStartDate'] = datetime.datetime.strptime(query['statementStartDate'], '%d/%m/%y')
                query['statementEndDate'] = datetime.datetime.strptime(query['statementEndDate'], '%d/%m/%y')
                skipFrom = query['skip'] if 'skip' in query else 0
                limitTo = query['limit'] if 'limit' in query else config.hdfMatchedLimit
                jobsList = ReconWorker(query['recon_id']).zfsQueue.get_job_ids()
                if not jobsList and len(jobsList) != 0:
                    return True, {'background_process': True}
                status, data = HDF5Interface(query['recon_id'], '').fetch_matched_search_rst(query, execDT=True,
                                                                                             skip=skipFrom,
                                                                                             limit=limitTo)
                totalMatchedItems = 0
                if len(data) > 0:
                    totalMatchedItems = len(data)
                data = data.loc[skipFrom:limitTo - 1]
                json_out = dict()
                # if data exceeds configured threshold limit provide export all to retrieve data
                if len(data) > config.threshold:
                    status, business_doc = Business().get({'RECON_ID': query['recon_id']})
                    json_out['fileName'] = self.export_to_csv(query['recon_id'], business_doc['RECON_NAME'], data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = True
                else:
                    json_out = self.convert_to_json(data)
                    json_out['txn_count'] = len(data)
                    json_out['totalMatchedCount'] = totalMatchedItems
                    json_out['ignore_load'] = False
                logger.info('matched record search on stmt date Completed in ' + str(time.time() - start_time))
                return True, json_out
        else:
            return False, "No data found."

    def getExceptionExecutionDate(self, id, query):
        if query is not None:
            status, business_doc = self.get_allocated_recons()
            exec_doc = list(ReconExecutionDetailsLog().find(
                {'EXECUTION_DATE': {
                    '$gte': datetime.datetime.strptime(
                        query['executionStartDate'],
                        '%d/%m/%y'),
                    '$lt': datetime.datetime.strptime(
                        query['executionEndDate'],
                        '%d/%m/%y') + datetime.timedelta(days=1)},
                    'RECORD_STATUS': 'ACTIVE'}))

            if len(exec_doc) == 0 or len(business_doc) == 0:
                return True, '[]'
            else:
                execution_data = pandas.DataFrame(exec_doc)
                business_doc = pandas.DataFrame(business_doc)[['RECON_ID', 'RECON_NAME']]
                execution_data = pandas.merge(execution_data, business_doc, on=["RECON_ID"])
                execution_data.loc[:, 'TOTAL_TRANSACTIONS'] = \
                    execution_data['PERFECT_MATCHED_RECORDS_COUNT'].astype(int) + \
                    execution_data['EXCEPTION_RECORDS_COUNT'].astype(int)
                execution_data.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)
                json_out = execution_data[config.execution_summary_columns].to_json(orient='records')
                return True, json_out

    def getExceptionStatementDate(self, id, query):
        if query is not None:
            status, business_doc = self.get_allocated_recons()
            exec_doc = list(ReconExecutionDetailsLog().find(
                {'STATEMENT_DATE': {'$gte': datetime.datetime.strptime(
                    query['statementStartDate'],
                    '%d/%m/%y'), '$lte': datetime.datetime.strptime(
                    query['statementEndDate'],
                    '%d/%m/%y')},
                    'RECORD_STATUS': 'ACTIVE'}))

            if len(exec_doc) == 0 or len(business_doc) == 0:
                return True, '[]'
            else:
                execution_data = pandas.DataFrame(exec_doc)
                business_doc = pandas.DataFrame(business_doc)[['RECON_ID', 'RECON_NAME']]
                execution_data = pandas.merge(execution_data, business_doc, on=["RECON_ID"])
                execution_data.loc[:, 'TOTAL_TRANSACTIONS'] = \
                    execution_data['PERFECT_MATCHED_RECORDS_COUNT'].astype(int) + \
                    execution_data['EXCEPTION_RECORDS_COUNT'].astype(int)
                execution_data.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)
                json_out = execution_data[config.execution_summary_columns].to_json(orient='records')
                return True, json_out

    def getException_St_Ex_Date(self, id, query):
        if query is not None:
            status, business_doc = self.get_allocated_recons()
            exec_doc = list(ReconExecutionDetailsLog().find(
                {'STATEMENT_DATE': {'$gte': datetime.datetime.strptime(
                    query['statementStartDate'],
                    '%d/%m/%y'), '$lte': datetime.datetime.strptime(
                    query['statementEndDate'],
                    '%d/%m/%y')}, 'EXECUTION_DATE': {
                    '$gte': datetime.datetime.strptime(
                        query['executionStartDate'],
                        '%d/%m/%y'),
                    '$lt': datetime.datetime.strptime(
                        query['executionEndDate'],
                        '%d/%m/%y') + datetime.timedelta(days=1)},
                    'RECORD_STATUS': 'ACTIVE'}))

            if len(exec_doc) == 0 or len(business_doc) == 0:
                return True, '[]'
            else:
                execution_data = pandas.DataFrame(exec_doc)
                business_doc = pandas.DataFrame(business_doc)[['RECON_ID', 'RECON_NAME']]
                execution_data = pandas.merge(execution_data, business_doc, on=["RECON_ID"])
                execution_data.loc[:, 'TOTAL_TRANSACTIONS'] = \
                    execution_data['PERFECT_MATCHED_RECORDS_COUNT'].astype(int) + \
                    execution_data['EXCEPTION_RECORDS_COUNT'].astype(int)
                execution_data.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)
                json_out = execution_data[config.execution_summary_columns].to_json(orient='records')
                return True, json_out

    # Method to retrieve recon_details of only recons allocated to the user
    def get_allocated_recons(self):
        (status_b, data_d) = ReconAssignment().getAll(
            {'perColConditions': {'user': flask.session['sessionData']['userName']}})
        rec_id = []
        if status_b and len(data_d['data']) > 0:
            for r in data_d['data']:
                rec_id.extend(r['recons'].split(','))
            business_doc = list(Business().find({'RECON_ID': {'$in': rec_id}}))
            return True, business_doc
        else:
            return False, []

    # Method to load execution summary details for latest date(today_date)
    def getExecutionSummary(self, id, query):
        if query is not None:
            to_date = datetime.date.today()
            to_date = datetime.datetime(to_date.year, to_date.month, to_date.day)

            status, business_doc = self.get_allocated_recons()
            exec_doc = list(ReconExecutionDetailsLog().find(
                {'EXECUTION_DATE': {'$gte': to_date, '$lt': to_date + datetime.timedelta(days=1)},
                 'RECORD_STATUS': 'ACTIVE'}))

            if len(exec_doc) == 0 or len(business_doc) == 0:
                return True, '[]'
            else:
                execution_data = pandas.DataFrame(exec_doc)
                business_doc = pandas.DataFrame(business_doc)[['RECON_ID', 'RECON_NAME']]
                execution_data = pandas.merge(execution_data, business_doc, on=["RECON_ID"])
                execution_data.loc[:, 'TOTAL_TRANSACTIONS'] = \
                    execution_data['PERFECT_MATCHED_RECORDS_COUNT'].astype(int) + \
                    execution_data['EXCEPTION_RECORDS_COUNT'].astype(int)
                execution_data.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)
                json_out = execution_data[config.execution_summary_columns].to_json(orient='records')
                return True, json_out

    def getSummary(self, id, query):
        (status, totalData) = ReportInterface().queryData(levelkey='BUSINESS_CONTEXT_ID', filters={},
                                                          datasetName='BusinessRecon', summary='get')
        # logger.info(status)
        # logger.info(totalData)
        # logger.info(totalData)
        # file_name = self.generateADFReport()
        file_name = ''

        total_data = {}
        total_data['summary_data'] = totalData
        total_data['file_name'] = file_name

        return True, total_data

    def getSummaryNew(self, id, query):
        status_b, data_d = True, list(ReconAssignment().find({'user': flask.session['sessionData']['userName']}))
        bus_id = []
        rec_id = []
        if len(data_d):
            for r in data_d:
                bus_id.append(str(r['businessContext']))
                rec_id.append(r['recons'].split(','))

            if len(bus_id) > 0:
                recon_ids = list(np.concatenate(rec_id, axis=0))
                # print recon_ids
                current_exec_data = list(
                    ReconExecutionDetailsLog().find({'RECON_ID': {"$in": recon_ids}, 'EXECUTION_STATUS': 'Completed',
                                                     'RECORD_STATUS': 'ACTIVE',
                                                     'PROCESSING_STATE_STATUS': 'Matching Completed'}))
                if len(current_exec_data) == 0:
                    return False, 'No Data Found'
                df = pandas.DataFrame(current_exec_data).groupby('RECON_ID').max().reset_index()
                # print  pandas.DataFrame(current_exec_data)
                business_df = ReconUtility().get_business_details()
                df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
                for col in ['WORKPOOL_NAME', 'RECON_NAME']:
                    if col in df.columns: del df[col]
                df = pandas.merge(df, df2, on=["RECON_ID"], how="left")
                df.fillna('', inplace=True)

                dashBoardCols = ['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME', u'EXECUTION_DATE', 'STATEMENT_DATE',
                                 'PERFECT_MATCHED_RECORDS_COUNT', 'FORCE_MATCHED', u'EXCEPTION_RECORDS_COUNT',
                                 u'AUTHORIZATION_PENDING',
                                 u'EXECUTION_STATUS',
                                 u'POSTMATCH_IGNORE_COUNT',
                                 u'ROLLBACK_AUTHORIZATION_PENDING', u'CARRYFORWARD_COUNT', 'BUSINESS_CONTEXT_ID'
                                 ]
                for col in ['STATEMENT_DATE', 'EXECUTION_DATE']:
                    df[col] = pandas.DatetimeIndex(df[col])
                # df = df.applymap(lambda x: str(x).strip())
                df = df[dashBoardCols]
                df.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME',
                                   'EXCEPTION_RECORDS_COUNT': 'UNMATCHED',
                                   'PERFECT_MATCHED_RECORDS_COUNT': 'AUTOMATCHED',
                                   'PERFECT_MATCHED_RECORDS_COUNT': 'AUTOMATCHED'
                                   }, inplace=True)
                return True, {'summary_data': df.to_json(orient="records"), 'file_name': ''}
        file_name = ''
        #
        # total_data = {}
        # total_data['summary_data'] = totalData
        # total_data['file_name'] = file_name

        return True, "{'summary_data':'{}'}"

    def generateADFReport(self):
        (status, connection) = sql.getConnection()
        if status and connection is not None:

            # check for directory
            if not os.path.exists(os.path.join(config.contentDir, "adf_report")):
                os.makedirs(os.path.join(config.contentDir, "adf_report"))

            with open(os.path.join(config.contentDir, "adf_report/tmp_adf_template.txt"), 'w') as csvfile:
                csv_file = csv.writer(csvfile, delimiter='|')
                today_date_fmt = str(datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%Y %H:%M:%S'))
                today_date_fmt1 = today_date_fmt.replace(' ', ':')
                today_date_fmt2 = today_date_fmt1.replace('-', '')

                today_date = datetime.datetime.strftime(datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'),
                                                        '%d%m%Y%H%M%S')

                column_order = ["HDR", "REPORT_DATE", "ACCOUNT_NO", "ACCOUNT_CCY", "BANK_TYPE", "DEBIT_CREDIT_FLAG",
                                "TXN_COUNT",
                                "TXN_AMT", "PENDING_SINCE", "LOSS_PROVISION_AMT", "GL_NO", "TRANSACTION_ID",
                                "ENTRY_SOURCE", "SOURCE_SYS", "Effective_from_Dttm", "Effective_To_Dttm",
                                "EXTRACT_DATE", "BATCH_ID", "BATCH_DATE", "ERECON"]
                column_order.append((today_date_fmt2))

                adf_report_query = "select trunc(statement_date) as Extract_date,account_number as ACCOUNT_NO,currency as ACCOUNT_CCY ,debit_credit_indicator as DEBIT_CREDIT_FLAG, amount as TXN_AMT, source_name as ENTRY_SOURCE, REF_NUMBER1 as TRANSACTION_ID from cash_output_txn " \
                                   "where record_status = 'ACTIVE' and entry_type = 'T' and matching_status = 'UNMATCHED' and recon_id = 'TRSC_CASH_APAC_20181'"

                adf_report_df = pandas.read_sql(adf_report_query, connection)

                for i in range(0, len(column_order)):
                    # add static columns
                    if column_order[i] not in adf_report_df:
                        adf_report_df[column_order[i]] = np.nan
                adf_report_df["REPORT_DATE"] = datetime.datetime.strftime(
                    datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'), '%d-%m-%Y')
                adf_report_df["TXN_COUNT"] = 1
                adf_report_df["LOSS_PROVISION_AMT"] = 0
                adf_report_df["BANK_TYPE"] = "I"
                adf_report_df["SOURCE_SYS"] = "ERECON"

                # calculating ageing
                adf_report_df['PENDING_SINCE'] = (datetime.datetime.now().date() - adf_report_df['EXTRACT_DATE']).apply(
                    lambda x: pandas.tslib.Timedelta(x).days)

                adf_report_df['EXTRACT_DATE'] = datetime.datetime.strftime(
                    datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'), '%d-%m-%Y')
                print (adf_report_df['PENDING_SINCE'])
                # re-ordering columns
                adf_report_df = adf_report_df[column_order]
                record_count = len(adf_report_df.index)

                # data = [["ADF Report"], ['REPORT_DATE', to_date], ["TXN_COUNT", record_count]]
                # csv_file.writerows(data)
                adf_report_df.to_csv(csvfile, index=False, sep="|", date_format='%d-%m-%Y')

            # As per the requirement, need to strip the pipe delimter for the HDR columns
            file_name = "ERECON_ADF_TRANSACTIONS_LONG_OS_UNRECONCILED_TRNX_" + today_date + ".txt"
            with open(os.path.join(config.contentDir, "adf_report/" + file_name), "w") as adf_output:

                with open(os.path.join(config.contentDir, "adf_report/tmp_adf_template.txt"), "r") as tmp_adf_report:
                    count = 0
                    for line in tmp_adf_report:
                        if count > 0:
                            frmt_line = line.lstrip('|')[:-3] + '\n'
                        else:
                            frmt_line = line
                        adf_output.write(frmt_line)
                        count += 1

                adf_output.write("TRL|" + str(record_count) + "|" + today_date_fmt2)

        return file_name

    def getSuspence(self, id, query):
        (status_sql, recon_con) = sql.getConnection()
        (status, suspData) = sql.read_col_query_unmatched_suspence(connection=recon_con, query={})
        # logger.info('suspense -------------------------------------' + str(suspData))
        if not suspData.empty:
            logger.info(suspData.columns)
            df1 = suspData
            df1["AC"] = 1

            df1 = df1.groupby(['ACCOUNT_NUMBER', 'TXN_MATCHING_STATUS'], as_index=False).agg({"AC": len})
            # logger.info('dfdfdfdfdfdfdf' + str(df1))
            df1 = pandas.pivot_table(df1, index=['ACCOUNT_NUMBER', 'TXN_MATCHING_STATUS'],
                                     values='AC',
                                     fill_value=0)
            df1 = df1.unstack(level=1)
            df1 = df1.reset_index()
            logger.info(df1)
            suspData['COUNT'] = 1
            indexcolums = ['ACCOUNT_POOL_NAME', 'ACCOUNT_NUMBER', 'ACCOUNT_NAME', 'ACCOUNT_POOL_CONTEXT_CODE',
                           'RECON_ID', 'MATCHING_STATUS']
            suspData = pandas.pivot_table(suspData, index=indexcolums, columns=['DEBIT_CREDIT_INDICATOR'],
                                          values=['AMOUNT', "COUNT"],
                                          aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0.0)
            # if 'AMOUNT' in suspData:
            #    suspData['AMOUNT'] = 0.0
            # if 'COUNT' in suspData:
            #    suspData['COUNT'] = 0

            suspData.columns = [''.join(list(col)) for col in suspData.columns.values]
            # suspData =suspData.unstack(level=-1)
            suspData = suspData.reset_index()
            if not df1.empty:
                suspData = pandas.merge(df1, suspData, on=['ACCOUNT_NUMBER'], how="left")
            dfToSend = pandas.DataFrame()
            if 'ACCOUNT_POOL_NAME_x' in suspData.columns:
                dfToSend['ACCOUNT_POOL_NAME'] = suspData['ACCOUNT_POOL_NAME_x']
            else:
                dfToSend['ACCOUNT_POOL_NAME'] = suspData['ACCOUNT_POOL_NAME']
            if 'ACCOUNT_POOL_CONTEXT_CODE_x' in suspData.columns:
                dfToSend['ACCOUNT_POOL_CONTEXT_CODE'] = suspData['ACCOUNT_POOL_CONTEXT_CODE_x']
            else:
                dfToSend['ACCOUNT_POOL_CONTEXT_CODE'] = suspData['ACCOUNT_POOL_CONTEXT_CODE']
            if 'ACCOUNT_NUMBER_x' in suspData.columns:
                dfToSend['ACCOUNT_NUMBER'] = suspData['ACCOUNT_NUMBER_x']
            else:
                dfToSend['ACCOUNT_NUMBER'] = suspData['ACCOUNT_NUMBER']
            if 'RECON_ID' in suspData.columns:
                dfToSend['RECON_ID'] = suspData['RECON_ID']
            if 'RECON_ID' in dfToSend.columns:
                df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                      recon_con)
                dfToSend = pandas.merge(dfToSend, df2, on=["RECON_ID"], how="left")
            if 'UNMATCHED' in suspData.columns:
                dfToSend.loc[:, 'UNMATCHED'] = suspData.loc[:, 'UNMATCHED'].fillna(0)
            if 'MATCHED' in suspData.columns:
                dfToSend.loc[:, 'MATCHED'] = suspData.loc[:, 'MATCHED'].fillna(0)
            if 'AUTOMATCHED' in suspData.columns:
                dfToSend.loc[:, 'AUTOMATCHED'] = suspData.loc[:, 'AUTOMATCHED'].fillna(0)
            if 'ROLLBACK_AUTHORIZATION_PENDING' in suspData.columns:
                dfToSend.loc[:, 'ROLLBACK_AUTHORIZATION_PENDING'] = suspData.loc[:,
                                                                    'ROLLBACK_AUTHORIZATION_PENDING'].fillna(0)
            if 'FORCE_MATCHED' in suspData.columns:
                dfToSend.loc[:, 'FORCE_MATCHED'] = suspData.loc[:, 'FORCE_MATCHED'].fillna(0)
            if 'AUTHORIZATION_PENDING' in suspData.columns:
                dfToSend.loc[:, 'AUTHORIZATION_PENDING'] = suspData.loc[:, 'AUTHORIZATION_PENDING'].fillna(0)

            dfToSend['ACCOUNT_NAME'] = suspData['ACCOUNT_NAME']
            dfToSend['CREDIT_COUNT'] = suspData['COUNTC']
            dfToSend['DEBIT_COUNT'] = suspData['COUNTD']
            dfToSend['CREDIT_AMOUNT'] = suspData['AMOUNTC']
            dfToSend['DEBIT_AMOUNT'] = suspData['AMOUNTD']
            dfToSend["NETOUTSTANDINGAMOUNT"] = dfToSend["DEBIT_AMOUNT"] - dfToSend["CREDIT_AMOUNT"]
            dfToSend["NETOUTSTANDINGCOUNT"] = dfToSend["CREDIT_COUNT"] + dfToSend["DEBIT_COUNT"]
            dfToSend.drop_duplicates(['ACCOUNT_NUMBER'], keep='last', inplace=True)
            for column in dfToSend.columns:
                if column.find("AMOUNT") != -1:
                    dfToSend[column] = dfToSend[column].apply(lambda x: '{:.2f}'.format(x))
            return True, dfToSend.to_json(orient="records")
        else:
            return True, '[]'

    def get_Inbox_transaction(self, id, query={}):
        (status, connection) = sql.getConnection()
        if status and connection is not None:
            (status, dfToSend) = sql.read_col_query_indox_txn(connection)
            for column in dfToSend.columns:
                if column == "AMOUNT":
                    dfToSend[column] = dfToSend[column].apply(lambda x: '{:.2f}'.format(x))
            return True, dfToSend.to_json(orient="records")
        else:
            return True, "No data found for the selection."

    def getMhadaReport(self, id, query={}):

        # df = pandas.read_csv('/home/ubuntu/Desktop/mhada.csv')
        (status, data) = MhadaReport().getAll({})

        (status, connection) = sql.getConnection()
        query = "select SOURCE_TYPE,DEBIT_CREDIT_INDICATOR,AMOUNT,TXT_5_002,MATCHING_STATUS from cash_output_txn" \
                " where record_status = 'ACTIVE' and recon_id = 'MHADA_CASH_APAC_20441'"

        df = pandas.read_sql(query, connection)
        result = dict()
        # print df
        df = df[['AMOUNT', 'TXT_5_002', 'SOURCE_TYPE', 'MATCHING_STATUS']]

        df['COUNT'] = 0

        df = pandas.pivot_table(df, index=['SOURCE_TYPE', 'MATCHING_STATUS'],
                                columns='TXT_5_002', values=['AMOUNT', "COUNT"],
                                aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0)

        logger.info(df)
        df.columns = [''.join(list(col)) for col in df.columns.values]

        # CREDIT & DEBIT counts
        credit_count = df['COUNTC'].to_dict()
        debit_count = df['COUNTD'].to_dict()

        # adding credit counts
        result['MATCHED_C_S1_COUNT'] = credit_count[('S1', 'MATCHED')]
        result['UNMATCHED_C_S1_COUNT'] = credit_count[('S1', 'UNMATCHED')]
        result['MATCHED_C_S2_COUNT'] = credit_count[('S2', 'MATCHED')]
        result['UNMATCHED_C_S2_COUNT'] = credit_count[('S2', 'UNMATCHED')]

        # adding debit counts
        result['MATCHED_D_S1_COUNT'] = debit_count[('S1', 'MATCHED')]
        result['UNMATCHED_D_S1_COUNT'] = debit_count[('S1', 'UNMATCHED')]
        result['MATCHED_D_S2_COUNT'] = debit_count[('S2', 'MATCHED')]
        result['UNMATCHED_D_S2_COUNT'] = debit_count[('S2', 'UNMATCHED')]

        # AMOUNTS
        credit_amount = df['AMOUNTC'].to_dict()
        result['MATCHED_C_S1_AMOUNT'] = credit_amount[('S1', 'MATCHED')]
        result['UNMATCHED_C_S1_AMOUNT'] = credit_amount[('S1', 'UNMATCHED')]
        result['MATCHED_C_S2_AMOUNT'] = credit_amount[('S2', 'MATCHED')]
        result['UNMATCHED_C_S2_AMOUNT'] = credit_amount[('S2', 'UNMATCHED')]

        debit_amount = df['AMOUNTD'].to_dict()
        result['MATCHED_D_S1_AMOUNT'] = debit_amount[('S1', 'MATCHED')]
        result['UNMATCHED_D_S1_AMOUNT'] = debit_amount[('S1', 'UNMATCHED')]
        result['MATCHED_D_S2_AMOUNT'] = debit_amount[('S2', 'MATCHED')]
        result['UNMATCHED_D_S2_AMOUNT'] = debit_amount[('S2', 'UNMATCHED')]

        result['CLOSING_BALANCE'] = data['data'][0]['CLOSING_BALANCE']
        result['OPENING_BALANCE'] = data['data'][0]['OPENING_BALANCE']
        result['S1_TOTAL_COUNT'] = result['MATCHED_C_S1_COUNT'] + result['UNMATCHED_C_S1_COUNT'] + result[
            'MATCHED_D_S1_COUNT'] + result['UNMATCHED_D_S1_COUNT']
        result['S2_TOTAL_COUNT'] = result['MATCHED_C_S2_COUNT'] + result['UNMATCHED_C_S2_COUNT'] + result[
            'MATCHED_D_S2_COUNT'] + result['UNMATCHED_D_S2_COUNT']

        return True, result



class ReportInterface():
    def __init__(self):
        # self.ip="192.168.2.129"
        # self.port=1521
        # self.sid='xe'
        # self.oradsn=cx_Oracle.makedsn(self.ip, self.port, self.sid)
        # self.oraconnection=cx_Oracle.connect('algoreconutil_dev', 'algorecon', self.oradsn)
        # self.metadata={"tree":["BUSINESS_CONTEXT_ID","RECON_ID","SOURCE_TYPE","RECONCILIATION_STATUS"],"aggregation":["COUNT","CREDIT","DEBIT","NETOUTSTANDINGAMOUNT"]}
        # self.df=None
        # self.statesdf = pandas.read_csv("meta/states.csv")
        self.statesNewdf = pandas.read_csv(config.flaskPath + "/meta/statesNew.csv")

    def getBusinessContextPerUser(self, id):
        (status_b, data_d) = ReconAssignment().getAll(
            {'perColConditions': {'user': flask.session['sessionData']['userName']}})
        bus_id = []
        rec_id = []
        if status_b and len(data_d['data']) > 0:
            # logger.info(data_d['data'])
            for r in data_d['data']:
                bus_id.append(r['businessContext'])
                rec_id.append(r['recons'].split(','))
            logger.info('bus ------------' + str(bus_id))
            (status, recon_con) = sql.getConnection()
            if len(bus_id) > 0:
                joined_ids = "','".join(bus_id)
                joined_rec_ids = "','".join(np.concatenate(rec_id, axis=0))
                # get max execution id for recons
                cursor = recon_con.cursor()
                current_exec_query = "select max(recon_execution_id) as execution_id from recon_execution_details_log where record_status = 'ACTIVE' and processing_state_status = 'Matching Completed' and recon_id in ('" + joined_rec_ids + "') group by recon_id"
                # fetchmany caching issue migrated to pandas
                # #cursor.execute(current_exec_query)
                # rows = cursor.fetchmany()
                df = pandas.read_sql(current_exec_query, recon_con)
                rows = df['EXECUTION_ID'].tolist()
                # logger.info(rows)
                execution_ids = ",".join(str(val) for val in rows)
                # logger.info(execution_ids)
                if id == "join":
                    return True, " and wo.business_context_id in ('" + joined_ids + "') and rd.recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                elif id == "dummy2":
                    return True, " and business_context_id in ('" + joined_ids + "') and recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                else:
                    return True, " and business_context_id in ('" + joined_ids + "') and recon_id in ('" + joined_rec_ids + "') and recon_execution_id in (" + execution_ids + ")", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "') and txn.recon_execution_id in (" + execution_ids + ")"
            else:
                return True, ''
        else:
            return False, ''

    def getBusinessContextPerUserNew(self, id):
        # (status_b,data_d) = ReconAssignment().getAll({'condition':{'user':flask.session['sessionData']['userName']}})
        status_b, data_d = True, list(ReconAssignment().find({'user': flask.session['sessionData']['userName']}))
        bus_id = []
        rec_id = []
        if len(data_d):
            for r in data_d:
                bus_id.append(str(r['businessContext']))
                rec_id.append(r['recons'].split(','))
            if len(bus_id) > 0:
                recon_ids = list(np.concatenate(rec_id, axis=0))
                current_exec_data = list(
                    ReconExecutionDetailsLog().find({'RECON_ID': {"$in": recon_ids}, 'EXECUTION_STATUS': 'Completed',
                                                     'RECORD_STATUS': 'ACTIVE',
                                                     'PROCESSING_STATE_STATUS': 'Matching Completed'}))
                # current_exec_data = pandas.DataFrame(current_exec_data)
                # current_exec_data.sort_values(by=['RECON_EXECUTION_ID'],ascending=False,inplace=True)
                # current_exec_data.drop_duplicates(subset=['RECON_ID'],keep='first',inplace=True)
                if len(current_exec_data) == 0:
                    return True, {'business_context_id': bus_id, 'recon_ids': recon_ids, 'reconData': [],
                                  'recon_execution_id': []}
                df = pandas.DataFrame(current_exec_data).groupby('RECON_ID').max().reset_index()
                # print df
                execution_ids = df['RECON_EXECUTION_ID'].astype(str).tolist()
                if id == "join":
                    return True, {'business_context_id': bus_id, 'recon_ids': recon_ids,
                                  'reconData': df.T.to_dict().values(),
                                  'recon_execution_id': execution_ids}
                    # return True, " and wo.business_context_id in ('" + joined_ids + "') and rd.recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                elif id == "dummy2":
                    return True, {'business_context_id': bus_id, 'recon_ids': recon_ids,
                                  'recon_execution_id': execution_ids,
                                  'reconData': df.T.to_dict().values()}
                    # return True, " and business_context_id in ('" + joined_ids + "') and recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                else:
                    return True, {'business_context_id': bus_id, 'recon_ids': recon_ids,
                                  'reconData': df.T.to_dict().values(),
                                  'recon_execution_id': execution_ids}
                    # return True," and business_context_id in ('"+joined_ids+"') and recon_id in ('"+joined_rec_ids+"') and recon_execution_id in ("+execution_ids+")"," and txn.business_context_id in ('"+joined_ids+"') and txn.recon_id in ('"+joined_rec_ids+"') and txn.recon_execution_id in ("+execution_ids+")"
            else:
                return True, {}
        else:
            return False, {}

    def queryData_old(self, levelkey, filters, datasetName, summary=None):
        filterstr = ""
        df = None
        if datasetName is not None:
            metadata = json.load(open("meta/" + datasetName + ".json"))
            (status, recon_con) = sql.getConnection()
            # (status, recon_con) = True, 'conn'
            if status:
                (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                logger.info('queryyyyyyyyyyyyyyyyyyyyyyyyyyyy' + str(query))
                df = pandas.read_sql(query, recon_con)
                # logger.info("df" + str(df))
                if summary is not None:
                    if "TXN_MATCHING_STATUS" in df.columns:
                        del df['TXN_MATCHING_STATUS']
                df = pandas.merge(df, self.statesdf,
                                  on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                      "AUTHORIZATION_STATUS",
                                      "FORCEMATCH_AUTHORIZATION"], how="left")

        for key, val in filters.items():
            if len(filterstr) > 1:
                filterstr += ' & '
            if type(val) == str:
                val = "\"" + val + "\""
            else:
                val = str(val)
            filterstr = "(df[\"" + key + "\"]==" + val + ")"
        filteredData = df
        # logger.info(filteredData)
        if len(filterstr) > 1:
            # x="self.df["+filters+"]"
            filteredData = filteredData[eval(filterstr)]

        x = dict()
        x["df"] = filteredData
        x["levelkey"] = levelkey
        if summary is not None:
            # filterData = filteredData.to_json(orient="records")
            x['levelkey'] = "Summary"
            filteredData = eval(metadata["customMethod"] + "(x,metadata = metadata)")
            # print metadata
            return True, filteredData
        else:
            filteredData = eval(metadata["customMethod"] + "(x,metadata = metadata)")
            return True, filteredData

    def queryDataOutStanding(self, levelkey, filters, datasetName, pivotFrame, summary=None):
        filterstr = ""
        df = None
        if datasetName is not None:
            df = pandas.DataFrame()
            metadata = json.load(open("meta/" + datasetName + ".json"))
            (concat_str, concat_data) = self.getBusinessContextPerUserNew('dummy')
            if len(pivotFrame) == 0:
                pivotFrame = pandas.DataFrame()
                colNames = metadata['queryColumns']
                if concat_str:
                    for reconDoc in concat_data['reconData']:
                        reconId, execId = reconDoc['RECON_ID'], reconDoc['RECON_EXECUTION_ID']
                        if DashBoardReports().exists(
                                {"RECON_EXECUTION_ID": execId, 'reportName': 'OutStandingAmount', 'RECON_ID': reconId}):
                            status, doc = DashBoardReports().get(
                                {"RECON_EXECUTION_ID": execId, 'reportName': 'OutStandingAmount'})
                            chunk = pandas.DataFrame(doc['ReportData'])
                            pivotFrame = pandas.concat([pivotFrame, chunk])
                            # print pivotFrame
                            # if 'EXCEPTION_RECORDS_COUNT' in reconDoc and int(reconDoc['EXCEPTION_RECORDS_COUNT']) > 0:
                            #     jobsList = ReconWorker(str(reconId)).zfsQueue.get_job_ids()
                            #     if jobsList is not None and len(jobsList) != 0:
                            #         continue
                            #     hdfStoreName = config.hdfFilesPathNew + reconDoc['RECON_ID'] + '/' + execId +'/' + execId + '.h5'
                            #     if os.path.exists(hdfStoreName):
                            #         whr_class = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' != 'AUTOMATCHED')" +\
                            #                     "& ('TXN_MATCHING_STATUS' != 'FORCE_MATCHED') &" +\
                            #                     " ('TXN_MATCHING_STATUS' != 'ROLLBACK_AUTHORIZATION_PENDING')"
                            #         status, chunk = HDF5Interface(reconDoc['RECON_ID'], execId).read_col_query_unmatched(whr_class)
                            #         df = pandas.concat([df,chunk],ignore_index=True)

            if len(df):
                df = df[colNames]
        if len(pivotFrame) == 0:
            for key, val in filters.items():
                if len(filterstr) > 1:
                    filterstr += ' & '
                if type(val) == str:
                    val = "\"" + val + "\""
                else:
                    val = str(val)
                filterstr = "(df[\"" + key + "\"]==" + val + ")"
            filteredData = df
            # logger.info(filteredData)
            if len(filterstr) > 1:
                # x="self.df["+filters+"]"
                filteredData = filteredData[eval(filterstr)]

            x = dict()
            x["df"] = filteredData
            x["levelkey"] = levelkey
            if summary is not None:
                # filterData = filteredData.to_json(orient="records")
                x['levelkey'] = "Summary"
                filteredData, pivotFrame = eval(
                    'self.customProcessOutStanding' + "(x,metadata = metadata,pivotFlag=False)")
                # print metadata["customMethod"]+"(x,metadata = metadata)",metadata
                return True, {'filteredData': filteredData, 'pivotFrame': pivotFrame}
            else:
                filteredData, pivotFrame = eval(
                    'self.customProcessOutStanding' + "(x,metadata = metadata,pivotFlag=False)")
                # print metadata
                return True, {'filteredData': filteredData, 'pivotFrame': pivotFrame}
        else:
            for key, val in filters.items():
                if len(filterstr) > 1:
                    filterstr += ' & '
                if type(val) == str:
                    val = "\"" + val + "\""
                else:
                    val = str(val)
                filterstr = "(df[\"" + key + "\"]==" + val + ")"
            df = pandas.DataFrame(pivotFrame)
            if len(filterstr) > 1:
                df = df[eval(filterstr)]
            x = dict()
            x["df"] = df
            x["pivotFrame"] = pandas.DataFrame(pivotFrame)
            x["levelkey"] = levelkey
            if summary is not None:
                # filterData = filteredData.to_json(orient="records")
                x['levelkey'] = "Summary"
                filteredData, pivotFrame = eval(
                    'self.customProcessOutStanding' + "(x,metadata = metadata,pivotFlag=True)")
                return True, {'filteredData': filteredData, 'pivotFrame': pivotFrame}
            else:
                filteredData, pivotFrame = eval(
                    'self.customProcessOutStanding' + "(x,metadata = metadata,pivotFlag=True)")
                return True, {'filteredData': filteredData, 'pivotFrame': pivotFrame}

    def customProcessOutStanding(self, data, metadata=None, pivotFlag=False):
        logger.info('here')
        levelkey = data["levelkey"]
        columns = []
        indexcolums = []
        for key in metadata["tree"]:
            if key == levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        if levelkey == 'BUSINESS_CONTEXT_ID':
            columns.append("RECON_ID")
        if 'SOURCE_NAME' in indexcolums and levelkey != "SOURCE_NAME":
            indexcolums.remove('SOURCE_NAME')
        if 'ACCOUNT_NAME' in indexcolums and levelkey != "ACCOUNT_NAME":
            indexcolums.remove('ACCOUNT_NAME')
        if levelkey == 'BUSINESS_CONTEXT_ID':
            indexcolums.append("RECON_ID")
        business_df = ReconUtility().get_business_details()

        if not pivotFlag:
            df = data["df"]
            pivotFrame = pandas.DataFrame()
            if levelkey == "TXN_MATCHING_STATUS" and "TXN_MATCHING_STATUS_x" in df.columns:
                df['TXN_MATCHING_STATUS'] = df['TXN_MATCHING_STATUS_y']
                del df['TXN_MATCHING_STATUS_x']
                del df['TXN_MATCHING_STATUS_y']
            dfOutstanding = df.copy()
            if 'TXN_MATCHING_STATUS' in dfOutstanding.columns:
                dfOutstanding = dfOutstanding[
                    (dfOutstanding['TXN_MATCHING_STATUS'].isin(['AUTOMATCHED', 'FORCE_MATCHED',
                                                                'ROLLBACK_AUTHORIZATION_PENDING'])) == False]
            dfOutstandingTotal = dfOutstanding.copy()
            dfOutstandingTotal["COUNT"] = 1
            if len(dfOutstanding) == 0:
                return dfOutstanding.to_json(orient="records"), pivotFrame.to_json(orient='records')
            dfOutstanding = dfOutstanding[columns]
            dfOutstanding["COUNT"] = 1

            if 'EXCEPTION_STATUS' in dfOutstanding.columns:
                dfOutstanding.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'
                dfOutstandingTotal.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'
            if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding[
                'DEBIT_CREDIT_INDICATOR'] is not None:
                dfOutstandingTotal = dfOutstandingTotal[
                    ["BUSINESS_CONTEXT_ID", "RECON_ID", "SOURCE_NAME", "ACCOUNT_NAME", "TXN_MATCHING_STATUS",
                     'DEBIT_CREDIT_INDICATOR', 'AMOUNT', "COUNT"]]
                pivotFrame = pandas.pivot_table(dfOutstandingTotal,
                                                index=["BUSINESS_CONTEXT_ID", "RECON_ID", "SOURCE_NAME", "ACCOUNT_NAME",
                                                       "TXN_MATCHING_STATUS", 'DEBIT_CREDIT_INDICATOR'],
                                                values=['AMOUNT', "COUNT"],
                                                aggfunc={"AMOUNT": np.sum, "COUNT": len})
                pivotFrame.reset_index(inplace=True)
                dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                                   columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                                   aggfunc={"AMOUNT": np.sum, "COUNT": len})
        else:
            pivotFrame = data['pivotFrame']
            df = data['df']
            if 'TXN_MATCHING_STATUS' in df.columns:
                df = df[(df['TXN_MATCHING_STATUS'].isin(['AUTOMATCHED', 'FORCE_MATCHED',
                                                         'ROLLBACK_AUTHORIZATION_PENDING'])) == False]
            # print pivotFrame.dtypes
            dfOutstanding = pandas.pivot_table(df, index=indexcolums,
                                               columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": np.sum})
        if 'AMOUNT' in dfOutstanding.columns:
            dfOutstanding['AMOUNT'].fillna(0.0, inplace=True)

        if 'COUNT' in dfOutstanding.columns:
            dfOutstanding['COUNT'].fillna(0, inplace=True)

        df23 = dfOutstanding.copy()
        df23 = df23.reset_index(col_level=1)
        cols = []
        for (s1, s2) in df23.columns.tolist():
            cols.append(s1 + str(s2))
        df23.columns = cols
        df23.rename(columns={'COUNTC': 'CREDITCOUNT', 'COUNTD': 'DEBITCOUNT', 'AMOUNTC': 'CREDIT', 'AMOUNTD': 'DEBIT'},
                    inplace=True)

        dfOutstanding = df23.copy()

        if "CREDIT" not in dfOutstanding:
            dfOutstanding["CREDIT"] = 0.0
        if "CREDITCOUNT" not in dfOutstanding:
            dfOutstanding["CREDITCOUNT"] = 0.0
        if "DEBIT" not in dfOutstanding:
            dfOutstanding["DEBIT"] = 0.0
        if "DEBITCOUNT" not in dfOutstanding:
            dfOutstanding["DEBITCOUNT"] = 0.0

        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["NETOUTSTANDINGCOUNT"] = dfOutstanding["CREDITCOUNT"] + dfOutstanding["DEBITCOUNT"]


        if dfOutstanding.empty:
            return dfOutstanding.to_json(orient="records")

        if levelkey == "BUSINESS_CONTEXT_ID" and not dfOutstanding.empty:
            df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        else:
            pass

        if levelkey == "RECON_ID" and not dfOutstanding.empty:
            df2 = business_df[['RECON_ID', 'RECON_NAME']]
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        else:
            pass

        if 'COUNT' in dfOutstanding.columns:
            del dfOutstanding['COUNT']
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        dfOutstanding = dfOutstanding.where(pandas.notnull(dfOutstanding), None)
        return dfOutstanding.to_json(orient="records"), pivotFrame.to_json(orient='records')

    def queryData(self, levelkey, filters, datasetName, summary=None):
        filterstr = ""
        df = None
        if datasetName is not None:
            metadata = json.load(open("meta/" + datasetName + ".json"))
            (concat_str, concat_data) = self.getBusinessContextPerUserNew('dummy')
            colNames = metadata['queryColumns']
            df = pandas.DataFrame()
            if concat_str:
                for reconDoc in concat_data['reconData']:
                    reconId, execId = reconDoc['RECON_ID'], reconDoc['RECON_EXECUTION_ID']
                    if DashBoardReports().exists(
                            {"RECON_EXECUTION_ID": execId, 'reportName': 'TransactionStatus', 'RECON_ID': reconId}):
                        status, doc = DashBoardReports().get(
                            {"RECON_EXECUTION_ID": execId, 'reportName': 'TransactionStatus'})
                        chunk = pandas.DataFrame(doc['ReportData'])
                        df = pandas.concat([df, chunk])

        for key, val in filters.items():
            if len(filterstr) > 1:
                filterstr += ' & '
            if type(val) == str:
                val = "\"" + val + "\""
            else:
                val = str(val)
            filterstr = "(df[\"" + key + "\"]==" + val + ")"
        filteredData = df
        # logger.info(filteredData)
        if len(filterstr) > 1:
            # x="self.df["+filters+"]"
            filteredData = filteredData[eval(filterstr)]

        x = dict()
        x["df"] = filteredData
        x["levelkey"] = levelkey
        filteredData = eval('self.transactionStatus' + "(x,metadata = metadata)")
        return True, filteredData

    def transactionStatus(self, data, metadata=None):
        df = data["df"]
        business_df = ReconUtility().get_business_details()

        levelkey = data["levelkey"]
        columns = []
        indexcolums = []
        for key in metadata["tree"]:
            if key == levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        if levelkey == 'BUSINESS_CONTEXT_ID':
            columns.append("RECON_ID")
            indexcolums.append("RECON_ID")
        dfOutstanding = df.copy()

        if 'EXCEPTION_STATUS' in dfOutstanding.columns:
            dfOutstanding.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'
        # print indexcolums
        if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding['DEBIT_CREDIT_INDICATOR'] is not None:
            dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                               columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": np.sum},
                                               fill_value=0.0).reset_index()

        df23 = dfOutstanding.copy()
        df23 = df23.reset_index(col_level=1)
        cols = []
        for (s1, s2) in df23.columns.tolist():
            cols.append(s1 + str(s2))
        df23.columns = cols
        df23.rename(columns={'COUNTC': 'CREDITCOUNT', 'COUNTD': 'DEBITCOUNT', 'AMOUNTC': 'CREDIT', 'AMOUNTD': 'DEBIT'},
                    inplace=True)
        dfOutstanding = df23.copy()
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["NETOUTSTANDINGCOUNT"] = dfOutstanding["CREDITCOUNT"] + dfOutstanding["DEBITCOUNT"]

        if levelkey == "BUSINESS_CONTEXT_ID" and not dfOutstanding.empty:
            df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
        elif levelkey == "RECON_ID" and not dfOutstanding.empty:
            df2 = business_df[['RECON_ID', 'RECON_NAME']]
        if levelkey != 'SOURCE_NAME':
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")

        if 'COUNT' in dfOutstanding.columns:
            del dfOutstanding['COUNT']
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))

        return dfOutstanding.to_json(orient="records")

    def makehighchartsjson(df, columnHierarchy):
        output = dict()
        columns = df.columns
        for index, row in df.iterrows():
            temp = output
            for col in columnHierarchy:
                # print row[col]
                if row[col] not in temp:
                    temp[row[col]] = dict()
                temp = temp[row[col]]
            index = 0
            for col in columns:
                if col in columnHierarchy:
                    continue
                else:
                    temp[col] = row[col]
        return output

    def getAgingReport_old(self, bucketsize, datasetName):
        logger.info(bucketsize)
        df = None
        # statesdf=pandas.read_csv("meta/states.csv")
        if datasetName is not None:
            logger.info(datasetName)
            metadata = json.load(open("meta/" + datasetName + ".json"))
            (status, recon_con) = sql.getConnection()
            (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
            if status:
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                df = pandas.read_sql(query, recon_con)
                # logger.info("df" + str(df))
                df = pandas.merge(df, self.statesdf,
                                  on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                      "AUTHORIZATION_STATUS",
                                      "FORCEMATCH_AUTHORIZATION"], how="left")

        records = df[["RECON_ID", "BUSINESS_CONTEXT_ID", "STATEMENT_DATE", "AMOUNT", "EXCEPTION_STATUS"]].dropna(
            subset=['STATEMENT_DATE'])
        records = records[pandas.notnull(records['STATEMENT_DATE'])]
        records["TRANSACTIONAGE"] = records["STATEMENT_DATE"].apply(
            lambda x: (datetime.datetime.now() - x).days if x.year != 0001  else 0)
        records["BUCKET"] = records["TRANSACTIONAGE"] - (records["TRANSACTIONAGE"] % int(bucketsize))
        records["COUNT"] = 1

        # Remove all matched transactions if any exists
        records.loc[records.loc[:, 'EXCEPTION_STATUS'] == 'CLOSED', 'EXCEPTION_STATUS'] = None
        # logger.info(records)
        x = records[["RECON_ID", "BUSINESS_CONTEXT_ID", "BUCKET", "COUNT", "AMOUNT", "EXCEPTION_STATUS"]].groupby(
            ["BUCKET", "RECON_ID", "EXCEPTION_STATUS", "BUSINESS_CONTEXT_ID"]).sum().reset_index()
        # logger.info(x.columns)
        df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'", recon_con)
        x = pandas.merge(x, df2, on=["RECON_ID"], how="left")
        df3 = pandas.read_sql("select workpool_name,business_context_id from workpool where record_status ='ACTIVE'",
                              recon_con)
        x = pandas.merge(x, df3, on=["BUSINESS_CONTEXT_ID"], how="left")
        for column in x.columns:
            if column.find("AMOUNT") != -1:
                x[column] = x[column].apply(lambda x: '{:.2f}'.format(x))
        y = json.loads(x.to_json(orient="records"))
        for x in range(0, len(y)):
            if x + 1 < len(y):
                y[x]["RANGE"] = str(y[x]["BUCKET"]) + "-" + str(y[x]["BUCKET"] + int(bucketsize))
            else:
                y[x]["RANGE"] = ">" + str(y[x]["BUCKET"])
        logger.info(y)
        return True, y

    def getAgingReport(self, bucketsize, datasetName):
        logger.info(bucketsize)
        df = None
        if datasetName is not None:
            logger.info(datasetName)
            (concat_str, concat_data) = self.getBusinessContextPerUserNew('dummy')
            df = pandas.DataFrame()
            if concat_str:
                for reconDoc in concat_data['reconData']:
                    reconId, execId = reconDoc['RECON_ID'], reconDoc['RECON_EXECUTION_ID']
                    if DashBoardReports().exists(
                            {"RECON_EXECUTION_ID": execId, 'reportName': 'AgingReport', 'RECON_ID': reconId}):
                        status, doc = DashBoardReports().get(
                            {"RECON_EXECUTION_ID": execId, 'reportName': 'AgingReport'})
                        chunk = pandas.DataFrame(doc['ReportData'])
                        df = pandas.concat([df, chunk])
        if len(df) == 0:
            return False, 'No Data Found'
        records = df.copy()
        records["BUCKET"] = records["TRANSACTIONAGE"] - (records["TRANSACTIONAGE"] % int(bucketsize))
        cols = ['WORKPOOL_NAME', 'RECON_NAME', "RECON_EXECUTION_ID", "RECON_ID", "BUSINESS_CONTEXT_ID",
                "BUCKET",
                "COUNT", "AMOUNT", "EXCEPTION_STATUS"]
        g_cols = ['WORKPOOL_NAME', 'RECON_NAME', "RECON_EXECUTION_ID", "RECON_ID", "BUSINESS_CONTEXT_ID",
                  "BUCKET",
                  "EXCEPTION_STATUS"]
        # Remove all matched transactions if any exists
        records.loc[records.loc[:, 'EXCEPTION_STATUS'] == 'CLOSED', 'EXCEPTION_STATUS'] = None
        x = records[cols].groupby(g_cols).sum().reset_index()
        y = json.loads(x.to_json(orient="records"))
        for x in range(0, len(y)):
            if x + 1 < len(y):
                y[x]["RANGE"] = str(y[x]["BUCKET"]) + "-" + str(y[x]["BUCKET"] + int(bucketsize))
            else:
                y[x]["RANGE"] = ">" + str(y[x]["BUCKET"])
        logger.info(y)
        return True, y

    def customProcess(self, data, metadata=None):
        df = data["df"]
        business_df = ReconUtility().get_business_details()

        levelkey = data["levelkey"]
        if levelkey == "TXN_MATCHING_STATUS" and "TXN_MATCHING_STATUS_x" in df.columns:
            df['TXN_MATCHING_STATUS'] = df['TXN_MATCHING_STATUS_y']
            del df['TXN_MATCHING_STATUS_x']
            del df['TXN_MATCHING_STATUS_y']

        columns = []
        indexcolums = []
        # print metadata["tree"]
        for key in metadata["tree"]:
            if key == levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        if levelkey == 'BUSINESS_CONTEXT_ID':
            columns.append("RECON_ID")

        dfOutstanding = df.copy()
        if 'TXN_MATCHING_STATUS_y' in dfOutstanding.columns:
            dfOutstanding = dfOutstanding[(dfOutstanding['TXN_MATCHING_STATUS_y'] != 'AUTOMATCHED') &
                                          (dfOutstanding['TXN_MATCHING_STATUS_y'] != 'FORCE_MATCHED') &
                                          (dfOutstanding['TXN_MATCHING_STATUS_y'] != 'ROLLBACK_AUTHORIZATION_PENDING')]
        # print columns
        dfOutstanding = dfOutstanding[columns]
        dfOutstanding["COUNT"] = 1
        if 'SOURCE_NAME' in indexcolums and levelkey != "SOURCE_NAME":
            indexcolums.remove('SOURCE_NAME')
        if 'ACCOUNT_NAME' in indexcolums and levelkey != "ACCOUNT_NAME":
            indexcolums.remove('ACCOUNT_NAME')
        if levelkey == 'BUSINESS_CONTEXT_ID':
            indexcolums.append("RECON_ID")

        if 'EXCEPTION_STATUS' in dfOutstanding.columns:
            dfOutstanding.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'
        # print indexcolums
        if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding['DEBIT_CREDIT_INDICATOR'] is not None:
            dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                               columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": len})
        # print dfOutstanding
        if 'AMOUNT' in dfOutstanding.columns:
            dfOutstanding['AMOUNT'].fillna(0.0, inplace=True)

        if 'COUNT' in dfOutstanding.columns:
            dfOutstanding['COUNT'].fillna(0, inplace=True)

        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"] = 0.0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"] = 0.0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0.0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0.0
        # df["UNKNOWN"]=dfOutstanding["AMOUNT"][" "]
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["NETOUTSTANDINGCOUNT"] = dfOutstanding["CREDITCOUNT"] + dfOutstanding["DEBITCOUNT"]

        dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1)
        if dfOutstanding.empty:
            return dfOutstanding.to_json(orient="records")
        if levelkey != "Summary":
            dfOutstanding.columns = dfOutstanding.columns.droplevel(1)
            dfOutstanding.reset_index(inplace=True)
            dfOutstanding.set_index(indexcolums[-1])
        else:
            dfOutstanding = dfOutstanding.unstack(level=-1)
            dfOutstanding = dfOutstanding.reset_index()

        if levelkey == "BUSINESS_CONTEXT_ID" and not dfOutstanding.empty:
            df2 = business_df[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        else:
            pass

        if levelkey == "RECON_ID" and not dfOutstanding.empty:
            df2 = business_df[['RECON_ID', 'RECON_NAME']]
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        else:
            pass

        if levelkey == "Summary":
            df1 = df
            # compute carry forward count, if any exists
            df_carry_forward = df1.loc[:, ['CARRY_FORWARD_FLAG', 'RECON_ID']]
            df1.drop('CARRY_FORWARD_FLAG', axis=1, inplace=True)
            df_carry_forward['CARRY_FORWARD_COUNT'] = df_carry_forward['CARRY_FORWARD_FLAG'].apply(
                lambda x: True if x == 'Y' else False)
            df_carry_fwd_cnt = df_carry_forward.groupby('RECON_ID').agg({'CARRY_FORWARD_COUNT': sum}).reset_index()

            df_stdt = pandas.DataFrame()
            df_stdt['EXECUTION_DATE_TIME'] = df1['EXECUTION_DATE_TIME']
            df_stdt['RECON_ID'] = df1['RECON_ID']
            df_stdt.drop_duplicates(['RECON_ID'], keep='last', inplace=True)
            df1["AC"] = 1
            df1 = df1.groupby(['RECON_ID', 'BUSINESS_CONTEXT_ID', 'TXN_MATCHING_STATUS'], as_index=False).agg(
                {"AC": len})

            df1 = pandas.merge(df1, df, on=["RECON_ID", "BUSINESS_CONTEXT_ID"], how="left")
            df_unique = pandas.DataFrame()
            df_unique['RECON_ID'] = df1['RECON_ID']
            df_unique['BUSINESS_CONTEXT_ID'] = df1['BUSINESS_CONTEXT_ID']
            df_unique['TXN_MATCHING_STATUS'] = df1['TXN_MATCHING_STATUS_x']
            df_unique['AC'] = df1['AC_x']
            df_unique['RECON_EXECUTION_ID'] = df1['RECON_EXECUTION_ID']
            # df_unique['STATEMENT_DATE'] = df1['STATEMENT_DATE']
            df_unique['EXECUTION_DATE_TIME'] = df1['EXECUTION_DATE_TIME']
            df_unique = df_unique.drop_duplicates(subset=['RECON_ID', 'TXN_MATCHING_STATUS'])
            # logger.info(df_unique)

            df1 = pandas.pivot_table(df_unique, index=['RECON_ID', 'BUSINESS_CONTEXT_ID', 'TXN_MATCHING_STATUS'],
                                     values='AC',
                                     fill_value=0)

            df1 = df1.unstack(level=2)

            # dfOutstanding.columns = [''.join(list(col)) for col in dfOutstanding.columns.values]

            df1 = df1.reset_index()
            dfOutstanding = df1

            dfTOSend = pandas.DataFrame()
            dfTOSend['RECON_ID'] = dfOutstanding['RECON_ID']
            colToSend = ['AUTOMATCHED', 'FORCE_MATCHED', 'UNMATCHED', 'AUTHORIZATION_PENDING',
                         'ROLLBACK_AUTHORIZATION_PENDING',
                         'AUTHORIZATION_SUBMISSION_PENDING', 'UNMATCHED_INVESTIGATION_PENDING',
                         'NETOUTSTANDINGAMOUNTAUTOMATCHED']
            for col in colToSend:
                if col in dfOutstanding.columns:
                    dfTOSend[col] = dfOutstanding[col]

            if 'NETOUTSTANDINGAMOUNTAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['AUTOMATCHED AMOUNT'] = dfOutstanding['NETOUTSTANDINGAMOUNTAUTOMATCHED']
            if 'NETOUTSTANDINGAMOUNTUNMATCHED' in dfOutstanding.columns:
                dfTOSend['UNMATCHED AMOUNT'] = dfOutstanding['NETOUTSTANDINGAMOUNTUNMATCHED']
            if 'NETOUTSTANDINGAMOUNTAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['AUTHORIZATION_PENDING AMOUNT'] = dfOutstanding['NETOUTSTANDINGAMOUNTAUTHORIZATION_PENDING']
            dfTOSend['BUSINESS_CONTEXT_ID'] = dfOutstanding['BUSINESS_CONTEXT_ID']
            if 'COUNTCAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_AUTHORIZATION_PENDING'] = dfOutstanding['COUNTCAUTHORIZATION_PENDING']
            if 'COUNTCAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_AUTOMATCHED'] = dfOutstanding['COUNTCAUTOMATCHED']
            if 'COUNTCUNMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_UNMATCHED'] = dfOutstanding['COUNTCUNMATCHED']
            if 'COUNTDAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_AUTHORIZATION_PENDING'] = dfOutstanding['COUNTDAUTHORIZATION_PENDING']
            if 'COUNTDAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_AUTOMATCHED'] = dfOutstanding['COUNTDAUTOMATCHED']
            if 'COUNTDUNMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_UNMATCHED'] = dfOutstanding['COUNTDUNMATCHED']
            if 'CREDITAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['CREDIT_AUTHORIZATION_PENDING_AMOUNT'] = dfOutstanding['CREDITAUTHORIZATION_PENDING']
            if 'CREDITAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_AUTOMATCHED_AMOUNT'] = dfOutstanding['CREDITAUTOMATCHED']
            if 'DEBITUNMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_UNMATCHED_AMOUNT'] = dfOutstanding['DEBITUNMATCHED']
            if 'DEBITAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['DEBIT_AUTHORIZATION_PENDING_AMOUNT'] = dfOutstanding['DEBITAUTHORIZATION_PENDING']
            if 'DEBITAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_AUTOMATCHED_AMOUNT'] = dfOutstanding['DEBITAUTOMATCHED']
            if 'DEBITUNMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_UNMATCHED_AMOUNT'] = dfOutstanding['DEBITUNMATCHED']
            # print '------------->',dfOutstanding
            (concat_str, concat_data) = self.getBusinessContextPerUserNew('join')
            # print '*'*100
            if concat_str and concat_data != []:
                df2 = business_df[business_df['RECON_ID'].isin(concat_data['recon_ids'])]
                df2 = df2[['WORKPOOL_NAME', 'RECON_NAME', 'RECON_ID']]
                # df2['BUSINESS_CONTEXT_ID'] = df2['BUSINESS_CONTEXT_ID'].astype(str).str.replace('.0','')
            else:
                df2 = business_df[['WORKPOOL_NAME', 'RECON_NAME', 'RECON_ID']]

            dfTOSend = pandas.merge(dfTOSend, df2, on=["RECON_ID"], how="right")
            if 'RECON_ID_y' in dfTOSend.columns:
                dfTOSend['RECON_ID'] = dfTOSend['RECON_ID_y']
            (concat_str, concat_data) = self.getBusinessContextPerUserNew('dummy2')
            if concat_str:
                df3 = business_df[business_df['RECON_ID'].isin(concat_data['recon_ids'])]
                df3 = df3[['RECON_NAME', 'RECON_ID']]
                # df3 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'"+concat_data+"",recon_con)
                # df3['BUSINESS_CONTEXT_ID'] = df3['BUSINESS_CONTEXT_ID'].astype(str).str.replace('.0', '')
                status, df_exec = sql.getLatestExecutionLogNew(concat_data)
            else:
                df3 = business_df[['RECON_NAME', 'RECON_ID']]
                # df3 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",recon_con)

                # update latest statement date from recon_execution_details_log
            if not df_exec.empty:
                dfTOSend = pandas.merge(dfTOSend, df_exec, on=["RECON_ID"], how="left")

            # update carry forward count
            if not df_carry_fwd_cnt.empty:
                dfTOSend = pandas.merge(dfTOSend, df_carry_fwd_cnt, on=['RECON_ID'], how="left")

            # logger.info(df3.columns)
            dfTOSend = pandas.merge(dfTOSend, df3, on=["RECON_ID"], how="left")
            recon_name = dfTOSend['RECON_NAME_x']
            dfTOSend.drop(labels=['RECON_NAME_x', 'RECON_NAME_y'], axis=1, inplace=True)
            dfTOSend.insert(1, 'RECON_NAME', recon_name)
            dfTOSend = pandas.merge(dfTOSend, df_stdt, on=['RECON_ID'], how="right")
            # logger.info(dfTOSend.columns)
            # execution_date_time order
            execution_Date = dfTOSend['EXECUTION_DATE_TIME']
            dfTOSend.drop(labels=['EXECUTION_DATE_TIME'], axis=1, inplace=True)
            dfTOSend.insert(2, 'EXECUTION_DATE_TIME', execution_Date)
            # statement_date order
            statement_Date = dfTOSend['STATEMENT_DATE']
            dfTOSend.drop(labels=['STATEMENT_DATE'], axis=1, inplace=True)
            dfTOSend.insert(3, 'STATEMENT_DATE', statement_Date)
            dfTOSend = dfTOSend[dfTOSend['BUSINESS_CONTEXT_ID'] != '1111131119']
            dfTOSend = dfTOSend.fillna(0)
            # dfTOSend.to_csv("Decimal.csv")
            for column in dfTOSend.columns:
                if column.find("AMOUNT") != -1:
                    dfTOSend[column] = dfTOSend[column].apply(lambda x: '{:.2f}'.format(x))
            # if "STATEMENT_DATE" in dfTOSend.columns:
            #     del dfTOSend['STATEMENT_DATE']
            #     dfTOSend = pandas.merge(dfTOSend,df_statement,on=['RECON_ID'],how='left')
            # logger.info(dfTOSend)

            return dfTOSend.to_json(orient="records")
        if 'COUNT' in dfOutstanding.columns:
            del dfOutstanding['COUNT']
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        return dfOutstanding.to_json(orient="records")

    def getNetOutstandingAmount(self):
        df = self.queryToDataframe(
            "select business_context_id, recon_id, recon_execution_id, statement_date, execution_date_time, source_type, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'")
        # print df
        # dfUnmatched=df.ix[(df["MATCHING_STATUS"]=="UNMATCHED") & (df["RECONCILIATION_STATUS"]=="EXCEPTION")]
        # dfUnmatched=dfUnmatched[["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        # dfUnmatched=dfUnmatched.groupby(["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR"]).sum()

        # print dfUnmatched.reset_index()

        # Business context level
        dfOutstanding = df[["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS", "DEBIT_CREDIT_INDICATOR", "AMOUNT"]]
        dfOutstanding["COUNT"] = 1
        # dfOutstanding=dfOutstanding.groupby(["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR"])
        dfOutstanding = pandas.pivot_table(dfOutstanding, index=['BUSINESS_CONTEXT_ID', 'RECONCILIATION_STATUS'],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len}).reset_index()
        # dfOutstanding=dfOutstanding.sum()
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["AMOUNT"]["D"] - dfOutstanding["AMOUNT"]["C"]
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
                # print dfOutstanding

    def getTransactionSummaryByDate_old(self, reconName=None):
        logger.info("Recon name passed is " + reconName)
        # reconName=None
        indexColumns = ["BUSINESS_CONTEXT_ID", "RECON_ID", "STATEMENT_DATE", "TXN_MATCHING_STATUS",
                        "DEBIT_CREDIT_INDICATOR", "AMOUNT"]
        # df=self.df
        (status, recon_con) = sql.getConnection()
        (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
        df = pandas.read_sql(
            "select business_context_id, recon_id, recon_execution_id,account_number,account_name, statement_date, execution_date_time, source_type, source_name, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T' " + concat_data,
            recon_con)
        # logger.info(df)
        # statesdf=pandas.read_csv("meta/states.csv")
        df = pandas.merge(df, self.statesdf, on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                                 "AUTHORIZATION_STATUS",
                                                 "FORCEMATCH_AUTHORIZATION"], how="left")
        logger.info(df.columns)
        if reconName and reconName != "dummy":
            df = df[df["RECON_ID"] == reconName]
            indexColumns.remove("RECON_ID")
            indexColumns.remove("BUSINESS_CONTEXT_ID")
        summarizer = df[indexColumns]
        # ["RECON_NAME", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "AMOUNT", "DEBIT_CREDIT_INDICATOR"]]
        # summarizer=self.df[["RECON_NAME","STATEMENT_DATE","TXN_MATCHING_STATUS","AMOUNT","DEBIT_CREDIT_INDICATOR"]]
        # summarizer=summarizer.groupby(indexColumns[0:-1]).sum().reset_index()
        # print summarizer
        # summarizer = self.df[indexColumns]

        summarizer["COUNT"] = 1
        summarizer = summarizer[pandas.notnull(summarizer['STATEMENT_DATE'])]
        summarizer = summarizer[summarizer['STATEMENT_DATE'].apply(lambda x: True if x.year != 0001 else False)]

        dfOutstanding = pandas.pivot_table(summarizer, index=indexColumns[0:-2],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len}).reset_index()

        dfOutstanding.fillna(0, inplace=True)

        # logger.info(dfOutstanding)
        presentColumns = []
        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"] = 0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"] = 0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["TOTALCOUNT"] = dfOutstanding["DEBITCOUNT"] + dfOutstanding["CREDITCOUNT"]
        dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1)
        dfOutstanding.columns = dfOutstanding.columns.droplevel(1)

        dfOutstanding = dfOutstanding[indexColumns[0:-2] + ["TOTALCOUNT", "NETOUTSTANDINGAMOUNT"]]
        dfOutstanding.reset_index(inplace=True)
        # logger.info(dfOutstanding.columns)
        if 'RECON_ID' in dfOutstanding.columns:
            df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                  recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
            df3 = pandas.read_sql(
                "select workpool_name,business_context_id from workpool where record_status ='ACTIVE'", recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df3, on=["BUSINESS_CONTEXT_ID"], how="left")
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        return True, dfOutstanding.to_json(orient="records")

    def getTransactionSummaryByDate(self, reconName=None):
        logger.info("Recon name passed is " + reconName)
        # reconName=None
        indexColumns = ["BUSINESS_CONTEXT_ID", "RECON_ID", "STATEMENT_DATE", "TXN_MATCHING_STATUS",
                        "DEBIT_CREDIT_INDICATOR", "AMOUNT"]
        (concat_str, concat_data) = self.getBusinessContextPerUserNew('dummy')
        colNames = indexColumns
        fileLoc = config.hdfFilesPathNew
        query = ''
        df = pandas.DataFrame()
        if concat_str:
            for reconDoc in concat_data['reconData']:
                reconId, execId = reconDoc['RECON_ID'], reconDoc['RECON_EXECUTION_ID']
                print reconId, execId
                if DashBoardReports().exists(
                        {"RECON_EXECUTION_ID": execId, 'reportName': 'TransactionSummaryByDate', 'RECON_ID': reconId}):
                    status, doc = DashBoardReports().get(
                        {"RECON_EXECUTION_ID": execId, 'reportName': 'TransactionSummaryByDate'})
                    chunk = pandas.DataFrame(doc['ReportData'])
                    print chunk
                    df = pandas.concat([df, chunk])
                    # hdfStoreName = fileLoc + reconDoc['RECON_ID'] + '/' + execId  + '/' + execId + '.h5'
                    # hdfStoreKey = '/TXN_DATA'
                    # jobsList = ReconWorker(str(reconId)).zfsQueue.get_job_ids()
                    # if jobsList is not None and len(jobsList) != 0:
                    #     continue
                    # if os.path.exists(hdfStoreName):
                    #     with HDFStore(hdfStoreName, 'r') as hdf:
                    #         if hdfStoreKey in hdf.keys():
                    #             for chunk in hdf.select(hdfStoreKey, chunksize=config.hdf5_chunk_size):  # ,columns=colNames
                    #                 df = pandas.concat(
                    #                     [df, chunk[(chunk['RECORD_STATUS'] == 'ACTIVE') & (chunk['ENTRY_TYPE'] == 'T')]],
                    #                     ignore_index=True)
                    #                 del chunk

        if len(df) == 0:
            return False, 'No Data Found'
        logger.info(df.columns)
        if reconName and reconName != "dummy":
            df = df[df["RECON_ID"] == reconName]
            indexColumns.remove("RECON_ID")
            indexColumns.remove("BUSINESS_CONTEXT_ID")
        summarizer = df.copy()

        dfOutstanding = pandas.pivot_table(summarizer, index=indexColumns[0:-2],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": np.sum}, fill_value=0.0).reset_index()

        dfOutstanding.fillna(0, inplace=True)
        df23 = dfOutstanding.copy()
        df23 = df23.reset_index(col_level=1)
        cols = []
        for (s1, s2) in df23.columns.tolist():
            cols.append(s1 + str(s2))
        df23.columns = cols
        df23.rename(
            columns={'COUNTC': 'CREDITCOUNT', 'COUNTD': 'DEBITCOUNT', 'AMOUNTC': 'CREDIT', 'AMOUNTD': 'DEBIT'},
            inplace=True)
        dfOutstanding = df23.copy()
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["TOTALCOUNT"] = dfOutstanding["DEBITCOUNT"] + dfOutstanding["CREDITCOUNT"]

        dfOutstanding = dfOutstanding[indexColumns[0:-2] + ["TOTALCOUNT", "NETOUTSTANDINGAMOUNT"]]
        dfOutstanding.index = range(1, len(dfOutstanding) + 1)

        # if "C" in dfOutstanding["AMOUNT"]:
        #     dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        # else:
        #     dfOutstanding["CREDIT"] = 0
        # if "D" in dfOutstanding["AMOUNT"]:
        #     dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        # else:
        #     dfOutstanding["DEBIT"] = 0
        # if "C" in dfOutstanding["COUNT"]:
        #     dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        # else:
        #     dfOutstanding["CREDITCOUNT"] = 0
        # if "D" in dfOutstanding["COUNT"]:
        #     dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        # else:
        #     dfOutstanding["DEBITCOUNT"] = 0
        # dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        # dfOutstanding["TOTALCOUNT"] = dfOutstanding["DEBITCOUNT"] + dfOutstanding["CREDITCOUNT"]
        # dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1)
        # dfOutstanding.columns = dfOutstanding.columns.droplevel(1)
        #
        # dfOutstanding = dfOutstanding[indexColumns[0:-2] + ["TOTALCOUNT", "NETOUTSTANDINGAMOUNT"]]
        # dfOutstanding.reset_index(inplace=True)
        # logger.info(dfOutstanding.columns)
        if 'RECON_ID' in dfOutstanding.columns:
            business_df = ReconUtility().get_business_details()
            df2 = business_df[business_df['RECON_ID'].isin(concat_data['recon_ids'])]
            df2 = df2[['WORKPOOL_NAME', 'RECON_ID', 'RECON_NAME']]
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
        # for column in dfOutstanding.columns:
        #     if column.find("AMOUNT") != -1:
        #         dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        return True, dfOutstanding.to_json(orient="records")

    def getCreditOutstandingAmount(self):
        pass

    def getDebitOutstandingAmount(self):
        pass

    def getAutoMatching(self):
        pass

    def getRollbackTransactionsWaitingForAuthorizations(self):
        pass

    def getOutstandingEntriesInOpenStatus(self):
        pass

    def getWaitingForInvestigationAndResolution(self):
        pass

    def getPendingForSubmission(self):
        pass

    def getWaitingForAuthorization(self):
        pass

    def getRbiDadSummaryReport(self, id):
        statementDate = id
        feedPath = config.flaskPath + 'rbi_dad_report'
        frames = []
        # Read File For RBIDATA
        fpattern = "RBIDAD_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        rbi_feed_name = files[0]
        rbi_dad_df = pandas.read_excel(feedPath + os.sep + rbi_feed_name)
        frames.append(rbi_dad_df)

        # Read File For CBS
        fpattern = "ALGO-GL-OUTFILE_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        cbs_feed_name = files[0]
        cbs_df = pandas.read_csv(feedPath + os.sep + cbs_feed_name, sep='|', error_bad_lines=False, header=None)
        frames.append(cbs_df)

        # Read File For OGL
        fpattern = "TRIALBAL_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        ogl_feed_name = files[0]
        ogl_df = pandas.read_csv(feedPath + os.sep + ogl_feed_name, sep='|', error_bad_lines=False)

        frames.append(ogl_df)
        if any([len(frame) == 0 for frame in frames]) == True:
            return False, "No data found"

        lookUpConditionDf = rbi_dad_df.loc[rbi_dad_df[rbi_dad_df.columns[3]] == 'Closing Balance']
        rbi_records = json.loads(lookUpConditionDf.reset_index().fillna(0).to_json(orient='records'))

        displayTable = []
        for ind in range(0, len(rbi_records)):
            rbi_dad_indicator, ogl_cbs_indicator = '', ''
            rbi_closing_balance, cbs_closing_balance, ogl_closing_balance = 0, 0, 0
            rbi_record = rbi_records[ind]
            rbi_closing_balance = rbi_record[rbi_dad_df.columns[7]]
            rbi_closing_balance = float("".join(re.split('\,', rbi_closing_balance)))
            rbi_dad_indicator = rbi_record[rbi_dad_df.columns[8]]
            ogl_cbs_indicator = 'DR' if rbi_dad_indicator == 'CR' else 'CR'
            # CBS Match Scenario
            try:
                accountToMatch = 98026102018
                cbsConditionDf = cbs_df.loc[
                    (cbs_df[cbs_df.columns[0]] == 'H') & (cbs_df[cbs_df.columns[2]] == accountToMatch) & (
                        cbs_df[cbs_df.columns[5]] == ogl_cbs_indicator)]
                cbsConditionDf.columns = [str(i) for i in cbsConditionDf.columns]

                if len(cbsConditionDf):
                    cbs_record = json.loads(cbsConditionDf.reset_index().fillna(0).to_json(orient='records'))[0]
                    cbs_closing_balance = cbs_record[str(cbsConditionDf.columns[4])]

                    if type(cbs_closing_balance) == 'str':
                        float("".join(re.split('\,', cbs_closing_balance)))
                        # cbs_closing_balance = round(
                        #    float("".join(re.split('\,', str(cbs_record[str(cbsConditionDf.columns[4])])))), 2)
            except Exception, e:
                return False, str(e)

            # OGL Match Scenario
            try:
                ogl_source_col = ogl_df.columns[0]  # 'SOURCE'  # ogl_df.columns[0]
                ogl_acc_no__col = ogl_df.columns[1]  # 'GL_NO'  # ogl_df.columns[1]
                ogl_ind_col = ogl_df.columns[5]  # 'DEBIT_CREDIT_FLAG'  # ogl_df.columns[5]
                ogl_balance__col = ogl_df.columns[6]  # 'BALANCE_AMT'  # ogl_df.columns[6]

                ogl_dfConditionDf = ogl_df.loc[
                    (ogl_df[ogl_source_col] == 'CBS') & (ogl_df[ogl_acc_no__col].str.contains('41201'))]

                # ogl_dfConditionDf = ogl_dfConditionDf.loc[
                #    ogl_dfConditionDf[ogl_acc_no__col].apply(
                #        lambda x: True if len(str(x).split('41201', 3)[0]) else False)]
                ogl_dfConditionDf = ogl_dfConditionDf.loc[(ogl_df[ogl_acc_no__col].str[12:17] == '41201')]
                if len(ogl_dfConditionDf):
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf[ogl_balance__col].map(
                        lambda x: float("".join(re.split('\,', '%.2f' % x))))
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf.apply(
                        lambda x: x[ogl_balance__col] * -1 if x[ogl_ind_col] == 'CR' else  x[ogl_balance__col],
                        axis=1)
                    ogl_closing_balance = sum(ogl_dfConditionDf[ogl_balance__col])
                    # ogl_closing_balance = round(
                    #    sum(ogl_dfConditionDf[ogl_balance__col].map(lambda x: float("".join(re.split('\,', str(x)))))),
                    #    2)
            except Exception, e:
                return False, str(e)

            # format statemtn date
            frmtStmtDate = datetime.datetime.strptime(str(statementDate), '%d%m%Y').strftime("%d/%m/%Y")

            obj = dict()
            obj['status'] = True if (rbi_closing_balance) == (cbs_closing_balance) == (ogl_closing_balance) else False
            displaycontent = [{'Indicator': rbi_dad_indicator, 'Closing_Balance': (cbs_closing_balance),
                               'Source': 'RBI Mirror Account Closing balance as of ' + str(frmtStmtDate)}]
            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': (rbi_closing_balance),
                 'Source': 'Closing Balance of RBI DAD Account as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': cbs_closing_balance - rbi_closing_balance,
                 'Source': 'Net Difference between RBI Mirror and RBI DAD a/c as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': (ogl_closing_balance),
                 'Source': 'OGL Balance as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': cbs_closing_balance - ogl_closing_balance,
                 'Source': 'Difference between RBI Mirror and OGL as of ' + str(frmtStmtDate)})

            obj['table'] = displaycontent
            displayTable.append(obj)
        return True, displayTable

    # Balance Reports
    def generateCustomReports(self, id, query):
        out_data = dict()
        reconId = query['reconId']
        functionName = query['functionName']
        statementDate = query['statementDate']
        print functionName
        out_data = eval(str(functionName))

        if len(out_data) > 0:
            return True, out_data
        else:
            return False, "No Data Found."

    # retrieve distinct currency from mongo for nostro reports
    def getDistinctCurrency(self, id, query):
        dis_currecy = NostroReport().distinct('CURRENCY')
        logger.info(dis_currecy)
        return True, dis_currecy

    # Nostro Reports
    def getNostroReportDetails(self, id, query):
        # if no query param passed, retrieve data for latest execution ID
        reportExtract = {'total': 0}
        reportData = []

        if query is not None and query == {}:
            (status, connection) = sql.getConnection()
            if status and connection is not None:
                recon_execution_id = sql.getLatestExecutionID(connection, 'TRSC_CASH_APAC_20181')
                logger.info(recon_execution_id)
                (status_b, qry_rst) = NostroReport().getAll(
                    {'perColConditions': {'execID': str(recon_execution_id)}})
                if qry_rst['total'] > 0:
                    reportData = qry_rst['data']
        else:
            # query based on the param passed
            q = {}
            query_result = []
            logger.info(query)
            q = json.loads(query, encoding='utf-8')
            logger.info("Nostro search query...")
            logger.info(q)
            reportData = list(NostroReport().find(q))

        if len(reportData) > 0:
            reportExtract['total'] = len(reportData)
            report_df = pandas.read_json(json.dumps(reportData))

            # order dataframe columns
            report_df = report_df.loc[:,
                        ['REPORT DATE', 'STATEMENT DATE', 'ACCOUNT NO', 'ACCOUNT NAME', 'CURRENCY',
                         'OP BAL DR / CR', 'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE']]

            report_df.fillna(0, inplace=True)

            report_df.loc[:, 'REPORT DATE'] = report_df.loc[:, 'REPORT DATE']. \
                apply(lambda x: datetime.datetime.fromtimestamp(x).strftime('%Y/%m/%d %H:%M:%S'))

            # reportExtract['pdf_file_path'] = self.generateNostroPDFReport(report_df)
            reportExtract['csv_file_path'] = self.generateNostroCSVReport(report_df.copy())
            reportExtract['excel_file_path'] = self.generateNostroExcelReport(report_df.copy())

            report_df = report_df.rename(
                columns={'REPORT DATE': 'REP_DATE', 'STATEMENT DATE': 'STMT_DATE', 'ACCOUNT NO': 'ACCOUNT_NUMBER',
                         'ACCOUNT NAME': 'ACC_NAME', 'OPENING BALANCE': 'OP_BAL', 'CLOSING BALANCE': 'CL_BAL',
                         'OP BAL DR / CR': 'OP_DR_CR',
                         'CL BAL DR / CR': 'CL_DR_CR'})
            reportExtract['reportData'] = report_df.to_json(orient="records")

        return True, reportExtract

    def generateNostroExcelReport(self, nostro_report):
        object_id = str(flask.session["sessionData"]['_id'])
        nostro_file_name = 'Nostro_Closing_balance.xlsx'
        nostro_file_Path = ''

        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports')):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports'))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + '*.xlsx'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        nostro_file_Path = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + nostro_file_name

        nostro_report.loc[:, 'OPENING BALANCE'] = nostro_report.loc[:, 'OPENING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.loc[:, 'CLOSING BALANCE'] = nostro_report.loc[:, 'CLOSING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.to_excel(nostro_file_Path, sheet_name='Nostro Balance Report', index=False)
        # writer.save()
        return object_id + '/nostro_reports/' + nostro_file_name

    # def generateNostroPDFReport(self, nostro_report):
    #     object_id = str(flask.session["sessionData"]['_id'])
    #     nostro_file_name = 'Nostro_Closing_balance.pdf'
    #     nostro_file_Path = ''
    #
    #     if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports')):
    #         os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports'))
    #
    #     file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + '*.pdf'
    #     fileName = glob.glob(file_name)
    #     for file in fileName:
    #         os.remove(file)
    #
    #     env = Environment(loader=FileSystemLoader('.'))
    #     template = env.get_template("nostro_report_template.html")
    #     template_vars = {"title": "Nostro Report Details",
    #                      "nostro_report_data": nostro_report.to_html(index=False)}
    #     html_out = template.render(template_vars)
    #     nostro_file_Path = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + nostro_file_name
    #     HTML(string=html_out).write_pdf(nostro_file_Path)
    #
    #     return object_id + '/nostro_reports/' + nostro_file_name

    def generateNostroCSVReport(self, nostro_report):
        object_id = str(flask.session["sessionData"]['_id'])
        nostro_file_name = 'Nostro_Closing_balance.csv'
        nostro_file_Path = ''

        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports')):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports'))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + '*.csv'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        nostro_report.loc[:, 'OPENING BALANCE'] = nostro_report.loc[:, 'OPENING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.loc[:, 'CLOSING BALANCE'] = nostro_report.loc[:, 'CLOSING BALANCE'].astype(float).map(
            '{:,.2f}'.format)

        nostro_file_Path = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + nostro_file_name
        nostro_report.to_csv(nostro_file_Path, index=False)

        return object_id + '/nostro_reports/' + nostro_file_name

    def getConsolidatedReport(self, id, query):
        reconId = query['reconId']
        status, recon_execution = ReconExecutionDetailsLog().getReconWiseMaxExecution(reconId)
        if status:
            recon_execution = pandas.DataFrame(recon_execution)
            cursor = DashBoardReports().find(
                {"RECON_EXECUTION_ID": {'$in': recon_execution['max'].tolist()}, "reportName": "OutStandingAmount"})

            if cursor.count() > 0:
                report_data = []
                distinct_src = []
                for item in cursor:
                    report_data.extend(item['ReportData'])
                report_data = pandas.DataFrame(report_data)
                print report_data.head()
                distinct_src = report_data['SOURCE_NAME'].unique().tolist()

                report_data = pandas.pivot_table(report_data, columns=['SOURCE_NAME'], index=['RECON_ID'],
                                                 values=['AMOUNT', 'COUNT'],
                                                 aggfunc=[np.sum], fill_value=0)

                report_data.columns = ['_'.join(list(col)) for col in report_data.columns.values]
                report_data = report_data.reset_index()
                output_dict = dict()
                output_dict['distinct_src'] = distinct_src
                output_dict['report_data'] = report_data.to_json(orient='records')
                return True, output_dict
            else:
                return True, {}

    def exportStatus(self, id, query):
        reconId = query['reconId']
        filters = query['filters']
        recon_name = query['recon_name']
        sourceName = query['source']
        execLog = ReconExecutionDetailsLog().getLatestExeDetails(query['reconId'])
        clonePath = execLog['READ_CLONE_PATH']

        iterator = HDF5Interface(reconId, execLog['RECON_EXECUTION_ID'], clonePath).fetch_all()
        data = pandas.DataFrame()
        for frame in iterator:
            data = pandas.concat([data, frame], ignore_index=True)

        # exec filters[sourceName]
        data.drop(config.reportCols, errors='ignore', axis=1, inplace=True)
        if sourceName is not None:
            data = data[data['SOURCE_NAME'] == sourceName]
            status, colToDownload = DashBoardReports().get({"RECON_ID": reconId, 'reportName': 'ExportColumns'})
            if status:
                if sourceName + '_COLUMNS' in colToDownload:
                    cols = colToDownload[sourceName + '_COLUMNS']
                    data = data[cols].copy()

        object_id = str(flask.session["sessionData"]['_id'])
        fileName = 'Report.csv'
        if len(data):
            status, columns = ReconAssignment().getReconColData('', {'recon_id': reconId})
            if status:
                recon_cols = pandas.read_json(columns)
                data.index = range(1, len(data) + 1)
                for col in data.columns:
                    if not col.startswith('AMOUNT') and 'DATE' not in col:
                        data.loc[data[(~data[col].isnull())].index, col] = "'" + data[col].astype(str)
                colspec = dict(zip(recon_cols['MDL_FIELD_ID'], recon_cols['UI_DISPLAY_NAME']))
                data.rename(columns=colspec, inplace=True)

            pth = config.contentDir + '/' + "recon_reports/" + "/" + object_id + "/"
            if not os.path.exists(pth): os.mkdir(pth)
            with open(pth + fileName, 'w') as outcsv:
                writer = csv.writer(outcsv)
                writer.writerows(
                    [['Recon_Name :', recon_name],
                     ['Report On', execLog['STATEMENT_DATE']],
                     ['Status', 'ALL'],
                     ['', '']])

                data.to_csv(outcsv, index=False, date_format='%d/%m/%Y')

        return True, object_id + '/' + fileName

    def closingBal(self, id, query):
        reconId = query['reconId']


class FeedLoaderInterface():
    def __init__(self):
        self.mongo_client = MongoClient('mongodb://localhost:27017/')
        self.mongo_collection = self.mongo_client.reconbuilder

    def getReconContextDetails(self, id):
        reconContextList = []
        cc_col = self.mongo_collection['datasource']
        for val in cc_col.find():
            reconContextDict = {}
            # print (val)
            reconContextDict['RECON_NAME'] = val['reconName']
            reconContextDict['RECON_ID'] = val['reconId']
            reconContextDict["SOURCE"] = val['sources'].keys()
            reconContextList.append(reconContextDict)

        return True, reconContextList

    def getMatchingRules(self, id):
        cc_col = self.mongo_collection['matchingrules']
        # print (cc_col.find({'recon_id': id}))


class CustomReports(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(CustomReports, self).__init__("customReports", hideFields)

    def get(self, condition):
        # print condition
        # print self._fixCondition(condition)
        # print self._canAccessCondition(self._fixCondition(condition))
        if self.forcePrimary:
            found = self.find_one(self._canAccessCondition(self._fixCondition(condition)),
                                  read_preference=ReadPreference.PRIMARY)
        else:
            found = self.find_one(self._canAccessCondition(self._fixCondition(condition)))
        if found:
            return True, found
        else:
            return False, "No data found"

    def getAll(self, query={}):
        skip, limit = 0, 10
        if 'skip' in query:
            skip = query['skip']
            del query['skip']
        if 'limit' in query:
            limit = query['limit'] + skip
            del query['limit']
        stats, resp = super(CustomReports, self).getAll(query)
        bizIds = flask.session['sessionData']['businessContextIds']
        df = pandas.DataFrame(resp['data'])
        if 'businessContextId' in df.columns:
            df['businessContextId'] = df['businessContextId'].astype(str)
            df['businessContextId'] = df['businessContextId'].str.replace('.0', '')
            df = df.loc[df[df['businessContextId'].isin(bizIds)].index]
            df.index = range(1, len(df) + 1)
            resp['total'] = len(df)
            df = df[skip:limit]
            resp['current'] = len(df)
            cols = ['reconID', 'businessContextId', 'reportName', 'functionName']
            if 'fileName' in df.columns:cols.append('fileName')
            df.fillna('',inplace=True)
            resp['data'] = df[cols].T.to_dict().values()
        return stats, resp


class CustomValidation():
    # Validation funciton for Retail Assests RA_CASH_APAC_61204
    # Validate Based on Gl_Code
    def RA_61204(self, df):
        if len(df) > 0:
            glCode = []
            df_s1 = df[df['SOURCE_TYPE'] == 'S1']
            df_s2 = df[df['SOURCE_TYPE'] == 'S2']
            glCode += df_s1['TXT_60_001'].astype(int).astype(str).unique().tolist()

            df_s2['C40'] = df_s2['GL_ACCOUNT_NUMBER'].astype(str).str[12:17]
            glCode += df_s2['C40'].unique().tolist()

            # Mismatch in GL-Code
            if len(set(glCode)) > 1:
                return True, ', Mismatch in GL-Code'
            else:
                return False, ''

    # Validation funciton for Retail Assests TW_CASH_APAC_61204
    # Validate Based on Gl_Code
    def TW_61208(self, df):
        if len(df) > 0:
            glCode = []
            df_s1 = df[df['SOURCE_TYPE'] == 'S1']
            df_s2 = df[df['SOURCE_TYPE'] == 'S2']
            glCode += df_s1['TXT_32_001'].astype(int).astype(str).unique().tolist()

            df_s2['C40'] = df_s2['GL_ACCOUNT_NUMBER'].astype(str).str[12:17]
            glCode += df_s2['C40'].unique().tolist()

            # Mismatch in GL-Code
            if len(set(glCode)) > 1:
                return True, ', Mismatch in GL-Code'
            else:
                return False, ''


class CustomReportInterface():
    # RSystem v/s OGL Balance Report (RA_CASH_APAC_61204)
    def RA_61204(self, statementDate):
        (status, connection) = sql.getConnection()
        if status:

            logger.info(statementDate)
            qry = "select TXT_60_001,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount,GL_ACCOUNT_NUMBER" \
                  " from cash_output_txn where recon_id ='RA_CASH_APAC_61204' and record_status= 'ACTIVE' and" \
                  " statement_date = to_date('" + statementDate + "','dd/mm/yy')"

            df = pandas.read_sql(qry, connection)

            if len(df) > 0:

                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']
                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1'].copy()
                dfs1.rename(columns={'TXT_60_001': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2'].copy()
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]

                df = dfs1.append(dfs2, ignore_index=True)
                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = (df.loc[:, 'AMOUNTD'] - df.loc[:, 'AMOUNTC']).abs()
                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()
                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                naturalAccNum = self.getNaturalAcctNum()
                naturalAccDF = pandas.DataFrame(naturalAccNum)[['ACCOUNT_NAME', 'C40']]
                naturalAccDF['C40'] = naturalAccDF['C40'].astype(str)
                df = pandas.merge(df, naturalAccDF, on='C40', how='left')

                df_out = dict()
                df_out['reportData'] = df.to_json(orient='records')
                df_out['sourceDetails'] = sourceDict

                return df_out
            else:
                return {}

    # RSystem v/s CBS Balance Report (RA_CASH_APAC_61207)
    def RA_61207(self, statementDate):
        (status, connection) = sql.getConnection()

        if status:
            qry = "select NATURAL_AC,Account_number,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount" \
                  " from cash_output_txn where recon_id ='RA_CASH_APAC_61207' and record_status= 'ACTIVE'" \
                  " and statement_date = to_date('" + statementDate + "','dd/mm/yy') "

            df = pandas.read_sql(qry, connection)

            if len(df) > 0:
                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']

                accountNumberMap = {'98160102080': '80104', '98159102083': '80117'}

                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1'].copy()
                dfs1.rename(columns={'NATURAL_AC': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2'].copy()
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'ACCOUNT_NUMBER'].apply(lambda x: accountNumberMap[str(x).lstrip('0')])

                df = dfs1.append(dfs2, ignore_index=True)

                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = df.loc[:, 'AMOUNTD'].abs() - df.loc[:, 'AMOUNTC'].abs()
                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()

                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                naturalAccNum = self.getNaturalAcctNum()
                naturalAccDF = pandas.DataFrame(naturalAccNum)[['ACCOUNT_NAME', 'C40']]
                naturalAccDF['C40'] = naturalAccDF['C40'].astype(str)
                df = pandas.merge(df, naturalAccDF, on='C40', how='left')

                df_out = dict()
                df_out['reportData'] = df.to_json(orient='records')
                df_out['sourceDetails'] = sourceDict

                return df_out
            else:
                return {}


                # Indus v/s OGL Balance Report (TW_CASH_APAC_61208)

    def TW_61208(self, statementDate):
        (status, connection) = sql.getConnection()
        if status:
            # exeID = sql.getLatestExecutionID(connection, 'TW_CASH_APAC_61208')
            logger.info(statementDate)

            qry = "select TXT_32_001,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount,GL_ACCOUNT_NUMBER" \
                  " from cash_output_txn where recon_id ='TW_CASH_APAC_61208' and record_status= 'ACTIVE' and statement_date = to_date('" + statementDate + "','dd/mm/yy')"

            df = pandas.read_sql(qry, connection)
            df['AMOUNT'] = df['AMOUNT'].abs()

            if len(df) > 0:

                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']
                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1']
                dfs1.rename(columns={'TXT_32_001': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2']
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]

                df = dfs1.append(dfs2, ignore_index=True)

                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = df.loc[:, 'AMOUNTD'].abs() - df.loc[:, 'AMOUNTC'].abs()

                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()

                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                naturalAccNum = self.getNaturalAcctNum()
                naturalAccDF = pandas.DataFrame(naturalAccNum)[['ACCOUNT_NAME', 'C40']]
                naturalAccDF['C40'] = naturalAccDF['C40'].astype(str)
                df = pandas.merge(df, naturalAccDF, on='C40', how='left')

                df_out = dict()
                df_out['reportData'] = df.to_json(orient='records')
                df_out['sourceDetails'] = sourceDict

                return df_out
            else:
                return {}

    def AB_BNA_PRINC(self, statementData, reconId, store_key='REPORT_DATA'):
        # reconId = 'BNA_CASH_APAC_0001'
        executionDoc = list(ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': reconId,
                                                             'EXECUTION_STATUS': 'Completed',
                                                             'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                             'STATEMENT_DATE': {
                                                                 '$gte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y'),
                                                                 '$lte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y')}}))
        if len(executionDoc) > 0:
            if len(executionDoc) == 1:
                execId = executionDoc[0]['RECON_EXECUTION_ID']
                key_path = executionDoc[0]['READ_CLONE_PATH']
            else:
                excDoc = []
                for i in executionDoc:
                    excDoc.append({'RECON_EXECUTION_ID': i['RECON_EXECUTION_ID'],
                                   'reconId': reconId
                                      , 'READ_CLONE_PATH': i['READ_CLONE_PATH']})
                df = pandas.DataFrame(excDoc)
                df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(float)
                execId = df['RECON_EXECUTION_ID'].max()
                key_path = df['READ_CLONE_PATH'][df['RECON_EXECUTION_ID'] == execId].max()

            iterator = HDF5Interface(reconId, str(execId).replace('.0', ''), key_path).fetch_all(storeKey=store_key)

            df = pandas.DataFrame()
            for item in iterator:
                df = pandas.concat([df, item])
            dateFormatCols = ['TRAN_DATE', 'POST_DATE', 'UPLDDATE', 'STATEMENT DATE']
            # Format date columns
            for col in dateFormatCols:
                if col in df.columns:
                    df[col] = df[col].dt.strftime('%d/%m/%Y')

            if reconId == 'BNA_CASH_APAC_0001':
                return df.to_json(orient='records')

            doc = dict()
            doc['prnpalData'] = df.to_json(orient='records')
            if not os.path.isdir(config.contentDir):
                os.mkdir(config.contentDir)
            ew = pandas.ExcelWriter(config.contentDir + '/' + 'P_ACC_REPORT.xlsx')
            df.to_excel(ew, sheet_name='Principal Report', index=False)
            # Read Settlement report extract from mongo db
            status, data = DashBoardReports().get(
                {"RECON_ID": reconId, "RECON_EXECUTION_ID": str(executionDoc[0]['RECON_EXECUTION_ID']),
                 'reportName': 'AB_Reports'})
            if status:
                df = pandas.DataFrame(data['ReportData'])
                df = df.fillna('')
                doc['settlementData'] = df.T.to_dict().values()

                df.to_excel(ew, sheet_name='Net Outstanding Balance', index=False)
            else:
                doc['settlementData'] = []
            ew.save()
            return doc

        else:
            return "[]"

    def getPositionData(self, statementData, reconId, store_key='POSITION_DATA'):
        executionDoc = list(ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': reconId,
                                                             'EXECUTION_STATUS': 'Completed',
                                                             'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                             'STATEMENT_DATE': {
                                                                 '$gte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y'),
                                                                 '$lte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y')}}))
        if len(executionDoc) > 0:
            if len(executionDoc) == 1:
                execId = executionDoc[0]['RECON_EXECUTION_ID']
                key_path = executionDoc[0]['READ_CLONE_PATH']
            else:
                excDoc = []
                for i in executionDoc:
                    excDoc.append({'RECON_EXECUTION_ID': i['RECON_EXECUTION_ID'],
                                   'reconId': reconId
                                      , 'READ_CLONE_PATH': i['READ_CLONE_PATH']})
                df = pandas.DataFrame(excDoc)
                df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(float)
                execId = df['RECON_EXECUTION_ID'].max()
                key_path = df['READ_CLONE_PATH'][df['RECON_EXECUTION_ID'] == execId].max()

            if store_key == 'POSITION_DATA':
                iterator = HDF5Interface(reconId, str(execId).replace('.0', ''), key_path).fetch_all(storeKey=store_key,
                                                                                                     where_clause="'ENTRY_TYPE' == 'P'")
            else:
                print store_key
                iterator = HDF5Interface(reconId, str(execId).replace('.0', ''), key_path).fetch_all(storeKey=store_key)

            df = pandas.DataFrame()
            for item in iterator:
                df = pandas.concat([df, item])
            dateFormatCols = ['TRAN_DATE', 'POST_DATE', 'UPLDDATE', 'STATEMENT DATE']
            # Format date columns
            for col in dateFormatCols:
                if col in df.columns:
                    df[col] = df[col].dt.strftime('%d/%m/%Y')

            status, colNames = ReconAssignment().getReconColData('', {"recon_id": reconId})
            colNames = pandas.read_json(colNames)
            colNames['UI_DISPLAY_NAME'] = colNames['UI_DISPLAY_NAME'].str.upper()
            cols = dict(zip(colNames['MDL_FIELD_ID'], colNames['UI_DISPLAY_NAME']))
            df.rename(columns=cols, inplace=True)
            if 'RECORD_STATUS' in df.columns: del df['RECORD_STATUS']
            return df.to_json(orient='records')
        else:
            return "[]"

    def AB_NEFT_REPORTS(self, statementData, reconId):
        # reconID = 'EPYMTS_NEFT_INWARD_APAC_0009'
        executionDoc = list(ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': reconId,
                                                             'EXECUTION_STATUS': 'Completed',
                                                             'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                             'STATEMENT_DATE': {
                                                                 '$gte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y'),
                                                                 '$lte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y')}}))
        if len(executionDoc) > 0:
            status, data = DashBoardReports().get({'RECON_ID': reconId, "RECON_EXECUTION_ID":
                str(executionDoc[0]['RECON_EXECUTION_ID']), 'reportName': 'AB_Reports'})
            print status, data
            if status:
                status, colNames = ReconAssignment().getReconColData('', {"recon_id": reconId})
                colNames = pandas.read_json(colNames)
                cols = dict(zip(colNames['MDL_FIELD_ID'], colNames['UI_DISPLAY_NAME']))
                reportData = pandas.DataFrame(data['ReportData'])
                reportData.rename(columns=cols, inplace=True)
                del reportData['RECON_ID']
                return reportData.to_json(orient='records')
            else:
                return "[]"
        else:
            return "[]"

    def getNaturalAcctNum(self):
        status, data = NaturalAccMaster().getAll(query={})
        naturalAccNum = data['data']
        return naturalAccNum

    def checkVal(self, row, sourceDict):
        return 0 if abs(row['CLOSING_BALS1']) - abs(row['CLOSING_BALS2']) == 0 \
            else sourceDict['S1'] if abs(row['CLOSING_BALS1']) > abs(row['CLOSING_BALS2']) else sourceDict['S2']

    def getChargeBackRecords(self,  statementData, reconId):
        stmt = datetime.datetime.strptime(statementData, '%d/%m/%y').strftime('%d%m%Y')
        file_name = config.contentDir + "/" + "recon_reports/" +stmt+os.sep
        df = pandas.DataFrame()
        if os.path.exists(file_name + 'RET File.csv'):
            df = pandas.concat([df,pandas.read_csv(file_name + 'RET file.csv')],ignore_index=True) # query['retFeedFileName']
        if os.path.exists(file_name + 'TCC File.csv'):
            df = pandas.concat([df, pandas.read_csv(file_name + 'TCC File.csv')], ignore_index=True)
        leftFrame = df.copy()
        if len(leftFrame) == 0:
            return 'File does not exits'
        leftFrame['RRN'] = leftFrame['RRN'].str.replace("'", '')

        # reconId = query['reconId']
        # statementData = query['statementData']
        executionDoc = list(ReconExecutionDetailsLog().find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': reconId,
                                                             'EXECUTION_STATUS': 'Completed',
                                                             'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                             'STATEMENT_DATE': {
                                                                 '$gte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y'),
                                                                 '$lte': datetime.datetime.strptime(statementData,
                                                                                                    '%d/%m/%y')}}))
        if len(executionDoc) > 0:
            if len(executionDoc) == 1:
                execId = executionDoc[0]['RECON_EXECUTION_ID']
                key_path = executionDoc[0]['READ_CLONE_PATH']
            else:
                excDoc = []
                for i in executionDoc:
                    excDoc.append({'RECON_EXECUTION_ID': i['RECON_EXECUTION_ID'],
                                   'reconId': reconId
                                      , 'READ_CLONE_PATH': i['READ_CLONE_PATH']})
                df = pandas.DataFrame(excDoc)
                df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(float)
                execId = df['RECON_EXECUTION_ID'].max()
                key_path = df['READ_CLONE_PATH'][df['RECON_EXECUTION_ID'] == execId].max()

            # iterator = pandas.read_hdf('/' + key_path + '/' + str(execId) + '.h5', 'TXN_DATA',
            #                            chunksize=config.hdf5_chunk_size,
            #                            where="(RECORD_STATUS == 'ACTIVE') & (TXT_5_015 == 'ISSUER' | TXT_5_015 == 'ACQUIRER' )")
            iterator = HDF5Interface(reconId, str(execId).replace('.0', ''), key_path).fetch_all(storeKey='TXN_DATA',
                                                                                                 where_clause="(RECORD_STATUS == 'ACTIVE') & (TXT_5_015 == 'ISSUER' | TXT_5_015 == 'ACQUIRER' | TXT_5_015 == 'MERCHENT') & (REJECTION_CODE =='RB')")

            rightFrame = pandas.DataFrame()
            for item in iterator:
                rightFrame = pandas.concat([rightFrame, item])
            rightFrame = rightFrame
            leftFrame.rename(columns={'RRN': 'TXT_32_003'}, inplace=True)
            leftFrame = leftFrame[['TXT_32_003', 'Txndate']]
            print leftFrame.head()
            rightFrame = pandas.merge(rightFrame, leftFrame, how='left', on='TXT_32_003', indicator=True)
            rightFrame = rightFrame[rightFrame['_merge'] == 'left_only']
            print rightFrame
            if len(rightFrame) == 0:
                return 'No Records.'
            cols = {u'REJECTION_CODE': u'Response Code ', u'TXT_32_010': u'Acquirer Settlement Amount ',
                    u'TXT_32_013': u'BENEFICIARY NUMBER ', u'TXT_32_014': u'REMITTER NUMBER ',
                    u'ACCOUNT_NUMBER': u'ACCOUNT NUMBER ', u'NUM_16_001': u'From Account Type ',
                    u'NUM_16_003': u'Transaction Time ', u'NUM_16_002': u'To Account Type ',
                    u'NUM_16_005': u'Acquirer Stl Proc Fee ', u'NUM_16_004': u'Acquirer Stl Fee ',
                    u'NUM_16_006': u'Acquirer Stl Conv Rate ', u'APPROVAL_STATUS': u'Approval Number ',
                    u'TXT_10_005': u'Acquirer Stl Curr Code ', u'TXT_5_004': u'Merchant Category Code ',
                    u'TXT_5_005': u'Transaction Type ', u'TXT_5_006': u'Member Number ',
                    u'TXT_5_007': u'Payee PSP code ',
                    u'TXT_5_003': u'Participant ID ', u'AMOUNT_3': u'Transaction Amount ',
                    u'AMOUNT_4': u'Actual Transaction Amount ', u'ADDRESS_LINE1': u'Trans Activity Fee ',
                    u'TXT_32_009': u'Aquirer ID ', u'TXT_32_008': u'Acquirer Settlement Date ',
                    u'TXT_32_007': u'Customer refernce Number ', u'TXT_32_005': u'PAN Number ',
                    u'TXT_32_003': u'Transaction Serial Number ', u'TRANSACTION_DATE': u'Transaction Date ',
                    u'TXT_180_004': u'CARDHOLDER BILL CURRENCY ', u'TXT_180_005': u'Acquirer Settlement Date ',
                    u'TXT_180_009': u'Acquirer Stl Conv Rate ', u'TXT_180_001': u'REMARKS ',
                    u'TXT_5_009': u'Acquirer Stl Curr Code ', u'ITEM_REFERENCE': u'System Trace Audit Number ',
                    u'TXT_5_012': u'Transaction Currency Code ', u'TXT_5_011': u'Card Acceptor Term. Location ',
                    u'TXT_5_010': u'Payer PSP code ', u'BRANCH_CODE': u'IFSC CODE ',
                    u'TXT_60_006': u'ORIGINAL CHANNEL ',
                    u'TXT_60_004': u'Card Acceptor ID ', u'TXT_60_005': u'Card Acceptor Terminal ID ',
                    u'TXT_60_002': u'UPI reference code ', u'TXT_60_003': u'Card Acceptor Settl Date ',
                    u'TXT_60_001': u'UPI Transaction ID '}

            rightFrame.rename(columns=cols, inplace=True)
            object_id = str(flask.session["sessionData"]['_id'])
            if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
                os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

            file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/' + reconId + '*.csv'
            fileName = glob.glob(file_name)
            for file in fileName:
                os.remove(file)
            fileName = config.contentDir + "/" + "recon_reports/"+stmt+os.sep +reconId + '-Chargesback.csv'
            print file_name + "/" + fileName,reconId
            rightFrame.to_csv(fileName, index=False)
            return stmt+os.sep +reconId + '-Chargesback.csv'
        return "No Data Found"


class NaturalAccMaster(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NaturalAccMaster, self).__init__("naturalAccMaster", hideFields)


class ReconUtility(object):
    def get_business_details(self):
        business_df = pandas.DataFrame()
        status, business_data = Business().getAll({})
        business_df = pandas.read_json(json.dumps(business_data['data']), orient='columns',
                                       dtype={'BUSINESS_CONTEXT_ID': 'str'})
        business_df.columns = business_df.columns.str.upper()
        return business_df


class ReconDynamicDataModel(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconDynamicDataModel, self).__init__("dynamic_data_setup", hideFields)


class ReconExecutionDetailsLog(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconExecutionDetailsLog, self).__init__("recon_execution_details_log", hideFields)

    def getLatestExeID(self, reconId):
        doc = ReconExecutionDetailsLog().find({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                               'PROCESSING_STATE_STATUS': 'Matching Completed',
                                               'RECORD_STATUS': 'ACTIVE'
                                               }).sort([('RECON_EXECUTION_ID', DESCENDING)])
        doc = list(doc)
        return str(doc[0]['RECON_EXECUTION_ID']) if len(doc) > 0 else ''

    def getLatestExeDetails(self, reconId):
        doc = ReconExecutionDetailsLog().find_one({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                                   'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                   'RECORD_STATUS': 'ACTIVE'
                                                   }, sort=[('RECON_EXECUTION_ID', DESCENDING)])
        return doc

    def getCurrentStatus(self, id, query):
        query['STATEMENT_DATE'] = datetime.datetime.strptime(query['STATEMENT_DATE'], '%d-%b-%Y')
        doc = list(
            ReconExecutionDetailsLog().find({'RECON_ID': query['RECON_ID']
                                             }, sort=[('RECON_EXECUTION_ID', DESCENDING)]))
        df = pandas.DataFrame(columns=['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID'])
        if len(doc):
            df = pandas.DataFrame(doc)
            # print df['RECON_EXECUTION_ID']
            df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(np.int64)
            # print df['RECON_EXECUTION_ID'].unique()
            df = df[(df['STATEMENT_DATE'] >= query['STATEMENT_DATE']) & (
                df['RECON_EXECUTION_ID'] == df['RECON_EXECUTION_ID'].max())]
            # print df[['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID']]
            df['RECORD_VERSION'] = df['RECORD_VERSION'].astype(np.int64)
            df.sort_values(by=['RECORD_VERSION'], inplace=True)
            df.index = range(1, len(df) + 1)
            df = df[['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID']]
        return True, df.T.to_dict().values()

    def getReconWiseMaxExecution(self, reconId=[]):
        from bson.son import SON
        pipeline = [{'$match': {"PROCESSING_STATE_STATUS": "Matching Completed", "RECORD_STATUS": "ACTIVE",
                                'RECON_ID': {'$in': reconId}}},
                    {'$group': {'_id': '$RECON_ID', 'max': {'$max': '$RECON_EXECUTION_ID'}}},
                    {"$sort": SON([("_id", -1)])}]

        agg_dict = ReconExecutionDetailsLog().aggregate(pipeline=pipeline)

        if 'result' in agg_dict and agg_dict['result'] is not None:
            agg_dict = agg_dict['result']
            return True, agg_dict
        else:
            return False, {}


class DashBoardReports(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(DashBoardReports, self).__init__("dashboard_reports", hideFields)


class ReconJobPost(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconJobPost, self).__init__("recon_job_post", hideFields)


class EventLog(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(EventLog, self).__init__("event_logs", hideFields)

    def queryEvents(self, id, query):
        print query
        q = json.loads(query, encoding='utf-8')
        print q
        # reportData = list(NostroReport().find(q))
        doc = list(EventLog().find(q))
        print doc
        if len(doc) == 0:
            return True, []
        else:
            return True, doc


class ChargeBack(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ChargeBack, self).__init__("chargeback", hideFields)



# Note : Donot re-arrange the import, placed here to avoid cylic import error
from dyn_reports import Reports as DashBrdReports

# if __name__ == '__main__':

# print ReconExecutionDetailsLog().get(
#     {"RECON_EXECUTION_ID": str(26469), "RECORD_STATUS": "ACTIVE",
#      "PROCESSING_STATE_STATUS": "Matching Completed"})
