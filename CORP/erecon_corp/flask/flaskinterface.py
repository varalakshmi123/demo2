from gevent import monkey
monkey.patch_all()
import gevent.wsgi

import sys
import flask
import flask.views
import sessionhandler
import functools
import json
import usermanagement
import inspect
import config
import socket
import noauthhandler
import httplib2
import traceback
import pprint
import utilities
import werkzeug.contrib.fixers
import gevent.socket
from gevent.pywsgi import WSGIServer

#from twisted.web import server, wsgi
#from twisted.python.threadpool import ThreadPool
#from twisted.internet import reactor
from logger import createGeventLogger
from logger import Logger
logger = Logger.getInstance("application").getLogger()

def getInstance(collectionName):
    modules = map(__import__, config.applicationModules)
    for module in modules:
            if hasattr(module, collectionName):
                klass = getattr(module, collectionName)
                if inspect.isclass(klass):
                    # print 'klass',klass
                    instance = klass()
                    return instance
    return None

def requestValidator(f):
    @functools.wraps(f)
    def setup_env(*args, **kwargs):
        collectionName = kwargs["collection"].title().replace("_", "")
        # print ' collection'
        # print collectionName
        flask.g.handler = getInstance(collectionName)
        if not flask.g.handler:
            flask.abort(400)
            #return flask.Response(json.dumps({"status": "error", "msg": "Unknown Collection: %s" % (collectionName,)}), 200, [("Content-Type", "application/json"), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

        if not isinstance(flask.g.handler, noauthhandler.noAuthHandler):
            if not "sessionData" in flask.session:
                flask.abort(401)
            if 'partnerId' in flask.request.args and not(flask.request.args['partnerId'] == flask.session['sessionData']['partnerId']):
                flask.session.clear()
                flask.abort(401)

        if "id" in kwargs:
            dummyId = False
            if hasattr(flask.g.handler, "exists"):
                found = False
                if kwargs["id"] == "dummy":
                    found = True
                    dummyId = True
                else:
                    try:
                        found = flask.g.handler.exists(kwargs["id"])
                        #found = flask.g.handler.hasAccess(kwargs["id"])
                    except: #If the passed id is not a valid mongo ObjectId, bson.ObjectId() will throw exception, handle gracefully
                        pass

                if not found:
                    return flask.Response(json.dumps(
                        {"status": "error", "msg": "Collection %s does not have %s" % (collectionName, kwargs["id"])}),
                                          200, [("Content-Type", "application/json"), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])
            if not dummyId and hasattr(flask.g.handler, "hasAccess") and not flask.g.handler.hasAccess(kwargs["id"]):
                return flask.Response(json.dumps({"status": "error",
                                                  "msg": "UnAuthorized for Collection %s / %s" % (
                                                  collectionName, kwargs["id"])}), 200,
                                      [("Content-Type", "application/json"), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])
        return f(*args, **kwargs)

    return setup_env


class RequestHandler(flask.views.MethodView):
    decorators = [requestValidator]

    def head(self, collection):
        return flask.Response("", 200, [("Items-Count", flask.g.handler.count())])

    def get(self, collection, id=None, action=None):
        if action and hasattr(flask.g.handler, action) and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, action)):
            actionHandler = getattr(flask.g.handler, action, None)
            kwargs = None
            #NOTE: Don't ask me why but if I don't do this here, the files will not be available later....
            flask.request.files
            if flask.request.args:
                kwargs = {}
                for k, v in flask.request.args.items():
                    if not k == "partnerId":
                        kwargs[k] = v
            if kwargs and len(kwargs):
                (status, msg) = actionHandler(id, **kwargs)
            else:
                (status, msg) = actionHandler(id)
        elif (id is not None) and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or (usermanagement.has_permission(flask.session["sessionData"]["role"], collection, "get"))):
            (status, msg) = flask.g.handler.get(id)
        elif (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, "getAll")):
            query = {}
            if "query" in flask.request.args:
                query = json.loads(flask.request.args["query"])

            (status, msg) = flask.g.handler.getAll(query)
        else:
            status = False
            msg = "Not Authorized to get data from collection: %s" % (collection)
        return flask.Response(json.dumps({"status": "ok" if status else "error", "msg": msg}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

    def post(self, collection, id=None, action=None):
        if action and hasattr(flask.g.handler, action) and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, action)):
            actionHandler = getattr(flask.g.handler, action, None)
            kwargs = None
            #NOTE: Don't ask me why but if I don't do this here, the files will not be available later....
            flask.request.files
            if flask.request.data:
                kwargs = flask.request.json
            if kwargs:
                (status, msg) = actionHandler(id, **kwargs)
            else:
                (status, msg) = actionHandler(id)
        elif id is None and action is None and hasattr(flask.g.handler, "create") and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, "create")):
            logger.info("Create:"+str(collection)+","+str(id)+","+str(action)+","+str(flask.request.json))
            (status, msg) = flask.g.handler.create(flask.request.json)
        else:
            status = False
            msg = "Invalid or unauthorized access %s / %s" % (collection, action)
        return flask.Response(json.dumps({"status": "ok" if status else "error", "msg": msg}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

    #PUT will be handled only for an instance in a collection, i.e. to update an instance
    def put(self, collection, id):
        if (id is not None) and hasattr(flask.g.handler, "modify") and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, "modify")):
            (status, msg) = flask.g.handler.modify(id, flask.request.json)
        else:
            status = False
            msg = "Invalid or unauthorized access %s / %s" % (collection, id)
        return flask.Response(json.dumps({"status": "ok" if status else "error", "msg": msg}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

    #DELETE will be handled only for an instance in a collection, i.e. to delete an instance
    def delete(self, collection, id):
        if (id is not None) and hasattr(flask.g.handler, "delete") and (isinstance(flask.g.handler, noauthhandler.noAuthHandler) or usermanagement.has_permission(flask.session["sessionData"]["role"], collection, "delete")):
            (status, msg) = flask.g.handler.delete(id)
        else:
            status = False
            msg = "Invalid or unauthorized access %s / %s" % (collection, id)
        return flask.Response(json.dumps({"status": "ok" if status else "error", "msg": msg}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])
def getSessions():
    params = flask.request.json 
    if len(params['partnerId']) == 0:
        return flask.Response(json.dumps({"status": "error", "msg": "Invalid partnerId"}), 200, [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])
    # print 'getinstance'
    # print params
    rsession = getInstance('RemoteSessions')
    rsession._setUser({'partnerId': params['partnerId']})
    (status, remoteSession) = rsession.getAll({"condition":params})
    return flask.Response(json.dumps({"status": "ok" , "msg":remoteSession}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

def checkVersion():
    params = flask.request.json 
    headers   = {'content-type': 'application/json'}
    loginData = {}
    loginData['userName'] = params['user']
    loginData['role'] = 'customer'
    loginData['partnerId'] = params['partnerId'] 
    http = httplib2.Http(disable_ssl_certificate_validation=True)
    response, content = http.request("http://localhost/api", 'POST', headers=headers, body=json.dumps(loginData))
    headers['Cookie'] = response['set-cookie']
    response, content = http.request("http://localhost/api/utilities/dummy/checkVersion", 'POST', headers=headers, body="")
    resp = json.loads(content)
    return flask.Response(json.dumps({"status": "ok" , "msg":resp["msg"]}), 200,
                              [('Content-Type', 'application/json'), ('Cache-Control', 'private, max-age=0, no-cache, no-store')])

def create_app():
    usermanagement.initRoleAndAcl()
    app = flask.Flask(__name__)
    #app.config["PROPAGATE_EXCEPTIONS"] = True
    if len(config.cookieDomain):
        app.config["SESSION_COOKIE_DOMAIN"] = config.cookieDomain
    app.secret_key = config.secretKey
    app.session_interface = sessionhandler.ItsdangerousSessionInterface()
    app.session_cookie_name = config.cookieName

    app.add_url_rule(config.flaskBaseUrl, "loginbase", usermanagement.login, methods=["POST", "GET"])
    app.add_url_rule(config.flaskBaseUrl+"/login", "login", usermanagement.login, methods=["POST", "GET"])
    app.add_url_rule(config.flaskBaseUrl+"/logout", "logout", usermanagement.logout, methods=["POST", "GET"])

    app.add_url_rule(config.flaskBaseUrl+"/<collection>", view_func=RequestHandler.as_view("collection"),
                     methods=["HEAD", "GET", "POST"])
    app.add_url_rule(config.flaskBaseUrl+"/<collection>/<id>", view_func=RequestHandler.as_view("instance"),
                     methods=["GET", "PUT", "DELETE"])
    app.add_url_rule(config.flaskBaseUrl+"/<collection>/<id>/<action>", view_func=RequestHandler.as_view("action"), methods=["POST", "GET"])

    #app.add_url_rule(config.flaskBaseUrl+"/createIntegrator", "createIntegrator", usermanagement.createIntegrator, methods=["POST"])
    return app

app = create_app()

@app.errorhandler(500)
def exceptionHandler(error):
    errFormat = '''Error:
Stack Trace:
%s
Environment:
%s
Payload Data:
%s
    ''' % (traceback.format_exc(), pprint.pformat(flask.request.environ), flask.request.data)
    logger.error(errFormat)
    if config.isAllowTicketCreation:
        utilities.sendReportMail("Error while handling request", "See the attached log.", errFormat)
    return '{"status": "error", "msg": "Internal Error"}'

@app.teardown_appcontext
def close_db(error):
    pass
    """Closes the database again at the end of the request."""

    # if hasattr(flask.g, 'oracleDB_conn'):
    #     # Incase of any error roll back the transaction
    #     if error is not None:
    #          flask.g.oracleDB_conn.rollback()
    #
    #     # commit the transactions incase there are no errors
    #     else:
    #         logger.info('comitting to oracle database')
    #         flask.g.oracleDB_conn.commit()
    #
    #     logger.info('OracleDB_connection_object <"{0}"> released'.format(hash(flask.g.oracleDB_conn)))
    #     flask.g.oracleDB_conn.close()
    #     #delattr(flask.g, 'oracleDB_conn')

def getAddrInfoWrapper(host, port, family=0, socktype=0, proto=0, flags=0):
    return origGetAddrInfo(host, port, socket.AF_INET, socktype, proto, flags)
if __name__ == "__main__":
    #thread_pool = ThreadPool()
    #thread_pool.start()
    #reactor.addSystemEventTrigger('after', 'shutdown', thread_pool.stop)
    #factory = server.Site(wsgi.WSGIResource(reactor, thread_pool, app))
    #reactor.listenTCP(8081, factory, interface="0.0.0.0")
    #reactor.run()
    origGetAddrInfo = gevent.socket.getaddrinfo
    gevent.socket.getaddrinfo = getAddrInfoWrapper
    socket.getaddrinfo = getAddrInfoWrapper
    http_server =  WSGIServer(('', int(sys.argv[1])), werkzeug.contrib.fixers.ProxyFix(app), log=createGeventLogger())
    http_server.serve_forever()