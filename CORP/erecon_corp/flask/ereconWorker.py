# *************************************************************************
#
# HashInclude CONFIDENTIAL
# ________________________
#
#  [2010] - [2017] HashInclude Computech Private Limited
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of HashInclude Computech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to HashInclude Computech
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from HashInclude Computech Private Limited.
# *************************************************************************

from logger import Logger

logger = Logger.getInstance("application").getLogger()
import application

''' Isolation class for isolation and routing all the method calls from the 
recon serializer(queue) to functions in application.py
'''
class workerThread:
    def __init__(self):
        pass

    def submitForAuthorization(self, query):
        application.ReconAssignment().submitForAuthorization('', query)

    def authorizeRecords(self, query):
        application.ReconAssignment().authorizeRecords('', query)

    def rejectRecords(self, query):
        application.ReconAssignment().rejectRecords('', query)

    def unGrouping(self, query):
        application.ReconAssignment().unGroupingColumns('', query)

    def rollBack(self, query):
        application.ReconAssignment().rollBack('', query)

    def authorizeRollBackRecords(self, query):
        application.ReconAssignment().authorizeRollBackRecords('', query)

    def rejectRollBackRecords(self, query):
        application.ReconAssignment().rejectRollBackRecords('', query)