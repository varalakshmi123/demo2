import time
from os import sep

import numpy as np
import pandas as pd
import tables

import application
import config
import flask
import logger
import datetime
from mdfieldSizes import modelFieldColSize as colSizes
import uuid
from Hdf5ZfsUtil import Hdf5ZfsUtil

logger = logger.Logger.getInstance("application").getLogger()


class HDF5Interface():
    def __init__(self, recon_id='', execution_id='', snapPath = ''):
        self.recon_id = recon_id
        self.execution_id = execution_id
        self.store_loc = '/' + snapPath + '/' + str(execution_id) + '.h5'
        logger.info('Store initialized for path %s' % self.store_loc)

    def read_col_query_unmatched(self):
        # (status, columnsList) = self.read_col_query(reconId)
        iterator = self.fetch_one(where_clause=config.unmatched_qry)
        data = pd.DataFrame()

        for df in iterator:
            data = pd.concat([data, df], ignore_index=True)
        logger.info('loading unmatched records completed.')
        return True, data

    def read_col_query_matched(self, reconId='', executioinId='', skip=0, limit=config.hdfMatchedLimit):
        iterator = self.fetch_one(leaf='MATCHED', startFrom=skip, stopAt=limit,where_clause=config.matched_qry)
        data = pd.DataFrame()
        for df in iterator:
            data = pd.concat([data, df], ignore_index=True)
        if 'LINK_ID' in data.columns:
            data.sort_values(by=['LINK_ID'], inplace=True)
        return True, data

    def fetch_matched_search_rst(self, query, execDT=False, stateDT=False, skip=0, limit=config.hdfMatchedLimit):
        df = pd.DataFrame(query['execution_log'])
        filter_data = pd.DataFrame()
        for val in df['RECON_EXECUTION_ID']:
            self.store_loc = config.hdf5_location + sep + str(query['recon_id']) + sep + str(val) + sep + str(val) + '.h5'
            self.store_key = 'TXN_DATA'
            where_clause = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' == 'MATCHED')"
            iterator = self.fetch_all(where_clause=where_clause)
            for data in iterator:
                if execDT:
                    data = data.loc[data[data['EXECUTION_DATE_TIME'].dt.date >= query['executionStartDate']].index]
                    data = data.loc[data[data['EXECUTION_DATE_TIME'].dt.date <= query['executionEndDate']].index]
                if stateDT:
                    data = data.loc[data[data['STATEMENT_DATE'] >= query['statementStartDate']].index]
                    data = data.loc[data[data['STATEMENT_DATE'] <= query['statementEndDate']].index]
                filter_data = pd.concat(
                    [filter_data, data[(data['RECORD_STATUS'] == 'ACTIVE') & (data['ENTRY_TYPE'] == 'T')]],
                    ignore_index=True)
        if 'LINK_ID' in filter_data.columns:
            filter_data.sort_values(by=['LINK_ID'], inplace=True)
        return True, filter_data

    def read_col_query_waitingForAuthorization(self, user_role=''):
        # (status, columnsList) = self.read_col_query(reconId)
        iterator = self.fetch_one(where_clause=config.waiting_auth_qry)

        data = pd.DataFrame()
        for df in iterator:
            data = pd.concat([data, df], ignore_index=True)

        if 'AUTHORIZED_BY' in data.columns and user_role == 'Authorizer':
            (status, userpools) = application.UserPool().getAll(
                {'perColConditions': {'users': flask.session["sessionData"]['userName'], 'role': 'Authorizer'}})
            logger.info(pd.DataFrame(userpools['data'])['name'])
            userpools = pd.DataFrame(userpools['data'])['name'].unique()
            data = data[data['AUTHORIZED_BY'].isin(userpools)]

        logger.info('loading waiting for auth records in chunks completed.')
        return True, data

    def read_col_query_rollback(self, user_role=''):
        data = pd.DataFrame()
        iterator = self.fetch_one(where_clause=config.roll_aut_qry)
        for df in iterator:
            data = pd.concat([data, df], ignore_index=True)

        if 'AUTHORIZED_BY' in data.columns and user_role == 'Authorizer':
            (status, userpools) = application.UserPool().getAll(
                {'perColConditions': {'users': flask.session["sessionData"]['userName'], 'role': 'Authorizer'}})
            logger.info(pd.DataFrame(userpools['data'])['name'])
            userpools = pd.DataFrame(userpools['data'])['name'].unique()
            data = data[data['AUTHORIZED_BY'].isin(userpools)]

        logger.info('loading rollback auth pending records in chunks completed.')
        return True, data

    # TODO need to impl audit history details
    # def read_col_query_auditTrail(self, raw_link=''):
    #     where_clause = "('RAW_LINK' == '%s')" % raw_link
    #     data = pd.DataFrame()
    #     for iterator in (unmatched_it, matched_it):
    #         for df in iterator:
    #             data = pd.concat([data, df]).reset_index(drop=True)
    #     data.sort_values(by='CREATED_DATE', inplace=True)
    #
    #     return True, data

    # Method to inactivate the records based on the values in the data_column & returns total number of rows updated
    def inactivate_txn(self, leaf='UNMATCHED', logged_user='', columns='', values=[], txn_status=''):
        key = self.store_key + leaf + sep + 'table'
        count = 0
        with tables.open_file(self.store_loc, mode='r+') as h5file:
            table = h5file.get_node(key)
            for value in values:
                where_clause = "(RECORD_STATUS == 'ACTIVE') & (TXN_MATCHING_STATUS == '%s') & (%s == '%s')" % (
                    str(txn_status), str(columns), str(value))
                for existing_row in table.where(where_clause):
                    existing_row['RECORD_STATUS'] = 'INACTIVE'
                    existing_row['UPDATED_DATE'] = datetime.datetime.now().strftime('%d/%m/%Y %I:%M %p')
                    existing_row['UPDATED_BY'] = logged_user
                    count += 1
                    existing_row.update()
            table.flush()
        return count

    def update_txn_status(self, query={}):
        key = sep + config.store_key + sep + 'table'
        count = 0
        logger.info('Update records started')
        with tables.open_file(self.store_loc, mode='r+') as h5file:
            table = h5file.get_node(key)
            for value in query['values']:
                where_clause = "(%s == '%s') & (TXN_MATCHING_STATUS == '%s')" % (str(query['column']),
                                                                                 str(value), str(query['status']))
                for existing_row in table.where(where_clause):
                    existing_row['CREATED_DATE'] = datetime.datetime.now().strftime('%d/%m/%Y %I:%M %p')
                    existing_row['CREATED_BY'] = query['logged_user']
                    existing_row['COMMENTS'] = query['COMMENTS']
                    existing_row['TXN_MATCHING_STATUS'] = query['TXN_MATCHING_STATUS']

                    # if state not part of query get next state on current state,
                    # state is not none set values from query
                    # state is none retain previous value
                    if 'TXN_MATCHING_STATE' not in query:
                        existing_row['TXN_MATCHING_STATE'] = config.txn_state_mapper[existing_row['TXN_MATCHING_STATE']]
                    elif query['TXN_MATCHING_STATE'] is not None:
                        existing_row['TXN_MATCHING_STATE'] = query['TXN_MATCHING_STATE']
                    else:
                        pass

                    if 'AUTHORIZED_BY' in query:
                        existing_row['AUTHORIZED_BY'] = query['AUTHORIZED_BY']

                    if 'TXN_MATCHING_STATE' in query and query['TXN_MATCHING_STATE'] == 'UNGROUPED':
                        existing_row['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    elif 'LINK_ID' in query:
                        existing_row['LINK_ID'] = query['LINK_ID']
                    else:
                        pass
                    existing_row.update()
                    count += 1
            table.flush()
        return count

    # Mehtod to check if there is un-refreshed stale data in user interface or not
    def check_stale_data(self, leaf='UNMATCHED', columns='', values=[]):
        key = self.store_key + leaf + sep + 'table'
        count = 0
        with tables.open_file(self.store_loc, mode='r') as h5file:
            table = h5file.get_node(key)
            for value in values:
                where_clause = "(RECORD_STATUS == 'ACTIVE') & (%s == '%s')" % (str(columns), str(value))
                for existing_row in table.where(where_clause):
                    count += 1
        return count

    # Method to fetch the data for the given key with limit in chunks
    def fetch_one(self, storeKey=config.store_key, leaf='UNMATCHED', where_clause="'RECORD_STATUS'=='ACTIVE'", startFrom=0, stopAt=5000):
        try:
            # TODO check if read snap shot is in sync with main frame before proceeding
            # to avoid stale data in the UI
            if not Hdf5ZfsUtil().sync_read_snap_shot(self.recon_id,self.execution_id):
                logger.info("Stale data in UI, read snap shot is not sync !!")

            if leaf == 'MATCHED':
                iterator = pd.read_hdf(self.store_loc, storeKey, chunksize=config.hdf5_chunk_size,
                                       start=startFrom, stop=stopAt, where=where_clause)
            else:
                iterator = pd.read_hdf(self.store_loc, storeKey, chunksize=config.hdf5_chunk_size,
                                       where=where_clause)
        except KeyError, e:
            return []

        logger.info('Data reading in chunk completed.')
        return iterator

    # Method to check if required key exists in the store or not
    def check_key_exists(self, keys=[]):
        exists = False
        self.loc = '/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/25829.h5'
        with pd.get_store(self.store_loc) as hdf_file:
            exists = True if keys in hdf_file.keys() else False
        return exists

    # Method to fetch the data for the given key without limit in chunks
    def fetch_all(self,storeKey=config.store_key, where_clause="'RECORD_STATUS' == 'ACTIVE'"):
        # ref: /usr/share/nginx/reconid/executionid.h5, /reconid/cash_output_txn/abc_123/(MATCHED/UNMATCHED)
        try:
            iterator = pd.read_hdf(self.store_loc, storeKey, chunksize=config.hdf5_chunk_size,where=where_clause)
        except KeyError, e:
            return []

        logger.info('Data reading in chunk completed.')
        return iterator

    def read_col_query(self, reconId=''):
        status, data = application.ReconDynamicDataModel().getAll({"reconId": reconId})
        if data['total'] > 0:
            df = pd.DataFrame(data['data'])
            df = df.sort_values(by=['position']).reset_index()
            return True, df
        else:
            return True, pd.DataFrame()
        return False, ''

    # Incase of flush : Drop frame and update new dataframe
    def save_hdf5_data(self, df=None, flush=True, leaf='UNMATCHED'):
        logger.info('Inserting frame to HDF5 initiated')

        df.index = range(1, len(df) + 1)
        colsize = {}
        for col in df.columns:
            if str(col) in colSizes.keys():
                colsize[col] = colSizes[col]
            else:
                del df[col]
        # minItemSize = {}
        # for col in df.columns:
        #     if str(col) in colSizes.keys():
        #         minItemSize[col] = int(df[col].astype(str).str.len().max())
        #     else:
        #         del df[col]
        # if minItemSize is not None:
        #     colsize = minItemSize
        if leaf == 'MATCHED':
            if 'RECORD_STATUS' in df.columns:
                df.sort_values(by=['RECORD_STATUS'], ascending=True, inplace=True)

        df = df[sorted(colsize.keys())]
        g = df.groupby(np.arange(len(df)) / 30000)  # config.hdf5_chunk_size

        if flush:
            pd.DataFrame().to_hdf(self.store_loc, self.store_key + leaf, format='table')

        for _, grp in g:
            grp.to_hdf(self.store_loc, self.store_key + leaf, append=True, mode='a', format='table',
                       min_itemsize=colsize,
                       data_columns=config.data_columns)

        logger.info('%s records inserted into hdf5 file with key %s' % (str(len(df)), leaf))

        # TODO
        # def save_data(data_frame, filename, output_name, append_to_file=True,
        #               compression_level=Settings.HDF5_COMPRESSION_LEVEL,
        #               compression_library=Settings.HDF5_COMPRESSION_LIBRARY):
        #     """
        #     Saves DataFrame into an HDF5 file in a table format.
        #
        #     Parameters
        #     ----------
        #     data_frame: pandas DataFrame
        #     filename: str
        #         Full name of the HDF5 file.
        #     output_name: str
        #         Name of the corresponding table withing the HDF5 file.
        #     append_to_file: bool, default True
        #         Whether to append to an existing file. If False, the file is overwritten.
        #     compression_level: int
        #         Compression level passed to complevel parameter in DataFrame.to_hdf() method.
        #     compression_library: str
        #         Compression library passet to complib parameter in DataFrame.to_hdf() method.
        #     """
        #     assert isinstance(data_frame, DataFrame)
        #     assert isinstance(filename, str)
        #     assert isinstance(output_name, str)
        #     assert isinstance(append_to_file, bool)
        #
        #     mode = 'a' if append_to_file else 'w'
        #     store = pd.HDFStore(filename, mode=mode, complevel=compression_level, complib=compression_library)
        #     store.append(output_name, data_frame, data_columns=True)
        #     store.close()

        # if __name__=='__main__':
        #     (status_userpools, userpools_data) = application.UserPool().getAll(
        #             {'perColConditions': {'users': 'authorizer1'}})
        #     loggedInUserID = 'authorizer1'
        #     print 'hello'
        #     print pd.DataFrame(userpools_data['data'])['name']

    def save_hdf5_data(self, df=None, flush=True, leaf='UNMATCHED'):
        logger.info('Inserting frame to HDF5 initiated')

        df.index = range(1, len(df) + 1)
        colsize = {}
        for col in df.columns:
            if str(col) in colSizes.keys():
                colsize[col] = colSizes[col]
            else:
                del df[col]
        # minItemSize = {}
        # for col in df.columns:
        #     if str(col) in colSizes.keys():
        #         minItemSize[col] = int(df[col].astype(str).str.len().max())
        #     else:
        #         del df[col]
        # if minItemSize is not None:
        #     colsize = minItemSize
        if leaf == 'MATCHED':
            if 'RECORD_STATUS' in df.columns:
                df.sort_values(by=['RECORD_STATUS'], ascending=True, inplace=True)

        df = df[sorted(colsize.keys())]
        g = df.groupby(np.arange(len(df)) / config.hdf5_write_chunk)  # config.hdf5_chunk_size

        if flush:
            pd.DataFrame().to_hdf(self.store_loc, self.store_key + leaf, format='table')

        for _, grp in g:
            grp.to_hdf(self.store_loc, self.store_key + leaf, append=True, mode='a', format='table',
                       min_itemsize=colsize,
                       data_columns=config.data_columns)

        logger.info('%s records inserted into hdf5 file with key %s' % (str(len(df)), leaf))

        # TODO
        # def save_data(data_frame, filename, output_name, append_to_file=True,
        #               compression_level=Settings.HDF5_COMPRESSION_LEVEL,
        #               compression_library=Settings.HDF5_COMPRESSION_LIBRARY):
        #     """
        #     Saves DataFrame into an HDF5 file in a table format.
        #
        #     Parameters
        #     ----------
        #     data_frame: pandas DataFrame
        #     filename: str
        #         Full name of the HDF5 file.
        #     output_name: str
        #         Name of the corresponding table withing the HDF5 file.
        #     append_to_file: bool, default True
        #         Whether to append to an existing file. If False, the file is overwritten.
        #     compression_level: int
        #         Compression level passed to complevel parameter in DataFrame.to_hdf() method.
        #     compression_library: str
        #         Compression library passet to complib parameter in DataFrame.to_hdf() method.
        #     """
        #     assert isinstance(data_frame, DataFrame)
        #     assert isinstance(filename, str)
        #     assert isinstance(output_name, str)
        #     assert isinstance(append_to_file, bool)
        #
        #     mode = 'a' if append_to_file else 'w'
        #     store = pd.HDFStore(filename, mode=mode, complevel=compression_level, complib=compression_library)
        #     store.append(output_name, data_frame, data_columns=True)
        #     store.close()

        # if __name__=='__main__':
        #     (status_userpools, userpools_data) = application.UserPool().getAll(
        #             {'perColConditions': {'users': 'authorizer1'}})
        #     loggedInUserID = 'authorizer1'
        #     print 'hello'
        #     print pd.DataFrame(userpools_data['data'])['name']


if '__name__' == '__main__':
    loc = '/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/25829.h5'
    keys = ['MATCHED', 'UNMATCHED'],
    with pd.get_store(loc) as hdf_file:
        exists = True if keys in hdf_file.keys() else False
        print exists
