import json
import os
import sys
from pymongo import MongoClient
from bson import ObjectId
from datetime import datetime
import config

def importReconDetails(reconId, importFolderName):
    importFolder = config.migrationFolder
    exportFolder = importFolder + '/backups/'
    if not os.path.exists(exportFolder):
        os.mkdir(exportFolder)


    mongoPath = config.mongoPath
    col_name = 'recon_context_details'
    mongo_client = MongoClient('mongodb://localhost:27017/')
    lmongo_db = mongo_client.reconbuilder
    cc_col_recon = lmongo_db[col_name]

    backupDT = datetime.now().strftime("%d%b%Y_%H%M")

    # importFolderName = '02Feb2017'
    folderName = importFolder + reconId + os.sep
    readPath = folderName + importFolderName + os.sep
    print readPath
    print 'Started Recon Backup for reconId: ', reconId
    if os.path.exists(readPath + reconId + '.json'):
        print 'vdsnkvlf'
        for record in cc_col_recon.find():
            if 'reconId' in record and record['reconId'] == str(reconId):
                folderName = exportFolder + reconId + os.sep
                if not os.path.exists(folderName):
                    os.mkdir(folderName)
                folderName = exportFolder + reconId + os.sep + backupDT + os.sep
                if not os.path.exists(folderName):
                    os.mkdir(folderName)
                q = './mongoexport -d reconbuilder -c  recon_context_details -q "{ reconId: ' + repr(
                    reconId) + ' }" --out ' + folderName + reconId + '.json'
                os.system(q)
                cc_col_recon.remove({'reconId': reconId})
                print '|Backedup Recon Matching Rules: ', record['reconName']
                # Save Feed Defination
                for feed in record['sources']:
                    for i in lmongo_db.feedDefination.find({'_id': ObjectId(feed['feedId'])}):
                        q = './mongoexport -d reconbuilder -c  feedDefination -q \'{ _id: ObjectId("' + (
                            feed['feedId']) + '") }\' --out ' + folderName + str(feed['feedId']) + '.json'
                        os.system(q)
                        cc_col_recon = lmongo_db['feedDefination']
                        cc_col_recon.remove({'_id': ObjectId(feed['feedId'])})
                        print '|Backedup Feed Details: ', i['feedName']

                for accMap in lmongo_db.feedAccMapping.find({'reconContext.reconId': reconId}):
                    q = './mongoexport -d reconbuilder -c  feedAccMapping -q "{' + repr(
                        'reconContext.reconId') + ': ' + repr(
                        reconId) + ' }" --out ' + folderName + str(reconId) + '_accMapping' + '.json'
                    os.system(q)
                    cc_col_recon = lmongo_db['feedAccMapping']
                    cc_col_recon.remove({'reconContext.reconId': reconId})
                    print '| Backedup Account Mapping Details: '

                for accMap in lmongo_db.feedAccMapping.find({'reconId': reconId}):
                    print '| Saved PreMatching Details: '
                    q = './mongoexport -d reconbuilder -c  feedAccMapping -q "{ reconId: ' + repr(
                        reconId) + ' }" --out ' + folderName + str(reconId) + '_prematch.json'
                    os.system(q)
                    cc_col_recon = lmongo_db['feedAccMapping']
                    cc_col_recon.remove({'reconId': reconId})

                for accMap in lmongo_db.postmatchscripts.find({'reconId': reconId}):
                    q = './mongoexport -d reconbuilder -c  postmatchscripts -q "{ reconId: ' + repr(
                        reconId) + ' }" --out ' + folderName + str(reconId) + '_postmatch.json'
                    os.system(q)
                    cc_col_recon = lmongo_db['feedAccMapping']
                    cc_col_recon.remove({'reconId': reconId})
                    print '|Backedup PostMatching Details: ', i['feedName']
                break
        print 'Ended Recon Backup for reconId: ', reconId
        print '*' * 50
        # exit(0)
        print 'Started Recon Migration for reconId: ', reconId
        record = json.loads(open(readPath + reconId + '.json', 'r').read())
        record['_id'] = ObjectId(record['_id']['$oid'])
        cc_col_recon.save(record)
        print "Migrated Recon Matching Rules for recon: ", record['reconName']

        # Feed Defination
        print "Migrating Feed Defination"
        for feed in record['sources']:
            feedPath = readPath + feed['feedId'] + '.json'
            feedObj = json.loads(open(feedPath, 'r').read())
            cc_col_recon = lmongo_db['feedDefination']
            feedObj['_id'] = ObjectId(feedObj['_id']['$oid'])
            cc_col_recon.save(feedObj)
            print 'Migrated Feed Details for: ', feedObj['feedName']

        if os.path.exists(readPath + str(reconId) + '_accMapping' + '.json'):
            feedPath = readPath + str(reconId) + '_accMapping' + '.json'
            feedAcc = json.loads(open(feedPath, 'r').read())
            cc_col_recon = lmongo_db['feedAccMapping']
            feedAcc['_id'] = ObjectId(feedAcc['_id']['$oid'])
            cc_col_recon.save(feedAcc)
            print 'Migrated Feed Account Details for: ', reconId

        if os.path.exists(readPath + str(reconId) + '_prematch' + '.json'):
            feedPath = readPath + str(reconId) + '_prematch' + '.json'
            preMatchObj = json.loads(open(feedPath, 'r').read())
            cc_col_recon = lmongo_db['feedAccMapping']
            preMatchObj['_id'] = ObjectId(preMatchObj['_id']['$oid'])
            cc_col_recon.save(preMatchObj)
            print 'Migrated Pre-Match scripts: ', reconId

        if os.path.exists(readPath + str(reconId) + '_postmatch' + '.json'):
            feedPath = readPath + str(reconId) + '_postmatch' + '.json'
            postMatchObj = json.loads(open(feedPath, 'r').read())
            cc_col_recon = lmongo_db['postmatchscripts']
            postMatchObj['_id'] = ObjectId(postMatchObj['_id']['$oid'])
            cc_col_recon.save(postMatchObj)
            print 'Migrated Post-Match scripts: ', reconId


if __name__ == '__main__':
    importFolderName = '02Feb2017'
    reconName = 'EPYMTS_CASH_APAC_20225'
    #importReconDetails(reconName, importFolderName)
    importReconDetails(sys.argv[1], sys.argv[2])
