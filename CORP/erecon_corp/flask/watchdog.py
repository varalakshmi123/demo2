from apscheduler.schedulers.background import BackgroundScheduler
import time
from pymongo import MongoClient
import pandas as pd
import checkUpstart as upstrt


# Start the init jobs
def monitorFissionFlask():
    mongo_client = MongoClient('mongodb://localhost:27017')
    mdb_cursor = mongo_client['reconbuilder']['recon_context_details'].find({})
    reconsList = list(mdb_cursor)
    df = pd.DataFrame(reconsList)
    df.drop_duplicates(subset=['reconId'],inplace=True)
    df.dropna(subset=['reconId'],inplace=True)
    df['reconId'] = df['reconId'].str.strip(' ')
    for recon in df['reconId'].unique().tolist():
        if not recon or recon.strip('') == '':
            continue
        upstrt.startUpstartJob('reconwatcher RECONID=%s' % str(recon))

if __name__ == "__main__":
    reconWatchDog = BackgroundScheduler()
    try:
        reconWatchDog.start()
        reconWatchDog.add_job(monitorFissionFlask, 'cron', minute='*/1')
    except:
        pass

    while True:
        time.sleep(3000)
