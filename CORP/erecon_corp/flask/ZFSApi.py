#!/usr/bin/env python

from logger import Logger

import os
import time
import json
import jsonpickle
# import network
import struct
import pymongo
import subprocess
from collections import namedtuple
# from bootKeyMap import *
from datetime import datetime
from xml.dom.minidom import Document
import config

logger = Logger.getInstance("application").getLogger()
cloneDestroy = "clone:destroyonexit"
encryptStatus = "clone:isencrypted"
zfsBase = config.zfsBase
zfsBaseName = config.zfsBaseName


def execution(command):
    childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                    close_fds=True)
    out, err = childProcess.communicate()
    status = childProcess.returncode
    return status, out, err


class ZFSApi():
    @classmethod
    def getZfsPath(cls, storePath=None):
        return zfsBase + storePath

    @classmethod
    def create(cls,storePath):
        (status, out, err) = execution('zfs create %s' % (ZFSApi.getZfsPath(storePath)))
        if status == 0:
            return status
        else:
            logger.error("ZFS create out:%s err:%s" % (out, err))
            return status

    @classmethod
    def delete(cls, snapPath):
        #snapPath = ZFSApi.getZfsPath(ps['store_path']) + "@" + str(ps["_id"])
        (status, out, err) = execution('zfs destroy -f -R %s' % (snapPath))
        if not status:
            return True
        else:
            logger.error("ZFS destroy out:%s err:%s" % (out, err))
            return False

    @classmethod
    def snapshot(cls, ps):
        snapPath = ZFSApi.getZfsPath(ps['store_path']) + "@" + str(ps["_id"])
        (status, out, err) = execution('zfs snapshot %s' % snapPath)
        if not status:
            return snapPath
        else:
            (status, out, err) = execution("zfs list %s" % snapPath)
            if not status:
                return snapPath
            logger.error("ZFS snapshot out:%s err:%s" % (out, err))
            return None

    @classmethod
    def rollBack(cls, snapPath):
        #snapPath = ZFSApi.getZfsPath(ps['store_path']) + "@" + str(ps["_id"])
        (status, out, err) = execution('zfs rollback -R %s' % snapPath)
        if not status:
            return True
        else:
            logger.error("ZFS rollback out:%s err:%s" % (out, err))
            return False

    @classmethod
    def snapShotExist(cls, snapPath):
        (status, out, err) = execution("zfs list %s" % snapPath)
        if not status:
            return True
        else:
            logger.error("ZFS snapshotexists out:%s err:%s" % (out, err))
            return False

    @classmethod
    def list(cls, syncSystemId, reconID="admin"):
        if reconID != "admin":
            (status, out, err) = execution(
                'zfs list -t snapshot -o name -rH %s%s/%s' % (zfsBase, reconID, syncSystemId))
        else:
            (status, out, err) = execution(
                'zfs list -t snapshot -o name -rH %s%s' % (ZFSApi.getZfsPath(), syncSystemId))
        if not status:
            return out
        else:
            logger.error("ZFS list out:%s err:%s" % (out, err))
            return ""

    @classmethod
    def getZfsSnapshots(cls, syncSystemId, allsnaps=False):
        # TODO:- Have to add reconID
        reconID = "admin"
        (status, out, err) = execution("zfs list -o name | grep %s | grep -v clone | cut -d '/' -f2" % syncSystemId)
        if (not status) and len(out.strip()):
            reconID = out.strip()

        '''
        if allsnaps:
            cmd = "zfs list -t snapshot -o name -rH %s%s/%s | cut -d '/' -f3" % (zfsBase, reconID, syncSystemId)
        else:
            cmd = "zfs list -t snapshot -o name -rH %s/%s/%s | cut -d '/' -f3 | tail -1" % (zfsBase, reconID, syncSystemId)
        (status, out, err) = execution(cmd)
        if not status:
            lines = out.splitlines()
            if allsnaps:
                return lines
            elif len(lines):
                return lines[0]
        '''
        if not os.path.isdir("/%s%s/%s/.zfs/snapshot" % (zfsBase, reconID, syncSystemId)):
            return []
        snapshots = os.listdir("/%s%s/%s/.zfs/snapshot" % (zfsBase, reconID, syncSystemId))
        if len(snapshots):
            snapshots.sort()
            if allsnaps:
                for i in range(0, len(snapshots)):
                    snapshots[i] = "%s@%s" % (syncSystemId, snapshots[i])
                return snapshots
            else:
                return "%s@%s" % (syncSystemId, snapshots[-1])
        return []

    @classmethod
    def rollBack(cls, snapshot):
        # Before calling this method proper login(even as user) is happening so not considering reconID
        snapshot = ZFSApi.getZfsPath() + snapshot
        (status, out, err) = execution('zfs rollback -r %s' % snapshot)
        if not status:
            return True
        else:
            logger.error("ZFS rollback out:%s err:%s" % (out, err))
            return False

    @classmethod
    def setProperty(cls, snapShot, propName, data):
        # Assuming snapShot contains "erecon/"
        (status, out, err) = execution('zfs set %s="%s" %s' % (propName, data, snapShot))
        if not status:
            return True
        else:
            logger.error("ZFS setproperty out:%s err:%s" % (out, err))
            return False

    @classmethod
    def getProperty(cls, snapShot, propName):
        # Assuming snapShot contains "erecon/"
        (status, out, err) = execution('zfs get -H -o value %s %s' % (propName, snapShot))
        if not status:
            return out
        else:
            logger.error("ZFS getproperty out:%s err:%s" % (out, err))
            return False

    @classmethod
    def cloneSnapshot(cls, destroy, snapShot='', cloneSnapName=''):
        # destroy    0 - clone will be destroyed
        #            1 - clone will not be destroyed
        cmd = "zfs clone %s %s" % (snapShot, cloneSnapName)
        (status, out, err) = execution(cmd)
        if not status:
            # When clone is created successfully
            # Setting the mode as property on clone
            ZFSApi.setProperty(cloneSnapName, cloneDestroy, destroy)
            return True
        else:
            (status, out, err) = execution("zfs list %s" % cloneSnapName)
            if not status:
                return True
            else:
                logger.error("Clone error: %s %s" % (out, err))
                return False, 'Failed'

    @classmethod
    def destroyClone(cls, cloneSnap):
        if not os.path.exists("/" + cloneSnap):
            logger.info("Clone %s does not exist, so returning success." % cloneSnap)
            return True

        propValue = int(ZFSApi.getProperty(cloneSnap, cloneDestroy))
        if propValue == 0:
            # clone can be destroyed
            time.sleep(5)
            for i in xrange(0, 5):
                (status, out, err) = execution('zfs destroy -R %s' % cloneSnap)
                if not status:
                    logger.info('ZFS clone destroy, so returning success')
                    return True
                else:
                    logger.error("ZFS destroy out:%s err:%s" % (out, err))
                    time.sleep(5)

            if not os.path.exists("/"+cloneSnap):
                logger.info("Clone %s does not exist, so returning success." % (cloneSnap))
                return True
            if status:
                logger.error("Failed to destroy clone even after sleeping why???")
                return False
        return True

    @classmethod
    def listFiles(cls, path):
        try:
            files = os.listdir(path)
        except OSError, e:
            return []

        diskFile = []
        diskMapping = {}
        for file in files:
            if not file.endswith(".meta"):
                if file + ".meta" in files:
                    if os.path.getsize(path + "/" + file + ".meta") == 0:
                        continue
                    diskFile.append(file)
            elif file.endswith(".meta"):
                if os.path.getsize(path + "/" + file) == 0:
                    continue
                fileName = file.split(".meta")[0]
                if fileName in files:
                    f = open(path + "/" + file, "r")
                    data = json.load(f)
                    diskMapping[os.path.splitext(file)[0]] = data['mountPointName']
        finalList = sorted(diskFile, key=diskMapping.__getitem__)
        return finalList

    @classmethod
    def fixDynamicDisks(cls, bootDiskPath):
        bootDiskFile = open(bootDiskPath, "r")
        mbr = bootDiskFile.read(512)
        if ord(mbr[511]) == 0XAA and ord(mbr[510]) == 0X55:
            partitionFormat = "<BBBBBBBBII"
            Partition = namedtuple('Partition',
                                   ['Status', "StartHead", "StartSec", "StartCyl", "FileSystem", "EndHead", "EndSec",
                                    "EndCyl", "FirstLBA", "NumLBA"])
            for i in xrange(4):
                part = Partition._make(struct.unpack_from(partitionFormat, mbr, 446 + (16 * i)))
                if part.FileSystem != 0 and part.FileSystem != 7:
                    logger.info("Fixing Dynamic disk for:%s FileSystemType:%s" % (bootDiskPath, part.FileSystem))
                    bootFileWrite = open(bootDiskPath, "r+")
                    bootFileWrite.seek(446 + (16 * i) + 4)
                    bootFileWrite.write('\x07')
                    bootFileWrite.close()
        bootDiskFile.close()

    @classmethod
    def _convert_to_12_hour(cls, snapshot):
        ts = snapshot.split('@')[1]
        tsl = ts.split('_')
        h = int(tsl[3])
        if h > 12:
            tsl[3] = str(h - 12)
            tsl.append('pm')
        else:
            tsl.append('am')

        return "_".join(tsl)

    @classmethod
    def poolStatus(cls):
        (status, out, err) = execution("zpool status %s" % zfsBaseName)
        if not status:
            return out
        else:
            return ""

    @classmethod
    def zfsDiskFreeSpace(cls, sizeInGb='50G', percentage=0):
        status, msg = ZFSApi.zfsStatus()
        if not status:
            return False
        poolStats = ZFSApi.poolUsagePercentage()
        if poolStats['Total'] != "--" and poolStats['Usable'] != "--" and poolStats['Avail'] != "--":
            freeSpace = getBytes(poolStats['Avail'])
            if freeSpace - getBytes(sizeInGb) <= 0:
                return False
            try:
                freePoolPercentage = 100 - int(poolStats['Percentage'])
            except Exception, e:
                freePoolPercentage = int((freeSpace * 100) / getBytes(poolStats['Usable']))
            if freePoolPercentage > percentage:
                return True
            else:
                if freeSpace - getBytes(sizeInGb) > 0:
                    return True
                return False
        else:
            return False

    @classmethod
    def zfsSyncSysUsage(cls, syncSystemId):
        diskStats = dict(used='0K', refer='0K')
        syncSystem = getSyncSystemData(syncSystemId)
        reconID = syncSystem['reconID']
        if (syncSystem['osType'] == 1) and not ('isZvol' in syncSystem and syncSystem['isZvol'] is False):
            path = "%s%s/Linux/" % zfsBase, reconID
            volumes = syncSystem['volumes']
            for vol in volumes:
                lpath = path + createVolumeLabel(syncSystemId, vol['uuid'])
                (status, out, err) = execution("zfs list -o used,refer -rH %s" % lpath)
                if not status:
                    refer = out.strip().split()[1]
                    used = out.strip().split()[0]
                    diskStats['used'] = convertToHumanReadable(getBytes(used) + getBytes(diskStats['used']))
                    diskStats['refer'] = convertToHumanReadable(getBytes(refer) + getBytes(diskStats['refer']))
                else:
                    print "ERROR: %s" % err
        else:
            path = "%s%s/%s" % (zfsBase, reconID, syncSystemId)
            (status, out, err) = execution("zfs list -o used,refer -rH %s" % path)
            if not status:
                lines = out.strip().split()
                if len(lines) > 0:
                    diskStats['used'] = lines[0]
                    diskStats['refer'] = lines[1]
        diskStats['used'] = getBytes(diskStats['used'])
        diskStats['refer'] = getBytes(diskStats['refer'])
        return diskStats

    @classmethod
    def zfsGetSnapSize(cls, snapShot, reconID):
        try:
            snapPath = "%s%s/%s" % (zfsBase, reconID, snapShot)
            (status, out, err) = execution("zfs list -t snapshot -o used -rH %s" % snapPath)
            if not status:
                return convertSize(getBytes(out.strip()))
            else:
                return "NA"
        except Exception, e:
            return 'NA'

    @classmethod
    def convertionSnapSize(cls, snapShot):
        (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0)
        cloneSnap = snapShot.replace('@', '_') + "_clone"
        if not status:
            logger.info("Error Creating Clone")
            return -1

        total = 0
        path = "/" + ZFSApi.getZfsPath() + cloneSnap
        files = os.listdir(path)
        for file in files:
            if not file.endswith(".meta"):
                total += os.path.getsize(path + '/' + file)

        ZFSApi.destroyClone(snapShot)
        return total

    @classmethod
    def zfsFreeSpace(cls, name):
        # img exports space
        (status, out, err) = execution(
            "zfs list %s%s | grep -v NAME | tr -s ' ' ' ' | cut -d ' ' -f2,3" % (zfsBase, name))
        if not status:
            lines = out.split("\n")
            if len(lines) > 0:
                line = lines[0]
                values = line.split(" ")
                if len(values):
                    return getBytes(values[1])
        return -1

    @classmethod
    def zfsStatus(cls):
        cmd = ("zpool status %s | grep state" % zfsBase)
        (state, out, err) = execution(cmd)
        if not state:
            out = out.split(":")[1].strip()
            if "ONLINE" in out:
                # Checking for zfs list, it will be empty if iscsi disk is down though zpool status shows online
                (state, out, err) = execution("zfs list")
                if len(out.strip()) == 0:
                    logger.info("Storage Pool Unavailable, zfs list is empty")
                    return False, "Unavailable"
            elif "UNAVAIL" in out:
                logger.info("Storage Pool Unavailable")
                return False, "Unavailable"
            elif "DEGRADED" in out:
                logger.info("Storage Pool Degraded")
                return False, "Degraded"
            elif "FAULTED" in out:
                logger.info("Storage Pool Faulted")
                return False, "Faulted"
        else:
            logger.info("zpool status command execution failed")
            return False, "Unavailable"

        return True, "Available"

    @classmethod
    def fuseAddMbr(cls, basePath, fusePath, syncSystem):
        if not os.path.exists(fusePath):
            execution("mkdir %s" % fusePath)
        else:
            (status, out, err) = execution("mount | grep %s" % fusePath)
            if not status:
                (status, out, err) = execution("/bin/fusermount -u %s" % fusePath)
                if status:
                    logger.info("addMbr files are already present in the path:%s" % fusePath)
                    return False
            if len(os.listdir(fusePath)) == 0:
                execution("/bin/fusermount -u %s" % fusePath)
            else:
                logger.info("addMbr files are already present in the path:%s" % fusePath)
                return False

        volumes = ZFSApi.listFiles(basePath)
        vols_list = []
        isXp = False
        if "windows xp" in syncSystem['osName'].lower():
            isXp = True

        for vol in volumes:
            mFile = basePath + vol + ".meta"
            if os.path.exists(mFile):
                metaFile = open(mFile, "r")
                data = json.load(metaFile)
                metaFile.close()
                if os.path.getsize(basePath + vol) < 1048576:
                    continue

                if not data['isBootable'] and not isXp:
                    vols_list.append(vol + ",0,")

        cmd = "/usr/share/nginx/www/framework/backend/add_mbr %s %s %s" % (" ".join(vols_list), basePath, fusePath)
        (status, out, err) = execution(cmd)
        if not status:
            return True
        else:
            logger.info("add_mbr failed status:%s out:%s err:%s" % (status, out, err))
            return False

    @classmethod
    def fuseDestroy(cls, fusePath):
        if os.path.exists(fusePath):
            cmd = "/bin/fusermount -u %s" % fusePath
            (status, out, err) = execution(cmd)
            if not status:
                if "_fuse" in fusePath:
                    (status, out, err) = execution("rm -rf %s" % fusePath)
                    logger.info("fuse dir cleanup status:%s out:%s err:%s" % (status, out, err))
                return True
            else:
                return False
        return True

    @classmethod
    def userConsumePercent(cls, reconID):
        (status, out, err) = execution("zfs get -H -o value quota %s%s" % (zfsBase, reconID))
        quota = out.strip()
        if not quota.lower() == "none":
            (status, out, err) = execution("zfs list -H -o used %s/%s" % (zfsBase, reconID))
            used = getBytes(out.strip())
            total = getBytes(quota)
            usedAvg = int(math.ceil((used / total) * 100))
            return usedAvg
        return 0
