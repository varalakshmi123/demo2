import pandas
import datetime
# from application import ReconExecutionDetailsLog
# from hdf5interface import HDF5Interface
import config
from pymongo import MongoClient
from bson.objectid import ObjectId

mongo_client = MongoClient('mongodb://' + 'localhost' + ':27017')
erecondb = mongo_client['erecon']
query = {"retFeedFileName": config.mftpath + 'RET_TCC/RET file.csv',
         "tccFeedFileName": config.mftpath + 'RET_TCC/TCC File.csv', 'statementData': '01/06/17'}
query['reconId'] = 'CB_CASH_APAC_61171'
retDf = pandas.read_csv(query['retFeedFileName'])
tccDf = pandas.read_csv(query['tccFeedFileName'])
leftFrame = pandas.concat([retDf, tccDf], ignore_index=True)

leftFrame['RRN'] = leftFrame['RRN'].str.replace("'",'')
# print leftFrame.head()
# print leftFrame.columns.tolist()

reconId = query['reconId']
statementData = query['statementData']
executionDoc = list(erecondb['recon_execution_details_log'].find({'RECORD_STATUS': 'ACTIVE', 'RECON_ID': reconId,
                                                                  'EXECUTION_STATUS': 'Completed',
                                                                  'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                                  'STATEMENT_DATE': {
                                                                      '$gte': datetime.datetime.strptime(statementData,
                                                                                                         '%d/%m/%y'),
                                                                      '$lte': datetime.datetime.strptime(statementData,
                                                                                                         '%d/%m/%y')}}))
if len(executionDoc) > 0:
    if len(executionDoc) == 1:
        execId = executionDoc[0]['RECON_EXECUTION_ID']
        key_path = executionDoc[0]['READ_CLONE_PATH']
    else:
        excDoc = []
        for i in executionDoc:
            excDoc.append({'RECON_EXECUTION_ID': i['RECON_EXECUTION_ID'],
                           'reconId': reconId
                              , 'READ_CLONE_PATH': i['READ_CLONE_PATH']})
        df = pandas.DataFrame(excDoc)
        df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(float)
        execId = df['RECON_EXECUTION_ID'].max()
        key_path = df['READ_CLONE_PATH'][df['RECON_EXECUTION_ID'] == execId].max()

    iterator = pandas.read_hdf('/' + key_path + '/' + str(execId) + '.h5', 'TXN_DATA', chunksize=config.hdf5_chunk_size,
                               where="(RECORD_STATUS == 'ACTIVE') & (TXT_5_015 == 'ISSUER' | TXT_5_015 == 'ACQUIRER' ) &(REJECTION_CODE =='RB')")
    # iterator = HDF5Interface(reconId, str(execId).replace('.0', ''), key_path).fetch_all(storeKey='TXN_DATA',
    #                                                                                      where_clause="(RECORD_STATUS == 'ACTIVE') & (TXT_5_015 == 'ISSUER' | TXT_5_015 == 'ACQUIRER' )")

    rightFrame = pandas.DataFrame()
    for item in iterator:
        rightFrame = pandas.concat([rightFrame, item])
    rightFrame = rightFrame[['TXT_32_003','TXT_5_015']]
    leftFrame.rename(columns={'RRN':'TXT_32_003'},inplace=True)
    leftFrame = leftFrame[['TXT_32_003','Txndate']]
    rightFrame = pandas.merge(rightFrame,leftFrame,how='left',on='TXT_32_003',indicator=True)
    # rightFrame = rightFrame[rightFrame['_merge'] == 'left_only']

    print len( rightFrame[rightFrame['_merge'] != 'left_only'])
    print len( rightFrame[rightFrame['_merge'] == 'left_only'])