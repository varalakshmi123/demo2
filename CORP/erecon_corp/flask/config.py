appUrl = "https://127.0.0.1/"
flaskBaseUrl = '/api'
flaskPath = '/usr/share/nginx/www/erecon/flask/'
uiPath = '/usr/share/nginx/www/erecon/ui/'
commonFilesPath = '/usr/share/nginx/www/erecon/ui/app/'
secretKey = '58e57c3c-76cb-4c5c-8416-0f3226bc5e88' #Change per project
applicationModules = ["application", "usermanagement"]
sessionSalt = 'f80423b7-33f2-4b57-ae97-44ef3f8a0c32' #Change per project
cookieName = 'erecon' #Change per project
cookieDomain = '' #Change per project
sessionTimeoutSecs = 86400

logBaseDir = '/var/log/erecon/' #Change per project
dataBackupDir = '/tmp/data/erecon/' #Change per project

flaskLogFile="apiaccess"
flaskMaxBytes=50000000
flaskBackupCount=100
running = False
contentDir = '/usr/share/nginx/www/erecon/ui/app/files'
usersMongoHost = 'localhost'
usersDatabaseName = 'erecon' #Change per project
mongoHost = 'localhost'
databaseName = 'erecon' #Change per project
isReplicaSet = False
replicaSetName = ''

companyDatabaseName = "crmframework"
isAllowTicketCreation = False
####For Calling Purpose
ACCOUNT_SID = "AC16772aca9e88c27b34b9d6f2cf0df11f"
AUTH_TOKEN = "28fe815d7064d6b83e8f07a159f63978"

ticketServer = "smtp.gmail.com"
ticketPort = 465
ticketConnectionSecurity = "SSL/TLS"
ticketUserName = "hash.appointment@gmail.com"
ticketPassword = "hash@1234"

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

cash_columnsToConvert = ["LINK_ID", "RAW_LINK", "PARTICIPATING_RECORD_TXN_ID", "OBJECT_OID", "OBJECT_ID",
                         "RECON_EXECUTION_ID", "PREV_LINK_ID"]

# Recon Ids that needs account number valication while forcematching the records
account_number_validation = ['SUSP_CASH_APAC_18646', 'TRSC_CASH_APAC_20181', 'TF_CASH_APAC_20063',
                             'SUSP_CASH_APAC_20380', 'SUSP_CASH_APAC_20381', 'SUSP_CASH_APAC_20382',
                             'ECollections_CASH_APAC_20469', 'ECollections_CASH_APAC_20470']
execution_summary_columns = ['RECON_ID', 'RECON_NAME', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME', 'STATEMENT_DATE',
                             'TOTAL_TRANSACTIONS', 'CARRYFORWARD_COUNT', 'PERFECT_MATCHED_RECORDS_COUNT',
                             'EXCEPTION_RECORDS_COUNT', 'BUSINESS_CONTEXT_ID']

# By-pass currency validation for the below recons
avoid_currency_validation = ['EPYMTS_CASH_APAC_20229', 'EPYMTS_CASH_APAC_19106', 'EPYMTS_CASH_APAC_19104',
                             'EPYMTS_CASH_APAC_19110', 'EPYMTS_CASH_APAC_20221',
                             'EPYMTS_CASH_APAC_20223', 'EPYMTS_CASH_APAC_20227', 'EPYMTS_CASH_APAC_19108',
                             'EPYMTS_CASH_APAC_20225', 'EPYMTS_CASH_APAC_20201', 'CLG_CASH_APAC_20161']

# curreny validation will be performed only on below recons
currency_validation = ['FIC_CASH_APAC_18663', 'FIC_CASH_APAC_19074', 'FIC_CASH_APAC_19076', 'FIC_CASH_APAC_19082',
                       'FIC_CASH_APAC_19084', 'FIC_CASH_APAC_19086', 'TRSC_CASH_APAC_20181', 'TRSC_CASH_APAC_18904',
                       'TF_CASH_APAC_20261', 'TF_CASH_APAC_20263', 'TF_CASH_APAC_20262']

staticCols = ['SOURCE_TYPE', 'SOURCE_NAME', 'ACCOUNT_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME',
              'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE',
              'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS',
              'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'OBJECT_ID', 'OBJECT_OID',
              'BUSINESS_CONTEXT_ID', 'GROUPED_FLAG', 'UPDATED_DATE', 'UPDATED_BY', 'TXN_MATCHING_STATE_CODE',
              'TXN_MATCHING_STATUS', 'TXN_MATCHING_STATE', 'AUTHORIZATION_BY', 'RECON_ID',
              'RECORD_STATUS', 'RECORD_VERSION', 'SESSION_ID', 'ENTRY_TYPE', 'IPADDRESS', 'RAW_LINK', 'INVESTIGATED_BY',
              'RESOLUTION_COMMENTS', 'INVESTIGATOR_COMMENTS', 'UPDATED_BY', 'ACTION_BY', 'SOURCE_ID']

three_src_recon = ["CLG_CASH_APAC_20101", "CLG_CASH_APAC_20121","EPYMTS_NEFT_INWARD_APAC_0011"]
two_decimal_format = ["TRSC_CASH_APAC_20181", "TF_CASH_APAC_20063", "TF_CASH_APAC_20065"]

# TXT_60_001 added for retain leading zeros in benf acc for 20201
# GL_ACCOUNT_NUM,TXT_60_002,TXT_60_003 for bs_cash_apac_19128
dataTypes = {'OBJECT_OID': 'int64', 'LINK_ID': 'int64', 'RAW_LINK': 'int64', 'PARTICIPATING_RECORD_TXN_ID': 'int64',
             'OBJECT_ID': 'int64', 'PREV_LINK_ID': 'int64', 'ACCOUNT_NUMBER': 'str', 'TXT_60_001': 'str',
             'GL_ACCOUNT_NUMBER': 'str', 'TXT_60_002': 'str', 'TXT_60_003': 'str', 'BUSINESS_CONTEXT_ID': 'int64'}

# ReconID to Custom validation function name
customValidation = {'RA_CASH_APAC_61204': 'RA_61204', 'TW_CASH_APAC_61208': 'TW_61208'}

# To limit the number of records fetched
threshold = 20000

# export all report static columns
reportCols = ['SOURCE_TYPE',  'ACCOUNT_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME',
              'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE',
              #'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS',
              #'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'INVESTIGATED_BY',
              'RESOLUTION_COMMENTS', 'TXN_MATCHING_STATUS', 'TXN_MATCHING_STATE', 'INVESTIGATOR_COMMENTS', 'UPDATED_BY']

# exception master data_types
excepMasterDataTypes = {'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64',
                        'BUSINESS_CONTEXT_ID': 'int64'}

# exception allocation data_types
excepAllocDataTypes = {'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64', 'EXCEPTION_ALLOCATION_OID': 'int64',
                       'BUSINESS_CONTEXT_ID': 'int64', 'WORK_POOL_ID': 'int64'}
execution_log_dateparse = ['CREATED_DATE', 'STATEMENT_DATE', 'EXECUTION_DATE']

# limit threshold for the following recons
threshold_recons = ['GLBAL_CASH_APAC_20461']

# ignore matched transactions, since exceeding limited count
ignore_matched_txns = ['GLBAL_CASH_APAC_20461']
loginCount = 5

# ssh paremeters to connecting to engine
engineIp = 'localhost'
engineUserName = 'root'
enginePassword = 'recon@123'
enginePath = '/usr/share/nginx/v8/'
mftpath = '/usr/share/nginx/v8/mft/'

# Oracel database properties
ip = 'localhost'
port = 1521
SID = 'XE'
connection_name = 'ALGORECONUTIL_NEW'
connection_pwd = 'algorecon'
app_ip = 'localhost'
engine_ip = 'localhost'

# ZFS Location
hdfFilesPathNew = '/erecon/'



# # sql database properties
# ip = '172.30.3.42'
# port = 49413
# SID = ''
# connection_name = 'neftuser'
# connection_pwd =  'Welcome1'
# app_ip = 'localhost'
# engine_ip = 'localhost'

# Recon Migration Parameters
migrationFolder = '/usr/share/nginx/www/erecon/flask/'
reconMigrationFolder = '/home/hduser/reconmigrations/'
mongoPath = '/home/hduser/mongodb-linux-x86_64-2.4.9/bin/'

# HDF5 File location
#hdf5_location = '/usr/share/nginx/v8/zfs/ReconData/'
hdf5_location = '/erecon'
zfsBase = "erecon/"
zfsBaseName = "erecon"
hdf5_chunk_size = 10000
hdfMatchedLimit = 10000
hdf5_write_chunk = 30000
hdf5_bk_loc = '/usr/share/nginx/v8/zfs/ReconData/HDF5_BACKUP/'
data_columns = ['OBJECT_OID', 'LINK_ID', 'RECORD_STATUS', 'UPDATED_DATE', 'UPDATED_BY']

# Pytables queries

unmatched_qry = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' == 'UNMATCHED')"
matched_qry = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' == 'MATCHED')"
waiting_auth_qry = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') " \
                   "& ('TXN_MATCHING_STATUS' == 'AUTHORIZATION_PENDING')"
roll_aut_qry = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') " \
               "& ('TXN_MATCHING_STATUS' == 'ROLLBACK_AUTHORIZATION_PENDING')"

# states mapper, used to set next txn_matching_state field in hdf5 interface update txn
txn_state_mapper = {'SINGLE_SOURCE_SUBMISSION_PENDING': 'SINGLE_SOURCE_WAITING_FOR_AUTH',
                    'SINGLE_SOURCE_WAITING_FOR_AUTH': 'SINGLE_SOURCE_AUTHORIZED',
                    'SUBMISSION_PENDING': 'WAITING_FOR_AUTHORIZATION',
                    'WAITING_FOR_AUTHORIZATION':'FORCE_MATCHED'}

store_key = 'TXN_DATA'
