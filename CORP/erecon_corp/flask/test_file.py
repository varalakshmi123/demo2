from bson import ObjectId

recon = {
    "S1": {},
    "S2": {},
    "_id": ObjectId("585a3f98d2a70959323b85a8"),
    "businessContext": {
        "BUSINESS_CONTEXT_ID": 11113043120,
        "assetClass": "CASH",
        "_id": "57f494dc1a1bc773fd6a345b",
        "created": 1475626884,
        "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
        "updated": 1475626884,
        "WORKPOOL_NAME": "Treasury Pool",
        "functionalArea": "Wholesale",
        "BUSINESS_PROCESS_NAME": "Treasury",
        "partnerId": "54619c820b1c8b1ff0166dfc",
        "reconProcessCode": "TRE",
        "GEOGRAPHY_CODE": "APAC"
    },
    "businessContextId": 11113043120,
    "created": 1476628992,
    "currency": "INR",
    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
    "matchingRules": [{
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1",
            "conditions": ["s1_g1 = s1[(s1['DEBIT_CREDIT_INDICATOR']=='CR')]",
                           "s1_g1_fil = s1[(s1['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2",
            "conditions": ["s2_g1 = s2[(s2['DEBIT_CREDIT_INDICATOR']=='DR')]",
                           "s2_g1_fil = s2[(s2['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER1), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str' , 'REF_NUMBER1:str'],['CURRENCY:str' , 'REF_NUMBER1:str'])",
            "ruleName": "NOSTRO MATCH 1"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER1:str'],['REF_NUMBER2:str'])",
            "ruleName": "NOSTRO MATCH 2"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER1), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str' , 'REF_NUMBER2:str'],['CURRENCY:str' , 'REF_NUMBER1:str'])",
            "ruleName": "NOSTRO MATCH 3"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str' , 'REF_NUMBER2:str'],['CURRENCY:str' , 'REF_NUMBER2:str'])",
            "ruleName": "NOSTRO MATCH 4"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER1), SWIFT (REF_NUMBER3), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 3",
                            "mdlFieldName": "REF_NUMBER3",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502446"
                                },
                                "label": "REF_NUMBER3"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_9",
                            "uiDisplayName": "Reference Number 3",
                            "position": 9
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 5"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER1), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "sourcePosition": "2",
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER3",
                            "displayName": "Narration",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 6,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "sourcePosition": "1",
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s2_g1 ,s1_g1 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 6"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER2), SWIFT (REF_NUMBER3), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 3",
                            "mdlFieldName": "REF_NUMBER3",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502446"
                                },
                                "label": "REF_NUMBER3"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_9",
                            "uiDisplayName": "Reference Number 3",
                            "position": 9
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g1 ,s2_g1 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 7"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER2), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "sourcePosition": "2",
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER3",
                            "displayName": "Narration",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 6,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "sourcePosition": "1",
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s2_g1 ,s1_g1 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 8"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 1,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g1",
            "conditions": ["s1_g2 = s1_g1[(s1_g1['DEBIT_CREDIT_INDICATOR']=='DR')]",
                           "s1_g2_fil = s1_g1[(s1_g1['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g1",
            "conditions": ["s2_g2 = s2_g1[(s2_g1['DEBIT_CREDIT_INDICATOR']=='CR')]",
                           "s2_g2_fil = s2_g1[(s2_g1['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY,  REF_NUMBER1), SWIFT (CURRENCY,  REF_NUMBER1), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' , 'REF_NUMBER1:str'],['CURRENCY:str' ,  'REF_NUMBER1:str'])",
            "ruleName": "NOSTRO MATCH 11"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY,  REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' ,  'REF_NUMBER1:str'],['CURRENCY:str' ,  'REF_NUMBER2:str'])",
            "ruleName": "NOSTRO MATCH 12"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY,  REF_NUMBER2), SWIFT (CURRENCY,  REF_NUMBER1), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' ,  'REF_NUMBER2:str'],['CURRENCY:str' ,  'REF_NUMBER1:str'])",
            "ruleName": "NOSTRO MATCH 13"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY,  REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' ,  'REF_NUMBER2:str'],['CURRENCY:str' ,  'REF_NUMBER2:str'])",
            "ruleName": "NOSTRO MATCH 14"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER1), SWIFT (REF_NUMBER3), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER1",
                            "displayName": "Transaction Reference",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 3,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 3",
                            "mdlFieldName": "REF_NUMBER3",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502446"
                                },
                                "label": "REF_NUMBER3"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_9",
                            "uiDisplayName": "Reference Number 3",
                            "position": 9
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' ],['CURRENCY:str' ], ['REF_NUMBER1:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 15"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER1), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "sourcePosition": "2",
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER3",
                            "displayName": "Narration",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 6,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "sourcePosition": "1",
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 1",
                            "mdlFieldName": "REF_NUMBER1",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502444"
                                },
                                "label": "REF_NUMBER1"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_7",
                            "uiDisplayName": "Reference Number 1",
                            "position": 7
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s2_g2 ,s1_g2 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 16"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER2), SWIFT (REF_NUMBER3), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER2",
                            "displayName": "ExtPaymentRefNumber",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 12,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 3",
                            "mdlFieldName": "REF_NUMBER3",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502446"
                                },
                                "label": "REF_NUMBER3"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_9",
                            "uiDisplayName": "Reference Number 3",
                            "position": 9
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g2 ,s2_g2 ,['CURRENCY:str' ],['CURRENCY:str'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 17"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER2), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "sourcePosition": "2",
                    "fuzzyColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "REF_NUMBER3",
                            "displayName": "Narration",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 6,
                            "type": "D"
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "sourcePosition": "1",
                    "fuzzyColumns": [{
                        "id": {
                            "fieldName": "Reference Number 2",
                            "mdlFieldName": "REF_NUMBER2",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999161,
                                    "created": 1473999161,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe911a1bc74b62502445"
                                },
                                "label": "REF_NUMBER2"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_8",
                            "uiDisplayName": "Reference Number 2",
                            "position": 8
                        }
                    }],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s2_g2 ,s1_g2 ,['CURRENCY:str'],['CURRENCY:str'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'])",
            "ruleName": "NOSTRO MATCH 18"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 2,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g2",
            "conditions": [
                "s1_g3 = s1_g2[(s1_g2['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) & (s1_g2['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g3_fil = s1_g2[(~s1_g2['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) | (s1_g2['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g2",
            "conditions": [
                "s2_g3 = s2_g2[(s2_g2['REF_NUMBER1'].fillna('').str.contains('CLS', case = False)) & (s2_g2['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g3_fil = s2_g2[(~s2_g2['REF_NUMBER1'].fillna('').str.contains('CLS', case = False)) | (s2_g2['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Value Date",
                            "mdlFieldName": "VALUE_DATE",
                            "dataType": "np.datetime64",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999167,
                                    "created": 1473999167,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "VALUE_DATE",
                                    "MDL_FIELD_DATATYPE": "np.datetime64",
                                    "_id": "57dbbe971a1bc74b625024d6"
                                },
                                "label": "VALUE_DATE"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_1",
                            "uiDisplayName": "Value Date",
                            "datePattern": "%d%m%Y",
                            "position": 3
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g3 ,s2_g3 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64'],aggr1=None,aggr2='AMOUNT')",
            "ruleName": "NOSTRO MATCH 21"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "datePattern": "",
                            "position": 6,
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "datePattern": "%d%m%Y",
                            "position": 4,
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "datePattern": "",
                            "position": 7,
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g3 ,s2_g3 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "ruleName": "CLS V/s Forex Netting"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 3,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g3",
            "conditions": [
                "s1_g4 = s1_g3[(s1_g3['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) & (s1_g3['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g4_fil = s1_g3[(~s1_g3['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) | (s1_g3['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g3",
            "conditions": [
                "s2_g4 = s2_g3[(s2_g3['REF_NUMBER1'].fillna('').str.contains('CCF', case = False)) & (s2_g3['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g4_fil = s2_g3[(~s2_g3['REF_NUMBER1'].fillna('').str.contains('CCF', case = False)) | (s2_g3['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Value Date",
                            "mdlFieldName": "VALUE_DATE",
                            "dataType": "np.datetime64",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999167,
                                    "created": 1473999167,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "VALUE_DATE",
                                    "MDL_FIELD_DATATYPE": "np.datetime64",
                                    "_id": "57dbbe971a1bc74b625024d6"
                                },
                                "label": "VALUE_DATE"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_1",
                            "uiDisplayName": "Value Date",
                            "datePattern": "%d%m%Y",
                            "position": 3
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g4 ,s2_g4 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64'],aggr1=None,aggr2='AMOUNT')",
            "ruleName": "NOSTRO MATCH 22"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE, AMOUNT), SWIFT ()"
            },
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "datePattern": "",
                            "position": 6,
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "datePattern": "%d%m%Y",
                            "position": 4,
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "datePattern": "",
                            "position": 7,
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g4 ,s2_g4 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "ruleName": "CCF V/s Forex Netting"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 4,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g4",
            "conditions": [
                "s1_g5 = s1_g4[(s1_g4['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) & (s1_g4['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s1_g5_fil = s1_g4[(~s1_g4['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING', case = False)) | (s1_g4['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g4",
            "conditions": [
                "s2_g5 = s2_g4[(s2_g4['REF_NUMBER1'].fillna('').str.contains('PAYIN', case = False)) & (s2_g4['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s2_g5_fil = s2_g4[(~s2_g4['REF_NUMBER1'].fillna('').str.contains('PAYIN', case = False)) | (s2_g4['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Value Date",
                            "mdlFieldName": "VALUE_DATE",
                            "dataType": "np.datetime64",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999167,
                                    "created": 1473999167,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "VALUE_DATE",
                                    "MDL_FIELD_DATATYPE": "np.datetime64",
                                    "_id": "57dbbe971a1bc74b625024d6"
                                },
                                "label": "VALUE_DATE"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_1",
                            "uiDisplayName": "Value Date",
                            "datePattern": "%d%m%Y",
                            "position": 3
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g5 ,s2_g5 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64'],aggr1=None,aggr2='AMOUNT')",
            "ruleName": "NOSTRO MATCH 23"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "datePattern": "",
                            "position": 6,
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "datePattern": "%d%m%Y",
                            "position": 4,
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "datePattern": "",
                            "position": 7,
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g5 ,s2_g5 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "ruleName": "PAYIN V/s Forex Netting"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 5,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g5",
            "conditions": [
                "s1_g6 = s1_g5[(s1_g5['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING')) & (s1_g5['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s1_g6_fil = s1_g5[(~s1_g5['REF_NUMBER3'].fillna('').str.contains('FOREX NETTING')) | (s1_g5['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g5",
            "conditions": [
                "s2_g6 = s2_g5[((s2_g5['REF_NUMBER3'].fillna('').str.contains('CLEARING CORPORATION OF INDIA', case = False)) | (s2_g5['REF_NUMBER3'].fillna('').str.contains('CLEARING COPORATION OF INDIA', case = False))) & (s2_g5['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s2_g6_fil = s2_g5[((~s2_g5['REF_NUMBER3'].fillna('').str.contains('CLEARING CORPORATION OF INDIA', case = False)) & (~s2_g5['REF_NUMBER3'].fillna('').str.contains('CLEARING COPORATION OF INDIA', case = False))) | (s2_g5['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE), SWIFT ()"
            },
            "tag": "PERFECT",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "fieldName": "Currency",
                            "mdlFieldName": "CURRENCY",
                            "dataType": "str",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999156,
                                    "created": 1473999156,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "CURRENCY",
                                    "MDL_FIELD_DATATYPE": "str",
                                    "_id": "57dbbe8c1a1bc74b625023c6"
                                },
                                "label": "CURRENCY"
                            },
                            "enableEdit": False,
                            "displayType": "D",
                            "tag": "60_3",
                            "uiDisplayName": "Currency",
                            "position": 2
                        }
                    }, {
                        "id": {
                            "fieldName": "Value Date",
                            "mdlFieldName": "VALUE_DATE",
                            "dataType": "np.datetime64",
                            "mdlFieldDropDownVal": {
                                "Default": {
                                    "updated": 1473999167,
                                    "created": 1473999167,
                                    "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId": "54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID": "VALUE_DATE",
                                    "MDL_FIELD_DATATYPE": "np.datetime64",
                                    "_id": "57dbbe971a1bc74b625024d6"
                                },
                                "label": "VALUE_DATE"
                            },
                            "enableEdit": False,
                            "displayType": "M",
                            "tag": "61_1",
                            "uiDisplayName": "Value Date",
                            "datePattern": "%d%m%Y",
                            "position": 3
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g6 ,s2_g6 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64'],aggr1=None,aggr2='AMOUNT')",
            "ruleName": "NOSTRO MATCH 24"
        }, {
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (CURRENCY, VALUE_DATE, AMOUNT), SWIFT (CURRENCY, VALUE_DATE, AMOUNT), SWIFT ()"
            },
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 9,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "%d-%m-%Y",
                            "position": 2,
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "datePattern": "",
                            "position": 4,
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "datePattern": "",
                            "position": 6,
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "datePattern": "%d%m%Y",
                            "position": 4,
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "datePattern": "",
                            "position": 7,
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "condition": "self.mfns.performMatch(s1_g6 ,s2_g6 ,['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],['CURRENCY:str' , 'VALUE_DATE:np.datetime64' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "ruleName": "CCI V/s Forex Netting"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "derivedColumnsQB": [],
        "groupIndex": 6,
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g6",
            "conditions": [
                "s1_g7 = s1_g6[(s1_g6['CUSTOMER_NAME'].fillna('').str.contains('DEUTSCHE BANK', case = False)) & (s1_g6['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g7_fil = s1_g6[(~s1_g6['CUSTOMER_NAME'].fillna('').str.contains('DEUTSCHE BANK', case = False)) | (s1_g6['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g6",
            "conditions": [
                "s2_g7 = s2_g6[(s2_g6['REF_NUMBER1'].fillna('').str.contains('DEUT2L', case = False)) & (s2_g6['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g7_fil = s2_g6[(~s2_g6['REF_NUMBER1'].fillna('').str.contains('DEUT2L', case = False)) | (s2_g6['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "allowMultiLevel": True,
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            },
            "tag": "PERFECT",
            "ruleName": "Interbank DEUTSCHE BANK",
            "condition": "self.mfns.performMatch(s1_g7 ,s2_g7 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)"
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 7,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g7",
            "conditions": [
                "s1_g8 = s1_g7[(s1_g7['CUSTOMER_NAME'].fillna('').str.contains('JP MORGAN CHASE', case = False)) & (s1_g7['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g8_fil = s1_g7[(~s1_g7['CUSTOMER_NAME'].fillna('').str.contains('JP MORGAN CHASE', case = False)) | (s1_g7['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g7",
            "conditions": [
                "s2_g8 = s2_g7[(s2_g7['REF_NUMBER3'].fillna('').str.contains('JPMORGAN CHASE OR CHASGB2L', case = False)) & (s2_g7['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g8_fil = s2_g7[(~s2_g7['REF_NUMBER3'].fillna('').str.contains('JPMORGAN CHASE OR CHASGB2L', case = False)) | (s2_g7['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank JP MORGAN CHASE BANK",
            "condition": "self.mfns.performMatch(s1_g8 ,s2_g8 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 8,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g8",
            "conditions": [
                "s1_g9 = s1_g8[(s1_g8['CUSTOMER_NAME'].fillna('').str.contains('STATE BANK OF INDIA', case = False)) & (s1_g8['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g9_fil = s1_g8[(~s1_g8['CUSTOMER_NAME'].fillna('').str.contains('STATE BANK OF INDIA', case = False)) | (s1_g8['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g8",
            "conditions": [
                "s2_g9 = s2_g8[((s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BANK OF INDIA', case = False)) | (s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BANK OF IND', case = False)) | (s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BK OF INDIA', case = False))) & (s2_g8['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g9_fil = s2_g8[((~s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BANK OF INDIA', case = False)) & (~s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BANK OF IND', case = False)) & (~s2_g8['REF_NUMBER3'].fillna('').str.contains('STATE BK OF INDIA', case = False))) | (s2_g8['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank STATE BANK OF INDIA",
            "condition": "self.mfns.performMatch(s1_g9 ,s2_g9 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 9,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g9",
            "conditions": [
                "s1_g10 = s1_g9[(s1_g9['CUSTOMER_NAME'].fillna('').str.contains('CITIBANK', case = False)) & (s1_g9['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g10_fil = s1_g9[(~s1_g9['CUSTOMER_NAME'].fillna('').str.contains('CITIBANK', case = False)) | (s1_g9['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g9",
            "conditions": [
                "s2_g10 = s2_g9[(s2_g9['REF_NUMBER3'].fillna('').str.contains('CITIINB', case = False)) & (s2_g9['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g10_fil = s2_g9[(~s2_g9['REF_NUMBER3'].fillna('').str.contains('CITIINB', case = False)) | (s2_g9['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank CITI BANK",
            "condition": "self.mfns.performMatch(s1_g10 ,s2_g10 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 10,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g10",
            "conditions": [
                "s1_g11 = s1_g10[(s1_g10['CUSTOMER_NAME'].fillna('').str.contains('KOTAK MAHINDRA BANK', case = False)) & (s1_g10['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g11_fil = s1_g10[(~s1_g10['CUSTOMER_NAME'].fillna('').str.contains('KOTAK MAHINDRA BANK', case = False)) | (s1_g10['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g10",
            "conditions": [
                "s2_g11 = s2_g10[(s2_g10['REF_NUMBER3'].fillna('').str.contains('KOTAK MAHINDRA BANK', case = False)) & (s2_g10['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g11_fil = s2_g10[(~s2_g10['REF_NUMBER3'].fillna('').str.contains('KOTAK MAHINDRA BANK', case = False)) | (s2_g10['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank KOTAK MAHINDRA BANK",
            "condition": "self.mfns.performMatch(s1_g11 ,s2_g11 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 11,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g11",
            "conditions": [
                "s1_g12 = s1_g11[(s1_g11['CUSTOMER_NAME'].fillna('').str.contains('AXIS BANK', case = False)) & (s1_g11['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g12_fil = s1_g11[(~s1_g11['CUSTOMER_NAME'].fillna('').str.contains('AXIS BANK', case = False)) | (s1_g11['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g11",
            "conditions": [
                "s2_g12 = s2_g11[(s2_g11['REF_NUMBER3'].fillna('').str.contains('AXIS BANK', case = False)) & (s2_g11['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g12_fil = s2_g11[(~s2_g11['REF_NUMBER3'].fillna('').str.contains('AXIS BANK', case = False)) | (s2_g11['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank AXIS BANK LIMITED",
            "condition": "self.mfns.performMatch(s1_g12 ,s2_g12 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 12,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g12",
            "conditions": [
                "s1_g13 = s1_g12[(s1_g12['CUSTOMER_NAME'].fillna('').str.contains('BANK OF BARODA', case = False)) & (s1_g12['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g13_fil = s1_g12[(~s1_g12['CUSTOMER_NAME'].fillna('').str.contains('BANK OF BARODA', case = False)) | (s1_g12['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g12",
            "conditions": [
                "s2_g13 = s2_g12[(s2_g12['REF_NUMBER3'].fillna('').str.contains('BANK OF BARODA', case = False)) & (s2_g12['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g13_fil = s2_g12[(~s2_g12['REF_NUMBER3'].fillna('').str.contains('BANK OF BARODA', case = False)) | (s2_g12['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank BANK OF BARODA",
            "condition": "self.mfns.performMatch(s1_g13 ,s2_g13 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 13,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g13",
            "conditions": [
                "s1_g14 = s1_g13[(s1_g13['CUSTOMER_NAME'].fillna('').str.contains('DBS BANK', case = False)) & (s1_g13['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g14_fil = s1_g13[(~s1_g13['CUSTOMER_NAME'].fillna('').str.contains('DBS BANK', case = False)) | (s1_g13['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g13",
            "conditions": [
                "s2_g14 = s2_g13[(s2_g13['REF_NUMBER3'].fillna('').str.contains('DBS BANK', case = False)) & (s2_g13['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g14_fil = s2_g13[(~s2_g13['REF_NUMBER3'].fillna('').str.contains('DBS BANK', case = False)) | (s2_g13['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank DBS BANK",
            "condition": "self.mfns.performMatch(s1_g14 ,s2_g14 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 14,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g14",
            "conditions": [
                "s1_g15 = s1_g14[(s1_g14['CUSTOMER_NAME'].fillna('').str.contains('CANARA BANK', case = False)) & (s1_g14['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g15_fil = s1_g14[(~s1_g14['CUSTOMER_NAME'].fillna('').str.contains('CANARA BANK', case = False)) | (s1_g14['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g14",
            "conditions": [
                "s2_g15 = s2_g14[(s2_g14['REF_NUMBER3'].fillna('').str.contains('CANARA BANK', case = False)) & (s2_g14['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g15_fil = s2_g14[(~s2_g14['REF_NUMBER3'].fillna('').str.contains('CANARA BANK', case = False)) | (s2_g14['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank CANARA BANK",
            "condition": "self.mfns.performMatch(s1_g15 ,s2_g15 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 15,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g15",
            "conditions": [
                "s2_g16 = s2_g15[(s2_g15['REF_NUMBER3'].fillna('').str.contains('IDFC BANK LIMITED', case = False)) & (s2_g15['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g16_fil = s2_g15[(~s2_g15['REF_NUMBER3'].fillna('').str.contains('IDFC BANK LIMITED', case = False)) | (s2_g15['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g15",
            "conditions": [
                "s1_g16 = s1_g15[(s1_g15['CUSTOMER_NAME'].fillna('').str.contains('JP MORGAN CHASE', case = False)) & (s1_g15['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g16_fil = s1_g15[(~s1_g15['CUSTOMER_NAME'].fillna('').str.contains('JP MORGAN CHASE', case = False)) | (s1_g15['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "vInterbank JP MORGAN CHASE BANK FCY",
            "condition": "self.mfns.performMatch(s1_g16 ,s2_g16 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S1",
            "sourceId": "2fd9d849d",
            "feedId": "5803d769cdc9523a39dbb3a9",
            "useInMatching": True,
            "sourceName": "KPTP",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 16,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g16",
            "conditions": [
                "s1_g17 = s1_g16[(s1_g16['CUSTOMER_NAME'].fillna('').str.contains('BANK OF INDIA', case = False)) & (s1_g16['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g17_fil = s1_g16[(~s1_g16['CUSTOMER_NAME'].fillna('').str.contains('BANK OF INDIA', case = False)) | (s1_g16['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g16",
            "conditions": [
                "s2_g17 = s2_g16[((s2_g16['REF_NUMBER3'].fillna('').str.contains('BANK OF INDIA', case = False)) | (s2_g16['REF_NUMBER3'].fillna('').str.contains('BKID', case = False))) & (s2_g16['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g17_fil = s2_g16[((~s2_g16['REF_NUMBER3'].fillna('').str.contains('BANK OF INDIA', case = False)) & (~s2_g16['REF_NUMBER3'].fillna('').str.contains('BKID', case = False))) | (s2_g16['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank BANK OF INDIA",
            "condition": "self.mfns.performMatch(s1_g17 ,s2_g17 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 17,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g17",
            "conditions": [
                "s1_g18 = s1_g17[(s1_g17['CUSTOMER_NAME'].fillna('').str.contains('IFCI LIMITED', case = False)) & (s1_g17['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g18_fil = s1_g17[(~s1_g17['CUSTOMER_NAME'].fillna('').str.contains('IFCI LIMITED', case = False)) | (s1_g17['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g17",
            "conditions": [
                "s2_g18 = s2_g17[(s2_g17['REF_NUMBER1'].fillna('').str.contains('FCDEAL', case = False)) & (s2_g17['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s2_g18_fil = s2_g17[(~s2_g17['REF_NUMBER1'].fillna('').str.contains('FCDEAL', case = False)) | (s2_g17['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Interbank IFCI",
            "condition": "self.mfns.performMatch(s1_g18 ,s2_g18 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 18,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g18",
            "conditions": [
                "s1_g19 = s1_g18[(s1_g18['REF_NUMBER3'].fillna('').str.contains('KTPP FT', case = False)) & (s1_g18['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s1_g19_fil = s1_g18[(~s1_g18['REF_NUMBER3'].fillna('').str.contains('KTPP FT', case = False)) | (s1_g18['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g18",
            "conditions": [
                "s2_g19 = s2_g18[(s2_g18['REF_NUMBER1'].fillna('').str.contains('B2B FT', case = False)) & (s2_g18['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s2_g19_fil = s2_g18[(~s2_g18['REF_NUMBER1'].fillna('').str.contains('B2B FT', case = False)) | (s2_g18['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Nostro Fund Transfer",
            "condition": "self.mfns.performMatch(s1_g19 ,s2_g19 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 19,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    }, {
        "derivedSources": [{
            "derivedSourceIndicator": True,
            "dfLabel": "s1_g19",
            "conditions": [
                "s1_g20 = s1_g19[(s1_g19['REF_NUMBER3'].fillna('').str.contains('KTPP FT', case = False)) & (s1_g19['DEBIT_CREDIT_INDICATOR']=='CR')]",
                "s1_g20_fil = s1_g19[(~s1_g19['REF_NUMBER3'].fillna('').str.contains('KTPP FT', case = False)) | (s1_g19['DEBIT_CREDIT_INDICATOR']!='CR')]"],
            "sourceType": "S1"
        }, {
            "derivedSourceIndicator": True,
            "dfLabel": "s2_g19",
            "conditions": [
                "s2_g20 = s2_g19[(s2_g19['REF_NUMBER1'].fillna('').str.contains('B2B FT', case = False)) & (s2_g19['DEBIT_CREDIT_INDICATOR']=='DR')]",
                "s2_g20_fil = s2_g19[(~s2_g19['REF_NUMBER1'].fillna('').str.contains('B2B FT', case = False)) | (s2_g19['DEBIT_CREDIT_INDICATOR']!='DR')]"],
            "sourceType": "S2"
        }],
        "derivedSourcesQB": [],
        "rules": [{
            "columnDisplayString": {
                "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                "normalDiscol": "KPTP (VALUE_DATE, CURRENCY, AMOUNT), SWIFT (VALUE_DATE, CURRENCY, AMOUNT), SWIFT ()"
            },
            "tag": "PERFECT",
            "ruleName": "Nostro Fund Transfer",
            "condition": "self.mfns.performMatch(s1_g20 ,s2_g20 ,['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],['VALUE_DATE:np.datetime64' , 'CURRENCY:str' , 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
            "selDropDownMdlField": {
                "2fd9d849d": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 2,
                            "datePattern": "%d-%m-%Y",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "",
                            "position": 9,
                            "datePattern": "",
                            "type": "D"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "",
                            "position": 4,
                            "datePattern": "",
                            "type": "D"
                        }
                    }]
                },
                "703858a04": {
                    "fuzzyColumns": [],
                    "normalColumns": [{
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "VALUE_DATE",
                            "displayName": "Value Date",
                            "dataType": "np.datetime64",
                            "startIndex": 0,
                            "tag": "61_1",
                            "position": 4,
                            "datePattern": "%d%m%Y",
                            "type": "M",
                            "datePatternOld": "%Y-%m-%d %H:%M:%S.%f"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "CURRENCY",
                            "displayName": "Currency",
                            "dataType": "str",
                            "startIndex": 0,
                            "tag": "60_3",
                            "position": 6,
                            "datePattern": "",
                            "type": "M"
                        }
                    }, {
                        "id": {
                            "endIndex": 0,
                            "mdlFieldName": "AMOUNT",
                            "displayName": "Amount",
                            "dataType": "np.float64",
                            "startIndex": 0,
                            "tag": "61_5",
                            "position": 7,
                            "datePattern": "",
                            "type": "M"
                        }
                    }]
                },
                "9afac1272": {
                    "fuzzyColumns": [],
                    "normalColumns": []
                }
            }
        }],
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "5803d769cdc9523a39dbb3ab",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "derivedColumnsList": {},
        "derivedColumns": [],
        "groupIndex": 20,
        "derivedColumnsQB": [],
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": []
    },
    {
        "derivedSources": [
            {
                "derivedSourceIndicator": True,
                "dfLabel": "s1_g20",
                "conditions": [
                    "s1_g21 = s1_g20[(s1_g20['DEBIT_CREDIT_INDICATOR']=='CR')]",
                    "s1_g21_fil = s1_g20[(s1_g20['DEBIT_CREDIT_INDICATOR']!='CR')]"
                ],
                "sourceType": "S1"
            },
            {
                "derivedSourceIndicator": True,
                "dfLabel": "s2_g20",
                "conditions": [
                    "s2_g21 = s2_g20[(s2_g20['DEBIT_CREDIT_INDICATOR']=='DR')]",
                    "s2_g21_fil = s2_g20[(s2_g20['DEBIT_CREDIT_INDICATOR']!='DR')]"
                ],
                "sourceType": "S2"
            }
        ],
        "derivedColumnsList": {

        },
        "derivedColumns": [

        ],
        "derivedSourcesQB": [

        ],
        "derivedColumnsQB": [

        ],
        "rules": [
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY, REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER1), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "",
                                    "displayName": "Transaction Reference",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 3,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "fieldName": "Reference Number 1",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "61_7",
                                    "uiDisplayName": "Reference Number 1",
                                    "dataType": "str",
                                    "position": 7,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER1",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502444"
                                        },
                                        "label": "REF_NUMBER1"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str' , 'REF_NUMBER1:str', 'AMOUNT:np.float64'],['CURRENCY:str' , 'REF_NUMBER1:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 1"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY, REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            },
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "",
                                    "displayName": "Transaction Reference",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 3,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            },
                            {
                                "id": {
                                    "fieldName": "Reference Number 2",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "61_8",
                                    "uiDisplayName": "Reference Number 2",
                                    "dataType": "str",
                                    "position": 8,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER2",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502445"
                                        },
                                        "label": "REF_NUMBER2"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER1:str'],['REF_NUMBER2:str'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 2"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY, REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER1), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            },
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "",
                                    "displayName": "ExtPaymentRefNumber",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 12,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            },
                            {
                                "id": {
                                    "fieldName": "Reference Number 1",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "61_7",
                                    "uiDisplayName": "Reference Number 1",
                                    "dataType": "str",
                                    "position": 7,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER1",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502444"
                                        },
                                        "label": "REF_NUMBER1"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str' , 'REF_NUMBER2:str', 'AMOUNT:np.float64'],['CURRENCY:str' , 'REF_NUMBER1:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 3"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (), SWIFT (), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY, REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            },
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "",
                                    "displayName": "ExtPaymentRefNumber",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 12,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            },
                            {
                                "id": {
                                    "fieldName": "Reference Number 2",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "61_8",
                                    "uiDisplayName": "Reference Number 2",
                                    "dataType": "str",
                                    "position": 8,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER2",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502445"
                                        },
                                        "label": "REF_NUMBER2"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str' , 'REF_NUMBER2:str', 'AMOUNT:np.float64'],['CURRENCY:str' , 'REF_NUMBER2:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 4"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (REF_NUMBER1), SWIFT (REF_NUMBER3), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "",
                                    "displayName": "Transaction Reference",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 3,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "fieldName": "Reference Number 3",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER3",
                                    "tag": "61_9",
                                    "uiDisplayName": "Reference Number 3",
                                    "dataType": "str",
                                    "position": 9,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER3",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502446"
                                        },
                                        "label": "REF_NUMBER3"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 5"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER1), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "sourcePosition": "2",
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER3",
                                    "tag": "",
                                    "displayName": "Narration",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 6,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "sourcePosition": "1",
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "fieldName": "Reference Number 1",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER1",
                                    "tag": "61_7",
                                    "uiDisplayName": "Reference Number 1",
                                    "dataType": "str",
                                    "position": 7,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER1",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502444"
                                        },
                                        "label": "REF_NUMBER1"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s2_g21  ,s1_g21  ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 6"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (REF_NUMBER2), SWIFT (REF_NUMBER3), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "",
                                    "displayName": "ExtPaymentRefNumber",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 12,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "fieldName": "Reference Number 3",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER3",
                                    "tag": "61_9",
                                    "uiDisplayName": "Reference Number 3",
                                    "dataType": "str",
                                    "position": 9,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER3",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502446"
                                        },
                                        "label": "REF_NUMBER3"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s1_g21  ,s2_g21  ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 7"
            },
            {
                "columnDisplayString": {
                    "fuzzyDisCol": "KPTP (REF_NUMBER3), SWIFT (REF_NUMBER2), SWIFT ()",
                    "normalDiscol": "KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
                },
                "tag": "PERFECT",
                "selDropDownMdlField": {
                    "2fd9d849d": {
                        "sourcePosition": "2",
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "REF_NUMBER3",
                                    "tag": "",
                                    "displayName": "Narration",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 6,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "endIndex": 0,
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "",
                                    "displayName": "Currency",
                                    "datePattern": "",
                                    "dataType": "str",
                                    "position": 9,
                                    "type": "D",
                                    "startIndex": 0
                                }
                            }
                        ]
                    },
                    "703858a04": {
                        "sourcePosition": "1",
                        "fuzzyColumns": [
                            {
                                "id": {
                                    "fieldName": "Reference Number 2",
                                    "displayType": "M",
                                    "mdlFieldName": "REF_NUMBER2",
                                    "tag": "61_8",
                                    "uiDisplayName": "Reference Number 2",
                                    "dataType": "str",
                                    "position": 8,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999161,
                                            "created": 1473999161,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "REF_NUMBER2",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe911a1bc74b62502445"
                                        },
                                        "label": "REF_NUMBER2"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ],
                        "normalColumns": [
                            {
                                "id": {
                                    "fieldName": "Currency",
                                    "displayType": "D",
                                    "mdlFieldName": "CURRENCY",
                                    "tag": "60_3",
                                    "uiDisplayName": "Currency",
                                    "dataType": "str",
                                    "position": 2,
                                    "mdlFieldDropDownVal": {
                                        "Default": {
                                            "updated": 1473999156,
                                            "created": 1473999156,
                                            "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                                            "partnerId": "54619c820b1c8b1ff0166dfc",
                                            "MDL_FIELD_ID": "CURRENCY",
                                            "MDL_FIELD_DATATYPE": "str",
                                            "_id": "57dbbe8c1a1bc74b625023c6"
                                        },
                                        "label": "CURRENCY"
                                    },
                                    "enableEdit": False
                                }
                            }
                        ]
                    },
                    "9afac1272": {
                        "fuzzyColumns": [

                        ],
                        "normalColumns": [

                        ]
                    }
                },
                "condition": "self.mfns.performMatch(s2_g21  ,s1_g21  ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
                "ruleName": "NOSTRO MATCH ONE to ONE 8"
            }
        ],
        "groupIndex": 21,
        "selectedSrcVal": {
            "sourceType": "S2",
            "sourceId": "703858a04",
            "feedId": "57f0dba21a1bc709bf6d9302",
            "useInMatching": True,
            "sourceName": "SWIFT",
            "moveToProcessed": True,
            "applyAccountFilter": False
        },
        "selDerivedSrcType": "DERIVED_SRC",
        "virtualSourceArray": [

        ]
    }
        ,{
         "derivedSources":[
            {
               "derivedSourceIndicator":True ,
               "dfLabel":"s1_g21",
               "conditions":[
                  "s1_g22 = s1_g21[(s1_g21['DEBIT_CREDIT_INDICATOR']=='DR')]",
                  "s1_g22_fil = s1_g21[(s1_g21['DEBIT_CREDIT_INDICATOR']!='DR')]"
               ],
               "sourceType":"S1"
            },
            {
               "derivedSourceIndicator":True ,
               "dfLabel":"s2_g21",
               "conditions":[
                  "s2_g22 = s2_g21[(s2_g21['DEBIT_CREDIT_INDICATOR']=='CR')]",
                  "s2_g22_fil = s2_g21[(s2_g21['DEBIT_CREDIT_INDICATOR']!='CR')]"
               ],
               "sourceType":"S2"
            }
         ],
         "derivedColumnsList":{

         },
         "derivedColumns":[

         ],
         "derivedSourcesQB":[

         ],
         "derivedColumnsQB":[

         ],
         "rules":[
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (), SWIFT (), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY,  REF_NUMBER1), SWIFT (CURRENCY,  REF_NUMBER1), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        },
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"",
                              "displayName":"Transaction Reference",
                              "datePattern":"",
                              "dataType":"str",
                              "position":3,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        },
                        {
                           "id":{
                              "fieldName":"Reference Number 1",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"61_7",
                              "uiDisplayName":"Reference Number 1",
                              "dataType":"str",
                              "position":7,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502444"
                                 },
                                 "label":"REF_NUMBER1"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str' , 'REF_NUMBER1:str', 'AMOUNT:np.float64'],['CURRENCY:str' ,  'REF_NUMBER1:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 11"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (), SWIFT (), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY,  REF_NUMBER1), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        },
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"",
                              "displayName":"Transaction Reference",
                              "datePattern":"",
                              "dataType":"str",
                              "position":3,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        },
                        {
                           "id":{
                              "fieldName":"Reference Number 2",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"61_8",
                              "uiDisplayName":"Reference Number 2",
                              "dataType":"str",
                              "position":8,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502445"
                                 },
                                 "label":"REF_NUMBER2"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str' ,  'REF_NUMBER1:str', 'AMOUNT:np.float64'],['CURRENCY:str' ,  'REF_NUMBER2:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 12"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (), SWIFT (), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY,  REF_NUMBER2), SWIFT (CURRENCY,  REF_NUMBER1), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        },
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"",
                              "displayName":"ExtPaymentRefNumber",
                              "datePattern":"",
                              "dataType":"str",
                              "position":12,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        },
                        {
                           "id":{
                              "fieldName":"Reference Number 1",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"61_7",
                              "uiDisplayName":"Reference Number 1",
                              "dataType":"str",
                              "position":7,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502444"
                                 },
                                 "label":"REF_NUMBER1"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str' ,  'REF_NUMBER2:str', 'AMOUNT:np.float64'],['CURRENCY:str' ,  'REF_NUMBER1:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 13"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (), SWIFT (), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY,  REF_NUMBER2), SWIFT (CURRENCY, REF_NUMBER2), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        },
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"",
                              "displayName":"ExtPaymentRefNumber",
                              "datePattern":"",
                              "dataType":"str",
                              "position":12,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        },
                        {
                           "id":{
                              "fieldName":"Reference Number 2",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"61_8",
                              "uiDisplayName":"Reference Number 2",
                              "dataType":"str",
                              "position":8,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502445"
                                 },
                                 "label":"REF_NUMBER2"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str' ,  'REF_NUMBER2:str', 'AMOUNT:np.float64'],['CURRENCY:str' ,  'REF_NUMBER2:str', 'AMOUNT:np.float64'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 14"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (REF_NUMBER1), SWIFT (REF_NUMBER3), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"",
                              "displayName":"Transaction Reference",
                              "datePattern":"",
                              "dataType":"str",
                              "position":3,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[
                        {
                           "id":{
                              "fieldName":"Reference Number 3",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER3",
                              "tag":"61_9",
                              "uiDisplayName":"Reference Number 3",
                              "dataType":"str",
                              "position":9,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502446"
                                 },
                                 "label":"REF_NUMBER3"
                              },
                              "enableEdit":False
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str', 'AMOUNT:np.float64' ],['CURRENCY:str', 'AMOUNT:np.float64' ], ['REF_NUMBER1:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 15"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (REF_NUMBER3), SWIFT (REF_NUMBER1), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "sourcePosition":"2",
                     "fuzzyColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER3",
                              "tag":"",
                              "displayName":"Narration",
                              "datePattern":"",
                              "dataType":"str",
                              "position":6,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "sourcePosition":"1",
                     "fuzzyColumns":[
                        {
                           "id":{
                              "fieldName":"Reference Number 1",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER1",
                              "tag":"61_7",
                              "uiDisplayName":"Reference Number 1",
                              "dataType":"str",
                              "position":7,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER1",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502444"
                                 },
                                 "label":"REF_NUMBER1"
                              },
                              "enableEdit":False
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s2_g22 ,s1_g22 ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER1:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 16"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (REF_NUMBER2), SWIFT (REF_NUMBER3), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "fuzzyColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"",
                              "displayName":"ExtPaymentRefNumber",
                              "datePattern":"",
                              "dataType":"str",
                              "position":12,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "fuzzyColumns":[
                        {
                           "id":{
                              "fieldName":"Reference Number 3",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER3",
                              "tag":"61_9",
                              "uiDisplayName":"Reference Number 3",
                              "dataType":"str",
                              "position":9,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER3",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502446"
                                 },
                                 "label":"REF_NUMBER3"
                              },
                              "enableEdit":False
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s1_g22 ,s2_g22 ,['CURRENCY:str', 'AMOUNT:np.float64' ],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 17"
            },
            {
               "columnDisplayString":{
                  "fuzzyDisCol":"KPTP (REF_NUMBER3), SWIFT (REF_NUMBER2), SWIFT ()",
                  "normalDiscol":"KPTP (CURRENCY), SWIFT (CURRENCY), SWIFT ()"
               },
               "tag":"PERFECT",
               "selDropDownMdlField":{
                  "2fd9d849d":{
                     "sourcePosition":"2",
                     "fuzzyColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"REF_NUMBER3",
                              "tag":"",
                              "displayName":"Narration",
                              "datePattern":"",
                              "dataType":"str",
                              "position":6,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "endIndex":0,
                              "mdlFieldName":"CURRENCY",
                              "tag":"",
                              "displayName":"Currency",
                              "datePattern":"",
                              "dataType":"str",
                              "position":9,
                              "type":"D",
                              "startIndex":0
                           }
                        }
                     ]
                  },
                  "703858a04":{
                     "sourcePosition":"1",
                     "fuzzyColumns":[
                        {
                           "id":{
                              "fieldName":"Reference Number 2",
                              "displayType":"M",
                              "mdlFieldName":"REF_NUMBER2",
                              "tag":"61_8",
                              "uiDisplayName":"Reference Number 2",
                              "dataType":"str",
                              "position":8,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999161,
                                    "created":1473999161,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"REF_NUMBER2",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe911a1bc74b62502445"
                                 },
                                 "label":"REF_NUMBER2"
                              },
                              "enableEdit":False
                           }
                        }
                     ],
                     "normalColumns":[
                        {
                           "id":{
                              "fieldName":"Currency",
                              "displayType":"D",
                              "mdlFieldName":"CURRENCY",
                              "tag":"60_3",
                              "uiDisplayName":"Currency",
                              "dataType":"str",
                              "position":2,
                              "mdlFieldDropDownVal":{
                                 "Default":{
                                    "updated":1473999156,
                                    "created":1473999156,
                                    "machineId":"16af9057-69af-4c69-bbcf-70d99334e027",
                                    "partnerId":"54619c820b1c8b1ff0166dfc",
                                    "MDL_FIELD_ID":"CURRENCY",
                                    "MDL_FIELD_DATATYPE":"str",
                                    "_id":"57dbbe8c1a1bc74b625023c6"
                                 },
                                 "label":"CURRENCY"
                              },
                              "enableEdit":False
                           }
                        }
                     ]
                  },
                  "9afac1272":{
                     "fuzzyColumns":[

                     ],
                     "normalColumns":[

                     ]
                  }
               },
               "condition":"self.mfns.performMatch(s2_g22 ,s1_g22 ,['CURRENCY:str', 'AMOUNT:np.float64'],['CURRENCY:str', 'AMOUNT:np.float64'], ['REF_NUMBER2:str'],['REF_NUMBER3:str'],aggr1=None,aggr2=None)",
               "ruleName":"NOSTRO MATCH ONE to ONE 18"
            }
         ],
         "groupIndex":22,
         "selectedSrcVal":{
            "sourceType":"S2",
            "sourceId":"703858a04",
            "feedId":"57f0dba21a1bc709bf6d9302",
            "useInMatching":True ,
            "sourceName":"SWIFT",
            "moveToProcessed":True ,
            "applyAccountFilter":False
         },
         "selDerivedSrcType":"DERIVED_SRC",
         "virtualSourceArray":[

         ]
      }],
    "partnerId": "54619c820b1c8b1ff0166dfc",
    "processingCount": "2",
    "processingLevel": "Transaction Level",
    "processingType": "doubleEntry",
    "reconId": "TRSC_CASH_APAC_20181",
    "reconName": "Nostro Reconciliation",
    "sources": [{
        "sourceType": "S1",
        "sourceId": "2fd9d849d",
        "feedId": "5803d769cdc9523a39dbb3a9",
        "useInMatching": True,
        "sourceName": "KPTP",
        "moveToProcessed": True,
        "applyAccountFilter": False
    }, {
        "sourceType": "S1",
        "sourceId": "5a1c0cb9d",
        "feedId": "57f0d3fc1a1bc709bf6d9301",
        "useInMatching": False,
        "sourceName": "KPTP",
        "moveToProcessed": True,
        "applyAccountFilter": False
    }, {
        "sourceType": "S2",
        "sourceId": "703858a04",
        "feedId": "5803d769cdc9523a39dbb3ab",
        "useInMatching": True,
        "sourceName": "SWIFT",
        "moveToProcessed": True,
        "applyAccountFilter": False
    }, {
        "sourceType": "S2",
        "sourceId": "9afac1272",
        "feedId": "5803d769cdc9523a39dbb3ac",
        "useInMatching": True,
        "sourceName": "SWIFT",
        "moveToProcessed": True,
        "applyAccountFilter": False
    }],
    "updated": 1497322317
}


from pymongo import MongoClient
from bson.objectid import ObjectId

mongo_client = MongoClient('mongodb://' + 'localhost' + ':27017')
reconbuilderColNew = mongo_client['reconbuilder_uat']
reconbuilderColNew['recon_context_details'].save(recon)

exit(0)
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 set modeline syntax = on
from datetime import datetime
from dateutil.parser import parse
import commands
import time
import pandas
from dateutil.parser import parse
import tables
import numpy as np
import config
from pymongo import MongoClient
from bson.objectid import ObjectId

df = pandas.read_hdf('/erecon/EPYMTS_CASH_APAC_61167/26801_a5e001fa-2b7b-49e0-894e-b891135b159f_clone/26801.h5',
                     'TXN_DATA')

collection = MongoClient('mongodb://localhost:27017/')['erecon']['dynamic_data_setup']

mdl_field = pandas.DataFrame(list(collection.find({"RECON_ID": "EPYMTS_CASH_APAC_61167"})))
cols = mdl_field['MDL_FIELD_ID'].tolist()
df = df.loc[:, cols]

mdl_field.index = mdl_field['MDL_FIELD_ID']
df.rename(columns=mdl_field['UI_DISPLAY_NAME'].to_dict(), inplace=True)

print df.to_csv('IMPS_IW_Settlement_Recon.csv', index=False)

exit(0)
print df
exit(0)
print df[(df[13] == 900002) & (df[14] == 608022)]
# 004010100034606171011111111
# 916010058403900171070000465
# 914010002308024171040220513
exit(0)

# cols = []
# data_set = pandas.read_csv('/home/ubuntu/feedDefination.csv')[['Start Index','End Index','Field Name']]
# for x in data_set.to_records(index=False):
#     cols.append(x)
#
# header = data_set['Field Name'].tolist()
#
# cols = [(1, 3), (4, 5), (6, 7), (8, 9), (10, 21), (22, 23), (24, 42), (43, 43), (44, 49), (50, 61), (62, 67), (68, 73), (74, 77), (78, 83), (84, 98), (99, 106), (107, 146), (147, 157), (158, 160), (161, 179), (180, 189), (190, 208), (209, 218), (219, 221), (222, 236), (237, 251), (252, 266), (267, 269), (270, 284), (285, 299), (300, 314), (315, 317), (318, 332), (333, 347), (348, 362), (363, 377), (378, 392), (393, 407), (408, 457), (458, 460), (461, 471),(472, 501)]
# # print cols
# data = pandas.read_fwf('/home/ubuntu/Desktop/suryoday_bank_doc/Attach4/NPCI data 24 25 26 april/240417/IMPSRawDataSSF240417_4C/ISSRPSSF240417_4C.mSSF',header=None,
#                        colspecs=cols,engine='python',names =header)
# print data.to_csv('NPCI_Fixed_length_extract.csv',index=False)
# exit(0)
# data = data.T
# data.to_csv('/home/ubuntu/Desktop/transpose.csv',index=False)
# #data =  pandas.read_csv('/home/ubuntu/Desktop/suryoday_bank_doc/Attach4/IMPSIN_240417_240417_010402.txt',sep='|',engine='python',skiprows=1,skipfooter=1,header=None)
# #print data
# exit(0)
# mongo_client = MongoClient('mongodb://localhost:27017/')
# mongo_collection = mongo_client.dbName
#
# '''$match, $'''
# print mongo_collection['collectionName'].aggregate(
#     [{'$project':{'accontNumber':{'$group': {'_id': '$ACCOUNT_NUMBER', 'count': {'$sum': 1}}}}},{'$sort':{"count":-1}}])
# exit(0)
# doc = list(mongo_collection['collectionName'].find())
# print doc
# exit(0)
#
# df = pandas.DataFrame(doc)[['ACCOUNT_NUMBER','RECON_ID']]
# df_copy = df.copy()
# print len(df)
# print len(df.drop_duplicates(['ACCOUNT_NUMBER']))
# print df['RECON_ID'].unique()
# print df[df['ACCOUNT_NUMBER'] == 98220102049]['RECON_ID']
#
# df['COUNT'] = 1
# df =  df.groupby(['ACCOUNT_NUMBER'],as_index=False).agg({"COUNT":len})
# print df_copy
# print df_copy[df_copy['ACCOUNT_NUMBER'].isin(df[df['COUNT'] > 1]['ACCOUNT_NUMBER'])]['RECON_ID']
#
# exit(0)

# for val in mongo_collection['recon_context_details'].find({"reconId":"CMSC_CASH_APAC_61166"}):
#     if 'sources' in val:
#         for src in val['sources']:
#             if 'feedId' in src:
#                 feed = mongo_collection['feedDefination'].find_one({'_id': ObjectId(src['feedId'])})
#                 if feed:
#                     print '*' * 10
#                     print val['reconId'] + ',' if 'reconId' in val else '' + ',', val['reconName'] + ',', feed['feedName']
#
# exit(0)
#
#
# df = pandas.read_csv('/home/ubuntu/Downloads/feed/NACH-002F8_20170420.txt',sep='|')
# print df[df['TXN_TYPE'] == 67].head()
#
# exit(0)
store_loc = '/erecon/EPYMTS_NEFT_INWARD_APAC_0009/26714/26714.h5'

print tables.open_file(store_loc, mode="r")
exit(0)
df = pandas.read_hdf(store_loc, 'TXN_DATA', chunksize=config.hdf5_chunk_size)
for item in df:
    print item.head()[['STATEMENT_DATE', 'EXECUTION_DATE_TIME', 'CREATED_DATE']].dtypes
    print len(item.columns)
    exit(0)
# print df.drop_duplicates(['TXN_MATCHING_STATE','TXN_MATCHING_STATUS'])[['TXN_MATCHING_STATE','TXN_MATCHING_STATUS']]
#
# exit(0)
# print len(pandas.read_hdf(store_loc,'TXN_DATA',where=config.unmatched_qry))
# print '*' * 10
# print len(pandas.read_hdf(store_loc,'TXN_DATA',where=config.matched_qry))
# print '*' * 10
# print len(pandas.read_hdf(store_loc,'TXN_DATA',where=config.waiting_auth_qry))
# print '*' * 10
# print len(pandas.read_hdf(store_loc,'TXN_DATA',where=config.roll_aut_qry))
#
# exit(0)
# #
# # df['INDEX'] = df.reset_index().index + 1
# #
# # df.to_hdf('/erecon/EPYMTS_NEFT_OUTWARD_APAC_0010/26444_2017_04_27_17_49_51_clone/26444.h5',
# #                 '/EPYMTS_NEFT_OUTWARD_APAC_0010/cash_output_txn/EPYMTS_NEFT_OUTWARD_APAC_0010_26444/MATCHED',
# #           data_columns=['OBJECT_OID','LINK_ID','TXN_MATCHING_STATUS','INDEX'],format='table')
# # exit(0)
# # print len(df[['RECORD_STATUS','UPDATED_DATE','UPDATED_BY']])
# # exit(0)
# #values = np.asarray(df['OBJECT_OID'].astype('int')[:30])
# values = np.array([1,100,200,400,500])
# print values
#
# key = store_key + '/table'
# count = 0
# start_time = datetime.now()
# with tables.open_file(store_loc, mode='r+') as h5file:
#     table = h5file.get_node(key)
#     where_clause = "(TXN_MATCHING_STATUS == '%s') & (%s == values)" % (
#         str('MATCHED'), str('INDEX'))
#     table.where(where_clause)
#     print datetime.now() - start_time
#     exit(0)
#
#     for value in values:
#         inside_loop = datetime.now()
#         where_clause = "(RECORD_STATUS == 'ACTIVE') & (TXN_MATCHING_STATUS == '%s') & (%s == '%s')" % (
#             str('MATCHED'), str('OBJECT_OID'), str(value))
#         for existing_row in table.where(where_clause):
#             existing_row['RECORD_STATUS'] = 'INACTIVE'
#             existing_row['UPDATED_DATE'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#             existing_row['UPDATED_BY'] = 'configurer2'
#             count += 1
#             print count
#             existing_row.update()
#     table.flush()
#
# print datetime.now() - start_time
