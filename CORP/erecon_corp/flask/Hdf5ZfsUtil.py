import application
from datetime import datetime
import os
from ZFSApi import ZFSApi
from logger import Logger
import traceback
import time

logger = Logger.getInstance("application").getLogger()


class Hdf5ZfsUtil():
    def __init__(self):
        pass

    def createHDFSnapShot(self, job_id=''):
        status, doc = application.ReconJobPost().get({'job_id': job_id})
        try:
            snap_shot_doc = dict()
            snap_shot_doc['store_path'] = doc['recon_id'] + os.sep + doc['recon_execution_id']
            snap_shot_doc['_id'] = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
            snap_path = ZFSApi().snapshot(snap_shot_doc)
            if snap_path is None:
                logger.info('Snap shot path cannot be "None".')
                return False
            else:
                # update snap shot path for the job
                doc['snap_path'] = snap_path
                application.ReconJobPost().modify(doc['_id'], doc)
                logger.info('ZFS Snap Shot Completed for job %s' % job_id)
                return True
        except Exception as e:
            logger.info('Error creating hdf5 restore snap shot for job %s' % job_id)
            logger.info(traceback.format_exc())
            return False

    def restoreHDFSnapShot(self, job_id=''):
        status, doc = application.ReconJobPost().get({'job_id': job_id})
        if 'snap_path' in doc:
            status = ZFSApi().rollBack(doc['snap_path'])
            logger.info('ZFS Snap Shot Restored for job %s' % job_id)
        else:
            logger.info('ZFS Snap Shot not found, ignore restore for job %s' % job_id)
        return True

    def removeHDFSnapShot(self, job_id=''):
        status, doc = application.ReconJobPost().get({'job_id': job_id})
        if status:
            ZFSApi().delete(doc['snap_path'])
            logger.info('ZFS Snap Shot Removed for job %s' % job_id)
            return True
        else:
            logger.info('Job Status not updated for job %s' % job_id)

    def removeStaleSnapShot(self, snapPath=''):
        try:
            if snapPath == '':
                logger.info('removeStalSnapShot exit: Got empty snap shot path.')
                return True
            status = ZFSApi().snapShotExist(snapPath)
            if status:
                while True:
                    status = ZFSApi().delete(snapPath)
                    if status:
                        break
                    else:
                        time.sleep(1)
                        continue
                logger.info('removeStalSnapShot exit: Stale snap shots removed successfully!!')
            else:
                logger.info('removeStalSnapShot exit: No snap shot found with name %s, remove snap shot bypassed', snapPath)
            return True
        except Exception as e:
            logger.info('Error removing stale snap shot for path %s' % snapPath)
            logger.info(traceback.format_exc())
            return False

    # Method to Sync read snap shot with the master hdf5 file in-case if there any un-sysnc in the snaps shots
    def sync_read_snap_shot(self, reconID, execID):
        jobPost = application.ReconJobPost().find_one(
            {"recon_id": reconID, "job_status": "SUCCESS", "recon_execution_id": str(execID)},
            sort=[('created', -1)])
        if jobPost is not None:
            reconExeDetails = application.ReconExecutionDetailsLog().getLatestExeDetails(reconId=reconID)
            # check if updated read snap shot is updated or not
            logger.info('Checking if "%s" snapshot exists or not ??' % reconExeDetails['READ_SNAP_SHOT'])
            if jobPost['uuid'] in reconExeDetails['READ_SNAP_SHOT'] and \
                    ZFSApi().snapShotExist(reconExeDetails['READ_SNAP_SHOT']):
                logger.info('Read snap shot in sync, exiting without refresh !!')
                return True
            else:
                logger.info('Read snap shot not in sync, refresh initiated !!')
                query = dict()
                query['recon_id'] = reconExeDetails['RECON_ID']
                query['recon_execution_id'] = reconExeDetails['RECON_EXECUTION_ID']
                query['uuid'] = jobPost['uuid']
                status = self.update_snap_shot(query)
            return status
        else:
            logger.info('No job details found for recon %s' % reconID)
            return True

    # Method to initialize and update the latest read snap shots
    def update_snap_shot(self, query):
        snap_sht_doc = dict()
        snap_sht_doc['store_path'] = query['recon_id'] + os.sep + query['recon_execution_id']
        snap_sht_doc['_id'] = query['uuid']
        snap_shot_name = ZFSApi().snapshot(snap_sht_doc)
        logger.info('Read snap shot created with name %s', snap_shot_name)
        clone_path = snap_shot_name.replace('@', '_') + '_' + 'clone'
        clone_status = ZFSApi().cloneSnapshot(0, snap_shot_name, clone_path)
        logger.info('Read clone created with name %s', snap_shot_name)

        if snap_shot_name is not None and clone_status:
            status, doc = application.ReconExecutionDetailsLog().get(
                {'PROCESSING_STATE_STATUS': 'Matching Completed', 'RECON_ID': query['recon_id'],
                 'RECON_EXECUTION_ID': query['recon_execution_id'],
                 'RECORD_STATUS': 'ACTIVE'})
            if status:
                old_snap_shot = doc['READ_SNAP_SHOT']
                doc['READ_SNAP_SHOT'] = snap_shot_name
                doc['READ_CLONE_PATH'] = clone_path
                # check to avoid latest read snap shot getting removed
                if query['uuid'] not in old_snap_shot:
                    self.removeStaleSnapShot(snapPath=old_snap_shot)
                application.ReconExecutionDetailsLog().modify(doc['_id'], doc)
                return True
            else:
                logger.info('Recon Execution details not found for recon %s, with execution %s !!',
                            (query['recon_id'], str(query['recon_execution_id'])))
                return False
        elif snap_shot_name is None:
            logger.info('Snap shot name cannot be none, read snap shot not updated for %s' % query['recon_id'])
            return False
        else:
            logger.info('Snap shot " %s " could not be cloned, why ??' % snap_shot_name)
            return False

    ''' Update the Job status of operation, 
    1. on failure rollback, remove snap shot and update status
    2. on success, create read snap shot, remove stale snap shot exit'''
    def updateJobStatus(self, job_id='', job_status='SUCCESS', error_msg='', restore_snapshot=True):
        status, reconObj = application.ReconJobPost().get({'job_id': str(job_id)})

        if job_status == 'FAILURE' or reconObj['job_status'] == 'FAILURE':
            job_status = 'FAILURE'
            error_msg = reconObj['err_msg'] if 'err_msg' in reconObj and reconObj[
                                                                             'err_msg'] != '' else 'Internal Error, Contact Admin'
            if restore_snapshot:
                self.restoreHDFSnapShot(job_id)
                self.removeHDFSnapShot(job_id)
            else:
                logger.info('Proceeding without snap shot restore !!')
        else:
            self.update_snap_shot(reconObj)

        status, reconObj = application.ReconJobPost().get({'job_id': str(job_id)})
        if status:
            reconObj['job_status'] = job_status
            reconObj['err_msg'] = error_msg
            application.ReconJobPost().modify(reconObj['_id'], reconObj)
        else:
            logger.info('Job Post not updated for job id %s, check why??', str(job_id))
