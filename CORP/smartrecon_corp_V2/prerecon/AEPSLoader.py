from FixedFormatLoader import FixedFormatLoader
import config


class AEPSLoader(FixedFormatLoader):
    def __init__(self):
        self.params = {}
        self.feedformatfilecolumns = []
    def getType(self):
        return "AEPS"

    def loadFile(self, fileName, params):
        print params
        if params['type'] == 'INWARD':
            params["feedformatFile"] = "AEPS_INWARD_Structure.csv"
        elif params['type'] == 'OUTWARD':
            params["feedformatFile"] = "AEPS_OUTWARD_Structure.csv"

        else:
            raise KeyError("AEPS loader type not defined for %s" % params['type'])
        npci_data = FixedFormatLoader().loadFile(fileName, params)
        return npci_data
