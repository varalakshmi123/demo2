from traceback import format_exc

import pandas as pd
import numpy as np

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()

'''Usage : To load Delimiter files based on headers names, skiprows to be defined excluding header row and raises 
error if header key not found in the file. 
note : do not include header for skiprows, since files are loader based on the header'''


class FixedHeaderCSVLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "delimiter": ",", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["FeedHeaders", "Description", "DataType", "DatePattern"]

    def getType(self):
        return "FixedHeaderCSV"

    def loadFile(self, fileNames, params):
        dateColumns = list()
        csvTypes = dict()

        if "feedformatFile" in params:
            feedformatfile = params["feedformatFile"]
            df = pd.read_csv(config.basepath + "Reference/" + feedformatfile)
            # to rename original file column name with description
            columnNames = {row['FeedHeaders']: row['Description'] for idx, row in df.iterrows()}
            feedColumns = df['FeedHeaders'].tolist()

            for index, row in df.iterrows():
                if row.get('DataType', '') == 'np.datetime64':
                    dateColumns.append({"column": row['FeedHeaders'], 'dtpatterns': row['DatePattern']})
                    csvTypes[row['FeedHeaders']] = str
                if row.get('DataType', '') == 'np.int64':
                    continue
                csvTypes[row['FeedHeaders']] = eval(row.get('DataType', 'str'))
        else:
            raise ValueError("Structure file mandatory for loading Fixed Header CSV Files")

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:
                c = 0
                for line in open(fileName, 'rU'):
                    if c == params.get('skiprows', 0):
                        headers = (line).replace('\r', '').replace('\n', '').replace('"', '').split(
                            params.get("delimiter", ','))
                        columnPositions = []
                        for indx in range(0, len(feedColumns)):
                            if feedColumns[indx] in headers:
                                columnPositions.append(headers.index(feedColumns[indx]))
                            else:
                                logger.info("Error loading Fixed Header CSV File")
                                logger.info(
                                    "Cannot find the column name %s in file %s" % (str(feedColumns[indx]), fileName))
                                raise ValueError(
                                    "Cannot find the column name %s in file %s " % (str(feedColumns[indx]), fileName))
                    c += 1

                currentFrame = pd.read_csv(fileName, delimiter=params.get("delimiter", ","),
                                           skipfooter=params.get("skipfooter", 0), skiprows=params.get("skiprows", 0),
                                           usecols=columnPositions, converters=csvTypes, engine='python')

                currentFrame.rename(columns=columnNames, inplace=True)
                currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = pd.concat([df, currentFrame], join_axes=[currentFrame.columns])

            except Exception, e:
                logger.info(fileName)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']],
                            axis=1)
                    del df['tdlen']
                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        logger.info('Total Records In Fixed Header CSV File : %s' % str(len(df)))

        return df
