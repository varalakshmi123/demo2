from traceback import format_exc

import numpy as np
import pandas as pd

import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class CSVLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "delimiter": ",", "skipfooter": 0, "skiprows": 0}
        self.feedformatfilecolumns = ["Description", "DataType", "DatePattern"]

    def getType(self):
        return "CSV"

    def loadFile(self, fileNames, params):
        feed_format = params['feedformat']
        columnNames = feed_format['fileColumn'].tolist()
        dateColumns = feed_format[feed_format['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)
        feed_format['dataType'] = feed_format['dataType'].str.replace('np.datetime64', 'str')

        data_types = feed_format[feed_format['dataType'] != ''].set_index(feed_format['fileColumn'])[
            'dataType'].to_dict()
        data_types = {col: eval(dtype) for col, dtype in data_types.iteritems()}

        # while loading carry forward records, check if any derived columns defined format accordingly
        data_types.update(params.get('derivedCols', {}))

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:

                currentFrame = pd.read_csv(fileName, delimiter=str(params.get("delimiter", ",")),
                                           skipfooter=int(params.get("skipfooter", 0)),
                                           skiprows=int(params.get("skiprows", 0)),
                                           usecols=list(feed_format.get('position')) or None,
                                           header=None, names=columnNames, converters=data_types, engine='python')

                currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = pd.concat([df, currentFrame], join_axes=[currentFrame.columns])
            except Exception, e:
                logger.info(fileName)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["datePattern"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['fileColumn']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['fileColumn']] = df.apply(
                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                                dateCols['fileColumn']], axis=1)
                    del df['tdlen']
                df[dateCols['fileColumn']] = pd.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                            errors='raise')  # , errors='coerce'


        logger.info('Total Records In CSV File : %s' % str(len(df)))
        return df
