from traceback import format_exc

import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class HTMLLoader(FeedLoader):
    def __init__(self):
        pass

    def getType(self):
        return "CSV"

    def loadFile(self, fileNames, params):
        feed_format = params['feedformat']
        columnNames = feed_format['fileColumn'].tolist()
        dateColumns = feed_format[feed_format['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)
        feed_format['dataType'] = feed_format['dataType'].str.replace('np.datetime64', 'str')

        data_types = feed_format[feed_format['dataType'] != ''].set_index(feed_format['fileColumn'])[
            'dataType'].to_dict()
        data_types = {col: eval(dtype) for col, dtype in data_types.iteritems()}

        # while loading carry forward records, check if any derived columns defined format accordingly
        data_types.update(params.get('derivedCols', {}))

        df = pd.DataFrame(columns=columnNames)
        for fileName in fileNames:
            try:
                df_list = pd.read_html(fileName, skiprows=params.get('skiprows', 0))
                currentFrame = pd.DataFrame(df_list[params.get('parseIndex', 0)])
                if columnNames:
                    currentFrame.columns = columnNames
                currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df = df.append(currentFrame, ignore_index=True)

            except Exception, e:
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in dateColumns:
                dtpattern = dateCols["dtpatterns"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['column']] = df[dateCols['column']].astype(str)
                    df['tdlen'] = pd.Series(df[dateCols['column']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['column']] = df.apply(
                            lambda x: '0' + x[dateCols['column']] if x['tdlen'] == 7 else x[dateCols['column']], axis=1)
                    del df['tdlen']
                df[dateCols['column']] = pd.to_datetime(df[dateCols['column']], format=dtpattern,
                                                        errors='coerce')  # , errors='coerce'

        # discard non-mandatory columns
        # if requiredColumns:
        #     df = df[requiredColumns]

        logger.info('Total Records In HTML File : %s' % str(len(df)))
        return df
