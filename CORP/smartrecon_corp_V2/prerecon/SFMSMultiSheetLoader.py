import sys
import traceback
from traceback import format_exc

import numpy as np
import pandas
import pandas as pd
import xlrd

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class SFMSMultiSheetLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedformatFile": "", "columnNames": [], "skiprows": '', "skipfooter": '', "FooterKey": "",
                       "sheetIndex": []}
        self.feedformatfilecolumns = ["Description", "DataType", "DatePattern"]

    def getType(self):
        return "SFMSMultiSheetLoader"

    def loadFile(self, fileNames, params):
        feedformat = params['feedformat']
        columnNames = feedformat['fileColumn'].tolist()
        dateColumns = feedformat[feedformat['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)
        feedformat['dataType'] = feedformat['dataType'].str.replace('np.datetime64', 'str')

        data_types = feedformat[feedformat['dataType'] != ''].set_index(feedformat['fileColumn'])['dataType'].to_dict()
        data_types = {col: eval(dtype) for col, dtype in data_types.iteritems()}

        # while loading carry forward records, check if any derived columns defined format accordingly
        # data_types.update(params.get('derivedCols', {}))
        df = pd.DataFrame(columns=columnNames)

        for fileName in fileNames:
            try:
                xls = xlrd.open_workbook(fileName, on_demand=True)
                sheets = xls.sheet_names()
                colNames = None
                # if column names not defined in params, consider from ref file
                colNames = params.get("columnNames", ','.join(columnNames))
                if colNames:
                    checklen = True
                    result = pandas.DataFrame(columns=colNames.split(","))
                else:
                    result = pandas.DataFrame(columns=columnNames)
                    colNames = ','.join(columnNames)
                for sheetNo in params.get('sheetIndex', range(0, len(sheets))):
                    sheetName = sheets[sheetNo]
                    sheet = xls.sheet_by_index(sheetNo)
                    c = -1
                    footerRow = 0
                    for rownum in xrange(sheet.nrows):
                        # avoid empty records loading
                        rowvalues = [value for value in sheet.row_values(rownum) if value]
                        matchCount = 0

                        for col in colNames.split(','):
                            if col.strip() in rowvalues:
                                matchCount += 1

                        if rowvalues and matchCount >= 0.75 * len(rowvalues):
                            # check sheetvalues is subset of reference columnvalues
                            if checklen and len(rowvalues) != len(colNames.split(",")):
                                continue
                            c = rownum
                            break

                    footerKey = params.get('FooterKey', '')
                    if footerKey:
                        for rownum in xrange(sheet.nrows):
                            if footerKey in ','.join([str(i) for i in sheet.row_values(rownum)]):
                                footerRow = rownum

                    if c == -1:
                        continue
                    skip_footer = rownum - footerRow + 2
                    if footerRow == 0:
                        skip_footer = 0

                    currentFrame = pandas.read_excel(fileName, sheetname=sheetNo,
                                                     skiprows=c + 1 + int(params.get('skiprows', 0)),
                                                     skip_footer=skip_footer + int(params.get('skipfooter', 0)),

                                                     usecols=list(feedformat.get('position')) or None, header=None)

                    currentFrame = currentFrame.loc[:, range(0, len(columnNames))]
                    currentFrame.columns = columnNames

                    for index, data in data_types.iteritems():
                        currentFrame[index] = currentFrame[index].astype(data)

                    currentFrame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                    df = pandas.concat([df, currentFrame], ignore_index=True)
                    df = df.replace(["\\n", "\\r"], " ", regex=True)
                df.dropna(how='all', subset=columnNames, inplace=True)

            except Exception as e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

        for dateCols in dateColumns:
            dtpattern = dateCols["datePattern"]
            if dtpattern == '%d%m%Y':
                df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                df['tdlen'] = pd.Series(df[dateCols['fileColumn']]).str.len()
                if list(df['tdlen'].unique()) != [8]:
                    df[dateCols['fileColumn']] = df.apply(
                        lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[
                            dateCols['fileColumn']], axis=1)
                del df['tdlen']
            df[dateCols['fileColumn']] = pd.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        logger.info('Total Records In Excel File : %s' % str(len(df)))
        return df
