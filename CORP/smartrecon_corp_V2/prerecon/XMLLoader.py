from traceback import format_exc
import json
import numpy as np
import pandas
import xmltodict
import logger
from Utilities import Utilities
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class XMLLoader(FeedLoader):
    def getType(self):
        return "Excel"

    def loadFile(self, fileNames, params):
        feed_format = params['feedformat']
        date_columns = feed_format[feed_format['dataType'] == 'np.datetime64'][['fileColumn', 'datePattern']].to_dict(
            orient='records')

        # force date-time columns as string (numeric date will inferred as floats if there are blanks in read_csv)
        feed_format['dataType'] = feed_format['dataType'].str.replace('np.datetime64', 'str')

        data_types = feed_format[feed_format['dataType'] != '']['dataType']
        # data_types = {idx: eval(d_type) for idx, d_type in enumerate(data_types)}
        data_types = {col: eval(dtype) for col, dtype in data_types.iteritems()}

        column_names = feed_format['fileColumn'].tolist()

        df = pandas.DataFrame(columns=column_names)
        for fileName in fileNames:
            try:
                with open(fileName) as fd:
                    doc = xmltodict.parse(fd.read(),xml_attribs=True)
                    data=json.loads(json.dumps(doc))
                    current_frame = pandas.DataFrame(Utilities().getValues(data,params['query']),dtype=str)
                    current_frame.columns = column_names
                    for index, data in data_types.iteritems():
                        current_frame[column_names[index]] = current_frame[column_names[index]].astype(data)

                        current_frame['FEED_FILE_NAME'] = fileName.split('/')[-1]
                    df = pandas.concat([df, current_frame], ignore_index=True)
                    df = df.replace(["\\n", "\\r"], " ", regex=True)
                df.dropna(how='all', subset=column_names, inplace=True)



            except Exception, e:
                logger.info(fileName)
                logger.info(format_exc())
                logger.info("Unable to load the file " + fileName + ".\n"
                                                                    "Please check that file is readable and is "
                                                                    "as per the configured structure.\n"
                                                                    "If problem persists contact Administrator.")
                raise ValueError("Unable to load the file " + fileName)

        if not df.empty:
            # replace non-ascii error characters, if excel contains non-ascii characters.
            df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)

            for dateCols in date_columns:
                dtpattern = dateCols["datePattern"]
                if dtpattern == '%d%m%Y':
                    df[dateCols['fileColumn']] = df[dateCols['fileColumn']].astype(str)
                    df['tdlen'] = pandas.Series(df[dateCols['fileColumn']]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[dateCols['fileColumn']] = df.apply(
                            lambda x: '0' + x[dateCols['fileColumn']] if x['tdlen'] == 7 else x[dateCols['fileColumn']], axis=1)
                    del df['tdlen']
                df[dateCols['fileColumn']] = pandas.to_datetime(df[dateCols['fileColumn']], format=dtpattern,
                                                        errors='raise')  # , errors='coerce'

        logger.info('Total Records In Excel File : %s' % str(len(df)))
        return df
