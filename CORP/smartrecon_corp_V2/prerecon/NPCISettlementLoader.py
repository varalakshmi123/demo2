import numpy as np
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient

import config
import logger
from Utilities import Utilities as utils

logger = logger.Logger.getInstance("smartrecon").getLogger()


class NPCISettlementLoader():
    def __init__(self):
        self.params = {'respCode':'','statementDate':''}
        self.feedformatfilecolumns = []

    def getType(self):
        return 'NPCISettlement'

    def run(self, reportData=None, params={}):
        cycles = ['1C', '2C', '3C', '4C']
        reportData[params['respCode']] = reportData[params['respCode']].astype(str).str.ljust(2, '0')

        mongo_db = MongoClient(config.mongoHost)[config.databaseName]
        report_cycles_data = mongo_db['report_extracts'].find_one(
            {"statement_date": params['statementDate'].strftime('%d/%m/%y'), 'report_type': 'daily_settlement_report'})

        totalFrames = pd.DataFrame()

        if report_cycles_data is None:
            report_cycles_data = {'statement_date': params['statementDate'].strftime('%d/%m/%y'),
                                  'report_type': 'daily_settlement_report', 'created': utils().getUtcTime(),
                                  'updated': utils().getUtcTime(),
                                  'machineId': "16af9057-69af-4c69-bbcf-70d99334e027",
                                  'partnerId': '54619c820b1c8b1ff0166dfc',
                                  'apiKey': 'Mjg0YTUwYjg0MjUwNGM5MDJkNzBmZGRmZjM5YTI5ZTE1OTcxMWM4ZWM4NzdiM2Y4MTE1MjdhNWM1ZmQ1MmQzMg=='}

        for cycle in cycles:
            cycle_status = dict()
            cycle_status['cycle_type'] = cycle
            extract_data = reportData[reportData['FEED_FILE_NAME'].str.contains(cycle)]

            # add static details
            extract_data['INTERCHANGE_FEE'] = 0.0
            extract_data['NPCI_SWITCHING_FEE'] = 0.0
            extract_data[params['actualTxnAmt']] = extract_data[params['actualTxnAmt']].astype(np.float64)
            extract_data[params['txnAmt']] = extract_data[params['txnAmt']].astype(np.float64)

            extract_data[params['actualTxnAmt']] = extract_data[params['actualTxnAmt']] / 100.0
            # for P2A-08 user below column not default amount
            extract_data[params['txnAmt']] = extract_data[params['txnAmt']] / 100.0

            cycle_status['cycle_type'] = cycle
            approved_txn = extract_data[extract_data[params['respCode']] == '00']
            additional_approved_txn = extract_data[extract_data[params['respCode']] == '08']
            declined_txn = extract_data[extract_data[params['respCode']] != '00']

            if len(approved_txn) > 0:

                for fee, cap in config.interchange_fee.iteritems():
                    approved_txn.loc[
                        ((approved_txn[params['actualTxnAmt']] > cap['start'])
                         & (approved_txn[params['actualTxnAmt']] <= cap['end'])), 'INTERCHANGE_FEE'] = fee

                    additional_approved_txn.loc[
                        ((additional_approved_txn[params['txnAmt']] > cap['start'])
                         & (additional_approved_txn[params['txnAmt']] <= cap['end'])), 'INTERCHANGE_FEE'] = fee

                # NPCI Switching fee calculator
                for fee, cap in config.npci_switching_fee.iteritems():
                    approved_txn.loc[
                        ((approved_txn[params['actualTxnAmt']] > cap['start'])
                         & (approved_txn[params['actualTxnAmt']] <= cap['end'])), 'NPCI_SWITCHING_FEE'] = fee

                    additional_approved_txn.loc[
                        ((additional_approved_txn[params['txnAmt']] > cap['start'])
                         & (additional_approved_txn[params['txnAmt']] <= cap['end'])), 'NPCI_SWITCHING_FEE'] = fee

                cycle_status['approved_txn_count'] = len(approved_txn)
                cycle_status['additional_approved_txn_count'] = len(additional_approved_txn)
                cycle_status['declined_txn_count'] = len(declined_txn)

                cycle_status['approved_fee'] = self.custom_round(approved_txn['INTERCHANGE_FEE'].sum(), 2)
                cycle_status['approved_fee_gst'] = self.custom_round(cycle_status['approved_fee'] * config.gst_rate,
                                                                     2)
                cycle_status['approved_txn_amount'] = self.custom_round(approved_txn[params['actualTxnAmt']].sum())

                # P2A-08 transaction details
                cycle_status['additional_approved_fee'] = self.custom_round(
                    additional_approved_txn['INTERCHANGE_FEE'].sum(), 2)
                cycle_status['additional_approved_fee_gst'] = self.custom_round(
                    cycle_status['additional_approved_fee'] * config.gst_rate, 2)
                cycle_status['additional_approved_txn_amount'] = self.custom_round(
                    additional_approved_txn[params['txnAmt']].sum())

                sub_total = 0.0

                # NPCI excel report is HTML formatted excel
                feedPath = ''
                for file in params['settlementFiles']:
                    if cycle in file:
                        feedPath = file
                        break

                html_tables = pd.read_html(feedPath)
                if len(html_tables) != 4:
                    raise BaseException(
                        'NPCI Report %s is invalid, contact source system.' % feedPath.split('/')[-1])

                df = pd.DataFrame(html_tables[3])
                columns = df.iloc[0, :].tolist()
                df = df[1:]
                df.columns = columns
                df.dropna(how="all", inplace=True)

                # used by TTUM file generation
                df["cycle"] = cycle
                totalFrames = pd.concat([df, totalFrames], ignore_index=True)
                df.loc[:, 'Debit'] = df.loc[:, 'Debit'].fillna(0.0).astype(np.float64)
                df.loc[:, 'Credit'] = df.loc[:, 'Credit'].fillna(0.0).astype(np.float64)

                if params['reportType'] == 'remitter':
                    # cycle_status['npci_switching_fee'] = self.custom_round(
                    #     len(approved_txn) * config.npci_switching_fee, 2)
                    cycle_status['npci_switching_fee'] = self.custom_round(approved_txn['NPCI_SWITCHING_FEE'].sum(),
                                                                           2)
                    cycle_status['npci_switching_fee_gst'] = self.custom_round(
                        cycle_status['npci_switching_fee'] * config.gst_rate, 2)

                    # P2A-08 transaction details
                    # cycle_status['additional_npci_switching_fee'] = self.custom_round(
                    #     len(additional_approved_txn) * config.npci_switching_fee, 2)
                    cycle_status['additional_npci_switching_fee'] = self.custom_round(
                        additional_approved_txn['NPCI_SWITCHING_FEE'].sum(), 2)
                    cycle_status['additional_npci_switching_fee_gst'] = self.custom_round(
                        cycle_status['additional_npci_switching_fee'] * config.gst_rate, 2)

                    approved_txn_sum = cycle_status['approved_fee'] + cycle_status['approved_fee_gst'] + \
                                       cycle_status[
                                           'npci_switching_fee'] + cycle_status['npci_switching_fee_gst'] + \
                                       cycle_status[
                                           'approved_txn_amount']

                    additionl_txn_sum = cycle_status['additional_approved_fee'] + cycle_status[
                        'additional_approved_fee_gst'] + cycle_status['additional_npci_switching_fee'] + \
                                        cycle_status[
                                            'additional_npci_switching_fee_gst'] + cycle_status[
                                            'additional_approved_txn_amount']

                    sub_total = self.custom_round(approved_txn_sum + additionl_txn_sum, 2)
                    cycle_status['remitter_sub_total'] = sub_total

                    if df.loc[df['Description'] == 'Beneficiary/Remitter Sub Totals', 'Debit'].sum() == sub_total:
                        cycle_status['cycle_status'] = True
                    else:
                        cycle_status['cycle_status'] = False
                elif params['reportType'] == 'beneficiary':
                    approved_txn_sum = self.custom_round(
                        approved_txn[params['actualTxnAmt']].sum() + cycle_status['approved_fee_gst']
                        + cycle_status['approved_fee'], 2)

                    # P2A-08 Type transactions
                    additionl_txn_sum = self.custom_round(
                        additional_approved_txn[params['txnAmt']].sum() + cycle_status['additional_approved_fee_gst']
                        + cycle_status['additional_approved_fee'], 2)

                    cycle_status['beneficiary_sub_total'] = self.custom_round(approved_txn_sum + additionl_txn_sum,
                                                                              2)
                    if df.loc[df['Description'] == 'Beneficiary/Remitter Sub Totals', 'Credit'].sum() == \
                            cycle_status[
                                'beneficiary_sub_total']:
                        cycle_status['cycle_status'] = True
                    else:
                        cycle_status['cycle_status'] = False
                else:
                    raise BaseException(
                        'Invalid Report Type expecting remitter / beneficiary, but found %s' % params['reportType'])
            else:
                key = '%s_sub_total' % params['reportType']
                cycle_status[key] = 0.0
                cycle_status['cycle_status'] = True

            if cycle not in report_cycles_data:
                report_cycles_data[cycle] = {}
            report_cycles_data[cycle][params['reportType']] = cycle_status

        # update the data
        if '_id' in report_cycles_data:
            obj_id = report_cycles_data['_id']
            del report_cycles_data['_id']
            mongo_db['report_extracts'].update({'_id': ObjectId(obj_id)},
                                               {"$set": report_cycles_data}, multi=False)
        else:
            mongo_db['report_extracts'].insert(report_cycles_data)
            pass

        logger.info("NPCI Settlement Report Validation Completed.")

    # https://stackoverflow.com/questions/31818050/python-2-7-round-number-to-nearest-integer
    # function to round nearest integer
    # eg : round(0.075,2) --> 0.075, custom_round(0.075,2) --> 0.08, custom_round(0.074,2) --> 0.07
    def custom_round(self, value, decimal=2):
        return round(value + 10 ** (-2 * 5), decimal)
