from traceback import format_exc

import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader, FeedLoaderFactory
import subprocess

logger = logger.Logger.getInstance("smartrecon").getLogger()

class MasterCardIPMLoader(FeedLoader):
    def __init__(self):
        self.params={}
        self.flFactory = FeedLoaderFactory()
        self.csvloader=self.flFactory.getInstance("CSV")

    def loadFile(self, fileNames, params):
        csvfiles=[]
        for fileName in fileNames:
            output = subprocess.check_output(['mideu', 'extract',"-s","ascii",fileName])
            csvfiles.append(fileName+".csv")
        return self.csvloader.loadFile(csvfiles,params)

