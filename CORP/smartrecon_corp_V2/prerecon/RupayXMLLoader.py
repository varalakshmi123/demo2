import json

import pandas as pd
import xmltodict

from prerecon.FeedLoader import FeedLoader


class RupayXMLLoader(FeedLoader):
    def __init__(self):
        self.params = {}
        self.feedformatfilecolumns = []


    def getType(self):
        return "RupayXML"


    def convert(self,xml_file, xml_attribs=True):
        with open(xml_file) as f:
            d = xmltodict.parse(f, xml_attribs=xml_attribs)
            return d

    def loadFile(self, fileNames, params):
        count=0
        for fileName in fileNames:
            d=self.convert(fileName)
            print fileName
            transactions=json.loads(json.dumps(d["File"]["TxnBlock"]["Txn"]))
            if type(transactions) is dict:
                transactions=[transactions]

            for t in transactions:
                if type(t) is dict:
                    if "Fee" in t and type(t["Fee"]) is dict:
                        for f in t["Fee"].keys():
                            t["Fee."+f]=t["Fee"][f]
                        del t["Fee"]
                else:
                    "invalid type" ,t
            convert={}
            for key in transactions[0].keys():
                convert[key]=str


            if count==0:
                df=pd.read_json(json.dumps(transactions),orient='records',dtype=False)
                df['FEED_FILE_NAME'] = fileName.split('/')[-1]
            else:
                current=pd.read_json(json.dumps(transactions),orient='records',dtype=False)
                current['FEED_FILE_NAME'] = fileName.split('/')[-1]
                df=df.append(current)

            count+=1
        return df