import os
import numpy as np
import pandas as pd

import config
import logger
from prerecon.FeedLoader import FeedLoader

logger = logger.Logger.getInstance("smartrecon").getLogger()


class CarryForwardLoader(FeedLoader):
    def __init__(self):
        self.params = {"feedParams": {'feedformatFile': ""}}

    def getType(self):
        return "CarryForward"

    def loadFile(self, fileNames=[], params={}):
        payload = params['payload']
        properties = params['properties']

        cf_data_path = "{mft}/{reconName}/{stmtDate}/OUTPUT/{source}_UnMatched.csv".format(mft=config.mftpath,
                                                                                           stmtDate=payload[
                                                                                               'prevStmtDate'],
                                                                                           reconName=payload[
                                                                                               'reconName'],
                                                                                           source=properties[
                                                                                               'sourceName'])

        if not os.path.exists(cf_data_path):
            logger.info('Carry forward data path "%s" does not exists' % cf_data_path)
            return pd.DataFrame()

        logger.info('-----------------------------------------------')
        logger.info("Loading carry forward from file : %s" % cf_data_path)

        date_cols = [col for col, dtype in properties['dtypes'].iteritems() if dtype == np.datetime64]
        properties['dtypes'] = {k: eval('str') if v == np.datetime64 else v for k, v in
                                properties['dtypes'].iteritems()}
        cf_data = pd.read_csv(cf_data_path, header=0, converters=properties['dtypes'])

        # for col in cf_data.columns:
        #     if col in properties['dtypes']:
        #         cf_data[col] = cf_data[col].astype(properties['dtypes'][col])

        # Remove Status Columns, to be updated by NWay Matcher & Self Matcher
        cf_data.drop(columns=[col for col in cf_data if ' Match' in col], errors='ignore', inplace=True)
        for col in date_cols:
            cf_data[col] = pd.to_datetime(cf_data[col], format="%d-%m-%Y %H:%M:%S",
                                                    errors='raise')

        logger.info('Total Records In CarryForward File : %s' % str(len(cf_data)))
        logger.info('-----------------------------------------------')
        return cf_data
