# import selenium
# from selenium import webdriver
# from selenium.webdriver.support.ui import Select
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.by import By
# from time import sleep
# import datetime
# import os
# import zipfile
# import config
#
# class DownloadReport():
#     def __init__(self):
#         pass
#
#     def pvrReconReportDownload(self,reconName,statementDate):
#         try:
#             stmtDate = datetime.datetime.strptime(statementDate, '%d-%b-%Y')
#             startDate = stmtDate-datetime.timedelta(days=28)
#             startDate = datetime.datetime.strftime(startDate,'%d-%B-%Y')
#             startDate = startDate.split('-')
#             endDate= datetime.datetime.strftime(stmtDate,'%d-%B-%Y')
#             endDate = endDate.split('-')
#             endDate = {'month':endDate[1],'day':endDate[0].lstrip('0'),'year':endDate[2]}
#             fromDate= {'month':startDate [1],'day':startDate[0].lstrip('0'),'year':startDate [2]}
#             # connect To Browser and open Url
#             # options = webdriver.ChromeOptions()
#             # options.add_argument("download.default_directory=/home/varun/Downloads/pvr_auto/")
#             #
#             # driver = webdriver.Chrome(chrome_options=options)
#
#             path = '/usr/share/nginx/smartrecon_V2/autoFileDownload' + '/' + reconName
#             if not os.path.exists(path):
#                 os.mkdir(path)
#             chrome_options = webdriver.ChromeOptions()
#             prefs = {'download.default_directory': path}
#             chrome_options.add_experimental_option('prefs', prefs)
#             driver = webdriver.Chrome(chrome_options=chrome_options)
#             # driver = webdriver.Chrome()
#             driver.get('https://bo2.bookmyshow.com/default.aspx ')
#             driver.maximize_window()
#             sleep(1)
#
#             # Finding Login Page userName Element Using Id of Element
#             elem = driver.find_element_by_id("txtUserId")
#             elem.send_keys('pvr.panindia')
#
#             # Finding Login Page password Element Using Id of Element
#             elem = driver.find_element_by_id("txtPassword")
#             elem.send_keys('bms@456')
#
#             # click on login Button
#             login_box = driver.find_element_by_id('cmdLogin')
#             login_box.click()
#             driver.implicitly_wait(10)
#
#             # selecting Cinema Payment Reports Tab based on link Text:Cinema Payment Reports and click
#             driver.find_elements_by_partial_link_text('Cinema Payment Reports')[0].click()
#
#             # selecting Cinema Payment Details Tab based on link Text:Cinema Payment Details and click
#             driver.find_elements_by_partial_link_text('Cinema Payment Details')[0].click()
#
#             # Switch To Frame which consists Date to Select
#             driver.switch_to_frame('frmContent')
#
#             # Selecting Creditor
#             driver.find_element_by_id('s2id_cboCreditor').click()
#             driver.find_element_by_xpath('//*[@id="select2-results-4"]/li[2]').click()
#
#             # selecting From Date
#             driver.find_element_by_xpath('//*[@id="frmCinemaPaymentDetails"]/div[4]/table/tbody/tr[5]/td[2]/a').click()
#             driver.switch_to_window(driver.window_handles[1])
#             Select(driver.find_element_by_name('MonthSelector')).select_by_visible_text(fromDate['month'])
#             driver.find_element_by_link_text(fromDate['day']).click()
#             driver.switch_to_window(driver.window_handles[0])
#             driver.switch_to_frame('frmContent')
#
#             # selecting End Date
#             driver.find_element_by_xpath('//*[@id="frmCinemaPaymentDetails"]/div[4]/table/tbody/tr[5]/td[4]/a').click()
#             driver.switch_to_window(driver.window_handles[1])
#             Select(driver.find_element_by_name('MonthSelector')).select_by_visible_text(endDate['month'])
#             driver.find_element_by_link_text(endDate['day']).click()
#             driver.switch_to_window(driver.window_handles[0])
#             driver.switch_to_frame('frmContent')
#
#             # click on button Show Reports
#             driver.find_element_by_id('btnSummReport').click()
#
#             # wait Utill summary Report Loads
#             # element = WebDriverWait(driver, 60).until(
#             #     EC.presence_of_element_located((By.ID, "imgProgress"))
#             # )
#
#             # rowCount = len(driver.find_elements(By.XPATH,value=('//*[@id="GVCinemaPay"]/tbody/tr')))-2 # Row Count in Report Summary
#
#             # Download All Cinema Payment Details
#             # for x in range(0,rowCount):
#             #     elem = driver.find_element_by_id('GVCinemaPay_btnShowDetails_{count}'.format(count=x))
#             #     elem.click()
#             #     sleep(10)
#             #     driver.switch_to_window(driver.window_handles[1])
#             #     driver.find_element_by_id('chkNonTele').click()
#             #     driver.find_element_by_id('btnExportToExcel').click()
#             #     driver.close()
#             #     driver.switch_to_window(driver.window_handles[0])
#             #     driver.switch_to_frame('frmContent')
#             driver.find_element_by_id('btnExpExcel').click()
#
#             # Selecting Payment Type:
#             driver.find_element_by_id('s2id_cboCriteria').click()
#             driver.find_element_by_xpath('//*[@id="select2-results-1"]/li[2]').click()
#             driver.find_element_by_id('btnExpExcel').click()
#             # wait Utill summary Report Loads
#             sleep(5)
#             stmtDate = datetime.datetime.strftime(stmtDate,'%d%m%Y')
#             status, name = self.createZipFile(reconName, stmtDate)
#             return name
#         except Exception as e:
#             print e
#             return False,'Internal Error, Contact Admin'
#
#     def createZipFile(self, reconName, statementDate):
#         mftPath = config.mftpath
#         # mftPath = '/opt/reconDB/mft/'
#         path = mftPath + '/' + reconName
#         if not os.path.exists(path):
#             os.mkdir(path)
#         downloadedPath = config.downloadPath+'/'+reconName
#         # downloadedPath = '/usr/share/nginx/smartrecon_V2/autoFileDownload' + '/' + reconName
#         if os.path.exists(path + os.sep + statementDate + '.zip'):
#             os.remove(path + os.sep + statementDate + '.zip')
#         zipFile = zipfile.ZipFile(path + os.sep + statementDate + '.zip', 'w',
#                                   zipfile.ZIP_DEFLATED)
#         files = os.listdir(downloadedPath)
#         for file in files:
#             zipFile.write(downloadedPath + os.sep + file,
#                           os.path.basename(downloadedPath + os.sep + file))
#         for file in files:
#             os.remove(downloadedPath + '/'+file)
#         return True, statementDate + '.zip'
#
