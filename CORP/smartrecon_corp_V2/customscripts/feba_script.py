import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import xlrd
import csv

# command = 'python', scriptPath, feedPath + os.sep + file, feedPath + os.sep + loadFile
feedFile = sys.argv[1]
outputPath = sys.argv[2]

wb = xlrd.open_workbook(feedFile)
sh = wb.sheet_by_name('Sheet1')
your_csv_file = open(outputPath, 'w')
wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

for rownum in range(sh.nrows):
    wr.writerow(sh.row_values(rownum))
your_csv_file.close()

