import logging
import logging.handlers
import os

import config


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class createGeventLogger(object):
    __metaclass__ = Singleton

    def __init__(self, flaskLogFile=None):
        if not os.path.exists(config.logBaseDir):
            os.makedirs(config.logBaseDir)
        if flaskLogFile:
            logFilePath = os.path.join(config.logBaseDir, flaskLogFile + ".log")
            # Set up a specific logger with our desired output level
            self.my_logger = logging.getLogger(flaskLogFile)
        else:
            logFilePath = os.path.join(config.logBaseDir, config.flaskLogFile + ".log")
            # Set up a specific logger with our desired output level
            self.my_logger = logging.getLogger(config.flaskLogFile)
        self.my_logger.setLevel(logging.DEBUG)

        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(logFilePath, maxBytes=config.flaskMaxBytes,
                                                       backupCount=config.flaskBackupCount)
        formatter = logging.Formatter('%(message)s')
        handler.setFormatter(formatter)
        self.my_logger.addHandler(handler)

    def write(self, msg):
        self.my_logger.info(msg)


class Logger:
    _instance = None

    @classmethod
    def getInstance(cls, logFile=None):
        if not Logger._instance:
            if logFile:
                Logger._instance = Logger(logFile)
            else:
                raise Exception("Require log file name.")
        return Logger._instance

    def __init__(self, logFile):
        if not os.path.exists(config.logBaseDir):
            os.makedirs(config.logBaseDir)

        logFilePath = os.path.join(config.logBaseDir, logFile + ".log")

        # Set up a specific logger with our desired output level
        self.my_logger = logging.getLogger('af')
        self.my_logger.setLevel(logging.DEBUG)

        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(logFilePath, maxBytes=50000000, backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(module)s %(funcName)s %(lineno)s %(message)s')
        handler.setFormatter(formatter)
        self.my_logger.addHandler(handler)

    def getLogger(self):
        return self.my_logger
