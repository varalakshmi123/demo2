import pandas  as pd
# df = pd.read_csv('/home/varun/Downloads/PVR/Bank Statement/pvrpan/Apr18.csv',skiprows = 15)
# # print df['Amount'].head()
# # print df.dtypes
# dfpan = pd.read_excel('/home/varun/Downloads/PVR/Bank Statement/pvrpan/pvrpan_april.xlsx')
# # print dfpan['Net_Amt'].head()
# dfpandt = pd.read_excel('/home/varun/Downloads/PVR/Bank Statement/pvrdt/pvrdt_april.xlsx')
# # print dfpandt['Net_Amt'].head()
# dfleft = pd.read_csv('/home/varun/Downloads/PVR/leftwork.csv')
# import numpy as np
# dfleft['Net_Amt'] = dfleft['Net_Amt'].astype(np.int64)
# # print dfleft.head()
# dfright = pd.read_csv('/home/varun/Downloads/PVR/rightwork.csv')
# dfright['Amount'] = dfright['Amount'].astype(np.int64)
# print dfright.head()
# df = pd.read_csv('/home/varunrow = cursor.fetchmany(10)/Downloads/PVR/Bank Statement/pvr_1/Nov\'17/November-2017.csv')
# # df = df.drop_duplicates(subset = ['Net_Amt','req_Date','UTR_Number'])
# df.to_csv('/home/varun/Desktop/test.csv')
# print df.head()
import sys
import os
import pandas as pd
import subprocess

import paramiko
import sys
# import chilkat
# ssh = paramiko.SSHClient()
# ssh.connect('', username='recon', password='recon')
# from paramiko import SSHClient
# ssh = SSHClient()
# ssh.load_system_host_keys()
# ssh.connect('recon@123.201.255.101')
# ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('ssh recon@123.201.255.101')
# print(ssh_stdout) #print the output of ls command
# success = ssh.Connect("the-ssh-server.com",port)
class GenerateReconMetaInfo(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "GenerateReconMetaInfo"

        self.properties = dict()

    def run(self, payload, debug):
        reconDict = {"reconName": payload['reconName'], "sourcePath": payload['reconType'],
                     "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
                     "reconProcess": payload['reconType'], "reconId": payload['reconType'] + ' RECON',
                     "partnerId": "54619c820b1c8b1ff0166dfc", "reportDetails": []}

        for key, data in payload.get('results', {}).iteritems():
            reconDict['reportDetails'].extend([{'reportName': key, 'reportPath': '%s.csv' % key, "source": key},
                                               {'reportName': key + ' Un-matched',
                                                'reportPath': "%s_UnMatched.csv" % key, "source": key}])

            if 'Self Matched' in payload['results'][key]:
                reconDict['reportDetails'].append(
                    {'reportName': key + ' Self Match', 'reportPath': '%s_Self_Matched.csv' % key, "source": key})

        if 'duplicateStore' in payload.keys():
            for key, data in payload['duplicateStore'].iteritems():
                reconDict['reportDetails'].extend(
                    [{'reportName': key + ' Duplicated', 'reportPath': '%s_Duplicated.csv' % key, "source": key}])

        if 'custom_reports' in payload.keys():
            for key, data in payload['custom_reports'].iteritems():
                if len(data)>1000000:
                    data_split = np.array_split(data, 2)
                    for i in [0, 1]:
                        reconDict['reportDetails'].extend([{'reportName': key + ' Report', 'reportPath': '%s_Report.csv' % key, "source": key}])
                    # data_split[i].to_csv((outputPath + '{report}_Report_' + str(i) + '.csv').format(report=key)
                    # data_split = np.array_split(data, 2)
                    # for i in [0, 1]:
                    #     data_split[i].to_csv((outputPath + '{report}_Report_' + str(i) + '.csv').format(report=key),
                    #                          mode=mode, header=header, index=False,
                    #                          date_format='%d-%m-%Y %H:%M:%S', float_format=float_format,
                    #                          encoding='utf-8')


                else:
                    reconDict['reportDetails'].extend(
                        [{'reportName': key + ' Report', 'reportPath': '%s_Report.csv' % key, "source": key}])

        if 'reconSummary' in payload.keys():
            reconDict['reportDetails'].append(
                {'reportName': "Recon Summary", 'reportPath': 'recon_summary.csv', "source": 'Recon Summary'})
	print '===' * 50
        print 'Recon Defination'
        print reconDict
        print 'Recon License Defination'
        ''''print {"sourcePath": payload['reconName'], "reconId": payload['reconType'] + ' RECON',
               "reconName": payload['reconName'], "licenseCount": 1, "licensed": "Licensed",
               "reconProcess": payload['reconType'], "updated": datetime.datetime.now().strftime('%s'),
	               "partnerId": "54619c820b1c8b1ff0166dfc", "jobScript": main.__file__.split('smartrecon')[-1]}'''
	print '==='*50
# the above and  below are related to the old eqt smartrecon




class GenerateReconSummary(FlowElement):
    def __init__(self):
        FlowElement.__init__(self)
        self.type = "SummaryReport"

        self.properties = dict(
            sources=[dict(sourceTag="", aggrCol=[], matched='all/any')], groupbyCol=[])

    def run(self, payload, debug):
        try:
            summary_dict = dict(reconName=payload['reconName'], statementDate=payload['statementDate'],
                                reconType=payload['reconType'], reportName="ReconSummary", details=[])

            summary_list = []
            reorder_cols = ["Source"]
            reorder_cols.extend(self.properties.get('groupbyCol', []))
            summary_cols = collections.OrderedDict(
                [("Matched Amount", 0), ("Matched Count", 0.0), ("UnMatched Count", 0),
                 ("UnMatched Amount", 0.0), ("Reversal Amount", 0.0), ("Reversal Count", 0),
                 ("Carry-Forward Matched Amount", 0.0), ("Carry-Forward Matched Count", 0),
                 ("Carry-Forward UnMatched Amount", 0.0), ("Carry-Forward UnMatched Count", 0)])

            for source in self.properties['sources']:
                data = Utilities().getValues(payload, 'results.' + source['sourceTag']).copy()
                # check if data exists (incremental loads may generate empty dataframes)
                if data.empty:
                    pass
                else:
                    summary_list.extend(
                        self.compute_stats(source, data, groupbyCol=self.properties.get('groupbyCol', [])))

            if len(summary_list) == 0:
                return

            summary_df = pd.DataFrame(summary_list)

            # Swap column names from 'Amount#Matched'(pivoted result) to 'Matched Amount'
            rename_mapper = {}
            for col in summary_df.columns:
                if 'Amount#' in col or 'Count#' in col:
                    aggr, match_status = tuple(col.split('#'))
                    rename_mapper[col] = match_status + ' ' + aggr
                else:
                    rename_mapper[col] = col.rstrip('#')

            summary_df.rename(columns=rename_mapper, inplace=True)

            # add missing summary cols to dataframe to avoid column alignment issues
            for column, value in summary_cols.iteritems():
                if column in summary_df.columns:
                    summary_df[column] = summary_df[column].fillna(value=value)
                else:
                    summary_df[column] = value

            # merge closing balances on sources if exists
            if 'closingBalances' in payload.keys():
                closing_balances = pd.DataFrame(payload['closingBalances'])
                summary_df = summary_df.merge(closing_balances, on='Source', how='left')
            else:
                summary_df['Closing Balance'] = 0.0

            # Re-order source columns to first index
            source = summary_df[reorder_cols]
            summary_df.drop(labels=reorder_cols, axis=1, inplace=True)
            summary_df = summary_df[summary_cols.keys() + ['Closing Balance']]

            for idx, value in enumerate(reorder_cols):
                summary_df.insert(idx, value, source.loc[:, value])
            if 'STATEMENT_DATE' in self.properties.get('groupbyCol',[]):
                pass
            else:
                # payload['statementDate']=summary_df['STATEMENT_DATE']
                # summary_df.drop(labels=['STATEMENT_DATE'],axis=1,inplace=True)
                summary_df.insert(len(reorder_cols), 'Statement Date', payload['statementDate'])
            summary_df.insert(len(reorder_cols) + 1, 'Execution Date Time', datetime.datetime.now())
            summary_df.sort_values(by='Execution Date Time', inplace=True, ascending=False)
            summary_df.fillna('', inplace=True)

            payload['reconSummary'] = summary_df
            summary_dict['details']=summary_df.to_dict(orient='records')
            MongoClient(config.mongoHost)[config.databaseName]['custom_reports'].insert(summary_dict)

        except Exception, e:
            print traceback.format_exc()
            payload['error'] = dict(elementName="GenerateReconSummary", type="critical", exception=str(e),
                                    traceback=traceback.format_exc())

    def compute_stats(self, source={}, data=None, groupbyCol=[]):
        default_group = ['Source']
        default_group.extend(groupbyCol)

        data['matched_status'] = ''
        data['Count'] = 1
        data['Amount'] = data[source['aggrCol']].fillna(0.0).sum(axis=1)
        data.rename(columns={"SOURCE": "Source"}, inplace=True)

        # segregate matched and un-matched transactions
        match_cond = []
        un_match_cond = []
        for col in data.columns:
            if ' Match' in col and 'Self Matched' != col:
                match_cond.append("(data['%s'] == 'MATCHED')" % col)
                un_match_cond.append("(data['%s'] == 'UNMATCHED')" % col)
            else:
                pass

        # if matched == 'any', if either of the source is matched assume the records to be matched.
        if source.get('matched', '') == 'any':
            data.loc[eval(" | ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" & ".join(un_match_cond)), "matched_status"] = 'UnMatched'
        else:
            data.loc[eval(" & ".join(match_cond)), "matched_status"] = 'Matched'
            data.loc[eval(" | ".join(un_match_cond)), "matched_status"] = 'UnMatched'

        if 'Self Matched' in data.columns:
            data['Self Matched'] = data['Self Matched'].fillna('')
            data.loc[data['Self Matched'] == 'MATCHED', 'matched_status'] = 'Reversal'
        else:
            pass

        if 'CARRY_FORWARD' in data.columns:
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                data['matched_status'] == 'Matched'), 'matched_status'] = 'Carry-Forward Matched'
            data.loc[(data['CARRY_FORWARD'] == 'Y') & (
                data['matched_status'] == 'UnMatched'), 'matched_status'] = 'Carry-Forward UnMatched'
        else:
            pass

        pivot_df = pd.pivot_table(data, index=default_group, columns=['matched_status'], values=['Count', 'Amount'],
                                  aggfunc=np.sum).reset_index()
        pivot_df.columns = pivot_df.columns.to_series().str.join('#')
        summary_list = pivot_df.to_dict(orient='records')

        return summary_list

