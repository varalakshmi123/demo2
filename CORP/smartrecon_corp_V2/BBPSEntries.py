import pandas as pd
from bson import ObjectId
from pymongo import MongoClient
recons={
	"_id" : ObjectId("5b640dc81a1bc7467863aaad"),
	"updated" : 1544513174,
	"imagePath" : "/files/reconImg/download.png",
	"reconName" : "BBPS",
	"created" : 1533283784,
	"reconProcess" : "BBPS",
	"sources" : {
		"CBS" : {
			"CBS" : {
				"keyColumns" : [
					{
						"DataType" : "str",
						"keyCols" : "Transaction Ref"
					},
					{
						"DataType" : "float",
						"keyCols" : "Transaction Amount"
					},
					{
						"DataType" : "str",
						"keyCols" : "DR_CR"
					}
				],
				"feedId" : "1240498760549371783011122018125614"
			}
		},
		"SWITCH" : {
			"SWITCH" : {
				"keyColumns" : [
					{
						"DataType" : "str",
						"keyCols" : "Transaction Ref"
					},
					{
						"DataType" : "float",
						"keyCols" : "Transaction Amount"
					}
				],
				"feedId" : "1297570622271422894411122018125614"
			}
		},
		"NPCI" : {
			"NPCI" : {
				"keyColumns" : [
					{
						"DataType" : "str",
						"keyCols" : "Transaction Ref"
					},
					{
						"DataType" : "float",
						"keyCols" : "Transaction Amount"
					}
				],
				"feedId" : "1199170861018842069811122018125614"
			}
		}
	},
	"partnerId" : "54619c820b1c8b1ff0166dfc",
	"isLicensed" : True
}

sourcerefList=[{
	"_id": ObjectId("5b6414091a1bc7467863b07e"),
	"referenceSource": False,
	"updated": 1546835771,
	"position": 2,
	"loadType": "XML",
	"feedDetails": [{
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "agentId",
		"position": 1,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "billerCategory",
		"position": 2,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "billerFee",
		"position": 3,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "billerFeeCGST",
		"position": 4,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "billerFeeIGST",
		"position": 5,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "billerFeeSUTGST",
		"position": 6,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "billerId",
		"position": 7,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "casProcessed",
		"position": 8,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "clearingTimestamp",
		"position": 9,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "couCustConvFee",
		"position": 10,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerConvenienceFee",
		"position": 11,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerConvenienceFeeCGST",
		"position": 12,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerConvenienceFeeIGST",
		"position": 13,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerConvenienceFeeSUTGST",
		"position": 14,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerMobileNumber",
		"position": 15,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerOUCountMonth",
		"position": 16,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "customerOUId",
		"position": 17,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "customerOUSwitchingFee",
		"position": 18,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "customerOUSwitchingFeeCGST",
		"position": 19,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "customerOUSwitchingFeeIGST",
		"position": 20,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "customerOUSwitchingFeeSUTGST",
		"position": 21,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "decline",
		"position": 22,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "msgId",
		"position": 23,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "mti",
		"position": 24,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "paymentChannel",
		"position": 25,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "paymentMode",
		"position": 26,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "refId",
		"position": 27,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "responseCode",
		"position": 28,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "reversal",
		"position": 29,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "settlementCycleId",
		"position": 30,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "splitPay",
		"position": 31,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "txnAmount",
		"position": 32,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "txnCurrencyCode",
		"position": 33,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "txnDate",
		"position": 34,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "txnReferenceId",
		"position": 35,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "txnType",
		"position": 36,
		"datePattern": ""
	}],
	"created": 1533285385,
	"mappedColumns": [{
		"DataType": "str",
		"keyCols": "Transaction Ref",
		"mappedColumn": "Transaction Ref"
	}, {
		"DataType": "float",
		"keyCols": "Transaction Amount",
		"mappedColumn": "Amount"
	}],
	"query": "RAWFile.transactions",
	"source": "NPCI",
	"partnerId": "54619c820b1c8b1ff0166dfc",
	"filters": ["df['Transaction Ref']=''\n", "df=cfs.vlookUp('SWITCH',['refId'],['TXN_REF_NUM'],['Transaction Ref'])\n", "df['DR_CR']=df['txnAmount'].apply(lambda x:'DR' if x<0 else 'CR')\n", "df['Amount']=df['txnAmount']\n", "df['Amount']=df['Amount'].abs()/100\n"],
	"filePattern": ".*.xml",
	"summaryColMap": {},
	"derivedColumns": ["Transaction Ref", "DR_CR", "Amount"],
	"reconName": "BBPS",
	"struct": "NPCI",
	"feedId": "1199170861018842069811122018125614"
}, {
	"_id": ObjectId("5b6c242cc4debe190a7da12a"),
	"referenceSource": True,
	"updated": 1546835771,
	"position": 5,
	"loadType": "CSV",
	"feedDetails": [{
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo1",
		"position": 1
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo2",
		"position": 2
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo3",
		"position": 3
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo4",
		"position": 4
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo5",
		"position": 5
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo6",
		"position": 6
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo7",
		"position": 7
	}, {
		"dataType": "",
		"datePattern": "",
		"Required": "",
		"fileColumn": "pusedo8",
		"position": 8
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "No of Txns",
		"datePattern": "",
		"position": 9
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "Debit",
		"datePattern": "",
		"position": 10
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "Credit",
		"datePattern": "",
		"position": 11
	}],
	"created": 1533813804,
	"skiprows": 6,
	"source": "RRPT",
	"delimiter": ",",
	"partnerId": "54619c820b1c8b1ff0166dfc",
	"filters": ["df['Debit']=df['Debit'].str.replace(',','').astype(np.float64)\n"],
	"filePattern": "RRPT*.*",
	"summaryColMap": {},
	"derivedColumns": ["Debit"],
	"reconName": "BBPS",
	"struct": "RRPT_Structure"
}, {
	"_id": ObjectId("5b6414091a1bc7467863b07b"),
	"referenceSource": False,
	"updated": 1546835771,
	"position": 1,
	"loadType": "Excel",
	"feedDetails": [{
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TXN_REF_NUM",
		"position": 1,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "SRV_REF_NUM",
		"position": 2,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "paymentChannel",
		"position": 3,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "paymentMode",
		"position": 4,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "TRAN_AMOUNT",
		"position": 5,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRAN_DATE",
		"position": 6,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "BBPS_STATUS",
		"position": 7,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "is Reversal",
		"position": 8,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "REVERSAL_AMT",
		"position": 9,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "REVERSAL_STATUS",
		"position": 10,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "ACC_NO",
		"position": 11,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "Br_Code",
		"position": 12,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRAN_NARR",
		"position": 13,
		"datePattern": ""
	}],
	"created": 1533285385,
	"mappedColumns": [{
		"DataType": "str",
		"keyCols": "Transaction Ref",
		"mappedColumn": "SRV_REF_NUM"
	}, {
		"DataType": "float",
		"keyCols": "Transaction Amount",
		"mappedColumn": "TRAN_AMOUNT"
	}],
	"skiprows": 1,
	"source": "SWITCH",
	"partnerId": "54619c820b1c8b1ff0166dfc",
	"filters": [],
	"filePattern": "BBPS_RECON_FILE *.*",
	"summaryColMap": {},
	"derivedColumns": [],
	"reconName": "BBPS",
	"struct": "SWITCH",
	"feedId": "1297570622271422894411122018125614"
}, {
	"_id": ObjectId("5bdc5051c4debe348bae4a3c"),
	"referenceSource": True,
	"updated": 1546835771,
	"loadType": "CSV",
	"struct": "gst_structure",
	"created": 1541165137,
	"skiprows": 1,
	"source": "GST",
	"delimiter": ",",
	"summaryColMap": {},
	"partnerId": "54619c820b1c8b1ff0166dfc",
	"filters": ["df=df.iloc[df.index[(df['Operator']=='GST - Paid').eq(1)].values[0]:df.index[(df['Operator']=='Total GST - Paid').eq(1)].values[0]+1,:]\n", "df=df.loc[df.index[(df['Operator']=='Interchange Fee - Disputes (Refund)').eq(1)].values[0]:,:]\n", "df=df.loc[df.index[(df['Operator']=='Interchange Fee - Disputes (Refund)').eq(1)].values[0]:df.index[(df['Operator']=='Grand Total').eq(1)].values[0],:]\n", "df=df[df['Operator']=='Grand Total']\n", "df['Total Transaction Amount']=df['Total Transaction Amount'].str.replace(',','')\n", "df['Total Transaction Amount']=df['Total Transaction Amount'].astype(np.float64)\n", "df['Interchange Fee']=df['Interchange Fee'].astype(np.float64)\n", "df['CGST']=df['CGST'].astype(np.float64)\n", "df['SGST']=df['SGST'].astype(np.float64)\n", "df['IGST']=df['IGST'].astype(np.float64)\n"],
	"filePattern": "GST_*.*",
	"position": 3,
	"derivedColumns": ["Total Transaction Amount", "Interchange Fee", "CGST", "SGST", "IGST"],
	"reconName": "BBPS",
	"feedDetails": [{
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "Operator",
		"position": 1
	}, {
		"dataType": "str",
		"datePattern": "",
		"fileColumn": "psuedo1",
		"position": 2
	}, {
		"dataType": "str",
		"datePattern": "",
		"fileColumn": "psuedo2",
		"position": 3
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "Total Number of Transactions",
		"position": 4
	}, {
		"dataType": "str",
		"datePattern": "",
		"fileColumn": "GSTIN",
		"position": 5
	}, {
		"dataType": "str",
		"datePattern": "",
		"fileColumn": "State Code",
		"position": 6
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "Total Transaction Amount",
		"position": 7
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "Interchange Fee",
		"position": 8
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "CGST",
		"position": 9
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "SGST",
		"position": 10
	}, {
		"dataType": "str",
		"datePattern": "",
		"required": True,
		"fileColumn": "IGST",
		"position": 11
	}]
}, {
	"_id": ObjectId("5b67d81c7e4f7e7128a62558"),
	"referenceSource": False,
	"updated": 1546835771,
	"position": 4,
	"loadType": "CSV",
	"feedDetails": [{
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRAN RMKS ",
		"position": 1,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRANPARTICULAR",
		"position": 2,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRANPARTICULAR2",
		"position": 3,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "RRN",
		"position": 4,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRAN ID",
		"position": 6,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "TRAN DATE",
		"position": 7,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "DEBIT ACCT NUM",
		"position": 8,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "OFFICE ACCT NUM",
		"position": 9,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "np.float64",
		"Required": "y",
		"fileColumn": "AMOUNT",
		"position": 10,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "CHANNELID",
		"position": 11,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "PSTDTIME",
		"position": 12,
		"datePattern": ""
	}, {
		"required": True,
		"dataType": "str",
		"Required": "y",
		"fileColumn": "VFDTIME",
		"position": 13,
		"datePattern": ""
	}],
	"created": 1533285385,
	"mappedColumns": [{
		"DataType": "str",
		"keyCols": "Transaction Ref",
		"mappedColumn": "Transaction Ref"
	}, {
		"DataType": "float",
		"keyCols": "Transaction Amount",
		"mappedColumn": "AMOUNT"
	}, {
		"DataType": "str",
		"keyCols": "DR_CR",
		"mappedColumn": "DR_CR"
	}],
	"feedId": "1240498760549371783011122018125614",
	"skiprows": 1,
	"source": "CBS",
	"delimiter": "|",
	"partnerId": "54619c820b1c8b1ff0166dfc",
	"filters": ["df['Transaction Ref']=df['TRANPARTICULAR2'].str.extract('(\\_(.*?)\\/)')[1].replace('_|/','',regex=True)\n", "df=cfs.vlookUp('SWITCH',['Transaction Ref'],['Transaction Ref'],['TXN_REF_NUM'])\n", "df['refId']=df['TXN_REF_NUM']\n", "del df['TXN_REF_NUM']\t\n", "df['DR_CR']=df['TRANPARTICULAR'].apply(lambda x:'DR' if 'REV' in x else 'CR')\n", "payload['cbsdf']=df.copy()\n", "cfs.masterDumpHandler(properties={'masterType':\"BBPS\", 'operation':\"insert\",'source':'cbsdf','dropOnRollback':True,'insertCols':{'TRANPARTICULAR':'TRANPARTICULAR','Transaction Ref':'Transaction Ref','AMOUNT':'Amount','refId':'refId','DEBIT ACCT NUM':'DEBIT ACCT NUM','OFFICE ACCT NUM':'OFFICE ACCT NUM','DR_CR':'DR_CR'}})\n"],
	"filePattern": "MOB_BBPS_*.*",
	"summaryColMap": {},
	"derivedColumns": ["Transaction Ref", "refId", "DR_CR", "cbs", "cfs.masterDumpHandler(properties"],
	"reconName": "BBPS",
	"struct": "CBS"
}]

flows={
	"_id": ObjectId("5b645b6e1a1bc738a10af5b6"),
	"connections": [{
		"dest": {
			"nodeID": "298150026",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "677432489",
			"connectorIndex": 0
		}
	}, {
		"dest": {
			"nodeID": "287524614",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "298150026",
			"connectorIndex": 0
		}
	}, {
		"dest": {
			"nodeID": "173878437",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "287524614",
			"connectorIndex": 0
		}
	}, {
		"dest": {
			"nodeID": "710883172",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "173878437",
			"connectorIndex": 0
		}
	}, {
		"dest": {
			"nodeID": "451069462",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "710883172",
			"connectorIndex": 0
		}
	}, {
		"dest": {
			"nodeID": "734625388",
			"connectorIndex": 0
		},
		"source": {
			"nodeID": "451069462",
			"connectorIndex": 0
		}
	}],
	"reconProcess": "BBPS",
	"updated": 1544837544,
	"partnerId": None,
	"reconName": "BBPS",
	"created": 1533283862,
	"machineId": "bb305fe9-d451-4d59-86dc-26137a8a9ec1",
	"nodes": [{
		"outputConnectors": [{
			"name": "1"
		}],
		"color": "#cfbcff",
		"inputConnectors": [{
			"name": "X"
		}],
		"properties": {
			"reconProcess": "BBPS",
			"reconName": "BBPS"
		},
		"name": "Initializer",
		"id": "677432489",
		"className": "Initializer",
		"width": 110,
		"y": 57,
		"x": 59,
		"type": "Initializer"
	}, {
		"outputConnectors": [{
			"name": "1"
		}],
		"color": "#feeedf",
		"inputConnectors": [{
			"name": "X"
		}],
		"properties": {},
		"name": "PreLoader",
		"id": "298150026",
		"className": "PreLoader",
		"width": 110,
		"y": 137,
		"x": 243,
		"type": "PreLoader"
	}, {
		"outputConnectors": [{
			"name": "1"
		}],
		"color": "#bebbbb",
		"inputConnectors": [{
			"name": "X"
		}],
		"properties": {
			"resultkey": "results.CBS",
			"isSelfMatch": True,
			"sources": [{
				"sourceTag": "CBS",
				"source": "CBS",
				"columns": {
					"debitCreditColumn": "DR_CR",
					"matchColumns": ["Transaction Ref", "Transaction Amount"]
				}
			}]
		},
		"name": "NWayMatch",
		"id": "287524614",
		"className": "NWayMatch",
		"width": 110,
		"y": 251,
		"x": 396,
		"type": "NWayMatch"
	}, {
		"outputConnectors": [{
			"name": "1"
		}],
		"color": "#bebbbb",
		"inputConnectors": [{
			"name": "X"
		}],
		"properties": {
			"resultkey": "results.NPCI",
			"sources": [{
				"sourceTag": "CBS",
				"source": "results.CBS",
				"columns": {
					"matchColumns": ["Transaction Ref", "Transaction Amount"]
				}
			}, {
				"sourceTag": "SWITCH",
				"source": "SWITCH",
				"columns": {
					"matchColumns": ["Transaction Ref", "Transaction Amount"]
				}
			}, {
				"sourceTag": "NPCI",
				"source": "NPCI",
				"columns": {
					"matchColumns": ["Transaction Ref", "Transaction Amount"]
				}
			}]
		},
		"name": "NWayMatch",
		"id": "173878437",
		"className": "NWayMatch",
		"width": 110,
		"y": 327,
		"x": 608,
		"type": "NWayMatch"
	}, {
		"outputConnectors": [{
			"name": "1"
		}],
		"color": "#ddffdd",
		"inputConnectors": [{
			"name": "X"
		}],
		"properties": {},
		"name": "DataFiller",
		"id": "710883172",
		"className": "DataFiller",
		"width": 110,
		"y": 417,
		"x": 408,
		"type": "DataFiller"
	}, {
		"className": "AddPostMatchRemarks",
		"width": 110,
		"inputConnectors": [{
			"name": "X"
		}],
		"name": "AddPostMatchRemarks",
		"color": "#ffcccb",
		"x": 528,
		"outputConnectors": [{
			"name": "1"
		}],
		"type": "AddPostMatchRemarks",
		"id": "451069462",
		"y": 466
	}, {
		"className": "DumpData",
		"width": 110,
		"inputConnectors": [{
			"name": "X"
		}],
		"name": "DumpData\n",
		"color": "#cebcde",
		"x": 532,
		"outputConnectors": [{
			"name": "1"
		}],
		"type": "DumpData",
		"id": "734625388",
		"y": 540
	}],
	"Id": "581224323"
}


# client=MongoClient('localhost')['erecon']
client=MongoClient('localhost')['ngerecon1']
client['recons'].remove({"reconName":"BBPS"})
client['sourceReference'].remove({"reconName":"BBPS"})
client['flows'].remove({"reconName":"BBPS"})


id1=client['recons'].insert(recons)
print id1
for source in sourcerefList:
	id2=client['sourceReference'].insert(source)
	print id2
id3=client['flows'].insert(flows)
print id3