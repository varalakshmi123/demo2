import sys

from pymongo import MongoClient
import logger
from FlowElements import FlowRunner

logger = logger.Logger.getInstance("smartrecon").getLogger()


def flowRunner(stmtDate, reconName, file):
    logger.info(stmtDate)
    logger.info(reconName)
    logger.info(file)
    flows = MongoClient('localhost')['ngerecon']['flows'].find_one({'reconName': reconName})
    nodes = flows.get('nodes', [])
    if len(nodes):
        initializer = nodes[0]
        initializer['properties']['basePath'] = '/opt/reconDB/mft/'
        initializer['properties']['statementDate'] = stmtDate
        initializer['properties']['uploadFileName'] = file
        initializer['properties']['compressionType'] = ".zip"
        nodes[0] = initializer

        FlowRunner(nodes).run()

if __name__ == "__main__":
    reconName = sys.argv[1]
    statementDate = sys.argv[2]
    file = sys.argv[3]
'
    flowRunner(statementDate, reconName, file)
