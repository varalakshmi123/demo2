import uuid
import pandas
import numpy as np


class Matcher():
    def __init__(self):
        pass

    def match(self, left=None, right=None, leftsumColumns=[], rightsumColumns=[], leftmatchColumns=[],
              rightmatchColumns=[], isToleranceMatch=False, toleranceValue=None,rightSourceTag='',leftSourceTag='',
              righttoleranceSumColumns=[],lefttoleranceSumColumns=[]):

        # No data found in either of frame return others as un-matched
        if left.empty or right.empty:
            left['Matched'], right['Matched'] = 'UNMATCHED', 'UNMATCHED'

        left_columns = leftmatchColumns + leftsumColumns
        right_columns = rightmatchColumns + rightsumColumns

        right.drop(columns=['LINK_ID'], errors='ignore', inplace=True)
        left.drop(columns=['LINK_ID'], errors='ignore', inplace=True)

        left = left.reset_index(drop=True)
        right = right.reset_index(drop=True)

        if len(leftsumColumns) > 0:
            leftworking = left[left_columns].groupby(leftmatchColumns).sum().reset_index()
        else:
            leftworking = left[left_columns]


        if len(rightsumColumns) > 0:
            rightworking = right[right_columns].groupby(rightmatchColumns).sum().reset_index()
        else:
            rightworking = right[right_columns]


        # Incase of carry forward disable do not add carry forward indicator columns
        if 'CARRY_FORWARD' in left.columns:
            leftworking['left_carryforward'] = left['CARRY_FORWARD']
            left_cf_column = ['left_carryforward']
        else:
            left_cf_column = []

        if 'CARRY_FORWARD' in right.columns:
            rightworking['right_carryforward'] = right['CARRY_FORWARD']
            right_cf_column = ['right_carryforward']
        else:
            right_cf_column = []

        if 'LINK_ID' in left.columns:
            leftworking['LINK_ID'] = left['LINK_ID']

        if 'LINK_ID' in right.columns:
            rightworking['LINK_ID'] = right['LINK_ID']

        finalworking = leftworking.merge(rightworking, how="outer", left_on=left_columns,
                                         right_on=right_columns, indicator=True, suffixes=('', '_y'))

        for col in finalworking.columns.values:
            if col.endswith("_y"):
                del finalworking[col]

        matched = finalworking[finalworking["_merge"] == "both"]
        if 'LINK_ID' not in matched.columns:
            matched['LINK_ID'] = matched[matched.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
        if not matched.empty:
            matched.loc[matched['LINK_ID'].fillna('') == '', 'LINK_ID'] = matched[matched.columns[0]].apply(
                lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

        if '_merge' in matched.columns:
            del matched['_merge']

        if len(leftsumColumns) > 0:
            leftmatchColumns = list(set(leftmatchColumns) - set(leftsumColumns))

        if len(rightsumColumns) > 0:
            rightmatchColumns = list(set(rightmatchColumns) - set(rightsumColumns))

        left = left.merge(matched[leftmatchColumns + right_cf_column + ['LINK_ID']], how="left", on=leftmatchColumns,
                          indicator=True)

        if 'LINK_ID_x' in left.columns:
            left.loc[left['LINK_ID_x'].fillna('') == '', 'LINK_ID_x'] = left['LINK_ID_y']
            left.rename(columns={'LINK_ID_x': 'LINK_ID'}, inplace=True)

        right = right.merge(matched[rightmatchColumns + left_cf_column + ['LINK_ID']], how="left", on=rightmatchColumns,
                            indicator=True)

        if 'LINK_ID_x' in right.columns:
            right.loc[right['LINK_ID_x'].fillna('') == '', 'LINK_ID_x'] = right['LINK_ID_y']
            right.rename(columns={'LINK_ID_x': 'LINK_ID'}, inplace=True)

        left['Matched'] = 'UNMATCHED'
        left.loc[left['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del left["_merge"]

        right['Matched'] = 'UNMATCHED'
        right.loc[right['_merge'] == 'both', 'Matched'] = 'MATCHED'
        del right['_merge']

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

        if isToleranceMatch:

            leftMathed = left[left['Matched'] == 'MATCHED']

            if 'Tolerance Difference' in leftMathed.columns:
                leftMathed.loc[(leftMathed['ToleranceMatch'].fillna('') != 'Yes'), 'Tolerance Difference'] = np.nan

            leftUnMatched = left[left['Matched'] == 'UNMATCHED']

            rightMathed = right[right['Matched'] == 'MATCHED']

            if 'Tolerance Difference' in rightMathed.columns:
                rightMathed.loc[(rightMathed['ToleranceMatch'].fillna('') != 'Yes'), 'Tolerance Difference'] = np.nan

            rightUnMatched = right[right['Matched'] == 'UNMATCHED']

            if len(rightUnMatched)>0 and len(leftUnMatched)>0:
                if left['SOURCE'].unique()[0]=='FEBA' and right['SOURCE'].unique()[0]=='OLT':
                    print 'OLT'
                leftToleranceMatch, rightToleranceMatch = self.toleranceMatch(rightUnMatched, leftUnMatched,
                                                                          leftmatchColumns, leftsumColumns,
                                                                          rightmatchColumns,
                                                                          rightsumColumns, toleranceValue,righttoleranceSumColumns,lefttoleranceSumColumns)

            # if 'Tolerance Difference' not in leftToleranceMatch.columns:
            #     leftToleranceMatch['Tolerance Difference']=np.nan
            #     rightToleranceMatch['ToleranceMatch']=''
            #
            # if 'Tolerance Difference' not in rightToleranceMatch.columns:
            #     rightToleranceMatch['Tolerance Difference']=np.nan
            #     rightToleranceMatch['ToleranceMatch']=''

                left = pandas.concat([leftMathed, leftToleranceMatch], join_axes=[leftToleranceMatch.columns])
                right = pandas.concat([rightMathed, rightToleranceMatch], join_axes=[rightToleranceMatch.columns])

        # left, right = self.add_match_remarks(left, right, leftmatchColumns, rightmatchColumns, leftsumColumns,
        #                                          rightsumColumns,rightSourceTag,leftSourceTag)


        return left, right

    def add_match_remarks(self, left, right, leftkeyColumns=[], rightkeycolumns=[], leftsumColumns=[],
                          rightsumColumns=[],rightSourceTag='',leftSourceTag=''):

        left_len, right_len = len(left), len(right)

        left['Mismatch_ID'] = left[left.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
        right['Mismatch_ID'] = right[right.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

        orglleft = left.copy()
        orgright = right.copy()
        left_columns = leftkeyColumns
        right_columns = rightkeycolumns

        if len(leftsumColumns) > 0:
            leftworking = left[left_columns].groupby(leftkeyColumns).sum().reset_index()
        else:
            leftworking = left[left_columns]

        if len(rightsumColumns) > 0:
            rightworking = right[right_columns].groupby(rightkeycolumns).sum().reset_index()
        else:
            rightworking = right[right_columns]

        left = left.merge(leftworking, on=leftkeyColumns, suffixes=('', '_x'))

        right = right.merge(rightworking, on=rightkeycolumns, suffixes=('', '_y'))

        # req1 = req1.merge(s1, on=leftUnique, suffixes=('', '_x'))
        # req2 = req2.merge(s2, on=leftUnique, suffixes=('', '_y'))
        #
        # req1.drop(['AMOUNT', 'AMOUNT_1'], axis=1, inplace=True)
        # req2.drop(['AMOUNT', 'AMOUNT_1'], axis=1, inplace=True)


        merged = left.merge(right, left_on=leftkeyColumns, right_on=rightkeycolumns, how="outer", indicator=True)

        if not 'MisMatch' in merged.columns:
            merged['MisMatch'] = ''
        if not 'MisMatch2' in merged.columns:
            merged['MisMatch2'] = ''

        for i, j in zip(leftsumColumns, rightsumColumns):
            for row, col in merged[merged["_merge"] == 'both'].iterrows():
                if col[i + '_x'] != col[i + '_y']:
                    merged['MisMatch'] = merged['MisMatch'].astype(str)
                    merged.loc[row, 'MisMatch'] = merged.loc[row, 'MisMatch'] + "," + i if \
                        merged.loc[row, 'MisMatch'] != '' else \
                        i
                    merged['MisMatch2'] = merged['MisMatch2'].astype(str)
                    merged.loc[row, 'MisMatch2'] = merged.loc[row, 'MisMatch2'] + "," + j if \
                        merged.loc[row, 'MisMatch2'] != '' else \
                        j

        if 'MisMatch_x' in merged.columns:
            merged.drop(['MisMatch_x','MisMatch2_x'], axis=1, inplace=True)
            merged.drop(['MisMatch_y','MisMatch2_y'], axis=1, inplace=True)

        merged=merged.drop_duplicates(subset=['Mismatch_ID_x'])
        merged=merged.drop_duplicates(subset=['Mismatch_ID_y'])

        left=left.merge(merged,left_on=['Mismatch_ID'],right_on=['Mismatch_ID_x'],how='left',suffixes=('','_y'))
        right=right.merge(merged,left_on=['Mismatch_ID'],right_on=['Mismatch_ID_y'],how='left',suffixes=('','_y'))

        for col in left.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del left[col]

        for col in right.columns.values:
            if col.endswith("_y") or col.endswith("_x"):
                del right[col]

        if rightSourceTag + " Match" in right.columns:
             del right[rightSourceTag + " Match"]

        if leftSourceTag + " Match" in right.columns:
             del left[leftSourceTag + " Match"]

        # if 'Exception Remarks' not in right.columns:
        #     right['Exception Remarks'] = ''
        #
        # if 'Exception Remarks' not in left.columns:
        #     left['Exception Remarks'] = ''
        #
        # if 'Matching Remarks' not in right.columns:
        #     right['Matching Remarks'] = ''
        #
        # if 'Matching Remarks' not in left.columns:
        #     left['Matching Remarks'] = ''
        #
        # left.loc[left['Matched'] == 'MATCHED', ['Matching Remarks', 'Exception Remarks']] = '', ''
        # right.loc[right['Matched'] == 'MATCHED', ['Matching Remarks', 'Exception Remarks']] = '', ''
        #
        # for idx in range(0, len(left_columns)):
        #     left_col = left_columns[idx]
        #     right_col = right_columns[idx]
        #
        #     left_unique = left[[left_col]].drop_duplicates(keep='first')
        #     right_unique = right[[right_col]].drop_duplicates(keep='first')
        #
        #     right = right.merge(left_unique, how='left', left_on=right_col, right_on=left_col,
        #                         indicator=True,suffixes=('', '_y'))
        #
        #     left = left.merge(right_unique, how='left', left_on=left_col, right_on=right_col,
        #                       indicator=True,suffixes=('', '_y'))
        #
        #     # r_idx = right[right_col] == Utilities().infer_dtypes(right[right_col])
        #     # l_idx = left[left_col] == Utilities().infer_dtypes(left[left_col])
        #
        #     right[['Exception Remarks', 'Matching Remarks']] = right[['Exception Remarks', 'Matching Remarks']].fillna(
        #         '')
        #     left[['Exception Remarks', 'Matching Remarks']] = left[['Exception Remarks', 'Matching Remarks']].fillna('')
        #
        #
        #     right.loc[(right['_merge'] != 'both'), "Exception Remarks"] += right_col + ','
        #     left.loc[(left['_merge'] != 'both'), "Exception Remarks"] += left_col + ','
        #
        #     right.loc[(right['_merge'] == 'both'), "Matching Remarks"] += right_col + ','
        #     left.loc[(left['_merge'] == 'both'), "Matching Remarks"] += left_col + ','
        #
        #
        #     right.drop(columns='_merge', inplace=True, errors='ignore')
        #     left.drop(columns='_merge', inplace=True, errors='ignore')
        #
        # left['Exception Remarks'] = left['Exception Remarks'].str.rstrip(',')
        # right['Exception Remarks'] = right['Exception Remarks'].str.rstrip(',')
        #
        # left['Matching Remarks'] = left['Matching Remarks'].str.rstrip(',')
        # right['Matching Remarks'] = right['Matching Remarks'].str.rstrip(',')
        #
        # for col in left.columns.values:
        #     if col.endswith("_y"):
        #         del left[col]
        #
        #
        # for col in right.columns.values:
        #     if col.endswith("_y"):
        #         del right[col]
        #
        #
        # if len(left) != left_len or len(right) != right_len:
        #     print 'Possible cartesian product while adding matchin remarks !!!'

        left.drop(['_merge'], axis=1, inplace=True)
        right.drop(['_merge'], axis=1, inplace=True)

        return left, right

    def toleranceMatch(self, right, left, leftmatchColumns, leftsumColumns,
                       rightmatchColumns, rightsumColumns, tolerancevalue,righttoleranceSumColumns,lefttoleranceSumColumns):

        try:

            print 'in'

            left['L_TOL_ID'] = left[left.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
            right['R_TOL_ID'] = right[right.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
            leftcopy=left.copy()
            rightcopy=right.copy()
            leftSource=left['SOURCE'].unique()[0]
            rightSource=right['SOURCE'].unique()[0] 
            print len(left),len(right)

            if len(leftsumColumns) > 0:
                leftworking = left[leftmatchColumns + leftsumColumns+['L_TOL_ID']].groupby(leftmatchColumns).sum().reset_index()
                leftcopy=leftcopy.merge(leftworking,on=leftmatchColumns,how='inner',suffixes=('','_x'))
                for col in leftcopy.columns:
                    if col.replace('_x','') in leftsumColumns:
                        leftcopy.drop(columns=[col], errors='ignore', inplace=True)
                        leftcopy.rename(columns={col+'_x':col},inplace=True)
                leftworking=leftcopy

            else:
                leftworking = left[leftmatchColumns + leftsumColumns+['L_TOL_ID']]

            if len(rightsumColumns) > 0:
                rightworking = right[rightmatchColumns + rightsumColumns+['R_TOL_ID']].groupby(rightmatchColumns).sum().reset_index()
                rightcopy = rightcopy.merge(rightworking, on=rightmatchColumns, how='inner', suffixes=('', '_x'))
                for col in rightcopy.columns:
                    if col.replace('_x', '') in rightsumColumns:
                        rightcopy.drop(columns=[col], errors='ignore', inplace=True)
                        rightcopy.rename(columns={col + '_x': col},inplace=True)
                rightworking = rightcopy

            else:
                rightworking = right[rightmatchColumns + rightsumColumns+['R_TOL_ID']]

            # if 'Tolerance Difference' in right.columns:
            #     rightdelta=right[right['Tolerance Difference'].fillna(0)!=0]
            #     right=right[right['Tolerance Difference'].fillna(0)==0]
            #
            # if 'Tolerance Difference' in left.columns:
            #     leftdelta=left[left['Tolerance Difference'].fillna(0)!=0]
            #     left=left[left['Tolerance Difference'].fillna(0)==0]

            # right.drop(columns=['Tolerance Difference','ToleranceMatch'], errors='ignore', inplace=True)
            # left.drop(columns=['Tolerance Difference','ToleranceMatch'], errors='ignore', inplace=True)

            final = leftworking.merge(rightworking, right_on=list(set(rightmatchColumns) - set(righttoleranceSumColumns)), left_on=list(set(leftmatchColumns) - set(lefttoleranceSumColumns)), how='outer',
                                      suffixes=('_left', '_right'), indicator=True)


            final = final[final['_merge'] == 'both']

            # if 'Policy Number' in final.columns and len(final[final['Policy Number']=='F0481266'])>0:
            #     print 'yes'

            if '_merge' in final.columns:
                del final['_merge']

            if len(final) > 0:

                final['ToleranceMatch'] = ''
                for index in range(0,len(lefttoleranceSumColumns)):
                    # if leftsumColumns == rightsumColumns:

                    final['Tolerance Difference'+rightSource+lefttoleranceSumColumns[index]] = final[lefttoleranceSumColumns[index]+'_left'] - final[lefttoleranceSumColumns[index]+'_right']
                    # else:
                    #
                    #     final['Tolerance Difference'] = final[','.join(leftsumColumns)] - final[','.join(rightsumColumns)]

                    final.loc[final['Tolerance Difference'+rightSource+lefttoleranceSumColumns[index]].round(2).between(-tolerancevalue,
                                                                         tolerancevalue), "ToleranceMatch"+rightSource+lefttoleranceSumColumns[index]] = "Yes"

                for index in range(0, len(righttoleranceSumColumns)):
                        # if leftsumColumns == rightsumColumns:

                        final['Tolerance Difference' +leftSource+righttoleranceSumColumns[index]] = final[righttoleranceSumColumns[
                                                                                                   index] + '_left'] - \
                                                                                         final[righttoleranceSumColumns[
                                                                                                   index] + '_right']
                        # else:
                        #
                        #     final['Tolerance Difference'] = final[','.join(leftsumColumns)] - final[','.join(rightsumColumns)]

                        final.loc[final['Tolerance Difference' +leftSource+righttoleranceSumColumns[index]].round(2).between(
                            -tolerancevalue,
                            tolerancevalue), "ToleranceMatch" +leftSource+righttoleranceSumColumns[index]] = "Yes"


                        # final = final[final['ToleranceMatch'] == 'Yes']

                if not final.empty:
                        final['LINK_ID'] = final[final.columns[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                        final['LINK_ID'] = final['LINK_ID'].astype(str)

            for col in final.columns.tolist():
                if col.endswith("_left") or col.endswith("_right"):
                    del final[col]
            setcolleft=[]
            for col in lefttoleranceSumColumns:
                setcolleft.append('Tolerance Difference'+rightSource+col)
                setcolleft.append('ToleranceMatch'+rightSource+col)

            setcolright=[]
            for col in righttoleranceSumColumns:
                setcolright.append('Tolerance Difference' +leftSource+col)
                setcolright.append('ToleranceMatch' +leftSource+col)
                
            print 'After This'
            if set( setcolleft+['LINK_ID']).issubset(final.columns.tolist()):
                final=final.drop_duplicates(subset=['L_TOL_ID'])
                left = left.merge(final[list(set(leftmatchColumns)-set(lefttoleranceSumColumns)) + setcolleft+['LINK_ID','L_TOL_ID']],
                                  how="left", on=['L_TOL_ID'],
                                  indicator=True, suffixes=('', '_y'))

            else:
                final = final.drop_duplicates(subset=['L_TOL_ID'])
                left = left.merge(final[list(set(leftmatchColumns) - set(lefttoleranceSumColumns)) + ['L_TOL_ID']],
                                  how="left", on=['L_TOL_ID'],
                                  indicator=True, suffixes=('', '_y'))
            del left['L_TOL_ID']


            if set(setcolright + ['LINK_ID']).issubset(final.columns.tolist()):
                final = final.drop_duplicates(subset=['R_TOL_ID'])
                right = right.merge(final[list(set(rightmatchColumns)-set(righttoleranceSumColumns)) + setcolright+['LINK_ID','R_TOL_ID']],
                                    how="left", on=['R_TOL_ID'],
                                    indicator=True, suffixes=('', '_y'))
            else:
                final = final.drop_duplicates(subset=['R_TOL_ID'])
                right = right.merge(final[list(set(rightmatchColumns)-set(righttoleranceSumColumns))+['R_TOL_ID']], how="left", on=['R_TOL_ID'],
                                    indicator=True, suffixes=('', '_y'))

            # left['Matched'] = 'UNMATCHED'

            del right['R_TOL_ID']

            for col in lefttoleranceSumColumns:
                if 'Tolerance Difference'+rightSource+col+'_y' in left.columns:
                    left.loc[left['Tolerance Difference'+rightSource+col+'_y'].fillna(0) != 0, 'Tolerance Difference'+rightSource+col] = left[
                        'Tolerance Difference' +rightSource+col + '_y']

                    left.loc[left['Tolerance Difference' +rightSource+col + '_y'].fillna(0) != 0, 'ToleranceMatch'+rightSource+col] = left[
                        'ToleranceMatch' +rightSource+col + '_y']

                    left.loc[left['Tolerance Difference' +rightSource+col + '_y'].fillna(0) != 0, 'LINK_ID'] = left['LINK_ID_y']

                elif 'LINK_ID_y' in left.columns:
                    left.loc[left['ToleranceMatch'+rightSource+col].fillna('') == 'Yes', 'LINK_ID'] = left['LINK_ID_y']

                if 'Tolerance Difference'+leftSource+col+'_y' in right.columns:
                    right.loc[right['Tolerance Difference'+leftSource+col+'_y'].fillna(0) != 0, 'Tolerance Difference'+leftSource+col] = right['Tolerance Difference'+leftSource+col+'_y']

                    right.loc[right['Tolerance Difference'+leftSource+col+'_y'].fillna(0) != 0, 'ToleranceMatch'+leftSource+col] = right['ToleranceMatch' +leftSource+col + '_y']

                    right.loc[right['Tolerance Difference' +leftSource+col + '_y'].fillna(0) != 0, 'LINK_ID'] = right['LINK_ID_y']

                elif 'LINK_ID_y' in right.columns:
                    right.loc[right['ToleranceMatch'+leftSource+col].fillna('') == 'Yes', 'LINK_ID'] = right['LINK_ID_y']

            left['Matched'] = 'UNMATCHED'

            for col in lefttoleranceSumColumns:
                if 'Tolerance Difference'+rightSource+col in left.columns:
                    left.loc[(left['_merge'] == 'both') & (
                    left['Tolerance Difference'+rightSource+col].round(2).between(-tolerancevalue, tolerancevalue)), 'Matched'] = 'MATCHED'
                    left.loc[(left['Tolerance Difference'+rightSource+col]<-tolerancevalue)|(left['Tolerance Difference'+rightSource+col]>tolerancevalue),'Matched']='UNMATCHED'
            right['Matched'] = 'UNMATCHED'


            for col in righttoleranceSumColumns:
                if 'Tolerance Difference'+leftSource+col in right.columns:
                        right.loc[(right['_merge'] == 'both') & (
                        right['Tolerance Difference'+leftSource+col].round(2).between(-tolerancevalue, tolerancevalue)), 'Matched'] = 'MATCHED'
                        right.loc[(right['Tolerance Difference'+leftSource+col]<-tolerancevalue)|(right['Tolerance Difference'+leftSource+col]>tolerancevalue),'Matched']='UNMATCHED'



            del left["_merge"]

            del right['_merge']

            for col in left.columns.values:

                if col.endswith("_y") or col.endswith("_x"):
                    del left[col]

            for col in right.columns.values:

                if col.endswith("_y") or col.endswith("_x"):
                    del right[col]

            # left, right = self.add_match_remarks(left, right, leftmatchColumns, rightmatchColumns, leftsumColumns,
            #                                      rightsumColumns)
            # left = pandas.concat([left, leftdelta], join_axes=[left.columns])
            # right = pandas.concat([right, rightdelta], join_axes=[right.columns])
            # left=left.drop_duplicates(subset=['LINK_ID'],keep='first')
            # right=right.drop_duplicates(subset=['LINK_ID'],keep='first')
            print len(left),len( right)
            return left, right
        except Exception as e:
               print e