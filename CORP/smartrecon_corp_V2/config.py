logBaseDir = '/var/log/ngerecon/'
basepath = '/usr/share/nginx/smartrecon_V2/'
mftpath = "/opt/reconDB/mft/"

mongoHost = 'localhost'
databaseName = 'ngerecon'
bank_name = ''
contentDir='/usr/share/nginx/www/ngerecon/ui/app/files'
# common feed_params to be extracted from the source reference.
feed_params = {"CSV": ['skiprows', 'skipfooter', 'delimiter'],
               "Excel": ['skiprows', 'skipfooter','sheetNames'],
               'SFMSMultiSheet': ['skiprows', 'skipfooter', 'FooterKey', 'columnNames'], 'XML': ['query'],
               "HTML":["parseIndex"]}

preload_scriptPath = "/usr/share/nginx/smartrecon_V2/customscripts/"
splitSourcePath = '/usr/share/nginx/smartrecon/splitSources/'

sourceList=['SFMS_NEFT','SFMS_RTGS']

reconList=['NEFTInward','NEFTOutward','RTGSInward','RTGSOutward']
