# from pymongo import MongoClient
# import json
# import random
#
# flowstruct={
#  "reconProcess": "",
#  "updated": 1536708880,
#  "reconName": "",
#  "created": 1533283862,
#  "machineId": "bb305fe9-d451-4d59-86dc-26137a8a9ec1",
#  "connections": [
#                 ],
#  "partnerId":"", "nodes": [], "Id": "123456789"}
#
#
# def generateFlow(flowstruct,reconName,reconProcess,group,postNodesList):
#     flowstruct['reconName']=reconName
#     flowstruct['reconProcess']=reconProcess
#     # check if default nodes exist write
#     doc=MongoClient('localhost')['ngerecon']['flows'].find_one({"reconName":reconName,"reconProcess":reconProcess})
#     if not doc:
#      flow=getDefaultNodes(flowstruct)
#
#     for rule in group:
#         flow=getNodeForNway(flow,rule['columns'])
#
#     if postNodesList:
#         flow=getPostNodes(flow,postNodesList)
#     return flow
#
# def getDefaultNodes(flow):
#     # print flow
#     with open('/usr/share/nginx/www/ngerecon/flask/nodestemplate.json') as fp:
#        obj=json.load(fp)
#     flow=generateConnection(flow)
#     obj['Initializer']['properties']['reconName']=flow['reconName']
#     obj['Initializer']['properties']['reconProcess']=flow['reconProcess']
#     flow['nodes'].append(obj['Initializer'])
#     # flow=assignIdToNode(flow)
#     flow=generateConnection(flow)
#     flow['nodes'].append(obj['PreLoader'])
#     return flow
#
# def getNodeForNway(flow,matchColumns):
#     with open('/usr/share/nginx/www/ngerecon/flask/nodestemplate.json') as fp:
#        obj=json.load(fp)
#     flow=generateConnection(flow)
#     for node in flow['nodes']:
#         if node['name'].strip() == 'NWayMatch':
#             nwayMatchExists = True
#         else:
#             nwayMatchExists=False
#     sourcesList=[]
#     for source in matchColumns.keys():
#         data={}
#         data['sourceTag']=source
#         data['source']='results.'+source if nwayMatchExists else source
#         data['columns']={'matchColumns':matchColumns[source]['matchColumns'],'sumColumns':matchColumns[source]['sumColumns']}
#         sourcesList.append(data)
#     obj['NWayMatch']['properties']['sources']=sourcesList
#     flow['nodes'].append(obj['NWayMatch'])
#     return flow
#
#
# def generateConnection(flow):
#     connection = {"dest": {"nodeID": "", "connectorIndex": 0},
#                   "source": {"nodeID": "", "connectorIndex": 0}}
#     if not flow['connections']:
#         connection['source']['nodeID']=1000
#         connection['dest']['nodeID'] = connection['source']['nodeID']+ 1
#     else:
#         connectionId=flow['connections'][len(flow['connections'])-1]['dest']['nodeID']
#         connection['source']['nodeID']=connectionId
#         connection['dest']['nodeID']=connection['source']['nodeID']+1
#
#     flow['connections'].append(connection)
#     return flow
#
# def getPostNodes(flow,postNodesList):
#     with open('/usr/share/nginx/www/ngerecon/flask/nodestemplate.json') as fp:
#        obj=json.load(fp)
#     for node in postNodesList:
#
#         if node=='DumpData':
#             flow['nodes'].append(obj['DumpData'])
#     flow=assignIdToNodes(flow)
#     return flow
#
#
# def assignIdToNodes(flow):
#     idList=[]
#     nodeList=[]
#     for connection in flow['connections']:
#         idList.extend([connection['source']['nodeID'],connection['dest']['nodeID']])
#     idList = list(set(idList))
#
#     for id,node in zip(idList,flow['nodes']):
#         node['id']=str(id)
#         nodeList.append(node)
#     flow['nodes']=nodeList
#     return flow
#
# if __name__=='__main__':
#     group = [{"ruleName":"fine1",
#         "columns":{
#             "CBS":{"matchColumns":['Refnumber','Amount'],"sumColumns":[]},
#             "SFMS":{"matchColumns":['Refnumber','Amount'],"sumColumns":[]}}
#     },{"ruleName":"fine2",
#         "columns":{
#             "CBS":{"matchColumns":['Refnumber','Amount'],"sumColumns":[]},
#             "SFMS":{"matchColumns":['Refnumber','Amount'],"sumColumns":[]}}
#     }]
#     reconName='test'
#     reconProcess='testing'
#     postNodesList=['DumpData']
#     flow=generateFlow(flowstruct,reconName,reconProcess,group,postNodesList)
#
#     MongoClient('localhost')['ngerecon']['flows'].insert(flow)