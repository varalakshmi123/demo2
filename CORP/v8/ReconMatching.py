# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

from Utilities import Utilities
# from DBOps import DBOps
from MatchFunctions import MatchFunctions
from CustomFunctions import CustomFunctions
from PostMatch import PostMatch

import logging
import pandas as pd
import numpy as np
import uuid
from datetime import datetime
import traceback
from datetime import timedelta
import sys
from PropertiesHelper import PropertiesHelper
import os
import convertStringToFloat
import ZFSApi
import config
import time


class ReconMatching():
    ## Constructor
    def __init__(self, reconDef, stmtDate, execId, jobSummary, dbOps):
        self.jobSummary = jobSummary
        self.logger = logging.getLogger('PYRBE')
        self.reconDef = reconDef
        self.sourceCount = self.reconDef['processingCount']
        self.bizCtxtId = reconDef['businessContextId']
        self.reconId = reconDef['reconId']
        self.stmtDate = stmtDate
        self.execId = execId
        self.dbOps = dbOps
        self.utils = Utilities(dbOps=dbOps)
        self.mfns = MatchFunctions()
        self.cfns = CustomFunctions(self.reconId, execId, stmtDate, dbOps)
        self.postMatch = PostMatch(self.reconId, execId, dbOps)
        self.dtcols = []
        self.propsHelper = PropertiesHelper()
        self.crSymbols = self.propsHelper.getProperty('CREDIT_SYMBOLS').split(';')
        self.drSymbols = self.propsHelper.getProperty('DEBIT_SYMBOLS').split(';')
        self.rrSymbols = self.propsHelper.getProperty('REVERSAL_SYMBOLS').split(';')
        self.prevExecID = None

    ## Run the matching logic
    def run(self, reconData, nonPartData):
        self.reconData = reconData
        self.nonPartData = nonPartData
        qry = self.utils.getPreviousExecutionId(self.reconId)
        if qry != None:
            self.prevExecID = qry['RECON_EXECUTION_ID']

        self.utils.addDetailsToSummary(self.jobSummary, 'matchStart', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
        # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'EXEC_MATCH_PROG')
        # Log matching start
        self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                 self.stmtDate, self.execId, 'Matching',
                                 'Matching Initiated')
        self.logger.info("| Matching Process Initiated...")
        self.logger.info('----------------------------------------')
        self.totIp = 0
        self.totOp = 0
        self.positionData = None
        currCount = 0
        cfwdCount = 0
        try:
            self.carryFwdFrame = None
            for key in reconData.keys():
                if len(reconData[key])==0:
                    for source in self.reconDef['sources']:
                        if source['sourceType'] == key:
                            feedDef = self.utils.getFeedDefDSB(source['sourceType'], source['feedId'],
                                                               self.jobSummary)
                            types = self.utils.getColTypes(feedDef)
                            names = self.utils.getColNames(feedDef)
                            names = [i for i in names if i not in reconData[key].columns]
                            names.extend(reconData[key].columns)
                            reconData[key] = pd.DataFrame(columns=names)
                names = reconData[key].columns
                # print reconData[key].head()
                df = None
                if self.reconId not in config.ignoreCarryFwdRecons:
                    df = self.loadCarryFwd(key, names, self.reconDef['sources'])
                self.logger.info('Current execution data for ' + key + ' = %6d' %
                                 (0 if reconData[key] is None else len(reconData[key])))
                self.logger.info('Carryforward data for ' + key + ' = %6d' % (0 if df is None else len(df)))
                if df is not None:

                    cfwdCount += len(df)
                    if reconData[key] is None:
                        reconData[key] = df
                    else:
                        currCount += len(reconData[key])
                        reconData[key] = pd.concat([reconData[key], df], ignore_index=True)
                self.totIp += len(reconData[key])
                if 'AMOUNT' in reconData[key].columns:
                    reconData[key]['AMOUNT'] = reconData[key]['AMOUNT'].fillna(0).apply(lambda x: x if x else 0)
                self.logger.info('Total data for ' + key + ' = %6d' % len(reconData[key]))
                self.logger.info('----------------------------------------')
                # Check
                # Get positionData for this source
                if nonPartData is not None:
                    # Get entries with type 'P' from non participating data
                    if '_CR' in key or '_DR' in key:
                        data = nonPartData[key.split('_')[0]]
                    else:
                        data = nonPartData[key]
                    posData = None
                    if data is not None:
                        posData = data[data['ENTRY_TYPE'] == 'P']
                    if posData is not None:
                        posData['RECON_ID'] = self.reconId
                        posData['BUSINESS_CONTEXT_ID'] = self.bizCtxtId
                        posData['STATEMENT_DATE'] = self.stmtDate
                        posData['RECON_EXECUTION_ID'] = self.execId
                        posData['CREATED_DATE'] = datetime.now().strftime('%d/%m/%Y %I:%M %p')
                        posData['CREATED_DATE'] = pd.to_datetime(posData['CREATED_DATE'])
                        posData['CREATED_BY'] = 'SYSTEM'
                        posData['IPADDRESS'] = '0.0.0.0'
                        posData['MIRROR_ACCOUNT_NUMBER'] = posData[
                            'ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in posData.columns else ''
                        posData['OBJECT_OID'] = posData['RECON_ID'].apply(
                            lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                        posData['OBJECT_ID'] = posData['OBJECT_ID'].apply(
                            lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                        posData['MATCHING_STATUS'] = 'NA'
                        posData['RECORD_STATUS'] = 'ACTIVE'
                        posData['MATCHING_EXECUTION_MODE'] = 'AUTO'
                        posData['EXECUTION_DATE_TIME'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        posData['EXECUTION_DATE_TIME'] = pd.to_datetime(posData['EXECUTION_DATE_TIME'])
                        if 'AMOUNT' in posData.columns:
                            posData['AMOUNT'] = posData['AMOUNT'].fillna(0).apply(lambda x: x if x else 0)
                        if self.positionData is None:
                            self.positionData = pd.DataFrame.from_dict(posData)
                        else:
                            self.positionData = pd.concat([self.positionData, posData])
            self.logger.info('Total input data = %6d' % self.totIp)
            self.logger.info('----------------------------------------')
            preMatch = self.utils.getPrematch(self.reconId)
            if preMatch is not None:
                if 'globalFunc' in preMatch and preMatch['globalFunc']:
                    if isinstance(preMatch['expression'], unicode) or isinstance(preMatch['expression'], str):
                        exec (preMatch['expression']) in globals(), locals()
                    if isinstance(preMatch['expression'], list):
                        for xqtComd in preMatch['expression']:
                            exec (xqtComd) in globals(), locals()
                else:
                    expr = preMatch['expression']
                    eval(expr)

            if self.reconId == 'BS_CASH_APAC_19128':
                (self.reconData, filtered_rec) = self.cfns.bs_19128(self.reconData)

            if self.reconId == 'EPYMTS_CASH_APAC_70001' or self.reconId == 'EPYMTS_CASH_APAC_70002':
                (self.reconData, filtered_rec) = self.cfns.getCBS_RRNo(self.reconData)

            if self.reconId == 'DSB_CASH_APAC_20423':
                df = None
                s1DF = None
                s2DF = None
                recCount = 0
                self.logger.info('----------------------------------------')
                for source in self.reconDef['sources']:
                    if source['sourceType'] == 'S1':
                        feedDef = self.utils.getFeedDefDSB(source['sourceType'], source['feedId'], self.jobSummary)
                        types = self.utils.getColTypes(feedDef)
                        names = self.utils.getColNames(feedDef)
                        csvtypes = dict()
                        dtcols = []
                        for i in range(0, len(types)):
                            # print types[i]
                            if types[i] == 'np.datetime64':
                                dtcols.append(names[i])
                                continue
                            if types[i] == 'np.int64':
                                continue
                        df = self.cfns.DSB002_New(self.reconData[str(source['sourceType'])],
                                                  dtcols, self.dbOps,self.prevExecID )

                s1DF = df[df['SOURCE_TYPE'] == 'S1']
                s2DF = df[df['SOURCE_TYPE'] == 'S2']
                for source in self.reconDef['sources']:
                    if source['sourceType'] == 'S1':
                        recCount += len(s1DF)
                        self.reconData[str(source['sourceType'])] = s1DF
                    elif source['sourceType'] == 'S2':
                        oldDF = self.reconData[str(source['sourceType'])]
                        newDF = pd.concat([oldDF, s2DF])
                        recCount += len(newDF)
                        self.reconData[str(source['sourceType'])] = newDF

            if self.reconId == 'CB_CASH_APAC_61171':
                (self.reconData) = self.cfns.cropBReports(self.reconData,self.stmtDate)

            self.totIp = 0
            for key in self.reconData.keys():
                self.totIp += eval("len(self.reconData['" + key + "'])")
            self.logger.info('----------------------------------------')
            self.logger.info('Updated input data       = %6d' % self.totIp)
            (matched, unmatched) = self.processRules()

            self.totOp += (0 if matched is None else len(matched))
            self.totOp += (0 if unmatched is None else len(unmatched))
            self.logger.info('Total output data = %6d' % self.totOp)
            self.logger.info('----------------------------------------')

            self.utils.addDetailsToSummary(self.jobSummary, 'currentInput', currCount)
            self.utils.addDetailsToSummary(self.jobSummary, 'cfwdInput', cfwdCount)
            self.utils.addDetailsToSummary(self.jobSummary, 'totalInput', self.totIp)
            self.utils.addDetailsToSummary(self.jobSummary, 'matchedCount', (0 if matched is None else len(matched)))
            self.utils.addDetailsToSummary(self.jobSummary, 'unmatchedCount',
                                           (0 if unmatched is None else len(unmatched)))
            self.utils.addDetailsToSummary(self.jobSummary, 'totalOutput', self.totOp)
            self.matched = matched
            self.unmatched = unmatched
            self.btotOp = self.totOp
            postMatch = self.utils.getPostmatch(self.reconId)
            if postMatch is not None:
                if 'globalFunc' in postMatch and postMatch['globalFunc']:
                    self.totOp = 0
                    if isinstance(postMatch['expression'], unicode) or isinstance(postMatch['expression'], str):
                        exec (postMatch['expression']) in globals(), locals()
                else:
                    self.totOp = 0
                    expr = postMatch['expression']
                    eval(expr)

                self.totOp += (0 if self.matched is None else len(self.matched))
                self.totOp += (0 if self.unmatched is None else len(self.unmatched))
                self.logger.info('Total output data after post match = %6d' % self.totOp)
                self.logger.info('----------------------------------------')

            self.utils.addDetailsToSummary(self.jobSummary, 'postmatchIgnored', self.btotOp - self.totOp)
            totalDf = self.dumpDataToDB()

            if self.reconId == 'EPYMTS_CASH_APAC_0012':
                if totalDf is not None and len(totalDf) > 0:
                    self.logger.info('Generating Custom Reports.')
                    self.cfns.principle_account_report(totalDf)

            self.logger.info('Persisting balances...')
            if self.positionData is not None and len(self.positionData):
                self.dbOps.saveDFToHD5(self.positionData, 'POSITION_DATA', reconId=self.reconId, execId=self.execId)
            mdbDoc = self.postMatch.run(self.prevExecID, totalDf)
            for col in mdbDoc.keys():
                self.utils.addDetailsToSummary(self.jobSummary, col, mdbDoc[col])

            if self.reconId == 'TRSC_CASH_APAC_20181':
                self.cfns.nostroReport(self.reconId, reconData=self.reconData)

            txns_closed_2day = totalDf[(totalDf['RECORD_VERSION'] == 2) & (totalDf['TXN_MATCHING_STATUS'] == 'MATCHED')]
            tnx_closed_count_2day, tnx_closed_amount_2day = len(txns_closed_2day), txns_closed_2day['AMOUNT'].sum()

            self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                     self.stmtDate, self.execId, 'Matching',
                                     'Matching Completed', self.jobSummary['matchedCount'],
                                     self.jobSummary['unmatchedCount'], self.jobSummary['cfwdInput'],
                                     self.jobSummary['postmatchIgnored'], mdbDoc['AUTHORIZATION_PENDING'],
                                      mdbDoc['ROLLBACK_AUTHORIZATION_PENDING'],
                                     mdbDoc['FORCE_MATCHED'],tnx_closed_count_2day,tnx_closed_count_2day)
            flaskDir = config.flaskDir
            st, out, err = ZFSApi.execution(
                'sudo ' + flaskDir + 'erecon/bin/python2 ' + flaskDir + 'dyn_reports_gen.py ' + self.reconId + ' ' + self.execId)

            self.dbOps.commit()
        except Exception, e:
            self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                     self.stmtDate, self.execId, 'Matching',
                                     'Matching Failed', 0,
                                     0, 0)
            print e,(traceback.print_exc())
            self.logger.info('Updates rolled back')
            # self.dbOps.rollback()
            self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', 'Matching Failed,Check Log for more')

            raise BaseException(e)
        # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'EXEC_MATCH_OK')
        self.utils.addDetailsToSummary(self.jobSummary, 'matchEnd', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))

    ## Load carry forward data from database
    def loadCarryFwd(self, srcType, names, feedAccts):
        self.logger.info("Loading carryforward data for " + srcType + "...")
        # print names
        selectClause = ','.join(names)
        srcDtCols = []
        if 'STATEMENT_DATE' not in names:
            srcDtCols = ['STATEMENT_DATE']
        for name in names:
            if 'DATE' in name or 'TIMESTAMP' in name or name.startswith('TS'):
                srcDtCols.append(name)
        self.dtcols += srcDtCols
        if '_' in srcType:
            src = srcType.split('_')[0]
            dc = srcType.split('_')[1]
        else:
            src = srcType
            dc = ''
        sqlStr = "select " + selectClause
        if self.reconId == 'DSB_CASH_APAC_20423':
            sqlStr += " , CREATED_DATE "
        if 'STATEMENT_DATE' not in names:
            sqlStr += " , STATEMENT_DATE "
        sqlStr += " , RAW_LINK from cash_output_txn where " + \
                  "matching_status='UNMATCHED' and " + \
                  "reconciliation_status='EXCEPTION' and " + \
                  "record_status='ACTIVE' and " + \
                  "recon_id='" + str(self.reconId) + "' and "
        if self.prevExecID is not None:
            sqlStr += " recon_execution_id='" +str(self.prevExecID) +"' and "

        if int(self.sourceCount) == 1:
            sqlStr += "record_status = 'ACTIVE' and " + \
                      "entry_type = 'T' and " + \
                      "(authorization_status = 'AUTHORIZED' or " + \
                      "authorization_status = 'SYSTEM_AUTHORIZED')"
        else:
            sqlStr += "source_type='" + src + "' and " + \
                      "record_status = 'ACTIVE' and " + \
                      "entry_type = 'T' and " + \
                      "(authorization_status = 'AUTHORIZED' or " + \
                      "authorization_status = 'SYSTEM_AUTHORIZED')"

        if dc is not None and dc != '':
            sqlStr += " and debit_credit_indicator='" + dc + "'"
        df = None
        self.logger.info("Started Loading Carry Forward ")
        if self.prevExecID is not None:

            colToRead = [i.strip(' ') for i in sqlStr.split('select ')[1].split('from ')[0].split(',')]
            statusCols = ['RECORD_STATUS', 'TXN_MATCHING_STATUS','MATCHING_STATUS']
            if 'ENTRY_TYPE' not in colToRead:
                statusCols.append('ENTRY_TYPE')
            if 'SOURCE_TYPE' not in colToRead:
                statusCols.append('SOURCE_TYPE')
            where_clause = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' == 'UNMATCHED')"
            storeKey = '/TXN_DATA'
            df = self.dbOps.fetchFromHDFS(str(self.prevExecID), storeKey,where_clause=where_clause)
            if len(df):
                for col_ex in colToRead + statusCols:
                    if col_ex not in df.columns: df[col_ex] = ''
                # df = df[colToRead + statusCols]
                df = df.loc[df[(df['TXN_MATCHING_STATUS'] == 'UNMATCHED') & (
                    df['RECORD_STATUS'] == 'ACTIVE') & (df['ENTRY_TYPE'] == 'T')].index]
                if int(self.sourceCount) != 1:
                    df = df.loc[df[(df['SOURCE_TYPE'] == src)].index]
                for i in statusCols + ['ENTRY_TYPE', 'SOURCE_TYPE']:
                    if i not in colToRead: del df[i]
            else:
                df = None
        self.logger.info("End of Loading Carry Forward")
        if df is not None:
            df['LINK_ID'] = df['LINK_ID'].apply(str, 1)
            df['CARRY_FORWARD_FLAG'] = 'Y'
            if 'DEBIT_CREDIT_INDICATOR' in df.columns:
                df['DEBIT_CREDIT_INDICATOR'] = df['DEBIT_CREDIT_INDICATOR'].str.strip(' ')
                df.loc[df[df['DEBIT_CREDIT_INDICATOR'].isin(
                    self.crSymbols)].index, 'DEBIT_CREDIT_INDICATOR'] = 'CR'
                df.loc[df[df['DEBIT_CREDIT_INDICATOR'].isin(
                    self.drSymbols)].index, 'DEBIT_CREDIT_INDICATOR'] = 'DR'
                df.loc[df[df['DEBIT_CREDIT_INDICATOR'].isin(
                    self.rrSymbols)].index, 'DEBIT_CREDIT_INDICATOR'] = 'RR'
                # df['DEBIT_CREDIT_INDICATOR'] = df['DEBIT_CREDIT_INDICATOR'].apply(
                #     lambda x: 'CR' if x in self.crSymbols else 'DR')
        for i in feedAccts:
            if str(i['sourceType']) == str(srcType) and 'applyAccountFilter' in i and \
                    i['applyAccountFilter'] and df is not None:
                df.loc[df['ACCOUNT_NUMBER'].str[:5].astype(int) != 0, 'ACCOUNT_NUMBER'] = '00000' + df[
                    'ACCOUNT_NUMBER']
        return df

    ## Process configured match rules
    def processRules(self):
        self.logger.info("| Processing Rules...")
        self.logger.info('----------------------------------------')
        matched = None
        unmatched = None
        checkFillDFEmptyFlag = False
        initSourcesLen = 0
        for key in self.reconData.keys():
            exec key.lower() + " = self.reconData['" + key + "']"
            exec key.lower() + ".index = range(1,len(" + key.lower() + ") + 1)"
        ruleGroups = self.reconDef['matchingRules']  # ['groups']
        for ruleGroup in ruleGroups:
            self.logger.info("Rule Group " + str(int(ruleGroup['groupIndex'])))
            self.logger.info('----------------------------------------')
            if int(ruleGroup['groupIndex']) > 1:
                for key in self.reconData.keys():
                    cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + " = " + \
                          key.lower() + "_g" + str(int(ruleGroup['groupIndex']) - 1)
                    # print cmd
                    exec cmd
            else:
                for key in self.reconData.keys():
                    cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + " = " + key.lower()
                    # print cmd
                    exec cmd

            derivedSrcList = []
            if 'derivedSources' in ruleGroup.keys():
                print ruleGroup['derivedSources']
                derivedSources = ruleGroup['derivedSources']
                if len(derivedSources) > 0:
                    for derivedSource in derivedSources:
                        if derivedSource['derivedSourceIndicator'] and 'virtualSourceIndex' not in derivedSource.keys():
                            derivedSrcList.append({'derivedSource': derivedSource['sourceType'].lower() +
                                                                    "_g" + str(int(ruleGroup['groupIndex'])),
                                                   'derivedSourceIndicator': True})
                        elif derivedSource['derivedSourceIndicator'] and 'virtualSourceIndex' in derivedSource.keys():
                            derivedSrcList.append({'derivedSource': derivedSource['dfLabel'],
                                                   'derivedSourceIndicator': True})
                        conditions = derivedSource['conditions']
                        for condition in conditions:
                            # print condition,'B'
                            cString = ''.join(condition.split()).split('=', 1)
                            if len(cString) >= 2:
                                if cString[1].startswith('pd.DataFrame(columns='):
                                    checkFillDFEmptyFlag = True
                                    initSourcesLen = 0
                            print condition,'AF'
                            exec condition
                            # else:
                            #     if int(ruleGroup['groupIndex']) > 1:
                            #         for key in self.reconData.keys():
                            #             exec key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + " = " + \
                            #                 key.lower() + "_g" + str(int(ruleGroup['groupIndex'])-1)

            filtered = 0
            for derivedSrc in derivedSrcList:
                if derivedSrc['derivedSourceIndicator']:
                    filtered += len(eval(derivedSrc['derivedSource'] + "_fil"))
            if checkFillDFEmptyFlag:
                for key in self.reconData.keys():
                    exec "initSourcesLen += len(" + key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + ")"
                checkFillDFEmptyFlag = False
                if unmatched is not None:
                    self.totIp1 = initSourcesLen - (len(matched))
                    self.logger.info("Unmatched records = %6d" % (self.totIp1 - filtered if unmatched is None else len(unmatched) - filtered))
                    self.logger.info("Filtered records = %6d" % filtered)
                else:
                    filtered = self.totIp - initSourcesLen
                    self.totIp1 = initSourcesLen
                    self.logger.info("Initial records = %6d" % (self.totIp - filtered if unmatched is None else len(unmatched) - filtered))
                    self.logger.info("Ignored records = %6d" % filtered)
            else:
                self.totIp1 = self.totIp
                self.logger.info("Unmatched records = %6d" % (self.totIp1 - filtered if unmatched is None else len(unmatched) - filtered))
                self.logger.info("Filtered records = %6d" % filtered)
            self.logger.info("Matched records = %3d" % (0 if matched is None else len(matched)))
            self.logger.info('----------------------------------------')

            derivedColList = []
            if 'derivedColumns' in ruleGroup.keys():
                derivedColumns = ruleGroup['derivedColumns']
                for derivedColumn in derivedColumns:
                    # print derivedColumn['condition']
                    exec derivedColumn['condition']
                if 'derivedColumnsList' in ruleGroup.keys():
                    colList = ruleGroup['derivedColumnsList']
                    for key in colList.keys():
                        for col in colList[key]:
                            derivedColList.append(col['mdlFieldName'])
            rules = ruleGroup['rules']
            for rule in rules:
                self.logger.info("Applying rule '" + rule['ruleName'] + "'")
                self.logger.info('----------------------------------------')
                self.logger.info('| Rule Call----' +rule['condition'])

                (rmatched, runmatched) = eval(rule['condition'])
                self.logger.info('| Matched:' + str(len(rmatched)))
                self.logger.info('| UnMatched:' + str(len(runmatched)))
                self.logger.info('----------------------------------------')
                if len(rmatched): rmatched.index = range(1, len(rmatched) + 1)
                if len(runmatched): runmatched.index = range(1, len(runmatched) + 1)
                # Tag rule ID and rule category for matched set
                if rmatched is not None:
                    rmatched['TXN_MATCHING_STATUS'] = ''
                    rmatched['TXN_MATCHING_STATE'] = ''
                if runmatched is not None:
                    runmatched['TXN_MATCHING_STATUS'] = ''
                    runmatched['TXN_MATCHING_STATE'] = ''
                if rmatched is not None and len(rmatched) > 0:
                    for col in derivedColList:
                        if col in rmatched.columns:
                            del rmatched[col]
                    # rmatched['MATCHING_RULE_ID'] = str(int(rule['ruleId']))
                    rmatched['MATCHING_RULE_NAME'] = str(rule['ruleName'][0:1999])
                    # rmatched['MATCHING_RULE_INDEX'] = str(int(ruleGroup['groupIndex']))[0:1999]
                    if len(str(rule['condition'][0:1999]).split('self.mfns.performMatch(')) >1:
                        rmatched['MATCHING_RULE_CALL'] = str(rule['condition'][0:1999]).split('self.mfns.performMatch(')[1]
                    else:
                        rmatched['MATCHING_RULE_CALL'] = str(rule['condition'][0:1999])
                    # rmatched['MATCHING_RULE_CATEGORY'] = str(rule['tag'])
                    rmatched['TXN_MATCHING_STATUS'] = 'MATCHED'
                    rmatched['TXN_MATCHING_STATE'] = 'SYSTEM_MATCHED'
                if runmatched is not None and len(runmatched) > 0:
                    # runmatched['MATCHING_RULE_NAME'] = ''
                    # runmatched['MATCHING_RULE_INDEX'] = ''
                    runmatched['MATCHING_RULE_CALL'] = ''
                    # runmatched['MATCHING_RULE_CATEGORY'] = ''
                # if runmatched is not None and len(runmatched) > 0:
                #     for col in derivedColList:
                #         if col in runmatched.columns:
                #             del runmatched[col]
                if matched is None:
                    matched = rmatched
                else:
                    matched = pd.concat([matched, rmatched])
                unmatched = runmatched
                self.logger.info('----------------------------------------')

                no_of_sources = len(self.reconData.keys())
                if len(unmatched) > 0:
                    if no_of_sources == 1:
                        for derivedSource in derivedSources:
                            if not derivedSource['derivedSourceIndicator']:
                                condition = ''
                                condition = derivedSource['conditions'][0].replace(
                                    derivedSource['sourceType'].lower() + '[', 'unmatched[')
                                # print condition,'connd'
                                exec condition
                    else:
                        for key in self.reconData.keys():
                            cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + \
                                  " = unmatched[(unmatched['SOURCE_TYPE'] == '" + key + "')"
                            cmd += "]"
                            exec cmd
                else:
                    if no_of_sources == 1:
                        key = self.reconData.keys()[0]
                        for derivedSource in derivedSources:
                            derivedSource_name = (key.lower() + "_g" + str(int(ruleGroup['groupIndex']))) + '_' + \
                                                 str(int(derivedSource['virtualSourceIndex']))
                            cols = eval(key.lower() + ".columns.tolist()")
                            cmd = derivedSource_name + " = pd.DataFrame(columns=" + repr(cols) + ")"
                            exec cmd
                    else:
                        for key in self.reconData.keys():
                            cols = eval(key.lower() + ".columns.tolist()")
                            cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + \
                                  " = pd.DataFrame(columns=" + repr(cols) + ")"
                            exec cmd
            for derived in derivedSrcList:
                print derived
                exec "unmatched = unmatched.append(" + derived['derivedSource'] + "_fil)"
            for col in derivedColList:
                if col in unmatched.columns:
                    del unmatched[col]
            if len(unmatched) > 0:
                if no_of_sources == 1:
                    for derivedSource in derivedSources:
                        if not derivedSource['derivedSourceIndicator']:
                            condition = ''
                            if derivedSource['sourceType'].lower() + '[' in derivedSource['conditions'][0]:
                                condition = derivedSource['conditions'][0].replace(
                                    derivedSource['sourceType'].lower() + '[', 'unmatched[')
                            # elif derivedSource['dfLabel'].lower() + '[' in derivedSource['conditions'][0]:
                            #     condition = derivedSource['conditions'][0].replace(
                            #     derivedSource['dfLabel'].lower() + '[', 'unmatched[')
                            else:
                                condition = derivedSource['conditions'][0].replace(
                                    (key.lower() + "_g" + str(int(ruleGroup['groupIndex']) - 1)) + '[', 'unmatched[')

                            print condition,'connd'
                            exec condition
                else:
                    for key in self.reconData.keys():
                        cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + \
                              " = unmatched[(unmatched['SOURCE_TYPE']=='" + key + "')"
                        cmd += "]"
                        print cmd
                        exec cmd
            else:
                if no_of_sources == 1:
                    key = self.reconData.keys()[0]
                    for derivedSource in derivedSources:
                        # For virtualSourceIndex new if changed
                        # derivedSource_name = ('s' + str(int(derivedSource['virtualSourceIndex']))
                        #                       + "_g" + str(int(ruleGroup['groupIndex'])))
                        derivedSource_name = (key.lower() + "_g" + str(int(ruleGroup['groupIndex']))) + '_' + \
                                             str(int(derivedSource['virtualSourceIndex']))
                        cols = eval(key.lower() + ".columns.tolist()")
                        cmd = derivedSource_name + " = pd.DataFrame(columns=" + repr(cols) + ")"
                        print cmd,'cmd'
                        exec cmd
                else:
                    for key in self.reconData.keys():
                        cols = eval(key.lower() + ".columns.tolist()")
                        cmd = key.lower() + "_g" + str(int(ruleGroup['groupIndex'])) + \
                              " = pd.DataFrame(columns=" + repr(cols) + ")"

                        exec cmd
        self.logger.info('Total matched = %6d' % (0 if matched is None else len(matched)))
        self.logger.info('Total unmatched = %6d' % (0 if unmatched is None else len(unmatched)))
        self.logger.info('----------------------------------------')
        # raise 'aina'
        return (matched, unmatched)

    ## Append static columns to be written to DB
    def dumpDataToDB(self):
        self.logger.info('Persisting transactions...')
        if self.unmatched is not None:
            if len(self.unmatched) == 0 or 'LINK_ID' not in self.unmatched.columns:
                self.unmatched['LINK_ID'] = ''
            self.unmatched['LINK_ID'] = self.unmatched['LINK_ID'].fillna('')
            self.unmatched.index = range(1, len(self.unmatched) + 1)
            objOID = str(uuid.uuid4().int & (1 << 64) - 1)
            self.unmatched['objOID'] = range(1, len(self.unmatched) + 1)
            condIdx = self.unmatched[(self.unmatched['LINK_ID'] == '')].index
            self.unmatched.loc[condIdx,'LINK_ID'] = objOID + self.unmatched['objOID'].astype(str)
            del self.unmatched['objOID']
            # self.unmatched['LINK_ID'] = self.unmatched['LINK_ID'].apply(
            #     lambda x: x if x else str(uuid.uuid4().int & (1 << 64) - 1))
            self.unmatched['MATCHING_STATUS'] = 'UNMATCHED'
            self.unmatched['TXN_MATCHING_STATUS'] = 'UNMATCHED'
            self.unmatched['TXN_MATCHING_STATE'] = 'SYSTEM_UNMATCHED'
        total = pd.concat([self.matched, self.unmatched])

        if total is not None and len(total) > 0:
            self.logger.info('adding extra columns')
            total['RECON_ID'] = str(self.reconId)
            total['BUSINESS_CONTEXT_ID'] = str(int(self.bizCtxtId))
            total['RECON_EXECUTION_ID'] = self.execId
            total['EXECUTION_DATE_TIME'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.dtcols.append('EXECUTION_DATE_TIME')
            self.dtcols.append('STATEMENT_DATE')
            total.index = range(1,len(total) + 1)
            total['cf_status'] = False
            # cf_status
            #     True: Fresh Records; Flase : Previous Day Records
            total.loc[total[(total['CARRY_FORWARD_FLAG'] == 'N') | (total['CARRY_FORWARD_FLAG'].isnull())
                            | (total['CARRY_FORWARD_FLAG'] == None)].index, 'cf_status'] = True
            total.loc[total[(total['cf_status'] == True)].index, 'STATEMENT_DATE'] = self.stmtDate

            self.logger.info('adding extra columns STATEMENT_DATE')
            # Old Way
            # total['STATEMENT_DATE'] = total.apply(lambda row: self.stmtDate if row['CARRY_FORWARD_FLAG'] is None
            #                                                                    or row['CARRY_FORWARD_FLAG'] == 'N' else
            # row['STATEMENT_DATE'], axis=1)
            objOID = str(uuid.uuid4().int & (1 << 64) - 1)

            # Old Way
            # total['OBJECT_OID'] = total['LINK_ID'].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
            # total['RAW_LINK'] = total.apply(self.getRawLinkId, axis=1)

            total['objOID'] = range(1,len(total) +1)
            total['OBJECT_OID'] = objOID + total['objOID'].astype(str)
            del total['objOID']

            objOID = str(uuid.uuid4().int & (1 << 64) - 1)
            total['objOID'] = range(1, len(total) + 1)
            total['objOID'] = objOID + total['objOID'].astype(str)

            for i in ['RAW_LINK','RECORD_VERSION']:
                if i not in total.columns:
                    total[i] = 0
            self.logger.info('adding extra columns RAW_LINK')
            total.loc[total[(total['cf_status'] == True)].index, 'RAW_LINK'] = total['objOID'][(total['cf_status'] == True)]
            self.logger.info('adding extra columns RAW_LINK')
            del total['objOID']

            # Column OBJECT_ID unwanted col
            # total['OBJECT_ID'] = total.apply(self.getObjectId, axis=1)

            # Old Way
            # total['RECORD_VERSION'] = total.apply(self.getRecVersion, axis=1)

            total.loc[total[(total['cf_status'] == True)].index, 'RECORD_VERSION'] = 0
            self.logger.info('adding extra columns RECORD_VERSION')
            # RECORD_VERSION 1: Fresh Txns; >1: Old Txn
            total['RECORD_VERSION'] = total['RECORD_VERSION'] + 1

            total['MATCHING_EXECUTION_MODE'] = 'AUTO'
            total['RECORD_STATUS'] = 'ACTIVE'
            total['CREATED_DATE'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            # self.dtcols.append('CREATED_DATE')
            total['CREATED_BY'] = 'SYSTEM'
            total['IPADDRESS'] = '0.0.0.0'
            total['UPDATED_DATE'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            total['UPDATED_BY'] = ''
            total['COMMENTS'] = ''
            total['AUTHORIZED_BY'] = ''


            if 'ACCOUNT_NUMBER' in total.columns:
                total['MIRROR_ACCOUNT_NUMBER'] = total['ACCOUNT_NUMBER']
            else:
                total['MIRROR_ACCOUNT_NUMBER'] = ''

            for col in self.dtcols:
                total[col] = pd.to_datetime(total[col], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                total[col] = total[col].fillna('')

            if int(self.reconDef['processingCount']) == 1:
                total['SOURCE_TYPE'] = 'S2'
                total.loc[total[total['DEBIT_CREDIT_INDICATOR'] == 'CR'].index, 'SOURCE_TYPE'] = 'S1'
                # total['SOURCE_TYPE'] = total['DEBIT_CREDIT_INDICATOR'].apply(lambda x: 'S1' if x == 'CR' else 'S2')
                srcName = self.reconDef['sources'][0]['sourceName']
                srcNameMap = { 'S1': srcName + '_CR' , 'S2': srcName + '_DR' }
                total['SOURCE_NAME'] = total['SOURCE_TYPE'].map(srcNameMap)
                self.logger.info('adding extra columns SOURCE_TYPE')

                # total['SOURCE_NAME'] = total['DEBIT_CREDIT_INDICATOR'] \
                #     .apply(lambda x: srcName + '_CR' if x == 'CR' else srcName + '_DR')
            else:
                srcNameMap = {}
                total['SOURCE_NAME'] = ''
                for value in self.reconDef['sources']:
                    srcNameMap[value['sourceType']] = value['sourceName']
                    # total.loc[total[]'SOURCE_NAME'] = ''
                total['SOURCE_NAME'] = total['SOURCE_TYPE'].map(srcNameMap)
                self.logger.info('adding extra columns SOURCE_NAME')


            if 'DEBIT_CREDIT_INDICATOR' in total.columns:
                total['DEBIT_CREDIT_INDICATOR1'] = 'D'
                total.loc[total[total['DEBIT_CREDIT_INDICATOR'] == 'CR'].index, 'DEBIT_CREDIT_INDICATOR1'] = 'C'
                total['DEBIT_CREDIT_INDICATOR'] = total['DEBIT_CREDIT_INDICATOR1']
                # total['DEBIT_CREDIT_INDICATOR'] = total['DEBIT_CREDIT_INDICATOR'].map({"CR": 'C', 'DR': 'D'})
                del total['DEBIT_CREDIT_INDICATOR1']
                self.logger.info('adding extra columns DEBIT_CREDIT_INDICATOR')

                # total['DEBIT_CREDIT_INDICATOR'] = total['DEBIT_CREDIT_INDICATOR'].apply(
                #     lambda x: 'C' if x == 'CR' else 'D')

            # Fill null amounts with zeros
            if 'AMOUNT' in total.columns:
                total['AMOUNT'] = total['AMOUNT'].fillna(0).apply(lambda x: x if x else 0)
            self.logger.info('adding extra columns DEBIT_CREDIT_INDICATOR')

            total = total.replace('None', '').replace('none', '').replace('NaN', '').replace('nan', '')
            self.logger.info('adding extra columns None')

            if 'PROBABLE' in total.columns:
                del total['PROBABLE']
            if 'cf_status' in total.columns:
                del total['cf_status']

            # print total.head()
            self.logger.info('end of adding status columns')
            colConvertors = []
            cols = []
            for source in self.reconDef['sources']:
                if 'useInMatching' in source and source['useInMatching']:
                    feedDef = self.utils.getFeedDefDSB(source['sourceType'], source['feedId'], self.jobSummary)
                    for col in feedDef['fieldDetails']:
                        if col['mdlFieldName'] not in cols and col['dataType'] in ['np.float64', 'np.int64']:
                            colConvertors.append({'mdlFieldName': col['mdlFieldName'], 'dataType': col['dataType']})
                        elif col['dataType'] in ['np.float64', 'np.int64']:
                            colConvertors.append({'mdlFieldName': col['mdlFieldName'], 'dataType': col['dataType']})
                            cols.append(col['mdlFieldName'])

            # total = self.updAccNames(total)
            for col in colConvertors:
                if col['mdlFieldName'] in total.columns:
                    if total[col['mdlFieldName']].dtypes not in [np.float64] and col['dataType'] == 'np.float64':
                        total[col['mdlFieldName']] = total[col['mdlFieldName']].apply(
                            lambda x: convertStringToFloat.formatStringToFloat(x))
                    if total[col['mdlFieldName']].dtypes not in [np.int64] and col['dataType'] == 'np.int64':
                        total[col['mdlFieldName']] = total[col['mdlFieldName']].apply(
                            lambda x: convertStringToFloat.formatStringToInt(x))

            # for col in ['LINK_ID', 'OBJECT_ID', 'RAW_LINK']:
            #     total[col] = total[col].astype(str)
            self.logger.info("End of column type formating")

            if 'TXN_MATCHING_STATUS' not in total.columns:
                total['TXN_MATCHING_STATUS'] = ''
            if 'RECON_EXECUTION_ID' in total.columns:
                total['RECON_EXECUTION_ID'] = total['RECON_EXECUTION_ID'].astype(int)
            if 'RECORD_VERSION' in total.columns:
                total['RECORD_VERSION'] = total['RECORD_VERSION'].astype(int)

            feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + self.reconId
            if not os.path.exists(feedPath + os.sep + 'output_dataframes/' + datetime.now().strftime(
                    "%d%B%Y")):
                os.makedirs(feedPath + os.sep + 'output_dataframes/' + datetime.now().strftime("%d%B%Y"))

            if 'STATEMENT_DATE' in total.columns:
                total.sort_values(by=['STATEMENT_DATE'],ascending=True,inplace=True)
            mandatoryCols, renameCols = self.utils.getColsToStore(self.reconId)
            if len(total) > 0:
                if len(renameCols.keys()) >0:
                    tempd = total.copy()
                    tempd.rename(columns=renameCols,inplace=True)
                    # sorted(tempd.columns)
                tempd[sorted(tempd.columns)].to_csv(feedPath + os.sep + 'output_dataframes/' + datetime.now().strftime(
                    "%d%B%Y") + os.sep + str(self.reconId) + '_' + datetime.now().strftime(
                    '%Y-%m-%d %H:%M:%S') + '.csv', index=False)
            self.logger.info("Started Saving to DB")

            c = self.dbOps.saveDFToHD5(total, 'TXN_DATA', reconId=self.reconId, execId=self.execId)
            self.dbOps.commit()
            self.logger.info("Saved in CASH_OUTPUT_TXN table")
            # self.updAccNames()
            self.logger.info(str(c) + ' records inserted')
        else:
            self.logger.info('No data to persist')
            # self.createPosition(self.positionData, total)
        return total

    ## Get existing record version or return default
    def getRecVersion(self, row):
        if row['CARRY_FORWARD_FLAG'] is None or row['CARRY_FORWARD_FLAG'] == 'N':
            return 1
        else:
            recver = row['RECORD_VERSION']
            return (recver + 1)

    ## Get existing record version or return default
    def getObjectId(self, row):
        if row['CARRY_FORWARD_FLAG'] is None or row['CARRY_FORWARD_FLAG'] == 'N':
            return str(uuid.uuid4().int & (1 << 64) - 1)
        else:
            return str(row['OBJECT_ID'])

    ## Get existing record raw_link or return default
    def getRawLinkId(self, row):
        if row['CARRY_FORWARD_FLAG'] is None or row['CARRY_FORWARD_FLAG'] == 'N':
            return str(uuid.uuid4().int & (1 << 64) - 1)
        else:
            return str(row['RAW_LINK'])

    ## Get existing record raw_link,record_version,ObjectId,Stmnt Date or return default
    def updateColumnData(self, row):
        row['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
        if row['CARRY_FORWARD_FLAG'] is None or row['CARRY_FORWARD_FLAG'] == 'N':
            row['RAW_LINK'] = str(uuid.uuid4().int & (1 << 64) - 1)
            row['OBJECT_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
            row['RECORD_VERSION'] = 1
            row['STATEMENT_DATE'] = self.stmtDate
        else:
            row['RECORD_VERSION'] = (row['RECORD_VERSION'].tolist()[0] + 1)
           # return str(row['RAW_LINK'].tolist()[0]), str(row['OBJECT_ID'].tolist()[0]), (row['RECORD_VERSION'].tolist()[0] + 1), row['STATEMENT_DATE'].tolist()[0]


	## Method to update account name to bgl account number 
    def updAccNames(self,total=None):
        #accMasterTable = self.dbOps.fetchFromDB(
        #    'select ACCOUNT_NAME,ACCOUNT_NUMBER from ACCOUNT_MASTER where RECORD_STATUS=\'ACTIVE\'')
        #accMasterTable.drop_duplicates(subset=['ACCOUNT_NUMBER'], inplace=True)
        #if accMasterTable is not None and len(accMasterTable) and 'ACCOUNT_NUMBER' in total.columns and 'ACCOUNT_NUMBER' in accMasterTable.columns:
        #    accMasDf = MatchFunctions().fuzzyProcess(total, accMasterTable, 'ACCOUNT_NUMBER', 'ACCOUNT_NUMBER',
        #                                             'ACCOUNT_NUMBER' + '_regx')
        #    accMasDf = accMasDf[accMasDf['ACCOUNT_NUMBER_regx'] != '$#$MISSING_DATA$#$']
        #    if len(accMasDf):
        #        del accMasDf['ACCOUNT_NUMBER']
        #        if 'ACCOUNT_NAME' in total.columns: del total['ACCOUNT_NAME']
        #        accMasDf.rename(columns={'ACCOUNT_NUMBER_regx': 'ACCOUNT_NUMBER'}, inplace=True)
        #        total = pd.merge(total, accMasDf, on=['ACCOUNT_NUMBER'], how='left')
        #return total

        accNameUpdQry = 'UPDATE cash_output_txn c ' + 'SET account_name=' + ' (SELECT unique account_name' + '  FROM account_master a' + "  WHERE RECORD_STATUS='ACTIVE'" + "  AND c.account_number like '%' || a.account_number || '%'" + '  AND rownum<=1) ' + 'WHERE c.account_number IS NOT NULL ' + 'AND EXISTS (SELECT account_name' + '  FROM account_master a' + "  WHERE RECORD_STATUS='ACTIVE'" + "  AND c.account_number like '%' || a.account_number || '%') " + 'AND c.recon_execution_id=' + str(
            self.execId)
        count = self.dbOps.executeUpdate(accNameUpdQry)
        self.logger.info('Account names set for ' + str(count) + ' transactions')

    ## Suryodaya Bank IMPS Reversal Rule on IMPS Swith
    def EPYMTS_61167(self, reconData):
        pass

