import pandas
import numpy
import sys
import re


def formatStringToFloat(x):
    if pandas.isnull(x):
        # print 'x', x
        return numpy.nan
    elif isinstance(x, float) or isinstance(x, int):
        return x

    else:
        x = re.sub('[^0-9\.\-]', '', x)
        r = "".join(re.split('\,', x))
        val = str(r).strip()
        # print  val
        if len(val) > 0:
            return numpy.float64(val)
        else:
            return numpy.nan

def formatStringToInt(x):
    if pandas.isnull(x):
        # print 'x', x
        return numpy.nan
    elif isinstance(x, float) or isinstance(x, int):
        return x

    else:
        x = re.sub('[^0-9\.\-]', '', x)
        r = "".join(re.split('\,', x))
        val = str(r).strip()
        # print  val
        if len(val) > 0:
            return numpy.int64(val)
        else:
            return numpy.nan