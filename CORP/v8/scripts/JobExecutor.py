# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

from PropertiesHelper import PropertiesHelper
from Utilities import Utilities
from FeedLoader import FeedLoader
from ReconMatching import ReconMatching
from PostJob import PostJob
from DBOps import DBOps

import logging
import uuid
import sys
import pandas
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler
import traceback
import shutil
import os
import config
import subprocess


class JobExecutor():
    ## Constructor
    def __init__(self, reconId, stmtDate):
        self.reconId = reconId

        self.strStmtDate = stmtDate
        # self.execId = str(uuid.uuid4().int & (1 << 64) - 1)
        self.utils = Utilities(None)
        self.execId = self.utils.generateReconExecutionID()
        self.dbOps = DBOps(reconId, self.execId)
        self.propsHelper = PropertiesHelper()
        self.postJob = PostJob()

        # Logging format
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] - %(message)s', datefmt='%m-%d-%Y %H:%M:%S')
        # Console logger
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(formatter)
        # File logger
        # loggerFileName = '../logs/pyrbe.log'
        loggerFileName = config.enginePath + 'logs' + os.sep + 'pyrbe_' + reconId + '.log'
        fh = TimedRotatingFileHandler(loggerFileName, when='D', interval=1)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        # Initialize logger
        self.logger = logging.getLogger('PYRBE')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(console)
        self.logger.addHandler(fh)

        pandas.options.mode.chained_assignment = None

    ## Run the executor
    def run(self):
        jobSummary = {}
        reconDef = self.utils.getReconDef(self.reconId, jobSummary)
        self.logger.info('----------------------------------------')
        self.logger.info('| Recon job execution started')
        self.logger.info('----------------------------------------')

        try:
            self.stmtDate = datetime.strptime(self.strStmtDate, '%d-%b-%Y')
        except:
            self.logger.error("Statement date provided is not in the required format 'DD-MMM-YYYY'")
            self.logger.info('----------------------------------------')
            self.logger.info('Recon job execution ABORTED')
            self.logger.info('----------------------------------------')
            exit(0)
        if reconDef is None:
            self.utils.insertExecLog(0000, self.reconId, self.stmtDate, self.execId, 'Definition not found'
                                     , 'Definition not found')
            self.logger.error("Definition not found for given recon ID - '" + self.reconId + "'")
            self.logger.info('----------------------------------------')
            self.logger.info('Recon job execution ABORTED')
            self.logger.info('----------------------------------------')
            exit(0)

        if reconDef['reconId'] == 'CB_CASH_APAC_61171':
            scriptPath = self.propsHelper.getProperty('CUSTOM_SCRIPTS_PATH') + os.sep + 'ParseInputs.py'
            dstr = self.stmtDate.strftime('%d%m%Y')
            mftPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + 'CORP' + os.sep + dstr + os.sep
            print mftPath
            command = '/usr/share/nginx/www/erecon/flask/erecon/bin/python2', scriptPath, mftPath, self.strStmtDate
            subprocess.call(command)
        self.bizCtxtId = reconDef['businessContextId']
        singleSideUpload = False
        if 'singleSideUpload' in reconDef and reconDef['singleSideUpload']: singleSideUpload = True

        # Move configuration to UI(check the flag in reconbuilder)
        # if self.reconId not in config.incrementalRecons:
        #    self.utils.getLatestExecDetails(self.reconId, self.stmtDate, self.execId, self.bizCtxtId)
        if 'incrementalRecon' not in reconDef or not reconDef['incrementalRecon']:
            self.utils.getLatestExecDetails(self.reconId, self.stmtDate, self.execId, self.bizCtxtId)

        self.logger.info('Recon ID = ' + str(self.reconId))
        self.logger.info('Recon Name = ' + reconDef['reconName'])
        self.logger.info('Statement Date = ' + self.stmtDate.strftime('%d-%b-%Y'))
        self.logger.info('Execution ID = ' + self.execId)
        self.logger.info('----------------------------------------')

        # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'REC_EXEC_PROG')
        self.utils.addDetailsToSummary(jobSummary, 'reconId', self.reconId)
        self.utils.addDetailsToSummary(jobSummary, 'reconName', reconDef['reconName'])
        self.utils.addDetailsToSummary(jobSummary, 'stmtDate', self.stmtDate.strftime('%d-%b-%Y'))
        self.utils.addDetailsToSummary(jobSummary, 'execDate', datetime.now().strftime('%d-%b-%Y'))
        self.utils.addDetailsToSummary(jobSummary, 'execId', self.execId)
        # Load feed data
        self.utils.addDetailsToSummary(jobSummary, 'jobStart', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
        if self.reconId in config.eventLogRecons:
            # status, self.prevExecID = self.utils.getCalendarEvent(self.reconId, self.stmtDate)
            # if not status:
            #     self.utils.insertExecLog(0000, self.reconId, self.stmtDate, self.execId, 'Definition not found'
            #                              , "Previous date data doesn't exits")
            #     self.logger.error("Previous date data doesn't exits")
            #     self.logger.info('Recon job execution ABORTED')
            #     self.utils.addDetailsToSummary(jobSummary, 'jobEnd', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
            #     self.utils.addDetailsToSummary(jobSummary, 'jobStatus', 'FAILURE')
            #     self.utils.addDetailsToSummary(jobSummary, 'failureReason', "The previous date data doesn't exits")
            #     self.utils.dumpJobSummary(jobSummary)
            #     self.logger.info('----------------------------------------')
            #     exit(0)
            pass
        try:
            feedLoader = FeedLoader(reconDef, self.stmtDate, self.execId, jobSummary, self.dbOps,
                                    singleSideUpload=singleSideUpload)
            reconMatch = ReconMatching(reconDef, self.stmtDate, self.execId, jobSummary, self.dbOps)

            (reconData, nonPartData) = feedLoader.run()
            reconMatch.run(reconData, nonPartData)
            # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'REC_EXEC_OK')

            # sys.exit(0)
            self.dbOps.commit()
            self.utils.addDetailsToSummary(jobSummary, 'jobEnd', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
            jobStatus = 'SUCCESS'
            self.utils.addDetailsToSummary(jobSummary, 'jobStatus', jobStatus)
            self.utils.dumpJobSummary(jobSummary)
            self.postJob.sendSummaryMail(jobSummary)
            # self.postJob.moveToProcessed(feedLoader.getFilesToMove())
        except BaseException, e:
            self.logger.error(e)
            jobStatus = 'FAILED'
            self.logger.info('Updates rolled back')
            self.dbOps.rollback()
            self.utils.addDetailsToSummary(jobSummary, 'jobStatus', jobStatus)
            self.utils.addDetailsToSummary(jobSummary, 'failureReason', str(e))
            self.utils.dumpJobSummary(jobSummary)

        except Exception, e:
            self.logger.error(e)
            self.logger.error(traceback.print_exc())
            self.logger.info('Updates rolled back')
            self.dbOps.rollback()
            jobStatus = 'FAILED'
            self.utils.addDetailsToSummary(jobSummary, 'jobStatus', jobStatus)
            self.utils.addDetailsToSummary(jobSummary, 'failureReason', str(e))
            self.utils.dumpJobSummary(jobSummary)
        self.logger.info('----------------------------------------')
        self.logger.info('Recon job execution ' + jobStatus)
        self.logger.info('----------------------------------------')


if __name__ == '__main__':
    # reconJob = JobExecutor("123456789", '15-MAR-2016')
    # reconJob = JobExecutor("123654790", '21-JUN-2016')
    reconJob = JobExecutor(sys.argv[1], sys.argv[2])
    reconJob.run()
