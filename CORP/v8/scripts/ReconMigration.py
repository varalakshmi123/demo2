from DBOps import DBOps
import sys
import xmltodict
import json
from collections import OrderedDict


class ReconMigration():

    def __init__(self, reconId):
        self.dbOps = DBOps()
        self.reconId = reconId

    def migrate(self):
        feedDefs = []
        qry = "select feed_name, format_type, delimiter_type, column_header_exists, " \
              "positions_feed, top_skip_lines, bottom_skip_lines, swift_msg_type, feed_id " \
              "from feed_details " \
              "where source_id in (select source_id from source_details " \
              "where source_recon_id='" + self.reconId + "' " \
              "and record_status='ACTIVE') and record_status='ACTIVE'"
        # print qry
        cur = self.dbOps.executeQuery(qry)
        for row in cur.fetchall():
            feedDef = {}
            feedDef['feedName'] = row[0]
            feedDef['format'] = row[1]
            feedDef['delimiter'] = '' if row[2] is None else row[2]
            feedDef['headerExists'] = False if row[3] is None or int(row[3]) == 0 else True
            feedDef['positionsFeed'] = False if row[4] is None or int(row[4]) == 0 else True
            feedDef['skipTopRows'] = 0 if row[5] is None else int(row[5])
            feedDef['skipBottomRows'] = 0 if row[6] is None else int(row[6])
            feedDef['swiftType'] = '' if row[7] is None else row[7]
            feedDef['feedId'] = row[8]
            feedDef['filters'] = []
            feedDefs.append(feedDef)
        jcFile = './recon_conf/' + self.reconId + '/job_config.xml'
        with open(jcFile) as f:
            jobConf = xmltodict.parse(f.read())
        jobFeeds = []
        if isinstance(jobConf['Job']['Parameters']['JobFeed'], dict):
            jobFeeds.append(jobConf['Job']['Parameters']['JobFeed'])
        else:
            jobFeeds = jobConf['Job']['Parameters']['JobFeed']
        for jobFeed in jobFeeds:
            for feedDef in feedDefs:
                if feedDef['feedId'] == jobFeed['@feedID']:
                    feedDef['feedPath'] = jobFeed['Path']
                    pattern = jobFeed['FileNamePattern']
                    pattern = pattern.replace('dd', '%d').replace('MMM','%b').replace('MM','%m').replace('yyyy','%Y').replace('yy','%y')
                    feedDef['fileNamePattern'] = pattern
                    preload = {}
                    preloadInd = False
                    if 'ALGO-GL-OUTFILE' in pattern or 'BGL-OUTFILE' in pattern:
                        if feedDef['positionsFeed']:
                            preload['scriptFile'] = 'bgl_dump_bal_splitter.sh'
                            preload['splitSuffix'] = 'P'
                        else:
                            preload['scriptFile'] = 'bgl_dump_txn_splitter.sh'
                            preload['splitSuffix'] = 'T'
                        preloadInd = True
                    cmd = ""
                    if 'SplitCommand' in jobFeed.keys():
                        cmd = jobFeed['SplitCommand']
                    elif 'SplitCondition' in jobFeed.keys():
                        cmd = jobFeed['SplitCondition']
                    if '.sh' in cmd:
                        preload['scriptFile'] = cmd[cmd.rfind('/') + 1:cmd.index('.sh') + 3]
                        preload['splitSuffix'] = feedDef['feedId']
                        preloadInd = True
                    else:
                        feedDef['filters'] = [cmd]
                        feedDef['preLoadScript'] = preload
                    feedDef['preLoadScriptIndicator'] = preloadInd
                    feedDef['limitFileCount'] = 1
                    schemaFile = './recon_conf/' + self.reconId + '/' + feedDef['feedId'] + '_schema.json'
                    with open(schemaFile) as f:
                        schema = json.load(f)
                    fldDetails = []
                    pairs = schema['feed-struct']['pairs']
                    fields = None
                    for pair in pairs:
                        if feedDef['positionsFeed'] and pair['type'] == 'HEADER':
                            fields = pair['pair']
                        if not feedDef['positionsFeed'] and pair['type'] == 'TRANSACTION':
                            fields = pair['pair']
                    for fld in fields:
                        field = {}
                        field['mdlFieldName'] = fld['mdl-fld']
                        field['displayName'] = fld['fld-name']
                        field['dataType'] = fld['data-type']
                        field['position'] = fld['position']
                        field['startIndex'] = fld['start-pos']
                        field['endIndex'] = fld['end-pos']
                        method = '' if isinstance(fld['method'], dict) else fld['method']
                        field['tag'] = method
                        field['type'] = 'M' if fld['mandatory'] else 'D'
                        pattern = '' if isinstance(fld['ts-pattern'], dict) else fld['ts-pattern']
                        field['datePattern'] = pattern.replace('dd', '%d').replace('MM', '%m').replace('yyyy',
                                                                                                       '%Y').replace(
                            'yy', '%y')
                        fldDetails.append(field)
                    feedDef['fieldDetails'] = fldDetails
        print json.dumps(feedDefs, indent=4)


if __name__ == '__main__':
    rm = ReconMigration(sys.argv[1])
    rm.migrate()
