# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

from pymongo import MongoClient
from bson.objectid import ObjectId
from PropertiesHelper import PropertiesHelper

# from DBOps import DBOps
import logging
import uuid
import pandas as pd
import numpy as np
import re
from datetime import datetime
import config
import time
from ZFSApi import ZFSApi
from datetime import timedelta


class Utilities():
    ## Constructor
    def __init__(self, dbOps=None):
        self.propsHelper = PropertiesHelper()
        self.mongo_client = MongoClient('mongodb://' + self.propsHelper.getProperty('MONGODB_HOST') + ':27017')
        self.mongo_db = self.mongo_client[self.propsHelper.getProperty('MONGODB_NAME')]
        self.mongo_db_app = self.mongo_client[self.propsHelper.getProperty('MONGODB_NAME_APP')]
        self.rdCollName = self.propsHelper.getProperty('RECONDEF_COLL_NAME')
        self.fdCollName = self.propsHelper.getProperty('FEEDDEF_COLL_NAME')
        self.amCollName = self.propsHelper.getProperty('ACCMAP_COLL_NAME')
        self.pmatchCollName = self.propsHelper.getProperty('POST_MATCH_COLL_NAME')
        self.custFnsCollName = self.propsHelper.getProperty('CUSTOM_FUNCS_COLL_NAME')
        self.execLogCollName = self.propsHelper.getProperty('RECON_EXECUTION_DETAILS_LOG_COLL_NAME')
        self.dbServerName = self.propsHelper.getProperty('DB_SERVER_NAME')
        self.logger = logging.getLogger('PYRBE')
        self.dbOps = dbOps

    ## Get recon definition from MongoDB
    def getReconDef(self, reconId, jobSummary):
        reconDef = list(self.mongo_db[self.rdCollName].find({"reconId": reconId}))[0]
        if reconDef is not None:
            sources = reconDef['sources']
            summarySrcs = {}
            for src in sources:
                source = {}
                source['sourceName'] = src['sourceName']
                source['feeds'] = []
                summarySrcs[src['sourceType']] = source
            jobSummary['sources'] = summarySrcs
        return reconDef

    ## Get feed definition from MongoDB
    def getFeedDef(self, sourceType, feedId, jobSummary):
        feedDef = self.mongo_db[self.fdCollName].find_one({"_id": ObjectId(feedId)})
        if feedDef is not None:
            sources = jobSummary['sources']
            if sources is None:
                source = {}
            else:
                source = sources[sourceType]
            feed = {}
            feed['feedId'] = feedDef['_id']
            feed['feedName'] = feedDef['feedName']
            source['feeds'].append(feed)
        return feedDef

    ## Get feed definition from MongoDB
    def getFeedDefDSB(self, sourceT, feedId, jobD):
        feedDef = self.mongo_db[self.fdCollName].find_one({"_id": ObjectId(feedId)})
        return feedDef

    ## Get column names as array from feed structure
    def getColNames(self, feedDef):
        colNames = []
        lines = feedDef['fieldDetails']
        feedDef['fieldDetails'] = sorted(feedDef['fieldDetails'], key=lambda k: k.get('position', 1),
                                         reverse=False)
        for col in feedDef['fieldDetails']:
            colNames.append(col['mdlFieldName'])
        return colNames


    ## Get file column names as array from feed structure
    def getFileColNames(self, feedDef):
        colNames = []
        lines = feedDef['fieldDetails']
        feedDef['fieldDetails'] = sorted(feedDef['fieldDetails'], key=lambda k: k.get('position', 1),
                                         reverse=False)
        for col in feedDef['fieldDetails']:
            colNames.append(col['displayName'])
        return colNames

    ## Get column positions
    def getColPositions(self, feedDef):
        colPositions = []
        for col in feedDef['fieldDetails']:
            colPositions.append(int(col['position']) - 1)
        colPositions = sorted(colPositions)
        return colPositions

    ## Get column data types as array from feed structure
    def getColTypes(self, feedDef):
        colTypes = []
        for col in feedDef['fieldDetails']:
            colTypes.append(col['dataType'])
        return colTypes

    ## Get formats of date columns
    def getDatePatterns(self, feedDef):
        formats = {}
        for col in feedDef['fieldDetails']:
            if col['dataType'] == 'np.datetime64':
                # print col
                formats[col['mdlFieldName']] = col['datePattern']
        return formats

    ## Get fixed length start and end
    def getFLPositions(self, feedDef):
        positions = []
        for col in feedDef['fieldDetails']:
            start = int(col['startIndex']) - 1
            end = int(col['endIndex'])
            positions.append((start, end))
        return positions

    ## Get Swift tags
    def getSwiftTags(self, feedDef):
        tags = []
        for col in feedDef['fieldDetails']:
            tag = col['tag']
            tags.append(tag)
        return tags

    ## Get feed accounts mapping
    def getFeedAccMap(self, reconId, feedId):
        # return self.mongo_db[self.amCollName].find_one({"reconContext.reconId": reconId, "feedDetails.feedId": {"$in":[feedId]}})
        mapping = self.mongo_db[self.amCollName].find_one({"reconContext.reconId": reconId})
        if mapping != None and 'feedDetails' in mapping:
            feedDet = mapping['feedDetails']
            if isinstance(feedDet, list):
                for feed in feedDet:
                    if feed['feedId'] == str(feedId):
                        return mapping['accounts']
            else:
                feed = feedDet
                if feed['feedId'] == str(feedId):
                    return mapping['accounts']
        return None

    ## Check if any custom prematch functions defined for the recon
    def getPrematch(self, reconId):
        return self.mongo_db[self.amCollName].find_one({"reconId": reconId, "processType": "PRE_MATCH_PROCESS"})

    ## Check if any custom prematch functions defined for the recon
    def getPostmatch(self, reconId):
        return self.mongo_db[self.pmatchCollName].find_one({"reconId": reconId, "processType": "POST_MATCH_PROCESS"})

    ## Add loaded file details to summary
    def addFeedSummary(self, jobSummary, sourceType, feedId, files,
                       splitApplied, filterApplied, useInMatching,
                       rawCount, filteredCount):
        sourceFeeds = jobSummary['sources'][sourceType]['feeds']
        sourceFeed = None
        for sf in sourceFeeds:
            if str(sf['feedId']) == str(feedId):
                sourceFeed = sf
                break
        sourceFeed['feedFiles'] = files
        sourceFeed['splitApplied'] = 'Yes' if splitApplied else 'No'
        sourceFeed['filterApplied'] = 'Yes' if filterApplied else 'No'
        sourceFeed['usedInMatching'] = 'Yes' if useInMatching else 'No'
        sourceFeed['rawInputCount'] = str(rawCount)
        sourceFeed['filteredCount'] = str(filteredCount)

    ## Add details to job summary
    def addDetailsToSummary(self, jobSummary, field, value):
        jobSummary[field] = str(value)

    ## Dump job summary to MongoDB
    def dumpJobSummary(self, jobSummary):
        jobSummary['created'] = self.getUtcTime()
        jobSummary['updated'] = self.getUtcTime()
        jobSummary['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
        jobSummary['partnerId'] = "54619c820b1c8b1ff0166dfc"
        jobSummary[
            'apiKey'] = "Mjg0YTUwYjg0MjUwNGM5MDJkNzBmZGRmZjM5YTI5ZTE1OTcxMWM4ZWM4NzdiM2Y4MTE1MjdhNWM1ZmQ1MmQzMg=="
        jscoll = self.mongo_db['jobSummary']
        jscoll.save(jobSummary)

    ## Insert/update DB log entries based on execution progress
    def insertExecLog(self, bizCtxtId, reconId, stmtDate, execId, state, status, matchedCnt=0, unMatchedCnt=0,
                      carryFwdCnt=0, postmatchIgnored=0, AUTHORIZATION_PENDING=0,
                      ROLLBACK_AUTHORIZATION_PENDING=0, FORCE_MATCHED=0, TXN_CLS_COUNT_2DAY=0, TXN_CLS_AMOUNT_2DAY=0):
        prevState = ''
        prevStatus = ''
        execState = 'Started'
        readSnapShot = ''
        readClonePath = ''
        version = 1
        if state == 'Definition not found' and status == 'Definition not found':
            prevState = 'Definition not found'
            prevStatus = 'Definition not found'
            version = 0
        if state == 'Statement date already exists' and status == 'Statement date already exists':
            prevState = 'Statement date already exists'
            prevStatus = 'Statement date already exists'
            version = 0
        if state == 'Feed Watching' and status=='Waiting For Feeds':
            prevState = 'Feed Watching'
            prevStatus = 'Waiting For Feeds'
            version = 1
        elif state == 'Feed Readiness' and status == 'Feeds Arrived':
            prevState = 'Feed Watching'
            prevStatus = 'Waiting For Feeds'
            version = 2
        elif state == 'Feed Readiness' and status == 'Feeds Not Arrived':
            prevState = 'Feed Readiness'
            prevStatus = 'Waiting For Feeds'
            version = 2
        elif state == 'Feed Loading' and status == 'Feeds Loading Initiated':
            prevState = 'Feed Readiness'
            prevStatus = 'Feeds Arrived'
            version = 3
        elif state == 'Feed Loading' and status == 'Feeds Loading Completed':
            prevState = 'Feed Loading'
            prevStatus = 'Feeds Loading Initiated'
            version = 4
        elif state == 'Feed Loading' and status == 'Feeds Loading Failed':
            prevState = 'Feed Loading'
            prevStatus = 'Feeds Loading Initiated'
            version = 4
        elif state == 'Matching' and status == 'Matching Initiated':
            prevState = 'Feed Loading'
            prevStatus = 'Feeds Loading Completed'
            version = 5
        elif state == 'Matching' and status == 'Matching Completed':
            prevState = 'Matching'
            prevStatus = 'Matching Initiated'
            execState = 'Completed'
            readSnapShot = self.create_snap_shot(str(reconId), str(execId))
            readClonePath = self.clone_snap_shot(readSnapShot)
            version = 6
        elif state == 'Matching' and status == 'Matching Failed':
            prevState = 'Matching'
            prevStatus = 'Matching Initiated'
            execState = 'Completed'
            version = 6
        else:
            prevState = status
            prevStatus = status
            execState = execState
            version = 6

        execDoc = {'BUSINESS_CONTEXT_ID': str(int(bizCtxtId)), 'RECON_ID': str(reconId),
                   'RECON_EXECUTION_ID': str(execId),
                   'STATEMENT_DATE': stmtDate, 'CREATED_DATE': datetime.now(), 'EXECUTION_DATE': datetime.now(),
                   'RECORD_STATUS': 'ACTIVE', 'RECORD_VERSION': str(version),
                   'CREATED_BY': 'SYSTEM', 'IPADDRESS': '0.0.0.0', 'EXECUTION_STATUS': execState,
                   'PROCESSING_STATE': state,
                   'PROCESSING_STATE_STATUS': status, 'RECON_EXECUTION_MODE': 'AUTO',
                   'RECON_EXECUTION_OID': str(uuid.uuid4().int & (1 << 64) - 1),
                   'EXCEPTION_RECORDS_COUNT': str(unMatchedCnt),
                   'PERFECT_MATCHED_RECORDS_COUNT': str(matchedCnt), 'CARRYFORWARD_COUNT': str(carryFwdCnt),
                   'POSTMATCH_IGNORE_COUNT': str(postmatchIgnored),
                   'FORCE_MATCHED': str(FORCE_MATCHED),
                   'AUTHORIZATION_PENDING': str(AUTHORIZATION_PENDING),
                   'ROLLBACK_AUTHORIZATION_PENDING': str(ROLLBACK_AUTHORIZATION_PENDING),
                   'TXN_CLS_COUNT_2DAY': str(TXN_CLS_COUNT_2DAY),
                   'TXN_CLS_AMOUNT_2DAY': str(TXN_CLS_AMOUNT_2DAY),
                   'READ_SNAP_SHOT': str(readSnapShot),
                   'READ_CLONE_PATH': str(readClonePath),
                   'machineId': "16af9057-69af-4c69-bbcf-70d99334e027",
                   'partnerId': "54619c820b1c8b1ff0166dfc",
                   'apiKey': "Mjg0YTUwYjg0MjUwNGM5MDJkNzBmZGRmZjM5YTI5ZTE1OTcxMWM4ZWM4NzdiM2Y4MTE1MjdhNWM1ZmQ1MmQzMg=="
                   }
        self.mongo_db_app[self.execLogCollName].save(execDoc)

        updLog = self.mongo_db_app[self.execLogCollName].find(
            {'RECON_EXECUTION_ID': str(execId), 'PROCESSING_STATE': prevState, 'PROCESSING_STATE_STATUS': prevStatus})
        for rec in list(updLog):
            rec['RECORD_STATUS'] = 'INACTIVE'
            rec['UPDATED_BY'] = 'SYSTEM'
            rec['CREATED_DATE'] = datetime.now()
            rec['PROCESSING_STATE_DATE_TIME'] = datetime.now()
            self.mongo_db_app[self.execLogCollName].update({'_id': ObjectId(rec['_id'])}, rec)

    ## Method to create clone for ZFS Snap Shot for read
    def create_snap_shot(self, recon_id, exec_id):
        snapPath = ZFSApi().snapshot({'snapPath': recon_id + '/' + exec_id})
        return snapPath

    def clone_snap_shot(self, snapPath):
        clonePath = snapPath.replace('@', '_') + '_' + 'clone'
        ZFSApi().cloneSnapshot(0, snapPath, clonePath)
        return clonePath

    ## Method to generate alert
    def createAlert(self, bizCtxtId, reconId, stmtDate, execId, event):
        qry = "select alert_message, alert_code from alert_master where event_type='" + event + "'"
        # cur = self.dbOps.executeQuery(qry)
        # row = cur.fetchone()
        row = ['', '']
        msg = row[0]
        code = row[1]
        if msg is not None and msg != '':
            msg = msg.replace('{recon_id}', reconId)
            msg = msg.replace('{recon_execution_id}', str(execId))
            qry = "insert into alert_transaction(" \
                  "alert_code, alert_message, alert_status, " \
                  "alert_txn_id, alert_txn_oid, business_context_id, " \
                  "alert_txn_business_id_list, alert_txn_business_type, " \
                  "created_by, created_date, ipaddress, recon_id, " \
                  "recon_execution_id, record_status, record_version, " \
                  "statement_date, execution_date_time) values(" + \
                  "'" + str(code) + "', '" + msg + "', 'OPEN', " \
                                                   "alert_transaction_id_seq.nextval, " \
                                                   "alert_transaction_oid_seq.nextval, " \
                                                   "'" + str(bizCtxtId) + "', " \
                                                                          "'[" + reconId + "]', 'EXECUTION', 'SYSTEM', current_timestamp, " + \
                  "'0.0.0.0', '" + reconId + "', " + str(execId) + ", " \
                                                                   "'ACTIVE', 1, to_timestamp('" + \
                  stmtDate.strftime('%m-%d-%Y') + \
                  "', 'DD-MM-YYYY'), to_timestamp('" + \
                  stmtDate.strftime('%m-%d-%Y') + \
                  "', 'DD-MM-YYYY'))"
            # self.dbOps.executeUpdate(qry)

    ## Methdod to get UTC time
    def getUtcTime(self, convertDate=None, resetSeconds=False):
        if convertDate is None:
            convertDate = datetime.utcnow()
        if resetSeconds:
            convertDate = convertDate.replace(second=0)

        ts = int(datetime.now().replace(second=0).strftime("%s"))
        utc_offset = datetime.fromtimestamp(ts) - datetime.utcfromtimestamp(ts)
        return int(convertDate.now().strftime("%s")) - utc_offset.seconds

    def formatStringToFloat(self, x):
        if pd.isnull(x):
            # print 'x', x
            return np.nan
        elif isinstance(x, float) or isinstance(x, int):
            return x

        else:
            x = re.sub('[^0-9\.\-]', '', x)
            r = "".join(re.split('\,', x))
            val = str(r).strip()
            # print  val
            if len(val) > 0:
                return np.float64(val)
            else:
                return np.nan

     # Genearate Recon ExecutionID
    def generateReconExecutionID(self):
        sequenceGenObj = self.mongo_db["sequenceGenerator"].find_one({"seqName": "reconExecutionSeq"})
        executionId = int(sequenceGenObj['seq'])
        self.mongo_db["sequenceGenerator"].update({"seqName": "reconExecutionSeq"},
                                                  {"$set": {"seq": executionId + 1}})

        return str(executionId)

    # Validation to avoid upload to duplicate statement date files
    # Get last successful execution for this recon
    def getLatestExecDetails(self, reconID, statementDate, execId, bizCtxtId):
        doc = self.mongo_db_app[self.execLogCollName].find({'RECON_ID': reconID, 'EXECUTION_STATUS': 'Completed',
                                                            'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                            'RECORD_STATUS': 'ACTIVE', 'STATEMENT_DATE': statementDate
                                                            }).sort([('RECON_EXECUTION_ID', -1)])
        if len(list(doc)) > 0:
            self.insertExecLog(bizCtxtId, reconID, statementDate, execId, 'Statement date already exists'
                               , 'Statement date already exists')
            self.logger.error('Statement date already exists in the system')
            self.logger.info('----------------------------------------')
            self.logger.info('| Recon job execution ' + 'ABORTED')
            self.logger.info('----------------------------------------')
            exit(0)

    # Get Columns to store in HDFS file
    def getColsToStore(self, reconID):
        doc = self.mongo_db_app['dynamic_data_setup'].find({'RECON_ID': reconID})
        cols = []
        doc = list(doc)
        if doc is not None and len(doc):
            df = pd.DataFrame(doc)
            if 'COL_STATUS' in df.columns:
                df = df.loc[df[df['COL_STATUS'] == 'ACTIVE'].index]
            
            cols = df['MDL_FIELD_ID'].unique().tolist()


        return cols + config.system_columns, dict(zip(df['MDL_FIELD_ID'], df['UI_DISPLAY_NAME']))

    def getPreviousExecutionId(self, reconId):
        doc = self.mongo_db_app[self.execLogCollName].find({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                                            'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                            'RECORD_STATUS': 'ACTIVE'
                                                            }).sort([('RECON_EXECUTION_ID', -1)])
        doc = list(doc)
        if doc is not None and len(doc):
            doc = doc[0]
        if len(doc) == 0:
            doc = None
        return doc

    def getCalendarEvent(self, reconId, stmtDT):
        count = 1

        calendarCol = None
        while True:
            qrystmtDT = datetime.strftime(stmtDT - timedelta(count), '%Y-%m-%d')
            calendarCol = list(self.mongo_db_app['event_logs'].find({'reconID':{'$in': [reconId]},'start': qrystmtDT}))
            if len(calendarCol) > 0:
                count += 1
            else:
                doc = list(self.mongo_db_app[self.execLogCollName].find(
                    {'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                     'PROCESSING_STATE_STATUS': 'Matching Completed',
                     'RECORD_STATUS': 'ACTIVE', 'STATEMENT_DATE': stmtDT - timedelta(count)
                     }).sort([('RECON_EXECUTION_ID', -1)]))
                if len(doc) > 0:
                    return True,doc[0]['RECON_EXECUTION_ID']
                else:
                    return False,''