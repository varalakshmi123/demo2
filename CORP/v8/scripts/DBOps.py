# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

from collections import OrderedDict
from PropertiesHelper import PropertiesHelper
import pandas as pd
import logging
import numpy
import datetime
import time
from pandas import HDFStore
import config
import os
from os import sep
import traceback
import warnings
from mdfieldSizes import modelFieldColSize as colSizes
from ZFSApi import ZFSApi
import tables
warnings.filterwarnings('ignore', category=pd.io.pytables.PerformanceWarning)


# warnings.filterwarnings('ignore',category=pd.io.pytables.NaturalNameWarning)
class DBOps():

    ## Constructor
    def __init__(self, reconId, execId):
        self.reconId = reconId
        self.execId = str(execId)
        self.propsHelper = PropertiesHelper()
        self.logger = logging.getLogger('PYRBE')
        self.hdfStoreName = None
        self.bckupTme = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        self.snpshotDir = self.reconId + '/' + self.execId
        self.hdfInit()
        self.logger.info(self.propsHelper.getProperty("DB_USER"))

    def hdfInit(self):
        self.hdfStoreName = config.hdfFilesPathNew + self.reconId + '/' + self.execId + '/' + self.execId + '.h5'
        if not os.path.exists(config.hdfFilesPathNew + self.reconId):
            ZFSApi().create(self.reconId)
        if not os.path.exists(config.hdfFilesPathNew + self.snpshotDir):
            ZFSApi().create(self.snpshotDir)
            # os.makedirs(config.hdfFilesPathNew + self.reconId)
        if (os.path.exists(self.hdfStoreName)):
            os.system('chmod -R 777 ' + config.hdfFilesPathNew)
            try:
                self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
                self.hdf.close()
                self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
            except Exception, e:
                print(traceback.format_exc())
                os.remove(self.hdfStoreName)
                self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
                self.hdf.close()
                self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
        else:
            self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
            # self.hdf.flush()
            self.hdf.close()
            self.hdf = HDFStore(self.hdfStoreName, complevel=9, complib='blosc', append=True)
        os.system('chmod -R 777 ' + config.hdfFilesPathNew)
        # print self.hdf.keys()

    ## Method to get DB connection instance
    def getConn(self):
        return self.hdf

    ## Method to get cursor
    def getCursor(self):
        return self.hdf.cursor

    ## Method to commit DB changes
    def commit(self):
        self.hdf.close()

    ## Method to rollback DB changes
    def rollback(self):
        try:
            self.hdf.close()
        except Exception, e:
            pass
        ZFSApi().delete(self.snpshotDir)
        # os.system('rm ' + self.hdfStoreName)

    def saveDFToHD5(self, df, table_name, reconId=None, execId=None, minItemSize=None):
        started = datetime.datetime.fromtimestamp(time.time())
        self.execId = execId
        count = 0
        if df is not None and len(df) > 0:
            try:
                df.index = range(1, len(df) + 1)
                for col in df.columns:
                    if df[col].dtypes in [object, str]:
                        # if 'DATE' in col or 'TIMESTAMP' in col or 'PERIOD' in col or 'TS_6' in col:
                        #     df.loc[df[df[col] == ''].index, col] = numpy.nan
                        #     # df[col] = df[col].where(pd.notnull(df[col]), None)
                        #     pass
                        # else:
                        df[col] = df[col].astype(str)
                        df[col].fillna('', inplace=True)
                    elif df[col].dtypes in [numpy.int64, numpy.float64]:
                        df[col] = df[col].fillna(0)
                self.hdfStorePath = table_name
                self.logger.info('Store Key ' + self.hdfStorePath)
                df.index = range(1, len(df) + 1)
                colsize = {}
                for col in df.columns:
                    if str(col) in colSizes.keys():
                        colsize[col] = colSizes[col]
                    else:
                        del df[col]
                if 'LINK_ID' in df.columns:
                    df.sort_values(by=['LINK_ID'], ascending=True, inplace=True)
                df = df[sorted(colsize.keys())]
                g = df.groupby(numpy.arange(len(df)) / 10000)
                queryCols = ['OBJECT_OID', 'LINK_ID', 'RECORD_STATUS', 'UPDATED_DATE', 'UPDATED_BY']
                for _, grp in g:
                    # grpUnMatched = grp.loc[grp[grp['MATCHING_STATUS'] == 'UNMATCHED'].index].copy()
                    # grpMatched = grp.loc[grp[grp['MATCHING_STATUS'] == 'MATCHED'].index].copy()
                    grp.to_hdf(self.hdfStoreName, self.hdfStorePath , append=True, mode='a',
                                        min_itemsize=colsize)  # Best Approach But UI query cannot happen (data_columns=queryCols)
                    # grpMatched.to_hdf(self.hdfStoreName, self.hdfStorePath + '/MATCHED', append=True, mode='a',
                    #                   min_itemsize=colsize)  # Best Approach But UI query cannot happen
                    # grp.to_hdf(self.hdfStoreName, self.hdfStorePath,append=True,mode='a',min_itemsize = 200) # Commented due to h5 size increases bcz all column has contant with of 200
                    # self.hdf.append(self.hdfStorePath,grp,data_columns=True, min_itemsize = 200)#,min_itemsize = 2000, index = True # Commented due to format ='table' cause slow loading
                # self.hdf.put(self.hdfStorePath, df, format='Table', data_columns=True)# comment due to Memory Error
                self.logger.info('Completed Inserting in table ' + table_name + ' with ' + str(len(df)) + ' records')
                count = str(len(df))
            except Exception as err:
                print 'error while inserting ' + table_name
                print err
                raise
            except:
                raise
        return count

    ## Method to fetch data from DB as data frame
    def fetchFromHDFS(self, execID, storekey, columnName=None,where_clause="'RECORD_STATUS' == 'ACTIVE'"):
        df = pd.DataFrame(columns=columnName)
        storeName = config.hdfFilesPathNew + self.reconId + '/' + execID + '/' + execID + '.h5'
        if columnName is not None and len(columnName) and os.path.exists(storeName):
            hdf = HDFStore(storeName)
            if storekey in hdf.keys():
                for chunk in hdf.select(storekey, chunksize=config.chunckSize, columns=columnName,where=where_clause):
                    df = pd.concat([df, chunk])
        elif os.path.exists(storeName):
            hdf = HDFStore(storeName)
            if storekey in hdf.keys():
                for chunk in hdf.select(storekey, chunksize=config.chunckSize,where=where_clause):
                    df = pd.concat([df, chunk], ignore_index=True)
        return df



    ## Method to generate latest executionID
    def generateReconExecutionID(self):
        executionID = 0
        qry = "select SEQ_TRANSACTION_ID.nextval from dual"
        executionID = pd.read_sql(qry, self.dbConn)['NEXTVAL'].astype(str).tolist()[0]
        return executionID
