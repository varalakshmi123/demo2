# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

from PropertiesHelper import PropertiesHelper
import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import traceback
import shutil
import os


class PostJob():

    ## Constructor
    def __init__(self):
        self.logger = logging.getLogger('PYRBE')
        self.propsHelper = PropertiesHelper()

    ## Method to send summary email
    def sendSummaryMail(self, jobSummary):
        email = self.propsHelper.getProperty('SEND_SUMMARY_EMAIL')
        if email is None or email == 'NO':
            self.logger.info('Sending Job Summary Email switched off')
            return
        try:
            msgHtml = self.prepMailMsg(jobSummary)
            smtp = smtplib.SMTP(self.propsHelper.getProperty('SMTP_HOST'),
                                self.propsHelper.getProperty('SMTP_PORT'))
            if self.propsHelper.getProperty('SMTP_START_TLS') == 'YES':
                smtp.ehlo()
                smtp.starttls()
            if self.propsHelper.getProperty('SMTP_AUTH') == 'YES':
                smtp.login(self.propsHelper.getProperty('SMTP_USER'),
                           self.propsHelper.getProperty('SMTP_PASSWORD'))
            sender = self.propsHelper.getProperty('EMAIL_SENDER')
            recipients = self.propsHelper.getProperty('EMAIL_RECIPIENTS').split(',')
            msg = MIMEMultipart()
            msg["From"] = sender
            msg["Subject"] = '{' + self.propsHelper.getProperty('ENV_CODE') + '} ' \
                             'Job Summary - ' + jobSummary['reconId'] + \
                             ' (' + jobSummary['stmtDate'] + ')'
            msg.attach(MIMEText(msgHtml, 'html'))
            smtp.sendmail(sender, recipients, msg.as_string())
            smtp.close()
            self.logger.info('Job Summary Email sent')
        except Exception, e:
            self.logger.warn('Error while sending Job Summary Email')
            self.logger.error(traceback.print_exc())

    ## Method to move feed files to processed
    def moveToProcessed(self, filesToMove):
        try:
            mftPath = self.propsHelper.getProperty("LOCAL_MFT_PATH")
            for key in filesToMove.keys():
                if not os.path.exists(mftPath + os.sep + key + os.sep + 'processed'):
                    os.makedirs(mftPath + os.sep + key + os.sep + 'processed')
                files = filesToMove[key]
                for file in files:
                    if os.path.exists(mftPath + os.sep + key + os.sep + 'processed' + os.sep + file):
                        os.remove(mftPath + os.sep + key + os.sep + 'processed' + os.sep + file)
                    shutil.move(mftPath + os.sep + key + os.sep + file,
                                mftPath + os.sep + key + os.sep + 'processed')
            self.logger.info('Feed files moved to processed folder')
        except Exception, e:
            self.logger.warn('Error while moving feed files to processed folder')
            self.logger.error(traceback.print_exc())

    ## Method to prepare email message
    def prepMailMsg(self, jobSummary):
        emailMsg = "<html>" \
                  "  <head>" \
                  "    <style>" \
                  "      body { font-family: Calibri }" \
                  "      table { border: 1px solid black;font-size: 85% }" \
                  "      th { background-color: #CFCFCF;border: 1px solid black;padding: 5px }" \
                  "      tr { border: 1px solid black;padding: 5px }" \
                  "      td { border: 1px solid black;padding: 5px }" \
                  "    </style>" \
                  "  </head>" \
                  "  <body>" \
                  "	  <p>Dear Receipient,</p>" \
                  "   <p>The execution of job for recon ID " + jobSummary['reconId'] + " is successful. PFB execution summary.</p>" \
                  "	  <p><table cellpadding='5'>" \
                  "		  <tr>" \
                  "			  <th align='left'>Recon ID</th>" \
                  "			  <th align='left'>Execution ID</th>" \
                  "			  <th align='left'>Statement Date</th>" \
                  "			  <th align='left'>Execution Date</th>" \
                  "		  </tr>" \
                  "		  <tr>" \
                  "			  <td>" + jobSummary['reconId'] + "</td>" \
                  "			  <td>" + jobSummary['execId'] + "</td>" \
                  "			  <td>" + jobSummary['stmtDate'] + "</td>" \
                  "			  <td>" + jobSummary['execDate'] + "</td>" \
                  "		  </tr></table></p></br>"
        sources = jobSummary['sources']
        for key in sources.keys():
            source = sources[key]
            emailMsg += "<p><table>" + \
                        "<tr>" \
                       "    <th align='left' colspan='7'>" + source['sourceName'] + "</th>" \
                       "</tr>" \
                       "<tr>" \
                       "    <th align='left'>Feed Name</th>" \
                       "    <th align='left'>Feed Files</th>" \
                       "    <th align='left'>Split Applied</th>" \
                       "    <th align='left'>Raw Input</th>" \
                       "    <th align='left'>Filter Applied</th>" \
                       "    <th align='left'>Filtered Input</th>" \
                       "    <th align='left'>Used For Matching</th>" \
                      "</tr>"
            feeds = source['feeds']
            for feed in feeds:
                emailMsg += "<tr>" \
                           "    <td>" + feed['feedName'] + "</td>" \
                           "    <td>" + ','.join(feed['feedFiles']) + "</td>" \
                           "    <td align='center'>" + feed['splitApplied'] + "</td>" \
                           "    <td align='right'>" + feed['rawInputCount'] + "</td>" \
                           "    <td align='center'>" + feed['filterApplied'] + "</td>" \
                           "    <td align='right'>" + feed['filteredCount'] + "</td>" \
                           "    <td align='center'>" + feed['usedInMatching'] + "</td>" \
                           "</tr>"
            emailMsg += "</table></p></br>"
        emailMsg += "<p><table cellpadding='5'>" \
                    "   <tr>" \
                    "       <th align='left'>Current Input</th>" \
                    "       <th align='left'>Carry Forward Input</th>" \
                    "       <th align='left'>Total Input</th>" \
                    "   </tr>" \
                    "   <tr>" \
                    "       <td align='right'>" + jobSummary['currentInput'] + "</td>" \
                    "       <td align='right'>" + jobSummary['cfwdInput'] + "</td>" \
                    "       <td align='right'>" + jobSummary['totalInput'] + "</td>" \
                    "   </tr>" \
                    "   <tr>" \
                    "       <th align='left'>Matched Count</th>" \
                    "       <th align='left'>Unmatched Count</th>" \
                    "       <th align='left'>Total Output</th>" \
                    "       <th align='left'>Total Output Ignored in Post Match</th>" \
                    "       <th align='left'>Total Output After Post Match</th>" \
                    "   </tr>" \
                    "   <tr>" \
                    "       <td align='right'>" + jobSummary['matchedCount'] + "</td>" \
                    "       <td align='right'>" + jobSummary['unmatchedCount'] + "</td>" \
                    "       <td align='right'>" + jobSummary['totalOutput'] + "</td>" \
                    "       <td align='right'>" + jobSummary['postmatchIgnored'] + "</td>" \
                    "       <td align='right'>" + str(int(jobSummary['totalOutput']) - int(jobSummary['postmatchIgnored'])) + "</td>" \
                    "</tr></table></p></br>" \
                    "<p><table cellpadding='5'>" \
                   "	<tr>" \
                   "		<th align='left'>Load Process Start</th>" \
                   "		<th align='left'>Load Process End</th>" \
                   "	</tr>" \
                   "	<tr>" \
                   "		<td>" + jobSummary['loadStart'] + "</td>" \
                   "		<td>" + jobSummary['loadEnd'] + "</td>" \
                   "	</tr>" \
                   "	<tr>" \
                   "		<th align='left'>Match Process Start</th>" \
                   "		<th align='left'>Match Process End</th>" \
                   "	</tr>" \
                   "	<tr>" \
                   "		<td>" + jobSummary['matchStart'] + "</td>" \
                   "		<td>" + jobSummary['matchEnd'] + "</td>" \
                   "	</tr>" \
                   "	<tr>" \
                   "		<th align='left'>Job Start</th>" \
                   "		<th align='left'>Job End</th>" \
                   "	</tr>" \
                   "	<tr>" \
                   "		<td>" + jobSummary['jobStart'] + "</td>" \
                   "		<td>" + jobSummary['jobEnd'] + "</td>" \
                   "	</tr>" \
                   "</table></p>" \
                   "<p><i><u><b>**Note:</b>&nbsp;System generated email. Do not reply</u></i></p>" \
                   "</body></html>"
        return emailMsg
