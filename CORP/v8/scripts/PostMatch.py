# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

# from DBOps import DBOps
from datetime import datetime
import pandas as pd
import uuid
import logging
import time
import traceback
import numpy
from mdfieldSizes import modelFieldColSize as colSizes

class PostMatch():

    ## Constructor
    def __init__(self, reconId, execId, dbOps):
        self.reconId = reconId
        self.execId = execId
        self.dbOps = dbOps
        self.logger = logging.getLogger('PYRBE')
        self.errmsg = "%d records expected to be updated. But only %d records updated"

    ## Run method
    def run(self, prevExecId,df):
        self.prevExecID = prevExecId
        self.currentDf = df
        mdbDoc = self.cfAuthPendRecs()
        self.dbOps.commit()
        return mdbDoc


    ## Method to carry forward authorization pending records to new execution
    def cfAuthPendRecs(self):
        outDF = pd.DataFrame()
        if self.prevExecID is not None:
            where_clause = "('RECORD_STATUS' == 'ACTIVE') & ('ENTRY_TYPE' == 'T') & ('TXN_MATCHING_STATUS' == 'UNMATCHED')"
            storeKey = 'TXN_DATA'
            try:
                outDF = self.dbOps.fetchFromHDFS(str(self.prevExecID), storeKey, where_clause=where_clause)
                if 'TXN_MATCHING_STATUS' in outDF.columns:
                    outDF = outDF.loc[outDF[(outDF['RECORD_STATUS'] == 'ACTIVE')].index]
                    outDF = outDF.loc[outDF[(outDF['TXN_MATCHING_STATUS'].isin(
                        ['AUTHORIZATION_PENDING', 'ROLLBACK_AUTHORIZATION_PENDING']))].index]
                else:
                    outDF = pd.DataFrame()
                if len(outDF):
                    outDF['OBJECT_OID'] = outDF['RAW_LINK'].apply(lambda x:  str(uuid.uuid4().int & (1 << 64) - 1))
                    # outDF['OBJECT_OID'] = outDF['OBJECT_OID'].apply(lambda x: str(x))
                    outDF['RECON_EXECUTION_ID'] = numpy.int64(self.execId)
                    outDF['EXECUTION_DATE_TIME'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    outDF['EXECUTION_DATE_TIME'] = pd.to_datetime(outDF['EXECUTION_DATE_TIME'])
                    for col in outDF.columns.values:
                        if 'DATE' in col.upper() or 'TIMESTAMP' in col.upper():
                            outDF[col] = outDF[col].fillna('')

                    self.dbOps.saveDFToHD5(outDF,'TXN_DATA',execId=self.execId)
                    mdbDoc = outDF['TXN_MATCHING_STATUS'].value_counts().reset_index()
                    mdbDoc = dict(zip(mdbDoc['index'],mdbDoc['TXN_MATCHING_STATUS']))
                    for i in ['AUTHORIZATION_PENDING','ROLLBACK_AUTHORIZATION_PENDING','FORCE_MATCHED']:
                        if i not in mdbDoc.keys():
                            mdbDoc[i] = 0
                    self.logger.info(str(len(outDF)) + ' authorization pending transactions carry forwarded')
                    return mdbDoc
                else:
                    self.logger.info(str(len(outDF)) + ' authorization pending transactions carry forwarded')
                    mdbDoc = {'AUTHORIZATION_PENDING':0,'ROLLBACK_AUTHORIZATION_PENDING':0,'FORCE_MATCHED':0}
                    return mdbDoc
            except Exception, e:
                print e
                raise
        else:
            self.logger.info('0 authorization pending transactions carry forwarded')
            mdbDoc = {'AUTHORIZATION_PENDING': 0, 'ROLLBACK_AUTHORIZATION_PENDING': 0, 'FORCE_MATCHED': 0}
            return mdbDoc
