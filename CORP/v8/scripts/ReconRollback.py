"""
 *******************************************************************************
 * 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 30-Jun-2016
 *******************************************************************************
"""
from PropertiesHelper import PropertiesHelper
from DBOps import DBOps
from logging.handlers import TimedRotatingFileHandler
import logging
import sys
from pymongo import MongoClient
from PropertiesHelper import PropertiesHelper
import time
from bson import ObjectId

class ReconRollback:

    def __init__(self, reconID):
        self.reconID = reconID
        # self.dbOps = DBOps(reconID)
        self.propsHelper = PropertiesHelper()
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] - %(message)s', datefmt='%m-%d-%Y %H:%M:%S')
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(formatter)
        fh = TimedRotatingFileHandler('../logs/pyrbe.log', when='D', interval=1)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.mongo_client = MongoClient('mongodb://' + self.propsHelper.getProperty('MONGODB_HOST') + ':27017')
        self.mongo_db = self.mongo_client['erecon']
        self.execLogCollName = self.propsHelper.getProperty('RECON_EXECUTION_DETAILS_LOG_COLL_NAME')
        self.logger = logging.getLogger('PYRBE')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(console)
        self.logger.addHandler(fh)

    def run(self):
        try:
            self.logger.info('#######################')
            self.logger.info('Recon ID: ' + self.reconID)
            self.logger.info('Command: ROLLBACK')
            self.logger.info('#######################')
            # recon = self.mongo_client['reconbuilder']['recon_context_details'].find_one({'reconId':self.reconID})
            recon = self.mongo_db['recon_execution_details_log'].find_one({'RECON_ID':self.reconID})
            if recon == None:
                self.logger.error("Provided recon ID doesn't exist")
                self.logger.info('#######################')
                self.logger.info('Job Status: Failed')
                self.logger.info('#######################')
                exit(0)
            # qry = "select count(*) from recon_execution_details_log where recon_id='" + self.reconID + "' and execution_status='Completed' and processing_state_status='Matching Completed' and record_status='ACTIVE'"

            prevExecDoc = self.mongo_db[self.execLogCollName].find({'RECON_ID': str(self.reconID),'EXECUTION_STATUS': 'Completed',
                                                                       'RECORD_STATUS': 'ACTIVE','PROCESSING_STATE_STATUS': 'Matching Completed'}).sort([('RECON_EXECUTION_ID',-1)])
            prevExecDoc = list(prevExecDoc)
            if len(prevExecDoc) == 0:
                self.logger.error('Provided recon has no completed executions to rollback')
                self.logger.info('#######################')
                self.logger.info('Job Status: Failed')
                self.logger.info('#######################')
                exit(0)
            else:
                rec = prevExecDoc[0]
                currExecID = str(rec['RECON_EXECUTION_ID'])
                self.logger.info('Details of execution to be rolled back -')
                self.logger.info('  Execution ID  : ' + str(rec['RECON_EXECUTION_ID']))
                self.logger.info('  Statement Date: ' + rec['STATEMENT_DATE'].strftime('%d-%m-%Y'))
                self.logger.info('  Execution Date: ' + rec['EXECUTION_DATE'].strftime('%d-%m-%Y'))
                self.logger.info('Details of active execution post rollback -')
                if len(prevExecDoc) >1:
                    lastExecDoc =  prevExecDoc[1]
                    self.logger.info('  Execution ID  : ' +  str(lastExecDoc['RECON_EXECUTION_ID']))
                    self.logger.info('  Statement Date: ' + lastExecDoc['STATEMENT_DATE'].strftime('%d-%m-%Y'))
                    self.logger.info('  Execution Date: ' + lastExecDoc['EXECUTION_DATE'].strftime('%d-%m-%Y'))
                else:
                    self.logger.info('  <No previous execution available for this recon>')
                proceed = ''
                while proceed != 'y' and proceed != 'n':
                    proceed = raw_input('Proceed? (y/n): ')
                if proceed == 'n':
                    self.logger.info('#######################')
                    self.logger.info('Job Status: Aborted by user')
                    self.logger.info('#######################')
                    exit(0)
                self.logger.info(
                    'Note: All user actions performed on execution ' + str(rec['RECON_EXECUTION_ID']) + ' will be cleared')
                proceed = ''
                while proceed != 'y' and proceed != 'n':
                    proceed = raw_input('Proceed? (y/n): ')
                if proceed == 'n':
                    self.logger.info('#######################')
                    self.logger.info('Job Status: Aborted by user')
                    self.logger.info('#######################')
                    exit(0)
                self.logger.info('Proceeding...')
                self.logger.info('Updating output...')
                rec['RECORD_STATUS'] = 'INACTIVE'
                rec['UPDATED_BY'] = 'SYSTEM'
                rec['UPDATED_DATE'] = int(time.time())
                rec['EXECUTION_STATUS'] = 'Rollback'
                self.mongo_db[self.execLogCollName].update({'_id': ObjectId(rec['_id'])}, rec)
                if self.reconID == 'TRSC_CASH_APAC_20181':
                    self.mongo_db.nostro_reports.remove({"execID":str(currExecID)})
                    self.logger.info('Removed nostro reports removed')
                self.logger.info('#######################')
                self.logger.info('Job Status: Success')
                self.logger.info('#######################')
        except Exception as e:
            self.logger.error(e)
            self.logger.info('DB changes rolled back')
            self.logger.info('#######################')
            self.logger.info('Job Status = Failed')
            self.logger.info('#######################')


if __name__ == '__main__':
    rb = ReconRollback(sys.argv[1])
    rb.run()

