import pandas
from datetime import datetime
from pandas import HDFStore
import re
import os
import numpy
from mdfieldSizes import modelFieldColSize as colSizes

# ps = '/usr/share/nginx/v8/mft/CBS/'
# pt = 'sol_[0-9]{4}_[0-9]{4}_1061657016_08022017_[0-9]{3}.lst'
# # files = [f for f in os.listdir(ps) if re.match(pt, f)]
# temp =pandas.DataFrame({'i':['i' for i in range(1,2)],'j':[12222 for i in range(1,2)],'d':None})
# # for i in files:
# #     os.system('python /usr/share/nginx/v8/custom_scripts/cbs_outward_account_filter.py')
# # temp.append('store1.h5', 'data')
# temp = temp.applymap(lambda x: str(x).strip())
# print temp.get_dtype_counts()
# hdf =HDFStore('store1.h5')
# hdfStorePath = 'store1.h5'
# hdf.append('data', temp,min_itemsize=5)
# print pandas.read_hdf('/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010.h5',
#                       '/EPYMTS_NEFT_INWARD_APAC_0009/cash_output_txn/EPYMTS_NEFT_INWARD_APAC_0009_26222',where=[])
# hdf = HDFStore('/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/26315.h5')
# print hdf.keys()
df = pandas.read_hdf('/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/26319.h5',
'/EPYMTS_NEFT_OUTWARD_APAC_0010/cash_output_txn/EPYMTS_NEFT_OUTWARD_APAC_0010_26319/UNMATCHED',where=['TXN_MATCHING_STATUS=="AUTHORIZATION_PENDING"'])
print len(df)
print df['TXN_MATCHING_STATUS'].unique().tolist()
print df['RECORD_STATUS'].unique().tolist()

exit(0)
tempdf = df.head().copy()
print df.columns
# df['UPDATED_BY'] = 'ForceMatched'
# df['COMMENTS'] = 'we'
# df['UPDATED_DATE'] = 'we'
# print df.columns
# print list(set(m) - set(un))
# exit(0)
df.to_hdf('/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/2631411.h5',
          '/EPYMTS_NEFT_OUTWARD_APAC_0010/cash_output_txn/EPYMTS_NEFT_OUTWARD_APAC_0010_26314/MATCHED',format='table')
colsize = {}
# for col in df.columns:
#     if str(col) in colSizes.keys():
#         colsize[col] = colSizes[col]
g = tempdf.groupby(numpy.arange(len(tempdf)) / 10000)
for _, grp in g:
    grpUnMatched = grp.loc[grp[grp['MATCHING_STATUS'] == 'UNMATCHED'].index].copy()

    grpUnMatched.to_hdf('/usr/share/nginx/v8/zfs/ReconData/EPYMTS_NEFT_OUTWARD_APAC_0010/26315.h5',
'/EPYMTS_NEFT_OUTWARD_APAC_0010/cash_output_txn/EPYMTS_NEFT_OUTWARD_APAC_0010_26315/MATCHED', append=True, mode='a',
                        min_itemsize=colsize)  # Best Approach But UI query cannot happen
89590
print