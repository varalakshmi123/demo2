import pandas
outputFile = 'RS110517ATM.csv'
inputFile = 'RS110517ATM.RPT'
wrfile = open(outputFile,'w')
countr = None
lines = []
for line in open(inputFile,'r').readlines():
   if 'LIST OF VISA ELECTRON DATA FROM' in line and countr is None:
      countr = 1
   elif countr ==1 and 'LIST OF VISA ELECTRON DATA FROM' not in line and 'SETL DATE  RECAP  CARD NUMBER      TRAN DATE  TC           AMOUNT CODE   MCC   DATE      CRA AMOUNT        PLACE'  not in line and 'AUTH        CRA NUMBER                  MERCHANT NAME'  not in line and countr is not None:
      if 'sum' not in line and '*' not in line  and '----' not in line :
         newLine = '                                                                              '
         if newLine in line:
            lines[len(lines) - 1] = lines[len(lines) - 1].strip('\n') + line
         else:
            lines.append(line)
         continue
for i in lines:
   wrfile.write(i)
wrfile.close()
colspecs = [(0,10),(10,17),(17,34),(34,45),(45,48),(48,65),(65,72),(72,77),(77,88),(88,105),(105,132),(132,220),(220,253)]
names = ['SETL DATE','RECAP','CARD NUMBER','TRAN DATE','TC','AMOUNT','AUTH CODE','MCC','CRA NUMBER','CRA AMOUNT','MERCHANT NAME PLACE','DATE','MPLACE2']

df = pandas.read_fwf(outputFile, header=None, colspecs=colspecs, names=names,convertors={'TC':str,'AUTH CODE':str,'MCC':str,'CRA NUMBER':str})
df['MERCHANT NAME PLACE'] = df['MERCHANT NAME PLACE'].astype(str) +' '+ df['MPLACE2'].astype(str)
del df['MPLACE2']
df.to_csv(outputFile,index=False)
exit(0)
'11/05/2017 917129 4688170132091747 02/04/2017 25         1,000.00 083539 7372 0000000000      6,78,301.74 CITYCOM NETWORKS PVT L    \n'
'11/05/2017 917129 4688170132091747 02/04/2017 25         1,000.00 083539 7372 0000000000      6,78,301.74 CITYCOM NETWORKS PVT L                                                                                  11/05/2017                   BANGALORE     \n'

import pandas

df = pandas.read_hdf('/erecon/BNA_CASH_APAC_0001/26943_421bf8c5-62ca-44a5-95f2-cb5b98cbce5a_clone/26943.h5','TXN_DATA')
dfM = df[df['TXN_MATCHING_STATUS'] =='MATCHED']
dfUnM = df[df['TXN_MATCHING_STATUS'] =='UNMATCHED']
print len(dfUnM),len(dfM)
exit(0)



from flask import Flask, session, redirect, url_for, escape, request
app = Flask(__name__)
app.secret_key = '58e57c3c-76cb-4c5c-8416-0f3226bc5e88'

@app.route('/')
def index():
   if 'username' in session:
      username = session['username']
      return 'Logged in as ' + username + '<br>' + \
         "<b><a href = '/logout'>click here to log out</a></b>"
   return "You are not logged in <br><a href = '/login'></b>" + \
      "click here to log in</b></a>"


@app.route('/login', methods=['GET', 'POST'])
def login():
   if request.method == 'POST':
      session['username'] = request.form['username/']
      return redirect(url_for('index'))
   return '''

   <form action = "" method = "post">
      <p><input type = text name = username/></p>
      <p><input type ='submit' value = Login/></p>
   </form>

   '''
@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   return redirect(url_for('index'))


if __name__ == '__main__':
   app.run(debug = True)
