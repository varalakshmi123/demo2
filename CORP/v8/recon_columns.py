import datetime
import sys
from collections import OrderedDict

#import cx_Oracle
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient
import os
import json
import pymssql
from DBOps import DBOps
from MSSQL import *
import datetime
import time
import numpy

class ReconMigration():
    def __init__(self, reconId,fileName):
        try:
            self.mongoClient = MongoClient('mongodb://' + 'localhost' + ':27017')
            self.reconId = reconId
            self.updateReconDynamicSetup(fileName)
        except:
            raise


    def appendAuditRecords(self, df):
        df['CREATED_BY'] = 'AlgoReconIT'
        df['CREATED_DATE'] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        df['CREATED_DATE'] = pd.to_datetime(df['CREATED_DATE'], format='%Y-%m-%d %H:%M:%S')
        df['IPADDRESS'] = '0.0.0.0'
        df['RECORD_STATUS'] = 'ACTIVE'
        df['RECORD_VERSION'] = 1
        return df


    def updateReconDynamicSetup(self,fileName):
        dataTypeMapper = {"str": "TEXT", "np.int64": "NUMBER", "np.float64": "DOUBLE", "np.datetime64": "DATE"}
        oid = 0
        id = 1
        df = pd.read_csv('/usr/share/nginx/v8/scripts/'+ fileName)
        df = df[["ORDER_SEQ_NO","MANDATORY","MDL_FIELD_ID", "UI_DISPLAY_NAME", "MDL_FIELD_DATA_TYPE",'position','COL_STATUS','BUSINESS_CONTEXT_ID']]
        # df['ORDER_SEQ_NO'] = df.index + 1
        # df['MANDATORY'] = 'M'
        df['RECON_ID'] = self.reconId
        df['RECON_DYNAMIC_DATA_MODEL_OID'] = df.index + oid
        df['RECON_DYNAMIC_DATA_MODEL_ID'] = df.index + id
        df = self.appendAuditRecords(df)
        df['partnerId'] = "54619c820b1c8b1ff0166dfc"
        df['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
        print len(df[df['COL_STATUS'] == 'ACTIVE'])
        self.mongoClient['erecon']['dynamic_data_setup'].remove({'RECON_ID':self.reconId})
        # print len(df.T.to_dict().values())
        for doc in df.T.to_dict().values():
            self.mongoClient['erecon']['dynamic_data_setup'].save(doc)
        print "### Recon Dynamic data setup updated ###"





if __name__ == '__main__':
    reconId = sys.argv[1]
    fileName =  sys.argv[2]
    reconJob = ReconMigration(reconId,fileName)
