import pandas
import uuid
import subprocess
import time
# from ZFSApi import ZFSApi
import ast
import json

# print json.loads("{'0': '0.0034'}")
df = pandas.read_csv('/home/ubuntu/Downloads/AzureBill.csv', converters={'MeterRates': str})
print df.head()


def dictConvertor(x):
    try:
        json_data = ast.literal_eval(x)
        return json_data
    except Exception as e:
        print e
        # exit(0)
    return x
import numpy


df['MeterRates'] = df['MeterRates'].apply(lambda x: ast.literal_eval(x))
tempdf = df['MeterRates'].apply(pandas.Series)
cols = []
for i in tempdf.columns:
    if '.0' in i and i not in cols:
        del tempdf[i]
        continue
    else:
        if i + '.0' in tempdf.columns:
            tempdf.loc[tempdf[tempdf[i].isnull()].index, i] = tempdf[i + '.0']

        else:
            tempdf[i] = tempdf[i].astype(numpy.float64)
        cols.append(i)

df = pandas.concat([df, tempdf], axis=1)
df['quantity'] = df['quantity'].astype(numpy.float64)
df.loc[df[df['quantity'] < 1024].index, 'total'] = df['0'] * df['quantity']

exit(0)


def execution(command):
    childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                    close_fds=True)
    out, err = childProcess.communicate()
    status = childProcess.returncode
    return status, out, err


def delete(storePath):
    count = 30
    while True:
        (status, out, err) = execution('zfs destroy -fr %s' % storePath)
        print status
        if not status:
            return True
        else:
            print("ZFS destroy out:%s err:%s" % (out, err))
            count -= 1
            if count != 0:
                time.sleep(1)
                continue
            print("ZFS destroy out:%s err:%s" % (out, err))
            return False


import os

for i in os.listdir('/erecon/'):
    print i, os.path.isdir('/erecon/' + i), not i.startswith('.')
    if os.path.isdir('/erecon/' + i) and not i.startswith('.'):
        delete('erecon/' + i)

exit(0)
# df = pandas.read_csv("/usr/share/nginx/v8/mft/EPYMTS_NEFT_OUTWARD_APAC_0010/output_dataframes/07April2017/EPYMTS_NEFT_OUTWARD_APAC_0010_2017-04-07 11:30:43.csv")
#
# df.to_hdf("EPYMTS_NEFT_OUTWARD_APAC_0010","Frame")
# df = pandas.read_hdf("EPYMTS_NEFT_OUTWARD_APAC_0010","Frame")

# print df["OBJECT_OID"].unique()[:40]
# print "TXN.BUSINESS_CONTEXT_ID, TXN.RECON_ID, TXN.RECON_EXECUTION_ID, TXN.STATEMENT_DATE, TRUNC(TXN.EXECUTION_DATE_TIME), TXN.SOURCE_TYPE, TXN.ACCOUNT_NUMBER, TXN.ACCOUNT_NAME, TXN.LINK_ID, TXN.RAW_LINK, TXN.AMOUNT, TXN.DEBIT_CREDIT_INDICATOR, TXN.CURRENCY, TXN.MATCHING_STATUS, TXN.RECONCILIATION_STATUS, TXN.AUTHORIZATION_STATUS, TXN.FORCEMATCH_AUTHORIZATION, TXN.MATCHING_EXECUTION_STATE, EM.EXCEPTION_ID, EM.EXCEPTION_STATUS, EM.EXCEPTION_CATEGORY, EM.REASON_CODE, EM.EXCEPTION_CREATED_DATE, EM.EXCEPTION_PROCESSING_DATE, EM.EXCEPTION_COMPLETION_DATE, EA.USER_POOL_ID, EA.ACTION_BY, EA.ALLOCATED_BY, EA.EXCP_RESL_REASON_CODE, EA.ALLOCATION_TIME, EA.COMPLETION_DATE_TIME FROM CASH_OUTPUT_TXN TXN, EXCEPTION_MASTER EM, EXCEPTION_ALLOCATION EA".split(",")
colNames = ["BUSINESS_CONTEXT_ID", "RECON_ID", "RECON_EXECUTION_ID", "STATEMENT_DATE", "EXECUTION_DATE_TIME",
            "SOURCE_TYPE", "ACCOUNT_NUMBER", "ACCOUNT_NAME", "LINK_ID", "RAW_LINK", "AMOUNT", "DEBIT_CREDIT_INDICATOR",
            "CURRENCY", "MATCHING_STATUS", "RECONCILIATION_STATUS", "AUTHORIZATION_STATUS", "FORCEMATCH_AUTHORIZATION",
            "MATCHING_EXECUTION_STATE", "EXCEPTION_ID", "EXCEPTION_STATUS", "EXCEPTION_CATEGORY", "REASON_CODE",
            "EXCEPTION_CREATED_DATE", "EXCEPTION_PROCESSING_DATE", "EXCEPTION_COMPLETION_DATE", "USER_POOL_ID",
            "ACTION_BY", "ALLOCATED_BY", "EXCP_RESL_REASON_CODE", "ALLOCATION_TIME", "COMPLETION_DATE_TIME"]

# SELECT TXN.BUSINESS_CONTEXT_ID, TXN.RECON_ID, TXN.RECON_EXECUTION_ID, TXN.STATEMENT_DATE, TRUNC(TXN.EXECUTION_DATE_TIME), TXN.SOURCE_TYPE, TXN.ACCOUNT_NUMBER, TXN.ACCOUNT_NAME, TXN.LINK_ID, TXN.RAW_LINK, TXN.AMOUNT, TXN.DEBIT_CREDIT_INDICATOR, TXN.CURRENCY, TXN.MATCHING_STATUS, TXN.RECONCILIATION_STATUS, TXN.AUTHORIZATION_STATUS, TXN.FORCEMATCH_AUTHORIZATION, TXN.MATCHING_EXECUTION_STATE, EM.EXCEPTION_ID, EM.EXCEPTION_STATUS, EM.EXCEPTION_CATEGORY, EM.REASON_CODE, EM.EXCEPTION_CREATED_DATE, EM.EXCEPTION_PROCESSING_DATE, EM.EXCEPTION_COMPLETION_DATE, EA.USER_POOL_ID, EA.ACTION_BY, EA.ALLOCATED_BY, EA.EXCP_RESL_REASON_CODE, EA.ALLOCATION_TIME, EA.COMPLETION_DATE_TIME FROM CASH_OUTPUT_TXN TXN, EXCEPTION_MASTER EM, EXCEPTION_ALLOCATION EA WHERE EM.RECON_EXECUTION_ID(+) =TXN.RECON_EXECUTION_ID AND TXN.LINK_ID = EM.LINK_ID(+) AND TXN.RECORD_STATUS ="ACTIVE" AND TXN.ENTRY_TYPE ="T" AND EM.RECORD_STATUS(+) ="ACTIVE" AND EA.RECORD_STATUS(+) ="ACTIVE" AND EA.EXCEPTION_ID(+) = EM.EXCEPTION_ID


exit(0)
group_txn = df.loc[df[df["OBJECT_OID"].isin(df["OBJECT_OID"].unique()[:40])].index]

df.iloc[df[df["OBJECT_OID"].isin(df["OBJECT_OID"].unique()[:40])].index, "RECORD_STATUS"] = "INACTIVE"

linkid = str(uuid.uuid4().int & (1 << 64) - 1)
group_txn.loc[:, "LINK_ID"] = linkid
group_txn.loc[:, "TXN_MATCHING_STATUS"] = "UNMATCHED"
group_txn.loc[:, "TXN_MATCHING_STATE_CODE"] = 3
group_txn.loc[:, "TXN_MATCHING_STATE"] = "SYSTEM_UNMATCHED_GROUPED"
group_txn.loc[:, "GROUPED_FLAG"] = "Y"
group_txn.loc[:, "OBJECT_OID"] = group_txn["LINK_ID"].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))

authorize_txn.loc[:, "TXN_MATCHING_STATUS"] = "AUTHORIZATION_PENDING"
authorize_txn.loc[:, "TXN_MATCHING_STATE"] = "SINGLE_SOURCE_WAITING_FOR_AUTH" \
    if singleEntryMatchIndictor else "WAITING_FOR_AUTHORIZATION"
authorize_txn.loc[:, "OBJECT_OID"] = authorize_txn["LINK_ID"].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
authorize_txn.loc[:, "REASON_CODE"] = query["reason_code"]
authorize_txn.loc[:, "RESOLUTION_COMMENTS"] = query["comments"]
self.update_audit_data(data=authorize_txn, record_status="ACTIVE")

# self.update_audit_data(data=group_txn,record_status = "ACTIVE")
