# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''
from numpy.core.operand_flag_tests import inplace_add
from pymongo import MongoClient
from datetime import datetime
from Utilities import Utilities
from DBOps import DBOps
from PropertiesHelper import PropertiesHelper
import uuid
import bson
import pandas as pd
import numpy as np
import time
import logging
import sys
import subprocess
import os
import re
import shutil
from datetime import timedelta, datetime
from bson.objectid import ObjectId
import config
import tables
import traceback
from tabulate import tabulate


class CustomFunctions():
    ## Constructor
    def __init__(self, reconID, execID, stmtDate, dbOps):
        self.reconID = reconID
        self.execID = execID
        self.stmtDate = stmtDate
        self.dbOps = dbOps
        self.utils = Utilities()
        # self.dbOps = DBOps()
        self.propsHelper = PropertiesHelper()
        self.logger = logging.getLogger('PYRBE')
        mongoHost = self.propsHelper.getProperty('MONGODB_HOST')
        if mongoHost is not None and mongoHost != '':
            try:
                self.mongo_client = MongoClient('mongodb://' + mongoHost + ':27017')
            except Exception, e:
                self.logger.error("Couldn't connect to mongodb service at " + mongoHost + ":27017")
        else:
            self.mongo_client = None
        if self.mongo_client is not None:
            self.mongo_db = self.mongo_client.erecon
        else:
            self.mongo_db = None

            ## Function for MHADA recon

    ## to push opening and closing balances to MongoDB
    def mhada(self, txn1_bal, txn1_amt, txn1_dc, clBal):
        doc = dict()
        doc['created'] = self.utils.getUtcTime()
        doc['updated'] = self.utils.getUtcTime()
        doc['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
        doc['partnerId'] = "54619c820b1c8b1ff0166dfc"
        doc['apiKey'] = "Mjg0YTUwYjg0MjUwNGM5MDJkNzBmZGRmZjM5YTI5ZTE1OTcxMWM4ZWM4NzdiM2Y4MTE1MjdhNWM1ZmQ1MmQzMg=="
        doc['_id'] = bson.objectid.ObjectId()
        if txn1_dc == 'D':
            opBal = txn1_bal + txn1_amt
        else:
            opBal = txn1_bal - txn1_amt
        doc['OPENING_BALANCE'] = opBal
        doc['CLOSING_BALANCE'] = clBal
        doc['RECON_ID'] = self.reconID
        doc['RECON_EXECUTION_ID'] = self.execID
        doc['STATEMENT_DATE'] = datetime.strptime(self.stmtDate, '%d-%b-%Y')
        rep_col = self.mongo_db['mhada_report']
        rep_col.save(doc)

    # Nostro Report
    ## Load swift files
    def loadSwiftFile(self, feedDef, file, names, csvtypes, positions,
                      dtcols, dtpatterns):
        # Create java command to call java wrapper for parsing swift files and save them as
        path = os.path.dirname(file)
        fileName = os.path.basename(file)
        fileNameNoExt = os.path.splitext(fileName)[0]
        # command = 'java -cp .' + os.pathsep + '..' + os.sep + 'lib' + os.sep + '* ' + \
        command = 'java -cp .' + os.pathsep + config.enginePath + 'lib' + os.sep + '* ' + \
                  'com.algofusion.reconciliation.adapter.swift.SwiftToCSVB ' + \
                  self.propsHelper.getProperty('MONGODB_HOST') + ' ' + \
                  self.propsHelper.getProperty('MONGODB_NAME') + ' ' + \
                  str(feedDef['_id']) + ' ' + \
                  (path + os.sep + fileName) + ' ' + \
                  (path + os.sep + fileNameNoExt + '.csv') + ' ' + 'Balance'
        # Output is number of transactions in file
        recs = 0
        try:
            recs = int(subprocess.check_output(command, shell=True))
        except:
            self.logger.error("Unable to load the file " + file + ".\n"
                                                                  "Please check that file is readable and is "
                                                                  "as per the configured structure.\n"
                                                                  "If problem persists contact Administrator.")
            raise BaseException("Unable to load the file " + file)
        df = pd.DataFrame(columns=names)
        if recs > 0:
            df = pd.read_csv(path + os.sep + fileNameNoExt + '.csv',
                             header=None, sep='|', names=names,
                             dtype=csvtypes, usecols=positions)
            if df is not None:
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
                for col in dtcols:
                    # print df[[col,'ACCOUNT_NUMBER']],dtpatterns[col]
                    df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')
        # self.logger.info("Records in file " + fileName + " = %6d" % recs)
        os.remove(path + os.sep + fileNameNoExt + '.csv')
        return df

    # # Mapped columns {TS_6_005 : Statement_data ,TXT_180_005:Opening_balance,,TXT_180_005:Closing_balance,
    # TXT_5_001:Opening Bal Indicator,TXT_5_002:Closing bal indicator,TXT_5_003:Opening currency,TXT_5_004:Closing currency
    def nostroReport(self, reconId, reconData):
        self.mongo_db = self.mongo_client[self.propsHelper.getProperty('MONGODB_NAME')]
        feedDef = self.mongo_db['feedDefination'].find_one({"_id": ObjectId('5875bfeda1dfaa3d1b9fec69')})

        # Append local MFT base path to feed path
        feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + feedDef['feedPath']

        # Find files in the path
        fpattern = feedDef['fileNamePattern']
        self.logger.info(
            "| Finding files for report with pattern '" + fpattern + "' in the path '" + feedDef['feedPath'] + "'")

        indx1 = fpattern.index('#')
        indx2 = fpattern.index('#', indx1 + 1)
        dpattern = fpattern[indx1 + 1:indx2]
        dstr = self.stmtDate.strftime(dpattern)
        fpattern = fpattern.replace('#' + dpattern + '#', dstr)
        files = [f for f in os.listdir(feedPath) if re.search(fpattern, f)]

        if len(files) != 0:
            names = self.utils.getColNames(feedDef)
            types = self.utils.getColTypes(feedDef)
            positions = self.utils.getColPositions(feedDef)
            dtpatterns = self.utils.getDatePatterns(feedDef)
            csvtypes = dict()
            dtcols = []
            for i in range(0, len(types)):
                if types[i] == 'np.datetime64':
                    dtcols.append(names[i])
                    continue
                if types[i] == 'np.int64':
                    continue
                csvtypes[names[i]] = eval(types[i])
            reportData = pd.DataFrame()
            for filen in files:
                loadFile = feedPath + os.sep + filen
                df = self.loadSwiftFile(feedDef, loadFile, names, csvtypes, positions, dtcols, dtpatterns)
                df['FEED_FILE_NAME'] = filen
                if len(df) > 0:
                    reportData = pd.concat([reportData, df], ignore_index=True)

            if len(reportData):

                # Drop row where both opening and closing balance are empty
                reportData = reportData.dropna(subset=['TXT_180_005', 'TXT_180_006'], how='all')
                # Rename Column names to report specified names
                reportData.rename(
                    columns={'TS_6_005': 'STATEMENT DATE', 'TXT_180_005': 'OPENING BALANCE',
                             'TXT_5_001': 'OP BAL DR / CR',
                             'TXT_5_002': 'CL BAL DR / CR', 'TXT_180_006': 'CLOSING BALANCE',
                             'TXT_5_003': 'Opening_Currency', 'TXT_5_004': 'Closing_Currency'}, inplace=True)

                reportData = reportData.drop_duplicates(
                    subset=['STATEMENT DATE', 'ACCOUNT_NUMBER', 'CURRENCY', 'OP BAL DR / CR', 'OPENING BALANCE',
                            'CLOSING BALANCE',
                            'Opening_Currency', 'CL BAL DR / CR', 'Closing_Currency'], keep='last')
                # Get Opening balance frame
                reportData.index = range(1, len(reportData) + 1)

                tempDf = reportData[
                    ['STATEMENT DATE', 'ACCOUNT_NUMBER', 'CURRENCY', 'OP BAL DR / CR', 'OPENING BALANCE',
                     'CLOSING BALANCE',
                     'Opening_Currency', 'CL BAL DR / CR', 'Closing_Currency']].loc[
                    reportData[(reportData['ACCOUNT_NUMBER'].notnull())
                               & (reportData['CURRENCY'].notnull())
                               & (reportData['OPENING BALANCE'].notnull())
                               & (reportData['CLOSING BALANCE'].notnull())
                               & (reportData['OP BAL DR / CR'].notnull()
                                  & (reportData['CL BAL DR / CR'].notnull()
                                     & (reportData['STATEMENT DATE'].notnull()))
                                  & (reportData['Opening_Currency'].notnull()))
                               & (reportData['Closing_Currency'].notnull())].index].copy()
                # print tempDf
                # print '*'*100
                if len(tempDf):
                    reportData = reportData.loc[list(set(reportData.index) - set(tempDf.index))]

                openingFrame = reportData[['Opening_Currency', 'ACCOUNT_NUMBER', 'CURRENCY', 'OP BAL DR / CR',
                                           'OPENING BALANCE']].dropna(subset=['OPENING BALANCE'], how='all')

                openingFrame.rename(columns={'Opening_Currency': 'Currency_m'}, inplace=True)

                # Get Closing balance frame
                closingFrame = reportData[
                    ['Closing_Currency', 'STATEMENT DATE', 'ACCOUNT_NUMBER', 'CL BAL DR / CR',
                     'CLOSING BALANCE']].dropna(
                    subset=['CLOSING BALANCE'], how='all')
                closingFrame.rename(columns={'Closing_Currency': 'Currency_m'}, inplace=True)

                # Merge the frames to display the Opening and Closing balances for same account and currency as per report
                reportData = pd.merge(openingFrame, closingFrame, on=['ACCOUNT_NUMBER', 'Currency_m'], how='outer',
                                      indicator=True).reset_index()

                # drop row if any rows with both Opening balance and closing balance are empty
                reportData = reportData.dropna(subset=['OPENING BALANCE', 'CLOSING BALANCE'], how='all')

                reportData['CURRENCY'] = reportData['Currency_m']
                if len(tempDf):
                    reportData = pd.concat([reportData, tempDf], ignore_index=True)

                # Adding report date and Account name
                reportDateTime = int(time.time())
                reportData['REPORT DATE'] = reportDateTime
                reportData['ACCOUNT NAME'] = ''

                # Converting Balance columns from str to float
                reportData['OPENING BALANCE'] = reportData['OPENING BALANCE'].replace("", None).astype(np.float64)
                reportData['CLOSING BALANCE'] = reportData['CLOSING BALANCE'].replace("", None).astype(np.float64)

                # Changing statement date format
                reportData['STATEMENT DATE'] = reportData['STATEMENT DATE'].astype(str)
                reportData['STATEMENT DATE'] = reportData['STATEMENT DATE'].apply(
                    lambda dateVal: datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                        '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')

                reportData['LAST TXN DATE'] = reportData['STATEMENT DATE']
                reportData = reportData[
                    ['REPORT DATE', 'STATEMENT DATE', 'ACCOUNT_NUMBER', 'ACCOUNT NAME', 'CURRENCY', 'OP BAL DR / CR',
                     'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE', 'LAST TXN DATE']]

                # file Name
                sampleDf = reportData[reportData['STATEMENT DATE'] != '']
                sampleDf['STATEMENT DATE'] = pd.DatetimeIndex(sampleDf['STATEMENT DATE'])
                stmtDate = sampleDf['STATEMENT DATE'].max().strftime('%Y%m%d')

                # Filling NaN with None to store in Mongo

                reportData['CL BAL DR / CR'] = reportData['CL BAL DR / CR'].fillna('')
                reportData['OP BAL DR / CR'] = reportData['OP BAL DR / CR'].fillna('')
                reportData = reportData.where((pd.notnull(reportData)), None)

                reportData = reportData[
                    ['REPORT DATE', 'STATEMENT DATE', 'ACCOUNT_NUMBER', 'ACCOUNT NAME', 'CURRENCY', 'OP BAL DR / CR',
                     'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE', 'LAST TXN DATE']]
                reportData.rename(columns={'ACCOUNT_NUMBER': 'ACCOUNT NO'}, inplace=True)

                # Pushing report data to Mongo db(erecon db) for UI with execID for rollback purpose
                # self.mongo_client = MongoClient('mongodb://' + '10.1.37.53' + ':27017')

                self.mongo_db = self.mongo_client['erecon']
                rep_col = self.mongo_db['nostro_reports']

                # Fetch the previous execution id
                # qry = "select max(recon_execution_id) from recon_execution_details_log where recon_id='" + self.reconID + "' and execution_status='Completed' and processing_state_status='Matching Completed' and record_status='ACTIVE'"
                # currExecID = self.dbOps.executeQuery(qry).fetchone()[0]
                qry = self.utils.getPreviousExecutionId(self.reconID)
                self.prevExecID = None
                if qry != None:
                    self.prevExecID = qry['RECON_EXECUTION_ID']

                # Adding missing currencies for each account from Previous txns to current txns.
                if self.prevExecID is not None:
                    previousRecords = [record for record in rep_col.find({'execID': str(self.prevExecID)})]
                    if len(previousRecords):
                        previousDayDf = pd.DataFrame(previousRecords)
                        # Sorting by stmt date to get last stmt txns of missing currencies
                        previousDayDf = previousDayDf[
                            ['REPORT DATE', 'STATEMENT DATE', 'ACCOUNT NO', 'ACCOUNT NAME', 'CURRENCY',
                             'OP BAL DR / CR',
                             'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE', 'LAST TXN DATE']]
                        previousDayDf['STATEMENT DATE'] = pd.DatetimeIndex(previousDayDf['STATEMENT DATE'])
                        previousDayDf.sort_values(by=['STATEMENT DATE'], inplace=True, ascending=False)
                        previousDayDf.drop_duplicates(subset=['ACCOUNT NO', 'CURRENCY'], inplace=True, keep='first')
                        # Changing stmt date to db format
                        previousDayDf['STATEMENT DATE'] = previousDayDf['STATEMENT DATE'].astype(str)
                        previousDayDf['STATEMENT DATE'] = previousDayDf['STATEMENT DATE'].apply(
                            lambda dateVal: datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                                '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')
                        previousDayDf['Acc_Curr'] = previousDayDf['ACCOUNT NO'] + '#_#_#' + previousDayDf['CURRENCY']
                        reportData['Acc_Curr'] = reportData['ACCOUNT NO'] + '#_#_#' + reportData['CURRENCY']

                        # Fetching missing txn.
                        previousDayDf = previousDayDf[
                            previousDayDf['Acc_Curr'].isin(reportData['Acc_Curr'].unique()) == False]

                        # Updating execId and stmt date with current
                        previousDayDf['STATEMENT DATE'] = sampleDf['STATEMENT DATE'].max().strftime('%Y/%m/%d')

                        reportData = pd.concat([previousDayDf, reportData], ignore_index=True)

                        reportData['CL BAL DR / CR'] = reportData['CL BAL DR / CR'].fillna('')
                        reportData['OP BAL DR / CR'] = reportData['OP BAL DR / CR'].fillna('')
                        reportData = reportData.where((pd.notnull(reportData)), None)
                        del reportData['Acc_Curr']
                for doc in reportData.T.to_dict().values():
                    doc['created'] = self.utils.getUtcTime()
                    doc['updated'] = self.utils.getUtcTime()
                    doc['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
                    doc['execID'] = self.execID
                    doc['partnerId'] = "54619c820b1c8b1ff0166dfc"
                    doc[
                        'apiKey'] = "Mjg0YTUwYjg0MjUwNGM5MDJkNzBmZGRmZjM5YTI5ZTE1OTcxMWM4ZWM4NzdiM2Y4MTE1MjdhNWM1ZmQ1MmQzMg=="
                    doc['_id'] = bson.objectid.ObjectId()
                    rep_col.save(doc)
                if not os.path.exists(feedPath + os.sep + 'Nostro_reports'):
                    os.mkdir(feedPath + os.sep + 'Nostro_reports')

                fpattern = stmtDate + '_Nostro_Closing_balance'
                files = [f for f in os.listdir(feedPath + os.sep + 'Nostro_reports/') if re.search(fpattern, f)]
                outputFileName = feedPath + os.sep + 'Nostro_reports' + os.sep + stmtDate + '_Nostro_Closing_balance.xlsx'
                if len(files):
                    outputFileName = feedPath + os.sep + 'Nostro_reports' + os.sep + stmtDate + '_Nostro_Closing_balance' + str(
                        len(files)) + '.xlsx'
                reportData['STATEMENT DATE'] = pd.DatetimeIndex(reportData['STATEMENT DATE'])
                reportData['LAST TXN DATE'] = pd.DatetimeIndex(reportData['LAST TXN DATE'])

                reportData = reportData.sort_values(by=['STATEMENT DATE', 'LAST TXN DATE', 'CURRENCY'],
                                                    ascending=[False, False, True])
                reportData['STATEMENT DATE'] = reportData['STATEMENT DATE'].astype(str)
                reportData['STATEMENT DATE'] = reportData['STATEMENT DATE'].apply(
                    lambda dateVal: datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                        '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')

                reportData['LAST TXN DATE'] = reportData['LAST TXN DATE'].astype(str)
                reportData['LAST TXN DATE'] = reportData['LAST TXN DATE'].apply(
                    lambda dateVal: datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                        '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')
                reportData.loc[:, 'REPORT DATE'] = reportData.loc[:, 'REPORT DATE'].apply(
                    lambda x: datetime.fromtimestamp(x).strftime('%Y/%m/%d %H:%M:%S'))

                reportData = reportData.loc[:,
                             ['REPORT DATE', 'STATEMENT DATE', 'LAST TXN DATE', 'ACCOUNT NO', 'ACCOUNT NAME',
                              'CURRENCY', 'OP BAL DR / CR', 'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE']]
                reportData['OPENING BALANCE'] = reportData['OPENING BALANCE'].replace("", None).astype(np.float64).map(
                    '{:,.2f}'.format)
                reportData['CLOSING BALANCE'] = reportData['CLOSING BALANCE'].replace("", None).astype(np.float64).map(
                    '{:,.2f}'.format)
                # print reportData
                reportData.to_excel(outputFileName, index=False)
                rsync_cmd = 'rsync -avr ' + feedPath + os.sep + 'Nostro_reports ' + 'mftusr@172.19.36.40:/u01/OUT/MO_REPORTS/NOSTRO_CASH_REPORT/'
                # os.system(rsync_cmd)
                self.logger.info("| Report Generated Successfully")

    def DSB002_New(self, dataDF, dtCols, dbOps, prevExecID):
        oidsToInactivate = []
        dataDF['CREATED_DATE'].fillna(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), inplace=True)
        # if len(dataDF[dataDF['TXT_5_002'] =='N'].index.tolist()) > 0:
        # dataDF[dataDF[dataDF['TXT_5_002'] =='N'].index,'CREATED_DATE'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        amendDF = dataDF[dataDF['TXT_5_002'] == 'Y']
        if amendDF is None or len(amendDF) == 0:
            return dataDF
        amendDF['query'] = "(LEAD_ID='" + amendDF['LEAD_ID'] + "' & TXT_32_003='" + amendDF['TXT_32_003'] + "')"
        # append created_date here
        inclDF = pd.DataFrame(columns=dataDF.columns.tolist() + ['OBJECT_OID'])

        if prevExecID is not None:
            storeKey = '/' + self.reconID + '/' + 'cash_output_txn' + '/' + self.reconID + '_' + str(
                prevExecID) + '/MATCHED'
            inclDF = self.dbOps.fetchFromHDFS(str(prevExecID), storeKey)
            inclDF['query'] = "(LEAD_ID=='" + inclDF['LEAD_ID'] + "' & TXT_32_003=='" + inclDF['TXT_32_003'] + "')"
            inclDF = inclDF.loc[inclDF[inclDF['query'].isin(amendDF['query'].unqiue().tolist()) == True].index]
            inclDF.index = range(1, len(inclDF) + 1)

        # selQry = 'select ' + ','.join(dataDF.columns) + ' , OBJECT_OID from cash_output_txn where link_id in (select link_id from cash_output_txn where (' + ' or '.join(amendDF['query'].tolist()) + ") and recon_id='" + self.reconId + "'" + " matching_status='MATCHED' and record_status='ACTIVE')"
        # inclDF = dbOps.fetchFromDB(selQry, dtCols)
        inclDF['CARRY_FORWARD_FLAG'] = 'Y'
        if len(inclDF) > 0:
            self.logger.info('Including ' + str(len(
                inclDF)) + ' records (including linked records) for processing.  These are in matched status in DB but got amendment record in feed file')
            key = storeKey + os.sep + 'table'
            logged_user = 'SYSTEM'
            storeName = config.hdfFilesPathNew + self.reconId + '/' + prevExecID + '/' + prevExecID + '.h5'
            with tables.open_file(storeName, mode='r+') as h5file:
                table = h5file.get_node(key)
                count = 0
                for value in range(0, len(inclDF)):
                    where_clause = "(RECORD_STATUS == 'ACTIVE') & (SOURCE_TYPE == 'S1') & %s " % (
                        str(inclDF.ix[value]['query']))
                    for existing_row in table.where(where_clause):
                        existing_row['RECORD_STATUS'] = 'INACTIVE'
                        existing_row['UPDATED_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        existing_row['UPDATED_BY'] = logged_user
                        count += 1
                        existing_row.update()
                count = 0
                amendDF_S2 = inclDF[inclDF['SOURCE_TYPE'] == 'S2']
                for value in amendDF_S2['OBJECT_OID'].unqiue().tolist():
                    where_clause = "(RECORD_STATUS == 'ACTIVE') & (SOURCE_TYPE == 'S2') & %s " % (
                        str(value))
                    for existing_row in table.where(where_clause):
                        existing_row['RECORD_STATUS'] = 'INACTIVE'
                        existing_row['UPDATED_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        existing_row['UPDATED_BY'] = logged_user
                        count += 1
                        existing_row.update()

        '''
        # comment because Matched records are in other H5 file
        count = 0
        amendDF_S1 = amendDF[amendDF['SOURCE_TYPE'] == 'S1']
        if amendDF_S1 is not None and len(amendDF_S1) > 0:
            updQry = "update cash_output_txn set RECORD_STATUS='INACTIVE', UPDATED_BY='SYSTEM', UPDATED_DATE=current_timestamp where " + ' or '.join(amendDF_S1['query'].tolist())
            count = dbOps.executeUpdate(updQry)
        amendDF_S2 = inclDF[inclDF['SOURCE_TYPE'] == 'S2']
        # print amendDF_S2
        if amendDF_S2 is not None and len(amendDF_S2) > 0:
            updQry = "update cash_output_txn set RECORD_STATUS='INACTIVE', UPDATED_BY='SYSTEM', UPDATED_DATE=current_timestamp where object_oid in (" + ','.join(map("'{0}'".format, amendDF_S2['OBJECT_OID'].tolist())) + ')'
            count += dbOps.executeUpdate(updQry)
        if count > 0:
            self.logger.info('Inactivated ' + str(count) + ' original records of amendment records')
        '''
        indxdDF = pd.concat([dataDF, inclDF], ignore_index=True)

        indxdDF['CREATED_DATE'] = pd.to_datetime(indxdDF['CREATED_DATE'])
        procDF = indxdDF.copy()
        procDF.sort_values(by=['CREATED_DATE'], ascending=True, inplace=True)
        procDF.drop_duplicates(subset=['LEAD_ID', 'TXT_32_003'], keep='last', inplace=True)
        # dbOps.commit()
        return procDF

    def interBranch(self, df1, df2, keys_eq, keys_ne, aggr='AMOUNT'):
        df1g = df1[keys_eq + keys_ne + [aggr]].groupby(keys_eq + keys_ne).sum()
        df1g = df1g.reset_index()
        df2g = df2[keys_eq + keys_ne + [aggr]].groupby(keys_eq + keys_ne).sum()
        df2g = df2g.reset_index()
        # df1g[aggr] = df1g[aggr].apply(lambda x: abs(x))
        # df2g[aggr] = df2g[aggr].apply(lambda x: abs(x))
        dfg = pd.merge(df1g, df2g, how='inner', on=keys_eq, indicator=True)
        df_matched = None
        df_unmatched = None
        if len(dfg) == 0:
            df_unmatched = pd.concat([df1, df2])
        else:
            expr = "dfg[(dfg['_merge'] == 'both')"
            for key in keys_ne:
                expr += " & (dfg['" + key + "_x'] != dfg['" + key + "_y'])"
            expr += "]"
            dfg_matched = eval(expr)
            if len(dfg_matched) > 0:
                del dfg_matched['_merge']
                dfg_matched['MATCHING_STATUS'] = 'MATCHED'
                dfg_matched['RECONCILIATION_STATUS'] = 'RECONCILED'
                dfg_matched['AUTHORIZATION_STATUS'] = 'SYSTEM_AUTHORIZED'
                dfg_matched['MATCHING_EXECUTION_STATE'] = 'SYSTEM_MATCHED'
                dfg_matched['LINK_ID'] = dfg_matched[keys_eq[0]].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                right_keys1 = []
                right_keys2 = []
                for key in keys_ne:
                    right_keys1 = right_keys1 + [key + "_x"]
                    right_keys2 = right_keys2 + [key + "_y"]
                df1_merged = pd.merge(df1, dfg_matched, how='left', left_on=keys_eq + keys_ne,
                                      right_on=keys_eq + right_keys1, suffixes=('', '_z'))
                df2_merged = pd.merge(df2, dfg_matched, how='left', left_on=keys_eq + keys_ne,
                                      right_on=keys_eq + right_keys2, suffixes=('', '_z'))
                df1_matched = df1_merged[(df1_merged['MATCHING_STATUS'] == 'MATCHED')]
                df2_matched = df2_merged[(df2_merged['MATCHING_STATUS'] == 'MATCHED')]
                df_matched = pd.concat([df1_matched, df2_matched])
                df_matched['PROBABLE'] = 'N'
                df1_unmatched = df1_merged[(df1_merged['MATCHING_STATUS'] != 'MATCHED')]
                df2_unmatched = df2_merged[(df2_merged['MATCHING_STATUS'] != 'MATCHED')]
                df_unmatched = pd.concat([df1_unmatched, df2_unmatched])

        if df_matched is not None:
            for col in df_matched.columns:
                if '_x' in col:
                    del df_matched[col]
                if '_y' in col:
                    del df_matched[col]
                if '_z' in col:
                    del df_matched[col]
            del df_matched['KEY_2']
            del df_matched['GLCC5']
        if df_unmatched is not None:
            for col in df_unmatched.columns:
                if '_x' in col:
                    del df_unmatched[col]
                if '_y' in col:
                    del df_unmatched[col]
                if '_z' in col:
                    del df_unmatched[col]
            del df_unmatched['KEY_2']
            del df_unmatched['GLCC5']
        df_matched = df_matched.drop_duplicates()
        df_unmatched = df_unmatched.drop_duplicates()
        return (df_matched, df_unmatched)

    def dealTally(self, reconData, fuzzyFlag, reconId, prevExecID=None):
        self.reconId = reconId
        dataDF = reconData['S1'].copy()
        source2 = reconData['S2'].copy()
        dtCols = []
        for col in dataDF.columns:
            if 'DATE' in col or 'TIMESTAMP' in col:
                dtCols.append(col)
        # dataDF['DEAL_NUMBER'] = dataDF['DEAL_NUMBER'].fillna('')

        allDeal_ID = dataDF['DEAL_NUMBER'][dataDF['DEAL_NUMBER'].notnull()][
            dataDF['CARRY_FORWARD_FLAG'] == 'N'].unique().tolist()
        qry = self.utils.getPreviousExecutionId(self.reconId)
        if qry != None:
            prevExecID = qry['RECON_EXECUTION_ID']

        matchedDf_s1 = pd.DataFrame(columns=dataDF.columns.tolist() + ['OBJECT_OID'])
        inclDFs = pd.DataFrame()

        if prevExecID is not None:
            storeKey = '/' + self.reconID + '/' + 'cash_output_txn' + '/' + self.reconID + '_' + str(
                prevExecID) + '/MATCHED'
            inclDF = self.dbOps.fetchFromHDFS(str(prevExecID), storeKey)
            if len(inclDF):
                matchedDf_s1 = inclDF[dataDF.columns.tolist() + ['OBJECT_OID']].loc[
                    inclDF[inclDF['DEAL_NUMBER'].isin(allDeal_ID) & (inclDF['SOURCE_TYPE'] == 'S1')].index]
                matchedDf_s1.index = range(1, len(matchedDf_s1) + 1)
        '''
        # comment because Matched records are in other H5 file
        selQry_s1 = "select " + ','.join(dataDF.columns) + ",OBJECT_OID from cash_output_txn " \
                                                           "where SOURCE_TYPE='S1' and recon_id='" + self.reconId + "'" + " and DEAL_NUMBER in ('" + "','".join(
            allDeal_ID) + "') and matching_status='MATCHED' and record_status='ACTIVE'"

        matchedDf_s1 = self.dbOps.fetchFromDB(selQry_s1, dtCols)
        '''

        if len(matchedDf_s1) > 0:
            allLink_ID_to_unmatch = matchedDf_s1['LINK_ID'].astype(str).unique().tolist()
            matchedDf_s2 = inclDF[source2.columns + ['OBJECT_OID']].loc[
                inclDF[inclDF['LINK_ID'].isin(allLink_ID_to_unmatch) & (inclDF['SOURCE_TYPE'] == 'S2')].index]
            matchedDf_s2.index = range(1, len(matchedDf_s2) + 1)
            '''
            # comment because Matched records are in other H5 file
            selQry_s2 = "select " + ','.join(source2.columns) + ",OBJECT_OID from cash_output_txn " \
                                                                "where SOURCE_TYPE='S2' and recon_id='" + \
                        self.reconId + "'" + " and LINK_ID in ('" + "','".join(
                allLink_ID_to_unmatch) + "') and matching_status='MATCHED' and record_status='ACTIVE'"
            dtCols1 = []
            for col in source2.columns:
                if 'DATE' in col or 'TIMESTAMP' in col:
                    dtCols1.append(col)
            matchedDf_s2 = self.dbOps.fetchFromDB(selQry_s2, dtCols)
            '''
            oids = matchedDf_s1['OBJECT_OID'].tolist()
            if len(matchedDf_s2):
                self.logger.info("Inactivated " + str(len(matchedDf_s1)) + " original records of amendment records")
                oids += matchedDf_s2['OBJECT_OID'].tolist()
                matchedDf_s2.drop(['OBJECT_OID'], axis=1, inplace=True)
                matchedDf_s2['CARRY_FORWARD_FLAG'] = 'Y'

                # Ignore the matched records from S2  where deal_number are in new s2 records
                # matchedDf_s2 = matchedDf_s2[~(matchedDf_s2['DEAL_NUMBER'].isin(source2['DEAL_NUMBER'].unique().tolist()))]

                source2 = pd.concat([source2, matchedDf_s2], ignore_index=True)
            self.logger.info(
                "Including " + str(len(matchedDf_s2)) + " records (including linked records) for processing.  "
                                                        "These are in matched status in DB but got amendment record in feed file")

            '''
            # comment because Matched records are in other H5 file
            # Status updating to INACTIVE for amended(new DEAL_NUMBER if present in Matched and unmatched) records for both sources
            updQry = "update cash_output_txn set RECORD_STATUS='INACTIVE', " \
                     "UPDATED_BY='SYSTEM', UPDATED_DATE=current_timestamp " \
                     "where object_oid in (" + \
                     (','.join(map("'{0}'".format, oids))) + ")"
            self.dbOps.executeUpdate(updQry)
            # print updQry

            self.dbOps.commit()
            '''

            self.logger.info('Updated Amended Records')

        newFeeds1 = dataDF[(dataDF['CARRY_FORWARD_FLAG'] == 'N')]

        # Update the carry forwarded records with new amended records(ie, drop from carryforwarded records)
        newFeeds1_carryforwarded = dataDF[(dataDF['CARRY_FORWARD_FLAG'] != 'N')]
        newFeeds1_carryforwarded = newFeeds1_carryforwarded[
            newFeeds1_carryforwarded['DEAL_NUMBER'].isin(allDeal_ID) == False]  #
        source1 = pd.concat([newFeeds1, newFeeds1_carryforwarded], ignore_index=True)
        reconData['S1'] = source1
        reconData['S2'] = source2

        return reconData

    def bs_19128(self, reconData):
        masterDict = reconData
        filterRecordCnt = 0

        if len(reconData) > 0:
            dfs1 = reconData['S1']
            dfs2 = reconData['S2']

            # TO display natural account number in the ui , part of the CR
            dfs1.loc[:, 'TXT_60_005'] = dfs1.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]
            dfs2.loc[:, 'TXT_60_005'] = dfs2.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]

            # retrieve transaction from OGL with balance amount as '0'
            dfs2 = dfs2[(dfs2['AMOUNT'] == 0)]

            # if balance_amount in OGL(s2) is zero the check gl_acc_num with BSMART extract(s1) gl_acc_num
            # if not exists ignore the transactions

            dfm = dfs1.merge(dfs2, on='GL_ACCOUNT_NUMBER', indicator=True, how='right')
            dfm = dfm[dfm['_merge'] == 'right_only']

            filterRecordCnt = len(dfm)
            self.logger.info('Post Match Process Initiated')
            self.logger.info("Filtered records = %6d" % filterRecordCnt)
            self.logger.info('----------------------------------------')

            glAccList = dfm['GL_ACCOUNT_NUMBER'].unique().tolist()

            dfs2_filter = masterDict['S2']
            dfs2_filter = dfs2_filter[(~ dfs2_filter['GL_ACCOUNT_NUMBER'].isin(glAccList))]

            masterDict['S2'] = dfs2_filter

        return masterDict, filterRecordCnt

    def bna_report(self, reconData):
        self.mongo_db = self.mongo_client[self.propsHelper.getProperty('MONGODB_NAME')]
        feedDef = self.mongo_db['feedDefination'].find_one({"_id": ObjectId('590991981a1bc75676e1381e')})

        # Append local MFT base path to feed path
        if '#' in feedDef['feedPath']:
            fpattern = feedDef['feedPath']
            indx1 = fpattern.index('#')
            indx2 = fpattern.index('#', indx1 + 1)
            dpattern = fpattern[indx1 + 1:indx2]
            dstr = self.stmtDate.strftime(dpattern)
            feedDef['feedPath'] = fpattern.replace('#' + dpattern + '#', dstr)

        feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + feedDef['feedPath']

        # Find files in the path
        fpattern = feedDef['fileNamePattern']

        indx1 = fpattern.index('#')
        indx2 = fpattern.index('#', indx1 + 1)
        dpattern = fpattern[indx1 + 1:indx2]
        dstr = self.stmtDate.strftime(dpattern)
        fpattern = fpattern.replace('#' + dpattern + '#', dstr)
        files = [f for f in os.listdir(feedPath) if re.match(fpattern, f)]
        skipTopRows = int(feedDef['skipTopRows'])
        dateFormatCols = ['TRAN_DATE', 'POST_DATE', 'UPLDDATE', 'STATEMENT_DATE']

        if feedDef['headerExists']:
            skipTopRows += 1
        skipBottomRows = int(feedDef['skipBottomRows'])

        if len(files) != 0:
            names = self.utils.getColNames(feedDef)
            types = self.utils.getColTypes(feedDef)
            positions = self.utils.getColPositions(feedDef)
            dtpatterns = self.utils.getDatePatterns(feedDef)
            csvtypes = dict()
            dtcols = []
            for i in range(0, len(types)):
                if types[i] == 'np.datetime64':
                    dtcols.append(names[i])
                    continue
                if types[i] == 'np.int64':
                    continue
                csvtypes[names[i]] = eval(types[i])
            reportData = pd.DataFrame()
            for filen in files:
                loadFile = feedPath + os.sep + filen.replace('.xlsx', '_Report.csv')
                df = self.loadDelimFile(feedDef, loadFile, names, csvtypes, positions,
                                        dtcols, dtpatterns, skipTopRows, skipBottomRows)
                reportData = pd.concat([reportData, df], ignore_index=True)
            if len(reconData):
                cols, colRename = self.utils.getColsToStore(self.reconID)
                reportData['RECORD_STATUS'] = 'ACTIVE'
                reportData['STATEMENT DATE'] = self.stmtDate
                reportData.rename(columns=colRename, inplace=True)
                reportData.to_hdf(self.dbOps.hdfStoreName, 'REPORT_DATA', append=True, mode='a',
                                  data_columns=['RECORD_STATUS', 'STATEMENT_DATE'])
                # self.dbOps.saveDFToHD5(reportData, 'REPORT_DATA', reconId=self.reconID, execId=self.execID)

    def principle_account_report(self, frame):
        cols = ['AMOUNT', 'AMOUNT_1', 'BORROWER_NAME', 'DEBIT_CREDIT_INDICATOR', 'TXN_MATCHING_STATUS']
        # frame = pd.DataFrame(columns=cols)
        # for source in sources:
        #     frame = pd.concat([frame, reconData[str(source['sourceType'])]], ignore_index=True)
        princAccName = 'NEFT - SETTLEMENT-PRINCIPLEACCT'
        outward_accts = ['NEFT- BRANCH SETTELMENT-ACCOUNT-OUTWARD', 'NEFT- RRB-SETTELMENT-ACCOUNT-OUTWARD',
                         'NEFT-INWARD-REJECTED-ACCOUNT-8006']
        inward_accts = ['NEFT-BRANCH-SETTELMENT-ACCOUNT-INWARD', 'NEFT-INWARD-REJECTED-ACCOUNT-0755(S.C)']

        accountHead = {'NEFT - SETTLEMENT-PRINCIPLEACCT': '07551025399010',
                       'NEFT -PRINCIPLEACCT-N04-RBI-NET-SETTLEMENT': 'N04-RBI-NET-SETTLEMENT',
                       'NEFT-PRINCIPLEACCT-MANUAL': '07551025399010',
                       'NEFT- BRANCH SETTELMENT-ACCOUNT-OUTWARD': 'XXXX1025399010',
                       'NEFT- RRB-SETTELMENT-ACCOUNT-OUTWARD': '07551025398005',
                       'NEFT-INWARD-REJECTED-ACCOUNT-8006': '07551025398006',
                       'NEFT-BRANCH-SETTELMENT-ACCOUNT-INWARD': 'XXXX1061657016',
                       'NEFT-INWARD-REJECTED-ACCOUNT-0755(S.C)': '07551061657016',
                       'NET Oustanding Outward (Unsettled Amount)': 'XXXX1025399010 + 07551025398006',
                       'NET Oustanding Inward (Unsettled Amount)': 'XXXX1061657016',
                       'NET Oustanding Outward (Unmatched Amount)': 'XXXX1025399010 + 07551025398006',
                       'NET Oustanding Inward (Unmatched Amount)': 'XXXX1061657016'}

        accountHeadS = {'NEFT - SETTLEMENT-PRINCIPLEACCT': 1,
                        'NEFT -PRINCIPLEACCT-N04-RBI-NET-SETTLEMENT': 2,
                        'NEFT-PRINCIPLEACCT-MANUAL': 3,
                        'NEFT- BRANCH SETTELMENT-ACCOUNT-OUTWARD': 4,
                        'NEFT- RRB-SETTELMENT-ACCOUNT-OUTWARD': 5,
                        'NEFT-INWARD-REJECTED-ACCOUNT-8006': 6,
                        'NEFT-BRANCH-SETTELMENT-ACCOUNT-INWARD': 7,
                        'NEFT-INWARD-REJECTED-ACCOUNT-0755(S.C)': 8,
                        'NET Oustanding Outward (Unsettled Amount)': 9,
                        'NET Oustanding Inward (Unsettled Amount)': 10}

        # Funds Credited Branchwise in Inward Settlement A/c xxxx1061657016
        frame['COUNT'] = 1
        res = frame[(frame['ACCOUNT_NUMBER'].str.contains('1061657016')) & (frame['SOURCE_TYPE'] == 'S2')]
        res = res[['REF_NUMBER', 'ACCOUNT_NUMBER', 'COUNT', 'AMOUNT_1']].groupby(['REF_NUMBER', 'ACCOUNT_NUMBER']).agg(
            {'COUNT': sum, 'AMOUNT_1': sum}).reset_index()
        res['RECORD_STATUS'] = 'ACTIVE'
        res.to_hdf(self.dbOps.hdfStoreName, 'IW_SOL_DATA', append=True, mode='a', data_columns=['RECORD_STATUS'])

        # Funds Credited Branchwise in Outward Settlement A/c xxxx1025399010
        res = frame[(frame['ACCOUNT_NUMBER'].str.contains('1025399010')) & (frame['SOURCE_TYPE'] == 'S2')]
        res = res[['REF_NUMBER', 'ACCOUNT_NUMBER', 'COUNT', 'AMOUNT']].groupby(['REF_NUMBER', 'ACCOUNT_NUMBER']).agg(
            {'COUNT': sum, 'AMOUNT': sum}).reset_index()
        res['RECORD_STATUS'] = 'ACTIVE'
        res.to_hdf(self.dbOps.hdfStoreName, 'OW_SOL_DATA', append=True, mode='a', data_columns=['RECORD_STATUS'])

        del frame['COUNT']

        # to compute NET Outstanding Outward & Inward Txn
        frame_fil = \
            frame.loc[
                frame[(frame['TXN_MATCHING_STATUS'] == 'UNMATCHED') & (
                    frame['BORROWER_NAME'].isin(outward_accts + inward_accts))].index, cols]
        frame_fil.loc[frame_fil['BORROWER_NAME'].isin(
            outward_accts), 'BORROWER_NAME'] = 'NET Oustanding Outward (Unsettled Amount)'
        frame_fil.loc[
            frame_fil['BORROWER_NAME'].isin(inward_accts), 'BORROWER_NAME'] = 'NET Oustanding Inward (Unsettled Amount)'
        frame = pd.concat([frame, frame_fil], ignore_index=True)

        frame['COUNT'] = 1
        frame.rename(columns={'AMOUNT': "DEBIT_AMT", 'AMOUNT_1': 'CREDIT_AMT', 'BORROWER_NAME': 'ACC NAME'},
                     inplace=True)
        report = frame.pivot_table(index=['ACC NAME'], columns=['DEBIT_CREDIT_INDICATOR']
                                   , aggfunc=({'DEBIT_AMT': np.sum, 'CREDIT_AMT': np.sum, 'COUNT': len}))
        report = report.reset_index(col_level=1)

        cols = []
        for col in report.columns:
            cols.append(col[0] + '_' + col[1])
        report.columns = cols

        for cl in [u'COUNT_C', u'COUNT_D', u'DEBIT_AMT_C', u'DEBIT_AMT_D',
                   u'CREDIT_AMT_C', u'CREDIT_AMT_D']:
            if cl not in report.columns: report[cl] = 0.0
            report[cl].fillna(0.0, inplace=True)

        # Report for all the account number starts here
        allAccRpt = report.copy()

        allAccRpt.loc[allAccRpt['_ACC NAME'].isin(outward_accts), 'CUM_OW_DEBIT'] = allAccRpt.loc[
            allAccRpt['_ACC NAME'].isin(outward_accts), 'DEBIT_AMT_D'].cumsum(skipna=True)

        allAccRpt.loc[allAccRpt['_ACC NAME'].isin(outward_accts), 'DIFF_OW_DR_PRN_CR'] = \
            allAccRpt.loc[allAccRpt['_ACC NAME'] == princAccName, 'CREDIT_AMT_C'].abs().sum() - \
            allAccRpt.loc[allAccRpt['_ACC NAME'].isin(outward_accts), 'CUM_OW_DEBIT']

        allAccRpt.loc[allAccRpt['_ACC NAME'].isin(inward_accts), 'CUM_IW_CREDIT'] = allAccRpt.loc[
            allAccRpt['_ACC NAME'].isin(inward_accts), 'CREDIT_AMT_C'].cumsum(skipna=True)
        # DIFF_IW_CR_PRN_DR
        allAccRpt.loc[allAccRpt['_ACC NAME'].isin(inward_accts), 'DIFF_IW_CR_PRN_DR'] = \
            allAccRpt.loc[allAccRpt['_ACC NAME'].isin(inward_accts), 'CUM_IW_CREDIT'] - \
            allAccRpt.loc[allAccRpt['_ACC NAME'] == princAccName, 'DEBIT_AMT_D'].abs().sum()

        allAccRpt.fillna(0.0, inplace=True)
        allAccRpt.loc[:, 'RECORD_STATUS'] = 'ACTIVE'
        allAccRpt.loc[:, 'STATEMENT DATE'] = self.stmtDate
        allAccRpt.loc[:, 'ACCOUNT HEAD'] = allAccRpt['_ACC NAME'].map(accountHead)
        allAccRpt.loc[:, 'ACCOUNT_SORT'] = allAccRpt['_ACC NAME'].map(accountHeadS)
        allAccRpt.sort_values(by=['ACCOUNT_SORT'], inplace=True)
        allAccRpt = allAccRpt.where((pd.notnull(allAccRpt)), None)
        allAccRpt.to_hdf(self.dbOps.hdfStoreName, 'REPORT_DATA', append=True, mode='a',
                         data_columns=['RECORD_STATUS', 'STATEMENT_DATE'])

        # Report for outstanding transactions
        outStandingRpt = report.copy()[['_ACC NAME', 'DEBIT_AMT_D', 'CREDIT_AMT_C']]
        outstandingAccts = ['NET Oustanding Outward (Unsettled Amount)', 'NET Oustanding Inward (Unsettled Amount)',
                            'NEFT - SETTLEMENT-PRINCIPLEACCT']
        outStandingRpt = outStandingRpt[outStandingRpt['_ACC NAME'].isin(outstandingAccts)]

        settlementAccts = ['NEFT - SETTLEMENT-PRINCIPLEACCT', 'NEFT -PRINCIPLEACCT-N04-RBI-NET-SETTLEMENT',
                           'NEFT-PRINCIPLEACCT-MANUAL']

        outStandingRpt['OPEN_BAL'] = 0.0
        outStandingRpt['CLOSE_BAL'] = 0.0

        qry = self.utils.getPreviousExecutionId(self.reconID)
        self.prevExecID = None
        report_doc = None
        if qry is None:
            outStandingRpt.loc[outStandingRpt[
                                   '_ACC NAME'] == princAccName, 'OPEN_BAL'] = config.principal_open_bal
        report_doc = {}
        if qry != None:
            status, self.prevExecID = self.utils.getCalendarEvent(self.reconID, self.stmtDate)
            if not status:
                raise BaseException('Closing Balance for the previous date doesn"t exits')
            # get previous execution closing balance and make it as current day opening balance
            report_doc = self.mongo_db['dashboard_reports'].find_one(
                {'RECON_ID': self.reconID, 'reportName': 'AB_Reports', 'RECON_EXECUTION_ID': self.prevExecID}
                , sort=[('RECON_EXECUTION_ID', -1)])

            if 'ReportData' in report_doc:
                prev_data = pd.DataFrame(report_doc['ReportData'])
                outStandingRpt.loc[outStandingRpt['_ACC NAME'] == princAccName, 'OPEN_BAL'] = \
                    prev_data.loc[prev_data['_ACC NAME'] == princAccName, 'CLOSE_BAL']

        outStandingRpt.rename(
            columns={'NET Oustanding Outward (Unsettled Amount)': 'NET Oustanding Outward (Unmatched Amount)',
                     'NET Oustanding Inward (Unsettled Amount)': 'NET Oustanding Inward (Unmatched Amount)',
                     'DEBIT_AMT_D': 'NET_DEBIT_AMT', 'CREDIT_AMT_C': 'NET_CREDIT_AMT'},
            inplace=True)
        cond = outStandingRpt['_ACC NAME'] == princAccName
        outStandingRpt.loc[cond, 'NET_DEBIT_AMT'] = report.loc[
            report['_ACC NAME'].isin(settlementAccts), 'DEBIT_AMT_D'].sum()
        outStandingRpt.loc[cond, 'NET_CREDIT_AMT'] = report.loc[
            report['_ACC NAME'].isin(settlementAccts), 'CREDIT_AMT_C'].sum()
        outStandingRpt['NET_OUTSTD_AMT'] = outStandingRpt['NET_CREDIT_AMT'] - outStandingRpt['NET_DEBIT_AMT']
        outStandingRpt.loc[cond, 'CLOSE_BAL'] = outStandingRpt.loc[cond, ['NET_OUTSTD_AMT', 'OPEN_BAL']].sum(axis=1)
        outStandingRpt.loc[~(cond), 'CLOSE_BAL'] = outStandingRpt['NET_OUTSTD_AMT'].loc[~(cond)]
        totalRow = pd.DataFrame([{'_ACC NAME': 'Total Outstanding Balance', 'ACCOUNT HEAD': 'Net Outstanding Balance',
                                  'CLOSE_BAL': outStandingRpt['CLOSE_BAL'].sum(), 'ACCOUNT_SORT': 10}])
        outStandingRpt.loc[:, 'ACCOUNT HEAD'] = outStandingRpt['_ACC NAME'].map(accountHead)
        outStandingRpt.loc[:, 'ACCOUNT_SORT'] = allAccRpt['_ACC NAME'].map(accountHeadS)
        outStandingRpt = pd.concat([outStandingRpt, totalRow], ignore_index=True)
        outStandingRpt = outStandingRpt.where((pd.notnull(outStandingRpt)), None)
        outStandingRpt.sort_values(by=['ACCOUNT_SORT'], inplace=True)
        doc = dict()
        doc['ReportData'] = outStandingRpt.T.to_dict().values()
        doc['RECON_ID'] = self.reconID
        doc['RECON_EXECUTION_ID'] = self.execID
        doc['reportName'] = 'AB_Reports'
        doc['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
        doc['partnerId'] = "54619c820b1c8b1ff0166dfc"
        doc['created'] = self.utils.getUtcTime()
        doc['updated'] = self.utils.getUtcTime()
        rep_col = self.mongo_db['dashboard_reports']
        rep_col.save(doc)

    ## Load delimited file
    def loadDelimFile(self, feedDef, file, names, csvtypes, positions,
                      dtcols, dtpatterns, skipTopRows, skipBottomRows):
        df = pd.DataFrame(columns=names)
        try:
            lines = sum(1 for line in open(file))
            if lines == skipTopRows:
                df = pd.DataFrame(columns=names)
            else:
                df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=names,
                                 converters=csvtypes, skiprows=skipTopRows, usecols=positions,
                                 skipfooter=skipBottomRows, engine='python')



        except Exception, e:
            if str(e) == 'line contains NULL byte':
                try:
                    df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=names,
                                     converters=csvtypes, skiprows=skipTopRows, usecols=positions,
                                     skipfooter=skipBottomRows)
                except:
                    self.logger.error("1Unable to load the file " + file + ".\n"
                                                                           "Please check that file is readable and is "
                                                                           "as per the configured structure.\n"
                                                                           "If problem persists contact Administrator.")
                    raise BaseException("Unable to load the file " + file)
            else:
                self.logger.error("Unable to load the file " + file + ".\n"
                                                                      "Please check that file is readable and is "
                                                                      "as per the configured structure.\n"
                                                                      "If problem persists contact Administrator.")
                raise BaseException("Unable to load the file " + file)
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            for col in dtcols:
                if dtpatterns[col] == '%d%m%Y':
                    df[col] = df[col].astype(str)
                    df['tdlen'] = pd.Series(df[col]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                    del df['tdlen']
                df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')

        return df

    ## Suryodaya Bank IMPS Inward Recon, Reversal rule on CBS & Reversal rule on IMPS Switch
    ## Refer FSD for rule description
    def epymts_61167(self, reconData):
        imps_data = reconData['S1']
        cbs_data = reconData['S2']

        # unq_imps_data = imps_data.drop_duplicates(subset=['REF_NUMBER'])[
        #     ['REF_NUMBER', 'ACCOUNT_NUMBER', 'TRANSACTION_DATE', 'TXT_32_001']]
        # imps_data = pd.merge(imps_data, unq_imps_data,
        #                      on=['REF_NUMBER', 'ACCOUNT_NUMBER', 'TRANSACTION_DATE', 'TXT_32_001'],
        #                      indicator=True, how='left')

        cbs_cr = cbs_data[cbs_data['DEBIT_CREDIT_INDICATOR'] == 'C']

        print imps_data
        pass
        # exit(0)

    def getCBS_RRNo(self, reconData):
        switch_kgb_data = reconData['S1'][['TXT_32_012', 'TXT_10_008']].copy()
        cbs_kgb_data = reconData['S2'].copy()
        switch_kgb_data.rename(columns={'TXT_10_008': 'REF_NUMBER'}, inplace=True)
        cbs_kgb_data = pd.merge(cbs_kgb_data, switch_kgb_data, how='left', on=['REF_NUMBER'])
        reconData['S2'] = cbs_kgb_data
        return reconData, 0

    def cropBReports(self, reconData, stmtDate):
        sampledict = {"bankadjref": "",
                      "flag": "",
                      "shtdat": "",
                      "adjamt shser": "",
                      "shcrd filename": "",
                      "reason": "",
                      "specifyother": ""}
        filLoc = config.appPath + 'recon_reports/' + stmtDate.strftime('%d%m%Y') + os.sep
        if not os.path.isdir(filLoc): os.mkdir(filLoc)
        cols = ['BRANCH_CODE', 'TRANSACTION_DATE', 'DEBIT_CREDIT_INDICATOR', 'AMOUNT', 'ACCOUNT_TYPE', 'TXT_10_003',
                'TXT_10_004',
                'ACCOUNT_NUMBER', 'TXT_180_001', "TXT_32_003", 'TXT_5_015']
        txns = reconData['S1'][cols]
        txns['DEBIT_CREDIT_INDICATOR'] = txns['DEBIT_CREDIT_INDICATOR'].map({'DR': 'D', 'CR': 'C', 'RR': 'R'})
        try:
            txns['txn_date'] = txns['TRANSACTION_DATE'].dt.strftime('%d%m%Y')
        except Exception as e:
            try:
                txns['TRANSACTION_DATE'] = pd.to_datetime(txns['TRANSACTION_DATE'])
                txns['txn_date'] = txns['TRANSACTION_DATE'].dt.strftime('%d%m%Y')
            except:
                print traceback.print_exc()
                raise BaseException('Invalid Date in Transaction Date column')
        txns['val_date'] = txns['txn_date']
        txns['Static value'] = '824603'
        txns['zeroes_3'], txns['zeroes_14'], txns['zeroes_42'] = 3 * '0', 14 * '0', 42 * '0'
        txns['spaces_3'], txns['spaces_7'] = 3 * ' ', 7 * ' '
        txns["DEBIT_CREDIT_INDICATOR"] = txns["DEBIT_CREDIT_INDICATOR"].str.strip()
        txns["AMOUNT"] = txns["AMOUNT"].fillna(0.0)

        colsToFetch = ["TXT_5_005", "TXT_32_003", "REJECTION_CODE", "AMOUNT_3", "AMOUNT_4",
                       "TXT_32_005", "APPROVAL_STATUS", "ITEM_REFERENCE", "ACCOUNT_NUMBER",
                       "TXT_180_001", 'TXT_5_010', "TXT_5_007",
                       "TRANSACTION_DATE", "NUM_16_003", "TXT_60_003", "TXT_60_005"]
        try:
            acquirer = reconData['S2'][colsToFetch]
        except:
            print traceback.print_exc()
            raise BaseException('Missing mandatory from Acquirer file')

        try:
            issuer = reconData['S3'][colsToFetch]
        except:
            print traceback.print_exc()
            raise BaseException('Missing mandatory from Acquirer file')

        for i in ['AMOUNT', 'BRANCH_CODE' 'DEBIT_CREDIT_INDICATOR' 'TXT_10_003' 'TXT_10_004']:
            if i in acquirer.columns: del acquirer[i]
            if i in issuer.columns: del issuer[i]

        acquirerfailed = acquirer[~(acquirer["REJECTION_CODE"] == "00") & ~(acquirer["REJECTION_CODE"] == "RB")]

        # Sreedhar's Way
        data = acquirerfailed.loc[acquirerfailed['TXT_32_003'].isin(txns['TXT_32_003'].unique())]
        txn1 = txns.loc[txns['TXT_32_003'].isin(acquirerfailed['TXT_32_003'].unique())].copy()
        txn1.loc[txn1.index, 'Reversal_TYPE_Manual'] = 'N'
        txn1.loc[txn1['TXT_180_001'].str.contains('Debit REV', case=False), 'Reversal_TYPE_Manual'] = 'Y'
        txn1.loc[txn1['DEBIT_CREDIT_INDICATOR'] == 'R', 'Reversal_TYPE_Manual'] = 'Y'
        df11 = txn1.copy()
        data = pd.DataFrame()
        for g, k in df11[['TXT_32_003', 'AMOUNT', 'Reversal_TYPE_Manual']].groupby(['TXT_32_003', 'AMOUNT']):
            if len(k) == 2:
                k.loc[k[k['Reversal_TYPE_Manual'] == 'Y'].index, 'AMOUNT'] = k['AMOUNT'] * -1
                if k['AMOUNT'].sum() != 0.0:
                    data = pd.concat([k, data], ignore_index=True)
            if len(k) == 1:
                data = pd.concat([k, data], ignore_index=True)
        # print data
        if 'Reversal_TYPE_Manual' in data.columns: del data['Reversal_TYPE_Manual']


        txns["AMOUNT"] = (txns["AMOUNT"] * -1).where(
            (txns["DEBIT_CREDIT_INDICATOR"] == "DR") | (txns["DEBIT_CREDIT_INDICATOR"] == "D"), other=txns["AMOUNT"])
        txnReversals = txns.copy()
        # # Shiva Sir's Way(best way)
        # data = acquirerfailed.merge(txnReversals, how='left', on="TXT_32_003", indicator=False)
        # data["AMOUNT"] = data["AMOUNT"].fillna(0)
        # data = data[["TXT_32_003", "AMOUNT"]].groupby(["TXT_32_003"]).sum().reset_index()
        del txnReversals['AMOUNT']
        txnReversals.drop_duplicates(subset=["TXT_32_003"], keep='last', inplace=True)
        output = data[data["AMOUNT"] != 0][["TXT_32_003", "AMOUNT"]].merge(txnReversals, how='left', on="TXT_32_003",
                                                                           indicator=False)
        temp = acquirerfailed[["TXT_32_003", "REJECTION_CODE", "NUM_16_003"]]
        output = output.merge(temp, how='left', on="TXT_32_003", indicator=False)
        cols103 = ['BRANCH_CODE', 'txn_date', 'val_date', 'DEBIT_CREDIT_INDICATOR', 'Static value', 'AMOUNT',
                   'TXT_10_003', 'TXT_10_004', 'zeroes_42', 'spaces_3', 'zeroes_3', 'spaces_7', 'zeroes_14',
                   'TXT_180_001']
        try:
            output["AMOUNT"] = output["AMOUNT"] * 100
        except:
            print traceback.print_exc()
            raise BaseException('Failed in Amount conversion.')
        #output["AMOUNT"] = output["AMOUNT_4"]#.astype(str).str.replace('.', '').str.replace('-', '')
        output["DEBIT_CREDIT_INDICATOR"] = 'C'

        try:
            cbsRevdf = output[cols103][output["TXT_5_015"].isin(['CBS', 'CBSAADHAR'])]
            cbsRevdf.fillna('', inplace=True)
            cbsRevdf['AMOUNT']=abs(cbsRevdf['AMOUNT'])
            for cl, wdth in {'BRANCH_CODE': 4, 'AMOUNT': 14, 'TXT_10_003': 6, 'TXT_10_004': 6,
                             'TXT_180_001': 35}.iteritems():
                cbsRevdf[cl] = cbsRevdf[cl].astype(str)
                cbsRevdf[cl] = cbsRevdf[cl].apply(
                    lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
            cbsRevdf = cbsRevdf.apply(lambda row: ''.join(map(str, row)), axis=1)
            with open(filLoc + '/ReversalCBStransactions.csv', "w") as fwf:
                for record in cbsRevdf.values.tolist():
                    fwf.write(record + '\n')
        except:
            print traceback.print_exc()
            raise BaseException('Failed at Fetching Old CBS Reversal txns')

        try:
            cols103 = ['BRANCH_CODE', 'DEBIT_CREDIT_INDICATOR', 'ACCOUNT_NUMBER', 'AMOUNT', 'TXT_180_001',
                       'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2']
            output['static_value1'] = 'N'
            output['blank1'], output['blank2'], output['blank3'], output['blank4'], output['blank5'], = '', '', '', '', ''
            output['static_value2'] = 'Y'
            print output.columns
            rvrfin = output[cols103][(output["TXT_5_015"].isin(['FINNACLE']))]
            rvrfin["AMOUNT"] = abs(rvrfin["AMOUNT"]/100.0)
            rvrfin["AMOUNT"]= rvrfin["AMOUNT"].apply(lambda x: '{0:.2f}'.format(x))
            cols103renames = {'BRANCH_CODE': 'Branch code', 'DEBIT_CREDIT_INDICATOR': 'Credit/Debit Flag',
                              'ACCOUNT_NUMBER': 'Acc no', 'AMOUNT': 'Amt', 'TXT_180_001': 'Narration',
                              'static_value1': 'static_value', 'static_value2': 'static_value', 'blank1': 'blank',
                              'blank2': 'blank', 'blank3': 'blank', 'blank4': 'blank', 'blank5': 'blank'}
            rvrfin.rename(columns=cols103renames, inplace=True)
            rvrfin.to_csv(filLoc + "ReversalFinacletransactions.csv", index=False)
        except:
            print traceback.print_exc()
            raise BaseException('Failed at Fetching Finacle Reversal txns')
            # 102 & 103 Report for Issuer Txns
        issuerrb = issuer[issuer["REJECTION_CODE"] == "RB"]
        import collections
        for i, v in collections.Counter(issuerrb.columns.tolist()).iteritems():
            if v > 1:
                issuerrb.drop(i, axis=1, inplace=True)

        # Begin Added by Shiva for 102 103 NPCI Upload Report
        rblist = []
        for index, row in issuerrb.iterrows():
            print row
            x = {}
            x["bankadjref"] = row["TXT_32_003"]
            x["flag"] = ""
            x["shtdat"] = datetime.strptime(row["TXT_60_003"], '%m%d%y').strftime('%Y-%m-%d')
            x["adjamt"] = row["AMOUNT_4"] / 100
            x["shser"] = row["TXT_32_003"]
            x["shcrd"] = row["TXT_32_005"]
            x['partnerId'] = "54619c820b1c8b1ff0166dfc"
            x["filename"] = "corp_upload_" + datetime.strptime(row["TXT_60_003"], '%m%d%y').strftime('%Y%m%d') + ".csv"
            # if x["Difference"] == 0:
            x["reason"] = "103"
            # else:
            #    x["reason"] = "103"
            x["specifyother"] = ""
            rblist.append(x)
        self.mongo_db = self.mongo_client[self.propsHelper.getProperty('MONGODB_NAME_APP')]
        coll = self.mongo_db['upifailedtransactionreport']
        coll.remove({"shtdat": rblist[0]["shtdat"]})
        for x in rblist:
            coll.insert(x)
        rblist = []
        issuerrb["AMOUNT_4"] = issuerrb["AMOUNT_4"] / 100  # Since Transaction Amount in paisa.
        # End Added by Shiva for 102 103 NPCI Upload Report
        cols102 = ['BRANCH_CODE', 'txn_date', 'val_date', 'DEBIT_CREDIT_INDICATOR', 'Static value', 'AMOUNT',
                   'TXT_10_003', 'TXT_10_004',
                   'ACCOUNT_NUMBER_y', 'zeroes_42', 'spaces_3', 'zeroes_3', 'spaces_7', 'zeroes_14', 'TXT_180_001_y',"AMOUNT_4"]

        if 'TXT_5_015' in issuerrb.columns: del issuerrb['TXT_5_015']
        try:
            txns['DEBIT_CREDIT_INDICATOR'] = txns.apply(
                lambda x: "D" if x["TXT_180_001"].startswith("UPI Debit REV") else x['DEBIT_CREDIT_INDICATOR'], axis=1)
            txns = txns[txns['DEBIT_CREDIT_INDICATOR'] == 'C']
            issuerrb_cbs = issuerrb.merge(txns, how='left', on="TXT_32_003", indicator=True)
            issuerrb_cbs["AMOUNT"] = issuerrb_cbs["AMOUNT"].fillna(0)

            issuerrb_cbs["Difference"] = issuerrb_cbs["AMOUNT_4"] + issuerrb_cbs["AMOUNT"]
            issuerrb_cbs["AMOUNT"] = issuerrb_cbs["AMOUNT"] * 100  # converting to paise, required format.
            issuerrb_cbs["AMOUNT"] = issuerrb_cbs["AMOUNT"].astype(str).str.replace('.', '').str.replace('-', '')

        except:
            print traceback.print_exc()
            raise BaseException('Failed at matching Issuer txns with CBS txns')

        # OLD CBS
        try:
            cbsDf = issuerrb_cbs[cols102][
                (issuerrb_cbs["_merge"] == 'both') & (issuerrb_cbs["TXT_5_015"].isin(['CBS', 'CBSAADHAR']))]
            if len(cbsDf):
                cbsDf.fillna('', inplace=True)
                cbsDf["AMOUNT"] = cbsDf["AMOUNT_4"].apply(lambda x: '{0:.2f}'.format(x))
                for cl, wdth in {'BRANCH_CODE': 4, 'AMOUNT': 14, 'TXT_10_003': 6, 'TXT_10_004': 6,
                                 'TXT_180_001_y': 35}.iteritems():
                    cbsDf[cl] = cbsDf[cl].astype(str)
                    cbsDf[cl] = cbsDf[cl].apply(
                        lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
            cbsDf = cbsDf.apply(lambda row: ''.join(map(str, row)), axis=1)

            with open(filLoc + '/issuer102transactionscbs.csv', "w") as fwf:
                for record in cbsDf.values.tolist():
                    fwf.write(record + '\n')
        except:
            print traceback.print_exc()
            raise BaseException('Failed in Converting to Fixed length')

        # Finnacle
        try:
            cols102 = ['BRANCH_CODE', 'DEBIT_CREDIT_INDICATOR', 'ACCOUNT_NUMBER_y', 'AMOUNT', 'TXT_180_001_y',
                       'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2',"AMOUNT_4"]
            #issuerrb_cbs["AMOUNT"] = (issuerrb_cbs["AMOUNT"].astype(float) / 100.0)
            issuerrb_cbs["AMOUNT"] = issuerrb_cbs["AMOUNT_4"].apply(lambda x: '{0:.2f}'.format(x))
            issuerrb_cbs['static_value1'] = 'N'
            issuerrb_cbs['blank1'], issuerrb_cbs['blank2'], issuerrb_cbs['blank3'], issuerrb_cbs[
                'blank4'], issuerrb_cbs['blank5'], = '', '', '', '', ''
            issuerrb_cbs['static_value2'] = 'Y'

            cols103renames = {'BRANCH_CODE': 'Branch code', 'DEBIT_CREDIT_INDICATOR': 'Credit/Debit Flag',
                              'ACCOUNT_NUMBER': 'Acc no', 'txn_date': 'Txn date', 'val_date': 'Value date'
                , 'TXT_10_003': 'GL no', 'TXT_10_004': 'Sub GL', 'ACCOUNT_NUMBER_y': 'Acc no',
                              'ACCOUNT_NUMBER_x': 'Acc no', 'AMOUNT_4': 'Amount (with paisa)', 'TXT_180_001': 'Narration',
                              'TXT_180_001_x': 'Narration', 'TXT_180_001_y': 'Narration',
                              'static_value1': 'static_value', 'static_value2': 'static_value', 'blank1': 'blank',
                              'blank2': 'blank', 'blank3': 'blank', 'blank4': 'blank', 'blank5': 'blank'}
            iss102txnFin = issuerrb_cbs[cols102][(issuerrb_cbs["_merge"] == 'both') &
                                                 (issuerrb_cbs["TXT_5_015"].isin(
                                                     ['FINNACLE']))]  # (issuerrb_cbs["Difference"] == 0) &
            iss102txnFin.rename(columns=cols103renames, inplace=True)
            iss102txnFin.to_csv(filLoc + "issuer102transactions.csv", index=False)
        except Exception as e:
            print traceback.print_exc()
            raise BaseException('Failed in generating Issuer 102 transactions')

        issuerrb_cbs = issuerrb_cbs[(issuerrb_cbs["_merge"] != 'both')]

        #issuerrb_cbs["AMOUNT"] = (issuerrb_cbs["AMOUNT_4"].astype(float) / 100.0)
        issuerrb_cbs["AMOUNT"] = issuerrb_cbs["AMOUNT_4"].apply(lambda x: '{0:.2f}'.format(x))
        issuerrb_cbs['static_value1'] = 'N'
        issuerrb_cbs['blank1'], issuerrb_cbs['blank2'], issuerrb_cbs['blank3'], issuerrb_cbs[
            'blank4'],  issuerrb_cbs['blank5'], = '', '', '', '', ''
        issuerrb_cbs['static_value2'] = 'Y'

        iss103txn = issuerrb_cbs[cols102]
        iss102txnFin.rename(columns=cols103renames, inplace=True)
        iss102txnFin.to_csv(filLoc + "issuer103transactions.csv", index=False)
        '''colsrename = {"REJECTION_CODE": "Response Code ", "TXT_32_010": "Acquirer Settlement Amount ",
                      "TXT_32_013": "BENEFICIARY NUMBER ", "TXT_32_014": "REMITTER NUMBER ",
                      "ACCOUNT_NUMBER": "ACCOUNT NUMBER ", "NUM_16_001": "From Account Type ",
                      "NUM_16_003": "Transaction Time ", "NUM_16_002": "To Account Type ",
                      "NUM_16_005": "Acquirer Stl Proc Fee ", "NUM_16_004": "Acquirer Stl Fee ",
                      "NUM_16_006": "Acquirer Stl Conv Rate ", "APPROVAL_STATUS": "Approval Number ",
                      "TXT_10_005": "Acquirer Stl Curr Code ", "TXT_5_004": "Merchant Category Code ",
                      "TXT_5_005": "Transaction Type ", "TXT_5_006": "Member Number ",
                      "TXT_5_007": "Payee PSP code ","TRANSACTION_DATE_x":'Transaction Date',
                      "TXT_5_003": "Participant ID ", "AMOUNT_3": "Transaction Amount ",
                      "AMOUNT_4": "Actual Transaction Amount ", "ADDRESS_LINE1": "Trans Activity Fee ",
                      "TXT_32_009": "Aquirer ID ", "TXT_32_008": "Acquirer Settlement Date ",
                      "TXT_32_007": "Customer refernce Number ", "TXT_32_005": "PAN Number ",
                      "TXT_32_003": "Transaction Serial Number ", "TRANSACTION_DATE": "Transaction Date ",
                      "TXT_180_004": "CARDHOLDER BILL CURRENCY ", "TXT_180_005": "Acquirer Settlement Date ",
                      "TXT_180_009": "Acquirer Stl Conv Rate ", "TXT_180_001": "REMARKS ",
                      "TXT_5_009": "Acquirer Stl Curr Code ", "ITEM_REFERENCE": "System Trace Audit Number ",
                      "TXT_5_012": "Transaction Currency Code ", "TXT_5_011": "Card Acceptor Term. Location ",
                      "TXT_5_010": "Payer PSP code ", "BRANCH_CODE": "IFSC CODE ",
                      "TXT_60_006": "ORIGINAL CHANNEL ",
                      "TXT_60_004": "Card Acceptor ID ", "TXT_60_005": "Card Acceptor Terminal ID ",
                      "TXT_60_002": "UPI reference code ", "TXT_60_003": "Card Acceptor Settl Date ",
                      "TXT_60_001": "UPI Transaction ID "}
        for i in ['FEED_FILE_NAME', 'TXT_5_015', 'TRANSACTION_DATE_y', 'SOURCE_TYPE', 'OBJECT_ID', 'LINK_ID',
                  'RECORD_VERSION', 'ENTRY_TYPE', 'CARRY_FORWARD_FLAG']:
            if i in issuerrb_cbs.columns: del issuerrb_cbs[i]
        #issuerrb_cbs.rename(columns=colsrename, inplace=True)
        issuerrb_cbs.rename(columns=cols103renames, inplace=True)
        del issuerrb_cbs['Difference'],issuerrb_cbs['_merge']
        issuerrb_cbs.to_csv(filLoc + "issuer103transactions.csv", index=False)'''
        return reconData

    def abDupReport(self, reconData, cols):
        df = reconData['S2']
        df['RECON_ID'] = str(self.reconID)
        df = df[cols].copy()
        df['COUNT'] = 1
        report = df.groupby(cols).sum().reset_index()
        report['RECORD_STATUS'] = 'ACTIVE'
        results = report[report['COUNT'] >= 2]
        print results.head()
        results.to_hdf(self.dbOps.hdfStoreName, 'DUP_UTR', append=True, mode='a',
                       data_columns=['RECORD_STATUS'])

