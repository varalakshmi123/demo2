import os
import sys
import time
from datetime import datetime

import schedule


class MongoBackScheduler():
    def __init__(self, bk_status):
        self.bk_status = bk_status
        self.bk_dir = '/home/hduser/erecon_data/mongo_backup'

    # TODO check if job is alredy scheduled
    def run_job(self):
        # check if job has already scheduled or not
        if False:
            print 'Job already scheduled'
            sys.exit(0)
        else:
            self.schedule_job()

    def schedule_job(self):
        schedule.every().days.at('18:53').do(self.initiate_ereconmdb_bk)
        schedule.every().days.at('18:54').do(self.initiate_rbmdb_bk)
        while True:
            schedule.run_pending()
            time.sleep(1)

    def initiate_ereconmdb_bk(self):
        self.remove_files('erecon')
        export_cmd = 'mongodump -d erecon -o ' \
                     '{}/erecon/erecon_{}'. \
            format(self.bk_dir, datetime.now().strftime('%d%b%Y_%H%M'))
        os.system(export_cmd)

    def initiate_rbmdb_bk(self):
        self.remove_files('reconbuilder')
        export_cmd = 'mongodump -d reconbuilder -o ' \
                     '{}/reconbuilder/reconbuilder_{}'. \
            format(self.bk_dir, datetime.now().strftime('%d%b%Y_%H%M'))
        os.system(export_cmd)

    # removes any files in specified folder older than 7 days
    def remove_files(self, dump_name):
        now = time.time()
        #cutoff = now - (7 * 86400)
        cutoff = now
        folders = os.listdir("{}/{}/".format(self.bk_dir, dump_name))
        for folder in folders:
            t = os.stat(self.bk_dir + os.sep + folder)
            c = t.st_ctime
            if c < cutoff:
                os.removedirs("{}/{}/{}".format(self.bk_dir, dump_name, folder))


if __name__ == '__main__':
    bkJob = MongoBackScheduler(sys.argv[1])
    bkJob.run_job()
