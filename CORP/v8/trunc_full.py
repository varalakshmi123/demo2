#import cx_Oracle
import sys
from DBOps import DBOps

class TruncFull():
    def __init__(self, reconID):
        self.reconID = reconID
        self.connection = self.getConnection()[1]
        self.truncFull()
        #self.delReconDynData()

    def getConnection(self):
        connection = DBOps().getConn()
        # dsn_tns = cx_Oracle.makedsn('localhost', 1521, 'XE')
        # connection = cx_Oracle.connect('algoreconutil_new', 'algorecon', dsn_tns)
        # begin new transaction
        #connection.begin()
        return True, connection

    def delReconDynData(self):
        qry = "delete from recon_dynamic_data_model where recon_id = '" + self.reconID + "'"
        self.connection.cursor().execute(qry)
        self.commit()
        print("Recon Dynamic Data truncated !!!")


    def truncFull(self):

        # execution 2 - to delete from exception_allocation
        qry1 = "delete from exception_allocation where exception_id in (select exception_id from exception_master where link_id in (select link_id from cash_output_txn where recon_id='l_recon_id'))"

        # execution 2 - to delete from exception_master
        qry2 = "delete from exception_master where link_id in (select link_id from cash_output_txn where recon_id='l_recon_id')"

        # execution 3 - to delete from output table
        qry3 = "delete from cash_output_txn where recon_id='l_recon_id'"

        # execution 4 - to delete from input table
        #qry4 = "delete from cash_input_txn where recon_id='l_recon_id'"

        # execution 5 - to delete from nostro_recon_positions
        qry4 = "delete from RECON_POSITIONS where recon_id='l_recon_id'"

        # execution 6 -  to delete from recon_execution_details_log
        qry5 = "delete from recon_execution_details_log where recon_id='l_recon_id'"

        # execution 7 -  to delete from recon_feed_processing_details
        #qry6 = "delete from recon_feed_processing_details where recon_id='l_recon_id'"

        try:
            for ixd in range(1, 6):
                print(eval('qry' + str(ixd)).replace('l_recon_id',self.reconID))
                self.connection.cursor().execute(eval('qry' + str(ixd)).replace('l_recon_id',self.reconID))
            self.commit()
            print("Full trunc completed !!!")
            print("########################")
        except Exception as excep:
            print("error")
            print(excep)

    def commit(self):
        self.connection.commit()

if __name__ == '__main__':
    print("########################")
    reconId = sys.argv[1]
    print("Full trunc on recon" + str(reconId))
    TruncFull(reconId)
