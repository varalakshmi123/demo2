dealTallyReconIds = ['TRE_CASH_APAC_61184', 'TRE_CASH_APAC_61182', 'TRE_CASH_APAC_61183',
                     'TRE_CASH_APAC_61190', 'TRE_CASH_APAC_61185', 'TRE_CASH_APAC_61186',
                     'TRE_CASH_APAC_61198', 'TRE_CASH_APAC_61199', 'TRE_CASH_APAC_61203',
                     'TRE_CASH_APAC_61196', 'TRE_CASH_APAC_61191', 'TRE_CASH_APAC_61192',
                     'TRE_CASH_APAC_61176', 'TRE_CASH_APAC_61175', 'TRE_CASH_APAC_61201']

dealTallyKptpFeedName = ['KPTP SWAP Feed File', 'KPTP SPOT Feed File', 'KPTP Forward Data Feed File']

t_run_recon = ['TRSC_CASH_APAC_20181']

ignoreCarryFwdRecons = ['EPYMTS_CASH_APAC_0012', 'CB_CASH_APAC_61171']

chunckSize = 10000
enginePath = '/usr/share/nginx/v8/'
appPath = '/usr/share/nginx/www/erecon/ui/app/files/'
flaskDir = '/usr/share/nginx/www/erecon/flask/'
# hdfFilesPathNew = '/usr/share/nginx/v8/zfs/ReconData/'
hdfFilesPathNew = '/erecon/'
zfsBase = "erecon/"
zfsBaseName = "erecon"

system_columns = [ 'AUTHORIZED_BY', 'BUSINESS_CONTEXT_ID', 'CARRY_FORWARD_FLAG', 'COMMENTS', 'CREATED_BY', 'CREATED_DATE',
  'ENTRY_TYPE', 'EXECUTION_DATE_TIME', 'FEED_FILE_NAME',
 'IPADDRESS', 'LINK_ID', 'MATCHING_EXECUTION_MODE',  'MATCHING_RULE_CALL',
 'MATCHING_STATUS',  'OBJECT_OID', 'RAW_LINK', 'RECON_EXECUTION_ID', 'RECON_ID', 'RECORD_VERSION', 'SOURCE_NAME', 'SOURCE_TYPE',
 'STATEMENT_DATE', 'TXN_MATCHING_STATE', 'TXN_MATCHING_STATUS', 'UPDATED_BY', 'UPDATED_DATE',
  'RECORD_STATUS']


incrementalRecons = ['TRSC_CASH_APAC_20181', 'EPYMTS_NEFT_INWARD_APAC_0009', 'EPYMTS_NEFT_OUTWARD_APAC_0010',
                     'EPYMTS_NEFT_INWARD_APAC_0011']
eventLogRecons =['EPYMTS_CASH_APAC_0012']
principal_open_bal = -4282885.24


