#from io import StringImport 
import pandas as pd
from bson import ObjectId
import numpy as np
import os
import zipfile
import fnmatch
import datetime
import pymongo
import sys
import json

# Function to Load Fixed Length files
class FeedParser:

    def __init__(self,feedtype):
        self.feedtype=feedtype
        self.names=[]
        self.positions=[]
        self.typedic={}
        f=open("/usr/share/nginx/v8/scripts/"+feedtype+".csv")
        df=pd.read_csv(f)
        #print df.columns

        df=df[["From ","To ","Description "]]
        for index,row in df.iterrows():
            self.positions.append((row["From "]-1,row["To "]))
            self.names.append(row['Description '])
            self.typedic[row['Description ']]=str


    def loadFLFile(self, file, csvtypes,dtcols, dtpatterns, skipTopRows, skipBottomRows):
        df = pd.DataFrame(columns=self.names)

        try:
            #lines = sum(1 for line in open(file))
            #if lines == skipTopRows:
            #    df = pd.DataFrame(columns=self.names)
            #else:
            #for x in zip(self.names,self.positions):
            #    print x
            df = pd.read_fwf(file, header=None, colspecs=self.positions, names=self.names,
                             converters=self.typedic, skiprows=skipTopRows,
                             skipfooter=skipBottomRows)
                             #engine='python')
                #df.to_csv('npci_dump.csv')
        except Exception as e:
            print e
            print("Unable to load the file " + file + ".\n"
                                                                  "Please check that file is readable and is "
                                                                  "as per the configured structure.\n"
                                                                  "If problem persists contact Administrator.")
            raise
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            for col in dtcols:
                if dtpatterns[col] == '%d%m%Y':
                    df[col] = df[col].astype(str)
                    df['tdlen'] = pd.Series(df[col]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                    del df['tdlen']
                df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')  # , errors='coerce'
        print("Records in file  = %6d" % len(df))
        # print df.head()
        return df

    ## Get fixed length start and end
    def getFLPositions(self, feedDef):
        positions = []
        for col in feedDef['fieldDetails']:
            start = int(col['startIndex']) - 1
            end = int(col['endIndex'])
            positions.append((start, end))
        return positions

class UPIRecon:
    def __init__(self):
        self.files={"RAW":"UPIRAW*.zip","MERCHANT":"UPIMERCHANT*.zip","SWITCH":"SWITCH*.csv","FINACLE":"FINACLE.lst","LEGACYCBS":""}



class Decompress:

    def __init__(self):
        pass


    def decompressAndLoad(self,file,payload):
        with zipfile.ZipFile(file, "r") as f:
            for name in f.namelist():
                data=f.read(name)
                print name,len(data)
                payload[name]=data
        return payload

    def recursive_glob(self,rootdir='.', pattern='*'):
        """Search recursively for files matching a specified pattern.

        Adapted :from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
        """
        matches = []
        for root, dirnames, filenames in os.walk(rootdir):



            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
        return matches

    def mergeByPrefix(self,pattern,resultkey,payload):
        for key in fnmatch.filter(payload.keys(),pattern):
            if key==resultkey:
                continue
            if resultkey in payload:
                payload[resultkey]=payload[resultkey]+os.linesep+payload[key]
            else:
                payload[resultkey]=payload[key]
            del payload[key]
        return payload


def createNPCIRecord(row):
    x = {}
    x["bankadjref"] = row["UPI RRN"]
    x["flag"] = ""
    x["shtdat"] = datetime.datetime.strptime(row['Transaction Date '], '%m%d%y').strftime('%Y-%m-%d')
    x["adjamt"] = abs(row["AMOUNT"]) 
    x["shser"] = row["UPI RRN"]
    x["shcrd"] = row["ACCOUNT 2 NUMBER "]
    x['partnerId'] = "54619c820b1c8b1ff0166dfc"
    x["filename"] = "corp_upload_" + datetime.datetime.strptime(row['Transaction Date '], '%m%d%y').strftime(
        '%Y%m%d') + ".csv"
    # if x["Difference"] == 0:
    x["reason"] = str(row["StatusCode"])
    x["specifyother"] = ""
    x["statementdate"] = statementdate
    return x


if __name__ == "__main__":
    #fp=FeedParser("Issuer")
    #df=fp.loadFLFile("UPIRAWDATAISSCRB010617_1CConsolidated.mCRB",None,[],None,0,0)
    #df.to_csv("UPIRAWDATAISSCRB010617_1CConsolidated.mCRB.csv")
    #rootdir='/Users/Shiva/Downloads/Algofusion/Data Files-1/1Jun/'
    statementdate='28052019'#"31072017"
    basedir="/usr/share/nginx/v8/mft/CORP/"
    outdir="/usr/share/nginx/www/erecon/ui/app/files/recon_reports/"
    #statementdate = "25092017"
    #basedir = "/Users/Shiva/Downloads/"
    #outdir = "output/"
    rootdir=basedir+statementdate+'/'
    outputdir=outdir+statementdate+'/'
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    d=Decompress()
    mongoHost = "localhost"
    mongoclient=pymongo.MongoClient(host=mongoHost)
    db=mongoclient["erecon"]
    #zip_ref = zipfile.ZipFile(rootdir+"NETWORK FILE.zip", 'r')
    #zip_ref.extractall(rootdir)
    #zip_ref.close()
    #root="/Users/Shiva/Downloads/Algofusion/Data Files-1/1Jun/NETWORK FILE"
    files=d.recursive_glob(rootdir,"UPI*.mCRB.zip")
    payload={}
    for f in files:
        d.decompressAndLoad(f,payload)
    d.mergeByPrefix("UPIRAWDATAACQ*.mCRB","UPIRAWDATAACQCRB",payload)
    f=open(rootdir+"UPIRAWDATAACQCRB","w+")
    f.write(payload["UPIRAWDATAACQCRB"])
    f.close()
    fp = FeedParser("RAWAcquirer")
    rawacquirer = fp.loadFLFile(rootdir+"UPIRAWDATAACQCRB", None, [], None, 0, 0)
    #print(acquirer[acquirer["Transaction Serial Number "]=='725222208028'].head(1).to_json(orient="records"))
    acquirercb = rawacquirer[
        ["Participant ID ", "Transaction Type ", "Transaction Serial Number ", "Response Code ", "UPI Transaction ID ",
         "Transaction Date ", "IFSC CODE ","ACCOUNT NUMBER ","PAN Number ","Transaction Amount "]]

    acquirer=rawacquirer[["Participant ID ","Transaction Type ","Transaction Serial Number ","Response Code ","UPI Transaction ID ","Transaction Date ","Transaction Amount ","PAN Number "]]
    #acquirer.to_csv(outputdir+"acquirerconsolidated.csv")
    d.mergeByPrefix("UPIRAWDATAISS*.mCRB", "UPIRAWDATAISSCRB", payload)
    f = open(rootdir+"UPIRAWDATAISSCRB", "w+")
    f.write(payload["UPIRAWDATAISSCRB"])
    f.close()
    issfp = FeedParser("Issuer")
    issuer = issfp.loadFLFile(rootdir+"UPIRAWDATAISSCRB", None, [], None, 0, 0)
    issuer.to_csv(outputdir+"issuerconsolidated.csv")
    d.mergeByPrefix("UPIMERCHANTRAWDATAACQ*.mCRB", "UPIMERCHANTRAWDATAACQCRB", payload)
    f = open(rootdir+"UPIMERCHANTRAWDATAACQCRB", "w+")
    f.write(payload["UPIMERCHANTRAWDATAACQCRB"])
    f.close()
    macqfp=FeedParser("MerchantAcquirer")
    merchantacquirer=macqfp.loadFLFile(rootdir+"UPIMERCHANTRAWDATAACQCRB",None,[],None,0,0)

    merchantacquirer=merchantacquirer[["Participant ID ","Transaction Type ","Transaction Serial Number ","Response Code ","UPI Transaction ID ",
                                       "Transaction Date ","Transaction Amount ","REMITTER NUMBER "]]
    merchantacquirer.rename(columns={'REMITTER NUMBER ': "PAN Number "}, inplace=True)

    acquirer=pd.concat([acquirer,merchantacquirer])
    #zip_ref = zipfile.ZipFile(rootdir + "Legacy CBS.zip", 'r')
    filelist= d.recursive_glob(rootdir,"*P2*.dat")
    #zip_ref.extractall(rootdir)
    #zip_ref.close()
    oldcbsheaders=      ["Sr. no.","Date","CBS RRN","DRCRIND","BRANCH CODE","ACCOUNT TYPE","A/C SUB TYPE","A/C NO.","IFSC Code","AMOUNT","ERROR CODE","BRANCH CODE","BATCH NO.","REVERSAL BRANCH CODE","REVERSAL BATCH NO.","GL","SUB GL","TRANSACTION TYPE","REMARKS","REMARKS 2"]
    oldcbsaadharheaders=["Sr. no.","Date","CBS RRN","DRCRIND","AADHAAR NO.","BRANCH CODE","ACCOUNT TYPE","A/C SUB TYPE","A/C NO.","IFSC Code","AMOUNT","ERROR CODE","BRANCH CODE","BATCH NO.","REVERSAL BRANCH CODE","REVERSAL BATCH NO.","GL","SUB GL","TRANSACTION TYPE","REMARKS","REMARKS 2"]
    finacleheaders=["BANK TRANSACTION ID","UPI RRN","TRANSACTION TYPE","AMOUNT IN PAISA","DATE","TIME","BRANCH CODE","","DRCRIND","GL","Bank Code","","REMARKS","Finacle tran ID","Debit Account Number","Credit ACCOUNT NO.","REMARKS","Merchant code","Split Transaction","Entered Time","Posted Time"]
    switchheaders=["35 Digit Transaction ID","message ID","NPCI UPI RRN","CBS RRN","Date and Time of transaction","Remitter Mobile number","Remitter Name","Beneficiary Details","Beneficiary name","amount in paise","Remitter Account Number","Pooling Account number","Beneficiary Account Number","Transaction Status","Issuer/Acquirer","Transaction type","Intra Bank Flag","Request Source","Payer PSP","Payee PSP","Issuer bank Code","Acquirer bank Code","EMPTY3","EMPTY5","EMPTY6","Response Code","Date and Time of Transaction","Split transaction flag","EMPTY","EMPTY1","Merchant Code","Transaction mode","transaction count"]

    for f in filelist:
        if f[-1] == '/':
            continue
        else:
            if "P2U.dat" in f:
                typedic = {}
                for x in oldcbsaadharheaders:
                    typedic[x]=str
                df=pd.read_csv(f,names=oldcbsaadharheaders,sep='|',dtype=typedic)
                payload["AADHARCBS"]=df

            elif "P2A.dat" in f:
                typedic = {}
                for x in oldcbsheaders:
                    typedic[x] = str
                df=pd.read_csv(f,names=oldcbsheaders,sep='|',dtype=typedic)
                payload["CBS"]=df
                #print df.head(20)
    #typedic = {}
    #for x in switchheaders:
    #    typedic[x] = str
    #switch =pd.read_csv(rootdir+"SWITCH1.csv",names=switchheaders,sep='|',skiprows=1,quoting=3,dtype=typedic)
    #payload["SWITCH"]=switch
    typedic = {}
    for x in finacleheaders:
        typedic[x] = str
    finacle = pd.read_csv(rootdir + "/FINACLE.lst", names=finacleheaders,sep='|',dtype=typedic)
    payload["FINNACLE"] = finacle
    payload["FINNACLE"].to_csv("finna.csv")
  
    payload["FINNACLE"]["AMOUNT IN PAISA"].fillna('',inplace=True)
    print payload["FINNACLE"]["AMOUNT IN PAISA"].str.contains('C').unique() 
    payload["FINNACLE"]["AMOUNT"]=payload["FINNACLE"]["AMOUNT IN PAISA"].astype(long)/100
    payload["FINNACLE"]["Debit Account Number"].fillna('',inplace=True)
    payload["FINNACLE"]["Credit ACCOUNT NO."].fillna('', inplace=True)
    payload["FINNACLE"]["A/C NO."]=payload["FINNACLE"]["Debit Account Number"]+payload["FINNACLE"]["Credit ACCOUNT NO."]
    payload["AADHARCBS"]["UPI RRN"]=payload["AADHARCBS"]["REMARKS"].str.extract("^RRN ([0-9]*)/")
    #print payload["AADHARCBS"][["UPI RRN","AMOUNT", "REMARKS"]].head(50)
    payload["CBS"]["UPI RRN"] = payload["CBS"]["REMARKS"].str.extract("^RRN ([0-9]*)/")

    payload["AADHARCBS"]["AMOUNT"]=payload["AADHARCBS"]["AMOUNT"].str.strip()
    payload["AADHARCBS"]["AMOUNT"]=payload["AADHARCBS"]["AMOUNT"].astype(float) / 1
    #payload["CBS"]["AMOUNT"] = payload["CBS"]["AMOUNT"].str.strip().astype(int) / 1
    payload["CBS"]["AMOUNT"] = payload["CBS"]["AMOUNT"].str.strip()
    payload["CBS"]["AMOUNT"] = payload["CBS"]["AMOUNT"].astype(float) / 1
    payload["AADHARCBS"]["DRCRIND"]=payload["AADHARCBS"]["DRCRIND"].str.strip()
    payload["CBS"]["DRCRIND"] = payload["CBS"]["DRCRIND"].str.strip()
    payload["AADHARCBS"]["CBSSOURCE"]="OLDCBSAADHAR"
    payload["CBS"]["CBSSOURCE"] ="OLDCBS"
    payload["FINNACLE"]["CBSSOURCE"]="FINNACLE"
    try:
        payload["FINNACLE"]['DRCRIND'] = payload["FINNACLE"].apply(
            lambda x: "R" if x["REMARKS"].startswith("UPI Debit REV") else x['DRCRIND'], axis=1)
    except:
        payload["FINNACLE"][["BANK TRANSACTION ID","UPI RRN", "AMOUNT","REMARKS"]].to_csv("FailedFile.csv")
    transactions=payload["AADHARCBS"][["UPI RRN","AMOUNT","A/C NO.","DRCRIND","CBSSOURCE","BRANCH CODE"]]
    transactions=transactions.append(payload["CBS"][["UPI RRN","AMOUNT","A/C NO.","DRCRIND","CBSSOURCE","BRANCH CODE"]],ignore_index=True)
    transactions["BANK TRANSACTION ID"]=""
    transactions=transactions.append(payload["FINNACLE"][["BANK TRANSACTION ID","UPI RRN", "AMOUNT","A/C NO.","DRCRIND","CBSSOURCE","BRANCH CODE"]], ignore_index=True)
    payload["FINNACLE"][["BANK TRANSACTION ID", "UPI RRN", "AMOUNT", "A/C NO.", "DRCRIND", "CBSSOURCE","BRANCH CODE"]].to_csv("Finnacle.csv")
    transactions["DRCRIND"]=transactions["DRCRIND"].str.strip()
    transactions["AMOUNT"]=transactions["AMOUNT"].fillna(0)

    transactions["AMOUNT"]=(transactions["AMOUNT"]*-1).where((transactions["DRCRIND"]=="Debit") | (transactions["DRCRIND"]=="D"),other=transactions["AMOUNT"])
    acquirertransacations=transactions[(transactions["DRCRIND"]!="Credit")&(transactions["DRCRIND"]!="C")]


    acquirerfailed=acquirer[(acquirer["Response Code "]=="RR") | (acquirer["Response Code "]=="UR")|(acquirer["Response Code "]=="R9")|(acquirer["Response Code "]=="U9") ]
    acquirerfailed = acquirerfailed.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    acquirerfailed['AMOUNT']=acquirerfailed['Transaction Amount '].astype(np.float64)/100
    acquirertransacations['AMOUNT']=acquirertransacations['AMOUNT']*-1
    data = acquirerfailed.merge(acquirertransacations, how='left', on=["UPI RRN","AMOUNT"], indicator=False)
    del acquirerfailed['AMOUNT']
    #print "--------------------------------------------"
    #print data[data["BANK TRANSACTION ID"]!=""].head()
    #print "--------------------------------------------"
    npciacqentries=[]
    #data['Transaction Date '] = pd.to_datetime(data['Transaction Date '], format="%m%d%y")
    data["AMOUNT"]=data["AMOUNT"].fillna(0)
    data["ACCOUNT 2 NUMBER "]=data["PAN Number "]
    noentry=data[pd.isnull(data["BANK TRANSACTION ID"])]
    noentry=noentry.drop_duplicates(subset=["UPI RRN"])
    for index,x in noentry.iterrows():
        x["StatusCode"]=104
        x["AMOUNT"] = float(x["Transaction Amount "])/100
        npciacqentries.append(createNPCIRecord(x))
    data=data.dropna(subset=["BANK TRANSACTION ID"])
    # data=data.drop_duplicates(subset=["UPI RRN"])
    #data["Transaction Amount "]=data["Transaction Amount "].astype(float)/100
    grouped = data[["UPI RRN","UPI Transaction ID ","BANK TRANSACTION ID","Transaction Amount ","CBSSOURCE","Transaction Date ","ACCOUNT 2 NUMBER "]].groupby(["UPI RRN","UPI Transaction ID ","BANK TRANSACTION ID","CBSSOURCE","Transaction Date ","ACCOUNT 2 NUMBER "])
    for name, group in grouped:
        g=group.reset_index()
        row=json.loads(g.head(1).to_json(orient='records'))[0]
        if g["Transaction Amount "].sum()==0:
            row["StatusCode"]=102
            row["AMOUNT"]=float(row["Transaction Amount "])/100
            npciacqentries.append(createNPCIRecord(row))
        elif g["Transaction Amount "].sum()!=0:
            row["StatusCode"] = 103
            row["AMOUNT"] = float(row["Transaction Amount "])/100
            npciacqentries.append(createNPCIRecord(row))

    #df=pd.read_json(json.dumps(npciacqentries),orient="records")
    #df.to_csv(outputdir + '/AcquirerNPCITransactions.csv',index=False)
    
    collection=db['upiacquirerfailedtransactionreport']
    collection.remove({"statementdate": statementdate})
    npciacqdf=pd.DataFrame(npciacqentries)
    npciacqdf=npciacqdf.drop_duplicates(subset=npciacqdf.columns)
    collection.insert(npciacqdf.to_dict(orient='records'))
    # for x in npciacqentries:
    #     collection.insert(x)
    acquirerrbtransactions = acquirercb[(acquirercb["Response Code "] == "RB")]
    acquirerrbtransactions = acquirerrbtransactions.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    acquirerrbtransactions["TransactionDate"] = acquirerrbtransactions['Transaction Date '].apply(
        lambda x: datetime.datetime.strptime(x, '%m%d%y').strftime('%Y-%m-%d'))
    acquirerrbtransactions = acquirerrbtransactions.merge(transactions, how='left', on="UPI RRN", indicator=True)
    acquirerrbtransactions=acquirerrbtransactions[["TransactionDate","UPI RRN","A/C NO.","BRANCH CODE","ACCOUNT NUMBER ","Response Code ","AMOUNT"]]
    acquirerrbtransactions.to_csv(outputdir + '/ChargebackTransactions.csv',index=False)
    #for index, x in acquirerrbtransactions.iterrows():
    #    print(x)

    "---------------------------------------------------------------------------------------------------"
    cbsdata=data[data["CBSSOURCE"]=='OLDCBS']
    cbsrrns = cbsdata["UPI RRN"].tolist()
    cbscolumns = ['BRANCH CODE', 'Date', 'Date1', 'DRCRIND', 'Static value', 'AMOUNT',
                  'GL', 'SUB GL', 'zeroes_42', 'spaces_3', 'zeroes_3', 'spaces_7', 'zeroes_14',
                  'REMARKS']

    cbs102reversals= payload["CBS"][(payload["CBS"]["UPI RRN"].isin(cbsrrns))&(payload["CBS"]["ERROR CODE"]=="0000")]
    rrns=cbs102reversals["UPI RRN"].unique()
    #cbs102reversals=cbs102reversals[cbs102reversals["GL"] == "000000"]
    cbs102reversals['DRCRIND']= cbs102reversals["DRCRIND"].str[0:1]
    cbs102reversals["Date"]=cbs102reversals["Date"].apply(lambda x:datetime.datetime.strptime(x, "%d/%m/%Y").strftime('%d%m%Y'))
    cbs102reversals['Static value'] = '824603'
    cbs102reversals['zeroes_3'], cbs102reversals['zeroes_14'], cbs102reversals['zeroes_42'] = 3 * '0', 14 * '0', 42 * '0'
    cbs102reversals['spaces_3'], cbs102reversals['spaces_7'] = 3 * ' ', 7 * ' '
    cbs102reversals["Date1"]=cbs102reversals["Date"]

    cbs102reversals.fillna('', inplace=True)
    for cl, wdth in {'BRANCH CODE': 4, 'AMOUNT': 14, 'GL': 6, 'SUB GL': 6,
                     'REMARKS': 35}.iteritems():
        cbs102reversals[cl] = cbs102reversals[cl].astype(str)
        cbs102reversals[cl] = cbs102reversals[cl].apply(
            lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
    cbs102reversals = cbs102reversals.apply(lambda row: ''.join(map(str, row)), axis=1)

    with open(outputdir + '/ReversalCBStransactions.csv', "w") as fwf:
        for record in cbs102reversals.values.tolist():
            fwf.write(record + '\n')
    "---------------------------------------------------------------------------------------------------"


    aadharcbsdata = data[data["CBSSOURCE"] == 'OLDCBSAADHAR']
    cbsrrns = aadharcbsdata["UPI RRN"].tolist()
    cbs102reversals = payload["AADHARCBS"][payload["AADHARCBS"]["UPI RRN"].isin(cbsrrns)&(payload["AADHARCBS"]["ERROR CODE"]=="0000")]
    #cbsreversals = cbsreversals[cbsreversals["GL"] == "000000"]
    cbs102reversals['DRCRIND'] = cbs102reversals["DRCRIND"].str[0:1]
    cbs102reversals["Date"] = cbs102reversals["Date"].apply(
        lambda x: datetime.datetime.strptime(x, "%d/%m/%Y").strftime('%d%m%Y'))
    cbs102reversals['Static value'] = '824603'
    cbs102reversals['zeroes_3'], cbs102reversals['zeroes_14'], cbs102reversals['zeroes_42'] = 3 * '0', 14 * '0', 42 * '0'
    cbs102reversals['spaces_3'], cbs102reversals['spaces_7'] = 3 * ' ', 7 * ' '
    cbs102reversals["Date1"] = cbs102reversals["Date"]
    cbs102reversals = cbs102reversals[cbscolumns]
    cbs102reversals.fillna('', inplace=True)
    for cl, wdth in {'BRANCH CODE': 4, 'AMOUNT': 14, 'GL': 6, 'SUB GL': 6,
                     'REMARKS': 35}.iteritems():
        cbs102reversals[cl] = cbs102reversals[cl].astype(str)
        cbs102reversals[cl] = cbs102reversals[cl].apply(
            lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
    cbs102reversals = cbs102reversals.apply(lambda row: ''.join(map(str, row)), axis=1)
    #cbs102reversals = cbs102reversals.drop_duplicates()
    with open(outputdir + '/ReversalAadharCBStransactions.csv', "w") as fwf:
        for record in cbs102reversals.values.tolist():
            fwf.write(record + '\n')

    "---------------------------------------------------------------------------------------------------"

    finnacledata = data[(data["CBSSOURCE"]=="FINNACLE")&(data["UPI Transaction ID "]==data["BANK TRANSACTION ID"])]

    finnaclerrns = finnacledata["UPI RRN"].tolist()
    finacle102=payload["FINNACLE"][payload["FINNACLE"]["UPI RRN"].isin(finnaclerrns)]
    finacle102['DRCRIND'] = finacle102["DRCRIND"].str[0:1]
    finacle102['static_value1'] = 'N'
    finacle102['blank1'], finacle102['blank2'], finacle102['blank3'], finacle102['blank4'], finacle102['blank5'], = '', '', '', '', ''
    finacle102['static_value2'] = 'Y'
    finacle102.fillna('', inplace=True)
    finacle102['AMOUNT IN PAISA']=finacle102['AMOUNT IN PAISA'].astype(float) / 100.0
    # finacle102['AMOUNT IN PAISA']=finacle102['AMOUNT IN PAISA'].astype(str)
    finacle102['AMOUNT IN PAISA'] = finacle102['AMOUNT IN PAISA'].apply(lambda x: '{0:.2f}'.format(x))
    print finacle102.columns
    finacle102.to_csv('/usr/share/nginx/v8/finacle102.csv')
    finacledebrev=finacle102[finacle102.REMARKS.str.contains("UPI Debit REV")]
    print finacledebrev.columns
    print finacledebrev['REMARKS']
    print "******************************************************************************"
    finacledeb=finacle102[~finacle102.REMARKS.str.contains("UPI Debit REV")]
    print finacledeb.columns
    print finacledeb['REMARKS']
    finale=pd.merge(finacledeb,finacledebrev,on=['UPI RRN','BANK TRANSACTION ID'],how='left',indicator=True,suffixes=('','_y'))
    print finale['_merge'].unique()
    finacleupdate=finale[finale['_merge']=='both']
    collection=db['upiacquirerfailedtransactionreport']
    for i in finacleupdate['UPI RRN'].values.tolist():
    	res=collection.find_one({"shser" : i,"statementdate":statementdate})
        print res
        print collection.update({"_id":ObjectId(res['_id'])},{"$set":{"reason":"102"}})
	
    	
    finacle102=finale[finale['_merge']=='left_only']
    print finacle102
   
    finacle102['Credit/Debit Flag']='C'
    print finacle102.dtypes
    finacle102['DATE']=pd.to_datetime(finacle102['DATE'],format='%d-%b-%Y')
    finacle102['DATE']=finacle102['DATE'].apply(lambda x:datetime.datetime.strftime(x,'%d/%m/%Y') if not pd.isnull(x) else '')
    # finacle102['DATE']=finacle102['DATE'].str.title()
    #finacle102['DATE']=pd.to_datetime(finacle102['DATE'])
    #finacle102['DATE']=finacle102['DATE'].dt.month
    #finacle102['DATE']=pd.to_datetime(finacle102['DATE'],format="%d%m%Y")
    #print finacle102['DATE']
    
    finacle102["REMARKS"] = "UPI REV RRN " + finacle102["UPI RRN"] +' DT '+ finacle102["DATE"]
    
    print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$",finacle102.columns
    print finacle102.head()
    del finacle102['_merge']
    reverse=finacle102.merge(acquirer,left_on=['UPI RRN','BANK TRANSACTION ID'],right_on=['Transaction Serial Number ','UPI Transaction ID '],how='left',suffixes=('','_y'),indicator=True)
    print reverse.columns

    reverse=reverse[reverse['_merge']=='both']
    reverse=reverse[reverse['Response Code ']!='00']
    print reverse['Response Code '].unique()     
    finacle102=reverse

    finDate=finacle102["DATE"].unique()[0]
    finnaclecolumns = ['BRANCH CODE','Credit/Debit Flag', 'Debit Account Number', 'AMOUNT IN PAISA','REMARKS',
               'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2']
    finnaclerenames = {'BRANCH CODE': 'BRANCH CODE', 'Credit/Debit Flag': 'DRCRIND',
                      'Debit Account Number': 'ACCOUNT NUMBER ', 'AMOUNT IN PAISA': 'Transaction Amount ', 'REMARKS': 'NARRATION',
                      'static_value1': 'static_value1', 'blank1': 'blank1',
                      'blank2': 'blank2', 'blank3': 'blank3', 'blank4': 'blank4', 'blank5': 'blank5', 'static_value2': 'static_value2'} 
    finacle102 = finacle102[finnaclecolumns]   
    finacle102==finacle102.drop_duplicates()
    print finacle102.head()
    finacle102.rename(columns=finnaclerenames, inplace=True)
    finacle102['Transaction Amount ']=finacle102['Transaction Amount '].astype(float)
    finacle102['Transaction Amount ']=finacle102['Transaction Amount '].apply(lambda x: '{0:.2f}'.format(x))
    finacle102['Transaction Amount '] = finacle102['Transaction Amount '].fillna('0').astype(float)
    total= finacle102['Transaction Amount '].sum()
    print "/////////////////////////////////////////////",total
    dff=pd.read_csv("/usr/share/nginx/v8/scripts/sample1.csv")
    print dff
    dff['Transaction Amount ']=total
    dff["NARRATION"]=dff["NARRATION"]+finDate
    finacle102=pd.concat([finacle102,dff])
    finnaclecolumns = ['BRANCH CODE','DRCRIND', 'ACCOUNT NUMBER ', 'Transaction Amount ','NARRATION',
               'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2']
    finacle102 = finacle102[finnaclecolumns]

    print finacle102.head()

 
    #print finacle102.head()
  
    finacle102.to_csv(outputdir + "ReversalFinacletransactions.csv", index=False)

    #import sys
    #sys.exit(0)
    #output=data[data["AMOUNT"] != 0][["UPI RRN","AMOUNT","CBSSOURCE"]].merge(transactions, how='left', on="UPI RRN", indicator=False)

    #temp=acquirer[["Transaction Serial Number ","Response Code ","Transaction Date ","Transaction Time "]]
    #temp=temp.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    #output=output.merge(temp, how='left', on="UPI RRN", indicator=False)
    #output.to_csv(outputdir+"Reversals.csv")

    #Status RB Means that the transaction succeeded so there must be a credit or debit entry on the CBS Side. If not these need to be fixed

    '''acquirerrb=acquirer[acquirer["Response Code "]== "RB"]
    acquirerrb = acquirerrb.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    acquirerrb = acquirerrb.merge(transactions, how='left', on="UPI RRN", indicator=True)
    acquirerrb["AMOUNT"]=acquirerrb["AMOUNT"].fillna(0)
    acquirerrb=acquirerrb[["UPI RRN","Actual Transaction Amount ","AMOUNT"]]
    acquirerrb["Actual Transaction Amount "]=acquirerrb["Actual Transaction Amount "].str.strip().astype(int)/100
    acquirerrb["Difference"]=acquirerrb["Actual Transaction Amount "]+acquirerrb["AMOUNT"]
    acquirerrb[acquirerrb["Difference"]!=0].to_csv(outputdir+"103transactions.csv")
    acquirerrb[acquirerrb["Difference"] == 0].to_csv(outputdir+"102transactions.csv")'''

    '''issuersuccessful = issuer[issuer["Response Code "] == "00"]
    issuersuccessful = issuersuccessful.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    # print issuersuccessful.columns
    data = issuersuccessful.merge(transactions, how='left', on="UPI RRN", indicator=True)

    issuerfailed = issuer[~(issuer["Response Code "] == "00") & ~(issuer["Response Code "] == "RB")]
    issuerfailed = issuerfailed.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    data = issuerfailed.merge(transactions, how='left', on="UPI RRN", indicator=False)
    data["AMOUNT"] = data["AMOUNT"].fillna(0)
    data = data[["UPI RRN", "AMOUNT"]].groupby(["UPI RRN"]).sum().reset_index()
    output = data[data["AMOUNT"] != 0][["UPI RRN", "AMOUNT"]].merge(transactions, how='left', on="UPI RRN",
                                                                    indicator=False)
    temp = issuer[["Transaction Serial Number ", "Response Code ", "Transaction Date ", "Transaction Time "]]
    temp = temp.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    output = output.merge(temp, how='left', on="UPI RRN", indicator=False)
    output.to_csv(outputdir+"IssuerReversals.csv")'''
    # Status RB Means that the transaction succeeded so there must be a credit or debit entry on the CBS Side. If not these need to be fixed

    issuerrb = issuer[issuer["Response Code "] == "RB"]
    issuerrb = issuerrb.rename(columns={"Transaction Serial Number ": "UPI RRN"})
    transactions["DRCRIND"]=transactions["DRCRIND"].str[0:1]
    transactions=transactions[transactions["DRCRIND"]=='C']
    transrrns= transactions["UPI RRN"].tolist()
    #print issuerrb.columns
    #print transactions.columns
    #print transactions[transactions['AMOUNT'] == 0]
    #print '*' * 10
    issuerrb['AMOUNT']=issuerrb['Transaction Amount ']
    issuerrb['AMOUNT']=issuerrb['AMOUNT'].astype(np.float64)/100
    data = issuerrb.merge(transactions, how='left', left_on=["UPI RRN","AMOUNT","UPI Transaction ID "],right_on=['UPI RRN','AMOUNT','BANK TRANSACTION ID'],indicator=True)
    #print "%%%%%%%%%%%%%%%%%%%%%%%%"
    #print data[data['_merge'] == 'both']['AMOUNT']
    #print data.head()
    # data["AMOUNT"]=data['Transaction Amount '].astype(float)/100
    data["AMOUNT"] = data["AMOUNT"].fillna(0)
    data["CBSSOURCE"]=data["CBSSOURCE"].fillna("NONE")
    #data = data[["UPI RRN", "AMOUNT", "CBSSOURCE","_merge"]].groupby(["UPI RRN", "CBSSOURCE","_merge"]).sum().reset_index()
    #duplicate=data.duplicated(subset=["UPI RRN"])
    data["StatusCode"] = data.apply(lambda x:103 if x["_merge"]!="both" else 102,axis=1)
    #issuerrb["CBSSOURCE"]="FINNACLE".where(issuerrb["StatusCode"]==103)

    # Begin Added by Shiva for 102 103 NPCI Upload Report
    print data['AMOUNT']
   
    rblist = []
    for index, row in data.iterrows():
        rblist.append(createNPCIRecord(row))

    collection=db['upifailedtransactionreport']
    collection.remove({"shtdat": rblist[0]["shtdat"]})
    rbdf=pd.DataFrame(rblist)
    if '_id' in rbdf.columns:
        del rbdf['_id']
    rbdf=rbdf.drop_duplicates(subset=rbdf.columns)
    collection.insert(rbdf.to_dict(orient='records'))
    # for x in rblist:
    #     if '_id' in x:
    #         del x['_id']
    #     collection.insert(x)


    "------------------------------------------------------------------------------------------------------------------"

    finnacledata102 = data[(data["CBSSOURCE"] == "FINNACLE") & (data["StatusCode"]==102)]
    finnaclerrns = finnacledata102["UPI RRN"].tolist()
    finacle102 = payload["FINNACLE"][payload["FINNACLE"]["UPI RRN"].isin(finnaclerrns)]
    finacle102['DRCRIND'] = finacle102["DRCRIND"].str[0:1]
    finacle102['static_value1'] = 'N'
    finacle102['blank1'], finacle102['blank2'], finacle102['blank3'], finacle102['blank4'], \
    finacle102['blank5'], = '', '', '', '', ''
    finacle102['static_value2'] = 'Y'
    finacle102.fillna('', inplace=True)
    finacle102['AMOUNT IN PAISA']=(finacle102['AMOUNT IN PAISA'].astype(float)/100.0)
    #finacle102['AMOUNT IN PAISA']=finacle102['AMOUNT IN PAISA'].astype(str)
    finacle102['AMOUNT IN PAISA'] = finacle102['AMOUNT IN PAISA'].apply(lambda x: '{0:.2f}'.format(x))

    #for x in finacle102.columns:
    #    print x +":"
    finnaclecolumns = ['BRANCH CODE', 'DRCRIND', 'Credit ACCOUNT NO.', 'AMOUNT IN PAISA', 'REMARKS',
                       'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2']
    finnaclerenames = {'BRANCH CODE': 'Branch code', 'DRCRIND': 'Credit/Debit Flag',
                       'Credit ACCOUNT NO.': 'Acc no', 'AMOUNT IN PAISA': 'Amt', 'REMARKS': 'Narration',
                       'static_value1': 'static_value', 'static_value2': 'static_value', 'blank1': 'blank',
                       'blank2': 'blank', 'blank3': 'blank', 'blank4': 'blank', 'blank5': 'blank'}
    finacle102 = finacle102[finnaclecolumns]
    finacle102.rename(columns=finnaclerenames, inplace=True)
    finacle102.to_csv(outputdir + "102Finacletransactions.csv", index=False)

    aadharcbs102data = data[(data["CBSSOURCE"] == 'OLDCBSAADHAR') & (data["StatusCode"]==102)]
    cbsrrns = aadharcbs102data["UPI RRN"].tolist()
    cbs102reversals = payload["AADHARCBS"][payload["AADHARCBS"]["UPI RRN"].isin(cbsrrns)]
    # cbsreversals = cbsreversals[cbsreversals["GL"] == "000000"]
    cbs102reversals['DRCRIND'] = cbs102reversals["DRCRIND"].str[0:1]
    cbs102reversals["Date"] = cbs102reversals["Date"].apply(
        lambda x: datetime.datetime.strptime(x, "%d/%m/%Y").strftime('%d%m%Y'))
    cbs102reversals['Static value'] = '824603'
    cbs102reversals['zeroes_3'], cbs102reversals['zeroes_14'], cbs102reversals['zeroes_42'] = 3 * '0', 14 * '0', 42 * '0'
    cbs102reversals['spaces_3'], cbs102reversals['spaces_7'] = 3 * ' ', 7 * ' '
    cbs102reversals["Date1"] = cbs102reversals["Date"]
    cbs102reversals = cbs102reversals[cbscolumns]
    cbs102reversals.fillna('', inplace=True)
    for cl, wdth in {'BRANCH CODE': 4, 'AMOUNT': 14, 'GL': 6, 'SUB GL': 6,
                     'REMARKS': 35}.iteritems():
        cbs102reversals[cl] = cbs102reversals[cl].astype(str)
        cbs102reversals[cl] = cbs102reversals[cl].apply(
            lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
    cbs102reversals = cbs102reversals.apply(lambda row: ''.join(map(str, row)), axis=1)
    with open(outputdir + '/102AadharCBStransactions.csv', "w") as fwf:
        for record in cbs102reversals.values.tolist():
            fwf.write(record + '\n')

    cbs102data = data[(data["CBSSOURCE"] == 'OLDCBS') & (data["StatusCode"] == 102)]
    cbsrrns = cbs102data["UPI RRN"].tolist()
    cbs102reversals = payload["CBS"][payload["CBS"]["UPI RRN"].isin(cbsrrns)]
    # cbsreversals = cbsreversals[cbsreversals["GL"] == "000000"]
    cbs102reversals['DRCRIND'] = cbs102reversals["DRCRIND"].str[0:1]
    cbs102reversals["Date"] = cbs102reversals["Date"].apply(
        lambda x: datetime.datetime.strptime(x, "%d/%m/%Y").strftime('%d%m%Y'))
    cbs102reversals['Static value'] = '824603'
    cbs102reversals['zeroes_3'], cbs102reversals['zeroes_14'], cbs102reversals[
        'zeroes_42'] = 3 * '0', 14 * '0', 42 * '0'
    cbs102reversals['spaces_3'], cbs102reversals['spaces_7'] = 3 * ' ', 7 * ' '
    cbs102reversals["Date1"] = cbs102reversals["Date"]
    cbs102reversals = cbs102reversals[cbscolumns]
    cbs102reversals.fillna('', inplace=True)
    for cl, wdth in {'BRANCH CODE': 4, 'AMOUNT': 14, 'GL': 6, 'SUB GL': 6,
                     'REMARKS': 35}.iteritems():
        cbs102reversals[cl] = cbs102reversals[cl].astype(str)
        cbs102reversals[cl] = cbs102reversals[cl].apply(
            lambda x: str(x)[:wdth] if len(str(x)) >= wdth else (wdth - len(str(x))) * ' ' + x)
    cbs102reversals = cbs102reversals.apply(lambda row: ''.join(map(str, row)), axis=1)
    with open(outputdir + '/102CBStransactions.csv', "w") as fwf:
        for record in cbs102reversals.values.tolist():
            fwf.write(record + '\n')

    status103data= data[data["StatusCode"] == 103]
    print status103data['REMARKS ']
    status103data['BRANCH CODE']=status103data["IFSC CODE "].str[-4:]
    status103data['BRANCH CODE'].fillna('0000', inplace=True)
    status103data['Transaction Date ']=pd.to_datetime(status103data['Transaction Date '],format="%m%d%y")
    status103data['Transaction Date '] =status103data['Transaction Date '].dt.strftime("%d-%m-%Y")
    status103data["NARRATION"]="UPI TIMEOUT RRN "+status103data["UPI RRN"]+" DT"+status103data['Transaction Date ']
    print status103data.head().to_csv("head.csv")
    fin103date=status103data['Transaction Date '].unique()[0]
    finnaclecolumns = ['BRANCH CODE', 'DRCRIND', 'ACCOUNT NUMBER ', 'Transaction Amount ', "NARRATION",
                       'static_value1', 'blank1', 'blank2', 'blank3', 'blank4', 'blank5', 'static_value2']
    finnaclerenames = {'BRANCH CODE': 'Branch code', 'DRCRIND': 'Credit/Debit Flag',
                       'ACCOUNT NUMBER ': 'Acc no', 'Transaction Amount ': 'Amt', 'NARRATION': 'Narration',
                       'static_value1': 'static_value', 'static_value2': 'static_value', 'blank1': 'blank',
                       'blank2': 'blank', 'blank3': 'blank', 'blank4': 'blank', 'blank5': 'blank'}

    status103data['DRCRIND'] = 'C'
    status103data['static_value1'] = 'N'
    status103data['blank1'], status103data['blank2'], status103data['blank3'], status103data['blank4'], \
    status103data['blank5'], = '', '', '', '', ''
    status103data['static_value2'] = 'Y'
    status103data = status103data[finnaclecolumns]
    status103data.fillna('', inplace=True)

    status103data['Transaction Amount '] = (status103data['Transaction Amount '].astype(float) / 100.0)
    # finacle102['AMOUNT IN PAISA']=finacle102['AMOUNT IN PAISA'].astype(str)

    total=status103data['Transaction Amount '].sum()

    df=pd.read_csv("/usr/share/nginx/v8/scripts/sample.csv")
    df["Transaction Amount "]=total
    print df.columns 
    df["NARRATION"]=df["NARRATION"]+fin103date
    status103data=pd.concat([status103data,df])
    status103data['Transaction Amount '] = status103data['Transaction Amount '].apply(lambda x: '{0:.2f}'.format(x))
    status103data.rename(columns=finnaclerenames, inplace=True)
    status103data.to_csv(outputdir + "103Finacletransactions.csv", index=False)

    os.system(" cd %s; zip -r %s.zip %s" % (outdir, statementdate, statementdate))
