from pymongo import MongoClient
from bson.objectid import ObjectId

mongo_client = MongoClient('mongodb://' + 'localhost' + ':27017')
# reconbuilder = mongo_client['reconbuilder']


# import pandas
# df1 = pandas.DataFrame(reconbuilder['feedDefination'].find_one({'_id': ObjectId('5965bea21a1bc7033e8d574b')})['fieldDetails'])
# df2 = pandas.DataFrame(reconbuilder['feedDefination'].find_one({'_id': ObjectId('59671ea11a1bc7033e8d574e')})['fieldDetails'])
# df2 = df2[df2['mdlFieldName'].isin(df1['mdlFieldName']) == False]
# df1 = pandas.concat([df1,df2],ignore_index=True)
# df3 = pandas.DataFrame(reconbuilder['feedDefination'].find_one({'_id': ObjectId('5965f6ec1a1bc7033e8d574d')})['fieldDetails'])
# df3 = df3[df3['mdlFieldName'].isin(df1['mdlFieldName']) == False]
# df1 = pandas.concat([df1,df3],ignore_index=True)
# print df1
# print dict(zip(df1['mdlFieldName'],df1['displayName']))
# exit(0)
reconbuilderColNew = mongo_client['reconbuilder_new']
# ereconColNew = mongo_client['erecon_ab']
reconDef = mongo_client['reconbuilder']['recon_context_details'].find({})
reconDump = reconbuilderColNew['recon_context_details']

for recon in reconDef:
    if 'reconId' in recon and recon['reconId'] in ['CB_CASH_APAC_61171' ]:
        reconbuilderColNew['recon_context_details'].save(recon)
        for s in recon['sources']:
            print s['feedId']
            doc = mongo_client['reconbuilder']['feedDefination'].find_one({'_id': ObjectId(s['feedId'])})
            reconbuilderColNew['feedDefination'].save(doc)
            print '*' * 100
    if 'reconId' not in recon:
        reconDump.remove({'_id': recon['_id']})
    elif 'reconId' in recon and recon['reconId'] not in ['CB_CASH_APAC_61171']:
        reconDump.remove({'_id': recon['_id']})
    else:
        print recon['reconId']
exit(0)
for recon in erecon11:
    if 'userName' not in recon:
        erecondump.remove({'_id': recon['_id']})
    elif 'userName' in recon and recon['userName'] not in ['admin', 'varun']:
        erecondump.remove({'_id': recon['_id']})
    else:
        print recon['userName']

erecon11 = mongo_client['erecon11']['business'].find({})
erecondump = mongo_client['erecon11']['business']

for recon in erecon11:
    if 'RECON_ID' not in recon:
        erecondump.remove({'_id': recon['_id']})
    elif 'RECON_ID' in recon and recon['RECON_ID'] not in ['EPYMTS_NEFT_INWARD_APAC_0009',
                                                           'EPYMTS_NEFT_OUTWARD_APAC_0010']:
        erecondump.remove({'_id': recon['_id']})
    else:
        print recon['RECON_ID']

erecon11 = mongo_client['erecon11']['reconlicense'].find({})
erecondump = mongo_client['erecon11']['reconlicense']

for recon in erecon11:
    if 'reconId' not in recon:
        erecondump.remove({'_id': recon['_id']})
    elif 'reconId' in recon and recon['reconId'] not in ['EPYMTS_NEFT_INWARD_APAC_0009',
                                                         'EPYMTS_NEFT_OUTWARD_APAC_0010']:
        erecondump.remove({'_id': recon['_id']})
    else:
        print recon['reconId']

erecon['customReports'].remove()
erecon['userpool'].remove()
erecon['feedDefination'].remove()
erecon['jobstatus'].remove()
erecon['nostro_reports'].remove()
erecon['reconassignment'].remove()
erecon['recon_job_post'].remove()
