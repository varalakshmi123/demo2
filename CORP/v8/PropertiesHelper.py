# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''
import config
import os


class PropertiesHelper(object):

    def __init__(self):
        self.props = {}
        propFilePath = '../conf/engine.properties'
        propFilePath = config.enginePath + 'conf' + os.sep + 'engine.properties'
        with open(propFilePath, 'r') as f:
            for line in f:
                line = line.rstrip()
                if '=' not in line: continue
                if line.startswith('#'): continue
                k, v = line.split('=', 1)
                self.props[k] = v
                
    def getProperty(self, key):
        return self.props.get(key)
