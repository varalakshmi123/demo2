import logging
import sys
from datetime import datetime,timedelta
from JobExecutor import JobExecutor
import config


class AutoJobExecutor():

    def __init__(self, reconId):
        self.reconId = reconId

    def run(self):
        stmt_date = datetime.now().strftime('%d-%b-%Y') if self.reconId in config.t_run_recon else (datetime.now() + timedelta(days=-1)).strftime('%d-%b-%Y')
        job_executor = JobExecutor(self.reconId, stmt_date)
        job_executor.run()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print 'Please provide valid RECON_ID'
        print 'Usage : AutoJobExecutor <RECON_ID>'
    else:
        reconJob = AutoJobExecutor(sys.argv[1])
        reconJob.run()
