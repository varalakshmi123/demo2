# -*- coding: utf-8 -*-
'''
 *******************************************************************************
 * © 2011 Algofusion Technologies Limited, Bangalore, India. All rights reserved.
 *
 * Version: 5.0
 *
 * Except for any open source software components embedded in this
 * Algofusion Technologies proprietary software program ("Program"),
 * this Program is protected by copyright laws, international treaties
 * and other pending or existing intellectual property rights in India,
 * the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction,
 * storage, transmission in any form or by any means
 * (including without limitation electronic, mechanical, printing,
 * photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law.
 *******************************************************************************
 * Created on 28-Apr-2016
 *******************************************************************************
'''

import logging
import os
import re
import subprocess
import uuid
from CustomFunctions import CustomFunctions
from collections import OrderedDict
from datetime import datetime

import numpy as np
import pandas as pd
import config
from PropertiesHelper import PropertiesHelper
from Utilities import Utilities
from datetime import timedelta


class FeedLoader():
    ## Constructor
    def __init__(self, reconDef, stmtDate, execId, jobSummary, dbOps,singleSideUpload=False):
        self.jobSummary = jobSummary
        self.reconDef = reconDef
        self.bizCtxtId = reconDef['businessContextId']
        self.reconId = reconDef['reconId']
        self.stmtDate = stmtDate
        self.execId = execId
        self.logger = logging.getLogger('PYRBE')
        self.utils = Utilities(dbOps)
        self.propsHelper = PropertiesHelper()
        self.reconData = OrderedDict()
        self.nonPartData = OrderedDict()
        self.cfns = CustomFunctions(self.reconId, execId, stmtDate, dbOps)
        self.filesToMove = {}
        self.singleSideUpload = singleSideUpload
        for source in reconDef['sources']:
            self.reconData[str(source['sourceType'])] = None
            self.nonPartData[str(source['sourceType'])] = None
        self.crSymbols = self.propsHelper.getProperty('CREDIT_SYMBOLS').split(';')
        self.drSymbols = self.propsHelper.getProperty('DEBIT_SYMBOLS').split(';')
        self.rrSymbols = self.propsHelper.getProperty('REVERSAL_SYMBOLS').split(';')

    ## Run feed loader
    def run(self):
        self.utils.addDetailsToSummary(self.jobSummary, 'loadStart', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
        # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'FEED_LOAD_PROG')
        self.feedDefs = []
        sources = self.reconDef['sources']
        if sources is None or len(sources) == 0:
            self.logger.error("No sources configured")
            self.logger.info('----------------------------------------')
            self.logger.info('Recon job execution ABORTED')
            self.logger.info('----------------------------------------')
            self.utils.addDetailsToSummary(self.jobSummary, 'jobStatus', "FAILURE")
            self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "No sources configured")
            raise BaseException('No sources configured')
        for source in sources:
            if 'feedId' not in source.keys() or source['feedId'] is None or source['feedId'] =='' :
                self.logger.error("Feed definition not found")
                self.logger.info('----------------------------------------')
                self.logger.info('Recon job execution ABORTED')
                self.logger.info('----------------------------------------')
                self.utils.addDetailsToSummary(self.jobSummary, 'jobStatus', "FAILURE")
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Feed definition not found")
                raise BaseException('Feed definition not found')
            feedDef = self.utils.getFeedDef(source['sourceType'], source['feedId'], self.jobSummary)
            if feedDef is None:
                self.logger.error("Feed definition not found for given feed ID - '" + source['feedId'] + "'")
                self.logger.info('----------------------------------------')
                self.logger.info('Recon job execution ABORTED')
                self.logger.info('----------------------------------------')
                raise BaseException('Feed definition not found for given feed ID - '" + source['feedId'] + "'')

            feedDef['sourceType'] = source['sourceType']
            feedDef['useInMatching'] = source['useInMatching']
            feedDef['moveToProcessed'] = source['moveToProcessed']
            feedDef['applyAccountFilter'] = source['applyAccountFilter']

            self.feedDefs.append(feedDef)
        self.findFeedFiles()
        self.loadFeedFiles()
        # self.utils.createAlert(self.bizCtxtId, self.reconId, self.stmtDate, self.execId, 'FEED_LOAD_OK')
        self.utils.addDetailsToSummary(self.jobSummary, 'loadEnd', datetime.now().strftime('%d-%b-%Y %H:%M:%S'))
        return (self.reconData, self.nonPartData)

    ## Find feed files
    def findFeedFiles(self):
        # Log waiting for feed files
        self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                 self.stmtDate, self.execId, 'Feed Watching',
                                 'Waiting For Feeds')
        # if self.reconId == 'TRSC_CASH_APAC_20181':
        #     self.cfns.nostroReport(self.reconId, reconData=self.reconData)

        self.feedFiles = {}
        for feedDef in self.feedDefs:
            # Append local MFT base path to feed path
            feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + feedDef['feedPath']
            self.logger.info("| R-Sync Initiated on folder '" + feedDef['feedPath'] + "'...")
            self.logger.info('----------------------------------------')
            rsync_cmd = 'rsync -avr reconadmin@10.1.37.41:/recondata/{}/* {}'.format(feedDef['feedPath'], feedPath)
            # os.system(rsync_cmd)
            feedName = feedDef['feedName']
            self.logger.info("| Searching files for feed '" + feedName + "'... , feedId: "+str(feedDef['_id']))
            self.logger.info('----------------------------------------' ) 

            # Append local MFT base path to feed path
            if '#' in feedDef['feedPath']:
                fpattern = feedDef['feedPath']
                indx1 = fpattern.index('#')
                indx2 = fpattern.index('#', indx1 + 1)
                dpattern = fpattern[indx1 + 1:indx2]
                dstr = self.stmtDate.strftime(dpattern)
                feedDef['feedPath'] = fpattern.replace('#' + dpattern + '#', dstr)
            feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + feedDef['feedPath']

            # Replace statement date in file name pattern
            fpattern = feedDef['fileNamePattern']
            if '#' in feedDef['fileNamePattern']:
                fpattern = feedDef['fileNamePattern']
                indx1 = fpattern.index('#')
                indx2 = fpattern.index('#', indx1 + 1)
                dpattern = fpattern[indx1 + 1:indx2]
                dstr = self.stmtDate.strftime(dpattern)
                fpattern = fpattern.replace('#' + dpattern + '#', dstr)
            if not os.path.exists(feedPath):
                raise BaseException('Feed Path Does not exits')

            files = [f for f in os.listdir(feedPath) if re.match(fpattern, f)]
            if self.reconId == 'TRSC_CASH_APAC_20181':
                if feedName == "Nostro Mirror Acc Txn" or feedName == "Nostro Mirror Acc Bal" :
                    fpattern = feedDef['fileNamePattern']
                    pdpattern = fpattern[indx1 + 1:indx2]
                    pdstr = (self.stmtDate - timedelta(1)).strftime(dpattern)
                    fpattern = fpattern.replace('#' + pdpattern + '#', pdstr)
                    files = [f for f in os.listdir(feedPath) if re.search(fpattern, f)]
                else:
                    files = [f for f in os.listdir(feedPath) if re.search(fpattern, f)]
                

            self.logger.info("Finding files with pattern '" + fpattern + "' in the path '" + feedDef['feedPath'] + "'")

            self.logger.info("Found " + str(len(files)) + " files - " + ','.join(files))

            # If feed files found continue otherwise abort
            if len(files) == 0 and self.singleSideUpload == False:
                self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                         self.stmtDate, self.execId, 'Feed Readiness',
                                         'Feeds Not Arrived')
                self.logger.error("No files found for feed '" + feedName + "'")
                self.logger.info('----------------------------------------')
                self.logger.info('Recon job execution ABORTED')
                self.logger.info('----------------------------------------')
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', 'Feeds Not Arrived')
                raise BaseException('Feed files not received')

            # Validate found files count if found more than configured then abort
            flimit = feedDef['limitFileCount']
            if flimit > 0 and len(files) > flimit:
                self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                         self.stmtDate, self.execId, 'Feed Readiness',
                                         'Feeds Not Arrived')
                self.logger.error("Expected file count for '" + feedName + "' is " + str(int(flimit)) +
                                  " but found " + str(len(files)))
                self.logger.info('----------------------------------------')
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Expected file count for '" + feedName + "' is " + str(int(flimit)) +
                                  " but found " + str(len(files)))
                self.logger.info('Recon job execution ABORTED')
                self.logger.info('----------------------------------------')
                raise BaseException('Found files exceed feed file limit')

            self.feedFiles[feedDef['_id']] = files
            self.logger.info('----------------------------------------')
        # Log arrival of feed files

        self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                 self.stmtDate, self.execId, 'Feed Readiness',
                                 'Feeds Arrived')

    ## Load source data
    def loadFeedFiles(self):
        # Log start of feed files loading
        self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                 self.stmtDate, self.execId, 'Feed Loading',
                                 'Feeds Loading Initiated')
        for feedDef in self.feedDefs:
            feedData = pd.DataFrame()
            sourceType = str(feedDef['sourceType'])
            feedId = feedDef['_id']
            feedName = feedDef['feedName']
            self.logger.info("| Loading files for feed '" + feedName + "'...")
            self.logger.info('----------------------------------------')

            # Get names, types, positions and patterns
            names = self.utils.getColNames(feedDef)
            splitApplied = False

            # Load the files with appropriate adapter
            files = self.feedFiles[feedId]
            splitFiles = []
            # Find files in the path
            feedPath = self.propsHelper.getProperty('LOCAL_MFT_PATH') + os.sep + feedDef['feedPath']
            if files is not None and len(files) ==0:
                feedData = pd.DataFrame(columns=names)
            for file in files:
                try:
                    # If feed configured to run a pre loader script then run it
                    splitApplied = False
                    hasPreloadScript = feedDef['preLoadScriptIndicator']
                    if hasPreloadScript:
                        preLoadScript = feedDef['preLoadScript']
                        self.logger.info("Executing pre-load script...");
                        loadFile = os.path.splitext(file)[0] + '_' + \
                                   preLoadScript['splitSuffix'] + '.csv'

                        # print os.path.splitext(file)
                        scriptPath = self.propsHelper.getProperty('CUSTOM_SCRIPTS_PATH') + \
                                     os.sep + preLoadScript['scriptFile']

                        extn = os.path.splitext(os.path.basename(scriptPath))[1]
                        stmt_Date = self.stmtDate.strftime('%d/%m/%Y')
                        if extn == '.py':

                            # command = 'python ' + scriptPath + ' ' + feedPath + os.sep + file + ' ' + feedPath + os.sep + loadFile
                            command = '/usr/share/nginx/www/erecon/flask/erecon/bin/python2' , scriptPath ,feedPath + os.sep + file , feedPath + os.sep + loadFile,stmt_Date,self.reconId,self.execId
                            #print command
                            subprocess.call(command)
                            # os.system(command)
                        elif extn == '.sh':
                            command = scriptPath, feedPath + os.sep + file, feedPath + os.sep + loadFile, stmt_Date
                            # print command
                            subprocess.call(command)
                        self.logger.info("Generated output file - " + loadFile)
                        splitApplied = True
                        splitFiles.append(loadFile)
                    else:
                        loadFile = file
                    loadFile = feedPath + os.sep + loadFile

                    # Get names, types, positions and patterns
                    names = self.utils.getColNames(feedDef)
                    types = self.utils.getColTypes(feedDef)
                    positions = self.utils.getColPositions(feedDef)
                    dtpatterns = self.utils.getDatePatterns(feedDef)
                    csvtypes = dict()
                    dtcols = []
                    for i in range(0, len(types)):
                        if types[i] == 'np.datetime64':
                            dtcols.append(names[i])
                            continue
                        if types[i] == 'np.int64':
                            continue
                        # print names[i],types[i]
                        csvtypes[names[i]] = eval(types[i])
                    skipTopRows = int(feedDef['skipTopRows'])

                    if feedDef['headerExists']:
                        skipTopRows += 1
                    skipBottomRows = int(feedDef['skipBottomRows'])
                    ## Apply adapter
                    if feedDef['format'] == 'DELIMITED':

                        df = self.loadDelimFile(feedDef, loadFile, names, csvtypes, positions,
                                                dtcols, dtpatterns, skipTopRows, skipBottomRows)
                    elif feedDef['format'] == 'DELIMITEDWITHFIXEDCOLUMNS':
                        file_names = self.utils.getFileColNames(feedDef)
                        df = self.loadDelimFileWithFixedColumns(feedDef, loadFile, names, csvtypes, positions,
                                                                dtcols, dtpatterns, skipTopRows, skipBottomRows,
                                                                file_names)
                    elif feedDef['format'] == 'FIXED_LENGTH':
                        df = self.loadFLFile(feedDef, loadFile, names, csvtypes, positions,
                                             dtcols, dtpatterns, skipTopRows, skipBottomRows)
                    elif feedDef['format'] == 'SWIFT':
                        df = self.loadSwiftFile(feedDef, loadFile, names, csvtypes, positions, dtcols, dtpatterns)
                    if df is not None:
                        df['FEED_FILE_NAME'] = str(file)
                    if feedData is None:
                        feedData = df
                    elif df is not None:
                        feedData = pd.concat([feedData, df])
                except Exception, e:
                    print e
                    self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                             self.stmtDate, self.execId, 'Feed Loading',
                                             'Feeds Loading Failed')
                    self.utils.addDetailsToSummary(self.jobSummary, 'failureReason',  'Feeds Loading Failed')
                    raise BaseException('Feeds Loading Failed')

            if feedDef['moveToProcessed']:
                if feedDef['feedPath'] in self.filesToMove.keys():
                    self.filesToMove[feedDef['feedPath']] += files + splitFiles
                else:
                    self.filesToMove[feedDef['feedPath']] = files + splitFiles
            else:
                if feedDef['feedPath'] in self.filesToMove.keys():
                    self.filesToMove[feedDef['feedPath']] += splitFiles
                else:
                    self.filesToMove[feedDef['feedPath']] = splitFiles
            # Apply filters including account filter
            rawCount = len(feedData)
            self.logger.info("Total records for feed '" + feedName +
                             "' (before filtering) = %6d" % rawCount)
            self.logger.info('----------------------------------------')
            feedData = self.applyFilters(feedDef, feedData, feedDef['applyAccountFilter'])
            filterCount = len(feedData)
            self.logger.info("Total records for feed '" + feedName +
                             "' (after filtering) = %6d" % filterCount)

            # Append feed data to parent source data
            if feedData is not None:
                feedData['SOURCE_TYPE'] = sourceType
                feedData['OBJECT_ID'] = feedData['SOURCE_TYPE'].apply(lambda x: str(uuid.uuid4().int & (1 << 64) - 1))
                feedData['LINK_ID'] = ''
                feedData['RECORD_VERSION'] = 1
                if feedDef['positionsFeed']:
                    feedData['ENTRY_TYPE'] = 'P'
                else:
                    feedData['ENTRY_TYPE'] = 'T'
                feedData['CARRY_FORWARD_FLAG'] = 'N'
                if 'DEBIT_CREDIT_INDICATOR' in names and 'DEBIT_CREDIT_INDICATOR' in feedData.columns:
                    feedData['DEBIT_CREDIT_INDICATOR'] = feedData['DEBIT_CREDIT_INDICATOR'].str.strip(' ')
                    feedData.loc[feedData[feedData['DEBIT_CREDIT_INDICATOR'].isin(self.crSymbols)].index,'DEBIT_CREDIT_INDICATOR'] = 'CR'
                    feedData.loc[feedData[feedData['DEBIT_CREDIT_INDICATOR'].isin(self.drSymbols)].index,'DEBIT_CREDIT_INDICATOR'] = 'DR'
                    feedData.loc[feedData[feedData['DEBIT_CREDIT_INDICATOR'].isin(self.rrSymbols)].index,'DEBIT_CREDIT_INDICATOR'] = 'RR'
                if feedDef['useInMatching']:

                    if self.reconData[sourceType] is None:
                        self.reconData[sourceType] = feedData
                    else:
                        # print list(set(feedData.columns.difference(self.reconData[sourceType].columns )))
                        # for cl in list(set(feedData.columns.difference(self.reconData[sourceType].columns ))):
                        #     if cl not in self.reconData[sourceType].columns:
                        #         self.reconData[sourceType][cl] = np.NaN
                        # feedData = feedData[sorted(feedData.columns)]
                        # self.reconData[sourceType] = self.reconData[sourceType][sorted(self.reconData[sourceType].columns)]
                        # import collections
                        # print collections.Counter(self.reconData[sourceType].columns)
                        # print collections.Counter(feedData.columns)
                        # print self.reconData[sourceType].columns
                        # print feedData.columns

                        self.reconData[sourceType] = pd.concat([self.reconData[sourceType], feedData])
                else:
                    if self.nonPartData[sourceType] is None:
                        self.nonPartData[sourceType] = feedData
                    else:
                        self.nonPartData[sourceType] = pd.concat([self.reconData[sourceType], feedData])
            self.logger.info('----------------------------------------')

            filterApplied = (feedDef['filters'] is not None and len(feedDef['filters']) > 0) \
                            or feedDef['applyAccountFilter']
            self.utils.addFeedSummary(self.jobSummary, sourceType, feedId, files,
                                      splitApplied, filterApplied,
                                      feedDef['useInMatching'], rawCount, filterCount)
        # Split single source data as _CR and _DR
        # if int(self.reconDef['sourceCount']) == 1:
        #     self.splitSingleSrc()
        # Log end of feed files loading
        # exit(0)
        self.utils.insertExecLog(self.bizCtxtId, self.reconId,
                                 self.stmtDate, self.execId, 'Feed Loading',
                                 'Feeds Loading Completed')

    ## Split single source data based on debit/credit
    def splitSingleSrc(self):
        data = self.reconData['S1']
        data_cr = data[data['DEBIT_CREDIT_INDICATOR'] == 'CR']
        self.reconData['S1_CR'] = data_cr
        data_dr = data[data['DEBIT_CREDIT_INDICATOR'] == 'DR']
        self.reconData['S1_DR'] = data_dr
        del self.reconData['S1']

    ## Apply filters and return filtered feed data
    def applyFilters(self, feedDef, data, applyAccFilter=False):
        self.logger.info("| Applying filters...")
        self.logger.info('----------------------------------------')
        if applyAccFilter:
            self.logger.info("Filtering based on mapped accounts...")
            accounts = self.utils.getFeedAccMap(self.reconId, feedDef['_id'])
            if accounts is None:
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', 'Feed account mapping not found')
                raise BaseException('Feed account mapping not found')
            data['ACCOUNT_NUMBER'] = data['ACCOUNT_NUMBER'].astype(str) \
                .apply(lambda x: '00000' + x if not x.startswith('00000') else x)
            # accStr = '|'.join(map('00000{0}'.format, [val['accountNumber'] for val in accounts]))
            accStr = '|'.join(map('00000{0}'.format, [str(val['accountNumber']).replace('.0', '') for val in accounts]))
            data = data[data["ACCOUNT_NUMBER"].str.contains(accStr)]
        else:
            self.logger.info("Mapped accounts based filtering is not configured for this feed")

        filters = []
        if 'filters' in feedDef:
            filters = feedDef['filters']
        if filters is not None and len(filters) > 0 and data is not None and len(data) > 0:
            self.logger.info("Filtering based on configured filter conditions...")
            for filter in filters:
                if filter.strip() != '':
                    self.logger.info(filter)
                    # print data.head()
                    exec (filter)

        # data = self.utils.customFilters(self.reconId,feedDef,data)

        return data

    ## Load delimited file
    def loadDelimFile(self, feedDef, file, names, csvtypes, positions,
                      dtcols, dtpatterns, skipTopRows, skipBottomRows):
        df = pd.DataFrame(columns=names)
        try:
            lines = sum(1 for line in open(file))
            if lines == skipTopRows:
                df = pd.DataFrame(columns=names)
            else:
                df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=names,
                                 converters=csvtypes, skiprows=skipTopRows, usecols=positions,
                                 skipfooter=skipBottomRows, engine='python')



        except Exception, e:
            if str(e) == 'line contains NULL byte':
                try:
                    df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=names,
                                     converters=csvtypes, skiprows=skipTopRows, usecols=positions,
                                     skipfooter=skipBottomRows)
                except:
                    self.logger.error("1Unable to load the file " + file + ".\n"
                                                                           "Please check that file is readable and is "
                                                                           "as per the configured structure.\n"
                                                                           "If problem persists contact Administrator.")
                    self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

                    raise BaseException("Unable to load the file " + file)
            else:
                self.logger.error("Unable to load the file " + file + ".\n"
                                                                      "Please check that file is readable and is "
                                                                      "as per the configured structure.\n"
                                                                      "If problem persists contact Administrator.")
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

                raise BaseException("Unable to load the file " + file)
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            for col in dtcols:
                if dtpatterns[col] == '%d%m%Y':
                    df[col] = df[col].astype(str)
                    df['tdlen'] = pd.Series(df[col]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                    del df['tdlen']
                df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')

        self.logger.info("Records in file " + os.path.basename(file) + " = %6d" % len(df))
        # cols = df.columns.tolist()[0:15]
        # print df[cols]
        # print cols
        # print df.columns.tolist()
        # print df.head()
        # print df.tail()
        # print file
        return df

    ## Load delimited file with fixed columns in file
    def loadDelimFileWithFixedColumns(self, feedDef, file, names, csvtypes, positions,
                                      dtcols, dtpatterns, skipTopRows, skipBottomRows, file_names=[]):
        df = pd.DataFrame(columns=names)
        try:
            lines = sum(1 for line in open(file))
            if lines == skipTopRows:
                df = pd.DataFrame(columns=names)
            else:
                c = 1
                for line in open(file, 'rU'):
                    if c == skipTopRows:
                        line = line.decode('utf-8-sig')
                        headers = (line).replace('\r', '').replace('\n', '').split(feedDef["delimiter"])
                        positionsNew = []
                        namesNew = []
                        for indx in range(0, len(file_names)):
                            if file_names[indx] in headers:
                                # print headers[headers.index(file_names[indx])],names[indx]
                                positionsNew.append(headers.index(file_names[indx]))
                                namesNew.append(names[indx])
                            else:
                                self.logger.error("Cannot find the column name" + repr(file_names[indx]))
                                exit(0)
                    c += 1
                # print namesNew
                # print names
                if len(namesNew) > 0:
                    df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=namesNew,
                                     converters=csvtypes, skiprows=skipTopRows, usecols=positionsNew,
                                     skipfooter=skipBottomRows, engine='python')



        except Exception, e:
            if str(e) == 'line contains NULL byte':
                try:
                    df = pd.read_csv(file, header=None, sep=str(feedDef['delimiter']), names=names,
                                     converters=csvtypes, skiprows=skipTopRows, usecols=positions,
                                     skipfooter=skipBottomRows)
                except:
                    self.logger.error("1Unable to load the file " + file + ".\n"
                                                                           "Please check that file is readable and is "
                                                                           "as per the configured structure.\n"
                                                                           "If problem persists contact Administrator.")
                    self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

                    raise BaseException("Unable to load the file " + file)
            else:
                self.logger.error("Unable to load the file " + file + ".\n"
                                                                      "Please check that file is readable and is "
                                                                      "as per the configured structure.\n"
                                                                      "If problem persists contact Administrator.")
                self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

                raise BaseException("Unable to load the file " + file)
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            for col in dtcols:
                if dtpatterns[col] == '%d%m%Y':
                    df[col] = df[col].astype(str)
                    df['tdlen'] = pd.Series(df[col]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                    del df['tdlen']
                df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')  

        self.logger.info("Records in file " + os.path.basename(file) + " = %6d" % len(df))
        # cols = df.columns.tolist()[0:15]
        # print df[cols]
        # print cols
        # print file
        return df

    ## Load fixed length files
    def loadFLFile(self, feedDef, file, names, csvtypes, positions,
                   dtcols, dtpatterns, skipTopRows, skipBottomRows):
        df = pd.DataFrame(columns=names)
        colspecs = self.utils.getFLPositions(feedDef)
        try:
            lines = sum(1 for line in open(file))
            if lines == skipTopRows:
                df = pd.DataFrame(columns=names)
            else:
                df = pd.read_fwf(file, header=None, colspecs=colspecs, names=names,
                                 converters=csvtypes, skiprows=skipTopRows,
                                 skipfooter=skipBottomRows, usecols=positions,
                                 engine='python')
        except:
            self.logger.error("Unable to load the file " + file + ".\n"
                                                                  "Please check that file is readable and is "
                                                                  "as per the configured structure.\n"
                                                                  "If problem persists contact Administrator.")
            self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

            raise BaseException("Unable to load the file " + file)
        if df is not None and len(df) > 0:
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
            df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
            for col in dtcols:
                if dtpatterns[col] == '%d%m%Y':
                    df[col] = df[col].astype(str)
                    df['tdlen'] = pd.Series(df[col]).str.len()
                    if list(df['tdlen'].unique()) != [8]:
                        df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                    del df['tdlen']
                df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')  # , errors='coerce'
        self.logger.info("Records in file " + os.path.basename(file) + " = %6d" % len(df))
        # print df.columns.tolist()
        # print df.head()
        return df

    ## Load swift files
    def loadSwiftFile(self, feedDef, file, names, csvtypes, positions,
                      dtcols, dtpatterns):
        # Create java command to call java wrapper for parsing swift files and save them as
        path = os.path.dirname(file)
        fileName = os.path.basename(file)
        fileNameNoExt = os.path.splitext(fileName)[0]
        # command = 'java -cp .' + os.pathsep + '..' + os.sep + 'lib' + os.sep + '* ' + \
        command = 'java -cp .' + os.pathsep + config.enginePath + 'lib' + os.sep + '* ' + \
                  'com.algofusion.reconciliation.adapter.swift.SwiftToCSV ' + \
                  self.propsHelper.getProperty('MONGODB_HOST') + ' ' + \
                  self.propsHelper.getProperty('MONGODB_NAME') + ' ' + \
                  str(feedDef['_id']) + ' ' + \
                  (path + os.sep + fileName) + ' ' + \
                  (path + os.sep + fileNameNoExt + '.csv')

        # Output is number of transactions in file
        recs = 0
        try:
            recs = int(subprocess.check_output(command, shell=True))
        except:
            self.logger.error("Unable to load the file " + file + ".\n"
                                                                  "Please check that file is readable and is "
                                                                  "as per the configured structure.\n"
                                                                  "If problem persists contact Administrator.")
            self.utils.addDetailsToSummary(self.jobSummary, 'failureReason', "Unable to load the file " + file)

            raise BaseException("Unable to load the file " + file)
        df = pd.DataFrame(columns=names)
        if recs > 0:
            df = pd.read_csv(path + os.sep + fileNameNoExt + '.csv',
                             header=None, sep='|', names=names,
                             dtype=csvtypes, usecols=positions)
            if df is not None and len(df) > 0:
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].applymap(lambda x: str(x).strip())
                df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace('nan', np.NaN)
                for col in dtcols:
                    if dtpatterns[col] == '%d%m%Y':
                        df[col] = df[col].astype(str)
                        df['tdlen'] = pd.Series(df[col]).str.len()
                        if list(df['tdlen'].unique()) != [8]:
                            df[col] = df.apply(lambda x: '0' + x[col] if x['tdlen'] == 7 else x[col], axis=1)
                        del df['tdlen']
                    df[col] = pd.to_datetime(df[col], format=dtpatterns[col], errors='coerce')
        self.logger.info("Records in file " + fileName + " = %6d" % recs)
        os.remove(path + os.sep + fileNameNoExt + '.csv')


        return df

    ## Method to get the list of files to be moved to processed
    def getFilesToMove(self):
        return self.filesToMove
