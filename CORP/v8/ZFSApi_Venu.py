#!/usr/bin/env python

from logger import Logger
logger = Logger.getInstance("ZFSApi").getLogger()

import os
import time
import json
import libvirt
import jsonpickle
import network
import struct
import pymongo
import subprocess
import dbinterface
import fission_conf
from utilities import *
from collections import namedtuple
from bootKeyMap import *
from flask import session, request
from datetime import datetime
from xml.dom.minidom import Document
from api_interface import requestsInterface
from fissionGrubInstaller import fissionGrubInstaller


cloneDestroy  = "clone:destroyonexit"
encryptStatus = "clone:isencrypted"
zfsBase = "fission/"

class ZFSApi():
    @classmethod
    def getZfsPath(cls, userName=None):
        if userName is None:
            return zfsBase+getLoggedInUser()+"/"
        else:
            return zfsBase+userName+"/"
    
    @classmethod
    def create(cls, path, userName):
        (status, out, err) = execution('zfs create %s%s' % (ZFSApi.getZfsPath(userName), path))
        if status == 0:
            return status
        else:
            logger.error("ZFS create out:%s err:%s" % (out,err))
            return status

    @classmethod
    def setShareSMB(cls, path, userName):
        (status, out, err) = execution('zfs set sharesmb=on %s%s' % (ZFSApi.getZfsPath(userName), path))
        if status == 0:
            return status
        else:
            logger.error("ZFS sharesmb out:%s err:%s" % (out,err))
            return status

    @classmethod
    def setUnShareSMB(cls, path, userName):
        (status, out, err) = execution('zfs set sharesmb=off %s%s' % (ZFSApi.getZfsPath(userName), path))
        if status == 0:
            return status
        else:
            logger.error("ZFS unsharesmb out:%s err:%s" % (out,err))
            return status

    @classmethod
    def delete(cls, path, userName):
        (status, out, err) = execution('zfs destroy -R %s%s' % (ZFSApi.getZfsPath(userName), path))
        if not status:
            return True
        else:
            logger.error("ZFS destroy out:%s err:%s" % (out,err))
            return False

    @classmethod
    def snapshot(cls, ps, timestamp=None):
        # Before calling this method proper login(even as user) is happening so not considering username
        if timestamp is None:
            t = jsonpickle.decode(ps["syncJob"])['Time']
        elif timestamp is not None:
            t = timestamp
        t = datetime.fromtimestamp(t)
        t = t.strftime("%Y_%m_%d_%H_%M_%S")
        snapPath = ZFSApi.getZfsPath(ps['userName'])+str(ps["_id"])+"@"+t
        (status, out, err) = execution('zfs snapshot %s' % snapPath)
        if not status:
            return str(ps["_id"])+"@"+t
        else:
            (status, out, err) = execution("zfs list %s" % snapPath)
            if not status:
                return str(ps["_id"])+"@"+t
            logger.error("ZFS snapshot out:%s err:%s" % (out,err))
            return None

    @classmethod
    def list(cls, syncSystemId, userName="admin"):
        if userName != "admin":
            (status, out, err) = execution('zfs list -t snapshot -o name -rH fission/%s/%s' % (userName, syncSystemId))
        else:
            (status, out, err) = execution('zfs list -t snapshot -o name -rH %s%s' % (ZFSApi.getZfsPath(), syncSystemId))
        if not status:
            return out
        else:
            logger.error("ZFS list out:%s err:%s" % (out,err))
            return ""

    @classmethod
    def getZfsSnapshots(cls, syncSystemId, allsnaps=False):
        #TODO:- Have to add userName
        userName = "admin"
        (status, out, err) = execution("zfs list -o name | grep %s | grep -v clone | cut -d '/' -f2" % syncSystemId)
        if (not status) and len(out.strip()):
            userName = out.strip()

        '''
        if allsnaps:
            cmd = "zfs list -t snapshot -o name -rH fission/%s/%s | cut -d '/' -f3" % (userName, syncSystemId)
        else:
            cmd = "zfs list -t snapshot -o name -rH fission/%s/%s | cut -d '/' -f3 | tail -1" % (userName, syncSystemId)
        (status, out, err) = execution(cmd)
        if not status:
            lines = out.splitlines()
            if allsnaps:
                return lines
            elif len(lines):
                return lines[0]
        '''
        if not os.path.isdir("/fission/%s/%s/.zfs/snapshot" % (userName, syncSystemId)):
            return []
        snapshots = os.listdir("/fission/%s/%s/.zfs/snapshot" % (userName, syncSystemId))
        if len(snapshots):
            snapshots.sort()
            if allsnaps:
                for i in range(0, len(snapshots)):
                    snapshots[i] = "%s@%s" % (syncSystemId, snapshots[i])
                return snapshots
            else:
                return "%s@%s" % (syncSystemId, snapshots[-1])
        return []

    @classmethod
    def rollBack(cls, snapshot):
        # Before calling this method proper login(even as user) is happening so not considering username
        snapshot = ZFSApi.getZfsPath()+snapshot
        (status,out,err) = execution('zfs rollback -r %s' % snapshot)
        if not status:
            return True
        else:
            logger.error("ZFS rollback out:%s err:%s" % (out,err))
            return False

    @classmethod
    def setProperty(cls, snapShot, propName, data):
        # Assuming snapShot contains "fission/"
        (status,out,err) = execution('zfs set %s="%s" %s' % (propName, data, snapShot))
        if not status:
            return True
        else:
            logger.error("ZFS setproperty out:%s err:%s" % (out,err))
            return False

    @classmethod
    def getProperty(cls, snapShot, propName):
        # Assuming snapShot contains "fission/"
        (status,out,err) = execution('zfs get -H -o value %s %s' % (propName,snapShot))
        if not status:
            return out
        else:
            logger.error("ZFS getproperty out:%s err:%s" % (out,err))
            return False

    @classmethod
    def cloneSnapshot(cls, snapShot, destroy, cloneSnapName=None, userName=None):
        # destroy    0 - clone will be destroyed
        #            1 - clone will not be destroyed
        cloneBasePath = ZFSApi.getZfsPath(userName)

        fissionsnap = cloneBasePath+snapShot
        if cloneSnapName is not None:
            cloneSnap = cloneSnapName
        else:
            cloneSnap = cloneBasePath+snapShot.replace('@', '_')+"_clone"
        if checkIsMountPoint("/"+cloneSnap):
            return True, "cloned"

        cmd = "zfs clone %s %s" % (fissionsnap, cloneSnap)
        (status,out,err) = execution(cmd)
        if not status:
            # When clone is created successfully
            # Setting the mode as property on clone
            ZFSApi.setProperty(cloneSnap, cloneDestroy, destroy)

            #Changing MBR if needed
            syncSystemId = snapShot.split('@')[0]
            syncSystem = getSyncSystemData(syncSystemId)
            mbrFile = "/"+cloneBasePath+"mbr/"+syncSystemId+".mbr"
            if os.path.exists(mbrFile):
                cloneSnapDir = "/" + cloneSnap
                files = os.listdir(cloneSnapDir)
                for f in files:
                    if f.endswith(".meta"):
                        of = open(cloneSnapDir+"/"+f, "r")
                        data = json.load(of)
                        of.close()

                        if data['isBootable']:
                            cmd = "dd if=%s of=%s bs=512 count=1 conv=notrunc" % (mbrFile, cloneSnapDir+"/"+f.split(".meta")[0])
                            (status, out, err) = execution(cmd)
                            if not status:
                                logger.info("MBR Changed Successfully")
                            else:
                                logger.info("MBR change Failed with out:%s error:%s" % (out, err))
            backupDrives = []
            if syncSystem and (syncSystem['type'] == "FileStor" and 'fileSyncVersion' in syncSystem and syncSystem['fileSyncVersion'] == 1):
                # Discarding non backup drives in file backups
                if syncSystem['type'] == "FileStor":
                    if os.path.exists("/" + cloneSnap+ "/fileSyncContent.txt"):
                        f = open("/" + cloneSnap+ "/fileSyncContent.txt")
                        data = f.read()
                        f.close()
                        try:
                            backupDrives = json.loads(data)['drives'].split(",")
                        except Exception, e:
                            pass
                fileStorFixDyn = True
            else:
                fileStorFixDyn = False

            if syncSystem and syncSystem['osType'] == 0 and (syncSystem['type'] == "DiskStor" or fileStorFixDyn):
                cloneSnapDir = "/" + cloneSnap
                files = os.listdir(cloneSnapDir)
                for f in files:
                    if f.endswith(".meta"):
                        of = open(cloneSnapDir + "/" + f, "r")
                        data = json.load(of)
                        of.close()
                        if data['isBootable']:
                            if syncSystem['type'] == "FileStor":
                                if ("mountPointName" not in data) or ("mountPointName" in data and data["mountPointName"].lower() not in backupDrives):
                                    break
                            ZFSApi.fixDynamicDisks(cloneSnapDir + "/" + f.split(".meta")[0])
                            break

            return True, "cloned"
        else:
            logger.error("Clone error: %s %s" % (out, err))

        return False, "error"
#        else:
#            ret = int(ZFSApi.getProperty(cloneSnap,cloneDestroy))
#            if ret == 0 and destroy == 1:
#                ZFSApi.setProperty(cloneSnap,cloneDestroy,destroy) #Setting the mode as property on clone
#            print "Clone Failed,May be already exists Using existing Clone",out,err
#            return False

    @classmethod
    def destroyClone(cls, snapShot, userName=None):
        cloneBasePath = ZFSApi.getZfsPath(userName)

        if 'fission/' in snapShot:
            if snapShot[0] == "/":
                cloneSnap = snapShot[1:]
            else:
                cloneSnap = snapShot
        else:
            cloneSnap = cloneBasePath+snapShot.replace('@', '_')+"_clone"
        #Checking for Encryption,if encryption is on unmounting
        #propValue = int(ZFSApi.getProperty(cloneSnap,encryptStatus))
        #if propValue == 1:
        #    ZFSApi.unmountEcryptfs(cloneSnap)
        if not os.path.exists("/"+cloneSnap):
            logger.info("Clone %s does not exist, so returning success." % cloneSnap)
            return True

        propValue = int(ZFSApi.getProperty(cloneSnap, cloneDestroy))
        if propValue == 0:
            # clone can be destroyed
            time.sleep(5)
            for i in xrange(0, 5):
                (status, out, err) = execution('zfs destroy -R %s' % cloneSnap)
                if not status:
                    return True
                else:
                    logger.error("ZFS destroy out:%s err:%s" % (out, err))
                    time.sleep(5)
                    continue

            if not os.path.exists("/"+cloneSnap):
                logger.info("Clone %s does not exist, so returning success." % (cloneSnap))
                return True
            if status:
                logger.error("Failed to destroy clone even after sleeping why???")
                return False
        return True

    @classmethod
    def listFiles(cls, path):
        try:
            files = os.listdir(path)
        except OSError, e:
            return []

        diskFile = []
        diskMapping = {}
        for file in files:
            if not file.endswith(".meta"):
                if file+".meta" in files:
                    if os.path.getsize(path+"/"+file+".meta") == 0:
                        continue
                    diskFile.append(file)
            elif file.endswith(".meta"):
                if os.path.getsize(path+"/"+file) == 0:
                    continue
                fileName = file.split(".meta")[0]
                if fileName in files:
                    f = open(path+"/"+file, "r")
                    data = json.load(f)
                    diskMapping[os.path.splitext(file)[0]] = data['mountPointName']
        finalList = sorted(diskFile,key=diskMapping.__getitem__)
        return finalList

    @classmethod
    def kvmXML(cls, data, snapshot, fusePath, ram, cpu, cpuType, mode=0, netIface=None):
        doc = Document()

        # TODO:
        # To Check vmware backup is uefi or not
        # If uefi we have to add uefi bios file
        # Have to add --bios = /usr/share/ovmf/OVMF.fd
        # if (data['type'] == 3) and data['isuefi']:
        #    pass

        domain = doc.createElement("domain")
        domain.setAttribute("type","kvm")
        doc.appendChild(domain)

        name = doc.createElement("name")
        vmname = snapshot.replace("@","")
        vmname = vmname.replace("_","")
        text = doc.createTextNode(vmname)
        name.appendChild(text)
        domain.appendChild(name)

        mem  = doc.createElement("memory")
        text = doc.createTextNode(str(ram))
        mem.appendChild(text)
        domain.appendChild(mem)

        curmem  = doc.createElement("currentMemory")
        text = doc.createTextNode(str(ram))
        curmem.appendChild(text)
        domain.appendChild(curmem)

        vcpu = doc.createElement("vcpu")
        text = doc.createTextNode(str(cpu))
        vcpu.appendChild(text)
        domain.appendChild(vcpu)

        if cpuType != "host":
            cputype = doc.createElement("cpu")
            model = doc.createElement("model")
            text = doc.createTextNode(cpuType)
            model.appendChild(text)
            cputype.appendChild(model)
            domain.appendChild(cputype)

        osatt  = doc.createElement("os")

        ostype = doc.createElement("type")
        text = doc.createTextNode("hvm")
        ostype.appendChild(text)
        osatt.appendChild(ostype)

        osbootmenu = doc.createElement("bootmenu")
        osbootmenu.setAttribute("enable", "yes")
        osatt.appendChild(osbootmenu)

        boot = doc.createElement("boot")
        boot.setAttribute("dev","hd")
        osatt.appendChild(boot)
        domain.appendChild(osatt)

        features = doc.createElement("features")
        acpi = doc.createElement("acpi")
        features.appendChild(acpi)
        domain.appendChild(features)

        clockoff = doc.createElement("clock")
        clockoff.setAttribute("offset",'localtime')
        domain.appendChild(clockoff)

        prop = doc.createElement("on_poweroff")
        text = doc.createTextNode("destroy")
        prop.appendChild(text)
        domain.appendChild(prop)

        prop = doc.createElement("on_reboot")
        text = doc.createTextNode("restart")
        prop.appendChild(text)
        domain.appendChild(prop)

        prop = doc.createElement("on_crash")
        text = doc.createTextNode("destroy")
        prop.appendChild(text)
        domain.appendChild(prop)

        devices  = doc.createElement("devices")
        emulator = doc.createElement("emulator")
        text     = doc.createTextNode("/usr/bin/kvm")
        emulator.appendChild(text)
        devices.appendChild(emulator)

        volumes = ZFSApi.listFiles(fusePath)
        #Restrict to only 4 drives as virtio drivers are not supported yet
        startLetter = 98 #letter 'b'
        startLetterScsi = 97
        dataDiskCount = 0
        for voluuid in volumes:
            metafile = fusePath+voluuid+".meta"
            f = open(metafile,'r')
            volData = json.load(f)
            f.close()

            diskFile = fusePath+voluuid
            target = doc.createElement("target")
            if data['osType'] == 1:
                if (volData['isBootable'] is True): # or (voluuid == bootVolume):
                    target.setAttribute("dev", "hda")
                    dataDiskCount += 1
                    target.setAttribute("bus", "ide")
                elif dataDiskCount < 3:
                    target.setAttribute("dev", "hd"+chr(startLetter))
                    startLetter += 1
                    dataDiskCount += 1
                    target.setAttribute("bus","ide")
                else:
                    target.setAttribute("dev", "sd"+chr(startLetterScsi))
                    startLetterScsi += 1
                    dataDiskCount += 1
                    target.setAttribute("bus","scsi")
            else:
                if (volData['isBootable'] is True): # or (voluuid == bootVolume):
                    target.setAttribute("dev","hda")
                    #Fix boot disk if its sectors per track is != 63, this issues was faced at a customer location for SSI
                    if data['osType'] == 0:
                        ZFSApi.fixBootDisk(diskFile)
                elif dataDiskCount < 3:
                    target.setAttribute("dev","hd"+chr(startLetter))
                    startLetter += 1
                    dataDiskCount += 1
                else:
                    continue
                target.setAttribute("bus","ide")

            disk = doc.createElement("disk")
            disk.setAttribute("type","block")
            disk.setAttribute("device","disk")
            source = doc.createElement("source")
            source.setAttribute("dev", diskFile)
            disk.appendChild(source)

            disk.appendChild(target)

            devices.appendChild(disk)

        for nwIface in data['networkInterfaces']:
            interface = doc.createElement("interface")
            logger.info("mode: %d"% mode)
            netdevice = "eth0"

            if not netIface:
                try:
                    ifaceList = network.getInterfaces()
                except Exception, e:
                    logger.info("Error getting network : %s" % e)
                    ifaceList = []
                if len(ifaceList) != 0:
                    netdevice = ifaceList[0]
            else:
                netdevice = netIface
            if mode == 1:
                # Live Mode
                interface.setAttribute("type","direct")
                mac = doc.createElement("mac")
                mac.setAttribute("address",nwIface['mac'])
                interface.appendChild(mac)
                source = doc.createElement("source")
                source.setAttribute("dev",netdevice)
                source.setAttribute("mode","bridge")
                interface.appendChild(source)
            elif mode == 0:
                # Test Mode
                interface.setAttribute("type","network")
                source = doc.createElement("source")
                source.setAttribute("network","default")
                interface.appendChild(source)

                # Adding filter for test mode to block outbound
                (status, out, err) = execution('virsh nwfilter-list | grep no-outbound')
                if status:
                    (status, out, err) = execution('virsh nwfilter-define nooutbound.xml')
                (status, out, err) = execution('virsh nwfilter-list | grep no-outbound')
                if not status:
                    blocknw = doc.createElement("filterref")
                    blocknw.setAttribute("filter", "no-outbound")
                    interface.appendChild(blocknw)

            devices.appendChild(interface)

        tab = doc.createElement("input")
        tab.setAttribute("type","tablet")
        tab.setAttribute("bus","usb")
        devices.appendChild(tab)

        #mouse = doc.createElement("input")
        #mouse.setAttribute("type","mouse")
        #mouse.setAttribute("bus","ps2")
        #devices.appendChild(mouse)

        graphics = doc.createElement("graphics")
        graphics.setAttribute("type","vnc")
        graphics.setAttribute("port","-1")
        graphics.setAttribute("autoport","yes")
        devices.appendChild(graphics)

        domain.appendChild(devices)

        #print doc.toprettyxml()
        return doc.toprettyxml()

    @classmethod
    def fixBootDisk(cls, bootDisk):
        #Read mbr and find if its single or 2 partition install
        bootDiskFile = open(bootDisk, "r")
        mbr = bootDiskFile.read(512)
        if ord(mbr[511]) == 0XAA and ord(mbr[510]) == 0X55:
            logger.info("Found valid mbr")
            partitionFormat="<BBBBBBBBII"
            Partition=namedtuple('Partition', ['Status', "StartHead", "StartSec", "StartCyl", "FileSystem", "EndHead", "EndSec", "EndCyl", "FirstLBA", "NumLBA"])
            BPB=namedtuple('BPB', ['SizeOfSector','NumberOfSectorsInAnAllocationUnit','NumberOfReservedSectors','NumberOfFileAllocationTables','NumberOfRecordsInFixedSizeRootDirectory','TotalNumberOfSectorsInVolume','MediaDescriptionByte','SectorsPerFAT','SectorsPerTrack','NumberOfHeads','NumberOfHiddenSectors','TotalNumberOfSectorsInTheVolume','DiscUnitNumber','flags','SignatureByte','ReservedForFutureUse','SectorsInVolume','MFTFirstClusterNumber','MFTMirrorFirstClusterNumber','MFTRecordSize','IndexBlockSize','VolumeSerialNumber','Checksum'])
            bpbFormat="<HBHBHHBHHHIIBBBBQQQIIQI"
            numValidPartitions = 0
            bootPartition = None
            bootPartitionIndex = -1
            for i in xrange(4):
                part = Partition._make(struct.unpack_from(partitionFormat, mbr, 446+(16*i)))
                logger.info(str(part))
                if part.Status != 0:
                    numValidPartitions += 1
                if part.Status == 0x80:
                    bootPartition = part
                    bootPartitionIndex = i
                # This is for double extra care(if the File System is not 7 changing it to 7)
                if part.FileSystem != 0 and part.FileSystem != 7:
                    bootFileWrite = open(bootDisk, "r+")
                    bootFileWrite.seek(446+(16*i)+4)
                    bootFileWrite.write('\x07')
                    bootFileWrite.close()
            logger.info("numValidPartitions: %d " % numValidPartitions)
            if numValidPartitions == 1:
                logger.info("Single Partition")
                if bootPartition:
                    logger.info("Found Boot Partition")
                    logger.info("FirstLBA: %d %d " % (bootPartition.FirstLBA, bootPartition.FirstLBA*512))
                    bootDiskFile.seek(bootPartition.FirstLBA*512, 0)
                    bpb = BPB._make(struct.unpack_from(bpbFormat, bootDiskFile.read(512), 11))
                    logger.info("BPB: %s" % str(bpb))
                    if bpb.SectorsPerTrack != 63:
                        logger.info("SectorsPerTrack requires fix")
                        if bpb.SectorsPerTrack == 32 and bootPartition.FirstLBA == 32:
                            #Fix the BPB
                            bootFileWrite = open(bootDisk, "r+")
                            bootFileWrite.seek((bootPartition.FirstLBA*512)+24, 0)
                            bootFileWrite.write('\x3F')
                            #Fix mbr Partition
                            bootFileWrite.seek(446+(16*bootPartitionIndex)+1)
                            bootFileWrite.write('\x00\x21\x00')
                            bootFileWrite.close()
                        else:
                            logger.info("Not satisfying SectorsPerTrack==32 and FirstLBA == 32, not handling")
                    else:
                        logger.info("No SectorsPerTrack Fix")
                else:
                    logger.info("Missing Boot Partition, not handling")
            else:
                logger.info("More than one partition, not handling")
        else:
            logger.info("No Valid mbr not fixing disk")

        return True

    @classmethod
    def fixDynamicDisks(cls, bootDiskPath):
        bootDiskFile = open(bootDiskPath, "r")
        mbr = bootDiskFile.read(512)
        if ord(mbr[511]) == 0XAA and ord(mbr[510]) == 0X55:
            partitionFormat = "<BBBBBBBBII"
            Partition=namedtuple('Partition', ['Status', "StartHead", "StartSec", "StartCyl", "FileSystem", "EndHead", "EndSec", "EndCyl", "FirstLBA", "NumLBA"])
            for i in xrange(4):
                part = Partition._make(struct.unpack_from(partitionFormat, mbr, 446+(16*i)))
                if part.FileSystem != 0 and part.FileSystem != 7:
                    logger.info("Fixing Dynamic disk for:%s FileSystemType:%s" % (bootDiskPath, part.FileSystem))
                    bootFileWrite = open(bootDiskPath, "r+")
                    bootFileWrite.seek(446+(16*i)+4)
                    bootFileWrite.write('\x07')
                    bootFileWrite.close()
        bootDiskFile.close()

    @classmethod
    def _convert_to_12_hour(cls, snapshot):
        ts = snapshot.split('@')[1]
        tsl = ts.split('_')
        h = int(tsl[3])
        if h > 12:
            tsl[3] = str(h-12)
            tsl.append('pm')
        else:
            tsl.append('am')

        return "_".join(tsl)

    @classmethod
    def mountSnapShot(cls, snapShot, userName, syncSystem, mountUser, encPass):
        # check whether fissionimgexports is mounted or not
        mountBase = "fissionimgexports"
        if syncSystem['type'] == "FileStor":
            if syncSystem['osType'] == 0 and 'fileSyncVersion' in syncSystem and syncSystem['fileSyncVersion'] == 1:
                mountBase = "fissionfileexports"
            elif (syncSystem['osType'] == 1 and 'isZvol' in syncSystem and syncSystem['isZvol'] is False) or (syncSystem['osType'] == 2):
                mountBase = "fissionfileexports"
            else:
                mountBase = "fissionimgexports"
        exists = checkIsMountPoint("/fission/%s/" % mountBase)
        if not exists:
            logger.info("/fission/%s/ not mounted while trying to mount snapshot" % mountBase)
            return False, "Error Mounting, check storage"

        dirName = syncSystem['systemName']
        #dirPath = "/fission/" + mountBase + '/' + userName + "/MountedVolumes/" + dirName + "_" + ZFSApi._convert_to_12_hour(snapShot)
        dirPath = "/fission/" + mountBase + '/'+ mountUser + "/MountedVolumes/" + dirName + "_" + ZFSApi._convert_to_12_hour(snapShot)

        cloneSnap = ""
        status = ""
        msg = ""
        if syncSystem['type'] == "FileStor" and syncSystem['osType'] == 0 and 'fileSyncVersion' not in syncSystem:
            cloneSnap = dirPath
            (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0, cloneSnap[1:], userName=userName)
        elif (syncSystem['type'] == "FileStor" and syncSystem['osType'] == 1 and 'isZvol' in syncSystem and syncSystem['isZvol'] is False):
            cloneSnap = dirPath
            (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0, cloneSnap[1:], userName=userName)
        elif syncSystem['osType'] == 2:
            cloneSnap = dirPath
            (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0, cloneSnap[1:], userName=userName)
        else:
            cloneSnap = "/"+ZFSApi.getZfsPath(userName)+snapShot.replace('@','_')+"_clone"
            (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0, userName=userName)

        if not status:
            execution("rm -rf %s" % dirPath)
            return False, "Error Mounting, Clone failed."

        pathToMountVolumes = list()
        fname = "/etc/init/"+snapShot.replace('@','_')+"mountVolumes.conf"
        lines = list()
        lines.append("start on mountvolumes\n")
        lines.append("script\n")

        # if syncSystem['type'] == "FileStor":
        #     if os.path.exists(dirPath):
        #         execution("rm -rf %s" % dirPath)
        #     cmd = "ln -s %s %s" % (cloneSnap, dirPath)
        #     lines.append(cmd+"\n")
        if (syncSystem['type'] == "DiskStor") or ('fileSyncVersion' in syncSystem and syncSystem['fileSyncVersion'] == 1):
            if not os.path.exists(dirPath):
                try:
                    os.makedirs(dirPath)
                except OSError, e:
                    logger.info("Creating %s dir failed with error:%s" % (dirPath, e))
                    return False, "Error Mounting, Directory creation failed."

            volumes = ZFSApi.listFiles(cloneSnap)
            for voluuid in volumes:
                metafile = cloneSnap+"/"+voluuid+".meta"
                f = open(metafile,'r')
                volData = json.load(f)
                f.close()

                volpath = dirPath + "/" + volData['mountPointName']
                if not os.path.exists(volpath):
                    try:
                        os.makedirs(volpath)
                        pathToMountVolumes.append(volpath)
                    except OSError, e:
                        logger.info("Creating %s dir failed with error:%s" % (volpath, e))
                        ZFSApi.destroyClone(snapShot, userName=userName)
                        execution("rm -rf %s" % (dirPath))
                        return False, "Error Mounting, Volume directory creation failed."

                fileSystemType = 'ntfs'
                if 'filesystemType' in volData:
                    fileSystemType = volData['filesystemType'].lower()

                mountType = 'ro'

                if syncSystem['osType'] == 1 and 'agentVersion' in syncSystem:
                    # mountType = 'ro,norecovery'
                    mountType = 'rw'

                if volData['volumeStartOffset'] == 0:
                    cmd = "mount -t %s -o loop,%s,offset=0 %s/%s %s || true" % (fileSystemType, mountType, cloneSnap, voluuid, volpath)
                    lines.append(cmd+"\n")
                else:
                    cmd = "mount -t %s -o loop,%s,offset=%s %s/%s %s || true" % (fileSystemType, mountType, volData['volumeStartOffset'], cloneSnap, voluuid, volpath)
                    lines.append(cmd+"\n")

        lines.append("end script\n")
        if not os.path.exists(fname):
            f = open(fname, "w")
            f.writelines(lines)
            f.close()
        cmd = "initctl start %s" % (snapShot.replace('@','_')+"mountVolumes")
        execution(cmd)
        #TODO: handle based on initctl status
        time.sleep(3)
        successCount = 0
        for pathToMount in  pathToMountVolumes:
            if checkIsMountPoint(pathToMount):
                successCount += 1

        # Checking if content file is available
        if mountBase == "fissionfileexports":
            contentFile = dirPath + "/fileSyncContent.txt"
            contentFile_clone = cloneSnap + "/fileSyncContent.txt"
            if os.path.exists(contentFile_clone):
                execution("cp -f %s %s" % (contentFile_clone, contentFile))
            else:
                ri = requestsInterface()
                resp = ri.action("file_stor", syncSystem['uuid'], "getFileSyncConfiguration")
                if resp['status'] == "ok":
                    fc = open(contentFile, "w")
                    confDict = resp['msg']
                    if confDict['excludes'] == "*.*" and confDict['includes'] == "":
                        confDict['includes'] = "*.*"
                        confDict['excludes'] = ""
                    json.dump(confDict, fc)
                    fc.close()
                else:
                    logger.info("Querying file content failed with error:%s" % msg)
        return True, "Mounted %d of %d volume(s)" % (successCount, len(pathToMountVolumes))

    @classmethod
    def unMountSnapShot(cls, snapShot, userName, syncSystem, mountUser):
        dirName = syncSystem['systemName']
        mountBase = "fissionimgexports"
        if syncSystem['type'] == "FileStor":
            if syncSystem['osType'] == 0 and 'fileSyncVersion' in syncSystem and syncSystem['fileSyncVersion'] == 1:
                mountBase = "fissionfileexports"
            elif (syncSystem['osType'] == 1 and 'isZvol' in syncSystem and syncSystem['isZvol'] is False) or (syncSystem['osType'] == 2):
                mountBase = "fissionfileexports"
            else:
                mountBase = "fissionimgexports"
        cloneSnap = "/"+ZFSApi.getZfsPath(userName)+snapShot.replace('@','_')+"_clone"
        try:
            (status, out, err) = execution("mount -l | grep %s | grep MountedVolumes" % cloneSnap)
            if not status:
                out = out.strip().split("\n")
                mountPoint = out[0].strip().split()[2]
                if "fissionimgexports" in mountPoint:
                    mountBase = "fissionimgexports"
                elif "fissionfileexports" in mountPoint:
                    mountBase = "fissionfileexports"
        except Exception, e:
            logger.info("Exception:%s" % e)
        # dirPath = "/fission/"+mountBase+'/'+userName+"/MountedVolumes/" + dirName + "_" + ZFSApi._convert_to_12_hour(snapShot)
        dirPath = "/fission/" + mountBase + '/' + mountUser + "/MountedVolumes/" + dirName + "_" + ZFSApi._convert_to_12_hour(snapShot)
        if not os.path.exists(dirPath):
            if checkIsMountPoint(cloneSnap):
                ZFSApi.destroyClone(snapShot, userName=userName)
            return True, "Not Mounted"

        snap = snapShot
        if (syncSystem['type'] == "DiskStor") or ('fileSyncVersion' in syncSystem and syncSystem['fileSyncVersion'] == 1):
            volumes = ZFSApi.listFiles(cloneSnap)
            unMountMountPointsInPath(dirPath)
            for voluuid in volumes:
                metafile = cloneSnap+"/"+voluuid+".meta"
                f = open(metafile,'r')
                volData = json.load(f)
                f.close()

                volpath = dirPath + "/" + volData['mountPointName']
                if checkIsMountPoint(volpath):
                    (status,out,err) = execution("umount -f %s" % (volpath))
                    logger.info("Volume UnMounting %s %s" % (out, err))
                if os.path.exists(volpath):
                    (status,out,err) = execution("rm -rf  %s" % (volpath))
            (status,out,err) = execution("rm -rf %s" % dirPath)
            # Deleting the link's(created during file download)
            newfiledownloadpath = "/fission/downloads/"+dirName + "_" + ZFSApi._convert_to_12_hour(snapShot)
            if os.path.exists(newfiledownloadpath):
                (status, out, err) = execution("rm -rf %s" % newfiledownloadpath)
        elif syncSystem['type'] == "FileStor":
            if syncSystem['osType'] == 0 and ('fileSyncVersion' not in syncSystem):
                snap = dirPath
            elif (syncSystem['osType'] == 1 and 'isZvol' in syncSystem and syncSystem['isZvol'] is False) or (syncSystem['osType'] == 2):
                snap = dirPath
        if ZFSApi.destroyClone(snap, userName):
            cmd = "rm -f /etc/init/"+snapShot.replace('@','_')+"mountVolumes.conf"
            execution(cmd)
            logger.info("SnapShot and volumes UnMounted")
            return True, "Un Mounted"
        else:
            logger.info("Unable to destroy clone while unmounting %s" % snapShot)
            return False, "Error, Destroy Clone Failed"

    @classmethod
    def startVM(cls, snapShot, syncSystem, ram, cpu, mode, bootkey=None, encPass=None, cpuType="host", netIface=None):
        cloneSnap = snapShot.replace('@', '_')+"_clone"
        vmName = snapShot.replace("@", "")
        vmName = vmName.replace("_", "")
        snapData = getSnapData(snapShot)

        # Getting userName
        userName = "admin"
        if 'userName' in syncSystem:
            userName = syncSystem['userName']

        conn = libvirt.open(None)
        # If XML is already defined, and machine not running, undefine it and continue
        # If XML is already defined, and machine is running, return error
        try:
            dom = conn.lookupByName(vmName)
            if dom.isActive():
                conn.close()
                return False, "VM Start Failed, Already running"
            dom.undefine()
        except Exception, e:
            try:
                if 'Domain not found' in str(e):
                    pass
                else:
                    logger.info("Exception:%s" % e)
            except:
                logger.info("Exception:%s" % e)

        (status, msg) = ZFSApi.cloneSnapshot(snapShot, mode, userName=userName)
        if not status:
            return status, "Error starting VM, Clone failed."

        hasBootableVolume = False
        path = "/fission/"+userName+'/'+cloneSnap
        volumes = ZFSApi.listFiles(path)
        for voluuid in volumes:
            metafile = path+"/"+voluuid+".meta"
            f = open(metafile, 'r')
            volData = json.load(f)
            f.close()
            if volData['isBootable'] is True:
                hasBootableVolume = True

        if not hasBootableVolume:
            ZFSApi.destroyClone(snapShot, userName=userName)
            return False, "VM Start Failed, No bootable disk found"

        basePath = "/"+ZFSApi.getZfsPath(userName)+cloneSnap+"/"
        fusePath = basePath
        if syncSystem['osType'] == 0:
            fusePath = "/"+ZFSApi.getZfsPath(userName)+cloneSnap+"_fuse/"
            if not ZFSApi.fuseAddMbr(basePath, fusePath, syncSystem):
                ZFSApi.destroyClone(snapShot, userName=userName)
                return False, "Failed to add MBR."

        # http://ubuntuforums.org/showthread.php?t=1364782 -> autostart fix
        # start on (runlevel [2345] and net-device-up IFACE=br0) -> /etc/init/libvirt-bin.conf
        elif syncSystem['osType'] == 1:
            logger.info("Preparing for virtualization %s ..." % snapShot)
            if not(("prevUsage" in snapData) and (len(snapData["prevUsage"]) != 0)):
                fissGrub = fissionGrubInstaller('', syncSystem)
                fissGrub.createSnapGrubInstallScript(snapData)
                fissGrub.installGrub(snapShot, int(float(ram)*1024), cpu=cpu)
            logger.info("virtualizing %s ..." % snapShot)

        conn.defineXML(ZFSApi.kvmXML(syncSystem, snapShot, fusePath, int(float(ram)*1024*1024), cpu, cpuType, mode, netIface=netIface))
        #if mode == 1:  #Uncomment this line to start only live vm's on reboot
        (status,out,err) = execution("virsh autostart "+vmName)

        # Sending the key at the boot time
        if bootkey is not None:
            value = getKeyMap(bootkey)

            fname = "/etc/init/"+vmName+"sendKey.conf"
            if not os.path.exists(fname):
                f = open(fname, "w")
                lines = list()
                lines.append("script\n")
                lines.append("for i in `seq 1 600`\n")
                lines.append("do\n")
                lines.append("virsh send-key %s %s\n"%(vmName,value))
                lines.append("sleep 0.1\n")
                lines.append("done\n")
                lines.append("rm %s\n"% (fname))
                lines.append("end script\n")
                f.writelines(lines)
                f.close()
            execution("initctl start %ssendKey" % vmName)

        dom = conn.lookupByName(vmName)
        dom.create()
        time.sleep(3)
        if not dom.isActive():
            ZFSApi.destroyClone(snapShot, userName=userName)
            return False, "VM Creation Failed"
        conn.close()
        return True, "VM Started"

    @classmethod
    def forceStopVM(cls, snapShot):
        vmName = snapShot.replace("@","")
        vmName = vmName.replace("_","")

        conn = libvirt.open(None)
        try:
            dom = conn.lookupByName(vmName)
            if dom.isActive():
                dom.destroy()
        except:
            logger.info("Strange couldn't find domain.")
        conn.close()

    @classmethod
    def stopVM(cls, snapShot):
        vmName = snapShot.replace("@", "")
        vmName = vmName.replace("_", "")

        # Getting the UserName
        syncId = snapShot.split('@')[0]
        fis_sys = dbinterface.MongoCollection("sync_systems")
        syncData = fis_sys.find_one({"uuid": syncId})
        userName = "admin"
        if 'userName' in syncData:
            userName = syncData['userName']

        conn = libvirt.open(None)
        try:
            dom = conn.lookupByName(vmName)
            if dom.isActive():
                dom.shutdown()
            waitCount = 0
            while dom.isActive():
                time.sleep(3)
                waitCount += 1
                if waitCount == 100:
                    dom.destroy()
            dom.undefine()
        except:
            logger.info("Strange couldn't find domain, maybe force stopped.")

        conn.close()
        (status,out,err) = execution("initctl stop "+vmName)
        (status,out,err) = execution("rm -rf /etc/init/%s.conf" % (vmName))
        (status,out,err) = execution("virsh autostart %s --disable"%(vmName))

        # Destroy fuse
        cloneSnap = snapShot.replace('@', '_')+"_clone"
        fusePath = "/"+ZFSApi.getZfsPath(userName)+cloneSnap+"_fuse/"
        if not ZFSApi.fuseDestroy(fusePath):
            logger.error("fusermount -u %s failed" % fusePath)

        # Destroy Clone if VM is started in test mode
        if ZFSApi.destroyClone(snapShot, userName):
            return True, "VM Stopped"
        else:
            return False, "Error, Destroy Clone Failed"

    @classmethod
    def terminal(self, snapShot, ip):
        vmName = snapShot.replace("@","")
        vmName = vmName.replace("_","")

        conn = libvirt.open(None)
        try:
            dom = conn.lookupByName(vmName)
            if dom.isActive():
                fname = "/etc/init/"+vmName+".conf"
                if not os.path.exists(fname):
                    f = open(fname, "w")
                    lines=[]
                    lines.append("script\n")
                    lines.append("cd /opt/noVNC\n")
                    lines.append("disp=`virsh vncdisplay %s`\n" % (vmName))
                    lines.append("IP=`echo $disp | cut -d':' -f1`\n")
                    lines.append("PORT=`echo $disp | cut -d':' -f2`\n")
                    lines.append("./utils/websockify --web ./ $((PORT + 15000))  127.0.0.1:$((PORT + 5900))\n")
                    lines.append("end script\n")
                    f.writelines(lines)
                    f.close()
                execution("initctl start %s" % vmName)
                (status,out,err) = execution("virsh vncdisplay %s" % vmName)
                #return request.url_root[:-1] +":"+str(15000+int(out.strip().split(":")[1]))+"/vnc.html"
                selectedIp = "127.0.0.1"
                selectedIp = ip
                # interfaces = network.show("all")
                # interfaces = sorted(interfaces, key=lambda intf: intf['interface'])
                # for interface in interfaces:
                #     if len(interface["address"].strip()):
                #         selectedIp = interface["address"]
                #         break

                return True, "http://"+ selectedIp +":"+str(15000+int(out.strip().split(":")[1]))+"/vnc.html"
        except:
            pass

        return False, "None"

    @classmethod
    def backupCompleteCheck(cls, syncSystem):
        retValue = True
        path = "/fission/"
        if "userName" in syncSystem:
            path += syncSystem['userName'] + "/"
        else:
            path += "admin/"
        path += syncSystem['uuid'] + "/"
        dataFiles = ZFSApi.listFiles(path)
        if len(dataFiles) == 0:
            logger.info("No backup Disks Found! for %s" % syncSystem['uuid'])
            return False

        mPath = "/tmp/backupcheck/" + syncSystem['uuid']
        if os.path.exists(mPath):
            execution("umount -f %s" % mPath)
        else:
            execution("mkdir -p %s" % mPath)

        # Mount each backup disk and check if it mounts

        #Discarding non backup drives in file backups
        backupDrives = []
        if syncSystem['type'] == "FileStor":
            if os.path.exists(path+"fileSyncContent.txt"):
                f = open(path+"fileSyncContent.txt")
                data = f.read()
                f.close()
                try:
                    backupDrives = json.loads(data)['drives'].split(",")
                except Exception, e:
                    pass

        for dataFile in dataFiles:
            mFile = path+dataFile+".meta"
            if os.path.exists(mFile):
                metaFile = open(mFile, "r")
                data = json.load(metaFile)
                metaFile.close()
                if syncSystem['type'] == "FileStor":
                    if ("mountPointName" not in data) or ("mountPointName" in data and data["mountPointName"].lower() not in backupDrives):
                        continue
                if not os.path.exists(mPath):
                    execution("mkdir -p %s" % mPath)
                if data['isBootable']:
                    cmd = "mount -o offset=%s %s %s" % (data['volumeStartOffset'], path+dataFile, mPath)
                else:
                    cmd = "mount %s %s" % (path+dataFile, mPath)
            else:
                logger.info("%s meta file not found!" % mFile)
                if os.path.exists(mPath):
                    execution("umount -f %s" % mPath)
                    execution("rm -rf %s" % mPath)
                return False

            (status, out, err) = execution(cmd)
            if status == 0:
                dirList = []
                try:
                    dirList = os.listdir(mPath)
                except Exception, e:
                    dirList = []
                    logger.info('Exception in dirList: %s' % mPath)
                if not len(dirList):
                    logger.info("Directory is empty after mounting %s" % path+dataFile)
                    retValue = False
                else:
                    execution("umount -f %s" % mPath)
            else:
                logger.info("Failed to mount %s" % path+dataFile)
                retValue = False

            if not retValue:
                execution("umount -f %s" % mPath)
                # Checking if the issue can be fixed using ntfsfix
                retValue = True
                logger.info("performing ntfsfix for:%s" % path+dataFile)
                if data['isBootable']:
                    status, device = getLoDevice(logger, path+dataFile, data['volumeStartOffset'])
                    if status:
                        status, out, err = execution("/bin/ntfsfix %s" % device)
                        execution("losetup -d %s" % device)
                    #TODO:- has to handle lo device unavailability
                else:
                    status, out, err = execution("/bin/ntfsfix %s" % path+dataFile)
                if status:
                    logger.info("ntfsfix failed with out:%s error:%s" % (out, err))
                    retValue = False
                else:
                    # ntfsfix was successfull, remounting and checking
                    if data['isBootable']:
                        cmd = "mount -o offset=%s %s %s" % (data['volumeStartOffset'], path+dataFile, mPath)
                    else:
                        cmd = "mount %s %s" % (path+dataFile, mPath)
                    (status, out, err) = execution(cmd)
                    if status == 0:
                        dirList = []
                        try:
                            dirList = os.listdir(mPath)
                        except Exception, e:
                            dirList = []
                            logger.info('Exception in dirList after ntfsFix: %s' % mPath)
                        if not len(dirList):
                            logger.info("Directory is empty after mounting %s (ntfsfix)" % (path+dataFile))
                            retValue = False
                    else:
                        logger.info("Failed to mount %s (ntfsfix)" % (path+dataFile))
                        retValue = False
                    execution("umount -f %s" % mPath)

                if retValue is False:
                    execution("umount -f %s" % mPath)
                    execution("rm -rf %s" % mPath)
                    return retValue
        execution("umount -f %s" % mPath)
        execution("rm -rf %s" % mPath)
        return True

    @classmethod
    def poolStatus(cls):
        (status,out,err) = execution("zpool status fission")
        if not status:
            return out
        else:
            return ""

    @classmethod
    def zfsFullBackupUsage(cls, syncSytemId):
        syncSystem = getSyncSystemData(syncSytemId)
        userName = syncSystem['userName']
        if (syncSystem['osType'] == 1) and not ('isZvol' in syncSystem and syncSystem['isZvol'] is False):
            return "0"
        (status, out, err) = execution("zfs list fission/%s/%s -o used -rH" % (userName, syncSytemId))
        if not status and len(out):
            return out.strip()
        return "0"

    @classmethod
    def zfsFirstAvailableSnapUsage(cls, syncSystemId):
        snapshotsList = ZFSApi.getZfsSnapshots(syncSystemId, allsnaps=True)
        if len(snapshotsList) > 0:
            usage = "0"
            snapshot = snapshotsList[0]
            snapshotId = snapshot.split("@")[1]
            syncSystem = getSyncSystemData(syncSystemId)
            userName = syncSystem['userName']
            if (syncSystem['osType'] == 1) and not ('isZvol' in syncSystem and syncSystem['isZvol'] is False):
                return ""
            (status, out, err) = execution("zfs list -t snapshot -o name,refer -rH fission/%s/%s | grep %s | tr -s ' ' ' ' | cut -d ' ' -f1,2" % (userName, syncSystemId, snapshotId))
            if not status and len(out):
                return out.strip().split()[1]
        return ""

    @classmethod
    def zfssnapshotusage(cls, syncSystemId):
        snapUsage = {}
        syncSystem = getSyncSystemData(syncSystemId)
        userName = syncSystem['userName']
        if (syncSystem['osType'] == 1) and not ('isZvol' in syncSystem and syncSystem['isZvol'] is False):
            return snapUsage
        (status, out, err) = execution("zfs list -t snapshot -o name,used -rH fission/%s/%s | tr -s ' ' ' ' | cut -d ' ' -f1,2" % (userName, syncSystemId))
        if not status and len(out):
            lines = out.strip().split("\n")
            for line in lines:
                data = line.split()
                if len(data) == 0:
                    continue
                snapUsage[data[0].split("/")[-1]] = data[1]
        return snapUsage

    @classmethod
    def zfsDiskFreeSpace(cls, sizeInGb='50G', percentage=0):
        status, msg = ZFSApi.zfsStatus()
        if not status:
            return False
        poolStats = ZFSApi.poolUsagePercentage()
        if poolStats['Total'] != "--" and poolStats['Usable'] != "--" and poolStats['Avail'] != "--":
            freeSpace = getBytes(poolStats['Avail'])
            if freeSpace - getBytes(sizeInGb) <= 0:
                return False
            try:
                freePoolPercentage = 100 - int(poolStats['Percentage'])
            except Exception, e:
                freePoolPercentage = int((freeSpace * 100) / getBytes(poolStats['Usable']))
            if freePoolPercentage > percentage:
                return True
            else:
                if freeSpace - getBytes(sizeInGb) > 0:
                    return True
                return False
        else:
            return False

    @classmethod
    def zfsSyncSysUsage(cls, syncSystemId):
        diskStats = dict(used='0K', refer='0K')
        syncSystem = getSyncSystemData(syncSystemId)
        userName = syncSystem['userName']
        if (syncSystem['osType'] == 1) and not ('isZvol' in syncSystem and syncSystem['isZvol'] is False):
            path = "fission/%s/Linux/" % userName
            volumes = syncSystem['volumes']
            for vol in volumes:
                lpath = path+createVolumeLabel(syncSystemId, vol['uuid'])
                (status,out,err) = execution("zfs list -o used,refer -rH %s" % lpath)
                if not status:
                    refer = out.strip().split()[1]
                    used = out.strip().split()[0]
                    diskStats['used'] = convertToHumanReadable(getBytes(used) + getBytes(diskStats['used']))
                    diskStats['refer'] = convertToHumanReadable(getBytes(refer) + getBytes(diskStats['refer']))
                else:
                    print "ERROR: %s" % err
        else:
            path = "fission/%s/%s" % (userName, syncSystemId)
            (status,out,err) = execution("zfs list -o used,refer -rH %s" % path)
            if not status:
                lines = out.strip().split()
                if len(lines) > 0:
                    diskStats['used'] = lines[0]
                    diskStats['refer'] = lines[1]
        diskStats['used'] = getBytes(diskStats['used'])
        diskStats['refer'] = getBytes(diskStats['refer'])
        return diskStats

    @classmethod
    def zfsDiskUsage(cls, syncSystems):
        ret = []

        user = session["sessionData"]["userName"]
        if user == "admin":
            # total system/user space
            (status,out,err) = execution("zpool list fission | grep -v NAME | tr -s ' ' ' ' | cut -d' ' -f2,3,4")
            if not status:
                lines = out.split("\n")
                if len(lines) > 0:
                    line = lines[0]
                    values = line.split(" ")
                    if len(values):
                        #NAME   SIZE  ALLOC   FREE    CAP  DEDUP  HEALTH  ALTROOT
                        #fission  1.80T  33.2G  1.76T     1%  1.00x  ONLINE  -
                        data = {"PS":"FissionStoragePool","User":user,"Used":values[1],"Avail":values[2],"Total":values[0]}
                        # For Available space taking from zfs list fission as zpool list fission has different value
                        (status, out, err) = execution("zfs list fission | grep -v NAME | tr -s ' ' ' ' | cut -d' ' -f2,3,4")
                        if not status:
                            lines = out.split("\n")
                            if len(lines) > 0:
                                line = lines[0]
                                try:
                                    values = line.split(" ")
                                    data['Avail'] = values[1]
                                    data['Used'] = values[0]
                                    data['Usable'] = convertToHumanReadable(getBytes(data['Used']) + getBytes(data['Avail']))
                                except Exception, e:
                                    logger.info("Error getting used space: %s" % e)
                        ret.append(data)

            # vault bay space
            if fission_conf.server_config == "vault":
                (status,out,err) = execution("zfs list fission/fissionbay | grep -v NAME | tr -s ' ' ' ' | cut -d ' ' -f2,3")
                if not status:
                    lines = out.split("\n")
                    if len(lines) > 0:
                        line = lines[0]
                        values = line.split(" ")
                        if len(values):
                            ret.append({"PS":"VaultBayStoragePool","User":user,"Used":values[0],"Avail":values[1]})
            # img exports space
            '''(status,out,err) = execution("zfs list fission/fissionimgexports | grep -v NAME | tr -s ' ' ' ' | cut -d ' ' -f2,3")
            if user == "admin" and (not status):
                lines = out.split("\n")
                if len(lines) > 0:
                    line = lines[0]
                    values = line.split(" ")
                    if len(values):
                        ret.append({"PS":"ImgExportStoragePool","User":user,"Used":values[0],"Avail":values[1]})'''

        for system in syncSystems:
            path = "fission/"
            user = session["sessionData"]["userName"]
            if "userName" in system:
                user = system["userName"]
            path += user + "/" + system["_id"]
            if not os.path.exists('/'+path): #In vault the PS is available in mongo but the snapshots are not imported yet, then it can fail
                continue
            (status,out,err) = execution("zfs list %s | grep -v NAME | tr -s ' ' ' ' | cut -d ' ' -f2,3" % (path))
            if status:
                continue
            lines = out.split("\n")
            if len(lines) > 0:
                line = lines[0]
                values = line.split(" ")
                if len(values):
                    ret.append({"PS": system["systemName"], "type": system["type"], "User": user, "Used": values[0], "Avail": values[1], "psId": system['uuid']})
        return ret

    @classmethod
    def poolUsagePercentage(cls):
        poolStats = dict(PS="FissionStoragePool", Total="--", Usable="--", Avail="--", Used="--", Percentage="")
        status, msg = ZFSApi.zfsStatus()
        if not status:
            return poolStats
        (status, out, err) = execution("zpool list fission | grep -v NAME | tr -s ' ' ' ' | cut -d' ' -f2,3,4")
        if status:
            return poolStats
        data = out.strip().split()
        if len(data) == 3:
            poolStats['Total'] = data[0]
        (status, out, err) = execution("zfs list fission | grep -v NAME | tr -s ' ' ' ' | cut -d' ' -f2,3,4")
        if status:
            return poolStats
        data = out.strip().split()
        if len(data) == 3:
            poolStats['Avail'] = data[1]
            poolStats['Used'] = data[0]
            try:
                poolStats['Usable'] = convertToHumanReadable(getBytes(poolStats['Used']) + getBytes(poolStats['Avail']))
            except Exception, e:
                poolStats['Usable'] = "--"

        if poolStats["Used"] != "--" and poolStats["Usable"] != "--":
            percentage = int(round((getBytes(poolStats["Used"]) * 100) / getBytes(poolStats["Usable"])))
            poolStats['Percentage'] = percentage
        return poolStats

    @classmethod
    def zfsGetUserQuota(cls, userName):
        (status, out, err) = execution("zfs list -H fission/%s" % userName)
        if status == 0:
            available = out.strip()
            available = available.split('\t')[2]
            return int(math.ceil(getBytes(available)))
        return 0

    @classmethod
    def zfsDiskQuota(cls, users):
        ret = {}
        if "admin" == session["sessionData"]["userName"]:
            if fission_conf.server_config == "vault":
                for user in users:
                    (status, out,err) = execution("zfs get -H -o value quota fission/%s" % (user["userName"]))
                    ret[user["userName"]] = out.strip()
                (status, out,err) = execution("zfs get -H -o value quota fission/fissionbay")
                ret["fissionbay"] = out.strip()
            (status, out,err) = execution("zfs get -H -o value quota fission/fissionimgexports")
            ret["fissionimgexports"] = out.strip()
        return ret

    @classmethod
    def zfsTransmissionStatistics(cls, p1):
        total = ""
        totalTransmitted = 0
        currentTransmitted = 0
        snapshotSizes = {}
        transmittingSnapshot = ""

        while p1.poll() is None:
            line = p1.stderr.readline()
            line = line.strip()
            if len(total) == 0 and line.startswith("send from"):
                data = line.split()
                snapshotSizes[data[4]] = getBytes(data[8])
                continue
            elif len(total) == 0 and line.startswith("total estimated size is"):
                total = line.split("total estimated size is ")[1]
                continue
            elif len(total):
                data = line.split()
                if len(data) == 3:
                    if data[0] != "TIME":
                        transmittingSnapshot = data[2]
                        currentTransmitted = getBytes(data[1])
                    elif len(transmittingSnapshot):
                        currentTransmitted = 0
                        totalTransmitted = totalTransmitted + snapshotSizes[transmittingSnapshot]
            yield convertToHumanReadable(totalTransmitted+currentTransmitted) + ' of ' + total

    @classmethod
    def zfsGetSnapSize(cls, snapShot, userName):
        try:
            snapPath = "fission/%s/%s" % (userName, snapShot)
            (status, out, err) = execution("zfs list -t snapshot -o used -rH %s" % snapPath)
            if not status:
                return convertSize(getBytes(out.strip()))
            else:
                return "NA"
        except Exception, e:
            return 'NA'

    #mode first - Size of First Snap Shot
    #     inc - Size of Incrementals
    #     all - Total Size
    #returns the size in bytes
    @classmethod
    def zfsSnapShotSize(cls, syncSystemId, mode, userName="admin"):
        snapShots = ZFSApi.getZfsSnapshots(syncSystemId, True)
        firstSnap = "fission/" + userName + "/" + snapShots[0]
        lastSnap = "fission/" + userName + "/" + snapShots[-1]
        first = 0
        allInc = 0

        # Calculating Size of First Snap Shot
        (status, out, err) = execution("zfs send -nvD %s" % firstSnap)
        if "total estimated size is" in err:
            first = getBytes(err.split("total estimated size is ")[1].rstrip())

        # Calculating Size of All Incremental Snap Shots
        if len(snapShots) > 1:
            (status, out, err) = execution("zfs send -nvI %s %s" % (firstSnap, lastSnap))
            if "total estimated size is" in err:
                allInc = getBytes(err.split("total estimated size is ")[1].rstrip())

        if mode == "first":
            return first
        elif mode == "inc":
            return allInc
        elif mode == "all":
            return first + allInc

    @classmethod
    def convertionSnapSize(cls, snapShot):
        (status, msg) = ZFSApi.cloneSnapshot(snapShot, 0)
        cloneSnap = snapShot.replace('@', '_')+"_clone"
        if not status:
            logger.info("Error Creating Clone")
            return -1

        total = 0
        path = "/"+ZFSApi.getZfsPath()+cloneSnap
        files = os.listdir(path)
        for file in files:
            if not file.endswith(".meta"):
                total += os.path.getsize(path + '/' + file)

        ZFSApi.destroyClone(snapShot)
        return total

    @classmethod
    def zfsFreeSpace(cls, name):
        #img exports space
        (status,out,err) = execution("zfs list fission/%s | grep -v NAME | tr -s ' ' ' ' | cut -d ' ' -f2,3" % name)
        if not status:
            lines = out.split("\n")
            if len(lines) > 0:
                line = lines[0]
                values = line.split(" ")
                if len(values):
                    return getBytes(values[1])
        return -1

    @classmethod
    def lastSnapshot(cls, syncSystemId, userName, newMode=False):
        bayFirstSnap = ''
        bayLastSnap = ''
        zfsLastSnap = ''
        filesWaiting = 0

        # Finding last snapshot in fissionbay
        endsWith = ['_full.bin', '_full.bin.gz', '_inc.bin', '_inc.bin.gz']
        if os.path.exists('/fission/fissionbay/%s/%s' % (userName, syncSystemId)):
            files = sorted(os.listdir('/fission/fissionbay/%s/%s' % (userName, syncSystemId)))
            snapFiles = [x for x in files if x.endswith(".bin") or x.endswith(".bin.gz")]
            filesWaiting = len(snapFiles)
            if len(snapFiles):
                firstFile = snapFiles[0]
                lastFile = snapFiles[-1]
                for endwith in endsWith:
                    if lastFile.endswith(endwith):
                        bayLastSnap = lastFile.replace(endwith, '')
                    if firstFile.endswith(endwith):
                        bayFirstSnap = firstFile.replace(endwith, '')

        # Finding last snapshot in ZFS(based on mongo entries)
        zfsLastSnap = ZFSApi.getZfsSnapshots(syncSystemId)
        if not zfsLastSnap:
            zfsLastSnap = ''

        if newMode is False:
            if len(bayLastSnap):
                return bayLastSnap
            elif len(zfsLastSnap):
                return zfsLastSnap
        elif newMode is True:
            ret = {"psidInZFS": False, "zfsLastSnap": zfsLastSnap, "bayFirstSnap": bayFirstSnap,
                   "bayLastSnap": bayLastSnap, "filesWaiting": filesWaiting}
            # Checking whether PSID is already in the ZFS
            (status, out, err) = execution("zfs list fission/%s/%s" % (userName, syncSystemId))
            if status == 0:
                # Indicates that PSID is already there in ZFS
                ret['psidInZFS'] = True
            return json.dumps(ret)
        return ''

    @classmethod
    def zfsStatus(cls):
        cmd = "zpool status fission | grep state"
        (state, out, err) = execution(cmd)
        if not state:
            out = out.split(":")[1].strip()
            if "ONLINE" in out:
                # Checking for zfs list, it will be empty if iscsi disk is down though zpool status shows online
                (state, out, err) = execution("zfs list")
                if len(out.strip()) == 0:
                    logger.info("Storage Pool Unavailable, zfs list is empty")
                    return False, "Unavailable"
            elif "UNAVAIL" in out:
                logger.info("Storage Pool Unavailable")
                return False, "Unavailable"
            elif "DEGRADED" in out:
                logger.info("Storage Pool Degraded")
                return False, "Degraded"
            elif "FAULTED" in out:
                logger.info("Storage Pool Faulted")
                return False, "Faulted"
        else:
            logger.info("zpool status command execution failed")
            return False, "Unavailable"

        return True, "Available"

    @classmethod
    def fuseAddMbr(cls, basePath, fusePath, syncSystem):
        if not os.path.exists(fusePath):
            execution("mkdir %s" % fusePath)
        else:
            (status, out, err) = execution("mount | grep %s" % fusePath)
            if not status:
                (status, out, err) = execution("/bin/fusermount -u %s" % fusePath)
                if status:
                    logger.info("addMbr files are already present in the path:%s" % fusePath)
                    return False
            if len(os.listdir(fusePath)) == 0:
                execution("/bin/fusermount -u %s" % fusePath)
            else:
                logger.info("addMbr files are already present in the path:%s" % fusePath)
                return False

        volumes = ZFSApi.listFiles(basePath)
        vols_list = []
        isXp = False
        if "windows xp" in syncSystem['osName'].lower():
            isXp = True

        for vol in volumes:
            mFile = basePath+vol+".meta"
            if os.path.exists(mFile):
                metaFile = open(mFile, "r")
                data = json.load(metaFile)
                metaFile.close()
                if os.path.getsize(basePath+vol) < 1048576:
                    continue

                if not data['isBootable'] and not isXp:
                    vols_list.append(vol+",0,")

        cmd = "/usr/share/nginx/www/framework/backend/add_mbr %s %s %s" % (" ".join(vols_list), basePath, fusePath)
        (status, out, err) = execution(cmd)
        if not status:
            return True
        else:
            logger.info("add_mbr failed status:%s out:%s err:%s" % (status, out, err))
            return False

    @classmethod
    def fuseDestroy(cls, fusePath):
        if os.path.exists(fusePath):
            cmd = "/bin/fusermount -u %s" % fusePath
            (status, out, err) = execution(cmd)
            if not status:
                if "_fuse" in fusePath:
                    (status, out, err) = execution("rm -rf %s" % fusePath)
                    logger.info("fuse dir cleanup status:%s out:%s err:%s" % (status, out, err))
                return True
            else:
                return False
        return True

    @classmethod
    def userConsumePercent(cls, userName):
        (status, out, err) = execution("zfs get -H -o value quota fission/%s" % userName)
        quota = out.strip()
        if not quota.lower() == "none":
            (status, out, err) = execution("zfs list -H -o used fission/%s" % userName)
            used = getBytes(out.strip())
            total = getBytes(quota)
            usedAvg = int(math.ceil((used / total)*100))
            return usedAvg
        return 0

