import datetime
import sys
from collections import OrderedDict

# import cx_Oracle
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient
import os
import json
import datetime
import time
import numpy


class ReconMigration():
    def __init__(self, reconId):
        try:
            # change per sql database
            # self.ip = '172.19.36.226'
            # self.port = 1653
            # self.SID = 'reconuat1'
            self.mongoClient = self.getMongoConnection()
            # self.updateBusinessContextDetails()
            self.reconId = reconId
            self.getReconDef()
            self.updateReconDynamicSetup()
            self.updateMongoDB()

            # self.writeToFile()
            # self.commitToSql()
        except:
            raise
            self.rollBack()

    def getMongoConnection(self):
        mongo_client = MongoClient('mongodb://' + 'localhost' + ':27017')
        return mongo_client

    def getReconDef(self):
        self.reconDef = self.mongoClient['reconbuilder']['recon_context_details'].find_one({"reconId": self.reconId})


    def updateMongoDB(self):
        self.mongoClient['erecon']['business'].remove({"RECON_ID": self.reconId})
        businessDef = self.mongoClient['erecon']['business'].find_one({"RECON_ID": self.reconId})

        if businessDef is None:
            business = dict()
            business['BUSINESS_CONTEXT_ID'] = self.reconDef['businessContextId']
            business['BUSINESS_PROCESS_NAME'] = self.reconDef['businessContext']['BUSINESS_PROCESS_NAME']
            business['WORKPOOL_NAME'] = self.reconDef['businessContext']['WORKPOOL_NAME']
            business['partnerId'] = "54619c820b1c8b1ff0166dfc"
            business['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
            business['RECON_ID'] = self.reconDef['reconId']
            business['RECON_NAME'] = self.reconDef['reconName']
            business['TXN_PROCESSING_LEVEL'] = self.reconDef['processingLevel']
            business['TXN_PROCESSING_TYPE'] = self.reconDef['processingType']

            self.mongoClient['erecon']['business'].insert(business)
            print "### Recon Details Migrate to Mongo DB ###"
        else:
            print "### Recon Details Already exists Mongo DB ###"

    def appendAuditRecords(self, df):
        df['CREATED_BY'] = 'AlgoReconIT'
        df['CREATED_DATE'] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        df['CREATED_DATE'] = pd.to_datetime(df['CREATED_DATE'], format='%Y-%m-%d %H:%M:%S')
        df['IPADDRESS'] = '0.0.0.0'
        df['RECORD_STATUS'] = 'ACTIVE'
        df['RECORD_VERSION'] = 1
        return df

    def convertSequenceToDict(self, list1):

        dict1 = OrderedDict()
        argList = range(1, len(list1) + 1)
        for k, v in zip(argList, list1):
            dict1[str(k)] = v
        for x in dict1.keys():
            if str(dict1[x]) == 'nan':
                dict1[x] = ''
        return dict1

    def updateReconDynamicSetup(self):
        qry = "select count(*) from recon_dynamic_data_model where recon_id ='{}'".format(self.reconDef['reconId'])
        if 0 > 0:
            print "### Recon Dynamic data setup already exists, Hence Ignoring ###"
        else:

            sourceDetails = self.reconDef['sources']
            fieldDetails = []
            duplicateTracker = []
            dataTypeMapper = {"str": "TEXT", "np.int64": "NUMBER", "np.float64": "DOUBLE", "np.datetime64": "DATE"}

            qry_1 = "select max(RECON_DYNAMIC_DATA_MODEL_OID)+1 from recon_dynamic_data_model"
            qry_2 = "select max(RECON_DYNAMIC_DATA_MODEL_ID)+1 from recon_dynamic_data_model"
            # print self.connection.cursor().execute(qry_1)
            oid = 0
            id = 1
            for val in sourceDetails:
                # print val
                feedDef = {}
                feedDef = self.mongoClient['reconbuilder']['feedDefination'].find_one({"_id": ObjectId(val['feedId'])},
                                                                                      {"fieldDetails": 1})

                for value in feedDef['fieldDetails']:
                    # print value ,duplicateTracker
                    pos = [i for i, x in enumerate(duplicateTracker) if x == value['mdlFieldName']]
                    if len(pos) == 0:
                        fieldDetails.append(value)
                        duplicateTracker.append(value['mdlFieldName'])

            df = pd.DataFrame(fieldDetails)
            df = df[['mdlFieldName', 'displayName', 'dataType', 'position']]

            df.rename(columns={"mdlFieldName": "MDL_FIELD_ID",
                               "displayName": "UI_DISPLAY_NAME",
                               "dataType": "MDL_FIELD_DATA_TYPE"}, inplace=True)

            df['MDL_FIELD_DATA_TYPE'] = df['MDL_FIELD_DATA_TYPE'].apply(lambda x: dataTypeMapper[x])
            df['ORDER_SEQ_NO'] = df.index + 1
            print df.head()

            df['MANDATORY'] = 'M'
            df['RECON_ID'] = self.reconDef['reconId']
            df['BUSINESS_CONTEXT_ID'] = self.reconDef['businessContextId']
            df['COL_STATUS'] = 'ACTIVE'
            df['RECON_DYNAMIC_DATA_MODEL_OID'] = df.index + oid
            df['RECON_DYNAMIC_DATA_MODEL_ID'] = df.index + id
            df = self.appendAuditRecords(df)
            df['partnerId'] = "54619c820b1c8b1ff0166dfc"
            df['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"

            df.to_csv('/usr/share/nginx/v8/scripts/' + reconId +'_' + str(datetime.datetime.now().strftime('%s')) + '.csv',index=False)
            self.mongoClient['erecon']['dynamic_data_setup'].remove({'RECON_ID': self.reconDef['reconId']})
            for doc in df.T.to_dict().values():
                self.mongoClient['erecon']['dynamic_data_setup'].save(doc)

            print "### Recon Dynamic data setup updated ###"

    def commitToSql(self):
        self.connection.commit()

    def rollBack(self):
        self.connection.rollback()


if __name__ == '__main__':
    reconId = sys.argv[1]
    print reconId
    reconJob = ReconMigration(reconId)
