function gstMappingController($scope, $rootScope, $http, $upload, fileUploadService, GSTSourceProductGroupService, $modal, GstStateCodeMapService, GstFeedDefService, $window, MasterDumpService) {

    // initialize constants
    $scope.submitButton=true
    $http.get('app/json/gst_constants.json').success(function (data) {
        $scope.constants = data
    })

    $scope.stateCodes = []
    $scope.reportNames = [];

    $scope.loadStateCodes = function (stateRef) {
        if (angular.isDefined(stateRef) && stateRef != '') {
            var perColConditions = {};
            perColConditions["refIdentifier"] = stateRef

            GstStateCodeMapService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                if (data['total'] > 0) {
                    $scope.stateCodes = data['data']
                } else {
                    $rootScope.showAlertMsg('No data found.')
                }
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })
        }
    }

    $scope.initStateCode = function () {
        GstStateCodeMapService.getDistinctStateCode('dummy', function (data) {
            $scope.stateCode = data
        })
    }

    $scope.Uploadfile = function () {
        $modal.open({
            templateUrl: 'uploadfile.html',
            windowClass: 'modal addGstFiles',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService, fileUploadService) {
                $scope.upload = function (files) {
                    console.log(files[0]);
                    $scope.fileName = files[0].name
                    if (files[0].size > 20971520) {
                        $scope.msgNotify('warning', 'Uploaded File was more than 20 MB.Please choose less than 20MB file');
                        files = [];
                    }
                    else {
                        $scope.saveFile = function (label) {
                            $scope.name = files[0].name.split('.')
                            $scope.array = [label, $scope.name[1].toLowerCase()]
                            $scope.id = $scope.array.join('.')

                            console.log($scope.id)

                            $upload.upload({
                                url: '/api/utilities/' + label['fileName'] + '.' + $scope.name[1].toLowerCase() + '/gst_state_code_upload?fileName=' + label['fileName'],
                                method: 'POST',
                                file: files[0],
                                data: {'fileName': "test", 'description': "TESTING"}
                            }).progress(function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

                            }).success(function (data, status, headers, config) {
                                if (data.status == "error") {
                                    $rootScope.showAlertMsg(data.msg)
                                }
                                else {
                                    $scope.loadStateCodes(label['fileName'])
                                    $rootScope.showSuccessMsg('File Uploaded Succesfully')
                                    $modalInstance.dismiss('cancel');
                                }

                            })
                        };

                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }
    $scope.Masterdump=function(){
        MasterDumpService.get(undefined,undefined,function (data) {
            console.log(data)
            details=data['data']
            angular.forEach(details,function (key,value) {
                
                if ($scope.reportNames.indexOf(key.reportName)==-1)
                    $scope.reportNames.push(key.reportName)
                    console.log($scope.reportNames)
            })

        })
    }

    $scope.deleteMastedData=function(reportName){
        $scope.reportName=reportName
        MasterDumpService.delete('dummy',{'reportName':$scope.reportName},function (data) {

            $scope.reportNames.splice($scope.reportNames.indexOf(reportName),1)
            $rootScope.showSuccessMsg(data)

        })

    }



    $scope.Uploaddumpfile = function (reportName) {
        $scope.reportName=reportName
        $modal.open({
            templateUrl: 'uploadmasterfile.html',
            windowClass: 'modal addGstFiles',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService, fileUploadService) {
                $scope.saveFile = function (file) {
                    var file = file;
                    $scope.submitButton=false
                    $scope.name = file.name
                    var uploadUrl = "/api/no_auth/dummy/upload"; //Url of webservice/api/server
                    $scope.data={'file':file,'reportName':reportName}
                    fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                        $scope.post = data.msg;
                        // $rootScope.openModalPopupClose()

                        MasterDumpService.inserttoMongo('dummy',{'fileName':$scope.name,'reportName':reportName},function (data) {
                           $rootScope.showSuccessMsg(data)
                            $scope.Masterdump()
                           console.log(data)
                            }, function (err) {
                                $rootScope.showAlertMsg(err.msg)
                                console.log(err.msg)

                            })
                    })

                        console.log($scope.post)
                     $modalInstance.dismiss('cancel');

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    $scope.downloadStateCodes = function (stateRef) {
        GstStateCodeMapService.exportStateCodes('dummy', {'stateRef': stateRef}, function (data) {
            var filePath = window.location.origin + '/app/files/recon_reports/'
            $window.open(filePath + data['exportFile'])
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.initStateCode()

// ------------- Feed defination starts here -------------
    $scope.feedDefination = {}
    $scope.addFields = function () {
        if (!angular.isDefined($scope.feedDefination.fieldDetails)) {
            $scope.feedDefination.fieldDetails = []
            $scope.feedDefination.fieldDetails.push({
                'enableEdit': true,
                'position': 1
            })
        } else {
            var position = $scope.feedDefination.fieldDetails.length + 1
            $scope.feedDefination.fieldDetails.push({
                'enableEdit': true,
                'position': position
            })
        }
    }

    $scope.initFeedMapping = function () {
        $scope.gstSources = []
        $scope.feedsList = []
        GSTSourceProductGroupService.get(undefined, undefined, function (data) {
            console.log(data)
            $scope.gstSources = data['data']

            // load GST State Ref Files
            GstStateCodeMapService.get(undefined, undefined, function (data) {
                $scope.stateRef = data['data']
            })
        })
    }

    $scope.onChangeFeedDetails = function (source) {
        GstFeedDefService.findFeedDetails('dummy', {'sourceCode': source['sourceCode']}, function (data) {
            $scope.clearFields()
            if (data['total'] > 0) {
                $scope.feedsList = data['data']
            } else {
                $rootScope.showAlertMsg("Feed-Structure not defined for source " + source['sourceName'])
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.deleteFields = function (index, value) {
        console.log($scope.feedDefination.fieldDetails)
        $scope.feedDefination.fieldDetails.splice(index, 1)

        // update the position of the fields
        angular.forEach($scope.feedDefination.fieldDetails, function (val, index) {
            val['position'] = index + 1
        })
    }

    $scope.saveFeedStructure = function (feedDefination) {
        $rootScope.openModalPopupOpen()
	
	var formula_pattern = /^=|^\+|^-|^@/
	angular.forEach(feedDefination.fieldDetails, function (value, index) {
	  if(formula_pattern.test(value['fieldName'])){
        	value['fieldName'] = "'" + value['fieldName']
	  }
	  if (formula_pattern.test(value['datePattern'])){
        	value['datePattern'] = "'" + value['datePattern']
          }	
       })

        if (angular.isDefined(feedDefination['_id'])) {
            GstFeedDefService.put(feedDefination['_id'], feedDefination, function (data) {
                $rootScope.showSuccessMsg("Feed-Definition updated Successfully")
                $rootScope.openModalPopupClose()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                console.log(err.msg)
            })
        }
        else {
            feedDefination['sourceCode'] = $scope.source['sourceCode']
            GstFeedDefService.post(feedDefination, function (data) {
                // load all the feed details
                $scope.onChangeFeedDetails($scope.source)
                $rootScope.showSuccessMsg("Feed-Definition saved Successfully")
                $rootScope.openModalPopupClose()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                $rootScope.openModalPopupClose()
                console.log(err.msg)
            })
        }
    }

    $scope.uploadFieldDetails = function () {
        $modal.open({
            templateUrl: 'uploadFieldDetails.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, $rootScope) {

                $scope.upload = function (files) {
                    console.log(files[0]);
                    $scope.fileName = files[0].name
                    if (files[0].size > 20971520) {
                        $scope.msgNotify('warning', 'Uploaded File was more than 20 MB.Please choose less than 20MB file');
                        files = [];
                    }
                    else {
                        $scope.saveFile = function (label) {
                            $scope.name = files[0].name.split('.')
                            $scope.array = [label, $scope.name[1].toLowerCase()]
                            $scope.id = $scope.array.join('.')

                            $upload.upload({
                                url: '/api/utilities/' + $scope.id + '/upload_gst_field_details',
                                method: 'POST',
                                file: files[0]
                            }).progress(function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            }).success(function (data, status, headers, config) {
                                if (data.status == "error") {
                                    $rootScope.showAlertMsg(data.msg)
                                } else {
                                    $rootScope.showSuccessMsg("Field Details uploaded sucessfully.")
                                    $scope.feedDefination['fieldDetails'] = data.msg
                                }
                                $rootScope.openModalPopupClose()
                                $modalInstance.dismiss('cancel');
                            })
                        };
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    $scope.downloadFieldDetails = function () {
        GstFeedDefService.exportFieldDetails('dummy', {'feedRef': $scope.feedDefination.feedName}, function (data) {
            var filePath = window.location.origin + '/app/files/recon_reports/'
            $window.open(filePath + data['exportFile'])
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.deleteFeedDef = function (data) {
        $modal.open({
            templateUrl: 'deletegstmapping.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    if (angualar.isDefined($scope.feedDefination['_id']) || $scope.feedDefination['_id'] == ''
                        || $scope.feedDefination['_id'] == null) {
                        $scope.cancel();
                        $scope.showErrormsg('Cannot delete un-save data.');
                    } else {
                        $rootScope.openModalPopupOpen();
                        GstFeedDefService.deleteData($scope.feedDefination["_id"], function (data) {
                            $rootScope.openModalPopupClose();
                            $scope.cancel();
                        }, function (errorData) {
                            $scope.cancel();
                            $rootScope.openModalPopupClose();
                            $scope.showErrormsg(errorData.msg);
                        });
                    }

                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.gstmappingOptions = {};
    $scope.gstmappingOptions.init = false;

    $scope.getgstmappingDetails = function () {
        $scope.gstmappingOptions.searchFields = {}
        $scope.gstmappingOptions.service = GSTSourceProductGroupService;
        $scope.gstmappingOptions.headers = [
            [
                {
                    name: "Source Name",
                    searchable: true,
                    sortable: true,
                    dataField: "sourceName",
                    colIndex: 1
                },
                {
                    name: "Source Code",
                    searchable: true,
                    sortable: true,
                    dataField: "sourceCode",
                    colIndex: 2
                },
                {
                    name: "Product Group",
                    searchable: true,
                    sortable: true,
                    dataField: "productGroup",
                    colIndex: 3
                },
                {
                    name: "Business Process",
                    searchable: true,
                    sortable: true,
                    dataField: "businessProcess",
                    colIndex: 4
                },
                {
                    name: "Matching Source",
                    searchable: true,
                    sortable: true,
                    dataField: "matchingSource",
                    colIndex: 5
                },
                {
                    name: "State Reference",
                    searchable: true,
                    sortable: true,
                    dataField: "stateCodeRef",
                    colIndex: 6
                },
                {
                    name: "Actions",
                    colIndex: 7,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addgstmapping",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg"
                        }]
                }
            ]
        ];
    }

    $scope.getgstmappingDetails()

    //-----------Source Product Group-------------------
    $scope.gstmappingOptions.addgstmapping = function (data) {
        $modal.open({
            templateUrl: 'addgstmapping.html',
            windowClass: 'modal addUsergst',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.mode = "Add";
                if (angular.isDefined(data)) {
                    $scope.mode = "Edit";
                    $scope.srcPorGrp = data;
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.gstmappingOptions.reQuery = true;
                };
                $scope.savegstmapping = function (val) {
                    if (angular.isDefined(val['_id'])) {
                        GSTSourceProductGroupService.put(val['_id'], val, function (data) {

                            $scope.gstmappingOptions.reQuery = true;
                            $scope.cancel()
                            $rootScope.showSuccessMsg("GST Mapping  Details Updated Successfully.")
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    } else {
                        GSTSourceProductGroupService.post(val, function (data) {

                            $scope.gstmappingOptions.reQuery = true;
                            $scope.cancel()
                            $rootScope.showSuccessMsg("GST Mapping Details Saved Successfully.")
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }
                }

            }


        });
    };


    $scope.confirmDelete = angular.noop();

    $scope.gstmappingOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deletegstmapping.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    GSTSourceProductGroupService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.gstmappingOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.clearFields = function () {
        $scope.feedsList = []
        $scope.feedDefination = {}
    }

    $scope.downloadDump = function (reportName) {
        $scope.reportName = reportName;
        MasterDumpService.masterDownload('dummy', {'reportName': $scope.reportName}, function (data) {
            var fil = window.location.origin
            var filePath = window.location.origin + '/app/files/'
            var data = data
            filePath = filePath + data
            $window.open(filePath);

        })
    }

    $scope.deleteFeedDefination = function () {
        $modal.open({
            templateUrl: 'deletegstmapping.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {

                    if (!angular.isDefined($scope.feedDefination['_id'])) {
                        $rootScope.showAlertMsg("Cannot delete unsaved changes.")
                    } else {
                        $rootScope.openModalPopupOpen();
                        GstFeedDefService.deleteData($scope.feedDefination["_id"], function (data) {
                            $scope.onChangeFeedDetails($scope.source)
                            $rootScope.openModalPopupClose();
                            $rootScope.showSuccessMsg("Feed-Definition Deleted Successfully")
                        }, function (errorData) {
                            $scope.cancel();
                            $rootScope.openModalPopupClose();
                            $scope.showErrormsg(errorData.msg);
                        });
                    }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

}
