/**
 * Created by swami on 13/4/15.
 */

function usersController($scope, $rootScope, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, UsersService, AuditsService, angularCryptoSerivces) {
//    if($rootScope.credentials.role != 'admin'){
//        $rootScope.msgNotify('error','Access restricted to view users.');
//        $location.path("/migratedSystems");
//        return false;
//    }
//    console.log($rootScope.businessData)
//    angular.forEach($rootScope.businessData,function(v,k){
//        AccountPoolService.post(v,function(data){
//            console.log(data)
//        })
//    })
    $scope.usersOptions = {};
    $scope.usersOptions.init = false;
    $scope.usersOptions.isLoading = true;
    $scope.usersOptions.isTableSorting = "userName";
    $scope.usersOptions.sortField = "userName";
//      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.usersOptions.searchFields = {}
        $scope.usersOptions.service = UsersService;
        $scope.usersOptions.headers = [
            [
                {name: "Name", searchable: true, sortable: true, dataField: "userName", colIndex: 0},
                {name: "Role", searchable: true, sortable: true, dataField: "role", filter: "toUser", colIndex: 1},
                {name: "Email", searchable: true, sortable: true, dataField: "email", colIndex: 2},
//                {name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/

            ]
        ];

        if ($rootScope.credentials.role != 'Approver') {
            $scope.usersOptions.headers[0].push({
                name: "Actions",
                colIndex: 4,
                width: '10%',
                actions: [{
                    toolTip: "Edit",
                    action: "addUser",
                    posticon: "fa fa-pencil fa-lg",
                    "disabledFn": "disableEdit"
                },
                    {
                        toolTip: "Delete",
                        action: "deleteFunc",
                        posticon: "fa fa-trash-o fa-lg",
                        "showHideFn": "checkRole",
                        "disabledFn": "hideButton"
                    }, {
                        toolTip: "Unlock",
                        action: "Unlock",
                        posticon: "fa fa-unlock-alt fa-lg",
                        "showHideFn": "checkRole",
                        "disabledFn": "hideButton"
                    }
                ]
            })
        }
        if ($rootScope.credentials.role == 'Approver') {
            $scope.usersOptions.headers[0].push({
                name: "Approval Action",
                searchable: true,
                sortable: true,
                dataField: "approvalAction",
                colIndex: 2
            })
            $scope.usersOptions.headers[0].push({
                    name: "Actions",
                    colIndex: 4,
                    width: '10%',
                    actions: [
                        {
                            toolTip: "Approve",
                            action: "approveUser",
                            posticon: "fa fa-check fa-lg",
                            "disabledFn": "disableEdit"
                        },
                        {
                            toolTip: "Reject",
                            action: "rejectUser",
                            posticon: "fa fa-times fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }
                    ]
                }
            )
        }


        $scope.usersOptions.approveUser = function (userData) {
            $modal.open({
                templateUrl: 'approveUser.html',
                windowClass: 'modal addUser',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance) {
                    $scope.selUserData = userData
                    $scope.confirmApproval = function () {
                        console.log($scope.selUserData)
                        if (angular.isDefined($scope.selUserData) && angular.isDefined($scope.selUserData.approvalAction)) {
                            if ($scope.selUserData.approvalAction == 'create') {
                                $scope.selUserData.isApproved = true
                                $rootScope.openModalPopupOpen();
                                $scope.eventDoc = {
                                    'type': 'User Creation Approved',
                                    'actionOn': $scope.selUserData.userName,
                                    'createDateTime': Date.now(),
                                    'createdBy': $rootScope.credentials.userName
                                }
                                AuditsService.post($scope.eventDoc)
                                UsersService.put($scope.selUserData["_id"], $scope.selUserData, function (data) {
                                    $rootScope.msgNotify('success', 'Successfully updated');
                                    $rootScope.openModalPopupClose();
                                    $scope.eventDoc = {
                                        'type': 'User Created Successfully',
                                        'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                        'createdBy': $rootScope.credentials.userName
                                    }
                                    AuditsService.post($scope.eventDoc)
                                    $scope.cancel();
                                });

                            }
                            else if ($scope.selUserData.approvalAction == 'delete') {
                                $scope.eventDoc = {
                                    'type': 'User Deletion Approved',
                                    'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                    'createdBy': $rootScope.credentials.userName,
                                }
                                AuditsService.post($scope.eventDoc)
                                UsersService.deleteData($scope.selUserData["_id"], function (data) {
                                    $scope.eventDoc = {
                                        'type': 'User Deleted Sucessfully',
                                        'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                        'createdBy': $rootScope.credentials.userName,
                                    }
                                    AuditsService.post($scope.eventDoc)
                                    $rootScope.openModalPopupClose();
                                    $scope.usersOptions.reQuery = true;
                                    $modalInstance.dismiss('cancel');
                                }, function (errorData) {
                                    $scope.cancel();
                                    $rootScope.openModalPopupClose();
                                    $scope.showErrormsg(errorData.msg);
                                });

                            }
                        }
                    };
                    console.log(userData)
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        $scope.usersOptions.reQuery = true;
                    };
                }
            });
        };

        $scope.usersOptions.rejectUser = function (userData) {
            $modal.open({
                templateUrl: 'rejectUser.html',
                windowClass: 'modal addUser',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance) {
                    $scope.selUserData = userData
                    $scope.rejectApproval = function () {
                        console.log($scope.selUserData)
                        if (angular.isDefined($scope.selUserData) && angular.isDefined($scope.selUserData.approvalAction)) {
                            // Delete the created entry
                            if ($scope.selUserData.approvalAction == 'create') {
                                $scope.eventDoc = {
                                    'type': 'User Creation Rejected',
                                    'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                    // 'operatorName': $scope.selUserData.operator,
                                    // 'approverName': $rootScope.credentials.userName
                                    'createdBy': $rootScope.credentials.userName
                                }
                                AuditsService.post($scope.eventDoc)
                                UsersService.deleteData($scope.selUserData["_id"], function (data) {
                                    $scope.eventDoc = {
                                        'type': 'User Remove Successfully',
                                        'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                        // 'operatorName': $scope.selUserData.operator,
                                        // 'approverName': $rootScope.credentials.userName
                                        'createdBy': $rootScope.credentials.userName
                                    }
                                    AuditsService.post($scope.eventDoc)
                                    $rootScope.openModalPopupClose();
                                    $scope.usersOptions.reQuery = true;
                                    $modalInstance.dismiss('cancel');
                                }, function (errorData) {
                                    $scope.cancel();
                                    $rootScope.openModalPopupClose();
                                    $scope.showErrormsg(errorData.msg);
                                });
                            }
                            // Cancel the delete action
                            else if ($scope.selUserData.approvalAction == 'delete') {
                                $scope.selUserData.isApproved = true
                                $rootScope.openModalPopupOpen();
                                $scope.eventDoc = {
                                    'type': 'User Deletion Rejected',
                                    'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                    // 'operatorName': $scope.selUserData.operator,
                                    // 'approverName': $rootScope.credentials.userName
                                    'createdBy': $rootScope.credentials.userName
                                }
                                AuditsService.post($scope.eventDoc)
                                UsersService.put($scope.selUserData["_id"], $scope.selUserData, function (data) {
                                    $rootScope.msgNotify('success', 'Operation Success');
                                    $rootScope.openModalPopupClose();
                                    $scope.eventDoc = {
                                        'type': 'User Added back Successfully',
                                        'actionOn': $scope.selUserData.userName, 'createDateTime': Date.now(),
                                        // 'operatorName': $scope.selUserData.operator,
                                        // 'approverName': $rootScope.credentials.userName
                                        'createdBy': $rootScope.credentials.userName
                                    }
                                    AuditsService.post($scope.eventDoc, function (data) {
                                        console.log("user created to mongo")
                                    })
                                    $scope.cancel();
                                });
                            }
                        }
                    };
                    console.log(userData)
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        $scope.usersOptions.reQuery = true;
                    };
                }
            });
        };

        $scope.usersOptions.disableEdit = function (data, index, action) {
//            if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                return true;
//            }
//            return false;
        }


        $scope.usersOptions.hideButton = function (data, index, action) {
//            if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                return true;
//            }
//            return false;
        }
        $scope.usersOptions.reQuery = true;
//          $scope.init();
//          console.log($scope.usersOptions)
//            $scope.init=function(){
        UsersService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.usersOptions.checkRole = function (data, index, action) {
        if (data.role == "admin") {
            return false;
        }
        return true;
    }
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

    $scope.showMsg = true
    $scope.analyze = function (value) {
        if (strongRegex.test(value)) {
            $scope.showMsg = true
        } else if (mediumRegex.test(value)) {
            $scope.showMsg = false
        } else {
            $scope.showMsg = false
        }
    };
    $scope.usersOptions.addUser = function (userData) {
        $modal.open({
            templateUrl: 'addUser.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.mode = "Add";
                $scope.userData = {};
                $scope.userData.role = ""
                $scope.tmpData = {};
//                $scope.userData["role"] = "customer";
                if (angular.isDefined(userData)) {
                    $scope.mode = "Edit";
                    $scope.userData = userData;
                    $rootScope.availableQuotaForReseller = $rootScope.availableQuotaForReseller + userData.quota;
                    if (userData.role == "reseller") {
                    }
                }
                $rootScope.isDisable = false;
                $scope.saveUser = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData)) {
                        $rootScope.openModalPopupOpen();
                        $scope.userData['userName'] = $scope.userData['userName'].toLowerCase()
                        console.log($scope.userData)
                        UsersService.put(userData["_id"], $scope.userData, function (data) {
                            $scope.eventDoc = {
                                'type': 'User updation',
                                'actionOn': $scope.userData['userName'],
                                // 'approverName': $rootScope.credentials.userName
                                // 'operatorName': $rootScope.credentials.userName,
                                'createdBy': $rootScope.credentials.userName,
                            }
                            AuditsService.post($scope.eventDoc)
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.usersOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    } else {
                        $rootScope.openModalPopupOpen();
                        $scope.userData['userName'] = $scope.userData['userName'].toLowerCase()
                        console.log($scope.userData)
                        $scope.todayDate = Math.floor((new Date().getTime()) / 1000)
                        $scope.userData['lastPwdUpdt'] = $scope.todayDate
                        $scope.userData['approvalAction'] = 'create'
                        $scope.userData['isApproved'] = false
                        if ($scope.userData.role == "Administrator" || $scope.userData.role == "admin" || $scope.userData.role == "Approver")
                            $scope.userData['isApproved'] = true
                        $scope.userData['createdBy'] = $rootScope.credentials.userName
                        $scope.userData['operator'] = $rootScope.credentials.userName
                        UsersService.post($scope.userData, function (data) {
                            $scope.eventDoc = {
                                'type': 'Requested for user creation',
                                'actionOn': $scope.userData['userName'],
                                'createdBy': $rootScope.credentials.userName,
                                // 'operatorName': $rootScope.credentials.userName,
                                // 'approverName': $rootScope.credentials.userName

                            }
                            AuditsService.post($scope.eventDoc, function (data) {
                                console.log("user created to mongo")
                            })
                            if ($scope.userData.role == "Administrator" || $scope.userData.role == "admin" || $scope.userData.role == "Approver") {
                                $scope.eventDoc = {
                                    'type': 'User creation approved and created',
                                    'actionOn': $scope.userData['userName'],
                                    'createdBy': $rootScope.credentials.userName,
                                    // 'operatorName': $rootScope.credentials.userName,
                                    // 'approverName': $rootScope.credentials.userName
                                }
                                AuditsService.post($scope.eventDoc, function (data) {
                                    console.log("user created to mongo")
                                })
                            }
                            $rootScope.msgNotify('success', 'Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.usersOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        }, function (err) {
                            $rootScope.msgNotify('warning', err.msg);

                        });
                    }
                };
                $scope.updatePassword = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.password)) {
                        if ($scope.userData.password == $scope.tmpData.pw2) {
                            $rootScope.openModalPopupOpen();
                            console.log($scope.userData)
                            if (angular.isDefined($scope.userData['lastPwdUpdt'])) {
                                $scope.userData['lastPwdUpdt'] = Math.floor((new Date().getTime()) / 1000)
                                $scope.userData['password'] = $scope.userData.password
                            } else {
                                console.log('no')
                                $scope.todayDate = Math.floor((new Date().getTime()) / 1000)
                                $scope.userData['lastPwdUpdt'] = $scope.todayDate
                                $scope.userData['password'] = $scope.userData.password
                                console.log($scope.userData['lastPwdUpdt'])
                                console.log($scope.userData)
                            }
                            var usersDetails= angular.copy($scope.userData)
                            $scope.encryptPassword = angular.copy(angularCryptoSerivces.encryptCredentials($rootScope.cipherText['AES_KEY'],
                                $rootScope.cipherText['IV'], usersDetails))

                            // $scope.userData.password = angularCryptoSerivces.encrypt($rootScope.cipherText['AES_KEY'],
                            //     $rootScope.cipherText['IV'], $scope.userData.password + '/<=%+,?->/')
                            UsersService.changePassword(userData["_id"], $scope.encryptPassword, function (data) {
                                $scope.eventDoc = {
                                    'type': 'User password updated',
                                    'actionOn': $scope.userData['userName'],
                                    'createdBy': $rootScope.credentials.userName,
                                    // 'operatorName': $rootScope.credentials.userName,
                                    // 'approverName': $rootScope.credentials.userName
                                }
                                AuditsService.post($scope.eventDoc)
                                $rootScope.msgNotify('success', 'Successfully updated');
                                $rootScope.openModalPopupClose();
                                $timeout(function () {
                                    $scope.cancel();
                                }, 3000);
                            }, function (err) {
                                $rootScope.isDisable = false;
                                $rootScope.msgNotify('warning', err.msg);
                                $rootScope.openModalPopupClose();
                            });
                        } else {
                            $rootScope.handleErrorMessages({'msg': 'Password mismatch.'})
                        }
                    }
                };
                $scope.updateQuota = function () {
                    $rootScope.isDisable = true;
                    if (angular.isDefined(userData) && angular.isDefined(userData.quota)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.setQuota(userData["_id"], $scope.userData.quota, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.updateEmail = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.email)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.setEmail(userData["_id"], $scope.userData.email, function (data) {
                            $scope.flag = true
                            if ($scope.flag) {
                                $scope.eventDoc = {}
                                $scope.eventDoc = {
                                    'type': 'User email updated',
                                    'actionOn': $scope.userData['userName'],
                                    // 'operatorName': $rootScope.credentials.userName,
                                    // 'approverName': $rootScope.credentials.userName
                                    'createdBy': $rootScope.credentials.userName,
                                }
                                console.log($scope.eventDoc)
                                AuditsService.post($scope.eventDoc)
                            }
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                    if (angular.isDefined(userData) && angular.isDefined(userData.fromEmail)) {
                        UsersService.setFromEmail(userData["_id"], $scope.userData.fromEmail, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }

                };
                $scope.updateStatus = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.isActive)) {
                        userData.isActive = !userData.isActive;
                        $rootScope.openModalPopupOpen();
                        UsersService.updateStatus(userData["_id"], userData.isActive, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.updateRole = function () {
                    if (angular.isDefined(userData) && angular.isDefined(userData.role)) {
                        $rootScope.openModalPopupOpen();
                        UsersService.updateRole(userData["_id"], userData.role, function (data) {
                            $rootScope.msgNotify('success', 'Successfully updated');
                            $rootScope.openModalPopupClose();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.usersOptions.reQuery = true;
                };
            }
        });
    };

    $scope.confirmDelete = angular.noop();

    $scope.usersOptions.Unlock = function (data) {
        $modal.open({
            templateUrl: 'UnlockUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    data['accountLocked'] = false
                    data['loginCount'] = 0
                    delete data['password']
                    UsersService.put(data["_id"], data, function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.usersOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.usersOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    console.log(data)
                    $rootScope.openModalPopupOpen();
                    $scope.name = data['userName']
                    data['approvalAction'] = 'delete'
                    data['operator'] = $rootScope.credentials.userName
                    data['isApproved'] = false
                    UsersService.put(data["_id"], data, function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.eventDoc = {
                            'type': 'Requested for User deletion',
                            'actionOn': $scope.name,
                            // 'operatorName': $rootScope.credentials.userName,
                            // 'approverName': $rootScope.credentials.userName
                            'createdBy': $rootScope.credentials.userName,
                        }
                        AuditsService.post($scope.eventDoc)
                        $scope.usersOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }

    $scope.getUsers();

    $scope.usersType = ["Administrator", "Approver", "Reconciler", "Dept User", "Investigator", "Configurer", "Enquirer", "Authorizer", "Recon Manager", "Recon Executor", "Recon Assignment", "GST User"]
};
