function mailNotificationsController($scope, $rootScope, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, UsersService, notificationOptionsService, ReconGroupService, BusinessService,notoficationRuleService) {

    //Notification Groupds creation code starts
    $scope.reconEventsDetails = ['Recon Completed','Recon Failed',,'Rollback Back']
    $scope.attachmentsDetails  = ['Recon Summary','Matched Report','Unmatched Report','Custom Reports']
    $scope.notificationOptions = {}
    $scope.notificationOptions.searchFields = {}
    $scope.notificationOptions.service = notificationOptionsService;
    $scope.notificationOptions.init = false;
    $scope.notificationOptions.isLoading = true;
    $scope.notificationOptions.isTableSorting = "name";
    $scope.notificationOptions.sortField = "name";

    $scope.notificationOptions.headers = [
        [
            {name: "Group", searchable: true, sortable: true, dataField: "group", colIndex: 0, width: '20%'},
            {name: "MailID", searchable: true, sortable: true, dataField: "mailids", colIndex: 1, width: '40%'},
            //{name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
            /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
             {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
            {
                name: "Actions",
                colIndex: 4,
                width: '10%',
                actions: [{
                    toolTip: "Edit",
                    action: "addNotificationGroup",
                    posticon: "fa fa-pencil fa-lg",
                    "disabledFn": "disableEdit"
                }, {
                    toolTip: "Delete",
                    action: "deleteFunc",
                    posticon: "fa fa-trash-o fa-lg"
                }]
            }
        ]
    ];

    $scope.notificationOptions.reQuery = true;

    //Add Notification Groups
    $scope.notificationOptions.addNotificationGroup = function (groupData) {
        $modal.open({
            templateUrl: 'notification.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.mode = 'Add'
                if (angular.isDefined(groupData)) {
                    $scope.mode = 'Edit'
                    $scope.groupData = groupData
                }
                else {
                    $scope.groupData = {}
                }
                $scope.saveGroup = function () {
                    $rootScope.openModalPopupOpen();
                    if (!angular.isDefined($scope.groupData._id)) {
                        notificationOptionsService.post($scope.groupData, function (data) {
                            $rootScope.msgNotify('success', 'Notification Group Added Successfully');
                            $rootScope.openModalPopupClose();
                            $scope.notificationOptions.reQuery = true;
                            $scope.cancel()
                        })
                    }
                    else {
                        notificationOptionsService.put($scope.groupData._id, $scope.groupData, function (data) {
                            $rootScope.openModalPopupClose();
                            $rootScope.msgNotify('success', 'Notification Group Updated Successfully');
                            $scope.cancel()
                        })
                    }
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.usersOptions.reQuery = true;
                };
            }
        })
    }

    //Delete Noitification Groups
    $scope.notificationOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $scope.groupData = data
                    $rootScope.openModalPopupOpen();
                    notificationOptionsService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.notificationOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.reconOptions = {};
    $scope.reconOptions.init = false;
    $scope.reconOptions.isLoading = true;
    $scope.reconOptions.isTableSorting = "name";
    $scope.reconOptions.sortField = "name";

    $scope.getReconGroups = function () {
        $scope.groupList = [];
        $scope.reconOptions.searchFields = {}
        $scope.reconOptions.service = ReconGroupService;
        $scope.reconOptions.headers = [
            [
                {name: "Group", searchable: true, sortable: true, dataField: "groupName", colIndex: 1, width: '10%'},

                {name: "Recons", searchable: true, sortable: true, dataField: 'recons', colIndex: 3, width: '20%'},

                {
                    name: "Actions",
                    colIndex: 4,
                    width: '20%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addReconGroup",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",

                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.reconOptions.reQuery = true;


    }

    $scope.getRecons = function () {
        $scope.reconDataAll = []
        BusinessService.get(undefined, undefined, function (data) {
            if (data.data.length > 0) {
                angular.forEach(data.data, function (val, key) {
                    $scope.reconDataAll.push({
                        'name': val.RECON_NAME,
                        'id': val.RECON_ID,
                    })
                })
            }

            $scope.recons = []
            angular.forEach($scope.reconDataAll, function (val, key) {
                $scope.recons.push({'name': val.name, 'id': val.id, 'ticked': false})

            })
        })

    }
    $scope.mode = 'Add'

    $scope.reconOptions.addReconGroup = function (mode) {
        $scope.reconGroup = {}

        $modal.open({
            templateUrl: 'addReconGroup.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                if (angular.isDefined(mode)) {
                    $scope.reconGroup['groupName'] = mode['groupName']
                    $scope.recons = []
                    angular.forEach($scope.reconDataAll, function (val, key) {
                        if (mode['recons'].indexOf(val.name) != -1) {
                            $scope.recons.push({'name': val.name, 'id': val.id, 'ticked': true})
                        }
                        else {
                            $scope.recons.push({'name': val.name, 'id': val.id, 'ticked': false})
                        }

                    })
                }
                $scope.saveReconGroup = function () {
                    reconList = []
                    angular.forEach($scope.reconGroup['recons'], function (val, key) {
                        reconList.push(val['name'])
                    })
                    $scope.reconGroup['recons'] = reconList

                    if (angular.isDefined(mode)) {

                        ReconGroupService.put(mode["_id"], $scope.reconGroup, function (data) {
                            $rootScope.msgNotify('success', 'Recon Group Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.reconOptions.reQuery = true;
                            $scope.cancel();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }

                    else {
                        ReconGroupService.post($scope.reconGroup, function (data) {
                            $rootScope.msgNotify('success', 'Recon Group Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.reconOptions.reQuery = true;
                            $scope.cancel();
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                }


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');

                };

            }


        });

    }

    $scope.reconOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    ReconGroupService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.reconOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.getReconGroups()

    $scope.configure = {}
    $scope.configureNotificationRule = {}
    $scope.configureNotificationRule.searchFields = {}
    $scope.configureNotificationRule.service = notoficationRuleService
    $scope.configureNotificationRule.isTableSorting = "name";
    $scope.configureNotificationRule.sortField = "name";

    $scope.configureNotificationRule.headers = [
        [
            {name: "RuleName", searchable: true, sortable: true, dataField: "ruleName", colIndex: 0, width: '20%'},
            {name: "Mail Subject", searchable: true, sortable: true, dataField: "mailSub", colIndex: 1, width: '20%'},
            {name: "Events", searchable: true, sortable: true, dataField: "events", colIndex: 2, width: '20%'},
            {name: "Mail Groups", searchable: true, sortable: true, dataField: "mailGroups", colIndex: 3, width: '20%'},
            {
                name: "Recon Groups",
                searchable: true,
                sortable: true,
                dataField: "reconGroups",
                colIndex: 4,
                width: '20%'
            },
            {
                name: "Attachments",
                searchable: true,
                sortable: true,
                dataField: "attachments",
                colIndex: 5,
                width: '20%'
            },
            //{name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
            /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
             {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
            {
                name: "Actions",
                colIndex: 4,
                width: '10%',
                actions: [{
                    toolTip: "Edit",
                    action: "addNotificationRule",
                    posticon: "fa fa-pencil fa-lg",
                    "disabledFn": "disableEdit"
                }, {
                    toolTip: "Delete",
                    action: "deleteFunc",
                    posticon: "fa fa-trash-o fa-lg"
                }]
            }
        ]
    ];

    notificationOptionsService.get(undefined, undefined, function (data) {
        if (data['total'] > 0) {
            $scope.mailDetails = data['data']
        }
    })

    ReconGroupService.get(undefined,undefined,function (data) {
        if (data['total'] > 0) {
            $scope.reconGroupsDetails = data['data']
        }
    })
    $scope.configureNotificationRule.addNotificationRule = function (ruleData) {

        $modal.open({
            templateUrl: 'configure.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.mode = 'Add'
                $scope.reconGroup = []
                $scope.mailGroup = []
                $scope.reconEvents = []
                $scope.attachments = []
                if (angular.isDefined(ruleData)) {
                    $scope.mode = 'Edit'

                    angular.forEach($scope.reconGroupsDetails,function (k,v) {
                        if(ruleData['reconGroups'].indexOf(k.groupName)!=-1){
                            $scope.reconGroup.push({'name': k.groupName, 'reconids': k.recons, 'ticked': true})
                        }
                        else{
                            $scope.reconGroup.push({'name': k.groupName, 'reconids': k.recons, 'ticked': false})
                        }
                    })

                    angular.forEach($scope.mailDetails, function (k, v) {
                        if (ruleData['mailGroups'].indexOf(k.group)!=-1) {
                            $scope.mailGroup.push({'name': k.group, 'reconids': k.mailds, 'ticked': true})
                        }
                        else {
                            $scope.mailGroup.push({'name': k.group, 'reconids': k.mailds, 'ticked': false})
                        }
                    })

                    angular.forEach($scope.reconEventsDetails, function (k, v) {
                        if (ruleData['events'].indexOf(k) != -1) {
                            $scope.reconEvents.push({'name': k, 'ticked': true})
                        }
                        else {
                            $scope.reconEvents.push({'name': k, 'ticked': false})
                        }
                    })

                    angular.forEach($scope.attachmentsDetails, function (k, v) {
                        if (ruleData['attachments'].indexOf(k)!=-1) {
                            $scope.attachments.push({'name': k, 'reconids': v.recons, 'ticked': true})
                        }
                        else {
                            $scope.attachments.push({'name': k, 'reconids': v.recons, 'ticked': false})
                        }

                    })

                    $scope.configure = ruleData

                }
                else {
                    $scope.configure = {}
                    angular.forEach($scope.mailDetails, function (k, v) {
                        $scope.mailGroup    .push({'name': k.group, 'mailids': k.mailids, 'ticked': false})
                    })
                    angular.forEach($scope.reconGroupsDetails, function (k, v) {
                        $scope.reconGroup.push({'name': k.groupName, 'reconids': v.recons, 'ticked': false})
                    })
                    angular.forEach($scope.attachmentsDetails , function (k, v) {
                        $scope.attachments.push({'name': k, 'ticked': false})
                    })
                    angular.forEach($scope.reconEventsDetails, function (k, v) {
                        $scope.reconEvents.push({'name': k, 'ticked': false})
                    })
                }
                $scope.saveNotificationRule = function () {
                    $rootScope.openModalPopupOpen();
                    $scope.recon_groups = []
                    $scope.mail_groups = []
                    $scope.events = []
                    $scope.attachments_group= []
                    if (!angular.isDefined($scope.configure._id)) {
                        angular.forEach($scope.configure['reconGroups'],function (v,k) {
                            if($scope.recon_groups.indexOf(v.name)==-1){
                                $scope.recon_groups.push(v.name)
                            }
                        })

                        angular.forEach($scope.configure['mailGroups'],function (v,k) {
                            if($scope.mail_groups.indexOf(v.name)==-1){
                                $scope.mail_groups.push(v.name)
                            }
                        })

                        angular.forEach($scope.configure['events'],function (v,k) {
                            if($scope.events.indexOf(v.name)==-1){
                                $scope.events.push(v.name)
                            }
                        })

                        angular.forEach($scope.configure['attachments'], function (v, k) {
                            if ($scope.attachments_group.indexOf(v.name) == -1) {
                                $scope.attachments_group.push(v.name)
                            }
                        })

                        $scope.configure['mailGroups'] = $scope.mail_groups
                        $scope.configure['reconGroups'] = $scope.recon_groups;
                        $scope.configure['events'] = $scope.events;
                        $scope.configure['attachments'] = $scope.attachments_group;

                        notoficationRuleService.post($scope.configure, function (data) {
                            $rootScope.msgNotify('success', 'Notification Rule Added Successfully');
                            $rootScope.openModalPopupClose();
                            $scope.configureNotificationRule.reQuery = true;
                            $scope.cancel()
                        })
                    }
                    else {
                        notoficationRuleService.put($scope.configure._id, $scope.configure, function (data) {
                            $rootScope.openModalPopupClose();
                            $rootScope.msgNotify('success', 'Notification Rule Updated Successfully');
                            $scope.configureNotificationRule.reQuery = true;
                            $scope.cancel()
                        })
                    }
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.configureNotificationRule.reQuery = true;
                };
            }
        })
    }

     //Delete Noitification Groups
    $scope.configureNotificationRule.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $scope.groupData = data
                    $rootScope.openModalPopupOpen();
                    notoficationRuleService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.configureNotificationRule.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

}