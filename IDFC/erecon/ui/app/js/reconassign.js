function reconAssignController($scope, $rootScope, $http, $routeParams, $location, $timeout, $route, $window, $modal, UsersService, AccountPoolService, UserPoolService, ReconAssignmentService, BusinessService, AuditsService) {
    $scope.reconOptions = {};
    $scope.reconOptions.init = false;
    $scope.reconOptions.isLoading = true;
    $scope.reconOptions.isTableSorting = "name";
    $scope.reconOptions.sortField = "name";
    //      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;

    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.reconOptions.searchFields = {}
        $scope.reconOptions.service = ReconAssignmentService;
        $scope.reconOptions.headers = [
            [
                {name: "UserPool", searchable: true, sortable: true, dataField: "userpool", colIndex: 0, width: '10%'},
                {name: "User", searchable: true, sortable: true, dataField: "user", colIndex: 1, width: '10%'},
                {
                    name: "Business Type",
                    searchable: true,
                    sortable: true,
                    dataField: "workPoolName",
                    colIndex: 2,
                    width: '20%'
                },
                {name: "Recon Types", searchable: true, sortable: true, dataField: "recons", colIndex: 3, width: '20%'},
                {
                    name: "AccountPools",
                    searchable: true,
                    sortable: true,
                    dataField: "accountPools",
                    colIndex: 4,
                    width: '20%'
                },
                //{name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
                {
                    name: "Actions",
                    colIndex: 4,
                    width: '20%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addUser",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.reconOptions.disableEdit = function (data, index, action) {
//                if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                    return true;
//                }
//                return false;
        }


        $scope.reconOptions.hideButton = function (data, index, action) {
//                if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
//                    return true;
//                }
//                return false;
        }
        $scope.reconOptions.reQuery = true;
        //          $scope.init();
        //          console.log($scope.reconOptions)
        //            $scope.init=function(){
        ReconAssignmentService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.reconOptions.checkRole = function (data, index, action) {
        if (data.role == "admin") {
            return false;
        }
        return true;
    }
    $scope.usersType = []
    $scope.userpools = []
    $scope.businessContextData = []
    $scope.filteredBusinessData = []
//    UsersService.get(undefined,undefined,function(data){
//        angular.forEach(data.data,function(val,key){
//            $scope.usersType.push(val.userName)
//        })
//    })
    UserPoolService.get(undefined, undefined, function (data) {
        angular.forEach(data.data, function (val, key) {
            $scope.userpools.push(val.name)
        })
    })
    $scope.selectedBusiness = true


    AccountPoolService.get(undefined, undefined, function (data) {
        $scope.accountpools = []
        $scope.acarray = []
        if (data.data.length > 0) {
            angular.forEach(data.data, function (val, key) {
                if ($scope.acarray.indexOf(val.ACCOUNT_POOL_NAME) == -1) {
                    $scope.acarray.push(val.ACCOUNT_POOL_NAME)
                    $scope.accountpools.push({
                        'name': val.ACCOUNT_POOL_NAME,
                        'id': val.ACCOUNT_POOL_CONTEXT_CODE
                    })
                }
            })

        }
    })
    $scope.reconDataAll = []
    $scope.arraycheck = []
    BusinessService.get(undefined, undefined, function (data) {
        if (data.data.length > 0) {
            angular.forEach(data.data, function (val, key) {
                if ($scope.arraycheck.indexOf(val.WORKPOOL_NAME) == -1) {
                    $scope.arraycheck.push(val.WORKPOOL_NAME)
                    $scope.businessContextData.push({'name': val.WORKPOOL_NAME, 'id': val.BUSINESS_CONTEXT_ID})
                }
                $scope.reconDataAll.push({
                    'name': val.RECON_NAME,
                    'id': val.RECON_ID,
                    'business_id': val.BUSINESS_CONTEXT_ID,
                    'workPool': val.WORKPOOL_NAME
                })
            })
        }
    })
    $scope.reconOptions.addUser = function (reconData) {
        $scope.reconBusinessData = []
        $modal.open({
            templateUrl: 'addUser.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, ReconAssignmentService, AccountPoolService) {
                $scope.getRecons = function (business) {
                    $scope.reconBusinessData = []
                    angular.forEach($scope.reconDataAll, function (val, key) {
                        if (val.workPool == business) {
                            $scope.reconData.businessContext = val.business_id
                            $scope.reconBusinessData.push({'name': val.name, 'id': val.id, 'ticked': false})
                        }

                    })

                }
                $scope.reconData = reconData
                $scope.usersType = []
                $scope.getUsers = function (userpool) {
                    console.log(userpool)
                    $scope.filteredBusinessData = []
                    var perColConditions = {};
                    perColConditions["name"] = userpool
                    UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                        console.log(data)
                        if (data.data.length > 0) {
                            $scope.usersType = data.data[0].users.split(",")
                            $scope.business_contexts = data.data[0].business_context.split(",")
                            angular.forEach($scope.business_contexts, function (v, k) {
                                angular.forEach($scope.businessContextData, function (val, key) {
                                    if (v == val.id) {
                                        $scope.filteredBusinessData.push({'name': val.name, 'id': val.id})

                                    }

                                })

                            })
                        }
                    })

                }
                if (angular.isDefined(reconData)) {
                    $scope.recons_Data = []
                    $scope.acPools_Data = []
                    angular.forEach($scope.accountpools, function (v, k) {
                        v['ticked'] = false
                    })
                    if (angular.isDefined(reconData._id)) {
                        console.log($scope.accountpools)
                        $scope.mode = "Edit";
                        $rootScope.openModalPopupOpen()
                        $scope.reconData_edit = reconData
                        $scope.getRecons($scope.reconData_edit.workPoolName)
                        $scope.getUsers($scope.reconData_edit.userpool)
                        $scope.recons_Data = $scope.reconData_edit.recons.split(',')
                        $scope.acPools_Data = $scope.reconData_edit.accountPools.split(',')
                        angular.forEach($scope.recons_Data, function (v, k) {
                            angular.forEach($scope.reconBusinessData, function (val, key) {
                                if (v == val.id) {
                                    $scope.reconBusinessData[key] = {'name': val.name, 'id': val.id, 'ticked': true}
                                }
                            })
                        })
                        angular.forEach($scope.acPools_Data, function (v, k) {
                            angular.forEach($scope.accountpools, function (val, key) {
                                if (v == val.id) {
                                    $scope.accountpools[key] = {'name': val.name, 'id': val.id, 'ticked': true}
                                }
                            })
                        })
                        $timeout(function () {
                            $rootScope.openModalPopupClose()
                        }, 5000)
                    }

                }
                else {
                    $scope.mode = "Add";
                    $scope.reconData = {};
                    $scope.reconData["user"] = "";
                    $scope.reconData["businessContext"] = "";
                    $scope.reconData["workPoolName"] = "";
                }

                $rootScope.isDisable = false;
                $scope.getSelectedAccountPools = function (data) {
                    console.log(data)
                }
                $scope.getAccountPools = function (recons) {
                    if (typeof recons == "string") {
                        console.log(recons.split(","))
                    }
                }
                $scope.saveRecon = function (reconData) {
                    $rootScope.isDisable = true;
                    console.log(reconData)
                    if (angular.isDefined($scope.reconData._id)) {
                        $scope.recons_data = []
                        $scope.acpools_data = []
                        if (typeof reconData['recons'] != "string") {
                            angular.forEach(reconData.recons, function (va, ke) {
                                console.log(va)
                                $scope.recons_data.push(va.id)
                            })
                        }
                        if (typeof reconData.accountPools != "string") {
                            angular.forEach(reconData.accountPools, function (v, k) {
                                console.log(v)
                                $scope.acpools_data.push(v.id)
                            })
                        }

                        if (typeof reconData['recons'] != "string") {
                            $scope.reconData['recons'] = $scope.recons_data.join(',')
                        }
                        if (typeof reconData['accountPools'] != "string") {
                            $scope.reconData['accountPools'] = $scope.acpools_data.join(',')
                        }
                        console.log($scope.reconData)
                        $rootScope.openModalPopupOpen();
                        ReconAssignmentService.put(reconData["_id"], $scope.reconData, function (data) {
                            $scope.eventDoc = {
                                'type': 'ReconAssignment updated',
                                'actionOn': $scope.reconData['userpool'],
                                'createDateTime': Date.now(),
                                'createdBy': $rootScope.credentials.userName
                            }
                            console.log($scope.eventDoc)
                            AuditsService.post($scope.eventDoc);
                            $rootScope.msgNotify('success', 'Recon Assignment Successfully updated');
                            $rootScope.openModalPopupClose();
                            $scope.reconOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    } else {
                        $rootScope.openModalPopupOpen();
                        $scope.recons_data = []
                        $scope.acpools_data = []
                        if (typeof reconData['recons'] != "string") {
                            angular.forEach(reconData.recons, function (va, ke) {
                                console.log(va)
                                $scope.recons_data.push(va.id)
                            })
                        }
                        if (typeof reconData.accountPools != "string") {
                            angular.forEach(reconData.accountPools, function (v, k) {
                                console.log(v)
                                $scope.acpools_data.push(v.id)
                            })
                        }

                        if (typeof reconData['recons'] != "string") {
                            $scope.reconData['recons'] = $scope.recons_data.join(',')
                        }
                        if (typeof reconData['accountPools'] != "string") {
                            $scope.reconData['accountPools'] = $scope.acpools_data.join(',')
                        }
                        ReconAssignmentService.post($scope.reconData, function (data) {
                            $scope.flag = true
                            if ($scope.flag) {
                                $scope.eventDoc = {}
                                $scope.eventDoc = {
                                    'type': 'ReconAssignment Created',
                                    'actionOn': $scope.reconData['userpool'],
                                    'createDateTime': Date.now(),
                                    'createdBy': $rootScope.credentials.userName
                                }
                                console.log($scope.eventDoc)
                                AuditsService.post($scope.eventDoc, function (data) {
                                    console.log('Recon assignment created in mongo')
                                });
                            }
                            $rootScope.msgNotify('success', 'Recon Assignment Successfully created');
                            $rootScope.openModalPopupClose();
                            $scope.reconOptions.reQuery = true;
                            $timeout(function () {
                                $scope.cancel();
                            }, 3000);
                        });
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.reconOptions.reQuery = true;
                };
            }
        });


    };
    $scope.acoountPoolsFlag = false
    $scope.reconOptions.addUserMultiple = function (reconData) {
        $scope.reconBusinessData = []
        $scope.reconData = {}

        $modal.open({
            templateUrl: 'addUserMultiple.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, ReconAssignmentService, AccountPoolService) {
                $scope.reconBusinessData = []

                function search(nameKey, myArray) {
                    for (var i = 0; i < myArray.length; i++) {
                        if (myArray[i].business_id === nameKey) {
                            return myArray[i];
                        }
                    }
                }

                $scope.getRecons = function (business) {
                    if (business['ticked'] == false) {
                        angular.forEach($scope.reconBusinessData, function (v, k) {
                            if (v.workPool == business['name']) {
                                $scope.reconBusinessData.splice(k)
                            }
                        })
                    }
                    else {
                        angular.forEach($scope.reconDataAll, function (val, key) {
                            if (val.workPool== business['name']) {
                                $scope.reconBusinessData.push({
                                    'name': val.name,
                                    'id': val.id,
                                    'business_id': val.business_id,
                                    'ticked': false
                                })
                            }
                        })
                    }
                    var resultObject = search("1111131119", $scope.reconBusinessData);
                    console.log(resultObject)
                    $scope.acoountPoolsFlag = (angular.isDefined(resultObject)) ? true : false
                }
                $scope.usersType = []
                $scope.getUsers = function (userpool) {
                    console.log(userpool)
                    $scope.usersType = []
                    $scope.filteredBusinessData = []
                    var perColConditions = {};
                    perColConditions["name"] = userpool
                    UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                        console.log(data)
                        if (data.data.length > 0) {
                            angular.forEach(data.data[0].users.split(","), function (v, k) {
                                $scope.usersType.push({'name': v, 'id': v})
                            })
                            $scope.business_contexts = data.data[0].business_context.split(",")
                            angular.forEach($scope.business_contexts, function (v, k) {
                                angular.forEach($scope.businessContextData, function (val, key) {
                                    if (v == val.id) {
                                        $scope.filteredBusinessData.push({'name': val.name, 'id': val.id})

                                    }

                                })

                            })
                        }
                    })

                }
                $rootScope.isDisable = false;
                $scope.getSelectedAccountPools = function (data) {
                    console.log(data)
                }
                $scope.getAccountPools = function (recons) {
                    console.log(recons)
                    if (typeof recons == "string") {
                        console.log(recons.split(","))
                    }
                }
                $scope.saveRecon = function (reconData) {
                    $rootScope.openModalPopupOpen()
                    ReconAssignmentService.addMultiple('dummy', reconData, function (data) {
                        $rootScope.msgNotify('success', 'Recon Assignment Successfully created');
                        $rootScope.openModalPopupClose();
                        $scope.reconOptions.reQuery = true;
                        $scope.cancel()
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.reconOptions.reQuery = true;
                };
            }
        });


    };

    $scope.confirmDelete = angular.noop();

    $scope.reconOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $scope.userpool = data['userpool']
                    $rootScope.openModalPopupOpen();
                    ReconAssignmentService.deleteData(data["_id"], function (data) {
                        $scope.eventDoc = {
                            'type': 'ReconAssignment Deletion',
                            'actionOn': $scope.userpool,
                            'createdBy': $rootScope.credentials.userName
                        }
                        AuditsService.post($scope.eventDoc)
                        $rootScope.openModalPopupClose();
                        $scope.reconOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    //      }
    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }
    $scope.getUsers();

}

