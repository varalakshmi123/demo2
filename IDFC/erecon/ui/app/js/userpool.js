function userpoolController($scope, $rootScope, $http, $routeParams, $location, $timeout, $route, $window, $modal, UsersService, UserPoolService, BusinessService, AuditsService, GSTSourceProductGroupService) {
//        if($rootScope.credentials.role != 'admin'){
//            $rootScope.msgNotify('error','Access restricted to view schedules.');
//            $location.path("/migratedSystems");
//            return false;
//        }
    $scope.userpoolOptions = {};
    $scope.userpoolOptions.init = false;
    $scope.userpoolOptions.isLoading = true;
    $scope.userpoolOptions.isTableSorting = "name";
    $scope.userpoolOptions.sortField = "name";
    //      $rootScope.maxQuotaLimit=2000;
    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    $scope.getUsers = function () {
        $scope.userList = [];
        $scope.userpoolOptions.searchFields = {}
        $scope.userpoolOptions.service = UserPoolService;
        $scope.userpoolOptions.headers = [
            [
                {
                    name: "",
                    dataField: "SelectedUserPool",
                    checkbox: true,
                    selectAll: false,
                    colIndex: 0,
                    datawidth: "5%"
                },
                {name: "Name", searchable: true, sortable: true, dataField: "name", colIndex: 0, datawidth: "5%"},
                {name: "Role", searchable: true, sortable: true, dataField: "role", colIndex: 0, datawidth: "5%"},
                {name: "Users", searchable: true, sortable: true, dataField: "users", colIndex: 2, datawidth: "40%"},
                {
                    name: "Business Contexts",
                    searchable: true,
                    sortable: true,
                    dataField: "business_context",
                    colIndex: 2,
                    datawidth: "40%"
                },
                {
                    name: "GST Sources",
                    searchable: true,
                    sortable: true,
                    dataField: "gst_sources",
                    colIndex: 2,
                    datawidth: "40%"
                },
                //                {name: "API Key", searchable: false, sortable: false, dataField: "apiKey", colIndex: 2},
                /*{name: "Quota in GB",width:"8%", searchable: false, sortable: true, dataField: "quota", colIndex: 3},
                 {name: "% Used",width:"8%", searchable: false, sortable: true, dataField: "percentUsed", colIndex: 4},*/
                {
                    name: "Actions",
                    colIndex: 4,
                    datawidth: "5%",
                    actions: [{
                        toolTip: "Edit",
                        action: "addUser",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg",
                            "showHideFn": "checkRole",
                            "disabledFn": "hideButton"
                        }]
                }
            ]
        ];

        $scope.userpoolOptions.disableEdit = function (data, index, action) {
            //if (($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
            //    return true;
            //}
            //return false;
        }


        $scope.userpoolOptions.hideButton = function (data, index, action) {
            //if (($rootScope.credentials.role == 'reseller' && data.role == 'reseller') || ($rootScope.credentials.role == 'admin' && data.isResellerCustomer)) {
            //    return true;
            //}
            //return false;
        }
        $scope.userpoolOptions.reQuery = true;
        //          $scope.init();
        //          console.log($scope.userpoolOptions)
        //            $scope.init=function(){
        UserPoolService.get(undefined, undefined, function (data) {
            $scope.users = data;
            $scope.available = 0;
            angular.forEach($scope.users.data, function (value, key) {
                if ($rootScope.credentials.role == 'reseller' && value.role == 'reseller') {
                    $rootScope.maxQuotaLimit = value.quota;
                }
                else if ($rootScope.credentials.role == 'reseller' && value.role != 'reseller') {
                    $scope.available = $scope.available + value.quota;
                }
                $scope.userList.push({
                    'Name': value.userName,
                    'Role': value.role,
                    'Owner': value.owner,
                    'API Key': value.apiKey,
                    'Quota in GB': value.quota,
                    'Percentage Used': value.percentUsed
                })
            });
            $rootScope.availableQuotaForReseller = $rootScope.maxQuotaLimit - $scope.available;

        });

    }
    $scope.userpoolOptions.checkRole = function (data, index, action) {
        if (data.role == "admin") {
            return false;
        }
        return true;
    }
    $scope.business_Data = []
    $scope.arraycheck = []
    $scope.userData = {}
    $scope.selectedUserPoolId = [];
    $scope.approveDelete = true;
    $scope.allGstSources = []
    $scope.userpoolOptions.addUser = function (userData) {
        $scope.business_Data = []
        $scope.arraycheck = []
        $scope.userData = {}
        $scope.allGstSources = []
        $rootScope.openModalPopupOpen()
        BusinessService.get(undefined, undefined, function (data) {
            $rootScope.openModalPopupClose()
            angular.forEach(data.data, function (val, key) {
                if ($scope.arraycheck.indexOf(val.WORKPOOL_NAME) == -1) {
                    $scope.arraycheck.push(val.WORKPOOL_NAME)
                    $scope.business_Data.push({
                        'name': val.WORKPOOL_NAME,
                        'id': val.BUSINESS_CONTEXT_ID,
                        'ticked': false
                    })

                    $timeout(function () {
                        $rootScope.openModalPopupClose()
                    }, 5000)
                    console.log($scope.userData)
                }
            })

            GSTSourceProductGroupService.get(undefined, undefined, function (data) {
                unique_src = []
                angular.forEach(data.data, function (val, key) {
                    if (unique_src.indexOf(val.sourceName) == -1) {
                        $scope.allGstSources.push({
                            "name": val.sourceName,
                            "id": val.sourceName,
                            'ticked': false
                        })
                        unique_src.push(val.sourceName)
                    }
                })

                $scope.tagData = []
                UsersService.get(undefined, undefined, function (data) {
                    console.log(data)
                    angular.forEach(data.data, function (val, key) {
                        $scope.tagData.push({'name': val.userName, 'ticked': false})
                    })
                    $modal.open({
                        templateUrl: 'addUserPool.html',
                        windowClass: 'modal addUser',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                            $rootScope.isDisable = false;
                            if (angular.isDefined(userData)) {
                                $rootScope.openModalPopupOpen()
                                $scope.mode = "Edit";
                                $scope.userData['_id'] = userData['_id'];
                                $scope.userData['name'] = userData['name'];
                                $scope.userData['role'] = userData['role'];
                                $scope.userData_edit = userData
                                console.log(userData.business_context)
                                $scope.businessData = $scope.userData_edit.business_context.split(',')
                                $scope.users_Data = $scope.userData_edit.users.split(',')

                                angular.forEach($scope.businessData, function (v, k) {
                                    angular.forEach($scope.business_Data, function (val, key) {
                                        if (v == val.id) {
                                            $scope.business_Data[key] = {'name': val.name, 'id': val.id, 'ticked': true}
                                        }
                                    })
                                })
                                angular.forEach($scope.users_Data, function (v, k) {
                                    angular.forEach($scope.tagData, function (val, key) {
                                        if (v == val.name) {
                                            $scope.tagData[key] = {'name': val.name, 'ticked': true}
                                        }
                                    })
                                })

                                if (angular.isDefined($scope.userData_edit.gst_sources)) {
                                    $scope.gstSources = $scope.userData_edit.gst_sources.split(',')
                                    angular.forEach($scope.gstSources, function (v, k) {
                                        angular.forEach($scope.allGstSources, function (val, key) {
                                            if (v == val.id) {
                                                $scope.allGstSources[key] = {
                                                    'name': val.name,
                                                    'id': val.id,
                                                    'ticked': true
                                                }
                                            }
                                        })
                                    })
                                    $scope.gstSourceData = $scope.userData_edit.gst_sources.split(',')
                                }

                                $timeout(function () {
                                    $rootScope.openModalPopupClose()
                                }, 5000)
                            }
                            else {
                                $scope.mode = "Add";
                                $scope.userData = {};
                                $scope.userData["role"] = "";
                            }
                            $scope.saveUserpool = function (userData) {
                                $rootScope.openModalPopupOpen();

                                $rootScope.isDisable = true;
                                $scope.business_context_data = []
                                $scope.users_data = []
                                $scope.gst_sources = []

                                if (typeof userData['business_context'] != "string") {
                                    angular.forEach(userData.business_context, function (va, ke) {
                                        console.log(va)
                                        $scope.business_context_data.push(va.id)
                                    })
                                }

                                if (typeof userData['gst_sources'] != "string") {
                                    angular.forEach(userData.gst_sources, function (va, ke) {
                                        $scope.gst_sources.push(va.id)
                                    })
                                }

                                if (typeof userData.users != "string") {
                                    angular.forEach(userData.users, function (v, k) {
                                        console.log(v)
                                        $scope.users_data.push(v.name)
                                    })
                                }

                                if (typeof userData['business_context'] != "string") {
                                    userData['business_context'] = $scope.business_context_data.join(',')
                                }
                                if (typeof userData['users'] != "string") {
                                    userData['users'] = $scope.users_data.join(',')
                                }
                                if (typeof userData['gst_sources'] != 'string') {
                                    userData['gst_sources'] = $scope.gst_sources.join(',')
                                }


                                if (angular.isDefined($scope.userData._id)) {
                                    UserPoolService.put(userData["_id"], $scope.userData, function (data) {
                                        $rootScope.msgNotify('success', 'UserPool Successfully updated');
                                        $scope.userpoolOptions.reQuery = true;
                                        $rootScope.openModalPopupClose();
                                        $scope.userData = {};
                                        $scope.cancel();
                                    });
                                } else {
                                    UserPoolService.post($scope.userData, function (data) {
                                        $rootScope.msgNotify('success', 'UserPool Successfully created');
                                        $scope.userpoolOptions.reQuery = true;
                                        $rootScope.openModalPopupClose();
                                        $scope.cancel();
                                    });
                                }
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                                $scope.userpoolOptions.reQuery = true;
                            };
                        }
                    });
                })
            })
        })

    };
    $scope.confirmDelete = angular.noop();
    $scope.addUsers = function () {
        //$scope.selectedUserpools = []
        //angular.forEach($scope.selectedUserPoolId, function (val,key) {
        //    UserPoolService.get(val,undefined, function (data) {
        //        console.log(data)
        //        $scope.selectedUserpools.push(data['name'])
        //    })
        //})
        $modal.open({
            templateUrl: 'addUsers.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, UsersService, UserPoolService, BusinessService) {
                $scope.mode = "Add"
                $scope.tagData = []
                UsersService.get(undefined, undefined, function (data) {
                    //console.log(data)
                    angular.forEach(data.data, function (val, key) {
                        $scope.tagData.push({'name': val.userName, 'id': val._id, 'ticked': false})
                    })
                })
                $scope.saveUsers = function (userData) {
                    console.log(userData)
                    var dataU = {}
                    dataU['selectedUserPool_ids'] = $scope.selectedUserPoolId
                    dataU['selectedUsers'] = userData['users']
                    UserPoolService.addUsers('dummy', dataU, function (data) {
                        console.log(data)
                    })
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.approveDelete = true
                    $scope.userpoolOptions.reQuery = true;
                };
            }
        });
    }

    $scope.selectedUsers = function (role) {
        $scope.SelectedUsers = []
        angular.forEach($scope.userData, function (k, v) {
            if (k['role'] == role) {
                $scope.SelectedUsers.push(k)
            }
        })
    }
    $scope.userpoolOptions.Change = function (data) {
        if ($scope.userpoolOptions.selectedAll) {
            $scope.userpoolOptions.selectedAll = false;
            $scope.selectedUserPoolId = [];
            angular.forEach($scope.userpoolOptions.datamodel.data, function (v, k) {
                if (v.SelectedUserPool && data._id != v._id) {
                    $scope.selectedUserPoolId.push(v._id);
                }
            });
        }
        else {
            angular.forEach($scope.userpoolOptions.datamodel.data, function (v, k) {
                if (data._id == v._id) {
                    if (angular.isDefined(v.SelectedUserPool)) {
                        if ($scope.selectedUserPoolId.indexOf(data._id) > -1) {
                            v['SelectedUserPool'] = false;
                        }
                        else {
                            v['SelectedUserPool'] = true;
                        }
                    }
                    else {
                        v['SelectedUserPool'] = true;
                    }
                }
            });
            $scope.selectedUserPoolId.length == 0 ? $scope.approveDelete = true : $scope.approveDelete = false;
            //$scope.selectedUserPoolId.length == 1 ? $scope.viewButton = false : $scope.viewButton = true;
            $scope.selectedUserPoolId.length == $scope.userpoolOptions.datamodel.data.length ? $scope.userpoolOptions.selectedAll = true : $scope.userpoolOptions.selectedAll = false;
        }


        console.log($scope.selectedUserPoolId);
    };
    $scope.userpoolOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteUser.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    console.log(data)
                    $scope.name = data['name']
                    console.log($scope.name)
                    UserPoolService.deleteData(data["_id"], function (data) {
                        $scope.eventDoc = {
                            'type': 'Userpool Deleted',
                            'actionOn': $scope.name,
                            'createdBy': $rootScope.credentials.userName,
                            'createDateTime': Date.now(),
                        }
                        AuditsService.post($scope.eventDoc)
                        $rootScope.openModalPopupClose();

                        $scope.userpoolOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    //      }
    $scope.exportUsersCSV = function () {
        return $scope.userList;
    }
    $scope.getUsers();
    $scope.usersType = ["Administrator", "Reconciler", "Dept User", "Investigator", "Configurer",
        "Enquirer", "Authorizer", "GST User"]

}
