agGrid.initialiseAgGridWithAngular1(angular);
agGrid.initialiseAgGridWithWebComponents();
var app = angular.module("webApp", ['ngRoute', 'ngSanitize', 'webApp.services', 'webApp.directives', 'webApp.filters', 'ui.bootstrap', 'ui', 'agGrid', 'highcharts-ng', 'ui.multiselect', 'isteven-multi-select', "ngIdle", "angularFileUpload", 'ngCsv', 'scrollable-table', 'angularjs-dropdown-multiselect']);

app.config(["$locationProvider", "$httpProvider", "$sceProvider", "IdleProvider", "KeepaliveProvider", function ($locationProvider, $httpProvider, $sceProvider, IdleProvider, KeepaliveProvider) {
    $sceProvider.enabled(false);
    //Move to html5 mode, clean URL's
    $locationProvider.html5Mode(true);

    IdleProvider.idle(15 * 60); // in seconds
    IdleProvider.timeout(30); // in seconds

    //http interceptor to handle 401 and redirect
    var respInterceptor = ["$q", "$location", "$rootScope", function ($q, $location, $rootScope) {
        function success(response) {
            //console.log(JSON.stringify(response));
            if (response.data.status === "error") {
                $rootScope.notificationMsg.push({
                    msg: response.data.msg,
                    time: new Date().getTime() / 1000
                });
            }
            return response;
        }

        function error(response) {
            if (response.status === 401) {
                $rootScope.errorMsg = '';
                $rootScope.credentials = null;
                $rootScope.openModalPopupClose();
                $location.path("/login");
                return $q.reject(response);
            }
            // otherwise, default behaviour
            //return $q.reject(response);
            return response;
        }

        return function (promise) {
            return promise.then(success, error);
        }
    }];

    $httpProvider.responseInterceptors.push(respInterceptor);
}]);

app.run(["$rootScope", "$http", "$location", "$modalStack", "$timeout", "$log", "$modal", '$window', "$templateCache", "Idle", "UsersService", "BusinessService", "ReconAssignmentService", "ReconLicenseService", "AuditsService", "NoAuthService", "angularCryptoSerivces",
    function ($rootScope, $http, $location, $modalStack, $timeout, $log, $modal, $window, $templateCache, Idle, UsersService, BusinessService, ReconAssignmentService, ReconLicenseService, AuditsService, NoAuthService, angularCryptoSerivces) {

        // Init acl based roles form json
        $http.get('app/json/role_mapping.json').success(function (data) {
            $rootScope.role_mapping = data['roles']
            $rootScope.cipherText = data['cipherText']


            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $rootScope.msgNotify = function (type, msg) {
                if (type == 'error') {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "0",
                        "hideDuration": "0",
                        "timeOut": "0",
                        "extendedTimeOut": "0",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    msg = msg + '<br /><br /><button type="button" class="btn clear">Ok</button>';
                } else {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                }
                toastr[type](msg);
            }
            $rootScope.allocated_recons = []
            Idle.watch();

            $rootScope.$on('IdleTimeout', function () {
                $modalStack.dismissAll('cancle');
                $rootScope.doLogout();
            });
            $rootScope.reset = function () {
                Idle.watch();
            }
            $rootScope.class = 'rightContent'
            $rootScope.classFlag = false
            $rootScope.removeClass = function () {
                if ($rootScope.classFlag && $rootScope.credentials != null) {
                    $rootScope.class = ''
                }
                else {
                    if ($rootScope.credentials != null) {
                        $rootScope.class = 'rightContent'
                    }
                }
            }
            var path = function () {
                return $location.path();
            };
            $rootScope.$watch(path, function (newVal, oldVal) {
                if (newVal.lastIndexOf("/")) {
                    newVal = newVal.substring(0, newVal.lastIndexOf("/"));
                }
                $rootScope.activetab = newVal;
            });
            $rootScope.notificationMsg = [];

            function init() {
                $rootScope.isauthenticated = false;
                $rootScope.credentials = {};
                $rootScope.notificationMsg = [];
                $rootScope.appData = {};
                $rootScope.user = {};
                $rootScope.user.role = "admin";
                $rootScope.curState = {};
                $rootScope.companies = false;
                $rootScope.administration = false;
                $rootScope.jobs = false;
                $rootScope.billingprofile = false;
                $rootScope.customeLoadingMsg = "";
                $rootScope.config = {};
                $rootScope.config.title = "";
            }

            function getInternetExplorerVersion() {
                var rv = -1;
                if (navigator.appName == 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{x`0,})");
                    if (re.exec(ua) != null)
                        rv = parseFloat(RegExp.$1);
                }
                else if (navigator.appName == 'Netscape') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
                    if (re.exec(ua) != null)
                        rv = parseFloat(RegExp.$1);
                }
                return rv;
            }

            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    var IEVersion = getInternetExplorerVersion();
                    console.log(IEVersion)
                    if (IEVersion == 11) {
                        $rootScope.handleErrorMessages({"msg": "ERECON does not support Internet Explorer 11 browser."});
                    }
                    ;
                    if (toState.name.indexOf('dashboard') == 0) {
                        angular.forEach($rootScope.curState, function (v, k) {
                            $rootScope.curState[k] = false;
                        });
                        var states = toState.name.split('.');
                        if (states.length >= 2) {
                            $rootScope.curState[states[1]] = true;
                        }
                    }
                });

            $rootScope.loadingModal = null;
            $rootScope.openModalPopupOpen = function (test) {
                if ($rootScope.loadingModal == null)
                    $rootScope.loadingModal = $modal.open({
                        templateUrl: 'modalLoadingView.html',
                        windowClass: 'modal loaderModalPopup',
                        backdrop: 'static'
                    });
            };
            $rootScope.openModalPopupClose = function () {
                if ($rootScope.loadingModal != null) {
                    $rootScope.customeLoadingMsg = '';
                    $rootScope.loadingModal.close("");
                    $rootScope.loadingModal = null;
                }
            };
            $rootScope.getAllBusiness = function () {
                $rootScope.openModalPopupOpen()
                BusinessService.get(undefined, undefined, function (data) {
                    $rootScope.openModalPopupClose()
                    $rootScope.allBusinessData = data.data
                    localStorage.setItem('allBusinessData', JSON.stringify($rootScope.allBusinessData))
                })
            }
            $rootScope.notificationMsg = [];
            $rootScope.errorMsg = '';
            //$rootScope.credentials = null;
            $rootScope.loading = false;
            $rootScope.ForgotPassword = function (credentials) {
                $rootScope.openModalPopupOpen()
                NoAuthService.ForgotPasswordService('dummy', credentials, function (data) {
                    if (data == 'Success') {
                        $rootScope.showSuccessMsg("Password has been sent to Registered Email Id")
                    }
                    else {
                        $rootScope.showErrormsg(data)
                    }
                    $rootScope.openModalPopupClose()
                })
            }

            //Register login/logout function at RootScope so that its available for the entire application
            $rootScope.doLogin = function (credentials) {
                //credentials['userName'] = credentials['userName'].toLowerCase()
                $rootScope.errorMsg = '';
                $rootScope.encryptcredentials = angular.copy(credentials)
                $rootScope.encryptcredentials = angular.copy(angularCryptoSerivces.encryptCredentials($rootScope.cipherText['AES_KEY'],
                    $rootScope.cipherText['IV'], $rootScope.encryptcredentials))

                $http.post("/api", $rootScope.encryptcredentials, {headers: {"Content-Type": "application/json"}}).success(function (data, status) {
                    if (data.status === "error") {
                        $rootScope.errorMsg = data.msg;
                        $rootScope.isauthenticated = false
                    }
                    else {
                        $rootScope.reset();

                        $rootScope.classFlag = false
                        $rootScope.isauthenticated = true
                        $rootScope.credentials = data.msg;
                        $rootScope.eventDoc = {
                            'type': 'User Logged In',
                            'actionOn': $rootScope.credentials.userName,
                            'createDateTime': Date.now(),
                            'createdBy': $rootScope.credentials.userName
                        }
                        AuditsService.post($rootScope.eventDoc)
                        ReconLicenseService.get(undefined, {'query': {'perColConditions': {'licensed': 'Non-Licensed'}}}, function (data) {
                            console.log(data)
                            if (data['total'] > 0) {
                                $rootScope.credentials.unlicensed_recon_exists = true
                            }
                            //$rootScope.openModalPopupClose()

                        })
                        $rootScope.allocated_recons = []
                        var perColConditions = {};
                        console.log($rootScope.credentials)
                        $rootScope.getAllBusiness()
                        $rootScope.openModalPopupOpen()
                        perColConditions["user"] = $rootScope.credentials.userName
                        // update user details to local store for later usage
                        localStorage.setItem('userName', JSON.stringify($rootScope.credentials.userName))
                        localStorage.setItem('userRole', $rootScope.credentials.role)

                        ReconAssignmentService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                            $rootScope.business_context_data = []
                            $rootScope.work_pool_name_data = []
                            //console.log(data)
                            if (data.data.length > 0) {
                                $rootScope.business_context_data = []
                                $rootScope.businessArrayCheck = []
                                angular.forEach(data.data, function (ab, cd) {
                                    console.log(ab.businessContext)
                                    $rootScope.business_context_data.push(ab.businessContext)
                                    $rootScope.work_pool_name_data.push(ab.workPoolName)

                                })
                                angular.forEach(data.data, function (v, k) {
                                    angular.forEach([].concat.apply([], v.recons.split(',')), function (val, key) {
                                        $rootScope.allocated_recons.push({
                                            'business': v.businessContext,
                                            'recon': val,
                                            'userpool': v.userpool
                                        })
                                    })
                                })
                            }

                            BusinessService.get(undefined, undefined, function (data) {
                                $rootScope.openModalPopupClose()
                                $rootScope.allBusinessData = data.data
                                localStorage.setItem('allBusinessData', JSON.stringify($rootScope.allBusinessData))
                                localStorage.setItem('allocated_recons', JSON.stringify($rootScope.allocated_recons))
                                localStorage.setItem('business_context_data', JSON.stringify($rootScope.business_context_data))
                                localStorage.setItem('work_pool_name_data', JSON.stringify($rootScope.work_pool_name_data))

                                // initialize view base on user role mapping json
                                if (angular.isDefined($rootScope.role_mapping[$rootScope.credentials.role]['default'])) {
                                    $location.path($rootScope.role_mapping[$rootScope.credentials.role]['default'])
                                    $rootScope.userpoolRole = $rootScope.credentials.role
                                }
                                else {
                                    console.log("Default Path not set for role " + $rootScope.credentials.role)
                                    $rootScope.doLogout()
                                }
                            })
                        })
                    }
                })
            };
            $rootScope.doLogout = function () {
                $rootScope.classFlag = true
                //$rootScope.openModalPopupOpen()
                $rootScope.errorMsg = '';
                $rootScope.eventDoc = {
                    'type': 'User Logged Out',
                    'actionOn': $rootScope.credentials.userName,
                    'createDateTime': Date.now(),
                    'createdBy': $rootScope.credentials.userName
                }
                // clear local history variable
                localStorage.clear();

                AuditsService.post($rootScope.eventDoc, function (data) {
                    $rootScope.credentials = null;

                    $http.post("/api/logout", {}, {headers: {"Content-Type": "application/json"}});
                })

                //$rootScope.openModalPopupClose()
            };
            $rootScope.showsub = false
            $rootScope.checkSession = function () {
                $http.get("/api").error(function () {
                    $location.path("/login");
                    $rootScope.loading = false;
                    $rootScope.classFlag = true
                }).success(function (data) {
                    if (data.status === "error") {
                        $rootScope.errorMsg = data.msg;
                        $rootScope.isauthenticated = false
                        $rootScope.classFlag = true
                    }
                    else {
                        console.log($location.path())
                        //console.log($rootScope.reconID)
                        $rootScope.isauthenticated = true
                        $rootScope.classFlag = false
                        $rootScope.credentials = data.msg;
                        localStorage.setItem('userRole', $rootScope.credentials.role)
                        ReconLicenseService.get(undefined, {'query': {'perColConditions': {'licensed': 'Non-Licensed'}}}, function (data) {
                            console.log(data)
                            if (data['total'] > 0) {
                                $rootScope.credentials.unlicensed_recon_exists = true
                            }
                            //$rootScope.openModalPopupClose()

                        })

                        $rootScope.allocated_recons = []
                        var perColConditions = {};
                        console.log($rootScope.credentials)
                        $rootScope.openModalPopupOpen()
                        perColConditions["user"] = $rootScope.credentials.userName
                        ReconAssignmentService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                            $rootScope.business_context_data = []
                            if (data.data.length > 0) {
                                $rootScope.business_context_data = []
                                $rootScope.work_pool_name_data = []
                                $rootScope.businessArrayCheck = []
                                angular.forEach(data.data, function (ab, cd) {
                                    console.log(ab.businessContext)
                                    $rootScope.business_context_data.push(ab.businessContext)
                                    if (Object.keys(ab).indexOf('workPoolName')!=-1){
                                        $rootScope.work_pool_name_data.push(ab.workPoolName)
                                    }
                                })
                                angular.forEach(data.data, function (v, k) {
                                    angular.forEach([].concat.apply([], v.recons.split(',')), function (val, key) {
                                        $rootScope.allocated_recons.push({
                                            'business': v.businessContext,
                                            'recon': val,
                                            'userpool': v.userpool
                                        })
                                    })
                                })
                            }

                            BusinessService.get(undefined, undefined, function (data) {
                                $rootScope.openModalPopupClose()
                                $rootScope.allBusinessData = data.data
                                localStorage.setItem('allBusinessData', JSON.stringify($rootScope.allBusinessData))
                                localStorage.setItem('allocated_recons', JSON.stringify($rootScope.allocated_recons))
                                localStorage.setItem('business_context_data', JSON.stringify($rootScope.business_context_data))
                                localStorage.setItem('work_pool_name_data', JSON.stringify($rootScope.work_pool_name_data))

                                // initialize view base on user role mapping json
                                if (angular.isDefined($rootScope.role_mapping[$rootScope.credentials.role]['default'])) {
                                    $location.path($rootScope.role_mapping[$rootScope.credentials.role]['default'])
                                    $rootScope.userpoolRole = $rootScope.credentials.role
                                }
                                else {
                                    console.log("Default Path not set for role " + $rootScope.credentials.role)
                                    $rootScope.doLogout()
                                }
                            })
                        })
                    }

                });
            }

            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

            $rootScope.showMsg = true
            $rootScope.analyze = function (value) {
                if (strongRegex.test(value)) {
                    $rootScope.showMsg = true
                } else if (mediumRegex.test(value)) {
                    $rootScope.showMsg = false
                } else {
                    $rootScope.showMsg = false
                }
            };
            $rootScope.$watch('credentials', function (newvalue, oldvalue) {
                return $rootScope.credentials = newvalue
            })

            /*      $rootScope.redirectCheck = function () {
             $rootScope.fissionServer="";
             StorageService.isInitialized("dummy", function (data) {
             if (data == "Not Initialized") {
             $location.path("/storage");
             }
             });
             }*/

            function clearNotifications() {
                var currTime = new Date().getTime() / 1000;
                var count = $rootScope.notificationMsg.length;
                while (count--) {
                    if (currTime - $rootScope.notificationMsg[count].time >= 2) {
                        $rootScope.notificationMsg.splice(count, 1);
                    }
                }
                $timeout(clearNotifications, 10000);
            }

            $timeout(clearNotifications, 10000);

            /*        function clearNotifications() {
             var currTime = new Date().getTime() / 1000;
             var count = $rootScope.notificationMsg.length;
             while (count--) {
             if (currTime - $rootScope.notificationMsg[count].time >= 2) {
             $rootScope.notificationMsg.splice(count, 1);
             }
             }
             $timeout(clearNotifications, 2000);
             }
             $timeout(clearNotifications, 2000);*/
            angular.element(document.body).bind('click', function (e) {
                var popups = document.querySelectorAll('*[popover]');
                if (popups) {
                    for (var i = 0; i < popups.length; i++) {
                        var popup = popups[i];
                        var popupElement = angular.element(popup);

                        var content;
                        var arrow;
                        if (popupElement.next()) {
                            content = popupElement.next()[0].querySelector('.popover-content');
                            arrow = popupElement.next()[0].querySelector('.arrow');
                        }
                        if (popup != e.target && e.target != content && e.target != arrow) {
                            if (popupElement.next().hasClass('popover')) {
                                popupElement.next().remove();
                                popupElement.scope().tt_isOpen = false;
                            }
                        }
                    }
                }
            });
            init();
            $rootScope.checkSession();
            $rootScope.getAllBusiness()
            $rootScope.openModalPopupAddDiskOpen = function () {
                if ($rootScope.loadingModal == null)
                    $rootScope.loadingModal = $modal.open({
                        templateUrl: 'modalAddingDiskView.html',
                        windowClass: 'modal loaderModalPopup',
                        backdrop: 'static'
                    });
            };
            $rootScope.openModalPopupAddDiskClose = function () {
                if ($rootScope.loadingModal != null) {
                    $rootScope.loadingModal.close("");
                    $rootScope.loadingModal = null;
                }
            };
            $rootScope.openModalPopupCreateDiskOpen = function () {
                if ($rootScope.loadingModal == null)
                    $rootScope.loadingModal = $modal.open({
                        templateUrl: 'modalCreatingDiskView.html',
                        windowClass: 'modal loaderModalPopup',
                        backdrop: 'static'
                    });
            };
            $rootScope.openModalPopupCreateDiskClose = function () {
                if ($rootScope.loadingModal != null) {
                    $rootScope.loadingModal.close("");
                    $rootScope.loadingModal = null;
                }
            };

            $rootScope.formValidation = {"isNotValid": false, "message": "", "type": ""};
            $rootScope.showAlert = function (msg, show, type) {
                $rootScope.formValidation.isNotValid = show;
                $rootScope.formValidation.message = msg;
                $rootScope.formValidation.type = type;
            };

            $rootScope.handleErrorMessages = function (data) {
                //$rootScope.openModalPopupClose();
                $rootScope.isDisable = false;
                $rootScope.customeLoadingMsg = "";
                $rootScope.showAlert(data.msg, true, "alert-danger");
            };
            $rootScope.toUTCDate = function (date) {
                var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                return _utc;
            };

            $rootScope.millisToUTCDate = function (millis) {
                return $rootScope.toUTCDate(new Date(millis));
            };

            $rootScope.showErrormsg = function (msg) {
                $rootScope.customeLoadingMsg = "";
                $modal.open({
                    templateUrl: 'showErrormsg.html',
                    windowClass: 'modal alert alert-danger',
                    backdrop: 'static',
                    controller: function ($rootScope, $modalInstance) {
                        $rootScope.bindMsg = msg;
                        $rootScope.submit = function (data) {
                            $modalInstance.dismiss('cancel');
                        };
                        $rootScope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            };

            $rootScope.showSuccessMsg = function (msg) {
                $modal.open({
                    templateUrl: 'showSuccessMsg.html',
                    windowClass: 'modal alert alert-success',
                    backdrop: 'static',
                    controller: function ($rootScope, $modalInstance) {
                        $rootScope.bindMsg = msg;
                        $rootScope.submit = function (data) {
                            $modalInstance.dismiss('cancel');
                        };
                        $rootScope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        //$timeout(function () {
                        //    $modalInstance.dismiss('cancel');
                        //}, 5000)
                    }
                });
            };

            $rootScope.showAlertMsg = function (msg) {
                $modal.open({
                    templateUrl: 'showAlertMsg.html',
                    windowClass: 'modal alert alert-success',
                    backdrop: 'static',
                    controller: function ($rootScope, $modalInstance) {
                        $rootScope.bindMsg = msg;
                        $rootScope.submit = function (data) {
                            $modalInstance.dismiss('cancel');
                        };
                        $rootScope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
//                    $timeout(function () {
//                        $modalInstance.dismiss('cancel');
//                    }, 5000)
                    }
                });
            };

            $rootScope.changePass = function () {
                $modal.open({
                    templateUrl: 'showPassword.html',
                    windowClass: 'modal addUser',
                    backdrop: 'static',
                    controller: function ($rootScope, $modalInstance, UsersService) {
                        $rootScope.userData = {}
                        console.log($rootScope.credentials['_id'])
                        $rootScope.saveUser = function (data) {
                            console.log(data)
                            $rootScope.updateUserCredentials = {}

                            $rootScope.updateUserCredentials['userName'] = $rootScope.credentials['userName']
                            $rootScope.updateUserCredentials['oldpassword']  = $rootScope.userData['oldpassword']
                            $rootScope.updateUserCredentials['password']  = $rootScope.userData['password']

                            $rootScope.encryptcredentials = angular.copy(angularCryptoSerivces.encryptCredentials($rootScope.cipherText['AES_KEY'],
                                $rootScope.cipherText['IV'], $rootScope.updateUserCredentials))

                            $rootScope.openModalPopupOpen();
                            UsersService.updatePassword($rootScope.credentials['_id'], $rootScope.encryptcredentials, function (data) {
                                console.log(data)
                                $rootScope.openModalPopupClose();
                                if (data['userName'] == $rootScope.credentials['userName']) {
                                    $rootScope.msgNotify('success', 'Successfully updated');
                                    $timeout(function () {
                                        $rootScope.cancel();
                                    }, 3000);
                                }
                            }, function (err) {
                                $rootScope.isDisable = false;
                                $rootScope.msgNotify('warning', err.msg);
                                $rootScope.openModalPopupClose();
                            })
                        };
                        $rootScope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            };

            $rootScope.clock = "loading clock...";
            $rootScope.tickInterval = 1000

            var tick = function () {
                $rootScope.clock = Date.now();
                $timeout(tick, $rootScope.tickInterval);
            }
            $timeout(tick, $rootScope.tickInterval);
        })

    }]);


