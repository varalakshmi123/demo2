/*i*
 * Created by ubuntu on 24/11/16.
 */

function gstReportController($scope, $rootScope, $http, $filter, CustomReportService, ReportInterfaceService, $window, $timeout, $modal, ReconAssignmentService, GstReportService, fileUploadService, GstMetaInfoService, GSTSourceProductGroupService) {

    $scope.reportDetails = {}
    $scope.selReportData = {}
    $scope.csvExtractName = ''
    $scope.reportHeaders = []
    $scope.filename = "dummfile"
    $scope.separator = ","
    $scope.nostroReportExtract = {}
    $scope.TodaynostroReportExtract = {}

    $scope.reportLocMap = {}
    $scope.reportDwnType = 'CSV'
    $scope.enableNostroReport = false

    $scope.selGstReport = {}
    $scope.gstReportType = ["Source and State wise Daily Report", "YTD Total GST Report", "Daily GST Report", "YTD GST Report"]
    $scope.groupByGstReport = ["OGL GST Trail Balance", "State Wise Balance Daily Report", "State Wise Year Balances Report",
        "System Wise Balance Daily Report", "System Wise Year Balances Report"]

    $scope.exportCSVReport = function (exportData) {
        $scope.csvReportData = [];
        $scope.csvHeader = ''
        $scope.csvHeader = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        $scope.csvReportData.push({
            "GL_ACC_NUM": "GL Account Number",
            "GL_NAME": "GL Name",
            "CLOS_BALS1": "Closing Balance of " + $scope.source1,
            "CLOS_BALS2": "Closing Balance of " + $scope.source2,
            "DIFF": "Difference b/w " + $scope.source1 + " - " + $scope.source2,
            "VALUE_GRE": "Value Greater in " + $scope.source1 + '/' + $scope.source2
        })
        $scope.filename = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        for (i = 0; i < exportData.length; i++) {
            $scope.csvReportData.push({
                "GL_ACC_NUM": exportData[i]['C40'],
                "GL_NAME": exportData[i]['ACCOUNT_NAME'],
                "CLOS_BALS1": $filter('number')(exportData[i]['CLOSING_BALS1'], 2),
                "CLOS_BALS2": $filter('number')(exportData[i]['CLOSING_BALS2'], 2),
                "DIFF": $filter('number')(exportData[i]['DIFF'], 2),
                "VALUE_GRE": exportData[i]['VALUE_GRE']
            })
        }
        //angular.element('#csvReport').triggerHandler('click');
        //$("#csvReport").trigger('click');
        $timeout(function () {
            angular.element("#csvReport").triggerHandler('click');
        }, 0);
        return false;

    }

    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };

    $scope.statementDate = 0
    $scope.statementDate1 = 0
    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.uploadFile = function () {
        var statementDate1 = $scope.toEpoch($scope.statementDate1)
        statementDate1 = $filter('date')(statementDate1 * 1000, 'dd/MM/yy')
        var searchParam = {}

        searchParam['statementDate'] = statementDate1
        var file = $scope.myFile;
        $scope.name = $scope.myFile.name;
        console.log($scope.name)
        var uploadUrl = "/api/no_auth/dummy/upload"; //Url of webservice/api/server

        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
            $scope.post = data.msg;
            $rootScope.openModalPopupClose()
            $rootScope.showSuccessMsg("File Uploaded Successfully")
            console.log($scope.post)
        }).catch(function () {
            $scope.error = 'unable to get posts';
        });
    };

    $scope.initializeGSTReport = function () {
        $scope.clearGSTData()
        $scope.gstReportDate = ''
        $scope.selGstReport = ''
    }

    $scope.generateGstReport = function () {
        var stmtDate = $filter('date')($scope.toEpoch($scope.gstReportDate) * 1000, 'yyyyMMdd')
        $scope.rawData = []
        $scope.sourcewiseGst = []

        $rootScope.openModalPopupOpen()
        if ($scope.groupByGstReport.indexOf($scope.selGstReport) != -1) {
            // source wise data else

            ReportInterfaceService.generateGstReport('dummy',
                {"statementDate": stmtDate, "gstType": $scope.selGstReport}, function (data) {
                    $scope.sourcewiseGst = data
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.openModalPopupClose()
                    $rootScope.showAlertMsg(err.msg)
                    console.log(err)
                })
        } else if ($scope.gstReportType.indexOf($scope.selGstReport) !== -1) {
            ReportInterfaceService.generateGstReport('dummy',
                {"statementDate": stmtDate, "gstType": $scope.selGstReport}, function (data) {
                    $rootScope.openModalPopupClose()
                    $scope.rawData = data
                    $scope.subcolumns = [{'name': ''}];
                    $scope.gstHeaders = [{"name": "State Wise", "subcolumns": [{"name": ""}]}];
                    $scope.sources = _.groupBy($scope.rawData, 'SOURCE');
                    $scope.sourceHeaders = [];

                    // sort sources by OGL
                    var sortOgl = {}
                    var sortOthers = {}
                    angular.forEach($scope.sources, function (a, b) {
                        if (b == 'OGL' || b == 'IDFC Bank Total') {
                            sortOgl[b] = a
                        } else {
                            sortOthers[b] = a
                        }
                    })
                    $scope.sources = angular.extend(sortOgl, sortOthers)
                    // sort sources ends

                    if (angular.isDefined($scope.sources.OGL)) {
                        $scope.oglData = angular.copy($scope.sources.OGL);
                    }
                    //delete $scope.sources.OGL;
                    angular.forEach($scope.sources, function (a, b) {
                        var obj = {
                            'name': b,
                            'subcolumns': [{"name": "No of Trnx"}, {"name": "Charge Value"}, {"name": "Tax Value"}]
                        };
                        angular.forEach(obj.subcolumns, function (c, d) {
                            $scope.subcolumns.push({'name': c.name})
                        })
                        $scope.gstHeaders.push(obj);
                        $scope.sourceHeaders.push(obj);

                        var singleobj = {
                            'headers': [{"name": "State Wise", "subcolumns": [{"name": ""}]},
                                {"name": b, "subcolumns": [{"name": "No of Trnx"}, {"name": "Tax Value"}]},
                                {
                                    "name": "OGL",
                                    "subcolumns": [{"name": "No of Trnx"}, {"name": "Tax Value"}, {"name": "Difference"}]
                                },
                            ], 'subcols': [], 'datas': []
                        }

                        angular.forEach(singleobj.headers, function (c, d) {
                            angular.forEach(c.subcolumns, function (i, j) {
                                singleobj.subcols.push({'name': i.name})
                            })
                        });

                        angular.forEach(a, function (y, z) {
                            if (a.SOURCE == b) {
                                singleobj.datas.push({'value': a['STATE NAME']});
                                singleobj.datas.push({'value': a['TRANSACTION COUNT']});
                                singleobj.datas.push({'value': a['TAX']});
                            }
                            angular.forEach($scope.sources['OGL'], function (w, x) {
                                if (a.SOURCE == w.MATCHSOURCE) {
                                    singleobj.datas.push({'value': w['TRANSACTION COUNT']});
                                    singleobj.datas.push({'value': w['TAX']});
                                    singleobj.datas.push({'value': Number(singleobj.datas[2]) + Number(w['TAX'])});
                                }
                            })
                        })
                        console.log('===========')
                        console.log(singleobj)
                        console.log('===========')
                    });

                    $scope.stateGroup = _.groupBy($scope.rawData, 'STATE NAME');
                    $scope.rowData = [];
                    $scope.totalData = [];
                    angular.forEach($scope.stateGroup, function (a, b) {
                        var obj = [{'value': b}];
                        var tmpSources = _.groupBy(a, 'SOURCE');
                        angular.forEach($scope.sourceHeaders, function (c, d) {
                            if (angular.isDefined(tmpSources[c.name])) {
                                if (c.name == 'OGL') {
                                    var tax = 0;
                                    var tsc = 0;
                                    angular.forEach(tmpSources[c.name], function (e, f) {
                                        tsc += e['TRANSACTION COUNT'];
                                        tax += e['TAX'];
                                    })
                                    obj.push({'value': tsc});
                                    obj.push({'value': ''});
                                    obj.push({'value': tax});
                                } else {
                                    obj.push({'value': tmpSources[c.name][0]['TRANSACTION COUNT']});
                                    obj.push({'value': tmpSources[c.name][0]['CHARGE']});
                                    obj.push({'value': tmpSources[c.name][0]['TAX']});
                                }
                            } else {
                                obj.push({'value': ''});
                                obj.push({'value': ''});
                                obj.push({'value': ''});
                            }
                        })

                        console.log(obj)

                        var computeTotal = angular.copy(obj)
                        if ($scope.totalData.length == 0) {
                            angular.forEach(computeTotal, function (v, k) {
                                computeTotal[0]['value'] = 'Total'
                                if (k > 0) {
                                    computeTotal[k]['value'] = $scope.convertFloat(v['value'])
                                }
                            })
                            $scope.totalData = computeTotal
                        } else {
                            angular.forEach(computeTotal, function (v, k) {
                                if (k > 0) {
                                    $scope.totalData[k]['value'] += $scope.convertFloat(v['value'])
                                }
                            })
                        }
                        $scope.rowData.push(obj);
                    });

                    console.log($scope.rowData)
                    console.log("====== Total Data ======")
                    console.log($scope.totalData)
                }, function (err) {
                    $rootScope.openModalPopupClose()
                    $rootScope.showAlertMsg(err.msg)
                    console.log(err)
                })
        }
    }


    $scope.resetNostroDetails = function () {
        $scope.initializeNostoReport(true)
        $scope.generateNostroReport()
    }

    $scope.convertFloat = function (number) {
        number = parseFloat(number)
        return isNaN(number) ? 0.0 : number
    }

    $scope.clearGSTData = function () {
        $scope.sourcewiseGst = []
        $scope.rowData = []
        $scope.totalData = []
    }

    $scope.freezeSource = function () {
        $modal.open({
            templateUrl: 'freezeSource.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                // load all GST sources
                $rootScope.openModalPopupOpen()
                GSTSourceProductGroupService.load_allocated_sources('dummy', function (data) {
                    var unique_src = []
                    $scope.gstSources = []
                    angular.forEach(data.data, function (val, key) {
                        if (unique_src.indexOf(val.sourceName) == -1) {
                            $scope.gstSources.push({
                                "name": val.sourceName,
                                "id": val.sourceCode,
                                'ticked': false
                            })
                            unique_src.push(val.sourceName)
                        }
                    })
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.showAlertMsg(err.msg)
                    $rootScope.openModalPopupClose()
                })

                $scope.freezeSources = function (freezeSrc) {
                    var srcList = []
                    angular.forEach(freezeSrc['selectedSources'], function (v, k) {
                        srcList.push({"sourceName": v['name'], "sourceCode": v['id']})
                    })

                    GstMetaInfoService.freeze_sources('dummy', {
                        'statementdate': $filter('date')($scope.toEpoch(freezeSrc['freezeStmtDate']) * 1000, 'yyyyMMdd'),
                        'freezed': true, 'sources': srcList
                    }, function (data) {
                        $rootScope.showSuccessMsg('GST sources frozen successfully.')
                        $modalInstance.dismiss('cancel');
                    }, function (err) {
                        $modalInstance.dismiss('cancel');
                        $rootScope.showAlertMsg(err.msg)
                    })
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        })
    }

    $scope.rollbackSources = function () {
        $modal.open({
            templateUrl: 'rollback.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                // load all GST sources
                //TODO load only allocated sources only
                $rootScope.openModalPopupOpen()
                GSTSourceProductGroupService.load_allocated_sources('dummy', function (data) {
                    var unique_src = []
                    $scope.gstSources = []
                    angular.forEach(data.data, function (val, key) {
                        if (unique_src.indexOf(val.sourceName) == -1) {
                            $scope.gstSources.push({
                                "name": val.sourceName,
                                "id": val.sourceCode,
                                'ticked': false
                            })
                            unique_src.push(val.sourceName)
                        }
                    })
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.showAlertMsg(err.msg)
                    $rootScope.openModalPopupClose()
                })

                $scope.rollbackData = function (rollback) {
                    var srcList = []
                    angular.forEach(rollback['rollbackSrc'], function (v, k) {
                        srcList.push({'sourceName': v['name'], 'sourceCode': v['id']})
                    })

                    GstMetaInfoService.rollback_sources('dummy', {
                        'statementdate': $filter('date')($scope.toEpoch(rollback['statementdate']) * 1000, 'yyyyMMdd'),
                        'sources': srcList
                    }, function (data) {
                        $rootScope.showSuccessMsg("Rollback successful")
                        $modalInstance.dismiss('cancel');
                    }, function (err) {
                        $modalInstance.dismiss('cancel');
                        $rootScope.showAlertMsg(err.msg)
                    })
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    // export gst report
    $scope.exportExcelReport = function (reportId) {
        // $("#mhada_table").table2excel({exclude: ".noExl",filename: "MHADA-REPORT -" +$filter('date')(new Date(Date.now()), 'dd/MM/yyyy')});
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var postfix = day + "-" + month + "-" + year;

        var tab_text = "<table border='3px'>";
        var textRange;
        var j = 0;
        tab = document.getElementById(reportId); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }
        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input     params

        var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'>";
        excelFile += "<head>";
        excelFile += "<!--[if gte mso 9]>";
        excelFile += "<xml>";
        excelFile += "<x:ExcelWorkbook>";
        excelFile += "<x:ExcelWorksheets>";
        excelFile += "<x:ExcelWorksheet>";
        excelFile += "<x:Name>";
        excelFile += "{worksheet}";
        excelFile += "</x:Name>";
        excelFile += "<x:WorksheetOptions>";
        excelFile += "<x:DisplayGridlines/>";
        excelFile += "</x:WorksheetOptions>";
        excelFile += "</x:ExcelWorksheet>";
        excelFile += "</x:ExcelWorksheets>";
        excelFile += "</x:ExcelWorkbook>";
        excelFile += "</xml>";
        excelFile += "<![endif]-->";
        excelFile += "</head>";
        excelFile += "<body>";
        excelFile += tab_text;
        excelFile += "</body>";
        excelFile += "</html>";

        //getting data from our div that contains the HTML table
        var data_type = 'data:application/vnd.ms-excel';
        a = document.createElement("a");
        a.download = ($scope.selGstReport || 'GST_Summary_Report') + postfix + '.xls';

        a.href = data_type + ', ' + encodeURIComponent(excelFile);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

}
