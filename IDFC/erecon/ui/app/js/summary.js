/**
 * Created by ubuntu on 22/6/16.
 */
function summaryController($scope, $rootScope, $routeParams, $upload, $filter, $location, $timeout, $route, $window, $modal, UsersService, ReconAssignmentService, BusinessService, UserPoolService, MhadaReportService, AccountPoolService, ReportInterfaceService) {
    $scope.getUserRecons = function () {
        $scope.allocated_recons = []
        var perColConditions = {};
        perColConditions["user"] = $rootScope.credentials.userName
        ReconAssignmentService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            angular.forEach(data.data, function (v, k) {
                angular.forEach([].concat.apply([], v.recons.split(',')), function (val, key) {
                    $scope.allocated_recons.push({
                        'business': v.businessContext,
                        'recon': val,
                        'userpool': v.userpool
                    })
                })
            })
        })
    }
    $scope.getUserRecons()
    $scope.SumgridOptions = {
        columnDefs: [],
        rowData: [],
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        //onRowSelected: $scope.rowSelectedFunc(),
        //onSelectionChanged: $scope.selectionChangedFunc(),
        onRowClicked: function (row) {
            console.log(row)
            if (angular.isDefined(row.data['RECON_ID'])) {
                $scope.reconBusinessData = []
                $rootScope.businessContextId = row.data['BUSINESS_CONTEXT_ID']
                $rootScope.reconID = row.data['RECON_ID']
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = row.data['BUSINESS_CONTEXT_ID']
                $rootScope.openModalPopupOpen()
                localStorage.setItem('summary', JSON.stringify({}));
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $rootScope.openModalPopupClose()
                    console.log(data.data)
                    angular.forEach(data.data, function (val, key) {
                        if (row.data['RECON_ID'] == val['RECON_ID']) {
                            console.log(val)

                            angular.forEach($scope.allocated_recons, function (v, k) {
                                if (v.recon == row.data['RECON_ID']) {
                                    var perColConditions = {};
                                    perColConditions["name"] = v.userpool
                                    UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                        console.log(data)
                                        if (data.data.length > 0) {
                                            $rootScope.userpoolRole = data.data[0]['role']
                                            $rootScope.userpoolName = data.data[0]['name']
                                            console.log($rootScope.userpoolRole)
                                        }
                                        $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                        var myObj = {}
                                        myObj['reconId'] = row.data['RECON_ID']
                                        myObj['reconName'] = row.data['RECON_NAME']
                                        myObj['businessId'] = row.data['BUSINESS_CONTEXT_ID']
                                        myObj['businessProcess'] = $scope.businessProcess
                                        myObj['from'] = 'summary'
                                        myObj['userRole'] = $rootScope.userpoolRole
                                        myObj['userPoolName'] = $rootScope.userpoolName
                                        myObj['workpool'] = val['WORKPOOL_NAME']

                                        console.log(myObj)
                                        localStorage.removeItem('saved')
                                        localStorage.removeItem('suspense')
                                        localStorage.setItem('summary', JSON.stringify(myObj));
                                        $location.path("/recons")
                                        //$window.location.href = 'https://' + $location.host() + '/recons'
                                    })
                                }
                            })
                            //$window.open('https://'+$location.host()+'/recons', '_blank');
                        }
                    })
                })
            }
            var lastSelectedRow = this.appSpecific.lastSelectedRow;
            var shiftKey = row.event.shiftKey,
                ctrlKey = row.event.ctrlKey;

            // If modifier keys aren't pressed then only select the row that was clicked
            if (!shiftKey && !ctrlKey) {
                this.api.deselectAll();
                this.api.selectNode(row.node, true);
            }
            // If modifier keys are used and there was a previously selected row
            else if (lastSelectedRow !== undefined) {
                // Select a block of rows
                if (shiftKey) {
                    var startIndex, endIndex;
                    // Get our start and end indexes correct
                    if (row.rowIndex < lastSelectedRow.rowIndex) {
                        startIndex = row.rowIndex;
                        endIndex = lastSelectedRow.rowIndex;
                    }
                    else {
                        startIndex = lastSelectedRow.rowIndex;
                        endIndex = row.rowIndex;
                    }
                    // Select all the rows between the previously selected row and the
                    // newly clicked row
                    for (var i = startIndex; i <= endIndex; i++) {
                        this.api.selectIndex(i, true, true);
                    }
                }
                // Select one more row
                if (ctrlKey) {
                    this.api.selectIndex(row.rowIndex, true);
                }
            }
            // Store the recently clicked row for future use
            this.appSpecific.lastSelectedRow = row;
        },
        cellClicked: true,
        onCellClicked: function (event) {
            console.log(event)
        },
        enableFilter: true,
        groupSuppressAutoColumn: true,
        groupUseEntireRow: true,
        rememberGroupStateWhenNewData: true,
        //groupHeaders: true,
        //rowHeight: 22,
        //groupSelectsChildren: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        //onModelUpdated: onModelUpdated,
        suppressRowClickSelection: true
    };
    var filterCount = 0;
    $scope.onFilterChangedSum = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.SumgridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }
    $scope.onFilterChangedSus = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.SusgridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }
    $scope.allocated_recons = []
    $scope.SusgridOptions = {
        columnDefs: [],
        rowData: [],
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        //onRowSelected: $scope.rowSelectedFunc(),
        //onSelectionChanged: $scope.selectionChangedFunc(),
        onRowClicked: function (row) {
            //console.log(row)
            if (angular.isDefined(row.data['ACCOUNT_NUMBER'])) {
                $rootScope.businessContextId = '1111131119'
                $rootScope.reconID = row.data['RECON_ID']
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = '1111131119'
                $rootScope.openModalPopupOpen()
                localStorage.setItem('summary', JSON.stringify({}));
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $rootScope.openModalPopupClose()
                    console.log(data.data)
                    angular.forEach(data.data, function (val, key) {
                        if (row.data['RECON_ID'] == val['RECON_ID']) {
                            console.log(val)
                            angular.forEach($scope.allocated_recons, function (v, k) {
                                if (v.recon == row.data['RECON_ID']) {
                                    var perColConditions = {};
                                    perColConditions["name"] = v.userpool
                                    UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                        console.log(data)
                                        if (data.data.length > 0) {
                                            $rootScope.userpoolRole = data.data[0]['role']
                                            $rootScope.userpoolName = data.data[0]['name']
                                            console.log($rootScope.userpoolRole)
                                        }
                                        $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                        var myObj = {}
                                        myObj['reconId'] = row.data['RECON_ID']
                                        myObj['reconName'] = row.data['RECON_NAME']
                                        myObj['businessId'] = '1111131119'
                                        myObj['businessProcess'] = $scope.businessProcess
                                        myObj['from'] = 'suspense'
                                        myObj['account_pool_name'] = row.data['ACCOUNT_POOL_NAME']
                                        myObj['account_number'] = row.data['ACCOUNT_NUMBER']
                                        myObj['userRole'] = $rootScope.userpoolRole
                                        myObj['userPoolName'] = $rootScope.userpoolName
                                        myObj['workpool'] = val['WORKPOOL_NAME']

                                        console.log(myObj)
                                        localStorage.removeItem('saved')
                                        localStorage.removeItem('summary')
                                        localStorage.setItem('suspense', JSON.stringify(myObj));
                                        $location.path("/recons")
                                        //$window.location.href = 'https://'+$location.host()+'/recons'
                                    })
                                }
                            })
                            //$window.open('https://'+$location.host()+'/recons', '_blank');
                        }
                    })
                })
            }

            var lastSelectedRow = this.appSpecific.lastSelectedRow;
            var shiftKey = row.event.shiftKey,
                ctrlKey = row.event.ctrlKey;

            // If modifier keys aren't pressed then only select the row that was clicked
            if (!shiftKey && !ctrlKey) {
                this.api.deselectAll();
                this.api.selectNode(row.node, true);
            }
            // If modifier keys are used and there was a previously selected row
            else if (lastSelectedRow !== undefined) {
                // Select a block of rows
                if (shiftKey) {
                    var startIndex, endIndex;
                    // Get our start and end indexes correct
                    if (row.rowIndex < lastSelectedRow.rowIndex) {
                        startIndex = row.rowIndex;
                        endIndex = lastSelectedRow.rowIndex;
                    }
                    else {
                        startIndex = lastSelectedRow.rowIndex;
                        endIndex = row.rowIndex;
                    }
                    // Select all the rows between the previously selected row and the
                    // newly clicked row
                    for (var i = startIndex; i <= endIndex; i++) {
                        this.api.selectIndex(i, true, true);
                    }
                }
                // Select one more row
                if (ctrlKey) {
                    this.api.selectIndex(row.rowIndex, true);
                }
            }
            // Store the recently clicked row for future use
            this.appSpecific.lastSelectedRow = row;
        },
        enableFilter: true,
        groupSuppressAutoColumn: true,
        groupUseEntireRow: true,
        rememberGroupStateWhenNewData: true,
        //groupHeaders: true,
        //rowHeight: 22,
        //groupSelectsChildren: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        //onModelUpdated: onModelUpdated,
        suppressRowClickSelection: true
    };

    $scope.getsummary = function () {
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.getSummary('dummy', {}, function (data) {
            $scope.adfReportFileName = data['file_name']
            data = data['summary_data']
            if (JSON.parse(data).length == 0) {
                $rootScope.openModalPopupClose()
                $rootScope.showAlertMsg("No data found for this selection")
            }
            if (JSON.parse(data).length > 0) {

                $scope.headers = []
                $scope.columnDefs = []
                angular.forEach(JSON.parse(data), function (v, k) {
                    $scope.headers = Object.keys(v)
                })
                angular.forEach($scope.headers, function (val, key) {
                    if (val == "RECON_NAME") {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'width': 302,
                            'field': val,
                            cellStyle: {color: '#428bca'}
                        })
                    }
                    else if (val == "AUTOMATCHED AMOUNT" || val == "UNMATCHED AMOUNT" || val == "AUTHORIZATION_PENDING AMOUNT" || val == "CREDIT_AUTHORIZATION_PENDING_AMOUNT" || val == "CREDIT_AUTOMATCHED_AMOUNT" || val == "CREDIT_UNMATCHED_AMOUNT" || val == "DEBIT_AUTHORIZATION_PENDING_AMOUNT" || val == "DEBIT_AUTOMATCHED_AMOUNT" || val == "DEBIT_UNMATCHED_AMOUNT") {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'field': val, 'width': 302,
                            'cellStyle': {'text-align': 'right'},
                            'cellRenderer': function (params) {
                                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                    return params.value.toLocaleString()
                                }
                                else {
                                    return params.value
                                }
                            }
                        })
                    }

                    /*else if (val == "EXECUTION_DATE_TIME" || val == "STATEMENT_DATE") {
                     $scope.columnDefs.push({
                     'headerName': val,
                     'suppressAggregation': true,
                     'suppressMenu': true,
                     'field': val, 'width': 302,
                     cellRenderer: function (params) {
                     if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                     if (params.value.toString().length == 13) {
                     if (val == "EXECUTION_DATE_TIME") {
                     var date = $filter('dateTimeFormat')(params.value);
                     }
                     else {
                     var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                     }
                     } else {
                     if (val == "EXECUTION_DATE_TIME") {
                     var date = $filter('dateTimeFormat')(params.value);
                     }
                     else {
                     var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                     }
                     }
                     //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                     return date
                     } else {
                     return "";
                     }
                     }
                     })
                     }*/
                    else {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'field': val,
                            'width': 302
                        })
                    }

                })
                console.log($scope.columnDefs)

                angular.forEach($scope.columnDefs, function (val, key) {
                    if (val.headerName == 'WORKPOOL_NAME') {
                        $scope.workPoolIndex = key
                    }
                })
                $rootScope.openModalPopupClose()
                $scope.reconsData = JSON.parse(data)
                $scope.workpoolObj = $scope.columnDefs[$scope.workPoolIndex]
                $scope.workpoolObj['sort'] = 'asc'
                $scope.columnDefs.splice($scope.workPoolIndex, 1)
                console.log($scope.workpoolObj)
                $scope.columnDefs.unshift($scope.workpoolObj)
                $scope.SumgridOptions.columnDefs = $scope.columnDefs;
                $scope.SumgridOptions.rowData = $scope.reconsData;
                $scope.SumgridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.SumgridOptions.api.setRowData($scope.reconsData)
            }
        })
    }

    $scope.getSuspence = function () {
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.getSuspence('dummy', {}, function (data) {
            if (JSON.parse(data).length == 0) {
                $rootScope.openModalPopupClose()
                $rootScope.showAlertMsg("No data found for this selection")
            }
            if (JSON.parse(data).length > 0) {
                console.log(JSON.parse(data))
                $scope.headers = []
                $scope.columnDefs = []
                angular.forEach(JSON.parse(data), function (v, k) {
                    $scope.headers = Object.keys(v)
                    //console.log(k)
                })
                angular.forEach($scope.headers, function (val, key) {
                    if (val == "ACCOUNT_POOL_NAME") {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'field': val,
                            'width': 302,
                            cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                             if (params.value!=null && params.value!= '' && params.value != undefined) {
                             if (angular.isDefined(params.data) && params.data != null){
                             if (angular.isDefined(params.data['ACCOUNT_NUMBER'])){
                             return '<a href="#" >'+params.value+'</a>'
                             }
                             }
                             }
                             }*/
                        })
                    }
                    else {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'field': val,
                            'width': 302
                        })
                    }

                    // $scope.columnDefs.push({'headerName': val, 'suppressAggregation': true, 'field': val})
                })

                $rootScope.openModalPopupClose()
                $scope.reconsData = JSON.parse(data)
                $scope.SusgridOptions.columnDefs = $scope.columnDefs;
                $scope.SusgridOptions.rowData = $scope.reconsData;
                $scope.SusgridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.SusgridOptions.api.setRowData($scope.reconsData)
            }
        })
    }

    $scope.getBooleanValue = function (cssSelector) {
        //return document.querySelector(cssSelector).checked === true;
    }

    $scope.onBtExportSummary = function () {
        $scope.exportFlag = true
        if ($scope.SumgridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("no data to export")
        }
        var params = {
            skipHeader: $scope.getBooleanValue('#skipHeader'),
            skipFooters: $scope.getBooleanValue('#skipFooters'),
            skipGroups: $scope.getBooleanValue('#skipGroups'),
            allColumns: $scope.getBooleanValue('#allColumns'),
            onlySelected: $scope.getBooleanValue('#onlySelected'),
            processCellCallback: function (params) {
                if (params.value != null) {
                    if (typeof params.value == "number") {
                        if (params.value.toString().length == 13 && params['column']['colId'].indexOf("AMOUNT") == -1) {
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else {
                            return params.value
                        }
                    }
                    else {
                        return params.value
                    }
                }
                //console.log(params.column.getColumnDef())
            },
            fileName: "Summary" + '-' + $filter('date')(new Date(Date.now()), 'dd/MM/yyyy') + '.csv',
            //columnSeparator: document.querySelector('#columnSeparator').value
        };
        if ($scope.getBooleanValue('#useCellCallback')) {
            params.processCellCallback = function (params) {
                if (params.value && params.value.toUpperCase) {
                    return params.value.toUpperCase();
                } else {
                    return params.value;
                }
            };
        }
        if ($scope.getBooleanValue('#customHeader')) {
            params.customHeader = '[[[ This ia s sample custom header - so meta data maybe?? ]]]\n';
        }
        if ($scope.getBooleanValue('#customFooter')) {
            params.customFooter = '[[[ This ia s sample custom footer - maybe a summary line here?? ]]]\n';
        }
        if ($scope.exportFlag) {
            $scope.SumgridOptions.api.exportDataAsCsv(params);
            console.log($scope.SumgridOptions.api.exportDataAsCsv(params))
        }
    }

    $scope.onBtExportSuspence = function () {
        $scope.exportFlag = true
        if ($scope.SusgridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("no data to export")
        }
        var params = {
            skipHeader: $scope.getBooleanValue('#skipHeader'),
            skipFooters: $scope.getBooleanValue('#skipFooters'),
            skipGroups: $scope.getBooleanValue('#skipGroups'),
            allColumns: $scope.getBooleanValue('#allColumns'),
            onlySelected: $scope.getBooleanValue('#onlySelected'),
            processCellCallback: function (params) {
                if (params.value != null) {
                    if (typeof params.value == "number") {
                        if (params.value.toString().length == 13 && params['column']['colId'].indexOf("AMOUNT") == -1) {
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else {
                            return params.value
                        }
                    }
                    else {
                        return params.value
                    }
                }
                //console.log(params.column.getColumnDef())
            },
            fileName: "Suspense" + '-' + $filter('date')(new Date(Date.now()), 'dd/MM/yyyy') + '.csv',
            //columnSeparator: document.querySelector('#columnSeparator').value
        };
        if ($scope.getBooleanValue('#useCellCallback')) {
            params.processCellCallback = function (params) {
                if (params.value && params.value.toUpperCase) {
                    return params.value.toUpperCase();
                } else {
                    return params.value;
                }
            };
        }
        if ($scope.getBooleanValue('#customHeader')) {
            params.customHeader = '[[[ This ia s sample custom header - so meta data maybe?? ]]]\n';
        }
        if ($scope.getBooleanValue('#customFooter')) {
            params.customFooter = '[[[ This ia s sample custom footer - maybe a summary line here?? ]]]\n';
        }
        if ($scope.exportFlag) {
            $scope.SusgridOptions.api.exportDataAsCsv(params);
        }
    }
    //mhada report
    $scope.getMhadaReport = function () {
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.getMhadaReport('dummmy', {}, function (data) {
            $rootScope.openModalPopupClose()
            console.log(data)
            $scope.mhada = data
        })
    }

    $scope.onBtExportMhada = function () {
        // $("#mhada_table").table2excel({exclude: ".noExl",filename: "MHADA-REPORT -" +$filter('date')(new Date(Date.now()), 'dd/MM/yyyy')});
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var postfix = day + "-" + month + "-" + year;

        var tab_text = "<table border='3px'>";
        var textRange;
        var j = 0;
        tab = document.getElementById('mhada_table'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }
        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input     params

        var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'>";
        excelFile += "<head>";
        excelFile += "<!--[if gte mso 9]>";
        excelFile += "<xml>";
        excelFile += "<x:ExcelWorkbook>";
        excelFile += "<x:ExcelWorksheets>";
        excelFile += "<x:ExcelWorksheet>";
        excelFile += "<x:Name>";
        excelFile += "{worksheet}";
        excelFile += "</x:Name>";
        excelFile += "<x:WorksheetOptions>";
        excelFile += "<x:DisplayGridlines/>";
        excelFile += "</x:WorksheetOptions>";
        excelFile += "</x:ExcelWorksheet>";
        excelFile += "</x:ExcelWorksheets>";
        excelFile += "</x:ExcelWorkbook>";
        excelFile += "</xml>";
        excelFile += "<![endif]-->";
        excelFile += "</head>";
        excelFile += "<body>";
        excelFile += tab_text;
        excelFile += "</body>";
        excelFile += "</html>";

        //getting data from our div that contains the HTML table
        var data_type = 'data:application/vnd.ms-excel';
        a = document.createElement("a");
        a.download = 'MHADA_Report_' + postfix + '.xls';

        a.href = data_type + ', ' + encodeURIComponent(excelFile);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }


    $scope.getRBIDADSummary = function () {
        console.log('here...!!!!', $scope.statementDate)

        $scope.rbiSummary = []
        $scope.rbiReportHeaders = []
        $scope.rbiDadTableData = []
        $scope.rbiDataFileName = 'RBI_DAD_SUMMARY_REPORT_'

        var statementDate = $scope.toEpoch($scope.statementDate)
        statementDate = $filter('date')(statementDate * 1000, 'ddMMyyyy')

        $scope.rbiDataFileName += $filter('date')($scope.toEpoch($scope.statementDate) * 1000, 'dd-MM-yyyy')

        ReportInterfaceService.getRbiDadSummaryReport(statementDate, function (response) {
            console.log(response)
            $scope.rbiSummary = response


            //array for report
            $scope.rbiReportHeaders = ['Particulars', 'Amount']

            angular.forEach($scope.rbiSummary, function (outerVal, index) {
                angular.forEach(outerVal['table'], function (value, index) {
                    $scope.rbiDadTableData.push({
                        "Particulars": value["Source"],
                        "Amount": value["Closing_Balance"]
                    })
                })
            })

        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
            console.log(err)
        })
    }


    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };

    $scope.statementDate = 0

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.clearRBIReport = function () {
        $scope.rbiSummary = []
        $scope.statementDate = 0
    }

    $scope.initializeSummary = function () {
        if (angular.isDefined($rootScope.showSuspenseSummary) && $rootScope.showSuspenseSummary) {
            $scope.showSuspense = true
            $scope.getSuspence()
        } else {
            $scope.showSummary = true
            $scope.getsummary()
        }
    }

    $scope.initializeSummary()
}
