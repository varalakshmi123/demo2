/*i*
 * Created by ubuntu on 24/11/16.
 */

function reportsController($scope, $rootScope, $http, $filter, CustomReportService, ReportInterfaceService, $window, $timeout, $modal, ReconAssignmentService, GstReportService, fileUploadService, GstMetaInfoService, GSTSourceProductGroupService) {

    $scope.reportDetails = {}
    $scope.selReportData = {}
    $scope.csvExtractName = ''
    $scope.reportHeaders = []
    $scope.filename = "dummfile"
    $scope.separator = ","
    $scope.nostroReportExtract = {}
    $scope.TodaynostroReportExtract = {}
    $scope.adfOglReports =
        {
            "Novopay V/s OGL Report": "Novopay_OGL",
            "SA_Credit V/s OGL Report": "SA_credit_OGL",
            "SA_Debit V/s OGL Report": "SA_debit_OGL",
            "TD_INR V/s OGL Report": "TD_INR_OGL",
            "TD_GBP V/s OGL Report": "TD_GBP_OGL",
            "TD_USD V/s OGL Report": "TD_USD_OGL",
            "TD_AUD V/s OGL Report": "TD_AUD_OGL",
            "TD_EUR V/s OGL Report": "TD_EUR_OGL"
        }

    $scope.reportLocMap = {}
    $scope.reportDwnType = 'CSV'
    $scope.enableNostroReport = false

    // TODO migrate to mongo DB is required
    $scope.nostroReportBusinessName = ['Treasury']
    $scope.nostroReportName = {}
    $scope.nostroReportName['Treasury'] = ['Nostro Account Closing Balance']
    $scope.multiDropDownStng = {enableSearch: true, scrollableHeight: '230px', scrollable: true}
    $scope.nostroAdvSearch = {'currency': []}
    $scope.nostroCurrencies = []
    $scope.amtOperator = ''
    $scope.selGstReport = {}
    $scope.dealreportDetails = ['DEAL_SPOT_SUMMARY', 'DEAL_SWAP_SUMMARY', 'DEAL_FORWARD_SUMMARY']
    $scope.cycles = {
        'CYCLE 1': 'Cycle 1',
        'CYCLE 2': 'Cycle 2',
        'CYCLE 3': 'Cycle 3',
        'CYCLE 4': 'Cycle 4',
        'CYCLE 5': 'Cycle 5'
    }
    $scope.gstReportType = ["Daily Total GST Report", "YTD Total GST Report", "Daily GST Report", "YTD GST Report"]
    $scope.groupByGstReport = ["OGL GST Trail Balance", "State Wise Balances", "State Wise Year Balances",
        "System Wise Balances", "System Wise Year Balances"]


    $scope.generateReport = function () {

        if (angular.isDefined($scope.selReportData)) {

            var statementDate = $scope.toEpoch($scope.statementDate)
            statementDate = $filter('date')(statementDate * 1000, 'dd/MM/yy')

            var searchParam = {}
            searchParam['statementDate'] = statementDate
            searchParam['functionName'] = $scope.selReportData['functionName']

            console.log(searchParam)
            $scope.source1 = ''
            $scope.source2 = ''
            $scope.extractDate = ''
            $scope.reportExtract = {}

            ReportInterfaceService.generateCustomReports('dummy', searchParam, function (data) {
                if (Object.keys(data).indexOf('fileName') != -1) {
                    var filePath = window.location.origin + '/app/files/recon_reports/'
                    $window.open(filePath + data['fileName'])
                }
                else if (angular.isDefined(data) && data != {} && data != null) {
                    srcDetails = data['sourceDetails']
                    $scope.source1 = srcDetails['S1']
                    $scope.source2 = srcDetails['S2']
                    $scope.extractDate = $filter('date')($scope.toEpoch($scope.statementDate) * 1000, 'dd/MM/yyyy')
                    $scope.reportExtract = JSON.parse(data['reportData'])
                    console.log($scope.reportExtract)
                } else {
                    $rootScope.openModalPopupClose()
                    $rootScope.showAlertMsg("No data found for this selection")
                }

            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
                console.log(err)
            })

            /*ReconAssignmentService.groupColumns('dummy', dataR, function (data) {
             //console.log(data)
             $rootScope.openModalPopupClose()

             if (data == "Success") {
             $scope.data = {}
             $rootScope.showSuccessMsg('Force Match Done Successfully');
             $scope.clearDates()
             $scope.getSelectedRecon($scope.reconId);
             } else {
             $rootScope.showAlertMsg(data);
             }
             }, function (err) {
             $rootScope.showErrormsg(err.msg)
             })*/
        }
    }


    $scope.exportCSVReport = function (exportData) {
        $scope.csvReportData = [];
        $scope.csvHeader = ''
        $scope.csvHeader = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        $scope.csvReportData.push({
            "GL_ACC_NUM": "GL Account Number",
            "GL_NAME": "GL Name",
            "CLOS_BALS1": "Closing Balance of " + $scope.source1,
            "CLOS_BALS2": "Closing Balance of " + $scope.source2,
            "DIFF": "Difference b/w " + $scope.source1 + " - " + $scope.source2,
            "VALUE_GRE": "Value Greater in " + $scope.source1 + '/' + $scope.source2
        })
        $scope.filename = "Reconciliation of " + $scope.source1 + " & " + $scope.source2 + " for the period of " + $scope.extractDate
        for (i = 0; i < exportData.length; i++) {
            $scope.csvReportData.push({
                "GL_ACC_NUM": exportData[i]['C40'],
                "GL_NAME": exportData[i]['ACCOUNT_NAME'],
                "CLOS_BALS1": $filter('number')(exportData[i]['CLOSING_BALS1'], 2),
                "CLOS_BALS2": $filter('number')(exportData[i]['CLOSING_BALS2'], 2),
                "DIFF": $filter('number')(exportData[i]['DIFF'], 2),
                "VALUE_GRE": exportData[i]['VALUE_GRE']
            })
        }
        //angular.element('#csvReport').triggerHandler('click');
        //$("#csvReport").trigger('click');
        $timeout(function () {
            angular.element("#csvReport").triggerHandler('click');
        }, 0);
        return false;

    }

    $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };

    $scope.retrieveCustomReportDef = function () {
        reponse = {}

        $rootScope.openModalPopupOpen()
        CustomReportService.get(undefined, undefined, function (data) {
            if (data['total'] > 0) {
                $scope.reportDetails = data['data']
            }
        }, function (error) {
            console.log(error)
        })
    }

    $scope.statementDate = 0
    $scope.statementDate1 = 0
    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: null,
        maxDate: new Date(),
        startingDay: 1,

    };


    $scope.generateADFOGLReport = function (report) {
        var query = {}
        var statementDate = $scope.toEpoch($scope.statementDate)
        statementDate = $filter('date')(statementDate * 1000, 'dd/MM/yyyy')
        var filePath = window.location.origin + '/app/files/recon_reports/'
        query['report'] = report
        query['stmtDate'] = statementDate
        ReportInterfaceService.generateADFOGLReport('dummy', query, function (data) {
            $window.open(filePath + data)
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.retrieveCustomReportDef()

    $scope.generateNostroReport = function () {
        $rootScope.openModalPopupOpen()
        $scope.nostroReportExtract = {}
        ReportInterfaceService.getNostroReportDetails('dummy', {}, function (data) {
            $rootScope.openModalPopupClose()
            if (data['total'] == 0) {
                $rootScope.showAlertMsg("No data found for this selection")
            } else {
                $scope.reportLocMap = {}
                $scope.nostroReportExtract = JSON.parse(data['reportData'])
                $scope.reportLocMap['CSV'] = data['csv_file_path']
                $scope.reportLocMap['EXCEL'] = data['excel_file_path']
                //$scope.reportLocMap['PDF'] = data['pdf_file_path']
                console.log($scope.nostroReportExtract)
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }
    $scope.getExceptionReport = function () {
        var filePath = window.location.origin + '/app/files/recon_reports/'
        // dataR['accountNumber'] = accountNumber
        $rootScope.openModalPopupOpen()
        ReportInterfaceService.getNostroExceptionReport('dummy', function (data) {
            $rootScope.openModalPopupClose()
            if (data != '' && angular.isDefined(data)) {
                console.log('here')
                console.log(data)
                filePath = filePath + data['fileName']
                $window.open(filePath);
                console.log(filePath, window.location.origin + filePath)
            }
            else {
                $rootScope.showAlertMsg('No data to export')
            }
        }, function (err) {
            $rootScope.showErrormsg(err.msg)
        })

    }


    $scope.getTodayNostroReport = function () {
        var statementDate = $scope.toEpoch($scope.statementDate1)
        statementDate = $filter('date')(statementDate * 1000, 'dd/MM/yy')
        var searchParam = {}

        searchParam['statementDate'] = statementDate
        console.log(searchParam)
        ReportInterfaceService.nostroReport1('dummy', searchParam, function (data) {
            // data = JSON.parse(data)
            $rootScope.openModalPopupClose()
            if (data == undefined) {
                $rootScope.showAlertMsg("No data found for this selection")
            } else {
                console.log(data)
                $scope.reportLocMap = {}
                $scope.TodaynostroReportExtract = data['reportData']
                $scope.reportLocMap['CSV'] = data['csv_file_path']
                $scope.reportLocMap['EXCEL'] = data['excel_file_path']
                //$scope.reportLocMap['PDF'] = data['pdf_file_path']
                console.log($scope.TodaynostroReportExtract)
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.uploadFile = function () {
        var statementDate1 = $scope.toEpoch($scope.statementDate1)
        statementDate1 = $filter('date')(statementDate1 * 1000, 'dd/MM/yy')
        var searchParam = {}

        searchParam['statementDate'] = statementDate1
        var file = $scope.myFile;
        $scope.name = $scope.myFile.name;
        console.log($scope.name)
        var uploadUrl = "/api/no_auth/dummy/upload"; //Url of webservice/api/server

        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
            $scope.post = data.msg;
            $rootScope.openModalPopupClose()
            $rootScope.showSuccessMsg("File Uploaded Successfully")
            console.log($scope.post)
        }).catch(function () {
            $scope.error = 'unable to get posts';
        });
    };


    $scope.searchNostroDetails = function (searchParam) {
        var queryParam = {}
        var validationFlag = false
        console.log(searchParam)
        $scope.nostroReportExtract = {}

        // statement from date and to date are mandatory
        if (angular.isDefined(searchParam['nostroFrmStmtDate']) || angular.isDefined(searchParam['nostroToStmtDate'])) {
            if (!angular.isDefined(searchParam['nostroFrmStmtDate']) || !angular.isDefined(searchParam['nostroToStmtDate'])) {
                validationFlag = true

                $rootScope.showAlertMsg("statment start/end date is mandatory for search")
            }
        }
        if (searchParam['nostroToStmtDate'] < searchParam['nostroFrmStmtDate']) {
            validationFlag = true
            $rootScope.showAlertMsg("statment from date should be greater than to date")
        }
        if (!validationFlag) {
            $rootScope.openModalPopupOpen()
            if (angular.isDefined(searchParam['nostroFrmStmtDate']) && angular.isDefined(searchParam['nostroToStmtDate'])) {
                queryParam['STATEMENT DATE'] = {
                    '$gte': $filter('date')($scope.toEpoch(searchParam['nostroFrmStmtDate']) * 1000, 'yyyy/MM/dd'),
                    '$lte': $filter('date')($scope.toEpoch(searchParam['nostroToStmtDate']) * 1000, 'yyyy/MM/dd')
                }
            }

            if (angular.isDefined(searchParam['reportDate'])) {
                var start_date = 0
                var end_date = 0

                start_date = $scope.toEpoch(searchParam['reportDate'])
                end_date = new Date(searchParam['reportDate'])
                end_date = end_date.setDate(end_date.getDate() + 1) / 1000

                queryParam['REPORT DATE'] = {'$gte': start_date, '$lt': end_date}
            }
            if (angular.isDefined(searchParam['amount'])) {

                if (searchParam['amountType'] == 'OPEN_BAL') {
                    if ($scope.amtOperator == '$eq') {
                        queryParam['OPENING BALANCE'] = searchParam['amount']
                    } else {
                        amtParam = {}
                        amtParam[$scope.amtOperator] = searchParam['amount']
                        queryParam['OPENING BALANCE'] = amtParam
                    }
                }

                if (searchParam['amountType'] == 'CLOSE_BAL') {
                    if ($scope.amtOperator == '$eq') {
                        queryParam['CLOSING BALANCE'] = searchParam['amount']
                    } else {
                        amtParam = {}
                        amtParam[$scope.amtOperator] = searchParam['amount']
                        queryParam['CLOSING BALANCE'] = amtParam
                    }
                }
            }
            if (angular.isDefined(searchParam['currency']) && searchParam['currency'].length > 0) {

                selCcy = []
                angular.forEach(searchParam['currency'], function (key, value) {
                    selCcy.push(key['id'])
                })
                queryParam['CURRENCY'] = {"$in": selCcy}
            }
            if (angular.isDefined(searchParam['nostroAccount'])) {
                queryParam['ACCOUNT NO'] = searchParam['nostroAccount']
            }

            console.log(queryParam)

            ReportInterfaceService.getNostroReportDetails('dummy', JSON.stringify(queryParam), function (data) {
                $rootScope.openModalPopupClose()

                if (data['total'] == 0) {
                    $rootScope.showAlertMsg("No data found for this selection")
                } else {
                    $scope.reportLocMap = {}
                    $scope.nostroReportExtract = JSON.parse(data['reportData'])

                    $scope.reportLocMap['CSV'] = data['csv_file_path']
                    $scope.reportLocMap['EXCEL'] = data['excel_file_path']
                    //$scope.reportLocMap['PDF'] = data['pdf_file_path']
                    console.log($scope.nostroReportExtract)
                }
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })

        }
    }

    $scope.initializeNostoReport = function (resetInd) {

        // check if authoirzed to access this report
        $scope.enableNostroReport = false
        var perColConditions = {};
        perColConditions["user"] = $rootScope.credentials.userName
        perColConditions["businessContext"] = "111119212723"
        ReconAssignmentService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {

                if (data['total'] > 0) {
                    for (i = 0; i < data['data'].length; i++) {
                        if (data['data'][i]['recons'].includes("NOSTRO_BAL_REPORT")) {
                            $scope.enableNostroReport = true
                            break;
                        }
                    }
                }

                if ($scope.enableNostroReport) {
                    $scope.nostroCurrencies = []
                    // initialize currency param
                    $scope.nostroAdvSearch = {'currency': []}
                    $scope.nostroReportExtract = {}

                    if (!angular.isDefined(resetInd)) {
                        $scope.selNostroBusinessName = ''
                        $scope.selNostroReportName = ''
                    }
                    $rootScope.openModalPopupOpen();
                    $scope.generateNostroReport()
                    ReportInterfaceService.getDistinctCurrency('dummy', {}, function (data) {
                        $rootScope.openModalPopupClose()
                        if (angular.isDefined(data)) {
                            angular.forEach(data, function (key, value) {
                                tmp = {}
                                tmp['id'] = key
                                tmp['label'] = key
                                $scope.nostroCurrencies.push(tmp)
                            })
                            console.log($scope.nostroCurrencies)
                        }
                    }, function (err) {
                        $rootScope.showAlertMsg(err.msg)
                    })
                }

                else {
                    $rootScope.showAlertMsg('un-authorized access')
                }
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            }
        )
    }

    $scope.initializeGSTReport = function () {
        $scope.clearGSTData()
        $scope.gstReportDate = ''
        $scope.selGstReport = ''
    }

    $scope.generateGstReport = function () {
        var stmtDate = $filter('date')($scope.toEpoch($scope.gstReportDate) * 1000, 'yyyyMMdd')
        $scope.rawData = []
        $scope.sourcewiseGst = []

        $rootScope.openModalPopupOpen()
        if ($scope.groupByGstReport.indexOf($scope.selGstReport) != -1) {
            // source wise data else
            ReportInterfaceService.generateGstReport('dummy',
                {"statementDate": stmtDate, "gstType": $scope.selGstReport}, function (data) {
                    $scope.sourcewiseGst = data
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.openModalPopupClose()
                    $rootScope.showAlertMsg(err.msg)
                    console.log(err)
                })
        } else if ($scope.gstReportType.indexOf($scope.selGstReport) !== -1) {
            ReportInterfaceService.generateGstReport('dummy',
                {"statementDate": stmtDate, "gstType": $scope.selGstReport}, function (data) {
                    $rootScope.openModalPopupClose()
                    $scope.rawData = data
                    $scope.subcolumns = [{'name': ''}];
                    $scope.gstHeaders = [{"name": "State Wise", "subcolumns": [{"name": ""}]}];
                    $scope.sources = _.groupBy($scope.rawData, 'SOURCE');
                    $scope.sourceHeaders = [];

                    // sort sources by OGL
                    var sortOgl = {}
                    var sortOthers = {}
                    angular.forEach($scope.sources, function (a, b) {
                        if (b == 'OGL' || b == 'IDFC Bank Total') {
                            sortOgl[b] = a
                        } else {
                            sortOthers[b] = a
                        }
                    })
                    $scope.sources = angular.extend(sortOgl, sortOthers)
                    // sort sources ends

                    if (angular.isDefined($scope.sources.OGL)) {
                        $scope.oglData = angular.copy($scope.sources.OGL);
                    }
                    //delete $scope.sources.OGL;
                    angular.forEach($scope.sources, function (a, b) {
                        var obj = {
                            'name': b,
                            'subcolumns': [{"name": "No of Trnx"}, {"name": "Charge Value"}, {"name": "Tax Value"}]
                        };
                        angular.forEach(obj.subcolumns, function (c, d) {
                            $scope.subcolumns.push({'name': c.name})
                        })
                        $scope.gstHeaders.push(obj);
                        $scope.sourceHeaders.push(obj);

                        var singleobj = {
                            'headers': [{"name": "State Wise", "subcolumns": [{"name": ""}]},
                                {"name": b, "subcolumns": [{"name": "No of Trnx"}, {"name": "Tax Value"}]},
                                {
                                    "name": "OGL",
                                    "subcolumns": [{"name": "No of Trnx"}, {"name": "Tax Value"}, {"name": "Difference"}]
                                },
                            ], 'subcols': [], 'datas': []
                        }

                        angular.forEach(singleobj.headers, function (c, d) {
                            angular.forEach(c.subcolumns, function (i, j) {
                                singleobj.subcols.push({'name': i.name})
                            })
                        });

                        angular.forEach(a, function (y, z) {
                            if (a.SOURCE == b) {
                                singleobj.datas.push({'value': a['STATE NAME']});
                                singleobj.datas.push({'value': a['TRANSACTION COUNT']});
                                singleobj.datas.push({'value': a['TAX']});
                            }
                            angular.forEach($scope.sources['OGL'], function (w, x) {
                                if (a.SOURCE == w.MATCHSOURCE) {
                                    singleobj.datas.push({'value': w['TRANSACTION COUNT']});
                                    singleobj.datas.push({'value': w['TAX']});
                                    singleobj.datas.push({'value': Number(singleobj.datas[2]) + Number(w['TAX'])});
                                }
                            })
                        })
                        console.log('===========')
                        console.log(singleobj)
                        console.log('===========')
                    });

                    $scope.stateGroup = _.groupBy($scope.rawData, 'STATE NAME');
                    $scope.rowData = [];
                    $scope.totalData = [];
                    angular.forEach($scope.stateGroup, function (a, b) {
                        var obj = [{'value': b}];
                        var tmpSources = _.groupBy(a, 'SOURCE');
                        angular.forEach($scope.sourceHeaders, function (c, d) {
                            if (angular.isDefined(tmpSources[c.name])) {
                                if (c.name == 'OGL') {
                                    var tax = 0;
                                    var tsc = 0;
                                    angular.forEach(tmpSources[c.name], function (e, f) {
                                        tsc += e['TRANSACTION COUNT'];
                                        tax += e['TAX'];
                                    })
                                    obj.push({'value': tsc});
                                    obj.push({'value': ''});
                                    obj.push({'value': tax});
                                } else {
                                    obj.push({'value': tmpSources[c.name][0]['TRANSACTION COUNT']});
                                    obj.push({'value': tmpSources[c.name][0]['CHARGE']});
                                    obj.push({'value': tmpSources[c.name][0]['TAX']});
                                }
                            } else {
                                obj.push({'value': ''});
                                obj.push({'value': ''});
                                obj.push({'value': ''});
                            }
                        })

                        console.log(obj)

                        var computeTotal = angular.copy(obj)
                        if ($scope.totalData.length == 0) {
                            angular.forEach(computeTotal, function (v, k) {
                                computeTotal[0]['value'] = 'Total'
                                if (k > 0) {
                                    computeTotal[k]['value'] = $scope.convertFloat(v['value'])
                                }
                            })
                            $scope.totalData = computeTotal
                        } else {
                            angular.forEach(computeTotal, function (v, k) {
                                if (k > 0) {
                                    $scope.totalData[k]['value'] += $scope.convertFloat(v['value'])
                                }
                            })
                        }
                        $scope.rowData.push(obj);
                    });

                    console.log($scope.rowData)
                    console.log("====== Total Data ======")
                    console.log($scope.totalData)
                }, function (err) {
                    $rootScope.openModalPopupClose()
                    $rootScope.showAlertMsg(err.msg)
                    console.log(err)
                })
        }
    }


    $scope.resetNostroDetails = function () {
        $scope.initializeNostoReport(true)
        $scope.generateNostroReport()
    }

    $scope.convertFloat = function (number) {
        number = parseFloat(number)
        return isNaN(number) ? 0.0 : number
    }

    $scope.rollBackGSTReport = function () {
        var stmtDate = $filter('date')($scope.toEpoch($scope.gstReportDate) * 1000, 'yyyyMMdd')
        GstReportService.get('dummy', {"query": {"perColConditions": {'statementdate': stmtDate}}}, function (data) {
            GstMetaInfoService.get(undefined, {"query": {'perColConditions': {"rollback": true}}}, function (data) {
                if (data['total'] > 0) {
                    data = data['data'][0]
                    data['statementdate'] = stmtDate
                    GstMetaInfoService.put(data['_id'], data, function (data) {
                        $rootScope.showSuccessMsg("Rollback successful")
                    }, function (err) {
                        console.log(err)
                    })
                } else {
                    GstMetaInfoService.post({
                        'statementdate': stmtDate,
                        'rollback': true
                    }, function (data) {
                        $rootScope.showSuccessMsg("Rollback successful")
                    }, function (err) {
                        console.log(err)
                    })
                }
            }, function (err) {
                console.log(err)
                $rootScope.showAlertMsg(err.msg)
            })
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
            console.log(err)
        })
    }

    $scope.clearGSTData = function () {
        $scope.sourcewiseGst = []
        $scope.rowData = []
        $scope.totalData = []
    }

    $scope.freezeSource = function () {
        $modal.open({
            templateUrl: 'freezeSource.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.freezeSources = function (freezeSrc) {
                    var srcList = []
                    angular.forEach(freezeSrc['selectedSources'], function (v, k) {
                        srcList.push(v['id'])
                    })

                    GstMetaInfoService.freeze_sources('dummy', {
                        'statementdate': $filter('date')($scope.toEpoch(freezeSrc['freezeStmtDate']) * 1000, 'yyyyMMdd'),
                        'freezed': true, 'sources': srcList
                    }, function (data) {
                        $rootScope.showSuccessMsg('GST sources frozen successfully.')
                        $modalInstance.dismiss('cancel');
                    }, function (err) {
                        console.log(err.msg)
                        $modalInstance.dismiss('cancel');
                    })

                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                // load all GST sources
                $rootScope.openModalPopupOpen()
                GSTSourceProductGroupService.get(undefined, undefined, function (data) {
                    var unique_src = []
                    $scope.gstSources = []
                    $scope.freezedSrc = []
                    angular.forEach(data.data, function (val, key) {
                        if (unique_src.indexOf(val.sourceName) == -1) {
                            $scope.gstSources.push({
                                "name": val.sourceName,
                                "id": val.sourceCode,
                                'ticked': false
                            })
                            unique_src.push(val.sourceName)
                        }
                    })
                    $rootScope.openModalPopupClose()
                }, function (err) {
                    $rootScope.showAlertMsg(err.msg)
                    $rootScope.openModalPopupClose()
                })
            }
        });
    }


    $scope.viewReport = function (selDealReportData) {
        var searchParam = {}
        if (angular.isDefined($scope.dealstatementDate)) {
            var statementDate = $scope.toEpoch($scope.dealstatementDate)
            statementDate = $filter('date')(statementDate * 1000, 'dd/MM/yy')
            searchParam['statementDate'] = statementDate
        }
        searchParam['selectedReport'] = selDealReportData
        searchParam['CYCLE'] = $scope.cycle
        $rootScope.openModalPopupOpen()
        ReportInterfaceService.dealTallyCycleWise('dummy', searchParam, function (data) {
            $scope.exportDel = true
            $rootScope.openModalPopupClose()
            $scope.dealTallyCycle = data['data']
            if (Object.keys(data).indexOf('nightDeal') != -1) {
                $scope.nightdealTallyCycle = data['nightDeal']
            }
            $scope.reportFilePath = data['filePath']
        }, function (err) {
            $scope.initializeDealReport()
            $rootScope.showAlertMsg(err.msg)
            $rootScope.openModalPopupClose()
        })
    }

    $scope.initializeDealReport = function () {
        $scope.exportDel = false
        $scope.nightdealTallyCycle = {}
        $scope.dealTallyCycle = {}
    }
    $scope.exportDealReport = function (selDealReportData) {
        var searchParam = {}
        var filePath = window.location.origin + '/app/files/recon_reports/'

        filePath = filePath + $scope.reportFilePath
        $window.open(filePath);

        // if (angular.isDefined($scope.dealstatementDate)) {
        //     var statementDate = $scope.toEpoch($scope.dealstatementDate)
        //     statementDate = $filter('date')(statementDate * 1000, 'dd/MM/yy')
        //     searchParam['statementDate'] = statementDate
        // }
        // searchParam['selectedReport'] = selDealReportData
        // ReportInterfaceService.getDealTallyCycleReport('dummy', searchParam, function (data) {
        //     if (data != '' && angular.isDefined(data)) {
        //         console.log('here')
        //         console.log(data)
        //
        //         console.log(filePath, window.location.origin + filePath)
        //     }
        //     else {
        //         $rootScope.showAlertMsg('No data to export')
        //     }
        // }, function (err) {
        //     $rootScope.showAlertMsg(err.msg)
        //     console.log(err)
        // })

    }

}
