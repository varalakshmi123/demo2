/**
 * Created by swami on 13/4/15.
 */
function dashboardController($scope, $rootScope, $location, $routeParams, $timeout, $route, $window, $modal, $filter,$http, ReportInterfaceService, UsersGraphService,ReconAssignmentService,BusinessService,UserPoolService,UsersService) {
    Highcharts.setOptions({
        lang: {
            numericSymbols: ['k', 'L', 'M', 'C', 'B', 'T']
        }
    });
    function Comparator(a, b) {
        if (a == null) {
            a = 0;
        }
        if (b == null) {
            b = 0;
        }
        if (a[1] > b[1]) return -1;
        if (a[1] < b[1]) return 1;
        return 0;
    }
    Array.prototype.contains = function(v) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === v) return true;
        }
        return false;
    };
    Array.prototype.unique = function() {
        var arr = [];
        for(var i = 0; i < this.length; i++) {
            if(!arr.contains(this[i])) {
                arr.push(this[i]);
            }
        }
        return arr;
    }
    $scope.resetDataDashboardOut = function(){
        $scope.bcId = '';
        $scope.rcId = '';
        $scope.srcName = '';
        $scope.acName = '';
        $scope.reconId = [];
        $scope.sourceNameArray = [];
        $scope.accountNameArray = [];
    }
    $scope.resetDataDashboardTrn = function(){
        $scope.metaBcId = '';
        $scope.metaRcId = ''
        $scope.metaReconId = []
        $scope.metaSourceName = []
    }
    //Select Data
    $scope.bcId = '';
    $scope.rcId = '';
    $scope.rsId = '';
    //Column Data Array
    $scope.resetData = function () {
        $scope.creditDataSeries = [];
        $scope.debitDataSeries = [];
        $scope.categoriesData = [];
        $scope.extraCreditData = [];
        $scope.extraDebitData = [];
        $scope.netOutStanding = [];
        $scope.netOutStandingCount = [];
        $scope.txnMatchStatus = [];
    }
    //Generic Data Grid Options
    $scope.dashboardGridOptions = {
        columnDefs: [],
        rowData: [],
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        onRowClicked: function (row) {
            //console.log(row)
            if (angular.isDefined(row.data['RECON_NAME'])){
                $rootScope.businessContextId = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.reconID = row.data['RECON_ID']
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.openModalPopupOpen()
                localStorage.setItem('saved', JSON.stringify({}));
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $rootScope.openModalPopupClose()
                    ////console.log(data.data)
                    angular.forEach(data.data, function (val, key) {
                        if (row.data['RECON_ID'] == val['RECON_ID']) {
                                //console.log(val)
                                angular.forEach($scope.allocated_recons, function (v, k) {
                                    if (v.recon == row.data['RECON_ID']) {
                                        var perColConditions = {};
                                        perColConditions["name"] = v.userpool
                                        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                            console.log(data)
                                            if (data.data.length > 0) {
                                                $rootScope.userpoolRole = data.data[0]['role']
                                                $rootScope.userpoolName= data.data[0]['name']
                                                console.log($rootScope.userpoolRole)
                                            }
                                            $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                            var myObj = {}
                                            myObj['reconId'] = row.data['RECON_ID']
                                            myObj['reconName'] = row.data['RECON_NAME']
                                            myObj['businessId'] = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                                            myObj['businessProcess'] = $scope.businessProcess
                                            myObj['from'] = 'dashboard'
                                            myObj['userRole'] = $rootScope.userpoolRole
                                            myObj['userPoolName'] = $rootScope.userpoolName
                                            myObj['workpool'] = val['WORKPOOL_NAME']
                                            //console.log(myObj)
                                            localStorage.removeItem('summary')
                                            localStorage.removeItem('suspense')
                                            localStorage.setItem('saved', JSON.stringify(myObj));
                                            $location.path("/recons")
                                            //$window.location.href = 'https://'+$location.host()+'/recons'
                                        })
                                    }
                                })
                                //$window.open('https://'+$location.host()+'/recons', '_blank');
                            }
                        })
                })
            }
            var lastSelectedRow = this.appSpecific.lastSelectedRow;
            var shiftKey = row.event.shiftKey,
                ctrlKey = row.event.ctrlKey;

            // If modifier keys aren't pressed then only select the row that was clicked
            if (!shiftKey && !ctrlKey) {
                this.api.deselectAll();
                this.api.selectNode(row.node, true);
            }
            // If modifier keys are used and there was a previously selected row
            else if (lastSelectedRow !== undefined) {
                // Select a block of rows
                if (shiftKey) {
                    var startIndex, endIndex;
                    // Get our start and end indexes correct
                    if (row.rowIndex < lastSelectedRow.rowIndex) {
                        startIndex = row.rowIndex;
                        endIndex = lastSelectedRow.rowIndex;
                    }
                    else {
                        startIndex = lastSelectedRow.rowIndex;
                        endIndex = row.rowIndex;
                    }
                    // Select all the rows between the previously selected row and the
                    // newly clicked row
                    for (var i = startIndex; i <= endIndex; i++) {
                        this.api.selectIndex(i, true, true);
                    }
                }
                // Select one more row
                if (ctrlKey) {
                    this.api.selectIndex(row.rowIndex, true);
                }
            }
            // Store the recently clicked row for future use
            this.appSpecific.lastSelectedRow = row;
        },
        //onModelUpdated: onModelUpdated,
        groupSelectsChildren: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        suppressRowClickSelection: true,
        groupSuppressAutoColumn:false
    };

    $scope.currentChart = {};
    //Generic Chart Data Options
    $scope.initializeChart = function () {
        $scope.chart = {
            chart: {
                type: 'column',
                spacingBottom: 50,
                options3d: {
                    enabled: true,

                    alpha: 0,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                },
                zoomType: 'x'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: $scope.categoriesData,
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            /*yAxis: {
             min: 0,
             title: {
             text: 'Count'
             }
             },*/
            legend: {
                /*layout: 'vertical',*/
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                y: 40
            },
            tooltip: {
                formatter: function () {
                    var index = $.inArray(this.x, $scope.categoriesData);
                    var s = '<b>' + this.x + '</b>';
                    s += '<br/>' + 'Credit Count: ' + $scope.creditDataSeries[index];
                    s += '<br/>' + 'Debit Count: ' + $scope.debitDataSeries[index];
                    s += '<br/>' + $scope.chart.series[0].name + ': ' + formatNumber($scope.extraCreditData[index]);
                    s += '<br/>' + $scope.chart.series[1].name + ': ' + formatNumber($scope.extraDebitData[index]);
                    s += '<br/>' + 'Net Outstanding Amount: ' + formatNumber($scope.netOutStanding[index]);
                    s += '<br/>' + 'Net Outstanding Count: ' + $scope.netOutStandingCount[index];
                    return s
                },
                /*headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                 '<td style="padding:0"><b>{point.y}</b></td></tr>',
                 footerFormat: '</table>',*/
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Credit',
                data: $scope.creditDataSeries,
                extraData: $scope.extraCreditData,
                /*dataLabels: {
                 enabled: true,
                 rotation: -90,
                 color: '#FFFFFF',
                 align: 'right',
                 //format: '{point.y}', // one decimal
                 formatter: function () {
                 var index = $.inArray(this.x, $scope.categoriesData);
                 return $scope.extraCreditData[index]
                 },
                 y: 10, // 10 pixels down from the top
                 style: {
                 fontSize: '13px',
                 fontFamily: 'Verdana, sans-serif'
                 }
                 }*/
            }, {
                name: 'Debit',
                data: $scope.debitDataSeries,
                extraData: $scope.extraDebitData,
                /*dataLabels: {
                 enabled: true,
                 rotation: -90,
                 color: '#FF00FF',
                 align: 'right',
                 //format: '{point.y}', // one decimal
                 formatter: function () {
                 var index = $.inArray(this.x, $scope.categoriesData);
                 return $scope.extraDebitData[index]
                 },
                 y: 10, // 10 pixels down from the top
                 style: {
                 fontSize: '13px',
                 fontFamily: 'Verdana, sans-serif'
                 }
                 }*/
            }, {
                name: 'Credit Amount',
                data: $scope.extraCreditData,
                extraData: $scope.netOutStanding,
            }, {
                name: 'Debit Amount',
                data: $scope.extraDebitData,
                extraData: $scope.netOutStanding,
            }, {
                name: 'Net Outstanding Amount',
                data: $scope.netOutStanding,
                extraData: $scope.netOutStanding,
            }, {
                name: 'Net Outstanding Count',
                data: $scope.netOutStandingCount,
                extraData: $scope.netOutStanding,
            }]
        }
    }
    $scope.resetData();
    $scope.pinnedCharts = [];

    //BusinessContext Code Begin
    $scope.getBCId = function () {
        $scope.rcId = '';
        $scope.reconId = [];
        $scope.rsId = '';
        $scope.reconStatusId = [];
        $scope.currentChart = {};
        $scope.currentChart.id = 'BUSINESS_CONTEXT_ID';
        $scope.currentChart.filter = {};
        $scope.currentChart.dataSet = 'BusinessRecon';
        $rootScope.openModalPopupOpen();
        ReportInterfaceService.queryData('BUSINESS_CONTEXT_ID', {}, 'BusinessRecon', function (data) {
            $rootScope.openModalPopupClose()
            $scope.businessContextId = JSON.parse(data);
            $scope.loadBCData($scope.businessContextId);
        })
    }
    $scope.getBCId();
    function formatNumber(input, decimalPlaces) {
        if (isNaN(input) && !angular.isDefined(input))
            return input;
        else {
            decimalPlaces = (angular.isDefined(decimalPlaces)) ? decimalPlaces : 2;
            return Number(parseFloat(input).toFixed(2)).toLocaleString('en', {
                minimumFractionDigits: decimalPlaces
            });
        }
  }
    $scope.loadBCData = function (data) {
        var columnDefs = [
            {headerName: "RECON NAME", field: "RECON_NAME",'width':302, cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                    //console.log(params)
                    if (params.value!=null && params.value!= '' && params.value != undefined) {
                        if (angular.isDefined(params.data['RECON_NAME'])){
                            return '<a href="#" >'+params.value+'</a>'
                        }
                    }
                }*/},
            {headerName: "RECON ID", field: "RECON_ID",'width':302,},
            {headerName: "WORKPOOL NAME",'width':302, field: "WORKPOOL_NAME"},
            {headerName: "BUSINESS CONTEXT ID",'width':302, field: "BUSINESS_CONTEXT_ID"},
            {
                headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'}, 'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }
            },
            {headerName: "DEBIT", field: "DEBIT", 'width':302,filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT", field: "CREDITCOUNT",'width':302, filter: 'number'},
            {headerName: "DEBIT COUNT", field: "DEBITCOUNT", 'width':302,filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT", field: "NETOUTSTANDINGAMOUNT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildBCColumnData(data);
    }
    $scope.buildBCColumnData = function (data) {
        $scope.resetData();

        angular.forEach(data, function (a, b) {
            console.log(typeof(a.CREDIT.toFixed(2)));
            $scope.categoriesData.push(a.RECON_NAME);
            $scope.creditDataSeries.push(a.CREDITCOUNT);
            $scope.debitDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditData.push(a.CREDIT);
            $scope.extraDebitData.push(a.DEBIT);
            $scope.netOutStanding.push(parseInt(a.NETOUTSTANDINGAMOUNT));
            $scope.netOutStandingCount.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeChart();
        $scope.loadBCGraph(data);
    }
    $scope.loadBCGraph = function (data) {
        $scope.chart.title.text = 'Recon Id wise Credit Debit';
        $('#container').highcharts($scope.chart);
        var chartcore = $('#container').highcharts(), $button = $('#button');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //BusinessContext Code End

    //wow code begin
    $scope.wowObj = {_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",toWow:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=$scope.wowObj._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},checkWow:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=$scope.wowObj._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
    $http.get("/app/lic/key.lic").error(function () {
        $rootScope.showErrormsg($scope.wowObj.decode('TGljZW5zZSBFeHBpcmVkLiBQbGVhc2UgdXBncmFkZSB5b3VyIGxpY2Vuc2Uu'));
        $timeout(function(){
          $location.path("/recons")
        }, 1000);
    }).success(function (data) {
        //if(data.charAt(0) == '<' || (new Date() > new Date(($scope.wowObj.checkWow(data).split("").reverse().join("")) * 1000))){
          //$rootScope.showErrormsg($scope.wowObj.checkWow('TGljZW5zZSBFeHBpcmVkLiBQbGVhc2UgdXBncmFkZSB5b3VyIGxpY2Vuc2Uu'));
       // $timeout(function(){
         // $location.path("/recons")
      //  }, 1000);
       // }
    });

    //wow code end

    //Recon Code Begin
    $scope.getRecons = function (bcId) {
        $scope.reconId =[];
        $scope.sourceNameArray = [];
        $scope.accountNameArray = [];
        $scope.reconStatusId = [];

        $scope.rcId =null;
        $scope.srcName = null;
        $scope.acName = null;
        $scope.rsId = null;

        if (bcId == '' || bcId == null || bcId == undefined) {
            $scope.getBCId();
        }
        else{
            $scope.rsId = '';
            $scope.rsId = '';
            $scope.reconStatusId = [];
            var filters = {}
            filters["BUSINESS_CONTEXT_ID"] = '"' + bcId + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'RECON_ID';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'BusinessRecon';

            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('RECON_ID', filters, 'BusinessRecon', function (data) {
                $rootScope.openModalPopupClose()
                $scope.reconId = JSON.parse(data);
                $scope.loadRCData($scope.reconId);
            })
        }
    }
    $scope.loadRCData = function (data) {
        var columnDefs = [
            {headerName: "RECON NAME", field: "RECON_NAME",'width':302, cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                    if (params.value!=null && params.value!= '' && params.value != undefined) {
                        if (angular.isDefined(params.data['RECON_NAME'])){
                            return '<a href="#" >'+params.value+'</a>'
                        }
                    }
                }*/},
            {headerName: "RECON ID",'width':302, field: "RECON_ID"},
            {headerName: "CREDIT", field: "CREDIT", 'width':302,filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT", 'width':302,filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT", 'width':302,field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT", 'width':302,field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildRCColumnData(data);
    }
    $scope.buildRCColumnData = function (data) {
        $scope.resetData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesData.push(a.RECON_NAME);
            $scope.creditDataSeries.push(a.CREDITCOUNT);
            $scope.debitDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditData.push(a.CREDIT);
            $scope.extraDebitData.push(a.DEBIT);
            $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCount.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeChart();
        $scope.loadRCGraph(data);
    }
    $scope.loadRCGraph = function (data) {
        $scope.chart.title.text = 'Recon wise Credit Debit';
        $('#container').highcharts($scope.chart);
        var chartcore = $('#container').highcharts(), $button = $('#button');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //Recon Code End

    //Source_name  Code Begin
    $scope.getSourceName = function (rcId) {
        $scope.sourceNameArray = [];
        $scope.accountNameArray = [];
        $scope.reconStatusId = [];
        $scope.srcName = null;
        $scope.acName = null;
        $scope.rsId = null;
        if (rcId == '' || rcId == null || rcId == undefined) {
            $scope.getRecons($scope.bcId);
        }
        else{
            var filters = {}
            filters["BUSINESS_CONTEXT_ID"] = '"' + $scope.bcId + '"';
            filters["RECON_ID"] = '"' + rcId + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'SOURCE_NAME';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'BusinessRecon';

            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('SOURCE_NAME', filters, 'BusinessRecon', function (data) {
                $rootScope.openModalPopupClose()
                //console.log(JSON.parse(data))
                $scope.sourceNameArray = JSON.parse(data);
                $scope.loadSrcData($scope.sourceNameArray);
            })
        }

    }
    $scope.loadSrcData = function (data) {
        var columnDefs = [
            {headerName: "SOURCE NAME", 'width':302,field: "SOURCE_NAME"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT",'width':302, field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT",'width':302, field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT", 'width':302,field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        //console.log(data)
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildSrcColumnData(data);
    }
    $scope.buildSrcColumnData = function (data) {
        $scope.resetData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesData.push(a.SOURCE_NAME);
            $scope.creditDataSeries.push(a.CREDITCOUNT);
            $scope.debitDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditData.push(a.CREDIT);
            $scope.extraDebitData.push(a.DEBIT);
            $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCount.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeChart();
        $scope.loadSrcGraph(data);
    }
    $scope.loadSrcGraph = function (data) {
        $scope.chart.title.text = 'Source name wise Credit Debit';
        $('#container').highcharts($scope.chart);
        var chartcore = $('#container').highcharts(), $button = $('#button');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //SOURCE_NAME STATUS Code End

    //ACCOUNT_NAME  Code Begin
    $scope.getAccount = function (srcName) {
        $scope.accountNameArray = [];
        $scope.reconStatusId = [];
        $scope.acName = null;
        $scope.rsId = null;
        if (srcName == '' || srcName == null || srcName == undefined) {
            $scope.getSourceName($scope.rcId);
        }
        else{
            var filters = {}
            filters["BUSINESS_CONTEXT_ID"] = '"' + $scope.bcId + '"';
            filters["RECON_ID"] = '"' + $scope.rcId + '"';
            filters["SOURCE_NAME"] = '"' + $scope.srcName + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'ACCOUNT_NAME';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'BusinessRecon';

            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('ACCOUNT_NAME', filters, 'BusinessRecon', function (data) {
                $rootScope.openModalPopupClose()
                $scope.accountNameArray = JSON.parse(data);
                if (JSON.parse(data).length == 0){
                    $rootScope.showAlertMsg('No data Found')
                }
                $scope.loadANData($scope.accountNameArray);
            })
        }
    }
    $scope.loadANData = function (data) {
        var columnDefs = [
            {headerName: "ACCOUNT NAME", 'width':302,field: "ACCOUNT_NAME"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT", field: "CREDITCOUNT",'width':302, filter: 'number'},
            {headerName: "DEBIT COUNT", field: "DEBITCOUNT",'width':302, filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildANColumnData(data);
    }
    $scope.buildANColumnData = function (data) {
        $scope.resetData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesData.push(a.ACCOUNT_NAME);
            $scope.creditDataSeries.push(a.CREDITCOUNT);
            $scope.debitDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditData.push(a.CREDIT);
            $scope.extraDebitData.push(a.DEBIT);
            $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCount.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeChart();
        $scope.loadANGraph(data);
    }
    $scope.loadANGraph = function (data) {
        $scope.chart.title.text = 'Account name wise Credit Debit';
        $('#container').highcharts($scope.chart);
        var chartcore = $('#container').highcharts(), $button = $('#button');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //ACCOUNT_NAME STATUS Code End


    //TXN_MATCHING_STATUS  Code Begin
    $scope.getTxn = function (acName) {
        $scope.txnMatchStatusArray = [];
        $scope.reconStatusId = [];
        $scope.rsId = null;

        if (acName == '' || acName == null || acName == undefined) {
            $scope.getAccount($scope.srcName);
        }
        else{

            var filters = {}
            filters["BUSINESS_CONTEXT_ID"] = '"' + $scope.bcId + '"';
            filters["RECON_ID"] = '"' + $scope.rcId + '"';
            filters["SOURCE_NAME"] = '"' + $scope.srcName + '"';
            filters["ACCOUNT_NAME"] = '"' + acName + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'TXN_MATCHING_STATUS';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'BusinessRecon';

            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('TXN_MATCHING_STATUS', filters, 'BusinessRecon', function (data) {
                $rootScope.openModalPopupClose()
                $scope.txnMatchStatusArray = JSON.parse(data);
                $scope.loadTMData($scope.txnMatchStatusArray);
            })
        }
    }
    $scope.loadTMData = function (data) {
        var columnDefs = [
            {headerName: "TXN MATCHING STATUS",'width':302, field: "TXN_MATCHING_STATUS"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                        return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT",'width':302, field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT",'width':302, field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildTMColumnData(data);
    }
    $scope.buildTMColumnData = function (data) {
        $scope.resetData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesData.push(a.TXN_MATCHING_STATUS);
            $scope.creditDataSeries.push(a.CREDITCOUNT);
            $scope.debitDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditData.push(a.CREDIT);
            $scope.extraDebitData.push(a.DEBIT);
            $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCount.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeChart();
        $scope.loadTMGraph(data);
    }
    $scope.loadTMGraph = function (data) {
        $scope.chart.title.text = 'Trend wise Credit Debit';
        $('#container').highcharts($scope.chart);
        var chartcore = $('#container').highcharts(), $button = $('#button');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
    }
    //TXN_MATCHING_STATUS STATUS Code End


    //Pinned graph code begin
    $scope.getPinnedCharts = function(){
        $rootScope.openModalPopupOpen();
        var perColConditions = {};
        perColConditions["name"] = $rootScope.credentials.userName
        UsersGraphService.get(undefined, {"query": {"perColConditions": perColConditions}}, function(data){
            $rootScope.openModalPopupClose();
            if(data.data[0] != undefined ){
                $scope.userGraphData = data.data[0];
                $scope.pinnedCharts = data.data[0].data;
                $scope.loadPinnedChart();
            }
        });
    }
    $scope.loadPinnedChart = function(){
        angular.forEach($scope.pinnedCharts, function(a, b){
            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData(a.id, a.filter, a.dataSet, function(data){
                $rootScope.openModalPopupClose();
                $scope.resetData();
                angular.forEach(JSON.parse(data), function (c, d) {
                    if(a.id =='RECON_ID'){
                        $scope.categoriesData.push(c['RECON_NAME']);
                    }else if(a.id == 'BUSINESS_CONTEXT_ID'){
                         $scope.categoriesData.push(c['WORKPOOL_NAME']);
                    }else{
                        $scope.categoriesData.push(c[a.id]);
                    }
                    $scope.creditDataSeries.push(c.CREDITCOUNT);
                    $scope.debitDataSeries.push(c.DEBITCOUNT);
                    $scope.extraCreditData.push(c.CREDIT);
                    $scope.extraDebitData.push(c.DEBIT);
                    $scope.netOutStanding.push(c.NETOUTSTANDINGAMOUNT);
                    $scope.netOutStandingCount.push(c.NETOUTSTANDINGCOUNT);
                });
                $scope.initializeChart();
                $scope.chart.title.text = a.title;
                $('#'+ a.id+''+b).highcharts($scope.chart);
                var chartcore = $('#'+ a.id+''+b).highcharts();
                var creditcount = chartcore.series[0];
                var debitcount = chartcore.series[1];
                var credit = chartcore.series[2];
                var debit = chartcore.series[3];
                var amount = chartcore.series[4];
                var outcount = chartcore.series[5];
                amount.hide();
                debit.hide();
                credit.hide();
                outcount.hide();
            })
        })
    }
    $scope.deleteAllUserGraph = function(){
        $rootScope.openModalPopupOpen();
        UsersGraphService.get(undefined, {}, function(data){
            $rootScope.openModalPopupClose();
            angular.forEach(data.data, function(a,b){
                UsersGraphService.deleteData(a._id);
            })
        });
    }
    $scope.removePinnedChart = function(index){
        $scope.pinnedCharts.splice(index,1);
        $scope.userGraphData.data = angular.copy($scope.pinnedCharts);
        UsersGraphService.put($scope.userGraphData._id, $scope.userGraphData, function(data){
            $rootScope.showSuccessMsg('Chart removed successfully');
        });
    }
    $scope.pinThisChart = function () {
        $rootScope.openModalPopupOpen();
        var perColConditions = {};
        perColConditions["name"] = $rootScope.credentials.userName
        UsersGraphService.get(undefined,  {"query": {"perColConditions": perColConditions}}, function(data){
            $rootScope.openModalPopupClose();
            if(data.data.length == 0){
                var chartdata={}; chartdata.name = $rootScope.credentials.userName; chartdata.data = [];
                $scope.currentChart.title = $scope.chart.title.text;
                chartdata.data.push($scope.currentChart);
                UsersGraphService.post(chartdata, function(data){
                    $scope.pinnedCharts = data.data;
                    $rootScope.showSuccessMsg('Chart added successfully');
                });
            }else{
                $scope.pinnedCharts = data.data[0].data;
                $scope.currentChart.title = $scope.chart.title.text;
                $scope.pinnedCharts.push($scope.currentChart);
                data.data[0].data = angular.copy($scope.pinnedCharts);
                UsersGraphService.put(data.data[0]._id, data.data[0], function(data){
                    $rootScope.showSuccessMsg('Chart added successfully');
                });
            }
        });
    }
    //Pinned graph code end

    /*-----------------Meta Data Section-----------------*/
    //Select Data
    $scope.metaBcId = '';
    $scope.metaRcId = '';
    $scope.metaRsId = '';
    //Column Data Array
    $scope.resetMetaData = function () {
        $scope.creditMetaDataSeries = [];
        $scope.debitMetaDataSeries = [];
        $scope.categoriesMetaData = [];
        $scope.extraCreditMetaData = [];
        $scope.extraDebitMetaData = [];
        $scope.netOutStandingMeta = [];
        $scope.netOutStandingCountMeta = [];
    }
    //Generic Data Grid Options
    $scope.dashboardMetaGridOptions = {
        columnDefs: [],
        rowData: [],
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        onRowClicked: function (row) {
            if (angular.isDefined(row.data['RECON_NAME'])){
                $rootScope.businessContextId = ($scope.metaBcId != '') ? $scope.metaBcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.reconID = row.data['RECON_ID']
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = ($scope.metaBcId != '') ? $scope.metaBcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.openModalPopupOpen()
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $rootScope.openModalPopupClose()
                    ////console.log(data.data)
                    angular.forEach(data.data, function (val, key) {
                        if (row.data['RECON_ID']== val['RECON_ID']) {
                                //console.log(val)
                                angular.forEach($scope.allocated_recons, function (v, k) {
                                    if (v.recon == row.data['RECON_ID']) {
                                        var perColConditions = {};
                                        perColConditions["name"] = v.userpool
                                        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                            console.log(data)
                                            if (data.data.length > 0) {
                                                $rootScope.userpoolRole = data.data[0]['role']
                                                $rootScope.userpoolName= data.data[0]['name']
                                                console.log($rootScope.userpoolRole)
                                            }
                                            $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                            var myObj = {}
                                            myObj['reconId'] = row.data['RECON_ID']
                                            myObj['reconName'] = row.data['RECON_NAME']
                                            myObj['businessId'] = ($scope.metaBcId != '') ? $scope.metaBcId : row.data['BUSINESS_CONTEXT_ID']
                                            myObj['from'] = 'dashboard'
                                            myObj['businessProcess'] = $scope.businessProcess
                                            myObj['userRole'] = $rootScope.userpoolRole
                                            myObj['userPoolName'] = $rootScope.userpoolName
                                            myObj['workpool'] = val['WORKPOOL_NAME']
                                            //console.log(myObj)
                                            localStorage.removeItem('summary')
                                            localStorage.removeItem('suspense')
                                            localStorage.setItem('saved', JSON.stringify(myObj));
                                            $location.path("/recons")
                                            //$window.location.href = 'https://'+$location.host()+'/recons'
                                        })
                                    }
                                })

                            }
                        })
                })
            }
            var lastSelectedRow = this.appSpecific.lastSelectedRow;
            var shiftKey = row.event.shiftKey,
                ctrlKey = row.event.ctrlKey;

            // If modifier keys aren't pressed then only select the row that was clicked
            if (!shiftKey && !ctrlKey) {
                this.api.deselectAll();
                this.api.selectNode(row.node, true);
            }
            // If modifier keys are used and there was a previously selected row
            else if (lastSelectedRow !== undefined) {
                // Select a block of rows
                if (shiftKey) {
                    var startIndex, endIndex;
                    // Get our start and end indexes correct
                    if (row.rowIndex < lastSelectedRow.rowIndex) {
                        startIndex = row.rowIndex;
                        endIndex = lastSelectedRow.rowIndex;
                    }
                    else {
                        startIndex = lastSelectedRow.rowIndex;
                        endIndex = row.rowIndex;
                    }
                    // Select all the rows between the previously selected row and the
                    // newly clicked row
                    for (var i = startIndex; i <= endIndex; i++) {
                        this.api.selectIndex(i, true, true);
                    }
                }
                // Select one more row
                if (ctrlKey) {
                    this.api.selectIndex(row.rowIndex, true);
                }
            }
            // Store the recently clicked row for future use
            this.appSpecific.lastSelectedRow = row;
        },
        groupSelectsChildren: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        suppressRowClickSelection: true
    };

    //Generic Chart Data Options
    $scope.initializeMetaChart = function () {
        $scope.metaChart = {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                },
                spacingBottom: 50
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: $scope.categoriesMetaData,
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            /*yAxis: {
             min: 0,
             title: {
             text: 'Count'
             }
             },*/
            legend: {
                /*layout: 'vertical',*/
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                y: 40
            },
            tooltip: {
                formatter: function () {
                    var index = $.inArray(this.x, $scope.categoriesMetaData);
                    var s = '<b>' + this.x + '</b>';
                    s += '<br/>' + 'Credit Count: ' + $scope.creditMetaDataSeries[index];
                    s += '<br/>' + 'Debit Count: ' + $scope.debitMetaDataSeries[index];
                    s += '<br/>' + $scope.chart.series[0].name + ': ' + formatNumber($scope.extraCreditMetaData[index]);
                    s += '<br/>' + $scope.chart.series[1].name + ': ' + formatNumber($scope.extraDebitMetaData[index]);
                    s += '<br/>' + 'Net Outstanding Amount: ' + formatNumber($scope.netOutStandingMeta[index]);
                    s += '<br/>' + 'Net Outstanding Count: ' + $scope.netOutStandingCountMeta[index];
                    return s
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Credit',
                data: $scope.creditMetaDataSeries,
                extraData: $scope.extraCreditMetaData,
            }, {
                name: 'Debit',
                data: $scope.debitMetaDataSeries,
                extraData: $scope.extraDebitMetaData,
            }, {
                name: 'Credit Amount',
                data: $scope.extraCreditMetaData,
                extraData: $scope.netOutStandingMeta,
            }, {
                name: 'Debit Amount',
                data: $scope.extraDebitMetaData,
                extraData: $scope.netOutStandingMeta,
            }, {
                name: 'Net Outstanding Amount',
                data: $scope.netOutStandingMeta,
                extraData: $scope.netOutStandingMeta,
            }, {
                name: 'Net Outstanding Count',
                data: $scope.netOutStandingCountMeta,
                extraData: $scope.netOutStandingMeta,
            }]
        }
    }
    $scope.resetMetaData();
    $scope.loadMetaRSGraph = function (data) {
        $scope.chart.title.text = 'Reconciliation status wise Credit Debit';
        $('#metaContainer').highcharts($scope.chart);
        var chartcore = $('#metaContainer').highcharts(), $button = $('#metaButton');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }

    //BusinessContext Code Begin
    $scope.getMetaBCId = function () {
        $scope.metaRcId = '';
        $scope.metaReconId = [];
        $scope.metaRsId = '';
        $scope.metaReconStatusId = [];
        $scope.currentChart = {};
        $scope.currentChart.id = 'BUSINESS_CONTEXT_ID';
        $scope.currentChart.filter = {};
        $scope.currentChart.dataSet = 'ExceptionReport';
        $rootScope.openModalPopupOpen();
        ReportInterfaceService.queryData('BUSINESS_CONTEXT_ID', {}, 'ExceptionReport', function (data) {
            $rootScope.openModalPopupClose()
            $scope.metaBusinessContextId = JSON.parse(data);
            $scope.loadMetaBCData($scope.metaBusinessContextId);
        })
    }
    //$scope.getMetaBCId();
    $scope.loadMetaBCData = function (data) {
        var columnDefs = [
            {headerName: "RECON NAME",'width':302, field: "RECON_NAME", cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                    if (params.value!=null && params.value!= '' && params.value != undefined) {
                        if (angular.isDefined(params.data['RECON_NAME'])){
                            return '<a href="#" >'+params.value+'</a>'
                        }
                    }
                }*/},
            {headerName: "RECON ID", 'width':302,field: "RECON_ID"},
            {headerName: "WORKPOOL NAME", 'width':302,field: "WORKPOOL_NAME"},
            {headerName: "BUSINESS CONTEXT ID",'width':302, field: "BUSINESS_CONTEXT_ID"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT",'width':302, field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT", 'width':302,field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardMetaGridOptions.columnDefs = columnDefs;
        $scope.dashboardMetaGridOptions.rowData = data;
        $scope.dashboardMetaGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardMetaGridOptions.api.setRowData(data);
        $scope.buildMetaBCColumnData(data);
    }
    $scope.buildMetaBCColumnData = function (data) {
        $scope.resetMetaData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesMetaData.push(a.RECON_NAME);
            $scope.creditMetaDataSeries.push(a.CREDITCOUNT);
            $scope.debitMetaDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditMetaData.push(a.CREDIT);
            $scope.extraDebitMetaData.push(a.DEBIT);
            $scope.netOutStandingMeta.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCountMeta.push(a.NETOUTSTANDINGCOUNT);
        });
        //$scope.creditMetaDataSeries = $scope.creditMetaDataSeries.sort(Comparator);
        //$scope.debitMetaDataSeries = $scope.debitMetaDataSeries.sort(Comparator);
        //$scope.extraCreditMetaData = $scope.extraCreditMetaData.sort(Comparator);
        //$scope.extraDebitMetaData = $scope.extraDebitMetaData.sort(Comparator);
        //$scope.netOutStandingMeta = $scope.netOutStandingMeta.sort(Comparator);
        //$scope.netOutStandingCountMeta = $scope.netOutStandingCountMeta.sort(Comparator);
        $scope.initializeMetaChart();
        $scope.loadMetaBCGraph(data);
    }
    $scope.loadMetaBCGraph = function (data) {
        $scope.metaChart.title.text = 'Recon Id wise Exception Report';
        $('#metaContainer').highcharts($scope.metaChart);
        var chartcore = $('#metaContainer').highcharts(), $button = $('#metaButton');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //BusinessContext Code End

    //Recon Code Begin
    $scope.getMetaRecons = function (metaBcId) {
        $scope.metaReconId = [];
        $scope.metaSourceName = [];

        $scope.metaRcId =null;
        $scope.metaSrcName = null;

        if (metaBcId == '' || metaBcId == null || metaBcId == undefined) {
            $scope.getMetaBCId();
        }
        else{

            $scope.metaRsId = '';
            $scope.metaRsId = '';
            $scope.metaReconStatusId = [];
            var filters = {};
            filters["BUSINESS_CONTEXT_ID"] = '"' + metaBcId + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'RECON_ID';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'ExceptionReport';
            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('RECON_ID', filters, 'ExceptionReport', function (data) {
                $rootScope.openModalPopupClose()
                $scope.metaReconId = JSON.parse(data);
                $scope.loadMetaRCData($scope.metaReconId);
            })
        }
    }
    $scope.loadMetaRCData = function (data) {
        var columnDefs = [
            {headerName: "RECON NAME", field: "RECON_NAME",'width':302, cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                    if (params.value!=null && params.value!= '' && params.value != undefined) {
                        if (angular.isDefined(params.data['RECON_NAME'])){
                            return '<a href="#" >'+params.value+'</a>'
                        }
                    }
                }*/},
            {headerName: "RECON ID",'width':302, field: "RECON_ID"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT",'width':302, field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT", 'width':302,field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT", 'width':302,field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT", 'width':302,field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardMetaGridOptions.columnDefs = columnDefs;
        $scope.dashboardMetaGridOptions.rowData = data;
        $scope.dashboardMetaGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardMetaGridOptions.api.setRowData(data);
        $scope.buildMetaRCColumnData(data);
    }
    $scope.buildMetaRCColumnData = function (data) {
        $scope.resetMetaData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesMetaData.push(a.RECON_NAME);
            $scope.creditMetaDataSeries.push(a.CREDITCOUNT);
            $scope.debitMetaDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditMetaData.push(a.CREDIT);
            $scope.extraDebitMetaData.push(a.DEBIT);
            $scope.netOutStandingMeta.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCountMeta.push(a.NETOUTSTANDINGCOUNT);
        });
        //console.log($scope.creditMetaDataSeries)
        $scope.initializeMetaChart();
        $scope.loadMetaRCGraph(data);
    }
    $scope.loadMetaRCGraph = function (data) {
        $scope.chart.title.text = 'Recon wise Credit Debit';
        $('#metaContainer').highcharts($scope.metaChart);
        var chartcore = $('#metaContainer').highcharts(), $button = $('#metaButton');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //Recon Code End

    //SourceName Code Begin
    $scope.getMetaSourceName = function (metaRcId) {
        $scope.metaSrcName = [];
        $scope.metaSrcName = null;
        if (metaRcId == '' || metaRcId == null || metaRcId == undefined) {
            $scope.getMetaRecons($scope.metaBcId);
        }
        else{
            $scope.metaSrcName = '';
            $scope.metaSourceName = [];
            var filters = {};
            filters["BUSINESS_CONTEXT_ID"] = '"' + $scope.metaBcId + '"';
            filters["RECON_ID"] = '"' + metaRcId + '"';
            $scope.currentChart = {};
            $scope.currentChart.id = 'SOURCE_NAME';
            $scope.currentChart.filter = filters;
            $scope.currentChart.dataSet = 'ExceptionReport';
            $rootScope.openModalPopupOpen();
            ReportInterfaceService.queryData('SOURCE_NAME', filters, 'ExceptionReport', function (data) {
                $rootScope.openModalPopupClose()
                $scope.metaSrcName = JSON.parse(data);
                $scope.loadMetaSNData($scope.metaSrcName);
            })
        }
    }
    $scope.loadMetaSNData = function (data) {
        var columnDefs = [
            {headerName: "TXN MATCHING STATUS",'width':302, field: "TXN_MATCHING_STATUS"},
            {headerName: "CREDIT",'width':302, field: "CREDIT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT",'width':302, field: "DEBIT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT", 'width':302,field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT", 'width':302,field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardMetaGridOptions.columnDefs = columnDefs;
        $scope.dashboardMetaGridOptions.rowData = data;
        $scope.dashboardMetaGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardMetaGridOptions.api.setRowData(data);
        $scope.buildMetaSNColumnData(data);
    }
    $scope.buildMetaSNColumnData = function (data) {
        $scope.resetMetaData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesMetaData.push(a.TXN_MATCHING_STATUS);
            $scope.creditMetaDataSeries.push(a.CREDITCOUNT);
            $scope.debitMetaDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditMetaData.push(a.CREDIT);
            $scope.extraDebitMetaData.push(a.DEBIT);
            $scope.netOutStandingMeta.push(a.NETOUTSTANDINGAMOUNT);
            $scope.netOutStandingCountMeta.push(a.NETOUTSTANDINGCOUNT);
        });
        $scope.initializeMetaChart();
        $scope.loadMetaSNGraph(data);
    }
    $scope.loadMetaSNGraph = function (data) {
        $scope.chart.title.text = 'TXN MATCHING STATUS wise Credit Debit';
        $('#metaContainer').highcharts($scope.metaChart);
        var chartcore = $('#metaContainer').highcharts(), $button = $('#metaButton');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //SourceName Code End


    //TXN_MATCHING_STATUS Code Begin
    $scope.getMetaTxtMatchStatus = function (metaSrcName) {
        if (metaSrcName == '' || metaSrcName == null || metaSrcName == undefined) {
            $scope.getMetaSourceName($scope.metaRcId);
        }
        var filters = {}
        filters["BUSINESS_CONTEXT_ID"] = '"' + $scope.metaBcId + '"';
        filters["RECON_ID"] = '"' + $scope.metaRcId + '"';
        filters["SOURCE_NAME"] = '"' + metaSrcName + '"';
        $scope.currentChart = {};
        $scope.currentChart.id = 'TXN_MATCHING_STATUS';
        $scope.currentChart.filter = filters;
        $scope.currentChart.dataSet = 'ExceptionReport';
        $rootScope.openModalPopupOpen();
        ReportInterfaceService.queryData('EXCEPTION_STATUS', filters, 'ExceptionReport', function (data) {
            $rootScope.openModalPopupClose()
            $scope.metaTxnMatchStatusId = JSON.parse(data);
            $scope.loadMetaRSData($scope.metaTxnMatchStatusId);
        })
    }
    $scope.loadMetaRSData = function (data) {
        var columnDefs = [
            {headerName: "RECONCILIATION STATUS",'width':302, field: "RECONCILIATION_STATUS"},
            {headerName: "CREDIT", field: "CREDIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "DEBIT", field: "DEBIT",'width':302, filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "CREDIT COUNT",'width':302, field: "CREDITCOUNT", filter: 'number'},
            {headerName: "DEBIT COUNT",'width':302, field: "DEBITCOUNT", filter: 'number'},
            {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                    return formatNumber(params.value)
                }
            }},
            {headerName: "NET OUTSTANDING COUNT",'width':302, field: "NETOUTSTANDINGCOUNT", filter: 'number'}
        ];
        $scope.dashboardGridOptions.columnDefs = columnDefs;
        $scope.dashboardGridOptions.rowData = data;
        $scope.dashboardGridOptions.api.setColumnDefs(columnDefs);
        $scope.dashboardGridOptions.api.setRowData(data);
        $scope.buildMetaRSColumnData(data);
    }
    $scope.buildMetaRSColumnData = function (data) {
        $scope.resetMetaData();
        angular.forEach(data, function (a, b) {
            $scope.categoriesMetaData.push(a.RECONCILIATION_STATUS);
            $scope.creditMetaDataSeries.push(a.CREDITCOUNT);
            $scope.debitMetaDataSeries.push(a.DEBITCOUNT);
            $scope.extraCreditMetaData.push(a.CREDIT);
            $scope.extraDebitMetaData.push(a.DEBIT);
            $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
        });
        $scope.initializeMetaChart();
        $scope.loadMetaRSGraph(data);
    }
    $scope.loadMetaRSGraph = function (data) {
        $scope.chart.title.text = 'Reconciliation status wise Credit Debit';
        $('#metaContainer').highcharts($scope.chart);
        var chartcore = $('#metaContainer').highcharts(), $button = $('#metaButton');
        var creditcount = chartcore.series[0];
        var debitcount = chartcore.series[1];
        var credit = chartcore.series[2];
        var debit = chartcore.series[3];
        var amount = chartcore.series[4];
        var outcount = chartcore.series[5];
        amount.hide();
        debit.hide();
        credit.hide();
        outcount.hide();
        $button.click(function () {
            if (amount.visible) {
                amount.hide();
                debit.hide();
                credit.hide();
                creditcount.show();
                debitcount.show();
                outcount.show();
            } else {
                amount.show();
                debit.hide();
                credit.hide();
                credit.show();
                debit.show();
                outcount.show();
            }
        });
    }
    //TXN_MATCHING_STATUS Code End
    /*-----------------Meta Data Section-----------------*/


    //Aging Report Begin
    $scope.resetAgingData = function () {
        $scope.reconSeries = [];
        $scope.execptionSeries = [];
        $scope.countSeries = [];
        $scope.amountSeries = [];
        $scope.bucketSeries = [];
        $scope.categoriesAgingData = [];
    }
    $scope.initializeAgingChart = function () {
        $scope.agingChart = {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                },
                spacingBottom: 50
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: $scope.categoriesAgingData,
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            /*yAxis: {
             min: 0,
             title: {
             text: 'Count & Amount'
             }
             },*/
            legend: {
                /*layout: 'vertical',*/
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                y: 40
            },
            tooltip: {
                formatter: function () {
                    var index = $.inArray(this.x, $scope.categoriesAgingData);
                    var s = '<b>' + this.x + '</b>';
                    s += '<br/>' + 'Recon Name: ' + $scope.reconSeries[index];
                    s += '<br/>' + 'Exception Status: ' + $scope.execptionSeries[index];
                    s += '<br/>' + 'Count: ' + $scope.countSeries[index];
                    s += '<br/>' + 'Bucket: ' + $scope.bucketSeries[index];
                    s += '<br/>' + 'Amount: ' + formatNumber($scope.amountSeries[index]);
                    return s
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Count',
                data: $scope.countSeries,
            }, {
                name: 'Amount',
                data: $scope.amountSeries,
            }]
        }
    }
    $scope.resetAgingData();
    $scope.bucketSize = '5';
    $scope.agingGridOptions = {
        columnDefs: [],
        rowData: [],
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableStatusBar: true,
        appSpecific: {},
        enableRangeSelection: true,
        groupRowAggNodes: true,
        onRowClicked: function (row) {
            if (angular.isDefined(row.data['RECON_NAME'])){
                $rootScope.businessContextId = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.reconID = row.data['RECON_ID']
                var perColConditions = {};
                perColConditions["BUSINESS_CONTEXT_ID"] = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                $rootScope.openModalPopupOpen()
                localStorage.setItem('saved', JSON.stringify({}));
                BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $rootScope.openModalPopupClose()
                    ////console.log(data.data)
                    angular.forEach(data.data, function (val, key) {
                        if (row.data['RECON_ID'] == val['RECON_ID']) {
                                //console.log(val)
                                angular.forEach($scope.allocated_recons, function (v, k) {
                                    if (v.recon == row.data['RECON_ID']) {
                                        var perColConditions = {};
                                        perColConditions["name"] = v.userpool
                                        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                            console.log(data)
                                            if (data.data.length > 0) {
                                                $rootScope.userpoolRole = data.data[0]['role']
                                                $rootScope.userpoolName= data.data[0]['name']
                                                console.log($rootScope.userpoolRole)
                                            }
                                            $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                            var myObj = {}
                                            myObj['reconId'] = row.data['RECON_ID']
                                            myObj['reconName'] = row.data['RECON_NAME']
                                            myObj['businessId'] = ($scope.bcId != '') ? $scope.bcId : row.data['BUSINESS_CONTEXT_ID']
                                            myObj['businessProcess'] = $scope.businessProcess
                                            myObj['from'] = 'dashboard'
                                            myObj['userRole'] = $rootScope.userpoolRole
                                            myObj['userPoolName'] = $rootScope.userpoolName
                                            myObj['workpool'] = val['WORKPOOL_NAME']
                                            //console.log(myObj)
                                            localStorage.removeItem('summary')
                                            localStorage.removeItem('suspense')
                                            localStorage.setItem('saved', JSON.stringify(myObj));
                                            $location.path("/recons")
                                            //$window.location.href = 'https://'+$location.host()+'/recons'
                                        })
                                    }
                                })
                                //$window.open('https://'+$location.host()+'/recons', '_blank');
                            }
                        })
                })
            }
            var lastSelectedRow = this.appSpecific.lastSelectedRow;
            var shiftKey = row.event.shiftKey,
                ctrlKey = row.event.ctrlKey;

            // If modifier keys aren't pressed then only select the row that was clicked
            if (!shiftKey && !ctrlKey) {
                this.api.deselectAll();
                this.api.selectNode(row.node, true);
            }
            // If modifier keys are used and there was a previously selected row
            else if (lastSelectedRow !== undefined) {
                // Select a block of rows
                if (shiftKey) {
                    var startIndex, endIndex;
                    // Get our start and end indexes correct
                    if (row.rowIndex < lastSelectedRow.rowIndex) {
                        startIndex = row.rowIndex;
                        endIndex = lastSelectedRow.rowIndex;
                    }
                    else {
                        startIndex = lastSelectedRow.rowIndex;
                        endIndex = row.rowIndex;
                    }
                    // Select all the rows between the previously selected row and the
                    // newly clicked row
                    for (var i = startIndex; i <= endIndex; i++) {
                        this.api.selectIndex(i, true, true);
                    }
                }
                // Select one more row
                if (ctrlKey) {
                    this.api.selectIndex(row.rowIndex, true);
                }
            }
            // Store the recently clicked row for future use
            this.appSpecific.lastSelectedRow = row;
        },
        groupSelectsChildren: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressGroups: true,
        showToolPanel: true,
        rowGroupPanelShow: 'always',
        suppressRowClickSelection: true
    };
    $scope.agingReport = function (bucketSize) {
        bucketSize = (bucketSize == undefined) ? '5' : bucketSize;
        $rootScope.openModalPopupOpen();
        ReportInterfaceService.getAgingReport(bucketSize, 'ExceptionReport', function (data) {
            $rootScope.openModalPopupClose()
            var columnDefs = [
                {headerName: "RECON_NAME", 'width':302,field: "RECON_NAME" ,cellStyle: {color: '#428bca'}},
                {headerName: "RECON_ID",'width':302, field: "RECON_ID"},
                {headerName: "BUSINESS_CONTEXT_ID",'width':302, field: "BUSINESS_CONTEXT_ID"},
                {headerName: "WORKPOOL_NAME", 'width':302,field: "WORKPOOL_NAME"},
                {headerName: "EXCEPTION_STATUS",'width':302, field: "EXCEPTION_STATUS"},
                {headerName: "RANGE", 'width':302,field: "RANGE"},
                {headerName: "BUCKET",'width':302, field: "BUCKET", filter: 'number'},
                {headerName: "AMOUNT",'width':302, field: "AMOUNT", cellFilter: 'formatNumber:2','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                        return formatNumber(params.value)
                    }
                }},
                {headerName: "COUNT",'width':302, field: "COUNT", filter: 'number'}
            ];
            $scope.agingGridOptions.columnDefs = columnDefs;
            $scope.agingGridOptions.rowData = data;
            $scope.agingGridOptions.api.setColumnDefs(columnDefs);
            $scope.agingGridOptions.api.setRowData(data);
            $scope.resetAgingData();
            angular.forEach(data, function (a, b) {
                $scope.categoriesAgingData.push(a.RANGE);
                $scope.reconSeries.push(a.RECON_NAME);
                $scope.execptionSeries.push(a.EXCEPTION_STATUS);
                $scope.countSeries.push(a.COUNT);
                $scope.amountSeries.push(a.AMOUNT);
                $scope.bucketSeries.push(a.BUCKET);
            });
            $scope.initializeAgingChart();
            $scope.agingChart.title.text = 'Ageing Chart';
            $('#ageingContainer').highcharts($scope.agingChart);
        })
    }
    //Aging Report End

    //Trend Report Begin
    $scope.trendGridOptions = angular.copy($scope.agingGridOptions);
    $scope.resetTrendData = function () {
        $scope.countSeries = [];
        $scope.amountSeries = [];
        $scope.bucketSeries = [];
        $scope.categoriesAgingData = [];
    }
    $scope.initializeTrendChart = function () {
        $scope.agingChart = {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 15,
                    viewDistance: 25,
                    depth: 40
                },
                spacingBottom: 50
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: $scope.categoriesTrendData,
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            legend: {
                /*layout: 'vertical',*/
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                y: 40
            },
            tooltip: {
                formatter: function () {
                    var index = $.inArray(this.x, $scope.categoriesTrendData);
                    var s = '<b>' + this.x + '</b>';
                    s += '<br/>' + 'Count: ' + $scope.countSeries[index];
                    s += '<br/>' + 'Bucket: ' + $scope.bucketSeries[index];
                    s += '<br/>' + 'Amount: ' + formatNumber($scope.amountSeries[index]);
                    return s
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Count',
                data: $scope.countSeries,
            }, {
                name: 'Amount',
                data: $scope.amountSeries,
            }]
        }
    }
    $scope.resetTrendData();
    $scope.trendReconKey = 'TOTALCOUNT';
    $scope.trendReport = function (recon) {
        recon = (recon == undefined || recon == "" || recon == null ) ? 'dummy' : recon;
        $rootScope.openModalPopupOpen();
        ReportInterfaceService.getTransactionSummaryByDate(recon, function (data) {
            $scope.trendReconId = JSON.parse(data);
            angular.forEach($scope.trendReconId, function(a,b){
                if(recon == 'dummy'){
                    //var recName = a.RECON_ID.split('_');
                    //a.RECON_NAME = recName[0]+" "+recName[3];
                }
                a.TMP_DATE = angular.copy(a.STATEMENT_DATE);
                a.STATEMENT_DATE = $filter('dateFormat')(a.STATEMENT_DATE)
            })
            $scope.tmpTrend = angular.copy($scope.trendReconId);
            $rootScope.openModalPopupClose()
            var columnDefs = []
            if (recon =="dummy"){
                columnDefs.push({headerName: "RECON NAME", 'width':302,field: "RECON_NAME", cellStyle: {color: '#428bca'}},
                {headerName: "RECON ID",'width':302, field: "RECON_ID"},{headerName: "WORKPOOL NAME",'width':302, field: "WORKPOOL_NAME"},{headerName: "BUSINESS CONTEXT ID",'width':302, field: "BUSINESS_CONTEXT_ID"})
            }
            columnDefs.push(
                {headerName: "STATEMENT DATE",'width':302, field: "STATEMENT_DATE"},
                {headerName: "TXN MATCHING STATUS",'width':302, field: "TXN_MATCHING_STATUS"},
                {headerName: "TOTAL COUNT",'width':302, field: "TOTALCOUNT", filter: 'number'},
                {headerName: "NET OUTSTANDING AMOUNT",'width':302, field: "NETOUTSTANDINGAMOUNT", filter: 'number','cellStyle': {'text-align': 'right'},'cellRenderer': function (params) {
                    if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                        return formatNumber(params.value)
                    }
                }})

            $scope.trendGridOptions.columnDefs = columnDefs;
            $scope.trendGridOptions.rowData = $scope.trendReconId;
            $scope.trendGridOptions.api.setColumnDefs(columnDefs);
            $scope.trendGridOptions.api.setRowData($scope.trendReconId);
            $scope.resetData();
            $scope.txnStatusData = [];
            angular.forEach($scope.trendReconId, function (a, b) {
                if(recon == 'dummy'){
                    $scope.categoriesData.push(a.RECON_NAME);
                }else{
                    $scope.categoriesData.push(a.STATEMENT_DATE);
                    $scope.txnStatusData.push(a.TXN_MATCHING_STATUS);
                }
                $scope.txnStatusData.push(a.TXN_MATCHING_STATUS);
                $scope.creditDataSeries.push(a.TOTALCOUNT);
                $scope.netOutStanding.push(a.NETOUTSTANDINGAMOUNT);
            });
            if(recon != 'dummy'){
                $scope.categoriesData = angular.copy($scope.categoriesData.unique());
            }
            $scope.initializeChart();
            if(recon != 'dummy'){
                var recName = recon.split('_');
                $scope.trendReconId = [{'RECON_ID': recon, 'RECON_NAME':recName[0]+" "+recName[3]}]
                $scope.trendRecId = recon;
                $scope.txnStatusData = ["UNMATCHED","AUTHORIZATION_PENDING","AUTOMATCHED","AUTHORIZATION_SUBMISSION_PENDING","FORCE_MATCHED"];
                $scope.hashTable = {};
                angular.forEach($scope.txnStatusData, function(a,b){
                    angular.forEach($scope.categoriesData, function(e,f){
                        $scope.hashTable[e] =(angular.isUndefined($scope.hashTable[e])) ? {} : $scope.hashTable[e];
                        $scope.hashTable[e][a] = (angular.isUndefined($scope.hashTable[e][a])) ? {'TOTALCOUNT': 0, 'NETOUTSTANDINGAMOUNT': 0} : $scope.hashTable[e][a];
                        angular.forEach($scope.tmpTrend, function(c,d){
                            if(e == c.STATEMENT_DATE && a == c.TXN_MATCHING_STATUS){
                                $scope.hashTable[e][a]['TOTALCOUNT'] = c.TOTALCOUNT;
                                $scope.hashTable[e][a]['NETOUTSTANDINGAMOUNT'] =c.NETOUTSTANDINGAMOUNT;
                            }
                        })
                    })
                })
                $scope.trendCountNetAmountSeries($scope.trendReconKey);
            }else{
                $scope.chart.series = [{
                    name: 'Total Count',
                    data: $scope.creditDataSeries,
                    extraData: $scope.txnStatusData,
                }, {
                    name: 'Net Outstanding Amount',
                    data: $scope.netOutStanding,
                    extraData: $scope.txnStatusData,
                }];
                $scope.chart.tooltip = {
                    formatter: function () {
                        var index = $.inArray(this.y, $scope.creditDataSeries);
                        var s = '<b>' + this.x + '</b>';
                        s += '<br/>' + $scope.chart.series[0].name + ': ' + $scope.creditDataSeries[index];
                        s += '<br/>' + $scope.chart.series[1].name + ': ' + formatNumber($scope.netOutStanding[index]);
                        s += '<br/>' + 'Transaction Status: ' + $scope.txnStatusData[index];
                        return s
                    },
                    shared: true,
                    useHTML: true
                }
                $scope.chart.title.text = 'Trend Chart';
                $('#trendContainers').highcharts($scope.chart);
            }
        })
    }
    $scope.trendCountNetAmountSeries = function(param){
        $scope.chart.series = [];
        angular.forEach($scope.txnStatusData, function(a,b) {
            var seriesObj = {};
            seriesObj['name'] = a; seriesObj['data'] = [];
            angular.forEach($scope.categoriesData, function (e, f) {
                angular.forEach($scope.hashTable[e], function(c,d){
                    if(a == d){
                       seriesObj['data'].push(c[param]);
                    }
                });
            });
            $scope.chart.series.push(seriesObj)
        });
        $scope.chart.tooltip = {
            shared: true,
            useHTML: true
        }
        $scope.chart.title.text = (param == 'TOTALCOUNT') ? 'Total Count Chart': 'Net Outstanding Amount Chart';
        $('#trendContainers').highcharts($scope.chart);
    }


    //execution summary code starts here
    $scope.ExgridOptions = {
            columnDefs: [],
            rowData: [],
            rowSelection: 'multiple',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            enableRangeSelection: true,
            groupRowAggNodes: true,
            //onRowSelected: $scope.rowSelectedFunc(),
            //onSelectionChanged: $scope.selectionChangedFunc(),
            onRowClicked: function (row) {
                var lastSelectedRow = this.appSpecific.lastSelectedRow;
                var shiftKey = row.event.shiftKey,
                    ctrlKey = row.event.ctrlKey;

                // If modifier keys aren't pressed then only select the row that was clicked
                if (!shiftKey && !ctrlKey) {
                    this.api.deselectAll();
                    this.api.selectNode(row.node, true);
                }
                // If modifier keys are used and there was a previously selected row
                else if (lastSelectedRow !== undefined) {
                    // Select a block of rows
                    if (shiftKey) {
                        var startIndex, endIndex;
                        // Get our start and end indexes correct
                        if (row.rowIndex < lastSelectedRow.rowIndex) {
                            startIndex = row.rowIndex;
                            endIndex = lastSelectedRow.rowIndex;
                        }
                        else {
                            startIndex = lastSelectedRow.rowIndex;
                            endIndex = row.rowIndex;
                        }
                        // Select all the rows between the previously selected row and the
                        // newly clicked row
                        for (var i = startIndex; i <= endIndex; i++) {
                            this.api.selectIndex(i, true, true);
                        }
                    }
                    // Select one more row
                    if (ctrlKey) {
                        this.api.selectIndex(row.rowIndex, true);
                    }
                }
                // Store the recently clicked row for future use
                this.appSpecific.lastSelectedRow = row;
            },
            enableFilter: true,
            groupSuppressAutoColumn: true,
            groupUseEntireRow: true,
            rememberGroupStateWhenNewData: true,
            //groupHeaders: true,
            //rowHeight: 22,
            //groupSelectsChildren: true,
            toolPanelSuppressValues: true,
            toolPanelSuppressGroups: true,
            showToolPanel: true,
            rowGroupPanelShow: 'always',
            //onModelUpdated: onModelUpdated,
            suppressRowClickSelection: true
        };
    $scope.getBooleanValue = function (cssSelector) {
        //return document.querySelector(cssSelector).checked === true;
    }
    $scope.onBtExportExecution = function () {
        $scope.exportFlag = true
        if ($scope.ExgridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("no data to export")
        }
        var params = {
            skipHeader: $scope.getBooleanValue('#skipHeader'),
            skipFooters: $scope.getBooleanValue('#skipFooters'),
            skipGroups: $scope.getBooleanValue('#skipGroups'),
            allColumns: $scope.getBooleanValue('#allColumns'),
            onlySelected: $scope.getBooleanValue('#onlySelected'),
            processCellCallback:function(params){
                if (params.value != null){
                    if (typeof params.value == "number"){
                        if (params.value.toString().length == 13  && params['column']['colId'].indexOf("AMOUNT") == -1){
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else{
                            return params.value
                        }
                    }
                    else{
                        return params.value
                    }
                }
                ////console.log(params.column.getColumnDef())
            },
            fileName: "Execution-Summary" +'-'+$filter('date')(new Date(Date.now()), 'dd/MM/yyyy') +'.csv',
            //columnSeparator: document.querySelector('#columnSeparator').value
        };
        if ($scope.getBooleanValue('#useCellCallback')) {
            params.processCellCallback = function (params) {
                if (params.value && params.value.toUpperCase) {
                    return params.value.toUpperCase();
                } else {
                    return params.value;
                }
            };
        }
        if ($scope.getBooleanValue('#customHeader')) {
            params.customHeader = '[[[ This ia s sample custom header - so meta data maybe?? ]]]\n';
        }
        if ($scope.getBooleanValue('#customFooter')) {
            params.customFooter = '[[[ This ia s sample custom footer - maybe a summary line here?? ]]]\n';
        }
        if ($scope.exportFlag) {
            $scope.ExgridOptions.api.exportDataAsCsv(params);
        }
    }
     $scope.toEpoch = function (date) {
        return Math.round(new Date(date) / 1000.0);
    };
    $scope.date = {}
    $scope.date.executionStartDate = 0
    $scope.date.executionEndDate = 0
    $scope.date.statementStartDate = 0
    $scope.date.statementEndDate = 0
    $scope.checkSyncMinData = function (data,value) {
        //console.log(data)
        //console.log(value)
        if (data == null){
            if (value == 'st_start_date'){
                $scope.date.executionStartDate = 0
            }if (value == 'st_end_date'){
                $scope.date.statementEndDate = 0
            }if (value == 'ex_start_date'){
                $scope.date.executionStartDate = 0
            }if (value == 'ex_end_date'){
                $scope.date.executionEndDate = 0
            }
        }
    }
    $scope.getexecutionsummary = function () {
        $scope.ExgridOptions.columnDefs = [];
        $scope.ExgridOptions.rowData = [];
        $scope.ExgridOptions.api.setColumnDefs([])
        $scope.ExgridOptions.api.setRowData([])
        $scope.reconBusinessData = []
        //console.log($scope.businessContexts)
        //console.log($scope.reconBusinessData)
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.getExecutionSummary('dummy', {}, function (data) {
            //console.log(data)
            if (JSON.parse(data).length == 0) {
                $rootScope.openModalPopupClose()
                $rootScope.showAlertMsg("No data found for this selection")
            }
            if (JSON.parse(data).length > 0) {
                //console.log(JSON.parse(data))
                $scope.headers = []
                $scope.columnDefs = []
                angular.forEach(JSON.parse(data), function (v, k) {
                    $scope.headers = Object.keys(v)
                    ////console.log(k)
                })
                angular.forEach($scope.headers, function (val, key) {
                    if (val == "PROCESSING_STATE_DATE_TIME" || val == "CREATED_DATE" || val == "EXECUTION_DATE_TIME" || val == "STATEMENT_DATE" || val == "EXECUTION_DATE") {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'suppressMenu': true,
                            //'hide': (val.mandatory == "M") ? false : true,
                            'field': val,'width':302,
                            cellRenderer: function (params) {
                                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                    if (params.value.toString().length == 13) {
                                        if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE"){
                                            var date = $filter('dateTimeFormat')(params.value);
                                        }
                                        else {
                                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                        }
                                    } else {
                                        if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE"){
                                            var date = $filter('dateTimeFormat')(params.value);
                                        }
                                        else {
                                            var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                        }
                                    }
                                    //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                    return date
                                } else {
                                    return "";
                                }
                            }
                        })

                    }
                    else {
                        $scope.columnDefs.push({'headerName': val, 'suppressAggregation': true,'width':302, 'field': val})
                    }
                })
                //console.log($scope.columnDefs)
                $rootScope.openModalPopupClose()
                $scope.reconsData = JSON.parse(data)
                $scope.ExgridOptions.columnDefs = $scope.columnDefs;
                $scope.ExgridOptions.rowData = $scope.reconsData;
                $scope.ExgridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.ExgridOptions.api.setRowData($scope.reconsData)
            }
        })

    }
    $scope.getFilteredExecutionDataByDates = function (date) {
        //Check date validation
        var currentdate = new Date();
        if (date.statementStartDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid statement start date");
            return false;
        } else if (date.statementEndDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid statement end date");
            return false;
        } else if (date.executionStartDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid execution start date");
            return false;
        } else if (date.executionEndDate > currentdate) {
            $rootScope.showAlertMsg("Please select the valid executiion end date");
            return false;
        }
        if ((date.executionStartDate == 0 || date.executionStartDate == null) && (date.executionEndDate ==0 || date.executionEndDate ==null) && (date.statementStartDate == 0 || date.statementStartDate == null) && (date.statementEndDate == 0 || date.statementEndDate == null)){
            $rootScope.showAlertMsg('Please Select Dates to filter')
            return false;
        }
        if (angular.isDefined(date)) {

            var dataD = {}
            dataD['recon_id'] = $scope.reconBusinessData
            dataD['executionStartDate'] = (date.executionStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.executionStartDate) * 1000), 'dd/MM/yy') : 0;
            dataD['executionEndDate'] = (date.executionEndDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.executionEndDate) * 1000), 'dd/MM/yy') : 0;
            dataD['statementStartDate'] = (date.statementStartDate != 0) ? $filter('date')(new Date($scope.toEpoch($scope.date.statementStartDate) * 1000), 'dd/MM/yy') : 0;
            dataD['statementEndDate'] = (date.statementEndDate != 0 ) ? $filter('date')(new Date($scope.toEpoch($scope.date.statementEndDate) * 1000), 'dd/MM/yy') : 0;
            //console.log(dataD)
            if ((dataD['statementStartDate'] != 0 && dataD['statementEndDate'] != 0) && (dataD['executionStartDate'] == 0 && dataD['executionEndDate'] == 0)) {
                $rootScope.openModalPopupOpen()
                $scope.ExgridOptions.columnDefs = [];
                $scope.ExgridOptions.rowData = [];
                $scope.ExgridOptions.api.setColumnDefs([])
                $scope.ExgridOptions.api.setRowData([])
                ReconAssignmentService.getExceptionStatementDate('dummy', dataD, function (data) {
                    //////console.log(data)
                    $rootScope.openModalPopupClose()
                    if (JSON.parse(data).length == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                    if (JSON.parse(data).length > 0) {
                        //console.log(JSON.parse(data))
                        //console.log(JSON.parse(data))
                        $scope.headers = []
                        $scope.columnDefs = []
                        angular.forEach(JSON.parse(data), function (v, k) {
                            $scope.headers = Object.keys(v)
                            ////console.log(k)
                        })
                        angular.forEach($scope.headers, function (val, key) {
                            if (val == "PROCESSING_STATE_DATE_TIME" || val == "CREATED_DATE" || val == "EXECUTION_DATE_TIME" || val == "STATEMENT_DATE" || val == "EXECUTION_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    //'hide': (val.mandatory == "M") ? false : true,
                                    'field': val,'width':302,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({'headerName': val, 'suppressAggregation': true,'width':302, 'field': val})
                            }
                        })
                        //console.log($scope.columnDefs)
                        $rootScope.openModalPopupClose()
                        $scope.reconsData = JSON.parse(data)
                        $scope.ExgridOptions.columnDefs = $scope.columnDefs;
                        $scope.ExgridOptions.rowData = $scope.reconsData;
                        $scope.ExgridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.ExgridOptions.api.setRowData($scope.reconsData)
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            } else if ((dataD['statementStartDate'] == 0 && dataD['statementEndDate'] == 0) && (dataD['executionStartDate'] != 0 && dataD['executionEndDate'] != 0)) {
                $rootScope.openModalPopupOpen()
                $scope.ExgridOptions.columnDefs = [];
                $scope.ExgridOptions.rowData = [];
                $scope.ExgridOptions.api.setColumnDefs([])
                $scope.ExgridOptions.api.setRowData([])
                ReconAssignmentService.getExceptionExecutionDate('dummy', dataD, function (data) {
                    ////console.log(data)
                    $rootScope.openModalPopupClose()
                    if (JSON.parse(data).length == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                    if (JSON.parse(data).length > 0) {
                        //console.log(JSON.parse(data))
                        //console.log(JSON.parse(data))
                        $scope.headers = []
                        $scope.columnDefs = []
                        angular.forEach(JSON.parse(data), function (v, k) {
                            $scope.headers = Object.keys(v)
                            ////console.log(k)
                        })
                        angular.forEach($scope.headers, function (val, key) {
                            if (val == "PROCESSING_STATE_DATE_TIME" || val == "CREATED_DATE" || val == "EXECUTION_DATE_TIME" || val == "STATEMENT_DATE" || val == "EXECUTION_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    //'hide': (val.mandatory == "M") ? false : true,
                                    'field': val,'width':302,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({'headerName': val, 'suppressAggregation': true,'width':302, 'field': val})
                            }
                        })
                        //console.log($scope.columnDefs)
                        $rootScope.openModalPopupClose()
                        $scope.reconsData = JSON.parse(data)
                        $scope.ExgridOptions.columnDefs = $scope.columnDefs;
                        $scope.ExgridOptions.rowData = $scope.reconsData;
                        $scope.ExgridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.ExgridOptions.api.setRowData($scope.reconsData)
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            } else if ((dataD['statementStartDate'] != 0 && dataD['statementEndDate'] != 0) && (dataD['executionStartDate'] != 0 && dataD['executionEndDate'] != 0)) {
                $rootScope.openModalPopupOpen()
                $scope.ExgridOptions.columnDefs = [];
                $scope.ExgridOptions.rowData = [];
                $scope.ExgridOptions.api.setColumnDefs([])
                $scope.ExgridOptions.api.setRowData([])
                ReconAssignmentService.getException_St_Ex_Date('dummy', dataD, function (data) {
                    ////console.log(data)
                    $rootScope.openModalPopupClose()
                    if (JSON.parse(data).length == 0) {
                        $rootScope.showAlertMsg("No data found for this selection")
                    }
                    if (JSON.parse(data).length > 0) {
                        //console.log(JSON.parse(data))
                        //console.log(JSON.parse(data))
                        $scope.headers = []
                        $scope.columnDefs = []
                        angular.forEach(JSON.parse(data), function (v, k) {
                            $scope.headers = Object.keys(v)
                            ////console.log(k)
                        })
                        angular.forEach($scope.headers, function (val, key) {
                            if (val == "PROCESSING_STATE_DATE_TIME" || val == "CREATED_DATE" || val == "EXECUTION_DATE_TIME" || val == "STATEMENT_DATE" || val == "EXECUTION_DATE") {
                                $scope.columnDefs.push({
                                    'headerName': val,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    //'hide': (val.mandatory == "M") ? false : true,
                                    'field': val,'width':302,
                                    cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val == "EXECUTION_DATE_TIME" || val.field == "CREATED_DATE") {
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }
                                })

                            }
                            else {
                                $scope.columnDefs.push({'headerName': val, 'suppressAggregation': true, 'width':302,'field': val})
                            }
                        })
                        //console.log($scope.columnDefs)
                        $rootScope.openModalPopupClose()
                        $scope.reconsData = JSON.parse(data)
                        $scope.ExgridOptions.columnDefs = $scope.columnDefs;
                        $scope.ExgridOptions.rowData = $scope.reconsData;
                        $scope.ExgridOptions.api.setColumnDefs($scope.columnDefs)
                        $scope.ExgridOptions.api.setRowData($scope.reconsData)
                    }
                }, function (err) {
                    $rootScope.showErrormsg(err.msg)
                })
            }
            else {
                $rootScope.showAlertMsg("Please select start and end dates properly")
            }
        }
        else {
            $rootScope.showAlertMsg('Date Fields are empty')
        }

    }
    //$scope.agingReport($scope.bucketSize);

    $scope.getUserRecons = function () {
        $scope.allocated_recons = []
        var perColConditions = {};
        perColConditions["user"] = $rootScope.credentials.userName
        ReconAssignmentService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            angular.forEach(data.data, function (v, k) {
                angular.forEach([].concat.apply([], v.recons.split(',')), function (val, key) {
                    $scope.allocated_recons.push({
                        'business': v.businessContext,
                        'recon': val,
                        'userpool': v.userpool
                    })
                })
            })
        })
    }
    $scope.getUserRecons()
    //recon summary code
    $scope.SumgridOptions = {
            columnDefs: [],
            rowData: null,
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableStatusBar: true,
            appSpecific: {},
            onRowClicked: function (row) {
                //console.log(row)
                if (angular.isDefined(row.data['RECON_ID'])){
                    $rootScope.businessContextId = row.data['BUSINESS_CONTEXT_ID']
                    $rootScope.reconID = row.data['RECON_ID']
                    var perColConditions = {};
                    perColConditions["BUSINESS_CONTEXT_ID"] = row.data['BUSINESS_CONTEXT_ID']
                    $rootScope.openModalPopupOpen()
                    localStorage.setItem('summary', JSON.stringify({}));
                    BusinessService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                        $rootScope.openModalPopupClose()
                        //console.log(data.data)
                        angular.forEach(data.data, function (val, key) {
                            if (row.data['RECON_ID'] == val['RECON_ID']) {
                                    //console.log(val)
                                    angular.forEach($scope.allocated_recons, function (v, k) {
                                    if (v.recon == row.data['RECON_ID']) {
                                        var perColConditions = {};
                                        perColConditions["name"] = v.userpool
                                        UserPoolService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                                            console.log(data)
                                            if (data.data.length > 0) {
                                                $rootScope.userpoolRole = data.data[0]['role']
                                                $rootScope.userpoolName= data.data[0]['name']
                                                console.log($rootScope.userpoolRole)
                                            }
                                            $scope.businessProcess = val['BUSINESS_PROCESS_NAME']
                                            var myObj = {}
                                            myObj['reconId'] = row.data['RECON_ID']
                                            myObj['reconName'] = row.data['RECON_NAME']
                                            myObj['businessId'] = row.data['BUSINESS_CONTEXT_ID']
                                            myObj['businessProcess'] = $scope.businessProcess
                                            myObj['from'] = 'summary'
                                            myObj['userRole'] = $rootScope.userpoolRole
                                            myObj['userPoolName'] = $rootScope.userpoolName
                                            myObj['workpool'] = val['WORKPOOL_NAME']
                                            console.log(myObj)
                                            localStorage.removeItem('saved')
                                            localStorage.removeItem('suspense')
                                            localStorage.setItem('summary', JSON.stringify(myObj));
                                            $location.path("/recons")
                                            //$window.location.href = 'https://' + $location.host() + '/recons'
                                        })
                                    }
                                })
                                }
                            })
                    })
                }
                var lastSelectedRow = this.appSpecific.lastSelectedRow;
                var shiftKey = row.event.shiftKey,
                    ctrlKey = row.event.ctrlKey;

                // If modifier keys aren't pressed then only select the row that was clicked
                if (!shiftKey && !ctrlKey) {
                    this.api.deselectAll();
                    this.api.selectNode(row.node, true);
                }
                // If modifier keys are used and there was a previously selected row
                else if (lastSelectedRow !== undefined) {
                    // Select a block of rows
                    if (shiftKey) {
                        var startIndex, endIndex;
                        // Get our start and end indexes correct
                        if (row.rowIndex < lastSelectedRow.rowIndex) {
                            startIndex = row.rowIndex;
                            endIndex = lastSelectedRow.rowIndex;
                        }
                        else {
                            startIndex = lastSelectedRow.rowIndex;
                            endIndex = row.rowIndex;
                        }
                        // Select all the rows between the previously selected row and the
                        // newly clicked row
                        for (var i = startIndex; i <= endIndex; i++) {
                            this.api.selectIndex(i, true, true);
                        }
                    }
                    // Select one more row
                    if (ctrlKey) {
                        this.api.selectIndex(row.rowIndex, true);
                    }
                }
                // Store the recently clicked row for future use
                this.appSpecific.lastSelectedRow = row;
            },
            enableFilter: true,
            groupSuppressAutoColumn: false,
            groupUseEntireRow: false,
            rememberGroupStateWhenNewData: true
        };
    var filterCount = 0;
    $scope.onFilterChangedSum = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.SumgridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }
    $scope.onFilterChangedEx = function (newFilter) {
        filterCount++;
        var filterCountCopy = filterCount;
        setTimeout(function () {
            if (filterCount === filterCountCopy) {
                $scope.ExgridOptions.api.setQuickFilter(newFilter);
            }
        }, 300);
    }
    $scope.getsummary = function () {
        //$scope.SumgridOptions.columnDefs = [];
        //$scope.SumgridOptions.rowData = [];
        //$scope.SumgridOptions.api.setColumnDefs([])
        //$scope.SumgridOptions.api.setRowData([])
        $rootScope.openModalPopupOpen()
        ReconAssignmentService.getSummary('dummy', {}, function (data) {
            ////console.log(data)
            ////console.log(typeof data)
             data = data['summary_data']

            if (JSON.parse(data).length == 0) {
                $rootScope.openModalPopupClose()
                $rootScope.showAlertMsg("No data found for this selection")
            }
            if (JSON.parse(data).length > 0) {
                ////console.log(JSON.parse(data))
                $scope.headers = []
                $scope.columnDefs = []
                angular.forEach(JSON.parse(data), function (v, k) {
                    $scope.headers = Object.keys(v)
                    ////console.log(k)
                })
                angular.forEach($scope.headers, function (val, key) {
                    ////console.log(val)
                    if (val == "RECON_NAME"){
                          $scope.columnDefs.push({'headerName': val, 'width': 302, 'suppressAggregation': true, 'field': val, cellStyle: {color: '#428bca'}/*,cellRenderer: function(params) {
                                if (params.value!=null && params.value!= '' && params.value != undefined) {
                                    if (angular.isDefined(params.data) && params.data != null){
                                        if (angular.isDefined(params.data['RECON_ID'])){
                                            return '<a href="#" >'+params.value+'</a>'
                                        }
                                    }
                                }
                            }*/})
                    }
                    else if (val == "AUTOMATCHED AMOUNT" || val == "UNMATCHED AMOUNT" || val == "AUTHORIZATION_PENDING AMOUNT" || val == "CREDIT_AUTHORIZATION_PENDING_AMOUNT" || val == "CREDIT_AUTOMATCHED_AMOUNT" || val == "CREDIT_UNMATCHED_AMOUNT" ||  val == "DEBIT_AUTHORIZATION_PENDING_AMOUNT" || val == "DEBIT_AUTOMATCHED_AMOUNT" || val == "DEBIT_UNMATCHED_AMOUNT") {
                        $scope.columnDefs.push({
                            'headerName': val,
                            'suppressAggregation': true,
                            'field': val, 'width': 302,
                            'cellStyle': {'text-align': 'right'},
                            'cellRenderer': function (params) {
                                if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                    return params.value.toLocaleString()
                                }
                                else{
                                    return params.value
                                }
                            }
                        })
                    }

                    else if (val == "EXECUTION_DATE_TIME" ||  val == "STATEMENT_DATE") {
                               $scope.columnDefs.push({
                                    'headerName': val,
                                    'suppressAggregation': true,
                                    'suppressMenu': true,
                                    'field': val, 'width': 302
                                    /*cellRenderer: function (params) {
                                        if (angular.isDefined(params.value) && params.value != null && params.value != undefined) {
                                            if (params.value.toString().length == 13) {
                                                if (val == "EXECUTION_DATE_TIME"){
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                                                }
                                            } else {
                                                if (val == "EXECUTION_DATE_TIME"){
                                                    var date = $filter('dateTimeFormat')(params.value);
                                                }
                                                else {
                                                    var date = $filter('date')(new Date(params.value * 1000), 'dd/MM/yyyy');
                                                }
                                            }
                                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                                            return date
                                        } else {
                                            return "";
                                        }
                                    }*/
                                })

                            }
                     else{
                        $scope.columnDefs.push({'headerName': val, 'width': 302, 'suppressAggregation': true, 'field': val})
                    }

                })
                //console.log($scope.columnDefs)

                $scope.reconsData = JSON.parse(data)

                angular.forEach($scope.columnDefs, function(val,key){
                    if(val.headerName == 'WORKPOOL_NAME') {
                        $scope.workPoolIndex = key
                    }
                })

                $scope.workpoolObj = $scope.columnDefs[$scope.workPoolIndex]
		        $scope.workpoolObj['sort'] = 'asc'
                $scope.columnDefs.splice($scope.workPoolIndex,1)
                console.log($scope.workpoolObj)
                $scope.columnDefs.unshift($scope.workpoolObj)
                $scope.SumgridOptions.columnDefs = $scope.columnDefs;
                $scope.SumgridOptions.rowData = $scope.reconsData;
                $scope.SumgridOptions.api.setColumnDefs($scope.columnDefs)
                $scope.SumgridOptions.api.setRowData($scope.reconsData)
                $timeout(function () {
                    $rootScope.openModalPopupClose()
                }, 12000)
            }
        })
    }
    $scope.getsummary()
    $scope.resetSumColState = function() {
        $scope.SumgridOptions.columnApi.autoSizeColumns($scope.headers);
    }

    $scope.getBooleanValue = function (cssSelector) {
        //return document.querySelector(cssSelector).checked === true;
    }
    $scope.onBtExportSummary = function () {
        $scope.exportFlag = true
        if ($scope.SumgridOptions.rowData.length == 0) {
            $scope.exportFlag = false
            $rootScope.showAlertMsg("no data to export")
        }
        var params = {
            skipHeader: $scope.getBooleanValue('#skipHeader'),
            skipFooters: $scope.getBooleanValue('#skipFooters'),
            skipGroups: $scope.getBooleanValue('#skipGroups'),
            allColumns: $scope.getBooleanValue('#allColumns'),
            onlySelected: $scope.getBooleanValue('#onlySelected'),
            processCellCallback:function(params){
                if (params.value != null){
                    if (typeof params.value == "number"){
                        if (params.value.toString().length == 13  && params['column']['colId'].indexOf("AMOUNT") == -1){
                            var date = $filter('date')(new Date(params.value), 'dd/MM/yyyy');
                            //date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
                            return date
                        }
                        else{
                            return params.value
                        }
                    }
                    else{
                        return params.value
                    }
                }
                ////console.log(params.column.getColumnDef())
            },
            fileName: "Summary" +'-'+$filter('date')(new Date(Date.now()), 'dd/MM/yyyy') +'.csv',
            //columnSeparator: document.querySelector('#columnSeparator').value
        };
        if ($scope.getBooleanValue('#useCellCallback')) {
            params.processCellCallback = function (params) {
                if (params.value && params.value.toUpperCase) {
                    return params.value.toUpperCase();
                } else {
                    return params.value;
                }
            };
        }
        if ($scope.getBooleanValue('#customHeader')) {
            params.customHeader = '[[[ This ia s sample custom header - so meta data maybe?? ]]]\n';
        }
        if ($scope.getBooleanValue('#customFooter')) {
            params.customFooter = '[[[ This ia s sample custom footer - maybe a summary line here?? ]]]\n';
        }
        if ($scope.exportFlag) {
            $scope.SumgridOptions.api.exportDataAsCsv(params);
        }
    }

    //grid toggle
    $rootScope.outstandingGridClass = "col-lg-6"
    $scope.toggleGrid = function () {
        if ($rootScope.outstandingGridClass == "col-lg-6"){
            $rootScope.outstandingGridClass = "col-lg-12"
        }else if ($rootScope.outstandingGridClass == "col-lg-12"){
            $rootScope.outstandingGridClass = "col-lg-6"
        }
    }
    $scope.getDays = function() {
        $scope.uName = $rootScope.credentials.userName
        console.log($scope.uName)
        var perColConditions = {};
        perColConditions['userName'] = $scope.uName
        console.log(perColConditions)
        UsersService.get(undefined,{"query": {"perColConditions": perColConditions}}, function (data) {
            console.log(data)
            if (data['total'] > 0){
                angular.forEach(data['data'], function (val, key) {
                    $scope.lastUpdated = val['lastPwdUpdt']
                    $scope.lastLogin = val['lastLogin']

                })
            }

            $scope.last = $filter('date')(new Date($scope.lastLogin * 1000) , 'dd/MM/yyyy')
            var z = $scope.last.split("/")
            var lastLoginDate = new Date(z[2], z[1]-1, z[0]);
            $scope.lastFormattedUpdated = $filter('date')(new Date($scope.lastUpdated * 1000), 'dd/MM/yyyy');
            var x = $scope.lastFormattedUpdated.split("/");
            var date1=new Date(x[2],(x[1]-1),x[0]);
            $scope.todayDate = (new Date().getTime())/1000
            $scope.todayFormattedDate = $filter('date')(new Date($scope.todayDate *1000) ,'dd/MM/yyyy')
            var y = $scope.todayFormattedDate.split("/");
            var date2=new Date(y[2],(y[1]-1),y[0]);
            var one_day=1000*60*60*24;
            var expiryDays = 45;
            $scope._Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day));
            $scope.expiryDate = (expiryDays - $scope._Diff)
            if ($scope.expiryDate == 10 || $scope.expiryDate <= 10) {
                if (lastLoginDate.getTime() !== date2.getTime() ){
                     $rootScope.openModalPopupOpen();
                     $rootScope.showAlertMsg('Your password Will Expire within : ' + ($scope.expiryDate)  + ' days')
                     $timeout(function () {
                         $scope.cancel();
                      }, 1500);
               }
            }
        })
    }


    $scope.getDays()
    //$(window).resize(function () {
    //    var height = $scope.chart.height
    //    var width = $("#container").width() / 2
    //    $scope.chart.setSize(width, height, doAnimation = true);
    //});

    $scope.export = function (gridOptions, reportName) {
        $scope.customHeader = "Report Name :," + reportName + '\n'
            + "Date-Time :," + new Date() + '\n'
            + "User :," + $rootScope.credentials.userName + '\n\n'

        var params = {
            customHeader: $scope.customHeader,
            fileName: reportName + '-' + '.csv'
        };
        gridOptions.api.exportDataAsCsv(params);
    }

    $rootScope.showSuspenseSummary = false
};
