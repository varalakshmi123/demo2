/**
 * Created by varun on 21/6/17.
 */
function auditsController($scope, $rootScope, $http, $location, $routeParams, $timeout, $route, $window, $modal, $filter, UsersService, AuditsService) {
    $scope.auditsOptions = {};
    $scope.auditsOptions.init = false;
    $scope.auditsOptions.isLoading = true;
    $scope.auditsOptions.isTableSorting = "userName";
    $scope.auditsOptions.sortField = "userName";

    $rootScope.minQuotaLimitAdmin = 0;
    $rootScope.minQuotaLimit = 1;
    $scope.getApprovedUsers = function () {
        $scope.userList = [];
        $scope.auditsOptions.searchFields = {}
        $scope.auditsOptions.service = AuditsService;
        $scope.auditsOptions.sortField = 'created';
        $scope.auditsOptions.sortBy = -1;
        $scope.auditsOptions.headers = [
            [
                {name: "Event Name", searchable: true, sortable: true, dataField: "type", colIndex: 0},
                {name: "Actioned On", searchable: true, sortable: true, dataField: "actionOn", colIndex: 1},
                {name: "Operator Name", searchable: true, sortable: true, dataField: "createdBy", colIndex: 2},
                {
                    name: "Audit Time",
                    searchable: true,
                    sortable: true,
                    dataField: "createDateTime",
                    filter: "date:'dd-MM-yyyy HH:mm:ss'",
                    colIndex: 3
                },
                // {name: "Approved By", searchable: true, sortable: true, dataField: "approverName", colIndex: 2},
            ]
        ];
        $scope.auditsOptions.reQuery = true;

    }
    $scope.getApprovedUsers()
};
