import pandas as pd
from pymongo import MongoClient
from datetime import datetime
import os
import re
import config

feedDef = {'Asset': 'ASSET_#%d%m%Y#.xls', 'TrialBal': 'TRIALBAL_#%d%m%Y#_[0-9]{6}.txt', 'SA': 'SA_#%Y%m%d#.txt',
           'TD': 'TD_#%Y%m%d#.txt'}


class AdfOglReport():
    def __init__(self):
        connection = MongoClient('localhost', 27017)
        self.db = connection[config.usersDatabaseName]
        self.pattern=pd.read_csv('./meta/AdfOglReports.csv',sep='|',names=['a','b'])
        self.dict1=self.pattern.to_dict(orient='split')
        self.dict1=dict(self.dict1['data'])


    def findFeedFiles(self, stmtDate, feeds):
        files = []
        stmtDate = datetime.strptime(stmtDate, '%d-%m-%Y')
        for fileName in feeds:
            if "#" in feedDef[fileName]:
                fpattern = feedDef[fileName]
                indx1 = fpattern.index('#')
                indx2 = fpattern.index('#', indx1 + 1)
                dpattern = fpattern[indx1 + 1:indx2]
                dstr = stmtDate.strftime(dpattern)
                fpattern = fpattern.replace('#' + dpattern + '#', dstr)
                file = [f for f in os.listdir(config.contentDir + '/Adf_OGL_report_Files/') if re.search(fpattern, f)]
                if file == []:
                    return fileName
                files.extend(file)
        return files

    def readFiles(self, file, header=[], sep='|'):
        filePath = config.contentDir + '/Adf_OGL_report_Files/' + file
        if header == []:
            if file.split('.')[1] in ['xls', 'xlsx']:
                df = pd.read_excel(filePath)
            elif file.split('.')[1] in ['txt', 'csv']:
                df = pd.read_csv(filePath, sep=sep, low_memory=False)
        else:
            if file.split('.')[1] in ['xls', 'xlsx']:
                df = pd.read_excel(filePath, names=header)
            elif file.split('.')[1] in ['txt', 'csv']:
                df = pd.read_csv(filePath, sep=sep, low_memory=False, names=header)
        return df

    def calculate(self, df1, df2):
        dr = df2[df2["DEBIT_CREDIT_FLAG"] == "DR"]
        cr = df2[df2["DEBIT_CREDIT_FLAG"] == "CR"]

        cr = cr.groupby("PRODUCT_CODE", as_index=False)["BALANCE_AMT"].sum()
        dr = dr.groupby("PRODUCT_CODE", as_index=False)["BALANCE_AMT"].sum()

        df3 = pd.merge(cr, dr, on="PRODUCT_CODE", how="outer")
        df3.rename(columns={'BALANCE_AMT_x': 'CR', 'BALANCE_AMT_y': 'DR'}, inplace=True)
        df3.fillna(0.00, inplace=True)

        df3['Grand Total'] = df3['DR'] - df3['CR']
        df3["PRODUCT_CODE"] = df3["PRODUCT_CODE"].astype(str)

        df4 = pd.merge(df3[["PRODUCT_CODE", "Grand Total"]], df1[["PRODUCT_CODE", "2"]], on="PRODUCT_CODE", how="outer")
        df4.rename(columns={"Grand Total": "OGL", "2": "CDR"}, inplace=True)
        df4.fillna(0.00, inplace=True)

        df4["DIFFERENCE"] = df4["OGL"] + df4["CDR"]
        df4.loc[len(df4)] = df4.sum()
        df4.loc[len(df4) - 1, 'PRODUCT_CODE'] = 'TOTAL'
        return True, df4

    def Novopay_OGL(self, stmtDate):
        feeds = ['Asset', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('ASSET'):
                df1 = self.readFiles(file)
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1 = df1[df1['BANK_SOURCE_SYS'] == 'NVP']
            df1.rename(columns={"product_code": "PRODUCT_CODE"}, inplace=True)
            df1["PRODUCT_CODE"] = df1["PRODUCT_CODE"].astype(str)
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['Novopay_Product'])]

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['Novopay_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['Novopay_Product'])]

            dr = df2[df2["DEBIT_CREDIT_FLAG"] == "DR"]
            cr = df2[df2["DEBIT_CREDIT_FLAG"] == "CR"]

            cr = cr.groupby("PRODUCT_CODE", as_index=False)["BALANCE_AMT"].sum()
            dr = dr.groupby("PRODUCT_CODE", as_index=False)["BALANCE_AMT"].sum()

            df3 = pd.merge(cr, dr, on="PRODUCT_CODE", how="outer")
            df3.rename(columns={'BALANCE_AMT_x': 'CR', 'BALANCE_AMT_y': 'DR'}, inplace=True)
            df3.fillna(0.00, inplace=True)

            df3['Grand Total'] = df3['DR'] - df3['CR']
            df3["PRODUCT_CODE"] = df3["PRODUCT_CODE"].astype(str)

            df4 = pd.merge(df3[["PRODUCT_CODE", "Grand Total"]], df1[["PRODUCT_CODE", "TOTAL"]], on="PRODUCT_CODE",
                           how="outer")
            df4.rename(columns={"Grand Total": "OGL", "TOTAL": "CDR"}, inplace=True)
            df4.fillna(0.00, inplace=True)

            df4["DIFFERENCE"] = df4["OGL"] - df4["CDR"]
            df4.loc[len(df4)] = df4.sum()
            df4.loc[len(df4) - 1, 'PRODUCT_CODE'] = 'TOTAL'
            return True, df4
        else:
            return False, 'No Data Found'

    def SA_credit_OGL(self, stmtDate):
        feeds = ['SA', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('SA'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['SA_Credit_Natural'])]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['SA_Credit_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['SA_Credit_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['SA_Credit_Product'])]

            return self.calculate(df1, df2)
        else:
            return False, 'No Data Found'

    def SA_debit_OGL(self, stmtDate):
        feeds = ['SA', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('SA'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['SA_Debit_Natural'])]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['SA_Debit_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['SA_Debit_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['SA_Debit_Product'])]
            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'

    def TD_INR_OGL(self, stmtDate):
        feeds = ['TD', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('TD'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[(df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_INR_Natural']) & (df1['3'] == 'INR'))]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_INR_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_INR_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_INR_Product'])]

            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'

    def TD_GBP_OGL(self, stmtDate):
        feeds = ['TD', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('TD'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[(df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_GBP_Natural']) & (df1['3'] == 'GBP'))]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_GBP_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_GBP_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_GBP_Product'])]

            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'

    def TD_USD_OGL(self, stmtDate):
        feeds = ['TD', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('TD'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[(df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_USD_Natural']) & (df1['3'] == 'USD'))]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_USD_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_USD_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_USD_Product'])]

            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'

    def TD_AUD_OGL(self, stmtDate):
        feeds = ['TD', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('TD'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[(df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_AUD_Natural']) & (df1['3'] == 'AUD'))]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_AUD_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_AUD_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_AUD_Product'])]

            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'

    def TD_EUR_OGL(self, stmtDate):
        feeds = ['TD', 'TrialBal']
        feedFiles = self.findFeedFiles(stmtDate, feeds)

        if isinstance(feedFiles, str):
            return False, 'Feed File Not Found for ' + feedFiles

        for file in feedFiles:
            if file.startswith('TD'):
                df1 = self.readFiles(file, header=['1', '2', '3'])
            else:
                df2 = self.readFiles(file)

        if len(df1) > 0 and len(df2) > 0:
            df1['NATURAL_CODE'] = df1["1"].astype(str).str[:5]
            df1=df1[(df1["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_EUR_Natural']) & (df1['3'] == 'EUR'))]

            df1['PRODUCT_CODE'] = df1["1"].astype(str).str[5:11]
            df1=df1[df1["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_EUR_Product'])]

            df1 = df1.groupby("PRODUCT_CODE", as_index=False)["2"].sum()

            df2["NATURAL_CODE"] = df2["GL_NO"].str[12:17]
            df2=df2[df2["NATURAL_CODE"].apply(lambda x: x in self.dict1['TD_EUR_Natural'])]

            df2["PRODUCT_CODE"] = df2["GL_NO"].str[17:22]
            df2=df2[df2["PRODUCT_CODE"].apply(lambda x: x in self.dict1['TD_EUR_Product'])]

            return self.calculate(df1, df2)

        else:
            return False, 'No Data Found'
