import csv
import datetime
import os
import sys

import pandas as pd
from traceback import format_exc
import sql
import sql_config
import cx_Oracle

output_file_path = "/usr/local/nginx/www/erecon/reports/"


def exportReport(reconId, startDate, endDate):
    try:
        #change per sql database
        ip = sql_config.ip
        port = sql_config.port
        SID = sql_config.SID

        dsn_tns = cx_Oracle.makedsn(ip, port, SID)
        connection = cx_Oracle.connect(sql_config.connection_name, sql_config.connection_pwd, dsn_tns)

        extract_path = output_file_path + reconId
        if not os.path.exists(extract_path):
            os.mkdir(extract_path)

        recon_name = pd.read_sql("select * from recon_details where recon_id = '%s'" % reconId, connection)[
            'RECON_NAME'][0]

        to_date = datetime.datetime.now().strftime("%b-%d-%Y")
        export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
        fileName = reconId + '-' + to_date + '-All.csv'
        query = {'statementStartDate': startDate, "statementEndDate": endDate, "recon_id": reconId, "suspenseFlag":False}
        (status_rec, reconData) = sql.read_col_query_matched_statement_date(connection, query)
        print 'Total records found = %s' % len(reconData)
        with open(os.path.join(extract_path + "/" + fileName),
                  'w') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerows(
                [['Recon_Name :', recon_name], ['Recon_ID', reconId],
                 ['Date-Time', export_date_time], ['User', "Recon Admin"],
                 ['Status', 'All'],
                 ['', '']])
            reconData.to_csv(outcsv, index=False, date_format='%d/%m/%Y')
        print '==========================================='
        print "Output report available in path %s" % (extract_path + "/" + fileName)
        print '==========================================='
    except Exception:
        print format_exc()
    finally:

        print 'Closing open sql connection'
        connection.close()


if __name__ == '__main__':
    reconId = sys.argv[1]
    startDate = sys.argv[2]
    endDate = sys.argv[3]

    try:
        datetime.datetime.strptime(startDate, '%d/%m/%Y')
        datetime.datetime.strptime(endDate, '%d/%m/%Y')
    except Exception as e:
        print 'Invalid start date/end date format expected format DD/MM/YY'
        exit(0)

    exportReport(reconId, startDate, endDate)
