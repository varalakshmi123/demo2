import datetime
import json
import sys
import time
import traceback
import uuid

import config
import cx_Oracle
import datatypes
import numpy as np
import pandas
import sql
from bson import json_util
from dateutil.parser import parse


class update_cash_output_txn():
    def __init__(self, type):
        ip = '10.1.37.101'
        port = 1653
        SID = 'recondb'
        dsn_tns = cx_Oracle.makedsn(ip, port, SID)
        self.connection = cx_Oracle.connect('algoreconutil', 'AE9_WSTr_3zb65', dsn_tns)
        self.recon_execution_id = '119315'
        self.recon_id = 'SUSP_CASH_APAC_20382'
        self.auth_linkid = '11823299808173818356'
        self.comments = 'Force Match based on confirmation received from OPS Team'
        self.authorizer = 'Authoriser Role Pool'

        self.run(type)

    def run(self, type):
        try:
            if type == 'FMA':
                status, df = self.read_col_query_unmatched(connection=self.connection)
                query = dict()
                json_out = df.to_json(orient='records')
                query['selected_rec'] = json_out
                query['groupAndForce'] = True
                query['comments'] = self.comments
                query['reason_code'] = 'Manual Match Entries'
                query['singleSideMatch'] = False
                query["groupForceMatchAuth"] = True

                self.groupColumns('dummy', query)
                self.connection.commit()
            elif type == 'AUTH':
                status, df = self.read_col_query_waitingForAuthorization(connection=self.connection)
                query = dict()
                json_out = df.to_json(orient='records')
                query['selected_rec'] = json_out
                query['comments'] = self.comments
                query['reason_code'] = 'Manual Matching Entries'
                self.authorizeRecords('dummy', query)
                self.connection.commit()
            elif type == 'REJ':
                status, df = self.read_col_query_waitingForAuthorization(connection=self.connection)
                query = dict()
                json_out = df.to_json(orient='records')
                query['selected_rec'] = json_out
                query['comments'] = self.comments
                query['reason_code'] = 'Manual Reject Entries'
                self.rejectRecords('dummy', query)
                self.connection.commit()
        except Exception:
            traceback.print_exc()
            self.connection.rollback()
        finally:
            self.connection.close()

    def groupColumns(self, id, query):
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        status_commit = False
        validataion_status = True
        tmp_var = json.loads(cloned_val)

        if query is not None and query['selected_rec'] is not None and validataion_status:
            status, recon_con = True, self.connection
            if status:
                tmp_var = json.loads(query['selected_rec'])
                for rec in tmp_var:
                    rec['UPDATED_BY'] = 'IDFC_IT'
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)

                (status, updata) = self.updateSqlData(query=newData, connection=recon_con)
                linkid = str(uuid.uuid4().int & (1 << 64) - 1)
                cloned_val = json.loads(cloned_val)
                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = 'IDFC_IT'
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "GROUPED"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 3
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_GROUPED"

                    rec_old['LINK_ID'] = linkid
                    rec_old['IPADDRESS'] = "0.0.0.0"
                    rec_old['GROUPED_FLAG'] = 'Y'
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = 1
                    rec_old["MATCHING_EXECUTION_STATUS"] = "OUTSTANDING"
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"

                    if 'PREV_LINK_ID' in rec_old:
                        del rec_old['PREV_LINK_ID']

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)

                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)

                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)

                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0

                print 'Grouping Entries Initiated'
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                exceptionData = dict()
                exceptionData["EXCEPTION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exceptionData["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exceptionData['CLEARING_DATE'] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                            '%Y-%m-%d %H:%M:%S')
                exceptionData["COMMENTS"] = query['comments']
                exceptionData["CREATED_BY"] = 'IDFC_IT'
                exceptionData["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                     '%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_GROUP_INDICATOR"] = "MERGED"
                exceptionData["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exceptionData["EXCEPTION_STATUS"] = "OPEN"
                exceptionData["IPADDRESS"] = '0.0.0.0'
                exceptionData["LINK_ID"] = str(linkid)
                exceptionData["REASON_CODE"] = query['reason_code']
                if "RECON_EXECUTION_ID" in oldData[0]:
                    exceptionData["RECON_EXECUTION_ID"] = oldData[0]["RECON_EXECUTION_ID"]
                exceptionData["RECON_ID"] = oldData[0]["RECON_ID"]
                exceptionData["RECORD_STATUS"] = "ACTIVE"
                exceptionData["RECORD_VERSION"] = 1
                exceptionData["SESSION_ID"] = 'df9232sfsdf_afdsf32sdfdfsdf'
                oldData_ex.append(exceptionData)
                parseDates = ["CREATED_DATE", "EXCEPTION_CREATED_DATE", "CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')

                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",
                                                                 connection=recon_con)
                exception_alloc_data = dict()
                exception_alloc_data["ACTION_BY"] = 'IDFC_IT'
                exception_alloc_data["ALLOCATED_BY"] = "SYSTEM"
                exception_alloc_data["ALLOCATION_REMARKS"] = query["comments"]
                exception_alloc_data["ALLOCATION_STATUS"] = "UNALLOCATED"
                exception_alloc_data["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exception_alloc_data["CREATED_BY"] = 'IDFC_IT'
                exception_alloc_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                  '%Y-%m-%d %H:%M:%S')
                exception_alloc_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exception_alloc_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exception_alloc_data["EXCEPTION_ID"] = exceptionData["EXCEPTION_ID"]
                exception_alloc_data["EXCEPTION_RESOLUTION_STATUS"] = "OPEN"
                exception_alloc_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exception_alloc_data["IPADDRESS"] = "192.168.2.248"
                exception_alloc_data["RECON_ID"] = oldData[0]["RECON_ID"]
                exception_alloc_data["RECORD_STATUS"] = "ACTIVE"
                exception_alloc_data["RECORD_VERSION"] = 1
                exception_alloc_data["SESSION_ID"] = 'df9232sfsdf_afdsf32sdfdfsdf'
                exception_alloc_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                     '%Y-%m-%d %H:%M:%S')
                # exception_alloc_data["USER_POOL_ID"] = ""
                exception_alloc_data["WORK_POOL_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                oldData_allocation.append(exception_alloc_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')

                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",
                                                                       connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)

                if query["groupAndForce"]:
                    print("Grouping done entered successfully, proceeding to force matching.")
                    query_link_id = dict()
                    query_link_id["LINK_ID"] = linkid
                    query_link_id["RECON_EXECUTION_ID"] = self.recon_execution_id
                    columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
                    columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                                  "WORK_POOL_ID"]
                    columnsToConvertCash = ["LINK_ID", "RAW_LINK", "PARTICIPATING_RECORD_TXN_ID", "OBJECT_OID",
                                            "OBJECT_ID", "RECON_EXECUTION_ID"]
                    (status_exception, exmaster_data) = sql.getExceptionMasterDetails(query=query_link_id,
                                                                                      connection=recon_con)
                    if status_exception:
                        for x in columnsToConvertMaster:
                            if x in exmaster_data.columns:
                                exmaster_data[x] = exmaster_data[x].apply(str, 1)
                        exmaster_data = exmaster_data.to_json(orient='records')
                    (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query_link_id,
                                                                                               connection=recon_con)
                    if status_allocation:
                        for x in columnsToConvertAllocation:
                            if x in exallocation_data.columns:
                                exallocation_data[x] = exallocation_data[x].apply(str, 1)
                        exallocation_data = exallocation_data.to_json(orient='records')
                    (status_cash, cash_data) = sql.getTxnDetails(query=query_link_id, recon_id=self.recon_id,
                                                                 connection=recon_con)
                    if status_cash:
                        for x in columnsToConvertCash:
                            if x in cash_data.columns:
                                cash_data[x] = cash_data[x].apply(str, 1)
                        cash_data = cash_data.to_json(orient='records')
                if query['groupAndForce']:
                    queryForce = dict()
                    queryForce['selected_rec'] = cloned_val
                    queryForce['comments'] = self.comments
                    queryForce["reason_code"] = 'Manual matching entries'
                    (status_force, forcematchData) = self.forceMatchingColumns('dummy', query=queryForce,
                                                                               exmaster_data=exmaster_data,
                                                                               exallocation_data=exallocation_data,
                                                                               singleEntryMatchIndictor=query[
                                                                                   'singleSideMatch'],
                                                                               sql_connection=recon_con)
                    if status_force and query["groupForceMatchAuth"]:
                        print("Submit for authorization process initiated .")
                        queryAuth = dict()
                        queryAuth['comments'] = self.comments
                        queryAuth['authorizer'] = self.authorizer

                        (status_auth, auth_data) = self.submitForAuthorization('dummy', query=queryAuth,
                                                                               oldData=cloned_val,
                                                                               exmaster_data=forcematchData[
                                                                                   'oldData_ex'],
                                                                               exallocation_data=forcematchData[
                                                                                   'oldData_allocation'],
                                                                               singleEntryMatchIndictor=query[
                                                                                   'singleSideMatch'],
                                                                               sql_connection=recon_con)

                return True, 'Success'
            else:
                return False, ''
        else:
            if not validataion_status:
                return True, ''
            else:
                return True, 'No records selected'

    def submitForAuthorization(self, id, query, oldData=None, exallocation_data=None, exmaster_data=None,
                               singleEntryMatchIndictor=False, sql_connection=None):
        # logger.info("submit for authorization- -----------------------------------------------")
        # logger.info(query)
        oldData_auth = []
        newData = []
        oldData_ex_auth = []
        oldData_allocation_auth = []
        (status, recon_con) = True, sql_connection
        txn_data = {}

        txn_data = oldData if oldData is not None else query['selected_rec']
        # retrieve unique list ID set from the selected txn list
        # uniqueLinkID = {each['LINK_ID']:each["LINK_ID"] for each in txn_data}.values()

        uniqueLinkID = pandas.DataFrame(txn_data)['LINK_ID'].unique()

        if exmaster_data is None and exallocation_data is None:
            query_link_id = dict()
            query_link_id["LINK_ID"] = "','".join([str(link_id) for link_id in uniqueLinkID])
            query_link_id["RECON_EXECUTION_ID"] = self.recon_execution_id

            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]
            columnsToConvertCash = ["LINK_ID", "RAW_LINK", "PARTICIPATING_RECORD_TXN_ID", "OBJECT_OID", "OBJECT_ID",
                                    "RECON_EXECUTION_ID"]
            (status_exception, exmaster_data) = sql.getExceptionMasterDetails(query=query_link_id,
                                                                              connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                # converting from data_frame --> JSON (Str) --> Dict()
                exmaster_data = json.loads(exmaster_data.to_json(orient='records'))

            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query_link_id,
                                                                                       connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                # converting from data_frame --> JSON (Str) --> Dict()
                exallocation_data = json.loads(exallocation_data.to_json(orient='records'))

        if status and txn_data is not None:

            for rec in txn_data:
                rec['UPDATED_BY'] = 'IDFC_IT'
                rec['RECORD_STATUS'] = "INACTIVE"
                newData.append(rec)
            (status, updata) = self.updateSqlData(query=newData, connection=recon_con)

            for rec_old in txn_data:
                rec_old["CREATED_BY"] = 'IDFC_IT'
                rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                rec_old["UPDATED_BY"] = None
                rec_old['MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_WAITING_FOR_AUTH" if singleEntryMatchIndictor or \
                                                                                         rec_old[
                                                                                             'MATCHING_EXECUTION_STATE'] == 'SINGLE_ENTRY_MATCHED' else "WAITING_FOR_AUTHORIZATION"
                rec_old['LINK_ID'] = rec_old['LINK_ID']
                rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                rec_old["MATCHING_STATUS"] = "MATCHED"
                rec_old["RECORD_STATUS"] = "ACTIVE"
                rec_old['AUTHORIZATION_BY'] = self.authorizer

                ## new added columns
                rec_old['TXN_MATCHING_STATE_CODE'] = 16
                rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_PENDING"
                rec_old[
                    'TXN_MATCHING_STATE'] = "SINGLE_SOURCE_WAITING_FOR_AUTH" if singleEntryMatchIndictor else "WAITING_FOR_AUTHORIZATION"

                rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                rec_old["RECONCILIATION_STATUS"] = "RECONCILED"

                rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                rec_old["MATCHING_EXECUTION_STATUS"] = "WAITING FOR AUTHORIZATION"
                rec_old["MANUAL_MATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"
                rec_old["FORCEMATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"

                # work around to avoid account number captured as exponential value
                rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                oldData_auth.append(rec_old)
            df_cash_auth = pandas.read_json(json.dumps(oldData_auth, default=json_util.default), orient='columns',
                                            dtype=config.dataTypes)
            parseDates = []
            for k, v in datatypes.datatypes.items():
                if v == "np.datetime64":
                    parseDates.append(k)
            for i in parseDates:
                if i in df_cash_auth.columns:
                    df_cash_auth[i] = df_cash_auth[i].apply(lambda x: self.parse_dates(x))
                    df_cash_auth[i] = pandas.to_datetime(df_cash_auth[i], format='%Y-%m-%d %H:%M:%S')
                    df_cash_auth[i] = df_cash_auth[i].fillna('')
                    # logger.info('waitingforauth')
                    # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
            df_cash_auth["POSITIONS_LINK_ID"] = 0
            df_cash_auth["PREV_LINK_ID"] = 0
            (status_cash_auth, ins_data_cash) = sql.insertDFToDB(df=df_cash_auth, table_name="cash_output_txn",
                                                                 connection=recon_con)


            for master_data in exmaster_data:
                master_data["COMMENTS"] = self.comments
                master_data["CREATED_BY"] = 'IDFC_IT'
                master_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                master_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                   '%Y-%m-%d %H:%M:%S')
                master_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                master_data["EXCEPTION_STATUS"] = "INPROGRESS"
                master_data["IPADDRESS"] = "0.0.0.0"
                master_data["REASON_CODE"] = 'Manual Matching Entries'
                master_data["RECORD_STATUS"] = "ACTIVE"
                master_data["RECORD_VERSION"] = master_data["RECORD_VERSION"] + 1
                master_data["SESSION_ID"] = 'abcd_afafsfdsf_asfrerr343'
                oldData_ex_auth.append(master_data)
            parseDates = ["EXCEPTION_CREATED_DATE", "CLEARING_DATE", "CREATED_DATE", "TRADE_DATE", "UPDATED_DATE",
                          "RECORD_END_DATE", "EXCEPTION_COMPLETION_DATE", "EXCEPTION_PROCESSING_DATE"]
            df_ex_auth = pandas.read_json(json.dumps(oldData_ex_auth, default=json_util.default), orient='columns',
                                          dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
            for i in parseDates:
                if i in df_ex_auth.columns:
                    df_ex_auth[i] = pandas.to_datetime(df_ex_auth[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                    df_ex_auth[i] = df_ex_auth[i].fillna('')
            (status_master_auth, ins_data_mas) = sql.insertDFToDB(df=df_ex_auth,
                                                                  table_name="exception_master", connection=recon_con)

            # exallocation_data = exallocation_data[0]
            for allocation_data in exallocation_data:
                allocation_data["ALLOCATED_BY"] = allocation_data["ACTION_BY"]
                allocation_data["ACTION_BY"] = self.authorizer
                allocation_data["ALLOCATION_HISTORY_FLAG"] = "Y"
                allocation_data["ALLOCATION_REMARKS"] = 'Manual Matching Entries'
                allocation_data["CREATED_BY"] = 'IDFC_IT'
                allocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                             '%Y-%m-%d %H:%M:%S')
                # exallocation_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                allocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                allocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                allocation_data["EXCP_RESL_REASON_CODE"] = 'Manual Matching Entries'
                # exallocation_data['COMPLETION_DATE_TIME'] = np.NaN
                allocation_data["IPADDRESS"] = "0.0.0.0"
                allocation_data["RECORD_STATUS"] = "ACTIVE"
                allocation_data["RECORD_VERSION"] = allocation_data["RECORD_VERSION"] + 1
                allocation_data["SESSION_ID"] = 'asdfa33_sdfds3sdf_asfsdf'
                allocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                '%Y-%m-%d %H:%M:%S')
                oldData_allocation_auth.append(allocation_data)
            parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
            df_al_auth = pandas.read_json(json.dumps(oldData_allocation_auth, default=json_util.default),
                                          orient='columns',
                                          dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                 'EXCEPTION_ALLOCATION_OID': 'int64'})
            for i in parseDates:
                if i in df_al_auth.columns:
                    df_al_auth[i] = pandas.to_datetime(df_al_auth[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                    df_al_auth[i] = df_al_auth[i].fillna('')
            (status_allocation_auth, ins_data_alloc) = sql.insertDFToDB(df=df_al_auth,
                                                                        table_name="exception_allocation",
                                                                        connection=recon_con)

            if status_cash_auth and status_master_auth and status_allocation_auth:
                (status_commit, toCommit) = sql.commitToSql(connection=recon_con)

            return True, 'Success'
        else:
            return False, "Oracle Connection Failed"

    def forceMatchingColumns(self, id, query, exmaster_data=None, exallocation_data=None,
                             singleEntryMatchIndictor=False, sql_connection=None):

        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = True, sql_connection
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]

            if exmaster_data is None:
                (status_exception, exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],
                                                                                  connection=recon_con)
                if status_exception:
                    for x in columnsToConvertMaster:
                        if x in exmaster_data.columns:
                            exmaster_data[x] = exmaster_data[x].apply(str, 1)
                    exmaster_data = exmaster_data.to_json(orient='records')
            if exallocation_data is None:
                (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(
                        query=query['selected_rec'][0], connection=recon_con)
                if status_allocation:
                    for x in columnsToConvertAllocation:
                        if x in exallocation_data.columns:
                            exallocation_data[x] = exallocation_data[x].apply(str, 1)
                    exallocation_data = exallocation_data.to_json(orient='records')

            if status:
                for rec in cloned_val:
                    rec['UPDATED_BY'] = 'IDFC_IT'
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = self.updateSqlData(query=newData, connection=recon_con)
                # linkid = uuid.uuid4().int & (1 << 64) - 1
                # logger.info("linkid  fif" + str(linkid))

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = 'IDFC_IT'
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old["UPDATED_BY"] = None
                    rec_old[
                        'MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_MATCHED" if singleEntryMatchIndictor else "FORCEMATCH_MATCHED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 14
                    rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old[
                        'TXN_MATCHING_STATE'] = "SINGLE_SOURCE_SUBMISSION_PENDING" if singleEntryMatchIndictor else "SUBMISSION_PENDING"

                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["MATCHING_EXECUTION_STATUS"] = "AUTHORIZATION SUBMISSION PEND"
                    rec_old["MANUAL_MATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)
                exmaster_data = json.loads(exmaster_data)[0]

                exmaster_data["COMMENTS"] = query['comments']
                exmaster_data["CREATED_BY"] = 'IDFC_IT'
                exmaster_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                     '%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exmaster_data["EXCEPTION_STATUS"] = "INPROGRESS"
                exmaster_data["IPADDRESS"] = "192.168.2.248"
                exmaster_data["REASON_CODE"] = query['reason_code']
                exmaster_data["RECORD_STATUS"] = "ACTIVE"
                exmaster_data["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
                exmaster_data["SESSION_ID"] = 'asdfa33_sdfds3sdf_asfsdf'
                oldData_ex.append(exmaster_data)
                parseDates = ["EXCEPTION_CREATED_DATE", "CLEARING_DATE", "CREATED_DATE", "TRADE_DATE", "UPDATED_DATE",
                              "RECORD_END_DATE", "EXCEPTION_COMPLETION_DATE", "EXCEPTION_PROCESSING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})

                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                                 table_name="exception_master", connection=recon_con)

                exallocation_data = json.loads(exallocation_data)[0]
                exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                exallocation_data["ACTION_BY"] = 'IDFC_IT'
                exallocation_data["ALLOCATED_BY"] = exallocation_data["ACTION_BY"]
                exallocation_data["CREATED_BY"] = 'IDFC_IT'
                exallocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                               '%Y-%m-%d %H:%M:%S')
                # exallocation_data["EXCEPTION_ALLOCATION_ID"] = exallocation_data["EXCEPTION_ALLOCATION_ID"]
                exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exallocation_data["IPADDRESS"] = "192.168.2.248"
                exallocation_data["RECORD_STATUS"] = "ACTIVE"
                exallocation_data["RECORD_VERSION"] = exallocation_data["RECORD_VERSION"] + 1
                exallocation_data["SESSION_ID"] = 'asdfa33_sdfds3sdf_asfsdf'
                exallocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                  '%Y-%m-%d %H:%M:%S')
                oldData_allocation.append(exallocation_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})

                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')

                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                                       table_name="exception_allocation",
                                                                       connection=recon_con)

                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                dataToReturn['oldData_ex'] = oldData_ex
                dataToReturn['oldData_allocation'] = oldData_allocation

                return True, dataToReturn
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def rejectRecords(self, id, query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            # if self.checkForPartialTxnSelection(cloned_val):
            #     return True,"Partial records found in the selected queue."

            # get unique link_id from the selected txn list
            unique_link_id_dict = dict()
            cash_output_df = pandas.read_json(cloned_val, orient='columns', dtype={'LINK_ID': 'int64'})
            unique_link_id_dict["LINK_ID"] = np.unique(cash_output_df['LINK_ID']).tolist()

            (status, recon_con) = True, self.connection
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]
            (status_exception, exmaster_data) = sql.getExceptionMasDetails(query=unique_link_id_dict,
                                                                           connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = json.loads(exmaster_data.to_json(orient='records'))
                # exmaster_data = json.loads(exmaster_data)[0]
            (status_allocation, exallocation_data) = sql.getExceptionAlloDetails(query=unique_link_id_dict,
                                                                                 connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = json.loads(exallocation_data.to_json(orient='records'))
                # exallocation_data = json.loads(exallocation_data)[0]
            if status:
                tmp_var = json.loads(query['selected_rec'])
                for rec in tmp_var:
                    rec['UPDATED_BY'] = self.authorizer
                    rec['RECORD_STATUS'] = "INACTIVE"
                    # rec['MATCHING_EXECUTION_STATE'] = "AUTHORIZED"
                    # rec['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    # rec['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    newData.append(rec)
                (status_up, upData) = self.updateSqlData(query=newData, connection=recon_con)
                # linkid = str(uuid.uuid4().int & (1<<64)-1)
                # logger.info("linkiddddddddddddd" + str(linkid))
                tmp_var = json.loads(cloned_val)
                for rec_old in tmp_var:
                    # if 'format' in flask.session['sessionData']:
                    #     for format_type in flask.session['sessionData']['format']:
                    #         for k,v in format_type.items():
                    #             for key,val in rec_old.items():
                    #                 if k == key and (v == "DATE" or v =="TIMESTAMP"):
                    #                     if val is not None:
                    #                         if type(rec_old[key]) != str:
                    #                             rec_old[key] = datetime.datetime.fromtimestamp((rec_old[key]) / 1000).strftime("%d-%b-%Y")
                    #                         else:
                    #                             rec_old[key] = rec_old[key]
                    rec_old["CREATED_BY"] = self.authorizer
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['RECONCILIATION_STATUS'] = "EXCEPTION"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    # if rec_old['STATEMENT_DATE'] is not None and type(rec_old['STATEMENT_DATE']) is int and rec_old['STATEMENT_DATE'] != 0:
                    #     logger.info(type(rec_old["STATEMENT_DATE"]))
                    #     rec_old['STATEMENT_DATE'] = datetime.datetime.fromtimestamp(rec_old['STATEMENT_DATE'] / 1000).strftime("%Y-%m-%d %H:%M:%S")
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"

                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 21
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "AUTHORIZATION_REJECTED"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')

                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                for val in exmaster_data:
                    upd_exmaster_data = dict()
                    upd_exmaster_data["COMMENTS"] = query['comments']
                    upd_exmaster_data["LINK_ID"] = val["LINK_ID"]
                    upd_exmaster_data["CREATED_BY"] = self.authorizer
                    upd_exmaster_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                   '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                             '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)

                    upd_exmaster_data["EXCEPTION_ID"] = val["EXCEPTION_ID"]
                    upd_exmaster_data["EXCEPTION_STATUS"] = "INPROGRESS"
                    upd_exmaster_data["IPADDRESS"] = "0.0.0.0"
                    upd_exmaster_data["REASON_CODE"] = query['reason_code']
                    upd_exmaster_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exmaster_data["RECORD_VERSION"] = val['RECORD_VERSION'] + 1
                    upd_exmaster_data["SESSION_ID"] = 'afb_ejfoljk3_asdfj343_3434'
                    upd_exmaster_data["BUSINESS_CONTEXT_ID"] = val['BUSINESS_CONTEXT_ID']
                    upd_exmaster_data["RECON_EXECUTION_ID"] = val['RECON_EXECUTION_ID']
                    upd_exmaster_data["RECON_ID"] = val["RECON_ID"]
                    upd_exmaster_data["PRODUCTLINE_ID"] = val["PRODUCTLINE_ID"]
                    upd_exmaster_data["BUSINESS_PROCESS_ID"] = val["BUSINESS_PROCESS_ID"]
                    upd_exmaster_data["ASSET_CLASS_ID"] = val["ASSET_CLASS_ID"]
                    oldData_ex.append(upd_exmaster_data)

                parseDates = ["CREATED_DATE", "EXCEPTION_CREATED_DATE", "CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",
                                                                 connection=recon_con)

                for val in exallocation_data:
                    upd_exallocation_data = dict()
                    upd_exallocation_data["ACTION_BY"] = self.authorizer
                    upd_exallocation_data["ALLOCATED_BY"] = val["CREATED_BY"]
                    upd_exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                    upd_exallocation_data["CREATED_BY"] = self.authorizer
                    upd_exallocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                       '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["EXCEPTION_ALLOCATION_ID"] = val["EXCEPTION_ALLOCATION_ID"]
                    upd_exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    upd_exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                    upd_exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                    upd_exallocation_data["IPADDRESS"] = "192.168.2.248"
                    upd_exallocation_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exallocation_data["RECORD_VERSION"] = val["RECORD_VERSION"] + 1
                    upd_exallocation_data["EXCEPTION_ID"] = val["EXCEPTION_ID"]
                    upd_exallocation_data["SESSION_ID"] = 'a12cl_d3as343jv034_3433v'
                    upd_exallocation_data["RECON_ID"] = val["RECON_ID"]
                    upd_exallocation_data["BUSINESS_CONTEXT_ID"] = val["BUSINESS_CONTEXT_ID"]
                    upd_exallocation_data["ALLOCATION_STATUS"] = val["ALLOCATION_STATUS"]
                    upd_exallocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                          '%Y-%m-%d %H:%M:%S')

                    oldData_allocation.append(upd_exallocation_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",
                                                                       connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                # if not status_commit:
                #     logger.info("Not Committed ............................................")
                print 'rejection successful'
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records to authorize"

    def updateSqlData(self, query, connection, rollBackRecordIndicator=False):
        cursor = connection.cursor()
        upd_df = pandas.DataFrame(query)
        _id = str(uuid.uuid4().int & (1 << 64) - 1)
        upd_df = upd_df[['LINK_ID', 'OBJECT_OID']]
        upd_df['UUID'] = _id
        print len(upd_df['LINK_ID'].unique())
        print len(upd_df['OBJECT_OID'].unique())
        print _id
        sql.insertDFToDB(upd_df, table_name='tmp_table', connection=connection)
        print _id
        try:
            if cursor is not None:
                cursor.execute(
                        "update cash_output_txn set record_status = 'INACTIVE', updated_date = systimestamp, updated_by = '" + str(
                                query[0][
                                    'UPDATED_BY']) + "' where object_oid in (select object_oid from tmp_table where uuid = '" + _id + "') and record_status = 'ACTIVE'")
                if not rollBackRecordIndicator:
                    cursor.execute(
                            "update exception_master set record_status = 'INACTIVE', updated_date = systimestamp, updated_by = '" + str(
                                    query[0][
                                        'UPDATED_BY']) + "' where link_id in (select link_id from tmp_table where uuid = '" + _id + "') and record_status = 'ACTIVE'")
                    cursor.execute(
                            "update exception_allocation set record_status = 'INACTIVE', updated_date = systimestamp, updated_by = '" + str(
                                    query[0][
                                        'UPDATED_BY']) + "' where exception_id in (select exception_id from exception_master where link_id in (select link_id from tmp_table where uuid = '" + _id + "') and record_status = 'ACTIVE')")
                    # exituuid
        except cx_Oracle.DatabaseError as db_exe:
            error, = db_exe.args
            print(error)
            print(error.code)
            print(error.message)
            print(error.context)
            raise
        except Exception as excep:
            print("exception details" + str(excep))
            error, = excep.args
            print(error.code)
            print(error.message)
            print(error.context)
            raise
        finally:
            if cursor is None:
                cursor.close()

        return True, ''

    def read_col_query_unmatched(self, connection):
        parsedate_cols = ['STATEMENT_DATE', 'VALUE_DATE', 'TRANSACTION_DATE', 'EXECUTION_DATE_TIME']
        df = pandas.read_csv('export.csv')
        df = df[['RECON_EXECUTION_ID', 'LINK_ID']]
        _id = str(uuid.uuid4().int & (1 << 64) - 1)
        df['UUID'] = _id
        sql.insertDFToDB(df, 'tmp_table', connection=self.connection)

        qry = "select OBJECT_ID,STATEMENT_DATE,REASON_CODE,MATCHING_EXECUTION_STATE,CREATED_BY,LINK_ID,SOURCE_TYPE,ACCOUNT_NUMBER,CREATED_DATE,RECON_EXECUTION_ID,AUTHORIZATION_BY,TXN_MATCHING_STATE_CODE,VALUE_DATE,RESOLUTION_COMMENTS,ACTION_BY,UPDATED_BY,SOURCE,RECORD_INDICATOR,DEBIT_CREDIT_INDICATOR,RAW_LINK,OPENING_BALANCE_AMOUNT,RECON_ID,EXECUTION_DATE_TIME,CBS_TRACE_NUMBER,AMOUNT,BUSINESS_CONTEXT_ID,SOURCE_ID,CLOSING_BALANCE,MATCHING_STATUS,TRANSACTING_TELLER,SOURCE_NAME,MATCHING_EXECUTION_STATUS,ENTRY_TYPE,TXN_MATCHING_STATE,RECONCILIATION_STATUS,TRANSACTION_DATE,SESSION_ID,CLOSING_DEBIT_CREDIT_INDICATOR,OBJECT_OID,FORCEMATCH_AUTHORIZATION,UPDATED_DATE,INVESTIGATOR_COMMENTS,INVESTIGATED_BY,RECORD_STATUS,BRANCH_CODE,AUTHORIZATION_STATUS,OPENING_DEBIT_CREDIT_INDICATOR,TXN_MATCHING_STATUS,NARRATION,RECORD_VERSION,REF_NUMBER1,REF_NUMBER2,IPADDRESS,ACCOUNT_NAME,GROUPED_FLAG from cash_output_txn where recon_id = '" + str(
                self.recon_id) + "' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'UNMATCHED' and reconciliation_status = 'EXCEPTION' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION') and recon_execution_id = '"+ self.recon_execution_id +"' and MATCHING_EXECUTION_STATE <> 'UNMATCHED_INVESTIGATION_PENDING' AND LINK_ID in (select LINK_ID from tmp_table where uuid = '" + str(
                _id) + "')"
        df = pandas.read_sql(qry, connection, parse_dates=parsedate_cols)
	
        if not df.empty:
            columnsToConvert = config.cash_columnsToConvert
            for x in columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)

            return True, df
        return False, ''

    def parse_dates(self, value):
        if value != value or value is None:
            return np.nan
        elif isinstance(value, pandas.tslib.Timestamp):
            return value
        elif isinstance(value, (np.int64, np.float64, int, float)):
            return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(value / 1000))
        else:
            return parse(value).strftime("%Y-%m-%d %H:%M:%S")

    def read_col_query_waitingForAuthorization(self, connection):
        parsedate_cols = ['STATEMENT_DATE', 'VALUE_DATE', 'TRANSACTION_DATE', 'EXECUTION_DATE_TIME']
        ## TODO Change the query later to auth query
        qry = "select OBJECT_ID,STATEMENT_DATE,REASON_CODE,MATCHING_EXECUTION_STATE,CREATED_BY,LINK_ID,SOURCE_TYPE,ACCOUNT_NUMBER,CREATED_DATE,RECON_EXECUTION_ID,AUTHORIZATION_BY,TXN_MATCHING_STATE_CODE,VALUE_DATE,RESOLUTION_COMMENTS,ACTION_BY,UPDATED_BY,SOURCE,RECORD_INDICATOR,DEBIT_CREDIT_INDICATOR,RAW_LINK,OPENING_BALANCE_AMOUNT,RECON_ID,EXECUTION_DATE_TIME,CBS_TRACE_NUMBER,AMOUNT,BUSINESS_CONTEXT_ID,SOURCE_ID,CLOSING_BALANCE,MATCHING_STATUS,TRANSACTING_TELLER,SOURCE_NAME,MATCHING_EXECUTION_STATUS,ENTRY_TYPE,TXN_MATCHING_STATE,RECONCILIATION_STATUS,TRANSACTION_DATE,SESSION_ID,CLOSING_DEBIT_CREDIT_INDICATOR,OBJECT_OID,FORCEMATCH_AUTHORIZATION,UPDATED_DATE,INVESTIGATOR_COMMENTS,INVESTIGATED_BY,RECORD_STATUS,BRANCH_CODE,AUTHORIZATION_STATUS,OPENING_DEBIT_CREDIT_INDICATOR,TXN_MATCHING_STATUS,NARRATION,RECORD_VERSION,REF_NUMBER1,REF_NUMBER2,IPADDRESS,ACCOUNT_NAME,GROUPED_FLAG from cash_output_txn where recon_id = '" + self.recon_id +"' and entry_type = 'T' and record_status = 'ACTIVE' and authorization_status = 'AUTHORIZED' and forcematch_authorization = 'WAITING_FOR_AUTHORIZATION' and recon_execution_id = '" + self.recon_execution_id + "' and link_id = '" + self.auth_linkid + "'"
        df = pandas.read_sql(qry, connection, parse_dates=parsedate_cols)

        if not df.empty:
            columnsToConvert = config.cash_columnsToConvert
            for x in columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
            return True, df
        return False, ''

    def authorizeRecords(self, id, query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            # if self.checkForPartialTxnSelection(cloned_val):
            #     return True,"Partial records found in the selected queue."

            (status, recon_con) = True, self.connection
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]

            # get unique link_id from the selected txn list
            unique_link_id_dict = dict()
            # cash_output_df = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns', dtype={'LINK_ID': 'int64'})

            cash_output_df = pandas.read_json(cloned_val, orient='columns', dtype={'LINK_ID': 'int64'})

            unique_link_id_dict["LINK_ID"] = np.unique(cash_output_df['LINK_ID']).tolist()

            (status_exception, exmaster_data) = sql.getExceptionMasDetails(query=unique_link_id_dict,
                                                                           connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = json.loads(exmaster_data.to_json(orient='records'))

            (status_allocation, exallocation_data) = sql.getExceptionAlloDetails(query=unique_link_id_dict,
                                                                                 connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)

                exallocation_data = json.loads(exallocation_data.to_json(orient='records'))

            if status:
                cloned_val = json.loads(cloned_val)
                for rec in cloned_val:
                    rec['UPDATED_BY'] = self.authorizer
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status_up, upData) = self.updateSqlData(query=newData, connection=recon_con)

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = self.authorizer
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_AUTHORIZED" if rec_old[
                                                                                           "MATCHING_EXECUTION_STATE"] == "SINGLE_ENTRY_WAITING_FOR_AUTH" else "MATCHED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "AUTHORIZED"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    # New columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 15 if rec_old[
                                                                   "TXN_MATCHING_STATE"] == "SINGLE_SOURCE_WAITING_FOR_AUTH" else 14
                    rec_old['TXN_MATCHING_STATUS'] = "FORCE_MATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SINGLE_SOURCE_AUTHORIZED" if rec_old[
                                                                                      "TXN_MATCHING_STATE"] == "SINGLE_SOURCE_WAITING_FOR_AUTH" else "FORCE_MATCHED"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                for val in exmaster_data:
                    upd_exmaster_data = dict()
                    upd_exmaster_data["COMMENTS"] = query['comments']
                    upd_exmaster_data["LINK_ID"] = val["LINK_ID"]
                    upd_exmaster_data["CREATED_BY"] = self.authorizer
                    upd_exmaster_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                   '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                             '%Y-%m-%d %H:%M:%S')
                    upd_exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    upd_exmaster_data["EXCEPTION_ID"] = val["EXCEPTION_ID"]
                    upd_exmaster_data["EXCEPTION_STATUS"] = "CLOSED"
                    upd_exmaster_data["IPADDRESS"] = "192.168.2.248"
                    upd_exmaster_data["REASON_CODE"] = query['reason_code']
                    upd_exmaster_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exmaster_data["RECORD_VERSION"] = val['RECORD_VERSION'] + 1
                    upd_exmaster_data["SESSION_ID"] = 'xfirejsfk_dfrer_dfderere34df'
                    upd_exmaster_data["BUSINESS_CONTEXT_ID"] = val['BUSINESS_CONTEXT_ID']
                    upd_exmaster_data["RECON_EXECUTION_ID"] = val['RECON_EXECUTION_ID']
                    upd_exmaster_data["RECON_ID"] = val["RECON_ID"]
                    upd_exmaster_data["PRODUCTLINE_ID"] = val["PRODUCTLINE_ID"]
                    upd_exmaster_data["BUSINESS_PROCESS_ID"] = val["BUSINESS_PROCESS_ID"]
                    upd_exmaster_data["ASSET_CLASS_ID"] = val["ASSET_CLASS_ID"]
                    oldData_ex.append(upd_exmaster_data)

                parseDates = ["CREATED_DATE", "EXCEPTION_CREATED_DATE", "CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",
                                                                 connection=recon_con)

                for val in exallocation_data:
                    upd_exallocation_data = dict()
                    upd_exallocation_data["ACTION_BY"] = self.authorizer
                    upd_exallocation_data["ALLOCATED_BY"] = val["ACTION_BY"]
                    upd_exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                    upd_exallocation_data["CREATED_BY"] = 'IDFC_IT'
                    upd_exallocation_data["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                       '%Y-%m-%d %H:%M:%S')
                    upd_exallocation_data["EXCEPTION_ALLOCATION_ID"] = val["EXCEPTION_ALLOCATION_ID"]
                    upd_exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    upd_exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "CLOSED"
                    upd_exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                    upd_exallocation_data["IPADDRESS"] = "192.168.2.248"
                    upd_exallocation_data["RECORD_STATUS"] = "ACTIVE"
                    upd_exallocation_data["RECORD_VERSION"] = val["RECORD_VERSION"] + 1
                    upd_exallocation_data["EXCEPTION_ID"] = val["EXCEPTION_ID"]
                    upd_exallocation_data["SESSION_ID"] = 'xfirejsfk_dfrer_dfderere34df'
                    upd_exallocation_data["RECON_ID"] = val["RECON_ID"]
                    upd_exallocation_data["BUSINESS_CONTEXT_ID"] = val["BUSINESS_CONTEXT_ID"]
                    upd_exallocation_data["ALLOCATION_STATUS"] = val["ALLOCATION_STATUS"]
                    upd_exallocation_data["ALLOCATION_TIME"] = datetime.datetime.strftime(datetime.datetime.now(),
                                                                                          '%Y-%m-%d %H:%M:%S')
                    oldData_allocation.append(upd_exallocation_data)

                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",
                                                                       connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records to authorize"

    def unGroupingColumns(self, id, query):
        cloned_val = query['selected_rec']
        newData = []
        oldData = []

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # To avoid [Python int too large to convert to C long] error
            part_match_frame = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                                dtype={'LINK_ID': 'int64'})
            part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
            df_cash_aggr = pandas.DataFrame(
                {'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()

            reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
            reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]

            part_match_count = np.count_nonzero(reconData_merge["count_res"])
            ungrouped_rec_count = len([1 for i in reconData_merge["validation_count"] if i == 1])

            if status and part_match_count == 0 and ungrouped_rec_count == 0:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = 'IDFC_IT'
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                cursor = recon_con.cursor()
                cursor.execute()
                (status, updata) = self.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata
                linkid = uuid.uuid4().int & (1 << 64) - 1
                print linkid
                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = 'IDFC_IT'
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old["UPDATED_BY"] = None
                    rec_old['MATCHING_EXECUTION_STATE'] = "UNGROUPED"
                    rec_old['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old['GROUPED_FLAG'] = 'N'
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 4
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_UNGROUPED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                return True, 'Success'
            elif part_match_count >= 1:
                return True, "Partial ungrouping of transaction is not allowed."
            elif ungrouped_rec_count >= 1:
                return True, "found singleton records in the seleted list."
            else:
                return False, ''
        else:
            return False, "No Records to Ungroup"


if __name__ == '__main__':
    type = sys.argv[1]
    update = update_cash_output_txn(type)
