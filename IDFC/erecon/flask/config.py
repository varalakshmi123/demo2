appUrl = "https://127.0.0.1/"
flaskBaseUrl = '/api'
flaskPath = '/usr/local/nginx/www/erecon/flask/'
uiPath = '/usr/local/nginx/www/erecon/ui/'
commonFilesPath = '/usr/local/nginx/www/erecon/ui/app/'
secretKey = '58e57c3c-76cb-4c5c-8416-0f3226bc5e88'  # Change per project
applicationModules = ["application", "usermanagement"]
sessionSalt = 'f80423b7-33f2-4b57-ae97-44ef3f8a0c32'  # Change per project
cookieName = 'erecon'  # Change per project
cookieDomain = ''  # Change per project
sessionTimeoutSecs = 86400
logBaseDir = '/var/log/erecon/'  # Change per project
dataBackupDir = '/tmp/data/erecon/'  # Change per project

ereconSwift  = '/usr/local/nginx/www/erecon/swiftDir'
swiftDir = '/home/reconadmin/algorecon/rbeappv5.0/swiftDir'

ageing = '/usr/local/nginx/www/erecon/ui/app/json/ageing.json'
flaskLogFile = "apiaccess"
flaskMaxBytes = 50000000
flaskBackupCount = 100
running = False
contentDir = '/usr/local/nginx/www/erecon/ui/app/files'
uploadDir = '/usr/local/nginx/www/erecon/ui/app/files/uploads'
gstUploaddDir = '/usr/share/nginx/www/erecon/ui/app/files/gst'
usersMongoHost = 'localhost'
usersDatabaseName = 'erecon' #Change per project
mongoHost = 'localhost'
aes_key = 'HjKlEdulcnI#h$ah'
iv = 'RjiclAt#$fc!wldS'
databaseName = 'erecon' #Change per project
isReplicaSet = False
replicaSetName = ''
# ticketServer = "smtp.gmail.com"
# ticketPort = 465
# ticketConnectionSecurity = "SSL/TLS"
# ticketUserName = "hash.izmosales@gmail.com"
# ticketPassword = "hash@1234"
companyDatabaseName = "crmframework"
isAllowTicketCreation = False
####For Calling Purpose
ACCOUNT_SID = "AC16772aca9e88c27b34b9d6f2cf0df11f"
AUTH_TOKEN = "28fe815d7064d6b83e8f07a159f63978"
ignore_amount_validation = ['TRTD_CASH_APAC_61169']
ticketServer = "smtp.gmail.com"
ticketPort = 465
ticketConnectionSecurity = "SSL/TLS"
ticketUserName = "hash.appointment@gmail.com"
ticketPassword = "hash@1234"

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

cash_columnsToConvert = ["LINK_ID", "RAW_LINK", "PARTICIPATING_RECORD_TXN_ID", "OBJECT_OID", "OBJECT_ID",
                         "RECON_EXECUTION_ID", "PREV_LINK_ID"]

# Recon Ids that needs account number valication while forcematching the records
account_number_validation = ['SUSP_CASH_APAC_18646', 'TRSC_CASH_APAC_20181', 'TF_CASH_APAC_20063',
                             'SUSP_CASH_APAC_20380', 'SUSP_CASH_APAC_20381', 'SUSP_CASH_APAC_20382',
                             'ECollections_CASH_APAC_20469', 'ECollections_CASH_APAC_20470']
execution_summary_columns = ['RECON_ID', 'RECON_NAME', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME', 'STATEMENT_DATE',
                             'RECORD_STATUS', 'TOTAL_TRANSACTIONS', 'CARRYFORWARD_COUNT', 'REASON_CODE',
                             'PARTIAL_MATCHED_RECORDS_COUNT', 'PERFECT_MATCHED_RECORDS_COUNT',
                             'TOLERANCE_MATCHED_COUNT', 'EXCEPTION_RECORDS_COUNT']

# By-pass currency validation for the below recons
avoid_currency_validation = ['EPYMTS_CASH_APAC_20229', 'EPYMTS_CASH_APAC_19106', 'EPYMTS_CASH_APAC_19104',
                             'EPYMTS_CASH_APAC_19110', 'EPYMTS_CASH_APAC_20221',
                             'EPYMTS_CASH_APAC_20223', 'EPYMTS_CASH_APAC_20227', 'EPYMTS_CASH_APAC_19108',
                             'EPYMTS_CASH_APAC_20225', 'EPYMTS_CASH_APAC_20201', 'CLG_CASH_APAC_20161']

# curreny validation will be performed only on below recons
currency_validation = ['FIC_CASH_APAC_18663', 'FIC_CASH_APAC_19074', 'FIC_CASH_APAC_19076', 'FIC_CASH_APAC_19082',
                       'FIC_CASH_APAC_19084', 'FIC_CASH_APAC_19086', 'TRSC_CASH_APAC_20181', 'TRSC_CASH_APAC_18904',
                       'TF_CASH_APAC_20261', 'TF_CASH_APAC_20263', 'TF_CASH_APAC_20262']

staticCols = ['SOURCE_TYPE', 'SOURCE_NAME', 'ACCOUNT_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME',
              'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE',
              'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS',
              'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'OBJECT_ID', 'OBJECT_OID',
              'BUSINESS_CONTEXT_ID', 'GROUPED_FLAG', 'UPDATED_DATE', 'UPDATED_BY', 'TXN_MATCHING_STATE_CODE',
              'TXN_MATCHING_STATUS', 'TXN_MATCHING_STATE', 'AUTHORIZATION_BY', 'RECON_ID',
              'RECORD_STATUS', 'RECORD_VERSION', 'SESSION_ID', 'ENTRY_TYPE', 'IPADDRESS', 'RAW_LINK', 'INVESTIGATED_BY',
              'RESOLUTION_COMMENTS', 'INVESTIGATOR_COMMENTS', 'UPDATED_BY', 'ACTION_BY', 'SOURCE_ID', 'FREE_TEXT_1',
              'FREE_TEXT_2', 'FREE_TEXT_3']

three_src_recon = ["CLG_CASH_APAC_20101", "CLG_CASH_APAC_20121"]
two_decimal_format = ["TRSC_CASH_APAC_20181", "TF_CASH_APAC_20063", "TF_CASH_APAC_20065"]

# TXT_60_001 added for retain leading zeros in benf acc for 20201
# GL_ACCOUNT_NUM,TXT_60_002,TXT_60_003 for bs_cash_apac_19128
dataTypes = {'OBJECT_OID': 'int64', 'LINK_ID': 'int64', 'RAW_LINK': 'int64', 'PARTICIPATING_RECORD_TXN_ID': 'int64',
             'OBJECT_ID': 'int64', 'PREV_LINK_ID': 'int64', 'ACCOUNT_NUMBER': 'str', 'TXT_60_001': 'str',
             'GL_ACCOUNT_NUMBER': 'str', 'TXT_60_002': 'str', 'TXT_60_003': 'str', 'BUSINESS_CONTEXT_ID': 'int64','REF_NUMBER1':'str'}

timestamp_columns = ['EXECUTION_DATE_TIME', 'CREATED_DATE', 'UPDATED_DATE']
# ReconID to Custom validation function name
customValidation = {'RA_CASH_APAC_61204': 'RA_61204', 'TW_CASH_APAC_61208': 'TW_61208',
                    'TRTD_CASH_APAC_61169': 'TD_61169'}

# To limit the number of records fetched
threshold = '20000'

# export all report static columns
reportCols = ['SOURCE_TYPE', 'SOURCE_NAME', 'ACCOUNT_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME',
              'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE',
              'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS',
              'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'INVESTIGATED_BY', 'RESOLUTION_COMMENTS',
              'INVESTIGATOR_COMMENTS', 'UPDATED_BY', 'FREE_TEXT_1', 'FREE_TEXT_2', 'FREE_TEXT_3']

# exception master data_types
excepMasterDataTypes = {'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64',
                        'BUSINESS_CONTEXT_ID': 'int64'}

# exception allocation data_types
excepAllocDataTypes = {'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64', 'EXCEPTION_ALLOCATION_OID': 'int64',
                       'BUSINESS_CONTEXT_ID': 'int64', 'WORK_POOL_ID': 'int64'}

# limit threshold for the following recons
threshold_recons = ['GLBAL_CASH_APAC_20461']

# ignore matched transactions, since exceeding limited count
ignore_matched_txns = ['GLBAL_CASH_APAC_20461']
loginCount = 5

# ssh paremeters to connecting to engine
engineIp = '10.1.37.51'
engineUserName = 'reconadmin'
enginePassword = 'X3G_0HT_JF7_nQ5'
enginePath = '/home/reconadmin/algorecon/rbeappv5.0/'

# Oracel database properties
ip = '10.1.37.101'
port = 1653
SID = 'recondb'
connection_name = 'algoreconutil'
connection_pwd = 'AE9_WSTr_3zb65'
app_ip = 'localhost'
engine_ip = 'localhost'

# Recon Migration Parameters
migrationFolder = '/usr/local/nginx/www/erecon/flask/'
reconMigrationFolder = '/home/reconadmin/algorecon/reconmigrations/'
mongoPath = '/home/reconadmin/algorecon/prod_libs/mongodb-linux-x86_64-2.4.9/bin/'

gst_basepath = "/reconapp/recondata"

#Gst Rsync commands
gst_rsync_cmd =  'rsync -avr --files-from=/tmp/rsyncfiles mftusr@10.1.37.41:/recondata/{} {}'
gst_files_list = 'ssh mftusr@10.1.37.41 "cd /recondata/{} && find . -type f -mtime -60" > /tmp/rsyncfiles' 
gst_rm_cmd = 'find {} -mindepth 1 -mtime +60 -delete'

passwordRule = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})"

delta_sources = ['OGL', 'GST_Reversal', "GST_Invoice","Service Tax Reversal"]
TRTD_COLS = ["DEAL_ID","Details_Of_Contract_Treasury","TRANSACTION_REF_Trade","TRANSACTION_DATE_Treasury","TRANSACTION_DATE_Trade","UCIC_Treasury","UCIC_Trade","EXCHANGE_RATE_Treasury","EXCHANGE_RATE_Trade","CUSTOMER_NAME_Treasury","CUSTOMER_NAME_Trade","VALUE_DATE_Treasury","VALUE_DATE_Trade","CURRENCY","CURRENCY_AMOUNT_Treasury","CURRENCY_AMOUNT_Trade","INR_AMOUNT_Treasury","INR_AMOUNT_Trade","TRANSACTION_TYPE_Treasury","TRANSACTION_TYPE_Trade","SOURCE_NAME_Treasury","SOURCE_NAME_Trade","REASON_Treasury","REASON_Trade"]


dealRecons = ['TRSC_CASH_APAC_61181', 'TRSC_CASH_APAC_61182', 'TRSC_CASH_APAC_61183', 'TRSC_CASH_APAC_61184',
              'TRSC_CASH_APAC_61185',
              'TRSC_CASH_APAC_61186', 'TRSC_CASH_APAC_61187', 'TRSC_CASH_APAC_61189', 'TRSC_CASH_APAC_61141',
              'TRSC_CASH_APAC_61142', 'TRSC_CASH_APAC_61143','TRSC_CASH_APAC_61144', 'TRSC_CASH_APAC_61145',
              'TRSC_CASH_APAC_61146', 'TRSC_CASH_APAC_61147','TRSC_CASH_APAC_61149', 'TRSC_CASH_APAC_61151',
              'TRSC_CASH_APAC_61152', 'TRSC_CASH_APAC_61153', 'TRSC_CASH_APAC_61154', 'TRSC_CASH_APAC_61155',
              'TRSC_CASH_APAC_61156', 'TRSC_CASH_APAC_61157', 'TRSC_CASH_APAC_61158', 'TRSC_CASH_APAC_61190',
              'TRSC_CASH_APAC_61188', 'TRSC_CASH_APAC_61160', 'TRSC_CASH_APAC_61188', 'TRSC_CASH_APAC_61190',
              'TRSC_CASH_APAC_61148', 'TRSC_CASH_APAC_61160', 'TRSC_CASH_APAC_61191','TRSC_CASH_APAC_61192','TRSC_CASH_APAC_61193','TRSC_CASH_APAC_61159']

rsyncGSTFiles = True
rsyncNostroReportFile = True


cycles = {'Cycle 1': ['Cycle 1'],
          'Cycle 2': ['Cycle 1', 'Cycle 2'],
          'Cycle 3': ['Cycle 1', 'Cycle 2', 'Cycle 3'],
          'Cycle 4': ['Cycle 1', 'Cycle 2', 'Cycle 3', 'Cycle 4'],
          'Cycle 5': ['Cycle 1', 'Cycle 2', 'Cycle 3', 'Cycle 4', 'Cycle 5']}
		  
#LDAP Properties
ldap = '10.4.8.11:389'
ldapServer = '10.20.8.11:636'
ldapAuthenticate = True
swiftRequiredCols = ['SOURCE_NAME', 'MTType', 'Correspondent', 'Value Date','CURRENCY', 'AMOUNT', 'MUR','Related Reference','Format/Status', 'Netw. Status Timestamp (GMT)', 'Netw. Status', 'Orig Inst RP', 'Reception Info','Mesg Creation','Suffix','REASON','Unit Name', 'Recon Remarks', 'Ops Remarks', 'STATEMENT_DATE', 'AGEING','AGEING BUCKET', 'MATCHING_STATUS']
swiftRecons = ['TRSC_CASH_APAC_61175','TRSC_CASH_APAC_61176']

# mongodb erecon
erecondb = 'erecon'

## SMTP Properties
SEND_EMAIL='YES'
SMTP_HOST='smtp.idfcbank.com'
SMTP_PORT=''
SMTP_AUTH='NO'
SMTP_USER='erecon.alerts'
SMTP_PASSWORD=''
SMTP_START_TLS='NO'
EMAIL_SENDER='erecon.alerts@idfcbank.com'
