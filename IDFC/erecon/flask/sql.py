import cx_Oracle
import pandas
from collections import OrderedDict
import datetime
import config
import logger
import flask
import numpy as np
import time
import application
import datatypes
logger = logger.Logger.getInstance("application").getLogger()
import sql_config
from uuid import uuid4
import locale
import json

# change per sql database
ip = sql_config.ip
port = sql_config.port
SID = sql_config.SID


# dsn_tns = cx_Oracle.makedsn(ip, port, SID)
#
# connection = cx_Oracle.connect('algoreconutil_dev', 'algorecon', dsn_tns)
#
# print connection
#connection = cx_Oracle.connect('algoreconutil_dev/algorecon@localhost:1521/dbname')
def getConnection():
    dsn_tns = cx_Oracle.makedsn(ip, port, SID)
    #connection = cx_Oracle.connect('reconuat', 'ID#$fc8455', dsn_tns)
    connection = cx_Oracle.connect(sql_config.connection_name, sql_config.connection_pwd, dsn_tns)
    #begin new transaction
    connection.begin()

    if not hasattr(flask.g,"oracleDB_conn"):
        flask.g.oracleDB_conn = connection
        logger.info('oracleDB_connection_object <"{0}">'.format(hash(flask.g.oracleDB_conn)))

    return True,flask.g.oracleDB_conn

def read_unmatched(connection,query):
    current_exec_id = str(get_max_execution_id(connection=connection, recon_id=str(query['recon_id'])))
    # logger.info(type(current_exec_id))
    recon_id = str(query['recon_id'])
    q = "select SOURCE_NAME,DEBIT_CREDIT_INDICATOR,TRANSACTION_DATE,value_date,ACCOUNT_NUMBER,CURRENCY,Ref_number1,Ref_number2,Amount,Free_text_1,Free_text_2,Free_text_3 from cash_output_txn where record_status = 'ACTIVE' and MATCHING_STATUS = 'UNMATCHED' and RECON_EXECUTION_ID='" + current_exec_id + "'and " \
                                                                                                                                                                                                                                                                                                              "recon_id ='TRSC_CASH_APAC_20181'"

    unmatched = pandas.read_sql(q, connection)
    logger.info("*" * 60)
    if len(unmatched):
        unmatched['AGEING'] = unmatched['VALUE_DATE'].apply(lambda x: (datetime.datetime.now() - x)).dt.days
    return True, unmatched


def read_query(connection, query):
    # cursor = connection.cursor()
    if connection is not None:
        current_exec_id = "select max(RECON_EXECUTION_ID) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and recon_id = '" + \
                          query['recon_id'] + "'"
        current_exec_id = pandas.read_sql(current_exec_id, connection)['RECON_EXECUTION_ID'][0]
        if current_exec_id is None:
            return True, pandas.DataFrame()
        else:
            (status, columnsDF) = read_col_query(connection=connection, query=query)
            columns_list = columnsDF['UI_DISPLAY_NAME'].tolist() + config.reportCols
            parsedate_cols = columnsDF[columnsDF['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
                'MDL_FIELD_ID'].tolist()
            cols = columnsDF['MDL_FIELD_ID'].tolist() + config.reportCols
            cols_to_query = ",".join(cols)
            if current_exec_id is None:
                return True, pandas.DataFrame()
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '{}' and recon_execution_id = '{}'" \
                                            " and record_status = 'ACTIVE' and entry_type ='T'".format(
                query['recon_id'], current_exec_id)

            if (query.get('statementStartDate', 0) != 0 and query.get('statementEndDate', 0) != 0) and (
                    query['executionStartDate'] != 0 and query['executionEndDate'] != 0):
                status, df = read_col_query_matched_ex_st_date(connection, query)
                return status, df

            if query.get('statementStartDate', 0) != 0 and query.get('statementEndDate', 0) != 0:
                status, df = read_col_query_matched_statement_date(connection, query)
                return status, df

            if query.get('executionStartDate', 0) != 0 and query.get('executionEndDate', 0) != 0:
                status, df = read_col_query_matched_statement_date(connection, query)
                return status, df

            if query.get('selectedFilter','')!='':
                d += "and MATCHING_STATUS= '"+query['selectedFilter'].upper()+"'"

                if query.get('selectedFilter', '') == 'Matched':
                    d += "and reconciliation_status = 'RECONCILED' and " \
                         "(forcematch_authorization is null or forcematch_authorization = 'AUTHORIZED')"

                if query.get('selectedFilter', '') == 'UnMatched':
                    d += "and reconciliation_status = 'EXCEPTION' and " \
                         "(forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION')"

            if query.get('suspenseFlag','False') and query.get('accountNumber'):
                d += " and (account_number like ('%" + str(query.get('accountNumber')) + "%'))"

            if 'business_context_id' in query:
                if query['business_context_id'] == "1111131119" and not query.get('suspenseFlag', 'False'):
                    (status, data) = getAccountNumbers()
                    if status:
                        d += " and (account_number like ('%" + data + "%'))"

            logger.info(d)
            df = pandas.read_sql(d, connection)
            format_amount(df)
            # update column label with alias names
            df.columns = columns_list
            return True, df
    return False, ''

def format_amount(recon_data):
    if recon_data is not None and len(recon_data) > 0:
        locale.setlocale(locale.LC_ALL, 'en_IN.utf8')
        format_cols = ['CLOSING_BALANCE_AMOUNT', 'AMOUNT','OPENING_BALANCE_AMOUNT','AMOUNT_1','AMOUNT_2']
        for col in format_cols:
            if col in recon_data.columns:
                recon_data[col].fillna(0.0, inplace= True)
                recon_data[col] = recon_data[col].apply(lambda x: locale.format("%.2f", x, grouping=True) if x else "0.00")
    else:
        pass

def read_col_query(connection, query):
    # cursor = connection.cursor()
    if connection is not None:
        d = "select mdl_field_id,ui_display_name,width,mandatory,mdl_field_data_type from recon_dynamic_data_model where recon_id = '"+query['recon_id']+"' and record_status = 'ACTIVE' order by order_seq_no"
        logger.info('queryyyyy' + str(d))
        df = pandas.read_sql(d,connection)
        return True,df
    # if cursor is None:
    #     cursor.close()
    return False,''


def get_max_execution_id(connection,recon_id):
    if connection is not None:
        query = "select max(RECON_EXECUTION_ID) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and recon_id = '"+recon_id+"'"
        exec_id_list = pandas.read_sql(query,connection)['RECON_EXECUTION_ID'].tolist()
        if len(exec_id_list) > 0:
            return exec_id_list[0]
        else:
            return None

def get_max_execution_id_statement_date(connection,recon_id):
    if connection is not None:
        query = "select max(RECON_EXECUTION_ID) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and recon_id = '"+recon_id+"'"
        exec_id_list = pandas.read_sql(query,connection)['RECON_EXECUTION_ID'].tolist()
        if len(exec_id_list) > 0:
            exec_id = exec_id_list[0].astype(str)
            logger.info(exec_id)
        else:
            return None
        qry = "select statement_date as STATEMENT_DATE from recon_execution_details_log where record_status ='ACTIVE' and processing_state_status = 'Matching Completed' and recon_execution_id ='"+ exec_id+"'"
        statement_date = pandas.read_sql(qry,connection)['STATEMENT_DATE'].dt.strftime("%Y/%m/%d").tolist()
	
	logger.info(statement_date)
	logger.info('*' * 10)
        if statement_date:
            return True, statement_date[0]
        else:
            return False, None

def read_col_query_unmatched(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    current_exec_id = get_max_execution_id(connection=connection, recon_id=str(query['recon_id']))

    if current_exec_id is None:
        return True, pandas.DataFrame()

    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE','TIMESTAMP'])]['MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if 'userPools' in query:
        logger.info(query["userPools"])
        dept_user_pools = []
        dept_user_pools = query['userPools'].split(',')
        tosend = "','".join(dept_user_pools)
    logger.info(query)
    if connection is not None:
        if query['suspenseFlag']:
            d = "select "+cols_to_query+" from cash_output_txn where recon_id = '"+query['recon_id']+"' and account_number like '%"+str(query['accountNumber'])+"%' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'UNMATCHED' and reconciliation_status = 'EXCEPTION' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION') and recon_execution_id = '" +str(current_exec_id)+ "'"
        else:
            d = "select "+cols_to_query+" from cash_output_txn where recon_id = '"+query['recon_id']+"' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'UNMATCHED' and reconciliation_status = 'EXCEPTION' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION') and recon_execution_id = '" +str(current_exec_id)+ "'"

        if 'userRole' in query:
            if query['userRole'] == 'Dept User':
               d += " and authorization_by in ('"+tosend+"') and MATCHING_EXECUTION_STATE = 'UNMATCHED_INVESTIGATION_PENDING'"
            else:
                d += " and MATCHING_EXECUTION_STATE <> 'UNMATCHED_INVESTIGATION_PENDING'"
        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status,data) = getAccountNumbers()
                if status:
                    d += " and (account_number like ('%"+data + "%'))"

         # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold
        logger.info('unmatched--------' + d)
        data = json.load(open(config.ageing))
	df = pandas.read_sql(d,connection,parse_dates=parsedate_cols)
        # Ageing patch for un-matched column
        if len(df) > 0:
            tm_yday = datetime.datetime.now().timetuple().tm_yday
            #            df['AGEING'] = tm_yday - df[data.get(query['recon_id'],'STATEMENT_DATE')].dt.dayofyear
            df['AGEING'] = df[data.get(query['recon_id'], 'STATEMENT_DATE')].apply(
                lambda x: (datetime.datetime.now() - x)).dt.days
            logger.info(parsedate_cols)
            columnsToConvert = config.cash_columnsToConvert
            for x in columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
            return True, df
        else:
            return True, pandas.DataFrame()
    # if cursor is None:
    #    cursor.close()
    return False, ''


def getAccountNumbers():
    if flask.session['sessionData'] is not None:
        (status, acData) = application.ReconAssignment().getAll(
            {"perColConditions": {"user": flask.session['sessionData']['userName']}})
        # totalAc = []
        if len(acData['data']) > 0:
            ac_codes = []
            for r in acData['data']:
                if 'accountPools' in r and type(r['accountPools']) != list:
                    logger.info(r)
                    # if len(ac_codes) > 0:
                    #     ac_codes = ac_codes + r['accountPools'].split(',')
                    # else:
                    #     if type(r['accountPools']) == str:
                    #         ac_codes = r['accountPools'].split(',')
                    #         logger.info(ac_codes)
                    ac_codes.append(r['accountPools'].split(','))
            logger.info(ac_codes)
            if len(ac_codes) > 0:
                totalAc = np.concatenate(ac_codes, axis=0)
            else:
                totalAc = ac_codes
            logger.info(totalAc)
            allAccountNumbers = []
            if len(totalAc) > 0:
                for code in totalAc:
                    (status_an, data_an) = application.AccountPool().getAll(
                        {"perColConditions": {"ACCOUNT_POOL_CONTEXT_CODE": code}})
                    # logger.info(data_an)
                    if len(data_an['data']) > 0:
                        for numb in data_an['data']:
                            if str(numb['ACCOUNT_NUMBER']) not in allAccountNumbers:
                                allAccountNumbers.append(str(numb['ACCOUNT_NUMBER']).replace('.0', ''))
                # logger.info(allAccountNumbers)
                tosend = "%') or account_number like ('%".join(allAccountNumbers)
                # logger.info(tosend)
                if len(allAccountNumbers) > 0:
                    return True, tosend
                else:
                    return False, ''
    return True, ''


def getSuspenseRecons():
    totalAc = []
    if flask.session['sessionData'] is not None:
        (status, acData) = application.ReconAssignment().getAll(
            {"perColConditions": {"user": flask.session['sessionData']['userName']}})
        # logger.info(acData)
        totalAc = []
        if len(acData['data']) > 0:
            ac_codes = []
            for r in acData['data']:
                logger.info(r['businessContext'])
                if r['businessContext'] == '1111131119':
                    logger.info(r['recons'])
                    ac_codes.extend(r['recons'].split(','))
            totalAc = ac_codes
            logger.info(totalAc)
            if len(totalAc) > 0:
                tosend = "','".join(totalAc)
                return True, tosend
            else:
                return True, ''
    else:
        return False, ''


def read_col_query_unmatched_suspence(connection, query):
    # (statusAc,dataAc) = getAccountNumbers()
    (status_susp, susp_re) = getSuspenseRecons()
    d = ''
    statesdf = pandas.read_csv("meta/states.csv")
    # logger.info(type(dataAc))
    if connection is not None:
        # get max execution id for the allocated recons
        current_exec_query = "select max(recon_execution_id) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and processing_state_status = 'Matching Completed' and recon_id in ('" + susp_re + "') group by recon_id"
        rows = pandas.read_sql(current_exec_query, connection)
        # cursor.execute(current_exec_query)
        # rows = cursor.fetchmany()
        execution_ids = ",".join(rows['RECON_EXECUTION_ID'].astype('str').tolist())
        logger.info("Latest executionIDs")
        logger.info(execution_ids)
        d = "select matching_status,txn_matching_status,MATCHING_EXECUTION_STATE,RECONCILIATION_STATUS,AUTHORIZATION_STATUS,FORCEMATCH_AUTHORIZATION,debit_credit_indicator,account_number,amount from cash_output_txn where record_status = 'ACTIVE' and recon_id in ('" + susp_re + "') and recon_execution_id in (" + execution_ids + ") and entry_type = 'T'"
        logger.info('Sql Query for getting suspense --------------' + d)
        (status, acData) = application.ReconAssignment().getAll(
            {"perColConditions": {"user": flask.session['sessionData']['userName']}})
        if len(acData['data']) > 0:
            ac_codes = []
            for r in acData['data']:
                if r['businessContext'] == '1111131119':
                    logger.info(r['recons'])
                    ac_codes.extend(r['accountPools'].split(','))
        (status, account_data) = application.AccountPool().getAll()
        df_ac_data = pandas.DataFrame.from_records(account_data['data'])
        df_ac_data.drop_duplicates(['ACCOUNT_NUMBER'], inplace=True)
        account_poolsData = pandas.DataFrame()
        account_poolsData['ACCOUNT_NAME'] = df_ac_data['ACCOUNT_NAME']
        account_poolsData['ACCOUNT_NUMBER'] = df_ac_data['ACCOUNT_NUMBER']
        account_poolsData['ACCOUNT_POOL_CONTEXT_CODE'] = df_ac_data['ACCOUNT_POOL_CONTEXT_CODE']
        account_poolsData['ACCOUNT_POOL_NAME'] = df_ac_data['ACCOUNT_POOL_NAME']
        account_poolsData['RECON_NAME'] = df_ac_data['RECON_NAME']
        account_poolsData['RECON_ID'] = df_ac_data['RECON_ID']

        if susp_re != '':
            account_poolsData = account_poolsData[account_poolsData['ACCOUNT_POOL_CONTEXT_CODE'].isin(ac_codes)]

            account_poolsData['REF_ACCOUNT_NUMBER'] = account_poolsData.loc[:, 'ACCOUNT_NUMBER'].apply(
                lambda x: str(x).lstrip('0')[:5])
        if susp_re != '':
            df = pandas.read_sql(d, connection)
            df['REF_ACCOUNT_NUMBER'] = df.loc[:, 'ACCOUNT_NUMBER'].apply(lambda x: str(x).lstrip('0')[:5])
            del df['ACCOUNT_NUMBER']
            df = pandas.merge(df, account_poolsData, on=['REF_ACCOUNT_NUMBER'], how="right")
            if "TXN_MATCHING_STATUS" in df.columns:
                del df['TXN_MATCHING_STATUS']
            df = pandas.merge(df, statesdf, on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                                "AUTHORIZATION_STATUS", "FORCEMATCH_AUTHORIZATION"], how="left")
        else:
            df = pandas.DataFrame()
        return True, df
    return False, ''


def cleanDf(df):
    columnsToClean = ["ENTRY_DATE"]
    for y in columnsToClean:
        if y in df.columns:
            df[y] = df[y].apply(
                lambda x: datetime.datetime.strptime(str(x), "%d-%b-%Y") if "00:00:00" not in str(x) else x)
        return df

def read_col_query_matched(connection, query):
    if connection is not None:
        # current_exec_id = "select max(RECON_EXECUTION_ID) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and recon_id = '"+query['recon_id']+"'"
        # current_exec_id = pandas.read_sql(current_exec_id,connection)['RECON_EXECUTION_ID'].tolist()
        current_exec_id = get_max_execution_id(connection=connection, recon_id=str(query['recon_id']))

        if current_exec_id is None:
            return True, pandas.DataFrame()

        (status, columnsList) = read_col_query(connection=connection, query=query)
        parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
            'MDL_FIELD_ID'].tolist()
        cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
        cols_to_query = ",".join(cols)

        if query['suspenseFlag']:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and account_number like '%" + str(
                query['accountNumber']) + "%' and recon_execution_id = '" + str(
                current_exec_id) + "' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and reconciliation_status = 'RECONCILED' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZED')"
        else:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and recon_execution_id = '" + str(
                current_exec_id) + "' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and reconciliation_status = 'RECONCILED' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZED')"

        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    d += " and (account_number like ('%" + data + "%'))"
        logger.info('matched -------' + d)
        columnsToConvert = config.cash_columnsToConvert
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def read_col_query_matched_statement_date(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        logger.info('Matched Statment Date Search between %s and %s' % (
            str(query['statementStartDate']), str(query['statementEndDate'])))

        d = "select RECON_EXECUTION_ID from recon_execution_details_log where recon_id = '" + query[
            'recon_id'] + "' and  PROCESSING_STATE_STATUS = 'Matching Completed' and record_status = 'ACTIVE'  and statement_date >= to_date('" + \
            query['statementStartDate'] + "','dd/mm/yy') and statement_date <= to_date('" + query[
                'statementEndDate'] + "','dd/mm/yy')"
        df_exec_log = pandas.read_sql(d, connection)

        # No data for the selected date range
        if len(df_exec_log) == 0:
            return True, pandas.DataFrame()

        values = []
        for index, row in df_exec_log.iterrows():
            values.append(str(row['RECON_EXECUTION_ID']))
        exec_ids = "',0),('".join(values)

        sql_query = ''
        if query['suspenseFlag']:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"") and account_number like '%" + str(
                query['accountNumber']) + "%'"
        else:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"")"

        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    sql_query += " and (account_number like ('%" + data + "%'))"
        logger.info(sql_query)

        df = pandas.read_sql(sql_query, connection, parse_dates=parsedate_cols)
        if len(df) > 0:
            for x in config.cash_columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
        else:
            df = pandas.DataFrame()
        return True, df
    return False, ''


def read_col_query_matched_execution_date(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        d = "select RECON_EXECUTION_ID from recon_execution_details_log where recon_id = '" + query[
            'recon_id'] + "' and record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and trunc(execution_date) >= to_date('" + \
            query['executionStartDate'] + "','dd/mm/yy') and trunc(execution_date) <= to_date('" + query[
                'executionEndDate'] + "','dd/mm/yy')"

        df_exec_log = pandas.read_sql(d, connection)

        # No data for the selected date range
        if len(df_exec_log) == 0:
            return True, pandas.DataFrame()

        values = []
        for index, row in df_exec_log.iterrows():
            values.append(str(row['RECON_EXECUTION_ID']))
        exec_ids = "',0),('".join(values)

        sql_query = ''
        if query['suspenseFlag']:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"") and account_number like '%" + str(
                query['accountNumber']) + "%'"
        else:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"")"

        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    sql_query += " and (account_number like ('%" + data + "%'))"
        df = pandas.read_sql(sql_query, connection, parse_dates=parsedate_cols)
        if len(df) > 0:
            for x in config.cash_columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
        else:
            df = pandas.DataFrame()
        return True, df
    return False, ''


def read_col_query_matched_ex_st_date(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        d = "select RECON_EXECUTION_ID from recon_execution_details_log where recon_id = '" + query[
            'recon_id'] + "'  and record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed'   and trunc(execution_date) >= to_date('" + \
            query['executionStartDate'] + "','dd/mm/yy') and trunc(execution_date) <= to_date('" + query[
                'executionEndDate'] + "','dd/mm/yy') and statement_date between to_date('" + query[
                'statementStartDate'] + "','dd/mm/yy') and to_date('" + query['statementEndDate'] + "','dd/mm/yy')"
        df_exec_log = pandas.read_sql(d, connection)

        # No data for the selected date range
        if len(df_exec_log) == 0:
            return True, pandas.DataFrame()

        values = []
        for index, row in df_exec_log.iterrows():
            values.append(str(row['RECON_EXECUTION_ID']))
        exec_ids = "',0),('".join(values)
        sql_query = ''
        if query['suspenseFlag']:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"") and account_number like '%" + str(
                query['accountNumber']) + "%'"
        else:
            sql_query = "select " + cols_to_query + " from cash_output_txn where (recon_execution_id,0) in (('" + exec_ids + "',0))"' and entry_type = '"'T'"' and record_status = '"'ACTIVE'"' and matching_status = '"'MATCHED'"' and reconciliation_status = '"'RECONCILED'"' and (forcematch_authorization is null or forcematch_authorization = '"'AUTHORIZED'"")"

        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    sql_query += " and (account_number like ('%" + data + "%'))"
        df = pandas.read_sql(sql_query, connection, parse_dates=parsedate_cols)
        if len(df) > 0:
            for x in config.cash_columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
        else:
            pandas.DataFrame()
        return True, df
    return False, ''


def read_col_query_rollback(connection, query):
    (status_userpools, userpools_data) = application.UserPool().getAll(
        {'perColConditions': {'users': flask.session['sessionData']['userName']}})
    logger.info(userpools_data)
    loggedInUserID = flask.session['sessionData']['userName']
    parseUserpools = []

    current_exec_id = get_max_execution_id(connection=connection, recon_id=str(query['recon_id']))
    if current_exec_id is None:
        return True, pandas.DataFrame()

    if userpools_data['total'] > 0:
        for r in userpools_data['data']:
            logger.info(str(r["users"]))
            logger.info(str(loggedInUserID))
            if str(loggedInUserID) in str(r["users"]):
                parseUserpools.append(r['name'])

    tosend = "%') or authorization_by like ('%".join(parseUserpools)
    # tosend = "','".join(parseUserpools)
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        if query['suspenseFlag']:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and account_number like '%" + str(query[
                                                                       'accountNumber']) + "%' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'ROLLBACK_AUTHORIZATION_PENDING'"
        else:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'ROLLBACK_AUTHORIZATION_PENDING' "

        if 'userRole' in query:
            if query['userRole'] == "Authorizer":
                d += " and (authorization_by like ('%" + tosend + "%') or LOWER(authorization_by)='" + loggedInUserID + "')"

        d += " and recon_execution_id = '" + str(current_exec_id) + "'"
        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    d += " and (account_number like ('%" + data + "%'))"

        # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold

        logger.info('rollback --------' + d)
        columnsToConvert = config.cash_columnsToConvert
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def read_col_query_pending(connection, query):
    current_exec_id = get_max_execution_id(connection=connection, recon_id=str(query['recon_id']))
    if current_exec_id is None:
        return True, pandas.DataFrame()

    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        if query['suspenseFlag']:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and account_number like '%" + str(query[
                                                                       'accountNumber']) + "%' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'AUTHORIZATION_SUBMISSION_PENDING' "
        else:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'AUTHORIZATION_SUBMISSION_PENDING' "

        d += " and recon_execution_id ='" + str(current_exec_id) + "'"
        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    d += " and (account_number like ('%" + data + "%'))"

        # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold

        logger.info('pending-----' + d)
        columnsToConvert = config.cash_columnsToConvert
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def read_col_query_waitingForAuthorization(connection, query):
    (status_userpools, userpools_data) = application.UserPool().getAll(
        {'perColConditions': {'users': flask.session['sessionData']['userName']}})
    logger.info(userpools_data)
    parseUserpools = []
    # logger.info(query['recon_id'])
    current_exec_id = get_max_execution_id(connection, query['recon_id'])

    if current_exec_id is None:
        return True, pandas.DataFrame()

    if userpools_data['total'] > 0:
        for r in userpools_data['data']:
            logger.info(r['name'])
            parseUserpools.append(r['name'])
        tosend = "%') or authorization_by like ('%".join(parseUserpools)
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    if connection is not None:
        if query['suspenseFlag']:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and account_number like '%" + str(query[
                                                                       'accountNumber']) + "%' and entry_type = 'T' and record_status = 'ACTIVE' and authorization_status = 'AUTHORIZED' and forcematch_authorization = 'WAITING_FOR_AUTHORIZATION'"
        else:
            d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
                'recon_id'] + "' and entry_type = 'T' and record_status = 'ACTIVE' and authorization_status = 'AUTHORIZED' and forcematch_authorization = 'WAITING_FOR_AUTHORIZATION'"
        if 'userRole' in query:
            # if query['userRole'] == "Reconciler" or query['userRole'] == "Configurer":
            #    d += " and created_by = '"+flask.session['sessionData']['userName']+"'"
            if query['userRole'] == "Authorizer":
                d += " and (authorization_by like ('%" + tosend + "%') or LOWER(authorization_by) = '" + \
                     flask.session['sessionData']['userName'] + "')"

        d += " and recon_execution_id = '" + str(current_exec_id) + "'"
        if 'business_context_id' in query:
            if query['business_context_id'] == "1111131119" and not query['suspenseFlag']:
                (status, data) = getAccountNumbers()
                if status:
                    d += " and (account_number like ('%" + data + "%'))"

        # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold

        logger.info('auth --------' + d)
        columnsToConvert = config.cash_columnsToConvert
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #   cursor.close()
    return False, ''


def read_col_query_auditTrail(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)
    logger.info(query['link_id'])
    # joined_raw = "','".join(set(query['link_id']))
    # logger.info(joined_raw)
    if connection is not None:
        d = "select " + cols_to_query + " from cash_output_txn where raw_link in ('" + str(query['raw_link']) + "')"

        # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold

        logger.info('auth --------' + d)
        columnsToConvert = config.cash_columnsToConvert
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #   cursor.close()
    return False, ''


def read_col_query_unmatched_all(connection, query):
    if connection is not None:
        d = "select * from cash_output_txn where entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'UNMATCHED' and reconciliation_status = 'EXCEPTION' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION')"
        parseDates = []
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        columnsToConvert = config.cash_columnsToConvert
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    # cursor.close()
    return False, ''


def read_col_query_matched_all(connection, query):
    if connection is not None:
        d = "select * from cash_output_txn where entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and reconciliation_status = 'RECONCILED' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZED')"
        parseDates = []
        columnsToConvert = config.cash_columnsToConvert
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    # cursor.close()
    return False, ''


def read_col_query_rollback_all(connection, query):
    if connection is not None:
        d = "select * from cash_output_txn where entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'ROLLBACK_AUTHORIZATION_PENDING'"
        parseDates = []
        columnsToConvert = config.cash_columnsToConvert
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #   cursor.close()
    return False, ''


def read_col_query_pending_all(connection, query):
    if connection is not None:
        d = "select * from cash_output_txn where entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'MATCHED' and forcematch_authorization = 'AUTHORIZATION_SUBMISSION_PENDING' "
        parseDates = []
        columnsToConvert = config.cash_columnsToConvert
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    # cursor.close()
    return False, ''


def read_col_query_waitingForAuthorization_all(connection, query):
    if connection is not None:
        d = "select * from cash_output_txn where entry_type = 'T' and record_status = 'ACTIVE' and authorization_status = 'AUTHORIZED' and forcematch_authorization = 'WAITING_FOR_AUTHORIZATION'"
        parseDates = []
        columnsToConvert = config.cash_columnsToConvert
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    # cursor.close()
    return False, ''


def insertDFToDB(df, table_name, connection):
    # print df
    # Drop tmp validation column

    if table_name == "cash_output_txn":
        del_columns = ["SRC_COUNT", "count", "AGEING"]

        for del_val in del_columns:
            if del_val in df.columns:
                del df[del_val]

    # Audit fix, remove all special characters from comment string
    for col in ['RESOLUTION_COMMENTS', 'FREE_TEXT_1', 'FREE_TEXT_2', 'FREE_TEXT_3']:
        if col in df.columns:
            df[col] = df[col].fillna('').astype(str).str.replace('[^A-Za-z0-9&_ \(\)]+', '')

    cols = [k.replace(' ', '_').strip() for k in df.dtypes.index]
    colnames = ','.join(cols)
    colpos = ', '.join([':' + str(i + 1) for i, f in enumerate(cols)])
    insert_sql = 'INSERT INTO %s (%s) VALUES (%s)' % (table_name, colnames, colpos)
    df.fillna('', inplace=True)
    dfCols = df.columns.tolist()
    df.columns = [str(i) for i in range(1, len(dfCols) + 1)]

    # Internal error fix, replace non-ascii values with ''
    df.replace({r'[^\x00-\x7F]+': ''}, regex=True, inplace=True)

    # XSS fix remove in-secure html tags before saving
    df.replace(r'<[^>]+>', '', regex=True, inplace=True)
    data = df.to_dict(orient='records')

    cur = connection.cursor()
    try:
        cur.executemany(insert_sql, data)
    except cx_Oracle.DatabaseError as db_exe:
        logger.info("database-exception caught while inserting into table " + str(table_name))
        error, = db_exe.args
        logger.info(error)
        logger.info(error.code)
        logger.info(error.message)
        logger.info(error.context)
        raise
    except Exception as excep:
        for x in data:
            try:
                cur.executemany(insert_sql, [x])
            except cx_Oracle.DatabaseError as db_exe:
                logger.info("database-exception caught while inserting into table " + str(table_name))
                error, = db_exe.args
                logger.info(error)
                logger.info(error.code)
                logger.info(error.message)
                logger.info(error.context)
                raise
            except Exception as excep:
                logger.info('Insert to table failed "{0}" '.format(table_name))
                logger.info(insert_sql)
                logger.info('Problem with "{0}" '.format(str(x)))
                raise
    finally:
        if cur is not None:
            cur.close()
    return True, "Success"


# TODO clear tmp table for fast accesability
def updateSqlData(query, connection, rollBackRecordIndicator=False):
    cursor = connection.cursor()
    uuid = str(uuid4().int & (1 << 64) - 1)
    df = pandas.DataFrame(query)[['LINK_ID', 'OBJECT_OID']]
    df['UUID'] = uuid
    # update tmp table and inactivate records using tmp table
    insertDFToDB(df, 'tmp_table', connection)
    try:
        if cursor is not None:
            # cursor.execute("update cash_output_txn set record_status = 'INACTIVE', updated_date = systimestamp, updated_by = '"+str(query[0]['UPDATED_BY'])+"' where (object_oid,0) in (('"+ objects + "',0)) and record_status = 'ACTIVE'")
            cursor.execute(
                "update cash_output_txn set record_status = 'INACTIVE', updated_date = systimestamp, updated_by = '" + str(
                    query[0][
                        'UPDATED_BY']) + "' where object_oid in (select object_oid from tmp_table where uuid = '%s') "
                                         "and record_status = 'ACTIVE'" % str(uuid))
            if cursor.rowcount != len(query):
                return False, 'Please Refresh Page'
    except cx_Oracle.DatabaseError as db_exe:
        error, = db_exe.args
        logger.info(error)
        logger.info(error.code)
        logger.info(error.message)
        logger.info(error.context)
        raise
    except Exception as excep:
        logger.info("exception details" + str(excep))
        error, = excep.args
        logger.info(error.code)
        logger.info(error.message)
        logger.info(error.context)
        raise
    finally:
        if cursor is None:
            cursor.close()

    return True, ''


# function not used to commit, instead refer flaskinterface.close_db
def commitToSql(connection):
    # cursor = connection.cursor()
    # if cursor is not None:
    #    logger.info("Commiting.............................................................................")
    #    logger.info('starting time' + str(time.time()))
    #  	 connection.commit()
    #    logger.info('ending time' + str(time.time()))
    # connection.close()
    #    return True,''
    # else:
    #    cursor.close()
    return True, ''


def convertSequenceToDict(list1):
    dict1 = OrderedDict()
    argList = range(1, len(list1) + 1)
    for k, v in zip(argList, list1):
        dict1[str(k)] = v
    for x in dict1.keys():
        if str(dict1[x]) == 'nan':
            dict1[x] = ''
    return dict1


# function not in use
def create_exception_master(query, connection):
    cursor = connection.cursor()
    if cursor is not None:
        cursor.execute(
            "Insert into EXCEPTION_MASTER (EXCEPTION_ID,EXCEPTION_CREATED_DATE,EXCEPTION_PROCESSING_DATE,EXCEPTION_COMPLETION_DATE,EXCEPTION_PRIORITY,EXCEPTION_SEVERITY,GEOGRAPHY_ID,COUNTRY_ID,BRANCH_ID,L1L2STATUS,SERVICE_LEVEL,RECONCILIATION_TYPE,UPDATED_BY,UPDATED_DATE,SESSION_ID,RECORD_END_DATE,RECORD_STATUS,CREATED_DATE,EXCEPTION_CATEGORY,EXCEPTION_DESCRIPTION,EXCEPTION_OID,EXCEPTION_STATUS,EXCEPTION_TYPE_ID,FUNCTIONAL_AREA_ID,IPADDRESS,PRODUCTLINE_ID,RECON_ID,RECORD_VERSION,CREATED_BY,LINK_ID,BUSINESS_CONTEXT_ID,RECON_EXECUTION_ID,BUSINESS_PROCESS_ID,MANUAL_ENTRY_FLAG,PROCESSING_TYPE,ASSET_CLASS_ID,TRADE_DATE,PARENT_EXCEPTION_ID,MERGED_EXCEPTION_ID,GROUPED_EXCEPTION_ID,EXCEPTION_GROUP_INDICATOR,TOTAL_OUTSTANDING_AMOUNT,CLEARING_DATE,REASON_CODE,COMMENTS,EXCEPTION_AUTHORIZATION_STATUS,SUSPENSE_ENTRY_FLAG) values ('7074962758030049630',to_date('06-05-16','DD-MM-RR'),null,null,null,null,1,1,1,null,null,null,null,null,'ehqEt_oY_eIy1ObygMME_ayJxerhTw2Njm0f89B-y5yRVVa-5xlx!-1733268559!1462515530264',null,'ACTIVE',to_timestamp('06-05-16 11:50:44.843000000 AM','DD-MM-RR HH12:MI:SS.FF AM'),null,null,7161981391399177400,'OPEN',null,147,'172.16.8.69',1,'FIC_CASH_APAC_19074',1,'configurer2',7155283327835767318,'1111148147112',18216,'148',null,null,'1',null,null,null,null,null,null,null,'Inward Remittance not realized','inward remittacne not realized',null,null)")


def getExceptionMasterDetails(query, connection):
    logger.info('query' + str(query))
    if connection is not None:
        d = "select * from exception_master where record_status = 'ACTIVE' and link_Id in ('" + str(
            query['LINK_ID']) + "') and recon_execution_id = '" + str(query['RECON_EXECUTION_ID']) + "'"
        logger.info('query----' + str(d))
        toparse = {'CREATED_DATE': 'np.datetime64', 'TRADE_DATE': 'np.datetime64', 'UPDATED_DATE': 'np.datetime64',
                   'CLEARING_DATE': 'np.datetime64', 'RECORD_END_DATE': 'np.datetime64',
                   'EXCEPTION_CREATED_DATE': 'np.datetime64', 'EXCEPTION_COMPLETION_DATE': 'np.datetime64',
                   'EXCEPTION_PROCESSING_DATE': 'np.datetime64'}
        parseDates = []
        for k, v in toparse.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        return True, df
    return False, ''


def getExceptionAllocationDetails(query, connection):
    if connection is not None:
        d = "select * from exception_allocation where record_status = 'ACTIVE' and exception_id in (select exception_id from exception_master where record_status = 'ACTIVE' and link_id in ('" + str(
            query['LINK_ID']) + "') and recon_execution_id  = '" + str(query['RECON_EXECUTION_ID']) + "')"
        toparse = {'CREATED_DATE': 'np.datetime64', 'ALLOCATION_TIME': 'np.datetime64'}
        parseDates = []
        for k, v in toparse.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        if 'COMPLETION_DATE_TIME' in df.columns:
            df.drop('COMPLETION_DATE_TIME', axis=1, inplace=True)
        # connection.close()
        logger.info(df)

        return True, df
    return False, ''


def getTxnDetails(query, recon_id, connection):
    if connection is not None:
        d = "select * from cash_output_txn where record_status = 'ACTIVE' and recon_id = '" + recon_id + "' and link_Id = '" + str(
            query["LINK_ID"]) + "' and recon_execution_id = '" + query['RECON_EXECUTION_ID'] + "'"
        parseDates = []
        for k, v in datatypes.datatypes.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def getAcPools(query, connection):
    if connection is not None:
        dataFrame = pandas.DataFrame()
        dataFrame = pandas.read_sql("select * from account_pool_context_map where record_status = 'ACTIVE'", connection)
        # connection.close()
        return True, dataFrame
    # if cursor is None:
    #    cursor.close()
    return False, ""


def getExecutionSummary(query, connection):
    (concat_str, concat_data, concat_data_ex) = application.ReportInterface().getBusinessContextPerUser('dummy')
    if connection is not None:
        d = "SELECT * FROM recon_execution_details_Log txn where txn.record_status = 'ACTIVE' and trunc(txn.execution_date) = trunc(systimestamp)" + concat_data_ex
        logger.info(d)
        # parseDates = []
        # for k,v in datatypes.datatypes.items():
        #     if v == "np.datetime64":
        #         parseDates.append(k)
        df = pandas.read_sql(d, connection)
        columnsToConvert = ['RECON_SCHEDULE_ID', 'RECON_EXECUTION_OID']
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def getExecutionSummary_statement_date(connection, query):
    (concat_str, concat_data, concat_data_ex) = application.ReportInterface().getBusinessContextPerUser('dummy')
    if connection is not None:
        d = "SELECT * FROM recon_execution_details_Log txn  where txn.record_status = 'ACTIVE' and trunc(txn.statement_date) between to_date('" + \
            query['statementStartDate'] + "','dd/mm/yy') and to_date('" + query[
                'statementEndDate'] + "','dd/mm/yy')" + concat_data_ex
        logger.info("sql----query" + d)
        parseDates = ['PROCESSING_STATE_DATE_TIME', 'RECON_COMPLETION_DATE', 'STATEMENT_DATE', 'UPDATED_DATE',
                      'CREATED_DATE', 'RECORD_END_DATE', 'EXECUTION_DATE']
        columnsToConvert = ['RECON_SCHEDULE_ID', 'RECON_EXECUTION_OID']
        # for k,v in datatypes.datatypes.items():
        #     if v == "np.datetime64":
        #         parseDates.append(k)
        df = pandas.read_sql(d, connection)
        logger.info(df.columns)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def getExecutionSummary_execution_date(connection, query):
    (concat_str, concat_data, concat_data_ex) = application.ReportInterface().getBusinessContextPerUser('dummy')
    if connection is not None:
        logger.info("query" + str(query))
        d = "SELECT * FROM recon_execution_details_Log txn where txn.record_status = 'ACTIVE' and  trunc(txn.execution_date) between to_date('" + \
            query['executionStartDate'] + "','dd/mm/yy') and to_date('" + query[
                'executionEndDate'] + "','dd/mm/yy')" + concat_data_ex
        logger.info("sql----query" + d)
        parseDates = ['PROCESSING_STATE_DATE_TIME', 'RECON_COMPLETION_DATE', 'STATEMENT_DATE', 'UPDATED_DATE',
                      'CREATED_DATE', 'RECORD_END_DATE', 'EXECUTION_DATE']
        columnsToConvert = ['RECON_SCHEDULE_ID', 'RECON_EXECUTION_OID', 'RECON_POSITIONS_ID', 'RECON_POSITIONS_OID']
        # for k,v in datatypes.datatypes.items():
        #     if v == "np.datetime64":
        #         parseDates.append(k)
        df = pandas.read_sql(d, connection)
        logger.info(df.columns)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def getExecutionSummary_ex_st_date(connection, query):
    (concat_str, concat_data, concat_data_ex) = application.ReportInterface().getBusinessContextPerUser('dummy')
    if connection is not None:
        logger.info("query" + str(query))
        d = "SELECT * FROM recon_execution_details_Log txn where  txn.record_status = 'ACTIVE'  and trunc(txn.statement_date) between to_date('" + \
            query['statementStartDate'] + "','dd/mm/yy') and to_date('" + query[
                'statementEndDate'] + "','dd/mm/yy') and trunc(txn.execution_date) between to_date('" + query[
                'executionStartDate'] + "','dd/mm/yy') and to_date('" + query[
                'executionEndDate'] + "','dd/mm/yy')" + concat_data_ex
        logger.info("sql----query" + d)
        parseDates = ['PROCESSING_STATE_DATE_TIME', 'RECON_COMPLETION_DATE', 'STATEMENT_DATE', 'UPDATED_DATE',
                      'CREATED_DATE', 'RECORD_END_DATE', 'EXECUTION_DATE']
        columnsToConvert = ['RECON_SCHEDULE_ID', 'RECON_EXECUTION_OID', 'RECON_POSITIONS_ID', 'RECON_POSITIONS_OID']
        # for k,v in datatypes.datatypes.items():
        #     if v == "np.datetime64":
        #         parseDates.append(k)
        df = pandas.read_sql(d, connection)
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
        # connection.close()
        return True, df
    # if cursor is None:
    #   cursor.close()
    return False, ''


# get exception details for a list of link_id
def getExceptionMasDetails(query, connection):
    logger.info('query' + str(query))
    link_id_list = "','".join(str(v) for v in query["LINK_ID"])
    logger.info(link_id_list)
    if connection is not None:
        d = "select * from exception_master where record_status = 'ACTIVE' and link_Id in ('" + link_id_list + "')"
        logger.info('query----' + str(d))
        toparse = {'CREATED_DATE': 'np.datetime64', 'TRADE_DATE': 'np.datetime64', 'UPDATED_DATE': 'np.datetime64',
                   'CLEARING_DATE': 'np.datetime64', 'RECORD_END_DATE': 'np.datetime64',
                   'EXCEPTION_CREATED_DATE': 'np.datetime64', 'EXCEPTION_COMPLETION_DATE': 'np.datetime64',
                   'EXCEPTION_PROCESSING_DATE': 'np.datetime64'}
        parseDates = []
        for k, v in toparse.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)

        logger.info(df)
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''

# get exceptioin allocation details for a list of exception_id in exception_master
def getExceptionAlloDetails(query, connection):
    if connection is not None:
        link_id_list = "','".join(str(v) for v in query["LINK_ID"])
        d = "select * from exception_allocation where record_status = 'ACTIVE' and exception_id in (select exception_id from exception_master where record_status = 'ACTIVE' and link_id in ('" + link_id_list + "'))"
        toparse = {'CREATED_DATE': 'np.datetime64', 'ALLOCATION_TIME': 'np.datetime64'}
        parseDates = []
        for k, v in toparse.items():
            if v == "np.datetime64":
                parseDates.append(k)
        df = pandas.read_sql(d, connection, parse_dates=parseDates)
        if 'COMPLETION_DATE_TIME' in df.columns:
            df.drop('COMPLETION_DATE_TIME', axis=1, inplace=True)
        # connection.close()
        return True, df
    # if cursor is None:
    #    cursor.close()
    return False, ''


def read_col_query_allocation_history(query, connection):
    # query = "select exception_id,allocation_time,exception_resolution_status,action_by,allocated_by from exception_allocation where exception_id in " \
    #         " (select exception_id from exception_master where record_status = 'ACTIVE' and link_id = '" +str(query['link_id'])+ "') and allocation_history_flag = 'Y'"
    sql_query = "select CREATED_DATE,CREATED_BY,AUTHORIZATION_BY,RESOLUTION_COMMENTS from CASH_OUTPUT_TXN " \
                "where LINK_ID = '%s' and RECON_EXECUTION_ID = %s and TXN_MATCHING_STATUS = 'AUTHORIZATION_PENDING'" % (
                    str(query['link_id']), str(query['recon_execution_id']))
    logger.info(sql_query)
    if connection is not None:
        df = pandas.read_sql(sql_query, connection)
    if len(df.index) > 0:
        df.rename(columns={'AUTHORIZATION_BY': 'ALLOCATED_TO', 'CREATED_BY': 'ALLOCATED_BY',
                           'CREATED_DATE': 'ALLOCATION_TIME'}, inplace=True)
        df = df.drop_duplicates(['ALLOCATED_TO'], keep='last')
        return True, df
    else:
        return False, ''


def read_col_query_indox_txn(connection, query):
    user_id = flask.session['sessionData']['userName']

    current_exec_id = get_max_execution_id(connection, query['recon_id'])
    parseUserpools = []

    (status, columnsList) = read_col_query(connection=connection, query=query)
    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols)
    cols_to_query = ",".join(cols)

    if current_exec_id is None:
        return True, pandas.DataFrame()
    if query['userRole'] == 'Dept User':
        (status_userpools, userpools_data) = application.UserPool().getAll({'perColConditions': {'users': user_id}})
        if userpools_data['total'] > 0:
            for r in userpools_data['data']:
                logger.info(r['name'])
                parseUserpools.append(r['name'])


        query = "select " + cols_to_query + " from cash_output_txn where record_status = 'ACTIVE' and recon_execution_id = '%s' and recon_id = '%s' and authorization_by " \
                "in ('%s')" % (str(current_exec_id), query['recon_id'], "','".join(parseUserpools))
    else:
        query = "select "+ cols_to_query +" from cash_output_txn " \
                "where record_status = 'ACTIVE' and recon_execution_id = '%s' and recon_id = '%s' and updated_by" \
                "= ('%s')" % (str(current_exec_id), query['recon_id'], user_id)

    logger.info(query)
    if connection is not None:
        df = pandas.read_sql(query, connection)
        # df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'", connection)
        # df = pandas.merge(df, df2, on=["RECON_ID"], how="left")
        df['LINK_ID'] = df['LINK_ID'].apply(str, 1)
        columnsToConvert = config.cash_columnsToConvert
        for x in columnsToConvert:
            if x in df.columns:
                df[x] = df[x].apply(str, 1)
    if df.empty:
        return True, pandas.DataFrame()
    else:
        return True, df


def getLatestExecutionID(connection, reconId):
    if connection is not None:
        query = "select max(RECON_EXECUTION_ID) as recon_execution_id from recon_execution_details_log where record_status = 'ACTIVE' and PROCESSING_STATE_STATUS = 'Matching Completed' and recon_id = '" + \
                str(reconId) + "'"
        current_exec_id = pandas.read_sql(query, connection)['RECON_EXECUTION_ID'].astype('str')[0]

        return current_exec_id

def getLatestExecutionLog(connection, query):
    if connection is not None:
        current_exec_query = "select max(recon_execution_id) as recon_execution_id,recon_id from recon_execution_details_log where record_status = 'ACTIVE' and processing_state_status = 'Matching Completed' " + query + " group by recon_id"
        rows = pandas.read_sql(current_exec_query, connection)
        logger.info(rows)
        execution_ids = ",".join(rows['RECON_EXECUTION_ID'].astype('str').tolist())

        qry = "select execution_date,statement_date,recon_id from recon_execution_details_log where record_status = 'ACTIVE' and recon_execution_id in (" + execution_ids + ")"
        df = pandas.read_sql(qry, connection)
        logger.info(qry)
        if df.empty:
            return True, pandas.DataFrame()
        else:
            return True, df


def getNostroExceptionReport(connection, query):
    (status, columnsList) = read_col_query(connection=connection, query=query)
    current_exec_id = get_max_execution_id(connection=connection, recon_id=str(query['recon_id']))

    if current_exec_id is None:
        return True, pandas.DataFrame()

    parsedate_cols = columnsList[columnsList['MDL_FIELD_DATA_TYPE'].isin(['DATE', 'TIMESTAMP'])][
        'MDL_FIELD_ID'].tolist()
    cols = set(columnsList['MDL_FIELD_ID'].tolist() + config.staticCols + ['Free_Text_1', 'Free_Text_2', 'Free_Text_3'])
    logger.info("TOtal nostro cols" + cols)
    cols_to_query = ",".join(cols)
    if 'userPools' in query:
        logger.info(query["userPools"])
        dept_user_pools = []
        dept_user_pools = query['userPools'].split(',')
        tosend = "','".join(dept_user_pools)
    logger.info(query)
    if connection is not None:
        d = "select " + cols_to_query + " from cash_output_txn where recon_id = '" + query[
            'recon_id'] + "' and account_number like '%" + str(query[
                                                                   'accountNumber']) + "%' and entry_type = 'T' and record_status = 'ACTIVE' and matching_status = 'UNMATCHED' and reconciliation_status = 'EXCEPTION' and (forcematch_authorization is null or forcematch_authorization = 'AUTHORIZATION_REJECTION') and recon_execution_id = '" + str(
            current_exec_id) + "'"
        # Add limiting parameter
        if query['recon_id'] in config.threshold_recons:
            d += " and ROWNUM <= " + config.threshold
        logger.info('unmatched--------' + d)
        df = pandas.read_sql(d, connection, parse_dates=parsedate_cols)
        # Ageing patch for un-matched column
        data = json.load(open(config.ageing))
        if len(df) > 0:
            tm_yday = datetime.datetime.now().timetuple().tm_yday
            # df['AGEING'] = tm_yday - df.loc[data.get(query['recon_id'],'STATEMENT_DATE')].dt.dayofyear
            df['AGEING'] = df['VALUE_DATE'].apply(lambda x: (datetime.datetime.now() - x)).dt.days
            logger.info(parsedate_cols)
            columnsToConvert = config.cash_columnsToConvert
            for x in columnsToConvert:
                if x in df.columns:
                    df[x] = df[x].apply(str, 1)
            return True, df
        else:
            return True, pandas.DataFrame()
    # if cursor is None:
    #    cursor.close()
    return False, ''
