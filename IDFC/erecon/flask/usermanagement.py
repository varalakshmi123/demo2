import AesEncrypt
import dbinterface
import hashlib
import base64
import flask
import utilities
import json
import config
import inspect
import pyclbr
import bson
import re
import datetime
import os
import logger
import httplib2
import random
import time
from datetime import datetime
from urlparse import urlparse
from ActiveDirectory import *
from shutil import rmtree

logger = logger.Logger.getInstance("application").getLogger()

# Key is the name of the role, value is the list of roles that can create the role
roles = {}
# Key is the name of the role, value is an object with collection name as key and list of functions as value
acls = {}


def apiKey(userId):
    return base64.b64encode(hashlib.sha256(userId).hexdigest())


def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)


def login():
    if flask.request.method == 'POST' and "sessionData" not in flask.session:
        userQuery = None
        logger.info(flask.request.json)
        logger.info(flask.request.json['userName'])
        flask.request.json['userName'] = AesEncrypt.decrypt(config.aes_key, config.iv, flask.request.json['userName'])
        flask.request.json['userName'], decryptKey = tuple(flask.request.json['userName'].split('|'))

        flask.request.json['password'] = AesEncrypt.decrypt(decryptKey, config.iv, flask.request.json['password'])
        flask.request.json['password'] = flask.request.json['password'].replace('/<=%+,?->/', '')

        logger.info(flask.request.json['password'])
        # Validate user/password
        flask.request.json["userName"] = flask.request.json["userName"].lower()
        if flask.request.remote_addr == "127.0.0.1":
            userQuery = {"userName": flask.request.json["userName"]}
        elif "userName" in flask.request.json and "password" in flask.request.json:
            if flask.request.json["userName"] == 'admin':
                userQuery = {"userName": flask.request.json["userName"],
                             "password": flask.request.json['password']}
            else:
                userQuery = {"userName": flask.request.json["userName"]}

        elif "userName" in flask.request.json and "apiKey" in flask.request.json:
            (status, found) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})
            if status and found and len(found):
                if flask.request.json["apiKey"] == apiKey(found["_id"]):
                    userQuery = {"userName": flask.request.json["userName"]}

        if userQuery:
            # logger.info("userquery-----" + str(userQuery))
            (status, found) = Users(hideFields=False).get(userQuery)
            # if 'lastPwdUpdt' in found:
            #     today = datetime.now()
            #     lastDay = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(found['lastPwdUpdt'])))
            #     lastDay = datetime.strptime(lastDay, "%Y-%m-%d %H:%M:%S")
            #     diffDays = (today - lastDay).days
            #     if diffDays >= 145:
            #         return flask.Response(
            #             json.dumps({"status": "error", "msg": "Your password got Expired, Please Contact Admin."}), 200,
            #             [("Content-Type", "application/json")])
            (statApprvl, foundApprvl) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})

            if "isApproved" in foundApprvl and not foundApprvl["isApproved"]:
                # if not found["isApproved"]:
                return flask.Response(
                    json.dumps({"status": "error", "msg": "Waiting for Approval, Please Contact Admin."}), 200,
                    [("Content-Type", "application/json")])

            if "accountLocked" in found:
                if found["accountLocked"]:
                    return flask.Response(
                        json.dumps({"status": "error", "msg": "Account is Locked Please Contact Admin."}), 200,
                        [("Content-Type", "application/json")])
            authenticateStatus = False
            if status and found and len(found):
                # Checking if user is active and allowed to login
                if 'isActive' in found and found['isActive'] is False:
                    return flask.Response(json.dumps(
                        {"status": "error", "msg": "User is disabled for login,please contact Administrator."}), 200,
                                          [("Content-Type", "application/json")])
                if config.ldapAuthenticate and flask.request.json["userName"] != 'admin':
                    authenticateStatus = validateADUser(flask.request.json["userName"], flask.request.json['password'])

                elif not config.ldapAuthenticate and flask.request.json["userName"] != 'admin':
                    userQuery = {"userName": flask.request.json["userName"],
                                 "password": hashlib.md5(flask.request.json['password']).hexdigest()}
                    (status, found) = Users(hideFields=False).get(userQuery)
                    authenticateStatus = status

                if flask.request.json["userName"] == 'admin':
                    del found["password"]
                    authenticateStatus = True
                    found['loginCount'] = 0
                    found['accountLocked'] = False
                    (status_up, found_up) = Users(hideFields=False).modify(found['_id'], found)
                    flask.session["sessionData"] = found

                elif authenticateStatus:
                    logger.info('Here it is authenticated')
                    del found["password"]
                    found['loginCount'] = 0
                    found['accountLocked'] = False
                    (status_up, found_up) = Users(hideFields=False).modify(found['_id'], found)
                    flask.session["sessionData"] = found

            if not authenticateStatus:
                logger.info('Checking for login Count')
                (status, found) = Users(hideFields=False).get({"userName": flask.request.json["userName"]})
                if not status:
                    return flask.Response(json.dumps({"status": "error", "msg": "Username is incorrect."}), 200,
                                          [("Content-Type", "application/json")])
                #
                elif 'loginCount' in found and (found['loginCount'] != config.loginCount):
                    found['loginCount'] = found['loginCount'] + 1
                    found['accountLocked'] = False
                    del found['password']
                    (status_up, found_up) = Users(hideFields=False).modify(found['_id'], found)
                    return flask.Response(json.dumps({"status": "error",
                                                      "msg": "Username/Password is incorrect. You have " + str(
                                                          (config.loginCount - found_up[
                                                              'loginCount'])) + " attempts Left"}), 200,
                                          [("Content-Type", "application/json")])

                elif 'loginCount' in found and found['loginCount'] == config.loginCount:
                    found['accountLocked'] = True
                    del found['password']
                    (status_up, found_up) = Users(hideFields=False).modify(found['_id'], found)
                    return flask.Response(
                        json.dumps({"status": "error", "msg": "Account is Locked Please Contact Admin."}), 200,
                        [("Content-Type", "application/json")])
                else:
                    found['loginCount'] = 1
                    found['accountLocked'] = False
                    del found['password']
                    (status_up, found_up) = Users(hideFields=False).modify(found['_id'], found)
                    return flask.Response(json.dumps({"status": "error",
                                                      "msg": "Username/Password is incorrect. You have " + str(
                                                          (config.loginCount - found_up[
                                                              'loginCount'])) + " attempts Left"}), 200,
                                          [("Content-Type", "application/json")])

    if "sessionData" in flask.session:
        if '_id' not in flask.session['sessionData']:
            flask.session['sessionData']['_id'] = found_up['_id']
        if 'partnerId' not in flask.session['sessionData']:
            flask.session['sessionData']['partnerId'] = found_up['partnerId']
        if 'lastLogin' not in flask.session['sessionData']:
            flask.session['sessionData']['lastLogin'] = int(time.time())
            found_up['lastLogin'] = int(time.time())
            Users(hideFields=False).modify(found_up['_id'], found_up)
        # flask.session["sessionData"]["role"] = "customer"
        return flask.Response(json.dumps(
            {"status": "ok",
             "msg": {"_id": flask.session["sessionData"]["_id"], "lastLogin": flask.session['sessionData']['lastLogin'],
                     "partnerId": flask.session["sessionData"]["partnerId"],
                     "apiKey": flask.session["sessionData"]["apiKey"], "role": flask.session["sessionData"]["role"],
                     "userName": flask.session["sessionData"]["userName"],
                     "machineId": utilities.getMachineId()}}), 200, [("Content-Type", "application/json")])
    else:
        flask.abort(401)


def logout():
    if 'sessionData' in flask.session:
        found = flask.session['sessionData']
        removeFiles(found['_id'])
        found['lastLogin'] = int(time.time())
        if 'old_passwords' in found:del found['old_passwords']
        (status_up,found_up) = Users(hideFields=False).modify(found['_id'],found)
    #if "sessionData" in flask.session:

    flask.session.clear()
    # May be update user for LastLogOut
    flask.abort(401)


# IRecon App remove report folders
def removeFiles(folder_name):
    rmtree(config.contentDir + "/" + "recon_reports/" + folder_name, ignore_errors=True)


def has_permission(role, collection, operation):
    global acls
    if len(acls) > 0:
        if role in acls:
            if collection in acls[role]:
                if operation in acls[role][collection]:
                    return True
                elif "*" in acls[role][collection]:
                    return True
            elif "*" in acls[role]:
                if operation in acls[role]["*"]:
                    return True
                elif "*" in acls[role]["*"]:
                    return True
        return False

    return True

# Called from flaskinterface.py
def initRoleAndAcl():
    global acls, roles
    acls = {}
    roles = {}
    (status, data) = Acls().getAll()
    for acl in data["data"]:
        acls[acl["role"]] = acl["access"]
        # roles[acl["role"]] = acl["creators"]


def createUser(doc):
    from pymongo import MongoClient
    mongoHost = MongoClient[config.mongoHost]
    db = mongoHost[config.usersDatabaseName]
    users = db.users
    doc = users.find_one({"userName": doc["userName"]})
    if doc:
        return False, "User already exists"
    doc["apiKey"] = base64.b64encode(hashlib.sha256(str(doc["_id"])).hexdigest())
    users.insert(doc)
    return True,


class Users(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Users, self).__init__("users", hideFields, mongoHost=config.usersMongoHost,
                                    mongoDBName=config.usersDatabaseName)

    def getHiddenFields(self):
        return ['password', "apiKey", "old_passwords", "oldPasswords"]

    def create(self, doc):
        if not all(prop in doc for prop in ["userName", "password", "role"]):
            return False, "Invalid input, Missing mandatory fields"

        if not len(roles):
            initRoleAndAcl()

        # If reseller, adding customer into roles for reseller so that he can create customer
        # if flask.session["sessionData"]["role"] != "reseller":
        #     if doc["role"] not in roles or flask.session["sessionData"]["role"] not in roles[doc["role"]]:
        #         logger.info("Error creating user")
        #         logger.info("SessionData: %s" % flask.session["sessionData"]["role"])
        #         logger.info("RolesData: %s" % doc['role'])
        #         logger.info("Roles: %s" % roles)
        #         return False, "Unauthorized to create this role"

        if self.exists({"userName": doc["userName"]}):
            return False, "User already exists"

        if ("password" in doc) and (len(doc['password'])):
	    if re.search(config.passwordRule, doc['password']) is None:
		return False, "Password Policy is not satisfied."
            doc["password"] = hashlib.md5(doc['password']).hexdigest()

        doc["_id"] = bson.objectid.ObjectId()
        # doc["partnerId"] = str(doc["_id"])
        doc["apiKey"] = base64.b64encode(hashlib.sha256(str(doc["_id"])).hexdigest())
        return super(Users, self).create(doc)

    def modify(self, condition, doc, multi=False):
        logger.info('coming here too')
        # if ("role" in doc) and ("admin" not in doc["role"]):
        #     return False, "Unauthorized to update this role"
        logger.info(doc)
        if "password" in doc:
            logger.info("inside")

	    if re.search(config.passwordRule, doc['password']) is None:
                return False, "Password Policy is not satisfied."

            doc["password"] = hashlib.md5(doc['password']).hexdigest()
        logger.info(doc)
        return super(Users, self).modify(condition, doc)

    def deleteData(self, condition):
        (status, found) = Users(hideFields=False).get({'_id': condition})
        return super(Users, self).delete(condition)

    def getUser(sefl,userName):
        userQuery = {'partnerId': flask.session["sessionData"]['partnerId'], "userName": getMongoCaseInsensitive(userName)}
        return Users(hideFields=True).get(userQuery)

    def getAllRoles(self, dummy):
        global roles
        rolesList = []
        for role in roles:
            if role not in ["integrator"]:
                rolesList.append(role)
        resp = {}
        resp["roles"] = rolesList
        return True, resp

    def changePassword(self, userId, userDetails):
        userName = AesEncrypt.decrypt(config.aes_key, config.iv, userDetails['userName'])
        (userName, decryptKey) = tuple(userName.split('|'))

        password = AesEncrypt.decrypt(decryptKey, config.iv, userDetails['password'])
        password = password.replace('/<=%+,?->/', '')

        doc = dict()
        (status, found) = Users(hideFields=True).get({'_id': userId})

        if 'lastPwdUpdt' in found:
            doc['lastPwdUpdt'] = datetime.now().strftime('%s')
        else:
            doc['lastPwdUpdt'] = datetime.now().strftime('%s')

        if re.search(config.passwordRule, password) is None:
            return False, "Password Policy is not satisfied."

        doc['password'] = hashlib.md5(password).hexdigest()
        if '#' in doc['password']:
            return False, "The # character cannot be used in the password ...."
        elif 'old_passwords' in found and found['old_passwords']:
            if doc['password'] in found['old_passwords']:
                return False, "Cannot use old passwords, try with new password"
            elif len(found['old_passwords'].split(',')) >= 10:
                found['old_passwords'] = ','.join(found['old_passwords'].split(',')[1:])
            found['old_passwords'] = found['old_passwords'] + ',' + doc['password']
            doc['old_passwords'] = found['old_passwords']
        else:
            doc['old_passwords'] = doc['password']
        # eventDoc ={'type':'password change','createdBy':flask.session["sessionData"]['userName'],'createdDate':utilities.getUtcTime(),'actionOn':found['userName']}
        # Events().create(eventDoc)
        return super(Users, self).modify(userId, doc)

    def updatePassword(self, userId, userDetails):
        userName = AesEncrypt.decrypt(config.aes_key, config.iv, userDetails['userName'])
        userName, decryptKey = tuple(userName.split('|'))

        logger.info(decryptKey)

        password = AesEncrypt.decrypt(decryptKey, config.iv, userDetails['password'])
        password = password.replace('/<=%+,?->/', '')

        oldpassword = userDetails['oldpassword']
        logger.info(oldpassword)
        #
        # oldpassword= AesEncrypt.decrypt(decryptKey, config.iv, userDetails['oldpassword'])
        # oldpassword= oldpassword.replace('/<=%+,?->/', '')
        # logger.info(oldpassword)
        doc = dict()
        if oldpassword is not None:
            (status, found) = Users(hideFields=False).get({'_id': userId})
            if re.search(config.passwordRule, password) is None:
                return False, "Password Policy is not satisfied."

            if found is not None:
                if found['password'] == hashlib.md5(oldpassword).hexdigest():
                    doc['password'] = hashlib.md5(password).hexdigest()
                    if '#' in doc['password']:
                        return False, "The # character cannot be used in the password ...."
                    elif 'old_passwords' in found and found['old_passwords']:
                        if doc['password'] in found['old_passwords']:
                            return False, "Cannot use old passwords, try with new password"
                        elif len(found['old_passwords'].split(',')) >= 10:
                            found['old_passwords'] = ','.join(found['old_passwords'].split(',')[1:])
                        found['old_passwords'] = found['old_passwords'] + ',' + doc['password']
                        doc['old_passwords'] = found['old_passwords']
                    else:
                        doc['old_passwords'] = doc['password']
                    if 'lastPwdUpdt' in found:
                        doc['lastPwdUpdt'] = datetime.now().strftime('%s')
                    else:
                        doc['lastPwdUpdt'] = datetime.now().strftime('%s')

                    return super(Users, self).modify(userId, doc)
                else:
                    return False, "Old password is incorrect"
        else:
            return False, 'oldpassword is required'

    def setEmail(self, userId, email):
        doc = dict()
        doc['email'] = email
        (status, found) = Users(hideFields=False).get({'_id': userId})
        # eventDoc = {'type':'email updation','changedBy':flask.session["sessionData"]['userName'],'actionOn':found['userName'],'createdDate':utilities.getUtcTime()}
        # logger.info(eventDoc)
        # Events().create(eventDoc)
        return super(Users, self).modify(userId, doc)

    def updateRole(self, userId, role):
        doc = dict()
        doc['role'] = role
        # (status,found) = Users(hideFields=False).get({'_id': userId})
        return super(Users, self).modify(userId, doc)

    def getAll(self, query={}):
        print 'Coming here...!', query
        if 'condition' not in query: query['condition'] = {}
        query['condition']['isApproved'] = False
        print flask.session["sessionData"]['role']
        if flask.session["sessionData"]['role'] != 'Approver':
            query['condition']['isApproved'] = True
        print query
        stats, resp = super(Users, self).getAll(query)
        return stats, resp


class Acls(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        # super(Acls, self).__init__("acls", hideFields, mongoHost=config.usersMongoHost, mongoDBName=config.usersDatabaseName)
        super(Acls, self).__init__("acls", hideFields)

    def getAll(self, query={}):
        (status, resp) = super(Acls, self).getAll()
        try:
            if 'sessionData' in flask.session and flask.session["sessionData"]["role"] == "reseller":
                # Adding the customer acl to reseller so the reseller can create the user
                data = Acls().find_one({"role": "customer"})
                resp['total'] += 1
                resp['current'] += 1
                resp['data'].append(data)
        except Exception, e:
            pass
        return status, resp

    def create(self, doc):
        if flask.session["sessionData"]["role"] == "admin":
            resp = super(Acls, self).create(doc)
            initRoleAndAcl()
            return resp
        return False, "Unauthorized to create new acls"

    def modify(self, condition, doc, multi=False):
        resp = super(Acls, self).update(condition, doc, multi)
        initRoleAndAcl()
        return resp

    def delete(self, condition):
        resp = super(Acls, self).delete(condition)
        initRoleAndAcl()
        return resp

    def getmodules(self, id):
        modules = {}
        for module in config.applicationModules:
            moduleInstance = __import__(module)
            moduleClasses = pyclbr.readmodule(module)
            for klassName, klassSpec in moduleClasses.items():
                modules[klassName] = []
                klassInstance = getattr(moduleInstance, klassName)
                for klassMethod, na in inspect.getmembers(klassInstance):
                    if not klassMethod.startswith("_") and not klassMethod in ['changeOwner', 'count', 'exists', 'getHiddenFields', 'hasAccess', 'setUser']:
                        modules[klassName].append(klassMethod)
        return True, modules
