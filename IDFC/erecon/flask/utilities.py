import subprocess
import os
import uuid
import config
from datetime import datetime, timedelta
import calendar
import telnetlib
import errno

import smtplib
# For guessing MIME type based on file name extension
import mimetypes
import tempfile

from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.audio import MIMEAudio
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

from xml.sax.saxutils import escape

import re

machineId = None

_chars = re.compile("|".join(["<", ">", "\(", "\)", "\{", "\}", "\[", "\]"]))


def execute(command, waitForCompletion=True):
    child = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    if waitForCompletion:
        out, err = child.communicate()
        status = child.returncode
        return status, out, err
    else:
        return child

def getUtcTime(convertDate=None, resetSeconds=False):
    if convertDate is None:
        convertDate = datetime.utcnow()
    if resetSeconds:
        convertDate = convertDate.replace(second=0)

    ts=int(datetime.now().replace(second=0).strftime("%s"))
    utc_offset = datetime.fromtimestamp(ts) - datetime.utcfromtimestamp(ts)
    return int(convertDate.now().strftime("%s"))-utc_offset.seconds

    #Javascript code to convert to local date/time
    #var d = new Date()
    #var n = d.getTimezoneOffset(); //Offset in minutes
    #var utcSeconds = 1385229463;
    #var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    #d.setUTCSeconds(utcSeconds - (n * 60));

def getMachineId():
    global machineId
    if not os.path.exists(".machineId"):
        open(".machineId", "w").write(str(uuid.uuid4()))
        if not os.path.exists(config.dataBackupDir):
            os.makedirs(config.dataBackupDir)
        execute("cp .machineId %s/.machineId" % (config.dataBackupDir))

    if not machineId:
        machineId = open(".machineId", "r").read().strip()

    return machineId

def portCheck(ip, portList):
    # Returns None if the IP is unreachable.
    # Returns a list of ports which failed.
    failedPorts = []
    for port in portList:
        try:
            tn = telnetlib.Telnet(ip, port, 5)
            tn.close()
        except Exception, e:
            if e.errno == errno.EHOSTUNREACH:
                return None
            if e.errno == errno.ECONNREFUSED or e.errno is None:
                failedPorts.append(port)
            else:
                failedPorts.append(port)

    return failedPorts

def sendReportMail(subject, body, msg):
    outer = MIMEMultipart()
    outer['Subject'] = subject
    outer['To'] = config.ticketToEmail
    outer['From'] = config.ticketFromEmail
    outer.preamble = 'This mail contains attachment.\n'
    part1 = MIMEText(body, 'plain')
    outer.attach(part1)

    attachFiles = []
    (fileFd, filePath) = tempfile.mkstemp('.txt', 'log', text=True)
    os.close(fileFd)
    f = open(filePath, 'w')
    f.write(msg)
    f.close()
    attachFiles.append(filePath)
    if attachFiles is not None:
        for filename in attachFiles:
            if not os.path.isfile(filename):
                continue
            # Guess the content type based on the file's extension.  Encoding
            # will be ignored, although we should check for simple things like
            # gzip'd or compressed files.
            ctype, encoding = mimetypes.guess_type(filename)
            if ctype is None or encoding is not None:
                # No guess could be made, or the file is encoded (compressed), so
                # use a generic bag-of-bits type.
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
            if maintype == 'text':
                fp = open(filename)
                # Note: we should handle calculating the charset
                msg = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'image':
                fp = open(filename, 'rb')
                msg = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'audio':
                fp = open(filename, 'rb')
                msg = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(filename, 'rb')
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
                fp.close()
                # Encode the payload using Base64
                encoders.encode_base64(msg)
                # Set the filename parameter
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
            outer.attach(msg)
            # Now send or store the message
    if config.ticketConnectionSecurity == "SSL/TLS":
        s = smtplib.SMTP_SSL(config.ticketServer, config.ticketPort, timeout=10)
    else:
        s = smtplib.SMTP(config.ticketServer, config.ticketPort, timeout=10)
    s.ehlo()
    if config.ticketConnectionSecurity == "STARTTLS":
        s.starttls()
        s.ehlo()
    if len(config.ticketUserName) > 0:
        s.login(config.ticketUserName,config.ticketPassword)
    s.sendmail(config.ticketFromEmail, config.ticketToEmail, outer.as_string())
    s.close()
    for f in attachFiles:
        execute("rm %s" % (f))


def fixXMLData(data):
    fixed = data
    #fixed = escape(fixed, {"'": '&#39;', '"': '&quot;'})
    fixed = escape(fixed, {"'": '\\\'', '"': '&quot;'})
    return fixed

def getMonthFirstLastDate():

    today = datetime.today()
    year = today.strftime("%Y")
    month = today.strftime("%m")
    last_month = int(month)-1

    print  " year ", year, "month ",month

    (first_day, last_day) = calendar.monthrange(int(year), last_month)

    print "first_day, last_day", first_day, last_day

    month_start_date = datetime.strptime(year+"-"+str(last_month)+"-1 00:00:00", "%Y-%m-%d %H:%M:%S")
    month_end_date = datetime.strptime(year+"-"+str(last_month)+"-"+str(last_day)+" 00:00:00", "%Y-%m-%d %H:%M:%S")

    return month_start_date, month_end_date

def getLastOneMonth():

    start_date = datetime.now() + timedelta(-30)
    last_month_date = datetime.strptime(start_date.strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
    conv_date = int(last_month_date.strftime("%s"))

    return conv_date

def escape_chars(doc, repl=""):
    
    if isinstance(doc, dict):
        for k, v in doc.items():
            if isinstance(v, str) or isinstance(v, unicode):
                doc[k] = re.sub(_chars, repl, v)
            else:
                escape_chars(v)
    elif isinstance(doc, list):
        for d in doc:
            escape_chars(d)

    return doc
	
