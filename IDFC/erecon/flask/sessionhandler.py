#Taken from http://flask.pocoo.org/snippets/51/
from werkzeug.datastructures import CallbackDict
from flask.sessions import SessionInterface, SessionMixin
from itsdangerous import URLSafeTimedSerializer, BadSignature
import config
import redis
import random
import string
import flask
from flask import request
import logger
logger = logger.Logger.getInstance("application").getLogger()


class ItsdangerousSession(CallbackDict, SessionMixin):

    def __init__(self, initial=None):
        def on_update(self):
            self.modified = True
        CallbackDict.__init__(self, initial, on_update)
        self.modified = False


class ItsdangerousSessionInterface(SessionInterface):
    salt = config.sessionSalt
    session_class = ItsdangerousSession

    def get_serializer(self, app):
        if not app.secret_key:
            return None
        return URLSafeTimedSerializer(app.secret_key, salt=self.salt)

    def _getFingerPrint(self):
        #TODO: Print the IP and check if its proper when we are behind NGINX....
        logger.info(flask.request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
        return "_" + flask.request.environ.get('HTTP_X_REAL_IP', flask.request.remote_addr) + flask.request.headers.get('User-Agent')

    def open_session(self, app, request):
        s = self.get_serializer(app)
        if s is None:
            return None
        val = request.cookies.get(app.session_cookie_name)
        if not val:
            return self.session_class()

        try:
            data = s.loads(val, max_age=config.sessionTimeoutSecs)
            redisServer = redis.Redis()
            if 'sessionData' in data and data.get('uniqueSessionId', '')+self._getFingerPrint() == redisServer.hget('uniqueSessionId', data["sessionData"]["userName"]):
                return self.session_class(data)
            return self.session_class()
        except BadSignature:
            return self.session_class()

    def save_session(self, app, session, response):
        domain = self.get_cookie_domain(app)
        if not session:
            if session.modified:
                response.delete_cookie(app.session_cookie_name, domain=domain)
            return
        expires = self.get_expiration_time(app, session)
        tmpSession = dict(session)
        if (not 'uniqueSessionId' in tmpSession) and ('sessionData' in tmpSession):
            genId = "".join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
            tmpSession['uniqueSessionId'] = genId
            genId += self._getFingerPrint()
            redisServer = redis.Redis()
            redisServer.hset('uniqueSessionId',tmpSession["sessionData"]["userName"], genId)

        val = self.get_serializer(app).dumps(tmpSession)
        response.set_cookie(app.session_cookie_name, val, expires=expires, httponly=True, domain=domain)

