from flask_pymongo import MongoClient

con = MongoClient()
db = con['erecon']
collection = db['audit_users']
import utilities

# collection = db['users']


db.audit_users.remove({})
for i in db['users'].find({}):
    if 'role' in i.keys():
        db['audit_users'].save(i)

users_result = list(db['audit_users'].find({}))
db.users.remove({})
db.auditTrails.remove({})
for i in users_result:
    if 'role' in i.keys():
        i['isApproved'] = True
        if i['role'] not in ['admin', 'Approver']:
            i['approvalAction'] = 'create'
            i['createdBy'] = 'admin'
           # i['isApproved'] = True
        eventDoc = {
            'type': 'Requested for user creation',
            'actionOn': i['userName'],
            'createdBy': 'admin'
        }
        eventDoc["created"] = utilities.getUtcTime()
        eventDoc["updated"] = utilities.getUtcTime()

        if 'created' in i: eventDoc["created"] = i['created']
        if 'updated' in i: eventDoc["updated"] = i['updated']


        eventDoc["machineId"] = utilities.getMachineId()
        eventDoc["partnerId"] = "54619c820b1c8b1ff0166dfc"
        db.auditTrails.save(eventDoc)
	eventDoc1 = eventDoc
	eventDoc1['type'] = 'User Approved and Created Succesfully'
        db.auditTrails.save(eventDoc1)
        db.users.save(i)
    else:
        db.users.remove(i['_id'])

print 'Success'
print len(list(db.auditTrails.find({})))
