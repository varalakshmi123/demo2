from datetime import datetime, timedelta
import calendar
import time
import sys
import os
import config
import utilities
import httplib2
import json
import pymongo
import logging
#import application

#from logger import Logger
#logger = Logger.getInstance("application").getLogger()

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.mongodb import MongoDBJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor


def alarm(time,msg):
    print msg
    #usermanagement.SyncContact().import_contact()
    print('Alarm! This alarm was scheduled at %s.' % time)

def processBoounce(time):
    print(str(time)+'Bounce mail process at %s.' % datetime.now())
    headers   = {'content-type': 'application/json'}
    http = httplib2.Http(disable_ssl_certificate_validation=True)
    response, content = http.request(config.flaskContactApiUrl+"/no_auth/dummy/processBounce", 'POST', headers=headers, body=json.dumps({}))
    #(status, msg) = ses_sendmail.executeBounce()

def executeCampaign(userName, role, time, partnerId, camp_id, social_campaign):
    print('Campaign executed at %s .' % time)

    # if social_campaign:
    #     application.sendSocialCampaign(camp_id, partnerId)
    # else:
    #     application.sendNow(camp_id, partnerId)

    # headers   = {'content-type': 'application/json'}
    # loginData = {}
    # loginData['userName'] = userName
    # loginData['role'] = role
    # loginData['partnerId'] = partnerId
    # http = httplib2.Http(disable_ssl_certificate_validation=True)
    #
    # scheduleApiUrl = config.flaskApiUrl
    # if partnerId in config.largeCampaignPartnerIds:
    #     scheduleApiUrl = config.flaskPartnerApiUrl
    #
    # response, content = http.request(scheduleApiUrl, 'POST', headers=headers, body=json.dumps(loginData))
    #
    # headers['Cookie'] = response['set-cookie']
    #
    # data = {}
    # data['camp_id'] = camp_id
    # #data['more_emails'] = ['raju.20noi@gmail.com','rajub@gmail.com']
    # response, content = http.request(scheduleApiUrl+"/campaign/id/sendNow", 'POST', headers=headers, body=json.dumps(data))
    # #print response, content
    # if response.status == 200:
    #     content = json.loads(content)
    #     if content['status'] == 'ok':
    #         return True, content['msg']
    #     return False, content['msg']
    # return False, content

    #campaign.sendNow(partnerId, camp_id)

class JobScheduler:
    _instance = None

    @classmethod
    def getInstance(cls):
        print "inside getinstance"
        if not JobScheduler._instance:
            JobScheduler._instance = JobScheduler()
            logging.basicConfig()
            JobScheduler._instance.start_scheduler()
            print "inside create instance"
        return JobScheduler._instance

    def __init__(self):
        executors = {
            'default': ThreadPoolExecutor(config.jobThreadPool),
            'processpool': ProcessPoolExecutor(config.jobProcessPool)
        }
        job_defaults = {
            'coalesce': False,
            'max_instances': config.jobMaxInstance
        }

        if config.isReplicaSet:
            connection = pymongo.MongoReplicaSetClient(config.mongoHost, replicaSet=config.replicaSetName)
            #connection.read_preference = pymongo.ReadPreference.PRIMARY_PREFERRED
        else:
            connection = pymongo.MongoClient(config.mongoHost)

        #connection.write_concern = {'j': True}


        self.scheduler = BackgroundScheduler() #executors=executors, job_defaults=job_defaults
        self.scheduler.add_jobstore(MongoDBJobStore(config.databaseName,'job_store', connection))
        print "inside init"

    def schedule_cron_job(self):
        job = self.scheduler.add_job(alarm, 'cron', seconds=10, args=[datetime.now(),"hello"])
        print('Job schedule ID : '+job.id)

    def schedule_job(self, userName, role, date, partnerId, camp_id, social_campaign):
        #alarm_time = datetime.now() + timedelta(seconds=10)
        job = self.scheduler.add_job(executeCampaign, 'date', run_date=date, args=[userName, role, date, partnerId, camp_id, social_campaign], misfire_grace_time=config.jobMisfireGraceTime)
        print('Job schedule ID : '+job.id)
        return job.id

    def schedule_job_interval(self, interval_type, interval_time):
        if interval_type == 'minutes':
            job = self.scheduler.add_job(processBoounce, 'interval', minutes=interval_time, args=[datetime.now()])
        elif interval_type == 'hours':
            job = self.scheduler.add_job(processBoounce, 'interval', hours=interval_time, args=[datetime.now()])

        print('Job schedule ID : '+job.id)
        #self.delete_job(job.id)

        return job.id

    def cancel_job(self, id):
        try:
            self.scheduler.remove_job(id)
        except Exception, e:
            print "delete job exception ",e
            pass

    def start_scheduler(self):
        try:
           self.scheduler.start()
        except (KeyboardInterrupt, SystemExit):
            pass

    def shutdown(self):
        try:
            self.scheduler.shutdown()
        except (KeyboardInterrupt, SystemExit):
            pass

if __name__ == '__main__':

    #print "test"
    # application.sendSocialCampaign("573554969c9f1c477b64508c", "5723074b9c9f1c23dc6049dd")

    #exit(1)

    job_scheduler = JobScheduler()

    #executeCampaign(time, "54b64fb603c0171cea70bcc8", "54b8be9703c0170e5b709ca3")

    conf_date = datetime.strptime("2014-10-8 18:40:00", "%Y-%m-%d %H:%M:%S")
    #ts=int(datetime.now().replace(second=0).strftime("%s"))
    #utc_offset = datetime.fromtimestamp(ts) - datetime.utcfromtimestamp(ts)
    #converted = int(conf_date.strftime("%s"))-utc_offset.seconds
    converted = utilities.getUtcTime(conf_date)

    #print "converted", converted
    #print (time.strftime("%d/%m/%Y %H:%M:%S"))
    #print datetime.fromtimestamp(converted).strftime("%Y-%m-%d %H:%M:%S")
    #job_scheduler.schedule_job("1412747463000","aa")
    #job_scheduler.schedule_job("2014-10-9 17:15:00", "542b99d703c0170d6cadcda3", "5436280003c0171303e71708")

    job_scheduler.schedule_job_interval("minutes", 1)

    #job_scheduler.start_scheduler()

    # curr_time = utilities.getUtcTime()

    # print datetime.fromtimestamp((curr_time)).strftime("%Y-%m-%d %H:%M:%S")
    # print datetime.fromtimestamp((1421394661)).strftime("%Y-%m-%d %H:%M:%S")

    #(month_start_date, month_end_date) = utilities.getMonthFirstLastDate()

    #print "month_start_date, month_end_date", month_start_date, month_end_date

    #print utilities.getUtcTime(month_start_date), " ==== ", utilities.getUtcTime(month_end_date)

    start_date = utilities.getLastOneMonth()


    #print "last_month",start_date

    #print datetime.fromtimestamp(start_date).strftime("%Y-%m-%d %H:%M:%S")
