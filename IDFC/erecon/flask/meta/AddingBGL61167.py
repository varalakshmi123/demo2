
import pandas
from pymongo import MongoClient

mongo = MongoClient('localhost')
db = mongo.reconbuilder
coll = db.feedAccMapping

det = coll.find({'reconContext.reconId':'SUSP_CASH_APAC_61167'})
if det is None:
    pass
else:
    coll.remove({'reconContext.reconId':'SUSP_CASH_APAC_61167'})

accList = pandas.read_csv('/usr/local/nginx/www/erecon/flask/meta/ATMMaster.csv')
# print accList
doc = []
data = accList.to_dict(orient='record')
for i in data:
    dict  = {}
    dict['accountNames'] = i['LEDGER_NAME']
    dict['accountNumber'] = i['ACCOUNT_NUMBER']
    doc.append(dict)


total ={
	"feedDetails" : [
		{
			"feedId" : "5b0266a8e082850425c1ce81",
			"feedName" : "ATM Manage Suspense Accts Txns"
		},
		{
			"feedId" : "5b026773e082850425c1ce82",
			"feedName" : "ATM Manage Suspense Accts Bal"
		}
	],
	"reconContext" : {
		"reconId" : "SUSP_CASH_APAC_61167",
		"reconName" : "ATM Management BGL Suspense"
	},
	"partnerId" : "54619c820b1c8b1ff0166dfc"
}

total['accounts'] = doc

coll.insert(total)
