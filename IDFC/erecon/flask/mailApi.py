#!/usr/bin/env python

#Code taken from http://docs.python.org/2/library/email-examples.html

from flask import Flask

import os
import socket
#import network
import smtplib
import socket
import sys
import boto.ses
# For guessing MIME type based on file name extension
import mimetypes
import utilities
import config
import flask
import os
import json
import time
import re
#import thread

from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.audio import MIMEAudio
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import application
from logger import Logger
logger = Logger.getInstance("application").getLogger()
import sys
sys.path.append('.')

#app = Flask(__name__)

COMMASPACE = ','

def loadSettings(toUsersList,ccUserList):
    global fromUser,replyTo,username,password,server,port,connection,toUsers,ccUsers,settings,sendfromaws,sesAccountDetails
    #emailData = requestsInterface.action("settings","email_settings","get_settings")
    fromUser = str(config.fromUser)
    username = str(config.userName)
    password = str(config.password)
    #replyTo =  settings["fromEmail"]
    server = str(config.server)
    port = int(config.port)
    connection = config.connection
    #sendfromaws=settings["data"].get("sendfromaws",False)
    sendfromaws=config.sendfromaws
    #sesAccountDetails=settings["data"].get("sesAccountDetails",{})
    sesAccountDetails=config.sesAccountDetails
    #toUsers = ['replibittest@gmail.com']
    users = toUsersList.split(",")
    toUsers=[] 
    for user in users:
      toUsers.append(user)    
    ccUsers=[] 
    if ccUserList is not None:
        users = ccUserList.split(",")
        for user in users:
            ccUsers.append(user)
    return True

#status,resp = super(Reportsview, self).modify({"_id":{"$in":userIds}},{"billingProfile":billingProfileId},True)
def sendemail(toUsersList,subject, body,fromUserPass=None, attachFiles=None,ccUserList=None,hasImage =None):
    logger.info("-----------At send mail--------------")
    if not loadSettings(toUsersList,ccUserList):
         return False,"Email settings are not configured."   
    logger.info("ToUserList"+str(toUsers))
    logger.info("CcUserList"+str(ccUsers))
    fromUser = str(config.fromUser)
    if fromUserPass != None :
       fromUser = fromUserPass
    outer = MIMEMultipart()
    outer.set_charset("utf-8")
    outer['Subject'] = subject
    outer['To'] = COMMASPACE.join(toUsers)
    outer['From'] = fromUser
    #outer['Reply-to'] = replyTo
    #if ccUserList is not None:
        #outer['bcc']= COMMASPACE.join(ccUsers)
    try:
        outer.preamble = 'This mail contains attachment.\n'
        #part1 = MIMEText(body, 'plain')
        #part1 = MIMEText(body, 'html')
        #part1 = MIMEText(body.encode('ascii', 'ignore'), 'html')
        part1 = MIMEText(body.encode("utf-8", 'ignore'), 'html','utf-8')
        outer.attach(part1)
        if hasImage:
            img_data = open('Invoice/images/replibit-img.jpg', 'rb').read()
            img = MIMEImage(img_data, 'jpg')
            img.add_header('Content-Id', '<image1>')  
            outer.attach(img)
            img_data = open('Invoice/images/BMC-bg.png', 'rb').read()
            img = MIMEImage(img_data, 'png')
            img.add_header('Content-Id', '<image2>') 
            outer.attach(img)

        if attachFiles is not None:
            for filename in attachFiles:
                if not os.path.isfile(filename):
                    continue
            # Guess the content type based on the file's extension.  Encoding
            # will be ignored, although we should check for simple things like
            # gzip'd or compressed files.
                ctype, encoding = mimetypes.guess_type(filename)
                if ctype is None or encoding is not None:
                    # No guess could be made, or the file is encoded (compressed), so
                    # use a generic bag-of-bits type.
                    ctype = 'application/octet-stream'
                maintype, subtype = ctype.split('/', 1)
                if maintype == 'text':
                    fp = open(filename)
                    # Note: we should handle calculating the charset
                    msg = MIMEText(fp.read(), _subtype=subtype)
                    fp.close()
                elif maintype == 'image':
                    fp = open(filename, 'rb')
                    msg = MIMEImage(fp.read(), _subtype=subtype)
                    fp.close()
                elif maintype == 'audio':
                    fp = open(filename, 'rb')
                    msg = MIMEAudio(fp.read(), _subtype=subtype)
                    fp.close()
                else:
                    fp = open(filename, 'rb')
                    msg = MIMEBase(maintype, subtype)
                    msg.set_payload(fp.read())
                    fp.close()
                    # Encode the payload using Base64
                    encoders.encode_base64(msg)
                    # Set the filename parameter
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
                outer.attach(msg)
            # Now send or store the message
        if sendfromaws == True:
              #print "HHH"
              try:
                sesAccounts =None
                if sesAccounts is None and sesAccountDetails:
                   for sesAcc in sesAccountDetails:
                        sesAccounts = sesAccountDetails[sesAcc]
                        break
                if sesAccounts:
                    region = sesAccounts['region']
                    access_key = sesAccounts['access_key']
                    secret_key = sesAccounts['secret_key']
                else:
                    return False, "Failed to send mail"
               # print sesAccounts

                conn = boto.ses.connect_to_region(
                        region,
                        aws_access_key_id=access_key,
                        aws_secret_access_key=secret_key)
              except Exception, e:
                #print "EXCEPTION: %s " %e
                logger.info(e)
                return False, "Failed to connect to mail server"
                #conn.verify_email_address('rajubishnoi84@gmail.com')

                #print "params fromUser ",fromUser," subject ",subject,' touser ',toUsers
                #resp = conn.send_email( fromUser, subject, outer.as_string(), toUsers)

              if "_id" in sesAccounts and sesAccounts['_id'] in config.sesAccountDetails:
                if (time() - config.sesAccountDetails[sesAccounts['_id']]['time_started']) <= (1 / config.MAX_PER_SEC):  # Rate condition
                     time.sleep(1 / config.MAX_PER_SEC)
                config.sesAccountDetails[sesAccounts['_id']]['time_started'] = time()

              resp = conn.send_raw_email(outer.as_string(), fromUser, toUsers)
            #{u'SendRawEmailResponse': {u'SendRawEmailResult': {u'MessageId': u'000001491d346968-b58e69d5-687c-4d24-b595-a65f1367a83b-000000'}, u'ResponseMetadata': {u'RequestId': u'e2d4d9f8-55d6-11e4-b5e4-2fff8d14350a'}}}
              logger.info( "AWS Mail response " + str(resp))
              messageId = ''
              if "SendRawEmailResponse" in resp and "SendRawEmailResult" in resp["SendRawEmailResponse"] and "MessageId" in resp["SendRawEmailResponse"]["SendRawEmailResult"]:
                messageId = resp["SendRawEmailResponse"]["SendRawEmailResult"]["MessageId"]

              if messageId:
                return True, messageId
              else:
                return False, "Failed to sent mail"
        else:
            if connection == "SSL/TLS":
                 s = smtplib.SMTP_SSL(server, port, timeout=10)
            else:
                 s = smtplib.SMTP(server, port, timeout=10)
            logger.info("SMTP connection set")
            s.ehlo()
            if connection == "STARTTLS":
                 s.starttls()
                 s.ehlo()
                #logger.info(username)
                #logger.info(password)
            if len(username) > 0:
                s.login(username,password)
             #logger.info("Logined into server")
            s.sendmail(fromUser, toUsers+ccUsers, outer.as_string())
            logger.info("--------------Mail sent-----------")
            return True,"Mail Sent"
            s.close()
    except Exception, e:
        logger.info("Error "+str(e))
        return False,str(e)




#if __name__== '__main__':
 #print sendemail("harikrishna1432@gmail.com","hi", "cheking","changeassistdemo@gmail.com")

