import pandas as pd
from application import GSTSourceProductGroup, GstFeedDef
import json
from copy import deepcopy
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--OverWrite', help='OverWrite data if already exists')
args = parser.parse_args()

delta_sources = pd.read_csv('meta/GST_DELTA_Mapping.csv', dtype=str)
delta_sources = delta_sources.to_dict(orient='records')

with open('meta/gst_delta.json', 'r') as file:
    delta_structure = json.load(file)

print '======== Adding GST Delta Source ========'

for source in delta_sources:
    status, data = GSTSourceProductGroup().get(
        {"sourceCode": source['Source Code'], "sourceName": source['Source Name']})
    if args.OverWrite:
        GSTSourceProductGroup().delete({"sourceName": source['Source Name']})
        GstFeedDef().delete({"sourceCode": source['Source Code']})
    elif status:
        print 'Source name "%s" | source code "%s" already exists in source product group' % (
        source['Source Name'], source['Source Code'])
        continue

    status, data = GSTSourceProductGroup().create({
        "businessProcess": source["Business Group"],
        "sourceName": source['Source Name'],
        "productGroup": "CBS-Manual",
        "stateCodeRef": "General State Code",
        "matchingSource": "CBS_GLExtract",
        "sourceCode": source['Source Code'],
        "partnerId": "54619c820b1c8b1ff0166dfc"
    })

    print '%s added to source group' % source['Source Name'] if status else "error adding %s to source group" % \
                                                                            source['Source Name']

    tmp = deepcopy(delta_structure)
    tmp['feedName'] = source['Feed Name']
    tmp['sourceCode'] = source['Source Code']
    tmp['fileNamePattern'] = source['Feed Pattern']
    tmp["partnerId"] = "54619c820b1c8b1ff0166dfc"

    status, data = GstFeedDef().create(tmp)
    print 'Feed structure added for %s' % source['Source Name']

print '=========================================='
