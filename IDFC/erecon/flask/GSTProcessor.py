import pandas as pd
import numpy as np
import os
import fnmatch
import csv
from dateutil.parser import *
import json
from copy import deepcopy
import datetime

class GSTProcessor():
    def __init__(self,path,statementDate):
        self.basepath="/usr/local/nginx/www/erecon/flask/GSTReport/"
        self.filepath=self.basepath+path+"/"+statementDate+"/"
        self.outputPath="/usr/local/nginx/www/erecon/flask/GSTReport/" + statementDate + '/output'
        self.statementDate=statementDate
        self.outputdir = self.outputPath + self.statementDate + '/'
        if not os.path.exists(self.outputdir):
            os.makedirs(self.outputdir)
        self.sourceprefixes=["BSM","CMS","NOVOLOAN","RETAIL","TECH1","WBO","RSYS","MTF","INDUS","CBS","TECH","TIPLUS"]
        self.feedstruct={}
        self.loadFeedStructures()
        self.data={}
        self.ogldata=None
        self.branchcodes = pd.read_csv(self.basepath + "Reference/GeneralStateCode.csv")
	#print self.branchcodes['BRANCH']
	print self.basepath + "Reference/GeneralStateCode.csv"
	#exit(0)
        self.statecodes = self.branchcodes[["STATECODE", "STATENAME"]].drop_duplicates()
        self.sourceproductgroup = pd.read_csv(self.basepath + "Reference/SourceProductGroup.csv", delimiter='|')
        self.columnnamechanges = pd.read_csv(self.basepath + "Reference/MappedColumnlist.txt")
        print self.basepath + "Reference/MappedColumnlist.txt"
        print type(self.columnnamechanges)
        print self.columnnamechanges.columns
        print self.columnnamechanges.head()
        print self.columnnamechanges['SOURCE']

        self.sourceprefixes=self.columnnamechanges["SOURCE"].unique().tolist()
        self.finaldata={}
        self.totaldata=[]
        self.template={"STATE CODE": 0, "TAX": 0.00, "STATE NAME": "", "SOURCE": "", "TRANSACTION COUNT": 0, "STATEMENT DATE": statementDate}


    def loadFeedStructures(self):
        with open(self.basepath + 'Reference/ProductFeedStruct.txt', 'r') as csvfile:
            cfile=csv.reader(csvfile, delimiter='|')
            for line in cfile:
                #print line
                self.feedstruct[line[0]]=line[3:]

    def recursive_glob(self,rootdir='.', pattern='*'):
        """Search recursively for files matching a specified pattern.

        Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
        """
        matches = []
        for root, dirnames, filenames in os.walk(rootdir):
            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
        return matches

    #def loadManualFiles(self):


    def loadFiles(self):
        files = self.recursive_glob(self.filepath, "*.txt")
        for selectedfile in files:
            #print selectedfile
            if os.path.basename(selectedfile.lower()).startswith("egl_gl"):
                self.ogldata=pd.read_csv(selectedfile,delimiter='|')
                self.ogldata = self.ogldata.merge(self.branchcodes, how="left", on=["BRANCH"])
                print self.ogldata.columns
                accounts=pd.read_csv(self.basepath + "Reference/Natural Accounts OGL Filter.csv")
                values=accounts["NATURAL_AC"].unique()
                self.ogldata=self.ogldata[self.ogldata["NATURAL_AC"].isin(values)]
                source="OGL"
                columns = self.columnnamechanges[self.columnnamechanges["SOURCE"] == source]
                srccolumns = columns["RAWDATACOL"].tolist()
                mappedcols = columns["MAPPEDCOL"].tolist()
                renamedict = {}
                for i in range(0, len(srccolumns)):
                    renamedict[srccolumns[i]] = mappedcols[i]
                self.ogldata = self.ogldata[srccolumns]
                self.ogldata = self.ogldata.rename(columns=renamedict)
                continue
            source=None
            for x in self.sourceprefixes:
                if os.path.basename(selectedfile.lower()).startswith(x.lower()):
                    source=x
                    break
            #print source,selectedfile
            if source:
                df=pd.read_csv(selectedfile,names=self.feedstruct[source],delimiter='|',skiprows=1,skipfooter=1)

                df["SOURCE"]=source
                print(len(df["SOURCE"]))
                df = df.merge(self.sourceproductgroup, how="left", on=["SOURCE"])
                print(len(df["SOURCE"]))
                if "sourceState" in list(df.columns.values):
                    df=df.rename(columns={'sourceState': 'STATECODE'})
                    df = df.merge(self.statecodes, how="left", on=["STATECODE"])
                print(len(df["SOURCE"]))
                columns=self.columnnamechanges[self.columnnamechanges["SOURCE"]==source]
                srccolumns=columns["RAWDATACOL"].tolist()
                mappedcols=columns["MAPPEDCOL"].tolist()
                renamedict={}
                for i in range(0,len(srccolumns)):
                    renamedict[srccolumns[i]]=mappedcols[i]
                #for x in srccolumns:
                df=df[srccolumns]
                df=df.rename(columns=renamedict)
                df["STATEMENT DATE"]=df["STATEMENT DATE"].apply(lambda x:parse(str(x)).strftime("%Y%m%d"))
                #df["YEAR"]=df["STATEMENT DATE"].apply(lambda x:parse(str(x)).year)
                df=df[["STATE CODE","STATE NAME","STATEMENT DATE","CHARGE VALUE","TAX VALUE"]].groupby(["STATEMENT DATE","STATE CODE","STATE NAME"]).agg(['sum','count']).reset_index()
                print df.columns
                df["TAX"]=df["TAX VALUE"]['sum']
                df["CHARGE"] = df["CHARGE VALUE"]['sum']
                df["TRANSACTION COUNT"]=df["TAX VALUE"]['count']
                df["SOURCE"]=source
                df=df[["STATE CODE","STATE NAME","STATEMENT DATE","TAX","CHARGE","TRANSACTION COUNT","SOURCE"]]
                df['TAX'] = df['TAX'].apply(lambda x:round(x,2))
                df['CHARGE'] = df['CHARGE'].apply(lambda x:round(x,2))
                df.columns = df.columns.droplevel(level=1)
                data=json.loads(df.to_json(orient='records'))
                for x in data:
                    if x['STATE NAME'] not in self.finaldata:
                        self.finaldata[x['STATE NAME']]=[]
                    self.finaldata[x['STATE NAME']].append(x)
                #df=df["".groupby(["STATECODE"])
                #self.data[source] = df
                #print source
                #print df.to_csv(self.outputdir+source+".csv")
                #print "_________________________________________________________________"

        #for key,val in self.data.items():
        #    print "GSTMama",key, val.head(5)


    def mapState(self):
        self.ogldata=self.ogldata[["SOURCECODE","STATE CODE","STATE NAME","TAX VALUE","STATEMENT DATE"]]
        self.ogldata =self.ogldata.merge(self.sourceproductgroup,how="left",on="SOURCECODE")
        print self.ogldata.columns
        self.ogldata=self.ogldata.rename(columns={"SOURCE":"MATCHSOURCE"})
        self.ogldata["STATEMENT DATE"] = self.ogldata["STATEMENT DATE"].apply(lambda x: parse(str(x)).strftime("%Y%m%d"))
        self.ogldata=self.ogldata.groupby(["MATCHSOURCE","STATE CODE","STATE NAME","STATEMENT DATE"]).agg(['sum','count']).reset_index()
        #print self.ogldata.columns
        self.ogldata["TAX"] = self.ogldata["TAX VALUE"]['sum']
        self.ogldata["TRANSACTION COUNT"] = self.ogldata["TAX VALUE"]['count']
        self.ogldata["SOURCE"] = "OGL"
        self.ogldata=self.ogldata[["MATCHSOURCE","STATE CODE","STATE NAME","TAX","TRANSACTION COUNT","SOURCE","STATEMENT DATE"]]
        self.ogldata["TAX"] = self.ogldata["TAX"].apply(lambda x:round(x,2))
        self.ogldata.columns = self.ogldata.columns.droplevel(level=1)
        data = json.loads(self.ogldata.to_json(orient='records'))
        for x in data:
            if x['STATE NAME'] not in self.finaldata:
                self.finaldata[x['STATE NAME']] = []
            self.finaldata[x['STATE NAME']].append(x)

        for x in self.finaldata.keys():
            autogen=deepcopy(self.sourceprefixes)
            for val in self.finaldata[x]:
                if val["SOURCE"] in autogen:
                    autogen.remove(val["SOURCE"])
                    self.totaldata.append(val)
            for x in autogen:
                data=deepcopy(self.template)
                data["SOURCE"]=x
                data["STATE CODE"]=val["STATE CODE"]
                data["STATE NAME"] = val["STATE NAME"]
                self.totaldata.append(data)

        #update mongodb with this
        x={"statementdate":self.statementDate,"data":self.totaldata,"year":int(self.statementDate[0:4])}
        print json.dumps(x)

    def filterData(self,allowedSources,data):
        filtereddata=[]
        for x in data:
            if x["SOURCE"]=="OGL":
                if x["MATCHSOURCE"] in allowedSources:
                    filtereddata.append(x)
            elif x["SOURCE"] in allowedSources:
                filtereddata.append(x)
        return filtereddata

    def getYTD(self,allowedSources):
        now = datetime.datetime.now()
        #get all records for this year {"YEAR":now.year,"SOURCE":{$in:allowedSources}}
        records=[]
        allrecords=[]
        for x in records:
            allrecords.extend(x["data"])
        json.dumps(allrecords)
        df=pd.read_json(json.dumps(allrecords),orient="records")
        df=df["STATE NAME","TAX","SOURCE","TRANSACTION COUNT"].groupby(["STATE NAME","SOURCE"]).sum().reset_index()
        return json.loads(df.to_json(orient='records'))



if __name__ == "__main__":
    g = GSTProcessor("", "20171128")
    g.loadFiles()
    g.mapState()
    print g.filterData(["CBS"], g.totaldata)
    # print g.ogldata.head(20)







