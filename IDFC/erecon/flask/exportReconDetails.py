import os
import sys
from datetime import datetime

import cx_Oracle
import pandas as pd
from bson import ObjectId
from pymongo import MongoClient
import sql_config as config


def exportReconMigration(reconId):
    ip, port, SID, userName, pwd = config.ip, config.port, config.SID, config.connection_name, config.connection_pwd
    exportFolder = config.migrationFolder
    mongoPath = config.mongoPath
    reconId = str(reconId)
    col_name = 'recon_context_details'
    # UAT Mongo
    lmongo_client = MongoClient('mongodb://localhost:27017/')
    lmongo_db = lmongo_client.reconbuilder
    lcc_col_recon = lmongo_db[col_name]
    print 'Started Recon Migration for reconId: ', reconId
    backupDT = datetime.now().strftime("%d%b%Y%H%M")
    for record in lcc_col_recon.find():
        if 'reconId' in record and record['reconId'] == str(reconId):
            folderName = exportFolder + reconId + os.sep
            # if not os.path.exists(folderName):
            #     os.mkdir(folderName)
            folderName = exportFolder + reconId + '_' + backupDT + os.sep
            if not os.path.exists(folderName):
                os.mkdir(folderName)
            q = mongoPath + 'mongoexport -d reconbuilder -c  recon_context_details -q "{ reconId: ' + repr(
                str(reconId)) + ' }" --out ' + folderName + reconId + '.json'
            os.system(q)
            print '|Saved Recon Matching Rules: ', record['reconName']

            # Save Feed Defination
            for feed in record['sources']:
                for i in lmongo_db.feedDefination.find({'_id': ObjectId(feed['feedId'])}):
                    q = mongoPath + 'mongoexport -d reconbuilder -c  feedDefination -q \'{ _id: ObjectId("' + (
                        feed['feedId']) + '") }\' --out ' + folderName + str(feed['feedId']) + '.json'
                    os.system(q)
                    print q
                    print '|Saved Feed Details: ', i['feedName']

            for accMap in lmongo_db.feedAccMapping.find({'reconContext.reconId': reconId}):
                q = mongoPath + 'mongoexport -d reconbuilder -c  feedAccMapping -q "{' + repr(
                    'reconContext.reconId') + ': ' + repr(str(reconId)) + ' }" --out ' + folderName + str(reconId) + '_accMapping' + '.json'
                os.system(q)
                print '| Saved Account Mapping Details: '

            for accMap in lmongo_db.feedAccMapping.find({'reconId': reconId}):
                print '| Saved PreMatching Details: '
                q = mongoPath + 'mongoexport -d reconbuilder -c  feedAccMapping -q "{ reconId: ' + repr(
                    reconId) + ' }" --out ' + folderName + str(reconId) + '_prematch.json'
                os.system(q)

            for accMap in lmongo_db.postmatchscripts.find({'reconId': reconId}):
                q = mongoPath + 'mongoexport -d reconbuilder -c  postmatchscripts -q "{ reconId: ' + repr(
                    reconId) + ' }" --out ' + folderName + str(reconId) + '_postmatch.json'
                os.system(q)
                print '| Saved PostMatching Details: '
            print 'Please Find the files in folder :', folderName

            # export recon_dynamic_data_model
            dsn_tns = cx_Oracle.makedsn(ip, port, SID)
            connection = cx_Oracle.connect(userName, pwd, dsn_tns)
            qry = "select * from recon_dynamic_data_model where recon_id = '{}' " \
                  "and record_status = 'ACTIVE' order by order_seq_no".format(reconId)
            df = pd.read_sql(qry, connection)
            df.to_csv(folderName + 'recon_dynamic_data_model.csv', index=False)
            return True, reconId + '_' + backupDT
            break
    return False, ''


if __name__ == '__main__':
    reconName = 'EPYMTS_CASH_APAC_20225'
    exportReconMigration(sys.argv[1])
