import logger
import subprocess
import smtplib
import noauthhandler

logger = logger.Logger.getInstance("application").getLogger()
import dbinterface
import traceback
import re
import flask
from flask import request
import pandas
import cx_Oracle
import numpy as np
import pymongo
import config
import hashlib
import shutil
import os
import utilities
import rstr
from bson import json_util
import time
import datetime
from flask import request
from datetime import timedelta, date
from bson.objectid import ObjectId
import json
import sql
import uuid
import csv
import datatypes
import time
from dateutil.parser import parse
from pymongo import MongoClient
from reconBuilder import ReconBuilder
# from engine import scripts
import glob
# from jinja2 import Environment, FileSystemLoader
# from weasyprint import HTML
import time
import paramiko
import exportReconDetails
from email.mime.multipart import MIMEMultipart
from email.MIMEText import MIMEText
# import importReconDetails
# import recon_deployer
import sql_config
from xlrd import open_workbook
from copy import deepcopy
import fnmatch
import shutil
from bson.objectid import ObjectId
from bson.son import SON
from ADF_OGL_Report import AdfOglReport


def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)


def getPartnerFilePath():
    return os.path.join(config.commonFilesPath, "theme_templates") + "/"


class Business(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Business, self).__init__("business", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(Business, self).create(doc)
        return status, businessDoc


class UsersGraph(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UsersGraph, self).__init__("usersgraph", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(UsersGraph, self).create(doc)
        return status, businessDoc

class UsersColl(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UsersColl, self).__init__("users", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": a'i'}}):
        #     return False, "Business already exists."
        (status, users) = super(UsersColl, self).create(doc)
        return status, users


class FileUpload(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(FileUpload, self).__init__("fileupload", hideFields)

    def create(self, doc):
        if self.exists({'fileName': {"$regex": "^" + doc['fileName'] + "$", "$options": 'i'}}):
            return False, "FileName already exists."
        (status, businessDoc) = super(FileUpload, self).create(doc)
        return status, businessDoc


class JobStatus(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(JobStatus, self).__init__("jobstatus", hideFields)


class ReconAudit(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAudit, self).__init__("reconaudit", hideFields)


class ReconLicense(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconLicense, self).__init__("reconlicense", hideFields)

    def getAll(self, query={}):
        stats, resp = super(ReconLicense, self).getAll(query)
        df = pandas.DataFrame(resp['data'])
        stats, jobStatusObj = JobStatus().getAll({})
        jStatusdf = pandas.DataFrame(jobStatusObj['data'])
        # print jStatusdf
        if len(jStatusdf) and len(df):
            jStatusdf = jStatusdf[jStatusdf['reconId'].isin(df['reconId'].unique())]
            jStatusdf.sort(columns=['updated'], inplace=True)
            jStatusdf.drop_duplicates(subset=['reconId'], keep='last')
            jStatusdf = jStatusdf[['reconId', 'executionDT', 'jobStatus', 'ExecutedBy', 'log_path']]
            if 'jobStatus' in df.columns: df.drop(['jobStatus'], axis=1, inplace=True)
            if 'executionDT' in df.columns: df.drop(['executionDT'], axis=1, inplace=True)
            if 'log_path' in df.columns: df.drop(['log_path'], axis=1, inplace=True)
            jStatusdf.drop_duplicates(subset=['reconId'], inplace=True)
            logger.info(len(df))
            response = pandas.merge(df, jStatusdf, on=['reconId'], how='left')
            logger.info(len(response))
            response = response.where((pandas.notnull(response)), '')
            resp['data'] = response.T.to_dict().values()
        return True, resp

    def getReconLicense(self, id):
        (status_builder, data_builder) = ReconBuilder().getReconContextDetails('dummy')
        (status_license, licenseData) = super(ReconLicense, self).getAll(query={})
        logger.info(licenseData)
        if licenseData['total'] == 0:
            for val in data_builder:
                (status, reconLicenseDoc) = super(ReconLicense, self).create(val)
        if len(data_builder) != licenseData['total']:
            existingData = []
            for y in licenseData['data']:
                existingData.append(y['reconId'])
            for x in data_builder:
                if x['reconId'] in existingData:
                    pass
                else:
                    # x['licensed'] = 'Non-Licensed'
                    (status, reconLicenseDoc) = super(ReconLicense, self).create(x)
        return True, {}

    def runRecon(self, id, query):
        jobStatus = {}
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sql_config.engineIp, username=sql_config.engineUserName, password=sql_config.enginePassword)
        # (stdin, stdout, stderr) = ssh.exec_command(
        #        'sudo python /usr/share/nginx/erecon/engine/scripts/JobExecutor.py {} {}'.
        #            format(query['recon_id'], query['statementDate']))
        if JobStatus().exists({'reconId': query['recon_id']}):
            status, reconObj = JobStatus().get({'reconId': query['recon_id']})
            reconObj['jobStatus'] = 'Job Running'
            reconObj['executionDT'] = int(time.time())
            st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)

        runcmd = 'sudo python ' + str(sql_config.enginePath) + os.sep + 'scripts' + os.sep + 'JobExecutor.py' + ' ' + \
                 query[
                     'recon_id'] + ' ' + query['statementDate']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        logger.info('Recon Job Initiated')
        # update recon job status
        # check for job success statusi
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)
        # check for job success statusi
        # log_entries = list(stdout)
        # for val in log_entries:
        #    logger.info(val)
        logger.info(log_entries[len(log_entries) - 2])
        logger.info('cmaldkcmcda')
        if 'SUCCESS' in log_entries[len(log_entries) - 2]:
            jobStatus['status'] = 'SUCCESS'
            reconObj['jobStatus'] = 'SUCCESS'
            reconObj['log_path'] = ''

        else:
            jobStatus['status'] = 'FAILURE'
            reconObj['jobStatus'] = 'FAILURE'
            log_path = query['recon_id'] + '/failure_logs_' + datetime.datetime.now().strftime(
                '%d%m%Y%I%M%S') + '.txt'

            if not os.path.exists(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id'])):
                os.makedirs(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id']))

            files_list = os.listdir(os.path.join(config.contentDir, "engine_error_logs/" + query['recon_id']))
            if len(files_list) == 3:
                for file in files_list:
                    os.remove(os.path.join(config.contentDir, "engine_error_logs/{}/".format(query['recon_id'])) + file)

            with open(os.path.join(config.contentDir, "engine_error_logs/") + log_path, 'w') as log_file:
                for entry in log_entries:
                    log_file.write(entry)
            jobStatus['log_path'] = log_path
            reconObj['log_path'] = log_path
        st, reconObj = JobStatus().modify(reconObj['_id'], reconObj)
        # print reconObj
        ssh.close()
        return True, jobStatus

    def exportReconDetails(self, id, query):
        status, exportPath = exportReconDetails.exportReconMigration(query['reconId'])
        tarFileName = query['reconId'] + '_' + datetime.datetime.now().strftime('%d%b%Y%H%M') + '.tar.gz'
        if status:
            cmd = 'tar -czf ' + tarFileName + ' ' + exportPath
            os.system(cmd)
            os.system('sudo rm -r ' + config.flaskPath + exportPath)
            cmd = 'cp ' + tarFileName + ' ' + config.contentDir + os.sep
            os.system(cmd)
            os.system('sudo rm -r ' + config.flaskPath + tarFileName)
        return status, tarFileName

    def getMigrationList(self, id, query):
        files = {}
        # print query
        path = config.reconMigrationFolder
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sql_config.engineIp, username=sql_config.engineUserName, password=sql_config.enginePassword)
        runcmd = 'sudo python ' + sql_config.reconMigrationFolder + 'getFiles.py ' + query['search']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stdout)
        f = []
        # print repr(log_entries[0])
        if str(log_entries[0]) == '[]' or str(log_entries[0]) == '[]\n':
            return True, files

        for val in log_entries[0].split('[')[1].split(']')[0].split(','):
            reconId = val.strip().replace("'", '').rsplit('_', 1)[0]
            arryName = val.strip().replace("'", '').rsplit('_', 1)[1]
            ts = arryName.replace('.tar.gz', '')
            if reconId not in f:
                f.append(reconId)
                files[reconId] = {}
                files[reconId]['fileTS'] = [ts]
                files[reconId]['fileNames'] = [val.strip().replace("'", '')]
            else:

                files[reconId]['fileTS'].append(ts)
                files[reconId]['fileNames'].append(val.strip().replace("'", ''))
        # exit(0)

        return True, files

    def importReconDetails(self, id, query):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sql_config.engineIp, username=sql_config.engineUserName, password=sql_config.enginePassword)
        reconId = str(query['reconId'])
        runcmd = 'sudo python ' + sql_config.reconMigrationFolder + 'importReconDetails.py ' + query['reconId'] + ' ' + \
                 query['fileName']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)

        log_entries = list(stdout)
        if log_entries[len(log_entries) - 1] == 'Recon Successfully Imported\n':
            return True, 'Recon Successfully Imported'
        for val in log_entries:
            logger.info(val)

        # runcmd = 'sudo python ' + sql_config.reconMigrationFolder + 'recon_deployer.py ' + query['reconId'] + ' ' + \
        #          query['fileName']
        # stdin, stdout, stderr = ssh.exec_command(runcmd)
        # print runcmd
        # log_entries = list(stdout)
        # for val in log_entries:
        #     print val
        # log_entries = list(stderr)
        # for val in log_entries:
        #     print val

        # reconJob = recon_deployer.ReconMigration(reconId, query['fileName'])

        auditObj = {
            'importBy': flask.session['sessionData']['userName'],
            'importDetails': query,
            'reconId': query['reconId'],
            'operation': 'implement'
        }
        status, auditObj = ReconAudit().create(auditObj)

        return True, ''

    def rollBackReconDetails(self, id, query):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sql_config.engineIp, username=sql_config.engineUserName, password=sql_config.enginePassword)
        reconId = str(query['reconId'])
        # if '#V0.tar.gz' in
        if query['rollBkStatus']:
            runcmd = 'sudo python ' + sql_config.reconMigrationFolder + 'backups/' + 'importReconDetails.py ' + query[
                'reconId'] + ' ' + \
                     query['fileName']
        else:
            runcmd = 'sudo python ' + sql_config.reconMigrationFolder + 'rollback.py ' + query[
                'reconId']
        stdin, stdout, stderr = ssh.exec_command(runcmd)
        log_entries = list(stderr)
        for val in log_entries:
            logger.info(val)

        log_entries = list(stdout)
        for val in log_entries:
            logger.info(val)

        auditObj = {
            'importBy': flask.session['sessionData']['userName'],
            'importDetails': query,
            'reconId': query['reconId'], 'operation': 'rollback'
        }
        status, auditObj = ReconAudit().create(auditObj)

        return True, ''

    def getJobSummary(self, id, query):
        status_job, jobData = ReconBuilder().getJobSummaryReconBuilder(id='dummy', query=query)
        resp = {}
        resp["total"] = len(jobData)
        resp["current"] = len(jobData)
        resp["data"] = list(jobData)
        return status_job, resp


class AccountPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(AccountPool, self).__init__("accountpool", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(AccountPool, self).create(doc)
        return status, businessDoc

    def getAccountPoolDetails(self, id):
        (status, recon_con) = sql.getConnection()
        if status:
            (status_ac, ac_data) = sql.getAcPools({}, connection=recon_con)
            return ac_data.to_json()


class ForgotPassword(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ForgotPassword, self).__init__("users", hideFields)

    def create(self, doc):
        (status, EmailId) = super(ForgotPassword, self).create(doc)
        return status, EmailId


class customReportGenerate(dbinterface.MongoCollection):

    def __init__(self, hideFields=True):
        super(customReportGenerate, self).__init__("customReportDetails", hideFields)

    def create(self, doc):
        (status, details) = super(customReportGenerate, self).create(doc)
        return status, details


class CBS_OGLreport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(CBS_OGLreport, self).__init__("cbsOglReport", hideFields)

    def create(self, doc):
        (status, doc1) = super(CBS_OGLreport, self).create(doc)
        return status, doc1


class NoAuth(noauthhandler.noAuthHandler):
    def __init__(self):
        pass

    def ForgotPasswordService(self, id, userName):
        data = ForgotPassword().find_one({"userName": userName['userName']})
        if data == None:
            return True, 'Invalid UserName'
        Num = rstr.xeger(r'[A-Z][a-z]{2}[@#$][0-9]{4}')
        email = data['email']
        logger.info(email)
        password = Num
        if email is None or email == 'NO':
            self.logger.info('Sending Job Summary Email switched off')
            return True, 'Job Summary Email switched off'
        try:
            recipients = data['email']
            body = "UserName : " + data['userName'] + "Kindly Use the following PASSWORD to login" + " : " + str(
                password)
            status = self.sendMailTo('PASSWORD CHANGE', recipients, body)
            if not status:
                return True, 'Error while sending Mail'
        except Exception, e:
            print 'Error while sending Email'
            logger.info(e)
            return True, 'Error while sending Mail'

        ForgotPassword().update({"userName": userName['userName']},
                                {'$set': {'password': hashlib.md5(str(Num)).hexdigest()}})
        return True, "Success"

    def sendMailTo(self, subject, recipients, body):
        try:
            smtp = smtplib.SMTP('smtp.idfcbank.com')
            sender = 'erecon.alerts@idfcbank.com'
            msg = MIMEMultipart()
            msg["From"] = sender
            msg['To'] = recipients
            msg["Subject"] = subject
            msg.attach(MIMEText(body, 'Plain'))
            smtp.sendmail(sender, recipients, msg.as_string())
            smtp.close()
            return True
        except Exception as e:
	    logger.info(traceback.format_exc())
            return False

    def upload(self, id):
        contentDir = os.path.join(config.uploadDir)
        fname = request.files['file'].filename
        if (len(fname.split('.')) > 2):
            return False, "File Extension is Not supported"
        elif fname.split('.')[-1] not in ['txt', 'csv', 'xls', 'xlsx', 'pdf', 'eml', 'doc', 'jpeg', 'msg']:
            return False, "FileName is Not supported"
        else:
            logger.info(fname)

        if os.path.exists(contentDir):
            shutil.rmtree(contentDir)

        if not os.path.exists(contentDir):
            os.makedirs(contentDir)
        request.files['file'].save(os.path.join(contentDir, fname))
        # df = pandas.read_csv(contentDir + '/' + fname)
        # jsonname=os.path.splitext(fname)[0]
        # jsonname=jsonname+'.json'
        # df.to_json(os.path.join(contentDir, jsonname ),orient='records')
        return True, "File Uploaded SuccessFully"


class GstSources(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstSources, self).__init__("gstSources", hideFields)


class MhadaReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(MhadaReport, self).__init__("mhada_report", hideFields)

    def create(self, doc):
        (status, mhadaDoc) = super(MhadaReport, self).create(doc)
        return status, mhadaDoc


class ClosingAccNum(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ClosingAccNum, self).__init__("ClosingAcc", hideFields)

    def create(self, doc):
        (status, ClosingAcc) = super(ClosingAccNum, self).create(doc)
        return status, ClosingAcc


class ClosingBalance(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ClosingBalance, self).__init__("closing_balance_data", hideFields)

    def create(self, doc):
        (status, closingBalanceData) = super(ClosingBalance, self).create(doc)
        return status, closingBalanceData


class NostroReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NostroReport, self).__init__("nostro_reports", hideFields)

    def create(self, doc):
        (status, nostroReport) = super(NostroReport, self).create(doc)
        return status, nostroReport

    def formatStringToFloat(self, x):
        if pandas.isnull(x):
            # print 'x', x
            return np.nan
        elif isinstance(x, float) or isinstance(x, int):
            return x

        else:
            x = re.sub('[^0-9\.\-]', '', x)
            r = "".join(re.split('\,', x))
            val = str(r).strip()
            # print  val
            if len(val) > 0:
                return np.float64(val)
            else:
                return np.nan

    def getPreviousExecutionId(self, reconId):
        mongo = MongoClient('localhost')
        db = mongo.reconbuilder_new
        doc = db['recon_execution_details_log'].find({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                                      'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                      'RECORD_STATUS': 'ACTIVE'
                                                      }).sort([('RECON_EXECUTION_ID', -1)])

        doc = list(doc)
        if doc is not None and len(doc):
            doc = doc[0]
        if len(doc) == 0:
            doc = None
        return doc


class TodayNostroReport(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(TodayNostroReport, self).__init__("nostro_closing_balance", hideFields)

    def create(self, doc):
        (status, nostroClosingBalance) = super(TodayNostroReport, self).create(doc)
        return status, nostroClosingBalance


class GstFeedDef(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstFeedDef, self).__init__("gstFeedDef", hideFields)

    def create(self, doc):
        (status, gstFeedDef) = super(GstFeedDef, self).create(doc)
        return status, gstFeedDef

    def exportFieldDetails(self, id, feedRef):
        feed_ref = feedRef['feedRef'] if 'feedRef' in feedRef and feedRef['feedRef'] else 'feed_ref_mapping'
        status, data = GstFeedDef().get({'feedName': feed_ref})

        field_details = pandas.DataFrame(
            columns=['Position', 'Field Name', 'Mapped Column', 'Data Type', 'Date Pattern'])

        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

        if status and 'fieldDetails' in data.keys():
            _field_def = pandas.DataFrame(data['fieldDetails'])
            field_details = _field_def[['fieldName', 'mappedCol', 'dataType', 'datePattern']]
            field_details['position'] = _field_def.get('position', '')
            field_details = field_details[['position', 'fieldName', 'mappedCol', 'dataType', 'datePattern']]
            field_details.rename(
                columns={'fieldName': 'Field Name', 'dataType': 'Data Type', 'datePattern': 'Date Pattern',
                         'mappedCol': 'Mapped Column', 'position': "Position"}, inplace=True)
        else:
            pass

        field_details.to_csv(os.path.join(config.contentDir, "recon_reports/" + object_id + '/feed_details.csv'),
                             index=False)
        return True, {"exportFile": object_id + '/feed_details.csv'}

    def findFeedDetails(self, id, sourceCode):
        cursor = GstFeedDef().find(sourceCode)
        return True, dict(total=cursor.count(False), current=cursor.count(True), data=list(cursor))

    def getStateCodeCsv(self, id, query):
        logger.info(query)
        status, dist = GstMapData().getAll({'perColConditions': query})
        logger.info()
        return True, dist


class GstStateCodeMap(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstStateCodeMap, self).__init__("gstStateCodes", hideFields)

    # retrive distinct statecode
    def getDistinctStateCode(self, id):
        dist = GstStateCodeMap().distinct('refIdentifier')
        logger.info(dist)
        return True, dist

    def exportStateCodes(self, id, stateRef):
        state_ref = stateRef['stateRef'] if 'stateRef' in stateRef and stateRef['stateRef'] else 'state_code_mapping'
        status, data = GstStateCodeMap().getAll(
            {"condition": {'refIdentifier': state_ref if state_ref else state_ref}})
        stateCodes = pandas.DataFrame(columns=['Branch Name', 'Branch Code', 'State Name', 'State Code'])

        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

        if status and data['total'] > 0:
            stateCodes = data['data']
            stateCodes = pandas.DataFrame(stateCodes)[['branchName', 'branchCode', 'stateName', 'stateCode']]
            stateCodes.rename(
                columns={'branchName': 'Branch Name', 'branchCode': "Branch Code", 'stateName': "State Name",
                         'stateCode': "state Code"}, inplace=True)

        else:
            pass
        stateCodes.to_csv(os.path.join(config.contentDir, "recon_reports/" + object_id + '/' + state_ref + '.csv'),
                          index=False)
        return True, {"exportFile": object_id + '/' + state_ref + '.csv'}


# ADF V/S OGL Report collection
class ADF_OGL_Report(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ADF_OGL_Report, self).__init__("adfOglReports", hideFields)

    def create(self, doc):
        (status, doc) = super(ADF_OGL_Report, self).create(doc)
        return status, doc


class UserPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UserPool, self).__init__("userpool", hideFields)

    def create(self, doc):
        print doc
        if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
            return False, "UserPool already exists."
        (status, userpoolDoc) = super(UserPool, self).create(doc)
        return status, userpoolDoc

    # def deleteData(self,condition):
    #     logger.info(condition)
    #     (status,found) = UserPool(hideFields=False).get({'_id':condition})
    #     eventDoc = {'type':'userpool deletion','actionOn':found['name'],'createdDate':utilities.getUtcTime(),'createdBy':flask.session["sessionData"]['userName']}
    #     Audits().create(eventDoc)
    #     return super(UserPool, self).delete(condition)

    def updatePool(self, condition):
        logger.info('@' * 200)
        logger.info(condition)
        (status, found) = UserPool(hideFields=False).get({'_id': condition})
        logger.info(found)
        eventDoc = {'type': 'userpool updation', 'actionOn': found['name'], 'createdDate': utilities.getUtcTime(),
                    'createdBy': flask.session["sessionData"]['userName']}
        return Audits().create(eventDoc)
        # return Audits().create(eventDoc)

    def addUsers(self, id, query):
        logger.info(query)
        users = []
        if query is not None:
            for r in query['selectedUsers']:
                users.append(r['name'])
            for i in query['selectedUserPool_ids']:
                (status, userpoolData) = self.get({'_id': i})
                userpool_users = userpoolData['users'].split(',')
                usersArray = users + userpool_users
                logger.info(usersArray)
        return True, {}


class Utilities(object):
    def upload(self, id):
        data = dict(request.args)
        contentDir = os.path.join(config.contentDir)
        if not os.path.exists(contentDir):
            os.makedirs(contentDir)
        type = id.split('.')
        doc = {}
        doc['fileName'] = data['fileName'][0]
        logger.info('application' in request.files['file'].content_type)
        if 'application' not in request.files['file'].content_type:
            if (len(request.files['file'].filename.split('.')) > 2):
                logger.info('hey here')
                return False, "Upload Only txt,csv,xls,xlsx,pdf,doc,jpeg,msg Files"
            elif request.files['file'].filename.split('.')[-1].lower() not in ['txt', 'csv', 'xls', 'xlsx', 'pdf',
                                                                               'eml', 'doc', 'jpeg', 'msg']:
                logger.info(request.files['file'].filename)
                return False, "Upload Only txt,csv,xls,xlsx,pdf,doc,jpeg,msg Files"
            else:
                logger.info(doc['fileName'])

            doc['type'] = type[1].lower()
            doc['recon'] = data['recon'][0]
            doc['raw_link'] = data['raw_link'][0]
            if 'description' in data and data['description'][0] is not None:
                doc['description'] = data['description'][0]
            doc['uploadedBy'] = flask.session['sessionData']['userName']
            (status, data) = FileUpload().create(doc)
            if status:
                request.files['file'].save(os.path.join(contentDir, id))
                # request.files['sendfile'].save('/tmp/'+dummy)
                return True, id
            else:
                return False, data
        else:
            return False, 'Please Upload Proper File '

    def upload_gst_field_details(self, id):
        contentDir = os.path.join(config.contentDir)
        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(contentDir + os.sep + object_id):
            os.makedirs(contentDir + os.sep + object_id)

        supported_types = ['text/csv', 'application/csv', 'application/vnd.ms-excel']
        if request.files['file'].content_type not in supported_types:
            return False, "Only csv file uploads are allowed."

        file = request.files['file']
        upload_file = contentDir + os.sep + object_id + os.sep + file.filename
        file.save(upload_file)

        logger.info("File Uploaded : %s" % os.path.join(contentDir, object_id))
        df = pandas.read_csv(upload_file, dtype=str).fillna('')

        df.rename(columns={'Field Name': 'fieldName', 'Data Type': 'dataType', 'Position': 'position',
                           'Date Pattern': 'datePattern', 'Mapped Column': 'mappedCol'}, inplace=True)

        os.remove(upload_file)
        return True, df.to_dict(orient='records')

    def gst_state_code_upload(self, id):
        contentDir = os.path.join(config.contentDir)
        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(contentDir + os.sep + object_id):
            os.makedirs(contentDir + os.sep + object_id)

        supported_types = ['text/csv', 'application/csv', 'application/vnd.ms-excel']
        if request.files['file'].content_type not in supported_types:
            return False, "Only csv file uploads are allowed."

        file = request.files['file']
        upload_file = contentDir + os.sep + object_id + os.sep + file.filename
        file.save(upload_file)

        df = pandas.read_csv(upload_file, header=None, skiprows=1, dtype=str,
                             names=['branchName', 'branchCode', 'stateName', 'stateCode'])

        data = dict(request.args)
        df['refIdentifier'] = data['fileName'][0]
        df['partnerId'] = '54619c820b1c8b1ff0166dfc'
        df["created"] = utilities.getUtcTime()

        GstStateCodeMap().delete({'refIdentifier': data['fileName'][0]})
        GstStateCodeMap().insert(df.to_dict(orient='records'))
        return True, {}


class MasterDump(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(MasterDump, self).__init__("master_dump")


    def masterDownload(self, id, query):
        details = MasterDump().find_one({'reportName': query['reportName']})
        df = pandas.DataFrame((details['masterdata']))

        df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
        df.to_csv(config.contentDir + '/' + query['reportName'] + '.csv', index=True)

        return True, query['reportName'] + '.csv'

    def inserttoMongo(self,id,query):
        reportName=query.get('reportName',)
        fileName=query['fileName']
        name=fileName.split(".")

        if reportName is None:
            reportName=name[0]

        # reportNames=MasterDump.distinct('reportName')
        # print reportNames

        if name[1]!="csv":
            return False,"Upload csv File only "

        masterdata = MasterDump().find_one({'reportName':reportName})

        if masterdata:
            MasterDump().remove({'reportName':masterdata['reportName']})
        df=pandas.read_csv(config.uploadDir+'/'+ fileName,index_col=False,na_values=False)

        newcolumns = [x.replace('.',' ')for x in df.columns]
        df.fillna('',inplace=True)
        df.columns = newcolumns
        df.to_csv(config.contentDir+'/master/'+reportName+'.csv',index=False,columns = newcolumns )

        dic=df.to_dict(orient='records')
        dict1={}
        dict1['masterdata']=dic
        dict1['partnerId']= "54619c820b1c8b1ff0166dfc"
        dict1['reportName']=reportName
        dict1['UpdatedBy']=flask.session["sessionData"]['userName']
        MasterDump().insert(dict1)
        return True,"Inserted Succesfully"


    def delete(self,id,query):
        data=MasterDump().find_one({'reportName':query['reportName']})
        MasterDump().remove({'reportName':data['reportName']})
        return True,'Deleted Succesfully'


class AdfReport(dbinterface.MongoCollection):

    def __init__(self, hideFields=True):
        super(AdfReport, self).__init__("adfreport")

    def reportDownload(self, id, query):
        print query['reportName']
        return True


class ReconAssignment(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        logger.info('coming herer')
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def updateReconComments(self, id, query):
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        logger.info("*" * 30)
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata

                linkid = uuid.uuid4().int & (1 << 64) - 1
                logger.info(newData)
                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                return True, 'Success'

        else:
            return False, "No records found to update comments."

    def getClosingBalance(self, id, query):
        print query
        if query is not None:
            details = ClosingAccNum().find_one({'reconId': query})
            if details is None:
                return False, ''
            else:
                (status, recon_con) = sql.getConnection()
                if status:
                    logger.info("Exporting STATEMENT_DATE for recon : " + query)
                    (status_rec, stmtdate) = sql.get_max_execution_id_statement_date(recon_con, query)
                    if status:
                        statement_date = stmtdate
                    else:
                        return False, "No Data Found for this reconid"
                    acc = details['ACCOUNT_NUMBER']
                    logger.info(acc)
                    logger.info("-" * 20)
                    closingBal = ClosingBalance().find_one({'ACCOUNT_NUMBER': acc, 'STATEMENT_DATE': statement_date})
        return True, closingBal['CLOSING_BALANCE_AMOUNT']

    def checkRunningStatusOfRecon(self, reconId):
        recon_mongo = MongoClient('mongodb://' + config.engineIp + ':27017')
        db = recon_mongo['reconbuilder']
        reconJobStaus = db['reconJobStatus']
        status = reconJobStaus.find_one({'reconId': reconId})
        if status:
            return False, 'Recon Job Running Wait for some minutes'
        return True, ''

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "UserPool already exists."
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def deleteData(self, condition):
        logger.info(condition)
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        logger.info(found)
        # eventDoc = {'type':'ReconAssignment deletion','actionOn':found['userpool'],'createdDate':utilities.getUtcTime(),'createdBy':flask.session["sessionData"]['userName']}
        # Audits().create(eventDoc)
        return super(ReconAssignment, self).delete(condition)

    def updateReconAss(self, condition):
        logger.info(condition)
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        logger.info(found)
        eventDoc = {'type': 'ReconAssignment updation', 'actionOn': found['userpool'],
                    'createdDate': utilities.getUtcTime(), 'createdBy': flask.session["sessionData"]['userName']}
        return Audits().create(eventDoc)

    def getData(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                logger.info("Exporting all records for recon : " + query['recon_id'])
                (status_rec, reconData) = sql.read_query(recon_con, query)
                (status, business) = Business().get({"RECON_ID": query['recon_id']})
                json_out = {'fileName': self.export_to_csv(query['recon_id'], business['RECON_NAME'], reconData, query),
                            'total': len(reconData)}

                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    # export all the records for the selected recon to csv file
    def export_to_csv(self, recon_id, recon_name, recon_data, query={}):
        recon_data.rename(
            columns={"FREE_TEXT_1": "Unit Name", "FREE_TEXT_2": "Recon Remarks", "FREE_TEXT_3": "Ops Remarks"},
            inplace=True)
        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/' + recon_id + '*.csv'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        to_date = datetime.datetime.now().strftime("%b-%d-%Y")
        export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
        fileName = recon_id + '-' + to_date + '-All.csv'

        headerRows = [['Recon_Name :', recon_name], ['Recon_ID', recon_id],
                      ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']],
                      ['Status', query.get('Status', 'All')],
                      ]

        if query.get('Account name') and query.get('Account Number'):
            headerRows.append(['Account Name :', query['Account name']])
            headerRows.append(['Account Number :', query['Account Number']])

        if query.get('Closing Balance'):
            headerRows.append(['Closing Balance :', query['Closing Balance']])

        headerRows.append(['', ''])
        with open(os.path.join(config.contentDir, "recon_reports/" + "/" + object_id + "/" + fileName), 'w') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerows(headerRows)
            recon_data.to_csv(outcsv, index=False, date_format='%d/%m/%Y')

        return object_id + '/' + fileName

    def addMultiple(self, id, query):
        if query is not None:
            logger.info(query)
            for u in query['user']:
                for b in query['businessContext']:
                    recons = []
                    acPools = []
                    for rec in query['recons']:
                        if b['id'] == rec['business_id']:
                            recons.append(rec['id'])
                    newData = dict()
                    newData['user'] = u['name']
                    newData['businessContext'] = b['id']
                    newData['workPoolName'] = b['name']
                    newData['userpool'] = query['userpool']
                    newData['recons'] = ",".join(recons)
                    if b['id'] == "1111131119":
                        if len(query['accountPools']) > 0:
                            for ac in query['accountPools']:
                                if 'id' in ac:
                                    acPools.append(ac['id'])
                            newData['accountPools'] = ",".join(acPools)
                        else:
                            newData['accountPools'] = ""
                    else:
                        newData['accountPools'] = ""
                    logger.info(newData)
                    (status, data) = self.create(newData)
            return True, ""
        else:
            return True, "No data Found"

    def getReconUnmatchedAll(self, id, query={}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_unmatched_all(recon_con, query)
                # print "recondataaaaaaa" + str(reconData)
                # print type(reconData)
                # pandas.to_datetime(reconData.index, unit='ms')
                if reconData is not None:
                    # Group by linkID and get the count of each linkID
                    reconData = self.groupByLinkID(reconData)
                # add total number of sources
                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatchedAll(self, id, query={}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_all(recon_con, query)
                # reconData = reconData[:2000]
                # logger.info(len(reconData))
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconRollBackAll(self, id, query={}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_rollback_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconPendingForSubmissionAll(self, id, query={}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_pending_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                # json_out = json.dumps(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconWaitingForAuthorizationAll(self, id, query={}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_waitingForAuthorization_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    # apply group by on the cash_output_txn data
    def groupByLinkID(self, txn_dataframe):
        start_time = time.time()
        if len(txn_dataframe) > 0 and txn_dataframe is not None:
            df = pandas.DataFrame()
            # To avoid [Python int too large to convert to C long] error
            df["LINK_ID"] = txn_dataframe["LINK_ID"].astype("str")
            # df["RAW_LINK"] = txn_dataframe["RAW_LINK"]
            grouperDataFrame = df.groupby("LINK_ID", as_index=False)
            # count of link_Id's (link_id, count)
            df_cash_aggr = pandas.DataFrame({'count': grouperDataFrame.size()}).reset_index()

            # raw_link set for each link_Id
            # link1,[rawlink1,rawlink2...] / link,rawlink
            # df_cash_rawlink_grp = grouperDataFrame.agg(lambda g: dict([(k,g[k].tolist()) for k in g])).reset_index()
            # df_cash_rawlink_grp = grouperDataFrame['RAW_LINK'].agg({'GROUPED_RAW_LINK': (lambda x: list(x))})
            reconData_merge = txn_dataframe.merge(df_cash_aggr, on="LINK_ID")
            end_time = time.time()
            logger.info('elapsed time ' + str(end_time - start_time))
            return reconData_merge
        else:
            return pandas.DataFrame()

    def getReconUnmatched(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_unmatched(recon_con, query)
                if not reconData.empty:
                    # count aggregate by link_id
                    reconData = self.groupByLinkID(reconData)
                    reconData["SRC_COUNT"] = len(reconData.groupby("SOURCE_TYPE").size())
                reconData = reconData.replace('None', '')
                if query['recon_id'] in config.two_decimal_format:
                    reconData = reconData.round({'AMOUNT': 2})
                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatched(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched(recon_con, query)
                if not reconData.empty:
                    reconData = self.groupByLinkID(reconData)
                reconData = reconData.replace('None', '')

                if query['recon_id'] in config.two_decimal_format:
                    reconData = reconData.round({'AMOUNT': 2})

                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconRollBack(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_rollback(recon_con, query)

                if not reconData.empty:
                    # Group by linkID and get the count of each linkID
                    reconData = self.groupByLinkID(reconData)
                reconData = reconData.replace('None', '')
                if query['recon_id'] in config.two_decimal_format:
                    reconData = reconData.round({'AMOUNT': 2})

                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconPendingForSubmission(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_pending(recon_con, query)
                reconData = reconData.replace('None', '')
                if query['recon_id'] in config.two_decimal_format:
                    reconData = reconData.round({'AMOUNT': 2})

                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconWaitingForAuthorization(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_waitingForAuthorization(recon_con, query)

                # used to identify partial txn in the selected queue
                reconData = self.groupByLinkID(reconData)
                reconData = reconData.replace('None', '')
                if query['recon_id'] in config.two_decimal_format:
                    reconData = reconData.round({'AMOUNT': 2})

                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getAuditTrail(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_auditTrail(recon_con, query)
                # order by created date
                # reconData.sort('CREATED_DATE',ascending = True, inplace = True)
                reconData = reconData.fillna(value=np.nan)
                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found."

    def getAllocationDetails(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_allocation_history(query=query, connection=recon_con)
                if status_rec:
                    json_out = reconData.to_json(orient='records')
                    return True, json_out
                else:
                    return True, "[]"
            else:
                return False, "No Data Found."
        else:
            return False, "No Data Found."

    def generateAdfReport(self, id):
        with open("hello.csv", 'a') as csvfile:
            csv_file = csv.writer(csvfile, delimiter=';')
            to_date = datetime.datetime.now().strftime("%d/%m/%Y")

            column_order = ["ACCOUNT_NO", "ACCOUNT_CCY", "BANK_TYPE", "DEBIT_CREDIT_FLAG", "TXN_AMT",
                            "PENDING_SINCE", "LOSS_PROVISION_AMT", "GL_NO", "SOURCE_SYS", "Effective_from_Dttm",
                            "Effective_To_Dttm",
                            "EXTRACT_DATE", "BATCH_ID", "BATCH_DATE"]

            adf_report_query = "select trunc(statement_date) as Extract_date,account_number as ACCOUNT_NO,currency as ACCOUNT_CCY ,debit_credit_indicator as DEBIT_CREDIT_FLAG, amount as TXN_AMT from cash_output_txn " \
                               "where record_status = 'ACTIVE' and entry_type = 'T' and matching_status = 'UNMATCHED' and recon_id = 'TRSC_CASH_APAC_20181'"

            adf_report_df = pandas.read_sql(adf_report_query, sql.getConnection()[1])

            for i in range(0, len(column_order)):
                # add static columns
                if column_order[i] not in adf_report_df:
                    adf_report_df[column_order[i]] = np.nan

            adf_report_df["BANK_TYPE"] = "I"
            adf_report_df["SOURCE_SYS"] = "ERECON"
            # calculating ageing
            adf_report_df['PENDING_SINCE'] = (
                    datetime.datetime.now().date() - adf_report_df['EXTRACT_DATE']).datetime.days
            # re-ordering columns
            adf_report_df = adf_report_df[column_order]
            record_count = len(adf_report_df.index)

            data = [["ADF Report"], ['REPORT_DATE', to_date], ["TXN_COUNT", record_count]]
            csv_file.writerows(data)
            adf_report_df.to_csv(csvfile, index=False)

    def getReconColData(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print 'json of colss' + str(json_out)

                tosession = []
                makekey = ''
                makevalue = ''
                for r in json.loads(json_out):
                    a = dict()
                    for (k, v) in r.items():
                        if k == "MDL_FIELD_ID":
                            makekey = v
                        if k == "MDL_FIELD_DATA_TYPE":
                            makevalue = v
                        if makekey is not None and makevalue is not None:
                            a[makekey] = makevalue
                    tosession.append(a)
                # logger.info(tosession)
                flask.session['sessionData']['format'] = tosession
                # print flask.session
                # print "to push to session" + str(tosession)
                return status_rec, json_out
            else:
                return False, "No Recon Column Data"

    def validateTransaction(self, selectedTxn):
        logger.info("Validation initiated ..")

        # Added gl_account_number as a part of custom validation changes
        df_cash = pandas.read_json(json.dumps(selectedTxn, default=json_util.default), orient='columns',
                                   dtype={'GL_ACCOUNT_NUMBER': 'str'})

        # df_validate = df_cash.loc[:,
        #              ['ACCOUNT_NUMBER', 'CURRENCY', 'AMOUNT', 'DEBIT_CREDIT_INDICATOR', 'SOURCE_TYPE', 'RECON_ID']]
        reconID = df_cash['RECON_ID'].iloc[0]

        validation_msg = "Unable to forcematch records "
        validation_status = True

        # Currency Validation
        if reconID in config.currency_validation and 'CURRENCY' in df_cash.columns:
            if len(pandas.unique(df_cash.loc[:, "CURRENCY"])) > 1:
                logger.info("mismatch in currency")
                validation_status = False
                validation_msg += ",mismatch in currency"
                # return False,"Could not forcematch the records due to mismatch in currency"

        # Amount Validation (Need to be upgraded)

        # checking for number of sources
        # incase of three src recons (Vald Rule --> {S1 - S2} == {S1 - S3} == 0)

        if 'AMOUNT' in df_cash.columns and reconID not in config.ignore_amount_validation:
            if 'DEBIT_CREDIT_INDICATOR' in df_cash.columns:
                df_cash.loc[df_cash['DEBIT_CREDIT_INDICATOR'].isnull(), 'DEBIT_CREDIT_INDICATOR'] = ''
            else:
                df_cash['DEBIT_CREDIT_INDICATOR'] = ''

            pivoted_frame = pandas.pivot_table(df_cash, columns=['SOURCE_TYPE', 'DEBIT_CREDIT_INDICATOR'],
                                               values=['AMOUNT'], aggfunc={"AMOUNT": np.sum})
            pivoted_frame = pivoted_frame.unstack(level=2)
            logger.info(pivoted_frame)
            pivoted_frame.columns = [''.join(list(col)) for col in pivoted_frame.columns.values]
            pivoted_frame.reset_index().drop(axis=1, labels="level_0")
            logger.info(pivoted_frame)
            if reconID not in config.three_src_recon:
                # pivoted_frame = pivoted_frame.unstack(level=2)
                # pivoted_frame.columns = [''.join(list(col)) for col in pivoted_frame.columns.values]
                # pivoted_frame.reset_index().drop(axis=1, labels="level_0")
                pivoted_frame['OutStanding_Amount'] = 0
                pivoted_frame = pivoted_frame.fillna(0)

                if 'D' in pivoted_frame.columns and 'C' in pivoted_frame.columns:
                    pivoted_frame["OutStanding_Amount"] = (-pivoted_frame.loc[:, 'D']) + pivoted_frame.loc[:, 'C']
                    pivoted_frame = pivoted_frame.reset_index()
                    diff_amount = pivoted_frame.loc[:, "OutStanding_Amount"].sum()
                else:
                    tranposed_frame = pivoted_frame.T
                    # the above transpose return multi-level index, get required index level
                    tranposed_frame.columns = tranposed_frame.columns.get_level_values(1)
                    diff_amount = tranposed_frame.loc[:, "S1"].sum() - tranposed_frame.loc[:, "S2"].sum()

            else:
                tranposed_frame = pivoted_frame.T
                logger.info(tranposed_frame)
                tranposed_frame.columns = tranposed_frame.columns.get_level_values(1)
                logger.info(tranposed_frame)
                diff_amount = ((tranposed_frame["S1"] - tranposed_frame["S2"]) + (
                        tranposed_frame["S1"] - tranposed_frame["S3"])).tolist()[0]
                logger.info(diff_amount)

            if round(diff_amount, 2) != 0 or round(diff_amount, 2) != 0.0:
                logger.info("could not forcematch records due to mismatch in amount")
                validation_status = False
                validation_msg += ",outstanding amount is not zero"
                # return False, "could not forcematch records, due to mismatch in amount."

        # Account number validation
        if reconID in config.account_number_validation:
            logger.info("Before ")
            logger.info(df_cash["ACCOUNT_NUMBER"])
            df_cash["ACCOUNT_NUMBER"] = df_cash["ACCOUNT_NUMBER"].apply(lambda x: str(x).lstrip("0"))
            logger.info("After")
            logger.info(df_cash["ACCOUNT_NUMBER"])
            if len(pandas.unique((df_cash.loc[:, "ACCOUNT_NUMBER"]))) > 1:
                logger.info("mismatch in account number")
                validation_status = False
                validation_msg += ",found different Account Numbers in selected transactions"
                # return False, "could not forcematch records, due to mismatch in account numbers."
                # if len(pandas.unique(str(df_validate.loc[:, "ACCOUNT_NUMBER"]).lstrip("0"))) > 1:
                #     logger.info("mismatch in account number")
                #     return False, "could not forcematch records, due to mismatch in account numbers."

        # check for custom validation if exists
        if reconID in config.customValidation:
            # Get custom validation function

            # Return True if validation fails else False
            funcName = config.customValidation[reconID]
            status, msg = eval('CustomValidation().' + funcName + '(df_cash)')
            if status:
                validation_status = False
                validation_msg += msg

        return validation_status, "Success" if validation_status else validation_msg

    # validation to check for partial records in the selected queue
    def checkForPartialTxnSelection(self, selectedTxn):

        # To avoid [Python int too large to convert to C long] error
        part_match_frame = pandas.read_json(json.dumps(selectedTxn, default=json_util.default), orient='columns',
                                            dtype={'LINK_ID': 'int64'})
        part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
        df_cash_aggr = pandas.DataFrame({'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()
        reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
        reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]

        return True if np.count_nonzero(reconData_merge["count_res"]) > 0 else False

    def parse_dates(self, value):
        if value != value or value is None:
            return np.nan
        elif isinstance(value, pandas.tslib.Timestamp):
            return value
        elif isinstance(value, (np.int64, np.float64, int, float)):
            return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(value / 1000))
        else:
            return parse(value).strftime("%Y-%m-%d %H:%M:%S")

    def groupColumns(self, id, query):
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        cloned_val = query['selected_rec']
        # reconId = query['recon_id']
        # logger.info(reconId)
        newData = []
        oldData = []

        status_commit = False
        validataion_status = True
        # validation to avoid partial selection of transactions
        if self.checkForPartialTxnSelection(cloned_val):
            return True, "Partial records found in the selected queue."

        tmp_var = json.loads(json.dumps(cloned_val))
        if cloned_val is not None and len(tmp_var) > 0:
            if tmp_var[0]["SRC_COUNT"] < 3 and tmp_var[0]["RECON_ID"] in config.three_src_recon:
                return True, "Please select transactions across the sources to proceed."

        if not (query['singleSideMatch']):
            (validataion_status, valdation_msg) = self.validateTransaction(cloned_val)

        if query is not None and query['selected_rec'] is not None and validataion_status:
            (status, recon_con) = sql.getConnection()
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata
                linkid = str(uuid.uuid4().int & (1 << 64) - 1)

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "GROUPED"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 3
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_GROUPED"
                    rec_old['LINK_ID'] = linkid
                    rec_old['IPADDRESS'] = "0.0.0.0"
                    rec_old['GROUPED_FLAG'] = 'Y'
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = 1
                    rec_old["MATCHING_EXECUTION_STATUS"] = "OUTSTANDING"
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    if 'PREV_LINK_ID' in rec_old:
                        del rec_old['PREV_LINK_ID']
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)

                logger.info('after loop and before converting to df time' + str(time.time()))
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                logger.info('after converting to df time' + str(time.time()))
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)

                for i in parseDates:
                    if i in df_cash.columns:
                        # logger.info(df_cash["STATEMENT_DATE"])
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                logger.info('before inserting time' + str(time.time()))

                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0

                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)
                logger.info('after inserting time' + str(time.time()))

                if query["groupAndForce"]:
                    logger.info("grouping done entered to force matching")
                    query_link_id = dict()
                    query_link_id["LINK_ID"] = linkid
                    columnsToConvertCash = ["LINK_ID", "RAW_LINK", "PARTICIPATING_RECORD_TXN_ID", "OBJECT_OID",
                                            "OBJECT_ID", "RECON_EXECUTION_ID"]

                if query['groupAndForce']:
                    queryForce = dict()
                    # queryForce['reconId'] = reconId
                    queryForce['selected_rec'] = cloned_val
                    queryForce['comments'] = query['comments']
                    queryForce["reason_code"] = query["reason_code"]
                    queryForce["recon_id"] = query['recon_id']
                    logger.info("for force matching")
                    (status_force, forcematchData) = self.forceMatchingColumns('dummy', query=queryForce,
                                                                               singleEntryMatchIndictor=query[
                                                                                   'singleSideMatch'])
                    if status_force and query["groupForceMatchAuth"]:
                        logger.info("for authorization-------")
                        queryAuth = dict()
                        queryAuth['comments'] = query['comments']
                        queryAuth['reason_code'] = query['reason_code']
                        queryAuth['authorizer'] = query['authorizer']
                        queryAuth['recon_id'] = query['recon_id']
                        # logger.info(type(forcematchData['oldData']))
                        (status_auth, auth_data) = self.submitForAuthorization('dummy', query=queryAuth,
                                                                               oldData=cloned_val,
                                                                               singleEntryMatchIndictor=query[
                                                                                   'singleSideMatch'])

                return True, 'Success'
            else:
                return False, ''
        else:
            if not validataion_status:
                return True, valdation_msg
            else:
                return True, 'No records selected'

    def submitForAuthorization(self, id, query, oldData=None, singleEntryMatchIndictor=False):
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg
        oldData_auth = []
        newData = []
        (status, recon_con) = sql.getConnection()
        txn_data = {}

        txn_data = oldData if oldData is not None else query['selected_rec']

        if status and txn_data is not None:
            for rec in txn_data:
                rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                rec['RECORD_STATUS'] = "INACTIVE"
                newData.append(rec)
            (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
            if not status:
                return False, updata
            for rec_old in txn_data:
                rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                rec_old["UPDATED_BY"] = None
                rec_old['MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_WAITING_FOR_AUTH" if singleEntryMatchIndictor or \
                                                                                         rec_old[
                                                                                             'MATCHING_EXECUTION_STATE'] == 'SINGLE_ENTRY_MATCHED' else "WAITING_FOR_AUTHORIZATION"
                rec_old['LINK_ID'] = rec_old['LINK_ID']
                rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                rec_old["MATCHING_STATUS"] = "MATCHED"
                rec_old["RECORD_STATUS"] = "ACTIVE"
                rec_old['AUTHORIZATION_BY'] = query['authorizer']
                rec_old["REASON_CODE"] = query["reason_code"]
                ## new added columns
                rec_old['TXN_MATCHING_STATE_CODE'] = 16
                rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_PENDING"
                rec_old[
                    'TXN_MATCHING_STATE'] = "SINGLE_SOURCE_WAITING_FOR_AUTH" if singleEntryMatchIndictor else "WAITING_FOR_AUTHORIZATION"
                rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                rec_old["MATCHING_EXECUTION_STATUS"] = "WAITING FOR AUTHORIZATION"
                rec_old["MANUAL_MATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"
                rec_old["FORCEMATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"

                # work around to avoid account number captured as exponential value
                rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                oldData_auth.append(rec_old)
            df_cash_auth = pandas.read_json(json.dumps(oldData_auth, default=json_util.default), orient='columns',
                                            dtype=config.dataTypes)
            parseDates = []
            for k, v in datatypes.datatypes.items():
                if v == "np.datetime64":
                    parseDates.append(k)
            for i in parseDates:
                if i in df_cash_auth.columns:
                    df_cash_auth[i] = df_cash_auth[i].apply(lambda x: self.parse_dates(x))
                    df_cash_auth[i] = pandas.to_datetime(df_cash_auth[i], format='%Y-%m-%d %H:%M:%S')
                    df_cash_auth[i] = df_cash_auth[i].fillna('')
                    # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
            df_cash_auth["POSITIONS_LINK_ID"] = 0
            df_cash_auth["PREV_LINK_ID"] = 0
            (status_cash_auth, ins_data_cash) = sql.insertDFToDB(df=df_cash_auth, table_name="cash_output_txn",
                                                                 connection=recon_con)
            return True, 'Success'
        else:
            return False, "Oracle Connection Failed"

    def forceMatchingColumns(self, id, query, singleEntryMatchIndictor=False):
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old["UPDATED_BY"] = None
                    rec_old[
                        'MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_MATCHED" if singleEntryMatchIndictor else "FORCEMATCH_MATCHED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 14
                    rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old[
                        'TXN_MATCHING_STATE'] = "SINGLE_SOURCE_SUBMISSION_PENDING" if singleEntryMatchIndictor else "SUBMISSION_PENDING"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["MATCHING_EXECUTION_STATUS"] = "AUTHORIZATION SUBMISSION PEND"
                    rec_old["MANUAL_MATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)

                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)
                dataToReturn = dict()
                dataToReturn['oldData'] = oldData

                # update recon dashboard summary
                return True, dataToReturn
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def unGroupingColumns(self, id, query):
        # print query
        # print query['comments']
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        cloned_val = query['selected_rec']
        newData = []
        oldData = []

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # To avoid [Python int too large to convert to C long] error
            part_match_frame = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                                dtype={'LINK_ID': 'int64'})
            part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
            df_cash_aggr = pandas.DataFrame(
                {'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()

            reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
            reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]

            part_match_count = np.count_nonzero(reconData_merge["count_res"])
            ungrouped_rec_count = len([1 for i in reconData_merge["validation_count"] if i == 1])

            if status and part_match_count == 0 and ungrouped_rec_count == 0:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata
                linkid = uuid.uuid4().int & (1 << 64) - 1
                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old["UPDATED_BY"] = None
                    rec_old['MATCHING_EXECUTION_STATE'] = "UNGROUPED"
                    rec_old['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old['GROUPED_FLAG'] = 'N'
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 4
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_UNGROUPED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                return True, 'Success'
            elif part_match_count >= 1:
                logger.info("ungrouping only part of the records")
                return True, "Partial ungrouping of transaction is not allowed."
            elif ungrouped_rec_count >= 1:
                logger.info("ungrouping the singleton records")
                return True, "found singleton records in the seleted list."
            else:
                return False, ''
        else:
            return False, "No Records to Ungroup"

    def authorizeRecords(self, id, query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        status1, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status1:
            return False, msg
        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                return True, "Partial records found in the selected queue."

            (status, recon_con) = sql.getConnection()

            # get unique link_id from the selected txn list
            unique_link_id_dict = dict()
            cash_output_df = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                              dtype={'LINK_ID': 'int64'})
            unique_link_id_dict["LINK_ID"] = np.unique(cash_output_df['LINK_ID']).tolist()
            createdUser = cash_output_df['CREATED_BY'].unique()
            if flask.session['sessionData']['userName'] in createdUser:
                return False, 'Selected records are force matched by You, so Authorization not allowed'

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status_up, upData) = sql.updateSqlData(query=newData, connection=recon_con)

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "SINGLE_ENTRY_AUTHORIZED" if rec_old[
                                                                                           "MATCHING_EXECUTION_STATE"] == "SINGLE_ENTRY_WAITING_FOR_AUTH" else "MATCHED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "AUTHORIZED"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    # New columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 15 if rec_old[
                                                                   "TXN_MATCHING_STATE"] == "SINGLE_SOURCE_WAITING_FOR_AUTH" else 14
                    rec_old['TXN_MATCHING_STATUS'] = "FORCE_MATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SINGLE_SOURCE_AUTHORIZED" if rec_old[
                                                                                      "TXN_MATCHING_STATE"] == "SINGLE_SOURCE_WAITING_FOR_AUTH" else "FORCE_MATCHED"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')

                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                return True, 'Success'
            else:
                return False, 'Sql Connection Not Found'
        else:
            return False, "No Records to authorize"

    def rejectRecords(self, id, query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                return True, "Partial records found in the selected queue."

            # get unique link_id from the selected txn list
            unique_link_id_dict = dict()
            cash_output_df = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                              dtype={'LINK_ID': 'int64'})

            createdUser = cash_output_df['CREATED_BY'].unique()
            if flask.session['sessionData']['userName'] in createdUser:
                return False, 'Selected records are force matched by You, so Authorization not allowed'
            unique_link_id_dict["LINK_ID"] = np.unique(cash_output_df['LINK_ID']).tolist()

            (status, recon_con) = sql.getConnection()

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status_up, upData) = sql.updateSqlData(query=newData, connection=recon_con)

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['RECONCILIATION_STATUS'] = "EXCEPTION"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 21
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "AUTHORIZATION_REJECTED"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records to authorize"

    def rollBack(self, id, query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # To avoid [Python int too large to convert to C long] error
            part_match_frame = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                                dtype={'LINK_ID': 'int64'})
            part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
            df_cash_aggr = pandas.DataFrame(
                {'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()

            reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
            reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]

            part_match_count = np.count_nonzero(reconData_merge["count_res"])

            if status and part_match_count == 0:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status_up, upData) = sql.updateSqlData(query=newData, connection=recon_con,
                                                        rollBackRecordIndicator=True)
                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "ROLLBACK_AUTH_WAITING"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "WAITING FOR ROLBACK AUTH"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old['AUTHORIZATION_BY'] = query['authorizer']
                    rec_old['AUTHORIZATION_STATUS'] = "AUTHORIZED"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 18
                    rec_old['TXN_MATCHING_STATUS'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "ROLLBACK_AUTH_WAITING"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    rec_old['RECON_EXECUTION_ID'] = sql.getLatestExecutionID(recon_con, rec_old['RECON_ID'])
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)
                return True, 'Success'
            elif part_match_count > 0:
                logger.info("Doing part match")
                return True, "Partial Rollback is not allowed"
            else:
                return False, ''
        else:
            return False, "No Records to authorize"

    def authorizeRollBackRecords(self, id, query):
        # logger.info('query ----------------------------------' + str(query['selected_rec']))
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []

        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                return True, "Partial records found in the selected queue."

            cash_output_df = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                              dtype={'LINK_ID': 'int64'})
            createdUser = cash_output_df['CREATED_BY'].unique()

            if flask.session['sessionData']['userName'] in createdUser:
                return False, 'Selected records are force matched by You, so Authorization not allowed'

            (status, recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID",
                                          "WORK_POOL_ID"]

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "FORCEMATCH_ROLLEDBACK"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "UNMATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = None
                    ## new added columns ( need to verify)
                    rec_old['TXN_MATCHING_STATE_CODE'] = 23
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "ROLLBACK_AUTHORIZED"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RECONCILIATION_STATUS"] = "EXCEPTION"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["MATCHING_EXECUTION_STATUS"] = "OUTSTANDING"
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)
                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def rejectRollBackRecords(self, id, query):
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        if query is not None and query['selected_rec'] is not None:

            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                logger.info("partial slection of records encountered")
                return True, "Partial records found in the selected queue."

            cash_output_df = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',
                                              dtype={'LINK_ID': 'int64'})
            createdUser = cash_output_df['CREATED_BY'].unique()

            if flask.session['sessionData']['userName'] in createdUser:
                return False, 'Selected records are force matched by You, so Authorization not allowed'

            (status, recon_con) = sql.getConnection()

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con, rollBackRecordIndicator=True)

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "MANUAL_REJECTED_MATCHED_ROLLBACK"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    ## new added columns ( need to verify)
                    rec_old['TXN_MATCHING_STATE_CODE'] = 22
                    rec_old['TXN_MATCHING_STATUS'] = "AUTOMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "ROLLBACK_REJECTED"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = "AUTHORIZED"

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def submitForInvestigator(self, id, query):
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                return True, "Partial records found in the selected queue."

                # Get Unique transactions from the selected list
            values = set()
            for items in cloned_val:
                values.add(items["LINK_ID"])
            unique_txndata = dict()
            unique_txndata["LINK_ID"] = values

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "UNMATCHED_INVESTIGATION_PENDING"
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RESOLUTION_COMMENTS"] = query["comments"]
                    rec_old['TXN_MATCHING_STATE_CODE'] = 24
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED_INVESTIGATION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "UNMATCHED_INVESTIGATION_PENDING"
                    # authorization_by
                    rec_old["AUTHORIZATION_BY"] = query["deptUserPool"]
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["FORCEMATCH_AUTHORIZATION"] = None
                    rec_old["AUTHORIZATION_STATUS"] = "SYSTEM_AUTHORIZED"
                    rec_old["RECONCILIATION_STATUS"] = "EXCEPTION"
                    rec_old["MATCHING_STATUS"] = "UNMATCHED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    # utilized only for dept user management
                    rec_old["ACTION_BY"] = flask.session["sessionData"]['userName']

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''

                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')

                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)

                if status_cash:
                    subject = 'RECORDS FOR INVESTIGATION'
                    body = "User {} has submitted {} records for the Investigation to your userPool \n Recon_ID: {}".format(
                        flask.session["sessionData"]['userName'], len(cloned_val), query['recon_id'])
                    status, doc = UserPool().getAll({'perColConditions': {'name': query["deptUserPool"]}})
                    if status:
                        userNames = []
                        for pool in doc['data']:
                            logger.info(pool)
                            userNames.extend(pool['users'].split(','))
                        if len(userNames) > 0:
                            recipents = []
                            for userName in userNames:
                                status, user = UsersColl().get({'userName': userName})
                                if user:
                                    recipents.append(user['email'])
                    recipents = ','.join(recipents)
                    NoAuth().sendMailTo(subject, recipents, body)
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records to found"

    def investigateRecords(self, id, query):
        # print query
        # print query['comments']
        status, msg = self.checkRunningStatusOfRecon(query['recon_id'])
        if not status:
            return False, msg

        cloned_val = query['selected_rec']
        newData = []
        oldData = []

        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            # Validation for part match
            # validation to avoid partial selection of transactions
            if self.checkForPartialTxnSelection(cloned_val):
                return True, "Partial records found in the selected queue."

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                if not status:
                    return False, updata

                for rec_old in cloned_val:
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "SYSTEM_UNMATCHED"
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "UNMATCHED"
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old["INVESTIGATOR_COMMENTS"] = query["comments"]
                    rec_old['TXN_MATCHING_STATE_CODE'] = 24
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED "
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED "
                    rec_old['INVESTIGATED_BY'] = flask.session["sessionData"]['userName']
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old['UPDATED_BY'] = None
                    rec_old['AUTHORIZATION_BY'] = None

                    # work around to avoid account number captured as exponential value
                    rec_old['ACCOUNT_NUMBER'] = rec_old['ACCOUNT_NUMBER'] if 'ACCOUNT_NUMBER' in rec_old else ''
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype=config.dataTypes)
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = df_cash[i].apply(lambda x: self.parse_dates(x))
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S')
                        df_cash[i] = df_cash[i].fillna('')

                        # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                                table_name="cash_output_txn", connection=recon_con)
                return True, 'Success'
            else:
                return False, ''
        else:
            return False, "No Records found."

    def getUserPoolDetails(self, recon_id, user_id):
        (status_b, data_d) = ReconAssignment().getAll({'perColConditions': {'user': user_id, 'recons': recon_id}})
        if data_d["total"] > 0:
            return data_d["data"][0]["userpool"]
        else:
            return ""

    def getReconMatchedExecutionDate(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_execution_date(recon_con, query)
                if reconData is not None and len(reconData) > 0:
                    if query['recon_id'] in config.ignore_matched_txns:
                        json_out = dict()
                        status, business = Business().get({'RECON_ID': query['recon_id']})
                        json_out['fileName'] = self.export_to_csv(query['recon_id'], business['RECON_NAME'], reconData)
                        json_out['total'] = len(reconData)
                        return True, json_out
                    else:
                        # To avoid [Python int too large to convert to C long] error
                        reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                        df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                        reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")
                        json_out = reconData_merge.to_json(orient='records')
                        # print "json_out" + str(json_out)
                        return status_rec, json_out
                else:
                    return True, '[]'
            else:
                return False, "No Data Found for this reconid"

    # for recons listed as ignore transactions return export data to csv
    # instead of returning the data back
    def getReconMatchedStatementDate(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_statement_date(recon_con, query)

                if reconData is not None and len(reconData) > 0:
                    if query['recon_id'] in config.ignore_matched_txns:
                        json_out = dict()
                        status, business = Business().get({'RECON_ID': query['recon_id']})
                        json_out['fileName'] = self.export_to_csv(query['recon_id'], business['RECON_NAME'], reconData)
                        json_out['total'] = len(reconData)
                        return True, json_out
                    else:
                        # To avoid [Python int too large to convert to C long] error
                        reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                        df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                        reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")

                        json_out = reconData_merge.to_json(orient='records')
                        # print "json_out" + str(json_out)
                        return status_rec, json_out
                else:
                    return True, "[]"
            else:
                return False, "No Data Found for this reconid"

    def getReconMatched_St_Ex_Date(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_ex_st_date(recon_con, query)

                if reconData is not None and len(reconData) > 0:
                    if query['recon_id'] in config.ignore_matched_txns:
                        json_out = dict()
                        status, business = Business().get({'RECON_ID': query['recon_id']})
                        json_out['fileName'] = self.export_to_csv(query['recon_id'], business['RECON_NAME'], reconData)
                        json_out['total'] = len(reconData)
                        return True, json_out
                    else:
                        # To avoid [Python int too large to convert to C long] error
                        reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                        df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                        reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")
                        json_out = reconData_merge.to_json(orient='records')
                        return status_rec, json_out
                else:
                    return True, '[]'
            else:
                return False, "No Data Found for this reconid"

    def getExceptionExecutionDate(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_execution_date(recon_con, query)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                          recon_con)
                    reconData = pandas.merge(reconData, df2, on=["RECON_ID"], how="left")

                    reconData['TOTAL_TRANSACTIONS'] = 0
                    reconData['TOTAL_TRANSACTIONS'] = reconData['PERFECT_MATCHED_RECORDS_COUNT'] + reconData[
                        'EXCEPTION_RECORDS_COUNT']
                    reconData.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)

                    execution_Data = pandas.DataFrame()
                    for i in config.execution_summary_columns:
                        execution_Data[i] = reconData[i]
                    json_out = execution_Data.to_json(orient='records')
                else:
                    json_out = str([])
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getExceptionStatementDate(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_statement_date(recon_con, query)
                # logger.info(len(reconData.index))
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                          recon_con)
                    reconData = pandas.merge(reconData, df2, on=["RECON_ID"], how="left")
                    # logger.info(reconData['RECON_EXECUTION_ID'])

                    reconData['TOTAL_TRANSACTIONS'] = 0
                    reconData['TOTAL_TRANSACTIONS'] = reconData['PERFECT_MATCHED_RECORDS_COUNT'] + reconData[
                        'EXCEPTION_RECORDS_COUNT']
                    reconData.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)

                    execution_Data = pandas.DataFrame()
                    for i in config.execution_summary_columns:
                        execution_Data[i] = reconData[i]
                    json_out = execution_Data.to_json(orient='records')
                else:
                    json_out = str([])
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getException_St_Ex_Date(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_ex_st_date(recon_con, query)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                          recon_con)
                    reconData = pandas.merge(reconData, df2, on=["RECON_ID"], how="left")

                    reconData['TOTAL_TRANSACTIONS'] = 0
                    reconData['TOTAL_TRANSACTIONS'] = reconData['PERFECT_MATCHED_RECORDS_COUNT'] + reconData[
                        'EXCEPTION_RECORDS_COUNT']
                    reconData.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)

                    execution_Data = pandas.DataFrame()
                    for i in config.execution_summary_columns:
                        execution_Data[i] = reconData[i]
                    json_out = execution_Data.to_json(orient='records')
                else:
                    json_out = str([])  # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getExecutionSummary(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary(query, recon_con)
                # logger.info(reconData)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T

                    df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                          recon_con)
                    reconData = pandas.merge(reconData, df2, on=["RECON_ID"], how="left")
                    reconData['TOTAL_TRANSACTIONS'] = 0
                    reconData['TOTAL_TRANSACTIONS'] = reconData['PERFECT_MATCHED_RECORDS_COUNT'] + reconData[
                        'EXCEPTION_RECORDS_COUNT']
                    reconData.rename(columns={'EXECUTION_DATE': 'EXECUTION_DATE_TIME'}, inplace=True)

                    execution_Data = pandas.DataFrame()
                    for i in config.execution_summary_columns:
                        execution_Data[i] = reconData[i]
                    json_out = execution_Data.to_json(orient='records')
                else:
                    json_out = str([])

                logger.info("json_out" + str(json_out))
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getSummary(self, id, query):
        logger.info('Comming Here......')
        (status, totalData) = ReportInterface().queryData(levelkey='BUSINESS_CONTEXT_ID', filters={},
                                                          datasetName='BusinessRecon', summary='get')

        file_name = self.generateADFReport()
        total_data = {'summary_data': totalData, 'file_name': file_name}
        return True, total_data

    def generateADFReport(self):
        (status, connection) = sql.getConnection()
        if status and connection is not None:
            with open(os.path.join("meta/tmp_adf_template.txt"), 'w') as csvfile:
                csv_file = csv.writer(csvfile, delimiter='|')
                today_date_fmt = str(datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%Y %H:%M:%S'))
                today_date_fmt1 = today_date_fmt.replace(' ', ':')
                today_date_fmt2 = today_date_fmt1.replace('-', '')

                today_date = datetime.datetime.strftime(datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'),
                                                        '%d%m%Y%H%M%S')

                column_order = ["HDR", "REPORT_DATE", "ACCOUNT_NO", "ACCOUNT_CCY", "BANK_TYPE", "DEBIT_CREDIT_FLAG",
                                "TXN_COUNT", "TXN_AMT", "PENDING_SINCE", "LOSS_PROVISION_AMT", "GL_NO",
                                "TRANSACTION_ID", "ENTRY_SOURCE", "SOURCE_SYS", "Effective_from_Dttm",
                                "Effective_To_Dttm", "EXTRACT_DATE", "BATCH_ID", "BATCH_DATE", "ERECON"]

                column_order.append((today_date_fmt2))

                adf_report_query = "select trunc(statement_date) as Extract_date,account_number as ACCOUNT_NO,currency as ACCOUNT_CCY ,debit_credit_indicator as DEBIT_CREDIT_FLAG, amount as TXN_AMT, source_name as ENTRY_SOURCE, REF_NUMBER1 as TRANSACTION_ID from cash_output_txn " \
                                   "where record_status = 'ACTIVE' and entry_type = 'T' and matching_status = 'UNMATCHED' and recon_id = 'TRSC_CASH_APAC_20181'"

                adf_report_df = pandas.read_sql(adf_report_query, connection)

                for i in range(0, len(column_order)):
                    # add static columns
                    if column_order[i] not in adf_report_df:
                        adf_report_df[column_order[i]] = np.nan
                adf_report_df["REPORT_DATE"] = datetime.datetime.strftime(
                    datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'), '%d-%m-%Y')
                adf_report_df["TXN_COUNT"] = 1
                adf_report_df["LOSS_PROVISION_AMT"] = 0
                adf_report_df["BANK_TYPE"] = "I"
                adf_report_df["SOURCE_SYS"] = "ERECON"

                # calculating ageing
                tm_yday = datetime.datetime.now().timetuple().tm_yday
                if len(adf_report_df):
                    adf_report_df['PENDING_SINCE'] = tm_yday - adf_report_df['EXTRACT_DATE'].dt.dayofyear

                adf_report_df['EXTRACT_DATE'] = datetime.datetime.strftime(
                    datetime.datetime.strptime(today_date_fmt, '%d-%m-%Y %H:%M:%S'), '%d-%m-%Y')
                # re-ordering columns
                adf_report_df = adf_report_df[column_order]
                record_count = len(adf_report_df.index)

                adf_report_df.to_csv(csvfile, index=False, sep="|", date_format='%d-%m-%Y')

            # As per the requirement, need to strip the pipe delimter for the HDR columns
            file_name = "ERECON_ADF_TRANSACTIONS_LONG_OS_UNRECONCILED_TRNX_" + today_date + ".txt"
            object_id = str(flask.session["sessionData"]['_id'])
            if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
                os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

            with open(os.path.join(config.contentDir, "recon_reports/" + "/" + object_id + "/" + file_name),
                      "w") as adf_output:
                with open(os.path.join("meta/tmp_adf_template.txt"), "r") as tmp_adf_report:
                    count = 0
                    for line in tmp_adf_report:
                        if count > 0:
                            frmt_line = line.lstrip('|')[:-3] + '\n'
                        else:
                            frmt_line = line
                        adf_output.write(frmt_line)
                        count += 1
                adf_output.write("TRL|" + str(record_count) + "|" + today_date_fmt2)
        return object_id + '/' + file_name

    def getSuspence(self, id, query):
        (status_sql, recon_con) = sql.getConnection()
        (status, suspData) = sql.read_col_query_unmatched_suspence(connection=recon_con, query={})

        if not suspData.empty:
            account_lookup = suspData.drop_duplicates(['ACCOUNT_NUMBER'], keep='first')
            account_lookup = account_lookup[
                ['ACCOUNT_POOL_NAME', 'ACCOUNT_NUMBER', 'ACCOUNT_NAME', 'ACCOUNT_POOL_CONTEXT_CODE', 'RECON_ID']]

            susp_df = suspData[~suspData['TXN_MATCHING_STATUS'].isnull()]
            susp_df['Count'] = 1
            susp_summary = susp_df.groupby(
                ['RECON_ID', 'ACCOUNT_NUMBER', 'TXN_MATCHING_STATUS']).sum().reset_index().to_dict(
                orient='records')

            # compute counts
            count_df = pandas.DataFrame(
                [{'RECON_ID': _x['RECON_ID'], 'ACCOUNT_NUMBER': _x['ACCOUNT_NUMBER'],
                  _x['TXN_MATCHING_STATUS']: _x['Count']} for _x in susp_summary])
            count_df = count_df.groupby(['ACCOUNT_NUMBER', 'RECON_ID']).sum().reset_index()

            # compute amount
            amount_df = pandas.pivot_table(susp_df, index=['ACCOUNT_NUMBER', 'RECON_ID'],
                                           columns=['DEBIT_CREDIT_INDICATOR'],
                                           values=['AMOUNT', "Count"], aggfunc={"AMOUNT": np.sum, "Count": len})
            amount_df.columns = [''.join(list(col)) for col in amount_df.columns.values]
            amount_df = amount_df.reset_index()

            total_df = amount_df.merge(count_df, on=['ACCOUNT_NUMBER', 'RECON_ID'])
            total_df.rename(
                columns={"CountC": "CREDIT_COUNT", "CountD": "DEBIT_COUNT", "AMOUNTC": "CREDIT_AMOUNT",
                         "AMOUNTD": "DEBIT_AMOUNT"},
                inplace=True)
            total_df[['CREDIT_COUNT', 'DEBIT_COUNT']].fillna(0, inplace=True)
            total_df[['CREDIT_AMOUNT', 'DEBIT_AMOUNT']].fillna(0.0, inplace=True)
            total_df["NETOUTSTANDINGAMOUNT"] = total_df["DEBIT_AMOUNT"].fillna(0.0) - total_df["CREDIT_AMOUNT"].fillna(
                0.0)
            total_df["NETOUTSTANDINGCOUNT"] = total_df["CREDIT_COUNT"].fillna(0) + total_df["DEBIT_COUNT"].fillna(0)

            total_df = total_df.merge(account_lookup, on=['RECON_ID', 'ACCOUNT_NUMBER'], how='right')
            total_df.fillna(0, inplace=True)

            if 'RECON_ID' in total_df.columns:
                recon_df = pandas.read_sql(
                    "select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                    recon_con)
                total_df = pandas.merge(total_df, recon_df, on=["RECON_ID"], how="left")

                for column in total_df.columns:
                    if column.find("AMOUNT") != -1:
                        total_df[column] = total_df[column].apply(lambda x: '{:.2f}'.format(x))
            return True, total_df.to_json(orient="records")
        else:
            return True, '[]'

    def get_Inbox_transaction(self, id, query={}):
        (status, connection) = sql.getConnection()
        if status and connection is not None:
            (status, dfToSend) = sql.read_col_query_indox_txn(connection, query)
            dfToSend = self.groupByLinkID(dfToSend)
            for column in dfToSend.columns:
                if column == "AMOUNT":
                    dfToSend[column] = dfToSend[column].apply(lambda x: '{:.2f}'.format(x))
            return True, dfToSend.to_json(orient="records")
        else:
            return True, "No data found for the selection."

    def getMhadaReport(self, id, query={}):

        (status, data) = MhadaReport().getAll({})

        (status, connection) = sql.getConnection()
        query = "select SOURCE_TYPE,DEBIT_CREDIT_INDICATOR,AMOUNT,TXT_5_002,MATCHING_STATUS from cash_output_txn" \
                " where record_status = 'ACTIVE' and recon_id = 'MHADA_CASH_APAC_20441'"

        df = pandas.read_sql(query, connection)
        result = dict()
        # print df
        df = df[['AMOUNT', 'TXT_5_002', 'SOURCE_TYPE', 'MATCHING_STATUS']]

        df['COUNT'] = 0

        df = pandas.pivot_table(df, index=['SOURCE_TYPE', 'MATCHING_STATUS'],
                                columns='TXT_5_002', values=['AMOUNT', "COUNT"],
                                aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0)

        logger.info(df)
        df.columns = [''.join(list(col)) for col in df.columns.values]

        # CREDIT & DEBIT counts
        credit_count = df['COUNTC'].to_dict()
        debit_count = df['COUNTD'].to_dict()

        # adding credit counts
        result['MATCHED_C_S1_COUNT'] = credit_count[('S1', 'MATCHED')]
        result['UNMATCHED_C_S1_COUNT'] = credit_count[('S1', 'UNMATCHED')]
        result['MATCHED_C_S2_COUNT'] = credit_count[('S2', 'MATCHED')]
        result['UNMATCHED_C_S2_COUNT'] = credit_count[('S2', 'UNMATCHED')]

        # adding debit counts
        result['MATCHED_D_S1_COUNT'] = debit_count[('S1', 'MATCHED')]
        result['UNMATCHED_D_S1_COUNT'] = debit_count[('S1', 'UNMATCHED')]
        result['MATCHED_D_S2_COUNT'] = debit_count[('S2', 'MATCHED')]
        result['UNMATCHED_D_S2_COUNT'] = debit_count[('S2', 'UNMATCHED')]

        # AMOUNTS
        credit_amount = df['AMOUNTC'].to_dict()
        result['MATCHED_C_S1_AMOUNT'] = credit_amount[('S1', 'MATCHED')]
        result['UNMATCHED_C_S1_AMOUNT'] = credit_amount[('S1', 'UNMATCHED')]
        result['MATCHED_C_S2_AMOUNT'] = credit_amount[('S2', 'MATCHED')]
        result['UNMATCHED_C_S2_AMOUNT'] = credit_amount[('S2', 'UNMATCHED')]

        debit_amount = df['AMOUNTD'].to_dict()
        result['MATCHED_D_S1_AMOUNT'] = debit_amount[('S1', 'MATCHED')]
        result['UNMATCHED_D_S1_AMOUNT'] = debit_amount[('S1', 'UNMATCHED')]
        result['MATCHED_D_S2_AMOUNT'] = debit_amount[('S2', 'MATCHED')]
        result['UNMATCHED_D_S2_AMOUNT'] = debit_amount[('S2', 'UNMATCHED')]

        result['CLOSING_BALANCE'] = data['data'][0]['CLOSING_BALANCE']
        result['OPENING_BALANCE'] = data['data'][0]['OPENING_BALANCE']
        result['S1_TOTAL_COUNT'] = result['MATCHED_C_S1_COUNT'] + result['UNMATCHED_C_S1_COUNT'] + result[
            'MATCHED_D_S1_COUNT'] + result['UNMATCHED_D_S1_COUNT']
        result['S2_TOTAL_COUNT'] = result['MATCHED_C_S2_COUNT'] + result['UNMATCHED_C_S2_COUNT'] + result[
            'MATCHED_D_S2_COUNT'] + result['UNMATCHED_D_S2_COUNT']

        return True, result


class ReportInterface():
    def __init__(self):
        # self.ip="192.168.2.129"
        # self.port=1521
        # self.sid='xe'
        # self.oradsn=cx_Oracle.makedsn(self.ip, self.port, self.sid)
        # self.oraconnection=cx_Oracle.connect('algoreconutil_dev', 'algorecon', self.oradsn)
        # self.metadata={"tree":["BUSINESS_CONTEXT_ID","RECON_ID","SOURCE_TYPE","RECONCILIATION_STATUS"],"aggregation":["COUNT","CREDIT","DEBIT","NETOUTSTANDINGAMOUNT"]}
        # self.df=None
        self.statesdf = pandas.read_csv("meta/states.csv")

    # Get CustomReport For report
    def getCustomReport(self, id, query):
        doc = customReportGenerate().find_one({'reconId': query['reconId'], 'selectedFilter': query['selectedFilter']})
        if doc:
            status, path = eval(doc['customScript'])
            return status, path
        else:
            return True, ''

    # Check Session File Exist
    def checkSessionFolder(self, fileName):
        object_id = str(flask.session["sessionData"]['_id'])
        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)
        return object_id

    # SUSPENSE ATM Management Report
    def SUSP_61167(self, id):
        (status, connection) = sql.getConnection()

        if status:
            exec_id = sql.getLatestExecutionID(connection, 'SUSP_CASH_APAC_61167')
            qry2 = "select * from cash_output_txn where recon_id ='SUSP_CASH_APAC_61167' and record_status= 'ACTIVE' and RECON_EXECUTION_ID = " + exec_id

            qry1 = {'recon_id': 'SUSP_CASH_APAC_61167'}
            df = pandas.read_sql(qry2, connection)

            # Recon Data is reading here to get columns in ag-grid order From sql.read_query function
            (status, recon_data) = sql.read_query(connection, qry1)
            recon_data.rename(
                columns={"FREE_TEXT_1": "Unit Name", "FREE_TEXT_2": "Recon Remarks", "FREE_TEXT_3": "Ops Remarks"},
                inplace=True)
            unmatched = recon_data[recon_data['MATCHING_STATUS'] == 'UNMATCHED']
            unmatched['AGEING'] = unmatched['STATEMENT_DATE'].apply(lambda x: datetime.datetime.now() - x).dt.days

            df1 = df[df['MATCHING_STATUS'] == 'UNMATCHED']
            df2 = df[df['ENTRY_TYPE'] == 'P']
            logger.info(df1.head())

            master = pandas.read_csv('meta/ATMMaster.csv', converters={'ACCOUNT_NUMBER': str})

            df1['AMOUNT'] = df1.loc[:].apply(
                lambda x: -x['AMOUNT'] if x['DEBIT_CREDIT_INDICATOR'] == 'D' else x['AMOUNT'], axis=1)

            df2['CLOSING_BALANCE_AMOUNT'] = df2.loc[:].apply(
                lambda x: -x['AMOUNT'] if x['DEBIT_CREDIT_INDICATOR'] == 'D' else x['CLOSING_BALANCE_AMOUNT'], axis=1)

            dfg1 = df1.groupby(['ACCOUNT_NUMBER']).agg({'VALUE_DATE': 'min', 'AMOUNT': 'sum'}).reset_index()

            # dfg1['VALUE_DATE'] = dfg1['VALUE_DATE'].apply(lambda x: datetime.datetime.strptime(str(x), '%d-%m-%Y') if x != '' else '')
            dfg1['AGEING'] = dfg1['VALUE_DATE'].apply(lambda x: datetime.datetime.now() - x).dt.days

            df2 = df2[['ACCOUNT_NUMBER', 'CLOSING_BALANCE_AMOUNT', 'CLOSING_DEBIT_CREDIT_INDICATOR']]
            df2['CLOSING_BALANCE_AMOUNT'] = df2.loc[:].apply(
                lambda x: -x['CLOSING_BALANCE_AMOUNT'] if x['CLOSING_DEBIT_CREDIT_INDICATOR'] == 'DR' else x[
                    'CLOSING_BALANCE_AMOUNT'], axis=1)

            merged = df2.merge(dfg1, on=['ACCOUNT_NUMBER'], how="outer", indicator=True, suffixes=('_x', ''))
            merged['DIFFERENCE'] = merged['CLOSING_BALANCE_AMOUNT'] - merged['AMOUNT']

            del merged['_merge']

            merged.fillna(0, inplace=True)

            merged['ACCOUNT_NUMBER'] = merged['ACCOUNT_NUMBER'].apply(lambda x: x.lstrip('0'))
            merged = merged.merge(master, on=['ACCOUNT_NUMBER'], how='left')

            merged = merged[['Branch Code', 'Branch Name', 'ACCOUNT_NUMBER', 'LEDGER_NAME',
                             'CLOSING_BALANCE_AMOUNT',
                             'AMOUNT', 'DIFFERENCE', 'AGEING']]

            object_id = self.checkSessionFolder('SUSP_CASH_APAC_61167' + '*.xlsx')

            # to_date = datetime.datetime.now().strftime("%b-%d-%Y")
            export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
            fileName = 'SUSP_CASH_APAC_61167' + '-' + '.xlsx'

            # Formating Amount Columns
            unmatched['Amount'] = unmatched['Amount'].str.replace(',', '').astype(float)

            merged.sort_values(by=['AGEING'], ascending=False, inplace=True)
            merged.reset_index(drop=True, inplace=True)
            merged.insert(0, 'Sr.No', merged.index + 1)

            writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                        engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')
            merged.replace(r'[^\x00-\x7F]+', '', inplace=True, regex=True)
            merged.to_excel(writer, 'BGL Balance Report', index=False)

            header = pandas.DataFrame(
                [['Recon_Name :', 'ATM Management BGL Suspense'], ['Recon_ID', 'SUSP_CASH_APAC_61167'],
                 ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']],
                 ['Status', 'UnMathched'],
                 ['', '']])

            header.to_excel(writer, 'Unmatched Exceptions', startrow=0, startcol=0, index=False, header=None)
            unmatched.to_excel(writer, 'Unmatched Exceptions', startrow=7, startcol=0, index=False)

            writer.save()

            merged.loc[merged['AMOUNT'].isnull(), 'AMOUNT'] = '0'
            merged.loc[merged['DIFFERENCE'].isnull(), 'DIFFERENCE'] = '0'

            return True, object_id + '/' + fileName

    def DSB_report(self, id):
        (status, connection) = sql.getConnection()
        if status:
            exec_id = sql.getLatestExecutionID(connection, 'DSB_CASH_APAC_61179')

            (status_rec, stmtdate) = sql.get_max_execution_id_statement_date(connection, 'DSB_CASH_APAC_61179')
            qry2 = "select * from cash_output_txn where recon_id ='DSB_CASH_APAC_61179' and record_status= 'ACTIVE' and RECON_EXECUTION_ID = " + exec_id

            qry1 = {'recon_id': 'DSB_CASH_APAC_61179'}
            df = pandas.read_sql(qry2, connection)
            (status, recon_data) = sql.read_query(connection, qry1)
            recon_data.rename(
                columns={"FREE_TEXT_1": "Unit Name", "FREE_TEXT_2": "Recon Remarks", "FREE_TEXT_3": "Ops Remarks"},
                inplace=True)
            unmatched = recon_data[recon_data['MATCHING_STATUS'] == 'UNMATCHED']
            unmatched['Net Amount'] = ''
            unmatched['CCY'] = 'INR'
            unmatched['Recon Type'] = 'Transaction'
            unmatched.rename(columns={'Account Number': 'GL/BGL'}, inplace=True)
            unmatched['GL/BGL'] = unmatched['GL/BGL'].apply(lambda x: x.lstrip('0'))
            unmatched['Amount'] = unmatched['Amount'].apply(lambda x: x.replace(',', ''))
            unmatched['Amount'] = unmatched['Amount'].fillna(0).astype(np.float64)
            unmatched['Unit'] = 'CLEARING, CASH, DSB'
            unmatched['AGEING'] = unmatched['STATEMENT_DATE'].apply(lambda x: datetime.datetime.now() - x).dt.days
            unmatched.loc[unmatched['Debit Credit Indicator'] == 'C', 'Net Amount'] = unmatched['Amount']
            unmatched.loc[unmatched['Debit Credit Indicator'] == 'D', 'Net Amount'] = unmatched['Amount'] * -1

            unmatched.loc[unmatched['GL/BGL'] == '98372401013', 'Recon Name'] = 'DSB Control HDFC Bank A/C'
            unmatched.loc[unmatched['GL/BGL'] == '99091401013', 'Recon Name'] = 'DSB Control IndusInd Bank Account'
            unmatched.loc[unmatched['GL/BGL'] == '99057401014', 'Recon Name'] = 'DSB Control ICICI Bank A/C'

            unmatched = unmatched[
                ["Recon Type", "GL/BGL", "Recon Name", "Tran Date", "Val Date", "CCY", "Debit Credit Indicator",
                 "Amount",
                 "Net Amount", "Trace Number", "Narrative", "AGEING", "Unit", "Unit Name", "Recon Remarks",
                 "Ops Remarks"]]

            unmatched1 = unmatched.groupby('GL/BGL')['Amount'].sum().reset_index()
            # unmatched1.groupby('GL/BGL')['AGEING'].max()
            unmatched1.rename(columns={'Amount': 'Exceptions Amount'}, inplace=True)
            unmatch = unmatched.groupby('GL/BGL')['AGEING'].max().reset_index()
            newdf1 = pandas.merge(unmatch, unmatched1, on='GL/BGL')
            newdf1['GL/BGL'] = newdf1['GL/BGL'].astype(str)
            newdf1.rename(columns={'Amount': 'Exceptions Amount'}, inplace=True)

            flows = list(ClosingBalance().find(
                {"ACCOUNT_NUMBER": {"$in": ["0000099091401013", "0000098372401013", "0000099057401014"]},
                 'STATEMENT_DATE': stmtdate}))

            if len(flows) > 0:
                tmpdf = pandas.DataFrame(list(flows))
                tmpdf.drop(['_id'], axis=1, inplace=True)
                # tmpdf['AGEING'] = tmpdf['STATEMENT_DATE'].apply(lambda x: datetime.datetime.now() - x).dt.days
                tmpdf['ACCOUNT_NUMBER'] = tmpdf['ACCOUNT_NUMBER'].apply(lambda x: x.lstrip('0'))
                tmpdf.loc[tmpdf['ACCOUNT_NUMBER'] == '98372401013', 'LEDGER_NAME'] = 'DSB Control HDFC Bank A/C'
                tmpdf.loc[tmpdf['ACCOUNT_NUMBER'] == '99091401013', 'LEDGER_NAME'] = 'DSB Control IndusInd Bank Account'
                tmpdf.loc[tmpdf['ACCOUNT_NUMBER'] == '99057401014', 'LEDGER_NAME'] = 'DSB Control ICICI Bank A/C'
                newdf = tmpdf.rename(columns={'CLOSING_BALANCE_AMOUNT': 'BGL Balance'})

                newdf1['GL/BGL'] = newdf1['GL/BGL'].str.strip()
                newdf['ACCOUNT_NUMBER'] = newdf['ACCOUNT_NUMBER'].str.strip()

                finaldf = newdf1.merge(newdf, indicator=True, how='right', left_on='GL/BGL', right_on='ACCOUNT_NUMBER')
                finaldf['DIFFERENCE'] = newdf1['Exceptions Amount'].sub(newdf['BGL Balance'])

                finaldf.drop(['STATEMENT_DATE'], axis=1, inplace=True)
                finaldf = finaldf[
                    ['ACCOUNT_NUMBER', 'LEDGER_NAME', 'BGL Balance', 'Exceptions Amount', 'DIFFERENCE', 'AGEING']]
                finaldf.fillna(0, inplace=True)
            object_id = self.checkSessionFolder('DSB_CASH_APAC_61179' + '*.xlsx')
            # export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
            fileName = 'DSB_CASH_APAC_61179' + '-' + '.xlsx'
            writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                        engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')
            finaldf.to_excel(writer, 'Summary', index=False)
            unmatched.to_excel(writer, 'CBS Extract', index=False)
            writer.save()
            return True, object_id + '/' + fileName

    # rbidad ogl report
    def RBI_20521(self, id):

        (status, connection) = sql.getConnection()
        if status:
            exec_id = sql.getLatestExecutionID(connection, 'RBIDAD_CASH_APAC_20521')
            qry1 = {'recon_id': 'RBIDAD_CASH_APAC_20521'}

            (status, recon_data) = sql.read_query(connection, qry1)
            df = recon_data[recon_data['MATCHING_STATUS'] == 'UNMATCHED']
            df.rename(columns={
                "FREE_TEXT_1": "Unit Name",
                "FREE_TEXT_2": "Recon Remarks",
                "FREE_TEXT_3": "Ops Remarks"
            }, inplace=True)

            df['AGEING'] = df['STATEMENT_DATE'].apply(lambda row: datetime.datetime.now() - row).dt.days
            df = df[df['MATCHING_STATUS'] == 'UNMATCHED']
            df['AMOUNT'] = df['AMOUNT'].str.replace(',','')
            df['AMOUNT'] = df['AMOUNT'].fillna(0).astype(np.float64)

            dadMirrorDf = df[['POSTED DATE', 'DR/CR', 'AMOUNT', 'NARRATION', 'ACCOUNT NUMBER',
                              'TRACE NO', 'Value Date', 'SOURCE_TYPE', 'SOURCE_NAME', 'ACCOUNT_NAME', 'LINK_ID',
                              'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME', 'CREATED_BY', 'CREATED_DATE',
                              'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE', 'MATCHING_EXECUTION_STATUS',
                              'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS',
                              'FORCEMATCH_AUTHORIZATION', 'REASON_CODE', 'INVESTIGATED_BY', 'RESOLUTION_COMMENTS',
                              'INVESTIGATOR_COMMENTS', 'UPDATED_BY', 'Unit Name', 'Recon Remarks', 'Ops Remarks',
                              'AGEING']]

            export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
            date = df.sort_values(by='STATEMENT_DATE')['STATEMENT_DATE'].iloc[-1]
            stmtdate = datetime.datetime.strftime(date, "%Y/%m/%d")
            statementDate = datetime.datetime.strftime(date, "%d%m%Y")
            date = datetime.datetime.strftime(date, "%d/%m/%Y")

            account_number = '0000098026102018'
            feedPath = config.flaskPath + 'rbi_dad_report'

            # Read File For RBIDATA
            fpattern = "RBIDAD_" + statementDate
            files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
            if len(files) > 1: return False, "Found duplicate files"
            if len(files) == 0: return False, "No data found"
            rbi_feed_name = files[0]
            rbi_dad_df = pandas.read_excel(feedPath + os.sep + rbi_feed_name, skiprows=14)

            rbiDADAccClosingBal = rbi_dad_df[rbi_dad_df['R.O'] == 'Closing Balance']['Balance (INR)'].tolist()[-1]

            # Read File For OGL
            fpattern = "TRIALBAL_" + statementDate
            files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
            if len(files) > 1: return False, "Found duplicate files"
            if len(files) == 0: return False, "No data found"
            ogl_feed_name = files[0]
            ogl_df = pandas.read_csv(feedPath + os.sep + ogl_feed_name, sep='|', error_bad_lines=False)

            try:
                ogl_source_col = ogl_df.columns[0]  # 'SOURCE'  # ogl_df.columns[0]
                ogl_acc_no__col = ogl_df.columns[1]  # 'GL_NO'  # ogl_df.columns[1]
                ogl_ind_col = ogl_df.columns[5]  # 'DEBIT_CREDIT_FLAG'  # ogl_df.columns[5]
                ogl_balance__col = ogl_df.columns[6]  # 'BALANCE_AMT'  # ogl_df.columns[6]

                ogl_dfConditionDf = ogl_df.loc[
                    (ogl_df[ogl_source_col] == 'CBS') & (ogl_df[ogl_acc_no__col].str.contains('41201'))]

                # ogl_dfConditionDf = ogl_dfConditionDf.loc[
                #    ogl_dfConditionDf[ogl_acc_no__col].apply(
                #        lambda x: True if len(str(x).split('41201', 3)[0]) else False)]
                ogl_dfConditionDf = ogl_dfConditionDf.loc[(ogl_df[ogl_acc_no__col].str[12:17] == '41201')]

                if len(ogl_dfConditionDf):
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf[ogl_balance__col].map(
                        lambda x: float("".join(re.split('\,', '%.2f' % x))))
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf.apply(
                        lambda x: x[ogl_balance__col] * -1 if x[ogl_ind_col] == 'CR' else x[ogl_balance__col], axis=1)
                    ogl_closing_balance = sum(ogl_dfConditionDf[ogl_balance__col])
                    # ogl_closing_balance = round(
                    #    sum(ogl_dfConditionDf[ogl_balance__col].map(lambda x: float("".join(re.split('\,', str(x)))))),
                    #    2)
            except Exception, e:
                return False, str(e)

            # rbimirror closing balance from closing_balance_data collection of db erecon_idfc
            records = ClosingBalance().find_one({'ACCOUNT_NUMBER': account_number, 'STATEMENT_DATE': stmtdate})
            if records:
                rbiMirrorAccClosingBal = records["CLOSING_BALANCE_AMOUNT"]
            else:
                rbiMirrorAccClosingBal = 0

            # total debit and credit
            total_dr_bal = df[df['DR/CR']=='D']['AMOUNT'].sum()
            total_cr_bal = df[df['DR/CR']=='C']['AMOUNT'].sum()

            netClosingDifference = abs(rbiDADAccClosingBal - rbiMirrorAccClosingBal)

            rbiMirror_gl_Difference = abs(rbiMirrorAccClosingBal - ogl_closing_balance)

            open_exceptions_total = abs(total_dr_bal - total_cr_bal)

            netdifference = abs(netClosingDifference - open_exceptions_total)

            res = pandas.DataFrame(columns=['Description', 'Difference'])

            res['Description'] = ['Recon Date','Particulars', 'RBI Mirror Account Closing balance', 'RBI DAD Account Closing Balance',
                                 'Net Difference between RBI Mirror and RBI DAD',
                                 'OGL Balance', 'Difference between RBI Mirror and OGL', 'Open Exceptions Total',
                                 'Net Difference']

            res.loc[res['Description'] == 'Recon Date', 'Difference'] = date
            res.loc[res['Description'] == 'Particulars', 'Difference'] = 'Amount'
            res.loc[res['Description'] == 'RBI Mirror Account Closing balance', 'Difference'] = rbiMirrorAccClosingBal
            res.loc[res['Description'] == 'RBI DAD Account Closing Balance', 'Difference'] = rbiDADAccClosingBal
            res.loc[res['Description'] == 'Net Difference between RBI Mirror and RBI DAD', 'Difference'] = netClosingDifference
            res.loc[res['Description'] == 'OGL Balance', 'Difference'] = ogl_closing_balance
            res.loc[res['Description'] == 'Difference between RBI Mirror and OGL', 'Difference'] = rbiMirror_gl_Difference
            res.loc[res['Description'] == 'Open Exceptions Total', 'Difference'] = open_exceptions_total
            res.loc[res['Description'] == 'Net Difference', 'Difference'] = netdifference

            object_id = self.checkSessionFolder('RBIDAD_CASH_APAC_20521' + '*.xlsx')
            fileName = 'RBIDAD_CASH_APAC_20521' + '-' + '.xlsx'

            header = pandas.DataFrame(['RBI DAD BALANCE REPORT'])
            header1 = pandas.DataFrame(
                [['Recon_Name :', 'RBI DAD Vs RBI Mirror Account'], ['Recon_ID', 'RBIDAD_CASH_APAC_20521'],
                 ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']],
                 ['Status', 'UnMathched'],
                 ['', '']])

            writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                        engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')

            header.to_excel(writer, 'RBI DAD Balance Report', startcol=0, startrow=0, index=False, header=None)
            res.to_excel(writer, sheet_name='RBI DAD Balance Report', startcol=0, startrow=1, index=False, header=None)

            header1.to_excel(writer, 'RBI DAD Vs RBI Mirror Recon-RBI', startcol=0, startrow=0, index=False, header=None)
            dadMirrorDf.to_excel(writer, sheet_name='RBI DAD Vs RBI Mirror Recon-RBI', startcol=0, startrow=7, index=False)

            writer.save()
            return True, object_id + '/' + fileName


    def readFiles(self,file, sep='|'):
        directory = config.contentDir + '/Adf_OGL_report_Files/'
        if file.split('.')[1] in ['xls', 'xlsx']:
            df = pandas.read_excel(directory + file)
        elif file.split('.')[1] in ['txt', 'csv']:
            df = pandas.read_csv(directory + file, sep=sep, low_memory=False)
        return df

    def CBAL_61177(self, id,stmtDate):
        print(stmtDate)
        stmtDate = datetime.datetime.strptime(stmtDate, '%d/%m/%y')

        feedDef = {'C40_TRIAL_BAL': 'C40_TRIAL_BALANCE_#%Y%m%d#_[0-9]{6}.txt',
                   'TRIALBAL_report': 'TRIALBAL_WBO_#%d%m%Y#_[0-9]{6}.txt'}
        files = []

        for fileName in feedDef:
            if "#" in feedDef[fileName]:
                fpattern = feedDef[fileName]
                indx1 = fpattern.index('#')
                indx2 = fpattern.index('#', indx1 + 1)
                dpattern = fpattern[indx1 + 1:indx2]

                dstr = stmtDate.strftime(dpattern)
                fpattern = fpattern.replace('#' + dpattern + '#', dstr)
                directory = config.contentDir + '/cbal_reports'
                file = [f for f in os.listdir(directory) if re.search(fpattern, f)]
                files.extend(file)
        s1=pandas.DataFrame()
        s2=pandas.DataFrame()
        df4=pandas.DataFrame()
        doc=CBS_OGLreport().find_one({"stmtDate": stmtDate})
        if doc:
            print("inside mongo")
            df4 = pandas.DataFrame(doc["report"])
            df4 = df4[
                ["Natural A/c", "C_40 Trial Balance(CBS)", "OGL Trial Balance(CBS)", "Description", "Type of Balance",
                 "Ownership", ]]

        if len(files)==2:
            CBS_OGLreport().delete({"stmtDate": stmtDate})
            for file in files:
                if file.startswith('C40'):
                    s1 = self.readFiles(file)
                else:
                    s2 = self.readFiles(file)

            s1.columns = s1.columns.str.strip()
            s2.columns = s2.columns.str.strip()

            s1["AMOUNT"]=pandas.to_numeric(s1["AMOUNT"], errors='coerce')
            s2["BALANCE_AMT"] = pandas.to_numeric(s2["BALANCE_AMT"], errors='coerce')

            G = s1.groupby("NATUR", as_index=False)["AMOUNT"].sum()

            s2["NATUR"] = s2["GL_NO"].str[12:17]
            G1 = s2.groupby("NATUR", as_index=False)["BALANCE_AMT"].sum()

            df2 = pandas.merge(G, G1, on="NATUR", how="outer")

            df2.rename(columns={'AMOUNT': 'C_40 Trial Balance(CBS)', "BALANCE_AMT": "OGL Trial Balance(CBS)",
                                'NATUR': 'Natural A/c'}, inplace=True)
            df3 = pandas.read_excel(config.contentDir + "/description.xls")
            df3.rename(columns={'GL Code': 'Natural A/c'}, inplace=True)
            df3['Natural A/c'] = df3['Natural A/c'].astype(str)
            df3 = df3.ffill()

            df4 = pandas.merge(df2, df3, on="Natural A/c")

            df5 = {}
            df5["stmtDate"] = stmtDate
            df5["report"] = df4.to_dict(orient='records')
            CBS_OGLreport().insert(df5)
            for file in files:
                os.remove(config.contentDir+'/Adf_OGL_report_Files/'+file)
        elif len(files)==1 or (len(files)==0 and len(df4)==0):
            return False,"Numbe of files found "+str(len(files))

        fileName = 'CBS_OGL_REPORT' + '-' + str(stmtDate) + '.csv'
        object_id = self.checkSessionFolder(fileName)
        df4 = df4.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
        df4.to_csv(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName, index=False)
        data = {'fileName':object_id + '/' + fileName}
        return data


    # Load TrialBal File
    def LOAD_TRIALWBO(self):
        OglData = pandas.DataFrame()
        # EGL DATE FROM TRIALBAL_WBO FILE
        headers = ['SOURCE', 'GL_NO', 'SL_NO', 'BALANCE_DATE', 'GL_CCY', 'BALANCE_AMT', 'BALANCE_AMT_INR', 'SOURCE_SYS',
                   'EFFECTIVE_FROM_DTTM', 'EFFECTIVE_TO_DTTM', 'EXTRACT_DATE', 'BATCH_ID', 'BATCH_DATE', 'EGL']
        feedPath = config.uploadDir
        fpattern = 'TRIALBAL_WBO_*.*'
        files = [f for f in os.listdir(feedPath) if re.search(fpattern, f)]
        if len(files) != 0:
            for filen in files:
                loadFile = feedPath + os.sep + filen
                df1 = pandas.read_csv(loadFile, sep='|', skipfooter=1, skiprows=1, converters={'BALANCE_DATE': str},
                                      names=headers, header=None, engine='python')
                if len(df1) > 0:
                    OglData = pandas.concat([OglData, df1], ignore_index=True)
        return OglData

    # Deal Utilisation Report
    def TRTD_61169(self, id):
        logger.info('hi this is comes')
        try:
            (status, connection) = sql.getConnection()
            # qry2 = "select * from cash_output_txn where recon_id ='TRTD_CASH_APAC_61169' and record_status= 'ACTIVE' and RECON_EXECUTION_ID = " + exec_id

            qry1 = {'recon_id': 'TRTD_CASH_APAC_61169', 'suspenseFlag': False}
            # df = pandas.read_sql(qry2, connection)

            # Recon Data is reading here to get columns in ag-grid order From sql.read_query function
            (status, recon_data) = sql.read_col_query_unmatched(connection, qry1)
            recon_data.rename(
                columns={"FREE_TEXT_1": "Unit Name", "FREE_TEXT_2": "Recon Remarks", "FREE_TEXT_3": "Ops Remarks"},
                inplace=True)

            status, jsonDf = ReconAssignment().getReconColData('dummy', qry1)
            jsonDf = json.loads(jsonDf)
            # logger.info(pandas.to_dict(jsonDf, orient='records'))
            for col in jsonDf:
                logger.info(col)
                recon_data.rename(columns={col['MDL_FIELD_ID']: col['UI_DISPLAY_NAME']}, inplace=True)
            source1 = recon_data[recon_data['SOURCE_NAME'] == 'Misys']
            source2 = recon_data[recon_data['SOURCE_NAME'] == 'K+']
            # sourceGroup = source1.groupby(['REF_NUMBER','CURRENCY']).count().reset_index()
            # print sourceGroup.columns

            df2g = source2.groupby(['DEAL_ID', 'CURRENCY'])['SOURCE_NAME'].count().reset_index()
            df1g = source1.groupby(['DEAL_ID', 'CURRENCY'])['SOURCE_NAME'].count().reset_index()

            df1g.rename(columns={'SOURCE_NAME': 'COUNT'}, inplace=True)
            df2g.rename(columns={'SOURCE_NAME': 'COUNT'}, inplace=True)

            df1 = source1.merge(df1g[['DEAL_ID', 'CURRENCY', 'COUNT']], on=['DEAL_ID', 'CURRENCY'], how='left',
                                suffixes=('', '_x'))
            df2 = source2.merge(df2g[['DEAL_ID', 'CURRENCY', 'COUNT']], on=['DEAL_ID', 'CURRENCY'], how='left',
                                suffixes=('', '_x'))

            oneDF1 = df1[df1['COUNT'] == 1]
            manyDF1 = df1[df1['COUNT'] != 1]

            oneDF2 = df2[df2['COUNT'] == 1]
            manyDF2 = df2[df2['COUNT'] != 1]

            manyDF2.sort_values(by=['DEAL_ID', 'CURRENCY'], ascending=True)
            manyDF1.sort_values(by=['DEAL_ID', 'CURRENCY'], ascending=True)

            merged = oneDF1.merge(oneDF2, on=['DEAL_ID', 'CURRENCY'], indicator=True, how='outer',
                                  suffixes=('', '_x'))

            col = {}
            merged = pandas.concat([merged, manyDF1], join_axes=[merged.columns])
            if len(manyDF2) > 0:
                for i in manyDF2.columns:
                    if i not in ['DEAL_ID', 'CURRENCY'] and i != None:
                        col[i] = i + '_x'

                manyDF2.rename(columns=col, inplace=True)
            merged = pandas.concat([merged, manyDF2], join_axes=[merged.columns])

            mappedCol = {}
            for col in df1.columns:
                if col != None and col not in ['DEAL_ID', 'CURRENCY']:
                    mappedCol[col] = col + "_Trade"
            for col in df2.columns:
                if col != None and col not in ['DEAL_ID', 'CURRENCY']:
                    mappedCol[col + '_x'] = col + '_Treasury'

            merged.rename(columns=mappedCol, inplace=True)

            # merged = merged.sort_values(by="_merge", ascending=False)
            merged.sort_values(by=['DEAL_ID', 'CURRENCY'], ascending=True, inplace=True)

            object_id = self.checkSessionFolder('TRTD_CASH_APAC_61169' + '*.xlsx')

            # to_date = datetime.datetime.now().strftime("%b-%d-%Y")
            export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
            fileName = 'TRTD_CASH_APAC_61169' + '-' + '.xlsx'

            #
            writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                        engine='xlsxwriter', datetime_format='DD-MM-YYYY', date_format='DD-MM-YYYY')
            # merged.replace(r'[^\x00-\x7F]+', '', inplace=True, regex=True)
            #
            merged = merged[config.TRTD_COLS]
            header = pandas.DataFrame(
                [['Recon_Name :', 'Deals utilisation recon (Trade V/s Treasury)'], ['Recon_ID', 'TRTD_CASH_APAC_61169'],
                 ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']],
                 ['Status', 'UnMathched'],
                 ['', '']])
            #
            header.to_excel(writer, 'Unmatched Exceptions', startrow=0, startcol=0, index=False, header=None)
            merged.to_excel(writer, 'Unmatched Exceptions', startrow=7, startcol=0, index=False)
            #
            writer.save()
            return True, object_id + '/' + fileName

        except Exception as e:
            logger.info(traceback.format_exc())
            logger.info(e)

    # LMS CONTROL RECON REPORT
    def LMS_REPORTS(self, id, reconId, naturalAcc, account_numbers, reconName):
        try:
            OGL_bal_sum = 0
            Bgl_bal_sum = 0
            OglData = self.LOAD_TRIALWBO()
            if len(OglData):
                OglData = OglData[(OglData['SOURCE'] == 'CBS') & (OglData['GL_NO'].str.contains(naturalAcc))]
                OglData['CLOSING_BALANCE'] = OglData["BALANCE_AMT_INR"]
                OglData['Natural Account'] = naturalAcc
                OGL_bal_sum = OglData['CLOSING_BALANCE'].sum()

            (status, connection) = sql.getConnection()
            if status:
                exec_id = sql.getLatestExecutionID(connection, reconId)
                (status_rec, stmtdate) = sql.get_max_execution_id_statement_date(connection, reconId)
                qry = "select * from cash_output_txn where record_status = 'ACTIVE' and matching_status='UNMATCHED' and recon_id ='" + reconId + "' and RECON_EXECUTION_ID = " + exec_id
                cbsData = pandas.read_sql(qry, connection)
                credit_trnx = cbsData[cbsData["DEBIT_CREDIT_INDICATOR"] == 'C']
                debit_trnx = cbsData[cbsData["DEBIT_CREDIT_INDICATOR"] == 'D']

                credit_sum = credit_trnx['AMOUNT'].sum()
                debit_sum = debit_trnx['AMOUNT'].sum()
                credit_debit_diff = credit_sum - debit_sum

                # closing balance amount of newCollection table from db erecon
                bglAcc = {}
                BGL_bal_sum = 0
                for acc_no in account_numbers:
                    records = ClosingBalance().find_one({'ACCOUNT_NUMBER': acc_no, 'STATEMENT_DATE': stmtdate})
                    if records:
                        bglAcc[acc_no] = records["CLOSING_BALANCE_AMOUNT"]
                        BGL_bal_sum += records["CLOSING_BALANCE_AMOUNT"]
                    else:
                        bglAcc[acc_no] = 0
                        BGL_bal_sum += 0

                object_id = self.checkSessionFolder(reconId + '*.xlsx')
                # Creating xlsx for report
                export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
                fileName = reconId + '-' + '.xlsx'
                writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                            engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')

                # create a header dataframe for recon creation details
                header = pandas.DataFrame(
                    [['Recon As On:-', stmtdate], ['Recon Name:-', reconName],
                     ['OGL Natural Account:-', naturalAcc], ['CBS BGL Account:-', ','.join(account_numbers)],
                     ['userName', flask.session["sessionData"]['userName']]
                     ])

                debitHead = pandas.DataFrame(['Debit in CBS'])
                creditHead = pandas.DataFrame(['Credit in CBS'])
                # create a report dataframe
                report_df = pandas.DataFrame(
                    columns=['Serial Number', 'Date', 'Description', 'ACCOUNT_NUMBER', 'TRACE_NUM', 'AMOUNT',
                             'SUM_AMOUNT',
                             'Recon Remark', 'Ops Remarks', 'AGEING'])
                report_df['Description'] = ('BALANCE AS PER OGL',
                                            'BALANCE AS PER BGL',
                                            'Difference')

                OGL_BGL_diff = OGL_bal_sum + BGL_bal_sum

                report_df.loc[
                    report_df['Description'] == 'BALANCE AS PER OGL', 'SUM_AMOUNT'] = OGL_bal_sum
                report_df.loc[
                    report_df['Description'] == 'BALANCE AS PER BGL', 'SUM_AMOUNT'] = BGL_bal_sum

                report_df.loc[report_df['Description'] == 'Difference', 'SUM_AMOUNT'] = OGL_BGL_diff

                # create debit dataframe
                logger.info(debit_trnx['ACCOUNT_NUMBER'])
                debit_df = debit_trnx[
                    ['VALUE_DATE', 'NARRATION', 'ACCOUNT_NUMBER', 'TXT_32_001', 'AMOUNT', 'FREE_TEXT_2', 'FREE_TEXT_3',
                     'STATEMENT_DATE']].copy()

                # logger.info(debit_df)
                if len(debit_df):
                    debit_df['AGEING'] = debit_df['STATEMENT_DATE'].apply(
                        lambda x: (datetime.datetime.now() - x)).dt.days
                    del debit_df['STATEMENT_DATE']

                debit_df.rename(index=str,
                                columns={'VALUE_DATE': 'Date', 'NARRATION': 'Description', 'TXT_32_001': 'TRACE_NUM',
                                         'FREE_TEXT_2': 'Recon Remarks', 'FREE_TEXT3': 'Ops Remarks',
                                         }, inplace=True)
                debit_df.insert(5, 'SUM_AMOUNT', '')

                # create debit_sum_df
                debitsum = pandas.DataFrame([['*Debit Transaction Sum', '', '', '', debit_sum]])
                creditsum = pandas.DataFrame([['*Credit Transaction Sum', '', '', '', credit_sum]])
                cd_diff_df = pandas.DataFrame(
                    [['*Difference of debitsum and creditsum', '', '', '', credit_debit_diff]])

                # create credit dataframe
                credit_df = credit_trnx[
                    ['VALUE_DATE', 'NARRATION', 'ACCOUNT_NUMBER', 'TXT_32_001', 'AMOUNT', 'FREE_TEXT_2', 'FREE_TEXT_3',
                     'STATEMENT_DATE']].copy()
                credit_df.rename(index=str,
                                 columns={'VALUE_DATE': 'Date', 'NARRATION': 'Description', 'TXT_32_001': 'TRACE_NUM',
                                          'FREE_TEXT_2': 'Recon Remarks', 'FREE_TEXT3': 'Ops Remarks',
                                          }, inplace=True)
                credit_df.insert(5, 'SUM_AMOUNT', '')
                if len(credit_df):
                    credit_df['AGEING'] = credit_df['STATEMENT_DATE'].apply(
                        lambda x: (datetime.datetime.now() - x)).dt.days
                    del credit_df['STATEMENT_DATE']
                # create a footer dataframe for exception description
                # BGL_bal_sum = 0
                value = (debit_sum - credit_sum) + BGL_bal_sum

                footer = pandas.DataFrame([['*Difference', '', '', '', value]])

                # concatenate header, dataframes and footer and save the result file
                header.to_excel(writer, 'Unmatched Exceptions', startrow=1, startcol=0, index=False, header=None)

                report_df.to_excel(writer, 'Unmatched Exceptions', startrow=len(header), startcol=0, index=False)

                debitHead.to_excel(writer, 'Unmatched Exceptions', startrow=len(header) + len(report_df) + 2,
                                   startcol=1,
                                   index=False, header=None)

                debit_df.to_excel(writer, 'Unmatched Exceptions', startrow=len(header) + len(report_df) + 4, startcol=1,
                                  index=False, header=None)

                debitsum.to_excel(writer, 'Unmatched Exceptions',
                                  startrow=len(debit_df) + len(header) + 4 + len(report_df),
                                  startcol=2, index=False, header=None)

                creditHead.to_excel(writer, 'Unmatched Exceptions',
                                    startrow=len(debit_df) + len(header) + len(report_df) + 6, startcol=1,
                                    index=False, header=None)

                credit_df.to_excel(writer, 'Unmatched Exceptions',
                                   startrow=len(debit_df) + len(header) + 6 + len(debitsum) + len(report_df),
                                   startcol=1,
                                   index=False, header=None)

                creditsum.to_excel(writer, 'Unmatched Exceptions',
                                   startrow=len(credit_df) + len(debit_df) + len(header) + 8 + len(debitsum) + len(
                                       report_df), startcol=2, index=False, header=None)

                # cd_diff_df.to_excel(writer, 'Unmatched Exceptions',
                #                   startrow=len(creditsum) + len(credit_df) + len(debit_df) + len(header) + 8 + len(
                #                      debitsum) + len(report_df), startcol=2, index=False, header=None)

                footer.to_excel(writer, 'Unmatched Exceptions',
                                startrow=len(cd_diff_df) + len(creditsum) + len(credit_df) + len(debit_df) + len(
                                    header) + 8 + len(debitsum) + len(report_df), startcol=2, index=False, header=None)

                writer.save()
                return True, object_id + '/' + fileName
        except Exception as e:
            logger.info(e)
            logger.info(traceback.format_exc())

    def swiftReconReports(self, id, reconId, reconName):
        try:
            status, connection = sql.getConnection()
            query = {}
            query['recon_id'] = reconId
            query['RECORD_STATUS'] = 'ACTIVE'
            query['MATCHING_STATUS'] = 'UNMATCHED'
            query['suspenseFlag'] = False
            (status_rec, df) = sql.read_col_query_unmatched(connection, query)
            df.rename(columns={
                "FREE_TEXT_1": "Unit Name",
                "FREE_TEXT_2": "Recon Remarks",
                "FREE_TEXT_3": "Ops Remarks",
                "TXT_10_003": "MTType"
            }, inplace=True)
            #
            # df['AGEING'] = df['STATEMENT_DATE'].apply(
            #     lambda row: datetime.datetime.now() - row)
            #
            df['AGEING BUCKET'] = ''

            df.loc[df['AGEING'] == 0, 'AGEING BUCKET'] = 'Today'
            df.loc[(df['AGEING'] == 1) | (df['AGEING'] == 2), 'AGEING BUCKET'] = 'Ageing of 1 & 2 days'
            df.loc[df['AGEING'] == 3, 'AGEING BUCKET'] = 'Ageing of 3 days'
            df.loc[df['AGEING'] >= 4, 'AGEING BUCKET'] = 'Ageing of 4 days & above'

            swiftRecords = df[df['TXT_180_005'] == 'Trade/Treasury Not Found']
            # swiftRecords = df[df['TXT_10_005']=='Swift']
            # swiftRecords = swiftRecords.loc[swiftRecords['MTType'].isin(['MT900', 'MT910'])]
            swiftRecords = swiftRecords.groupby(by=['MTType', 'AGEING BUCKET', 'Unit Name']).size().reset_index(
                name='counts')
            swiftRecords.fillna(0, inplace=True)
            pivot1 = pandas.pivot_table(swiftRecords, values='counts', index='AGEING BUCKET',
                                        columns=['Unit Name', 'MTType'],
                                        fill_value=0)

            swiftRecords = df[df['TXT_180_005'] == 'Not Found In Swift']
            swiftRecords = swiftRecords.groupby(by=['MTType', 'AGEING BUCKET', 'Unit Name']).size().reset_index(
                name='counts')
            swiftRecords.fillna(0, inplace=True)
            pivot2 = pandas.pivot_table(swiftRecords, values='counts', index='AGEING BUCKET',
                                        columns=['Unit Name', 'MTType'],
                                        fill_value=0)

            pivot3 = pandas.DataFrame()

            status, jsonDf = ReconAssignment().getReconColData('dummy', query)
            jsonDf = json.loads(jsonDf)

            for col in jsonDf:
                logger.info(col)
                df.rename(columns={col['MDL_FIELD_ID']: col['UI_DISPLAY_NAME']}, inplace=True)

            df = df[config.swiftRequiredCols]

            object_id = self.checkSessionFolder(reconId + '*.xlsx')

            # to_date = datetime.datetime.now().strftime("%b-%d-%Y")
            export_date_time = datetime.datetime.now().strftime("%A, %d- %B %Y %H:%M:%S")
            fileName = reconId + '-' + '.xlsx'

            #
            writer = pandas.ExcelWriter(config.contentDir + "/" + "recon_reports/" + object_id + '/' + fileName,
                                        engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')

            header = pandas.DataFrame(
                [['Recon_Name :', reconName], ['Recon_ID', reconId],
                 ['Date-Time', export_date_time], ['User', flask.session["sessionData"]['userName']],
                 ['Status', 'UnMathched'],
                 ['', '']])
            header1 = pandas.DataFrame([["Records Found only in Swift ", '']])
            header2 = pandas.DataFrame([["Records Found only Trade/Treasury", '']])

            header1.to_excel(writer, 'DASHBOARD', startrow=0, startcol=0, index=False, header=None)
            pivot1.to_excel(writer, 'DASHBOARD', startrow=2, startcol=0, index=True, header=True)
            header2.to_excel(writer, 'DASHBOARD', startrow=12, startcol=0, index=False, header=None)
            pivot2.to_excel(writer, 'DASHBOARD', startrow=14, startcol=0, index=True, header=True)

            header.to_excel(writer, 'EXCEPTIONS', startrow=0, startcol=0, index=False, header=None)
            # pivot3.to_excel(writer, 'DASHBOARD2', startrow=21, startcol=0, index=False, header=None)

            df.to_excel(writer, 'EXCEPTIONS', startrow=7, startcol=0, index=True, header=True)
            writer.save()
            return True, object_id + '/' + fileName
        except Exception as e:
            logger.info(e)
            logger.info(traceback.format_exc())
            return True, ''

    # dialy Nostro v/s OGL REPORT
    def nostroReport1(self, id, statementDate=''):
        # statementDate1 = datetime.datetime.strptime(statementDate, '%d/%m/%y')
        if config.rsyncNostroReportFile:
            rsync_cmd = 'rsync -avr ' + 'reconadmin@10.1.37.51:' + config.swiftDir + '/SwiftFile.csv ' + config.ereconSwift
            os.system(rsync_cmd)

        reportData = pandas.DataFrame()
        OglData = pandas.DataFrame()
        df1 = pandas.DataFrame()
        df2 = pandas.DataFrame()
	feedPath = config.uploadDir
        OglData = self.LOAD_TRIALWBO()

        # pandas.set_option('display.float_format',lambda x : '%.3f' %x)
        # Read Nostro report Records
        (status, connection) = sql.getConnection()
        if status:
            exec_id = sql.getLatestExecutionID(connection, 'TRSC_CASH_APAC_20181')
            # qry = "select *  from cash_output_txn where record_status = 'ACTIVE' and statement_date =to_date('" + statementDate + "','dd/mm/yy') and recon_id = 'TRSC_CASH_APAC_20181'"

            qry = "select *  from cash_output_txn where record_status = 'ACTIVE' and recon_id = 'TRSC_CASH_APAC_20181' and RECON_EXECUTION_ID = " + exec_id
            df2 = pandas.read_sql(qry, connection)

        reportData = pandas.read_csv(config.ereconSwift + '/SwiftFile.csv', sep=',')

        if len(OglData):
            # Drop row where both opening and closing balance are empty
            # Rename Column names to report specified names
            OglData.rename(
                columns={'GL_CCY': 'Closing_Currency',
                         'BALANCE_AMT': 'OGL_CLOSING_BALANCE',
                         'GL_NO': 'ACCOUNT_NUMBER'
                         }, inplace=True)

        logger.info(len(reportData))
        reconData = df2[df2['MATCHING_STATUS'] == 'UNMATCHED']
        logger.info(len(reconData))
        nostrodf = reportData
        ogldf = OglData

        ogldf['Closing_Currency'] = ogldf['Closing_Currency'].apply(lambda x: 'CNY' if x == 'CNH' else x)

        nostrodf['CLOSING_BALANCE_AMOUNT'] = nostrodf['CLOSING_BALANCE_AMOUNT'].apply(
            lambda x: NostroReport().formatStringToFloat(x))

        nostrodf['CLOSING_BALANCE_AMOUNT'] = nostrodf.apply(
            lambda x: -x['CLOSING_BALANCE_AMOUNT'] if x['CL BAL DR / CR'] == 'D' else x['CLOSING_BALANCE_AMOUNT'],
            axis=1)

        df1key = ['STATEMENT_DATE', 'ACCOUNT_NUMBER', 'Closing_Currency', 'CLOSING_BALANCE_AMOUNT']

        dfg1 = nostrodf[df1key].groupby(['Closing_Currency', 'ACCOUNT_NUMBER', 'STATEMENT_DATE']).sum().reset_index()

        dfg1['CLOSING_BALANCE_AMOUNT'] = dfg1['CLOSING_BALANCE_AMOUNT'].apply(
            lambda x: NostroReport().formatStringToFloat(x))
        dfg2 = dfg1.groupby('Closing_Currency').sum().reset_index()
        dfg1 = dfg1.merge(dfg2, on='Closing_Currency')

        for col in dfg1.columns:
            if col.endswith('_x'):
                dfg1.rename(columns={col: col.strip('_x')}, inplace=True)

        ogldf["BALANCE_DATE"] = pandas.to_datetime(ogldf["BALANCE_DATE"], format='%d%m%Y', errors='coerce')

        ogldf['ACCOUNT_NUMBER'] = ogldf['ACCOUNT_NUMBER'].apply(lambda x: x[12:17])

        ogldf = ogldf[ogldf['ACCOUNT_NUMBER'] == '42202']

        df2key = ['Closing_Currency', 'OGL_CLOSING_BALANCE', 'BALANCE_DATE']
        dfg2 = ogldf[df2key].groupby(['Closing_Currency', 'BALANCE_DATE']).sum().reset_index()

        pandas.set_option('display.float_format', lambda x: '%.3f' % x)
        merge = pandas.merge(dfg1, dfg2, how='outer', left_on='Closing_Currency', right_on='Closing_Currency',
                             indicator=True)

        del merge['_merge']

        merge['OGL Vs NOSTRO DIFFERENCE'] = merge['OGL_CLOSING_BALANCE'] - merge['CLOSING_BALANCE_AMOUNT_y']
        reconData['OPEN_EXCEPTION'] = reconData.apply(
            lambda x: x['AMOUNT'] if x['DEBIT_CREDIT_INDICATOR'] == 'D' else -x['AMOUNT'],
            axis=1)
        del merge['CLOSING_BALANCE_AMOUNT_y']
        exout = reconData[['CURRENCY', 'OPEN_EXCEPTION']].groupby(['CURRENCY']).sum().reset_index()

        merge.rename(columns={'Closing_Currency': 'CURRENCY'}, inplace=True)
        finalmerge = pandas.merge(merge, exout, how='outer', left_on=['CURRENCY'], right_on=['CURRENCY'])
        finalmerge = finalmerge.fillna(0)
        finalmerge['OPEN_EXCEPTION_DIFFERENCE'] = finalmerge['OGL Vs NOSTRO DIFFERENCE'] - finalmerge['OPEN_EXCEPTION']
        finalmerge['STATEMENT_DATE'] = finalmerge['STATEMENT_DATE'].astype(str)
        # finalmerge['STATEMENT_DATE'] = finalmerge['STATEMENT_DATE'].apply(
        #     lambda x: datetime.datetime.strftime(x, '%Y-%m-%d').datetime.strftime('%Y/%m/%d'))
        finalmerge['BALANCE_DATE'] = finalmerge['BALANCE_DATE'].astype(str)
        # finalmerge['BALANCE_DATE'] = finalmerge['BALANCE_DATE'].apply(
        #     lambda x: datetime.datetime.strftime(x, '%Y-%m-%d').datetime.strftime('%Y/%m/%d'))
        pandas.set_option('display.float_format', lambda x: '%.3f' % x)
        data = []
        doc = {}
        TdayNostro = {}

        for list in finalmerge.T.to_dict().values():
            data.append(list)

        data = pandas.DataFrame(data).fillna(0)

        if 'INR' in data['CURRENCY'].values:
            ind = data[data['CURRENCY'] == 'INR']
            data = data.drop(ind.index, axis=0)
            logger.info(data['CURRENCY'])

        df = pandas.DataFrame(data)
        df.to_csv(feedPath + '/NostroVsOglReport.csv', index=False)
        if not os.path.exists('/opt/NostroVsOglReport'):
            os.mkdir('/opt/NostroVsOglReport')
        df.to_csv('/opt/NostroVsOglReport/NostroVsOglReport.csv', index=False)
        span = {}

        listc = data['CURRENCY'].unique()

        for i in listc:
            span[i] = []
            for ind, val in data.iterrows():

                if i == val['CURRENCY']:
                    span[i].append(data.loc[ind].T.to_dict())

        TdayNostro['reportData'] = span

        data.rename(columns={
            'CLOSING_BALANCE_AMOUNT': 'NOSTRO_CLOSING_BALANCE',
            'STATEMENT_DATE': 'TRANSACTION_DATE'
        }, inplace=True)
        data = data[['CURRENCY', 'ACCOUNT_NUMBER', 'TRANSACTION_DATE', 'NOSTRO_CLOSING_BALANCE', 'BALANCE_DATE',
                     'OGL_CLOSING_BALANCE', 'OGL Vs NOSTRO DIFFERENCE', 'OPEN_EXCEPTION', 'OPEN_EXCEPTION_DIFFERENCE']]
        data.to_csv(config.uploadDir + '/NostroVsOglReport.csv', index=False)
        print ("| Report Generated Successfully")
        return True, TdayNostro

    def getNostroExceptionReport(self, id):
        (status, recon_con) = sql.getConnection()
        try:
            if status:
                query = {}
                query['recon_id'] = 'TRSC_CASH_APAC_20181'
                query['RECORD_STATUS'] = 'ACTIVE'
                query['MATCHING_STATUS'] = 'UNMATCHED'
                # logger.info(query)
                (status_rec, reconData) = sql.read_unmatched(recon_con, query)
                reconData['TRANSACTION_DATE'] = reconData['TRANSACTION_DATE'].apply(
                    lambda x: datetime.datetime.strftime(x, "%d-%m-%Y"))
                reconData['VALUE_DATE'] = reconData['VALUE_DATE'].apply(
                    lambda x: datetime.datetime.strftime(x, "%d-%m-%Y"))
                reconData.rename(columns={
                    "FREE_TEXT_1": "Unit Name",
                    "FREE_TEXT_2": "Recon Remarks",
                    "FREE_TEXT_3": "Ops Remarks"
                }, inplace=True)
                df1 = reconData[(reconData['SOURCE_NAME'] == 'SWIFT') & (reconData['DEBIT_CREDIT_INDICATOR'] == 'C')]
                df1.sort_values(by=['AGEING'], ascending=False, inplace=True)
                df1['TYPE'] = 'Outstanding Nostro Credit'
                # logger.info(df1.head(5))
                df2 = reconData[(reconData['SOURCE_NAME'] == 'SWIFT') & (reconData['DEBIT_CREDIT_INDICATOR'] == 'D')]
                df2['TYPE'] = 'Outstanding Nostro Debit'
                df2.sort_values(by=['AGEING'], ascending=False, inplace=True)
                df3 = reconData[(reconData['SOURCE_NAME'] == 'KPTP') & (reconData['DEBIT_CREDIT_INDICATOR'] == 'C')]
                df3['TYPE'] = 'Outstanding Mirror Credit'
                df3.sort_values(by=['AGEING'], ascending=False, inplace=True)
                df4 = reconData[(reconData['SOURCE_NAME'] == 'KPTP') & (reconData['DEBIT_CREDIT_INDICATOR'] == 'D')]
                df4['TYPE'] = 'Outstanding Mirror Debit'
                df4.sort_values(by=['AGEING'], ascending=False, inplace=True)
                df5 = pandas.read_csv(config.uploadDir + '/NostroVsOglReport.csv')

                nostrolist = [{'header': "Nostro V/s Ogl Report Summary", "df": df5},
                              {"header": "Pending for our Debit", "df": df1},
                              {"header": "Pending for our Credit", "df": df2},
                              {"header": "Pending for their Debit", "df": df3},
                              {"header": "Pending for Their Credit", "df": df4}]
                object_id = str(flask.session["sessionData"]['_id'])
                # logger.info(object_id)
                if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
                    os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))
                reportFile = {}
                dir = config.contentDir + "/" + "recon_reports/" + object_id + '/'
                file_name = str(datetime.datetime.now()) + query['recon_id'] + '*.csv'

                with open(dir + file_name, 'a') as csvfile:
                    csvwriter = csv.writer(csvfile)

                    for data in nostrolist:
                        csvwriter.writerow([data['header']])
                        csvwriter.writerow(data['df'].columns)
                        for index, row in data['df'].iterrows():
                            csvwriter.writerow(row)
                reportFile['fileName'] = object_id + '/' + file_name
                # reportFile['total'] = reconData
                # logger.info(reportFile)

                return True, reportFile
            else:
                return False, "No Data Found for this reconid"
        except:
            logger.info(traceback.format_exc())

    def getDealReport(self, cycles, recons, statementDate, connection):

        query1 = "select RECON_ID, SOURCE_NAME,MATCHING_STATUS,TXT_2000_002,TXN_MATCHING_STATUS from CASH_OUTPUT_TXN where recon_id in " + recons

        query1 += "and RECORD_STATUS='ACTIVE' and statement_date = to_date('" + statementDate + "','dd/mm/yy') and TXT_2000_002 in " + cycles + ""

        df = pandas.read_sql(query1, connection)

        if len(df) > 0:
            df.loc[df['TXN_MATCHING_STATUS'] == 'FORCE_MATCHED', 'MATCHING_STATUS'] = 'FORCE_MATCHED'

            df['COUNT'] = 1

            # Get Source And Deal System From SourceName
            df['SOURCE'] = df['SOURCE_NAME'].str.rsplit('_', 1).str[0]
            df['DEAL'] = df['SOURCE_NAME'].str.split('_').str[-1]

            df = df.groupby(['SOURCE', 'DEAL', 'MATCHING_STATUS'])['COUNT'].sum().reset_index()

            # Geting Total
            f = df.groupby(['SOURCE', 'DEAL'])['COUNT'].sum().reset_index()
            f['MATCHING_STATUS'] = 'TOTAL'

            df = pandas.concat([df, f])

            a = df.groupby(['MATCHING_STATUS', 'DEAL']).sum().reset_index()
            a['SOURCE'] = 'TOTAL_COUNT'
            df = pandas.concat([df, a])

        return df

    def dealTallyCycleWise(self, id, query):
        try:
            (status, connection) = sql.getConnection()
            executionId = []
            nightData = None
            dayData = None
            nightDeal = pandas.DataFrame()
            statementDate = query['statementDate']
            recons = config.dealRecons
            recons = "','".join(recons).join(("'", "'")).join(("(", ")"))

            cycles = config.cycles[query['CYCLE']]
            cycles = "','".join(cycles).join(("'", "'")).join(("(", ")"))

            query1 = "select RECON_ID, SOURCE_NAME,MATCHING_STATUS,TXT_2000_002,TXN_MATCHING_STATUS from CASH_OUTPUT_TXN where recon_id in " + recons

            query1 += "and RECORD_STATUS='ACTIVE' and statement_date = to_date('" + statementDate + "','dd/mm/yy') and TXT_2000_002 = '" + \
                      query['CYCLE'] + "'"

            cycleData = pandas.read_sql(query1, connection)
            logger.info(len(cycleData))

            if len(cycleData) > 0:

                df = self.getDealReport(cycles, recons, statementDate, connection)

                if len(df) > 0:
                    # Converting Data Format to view in UI
                    sources = df['SOURCE'].unique()
                    dictDF = df.to_dict(orient='records')
                    deal = {}
                    for i in sources:
                        for entry in dictDF:
                            if entry['SOURCE'] == i:
                                if not i in deal.keys():
                                    deal[i] = {}
                                if not entry['DEAL'] in deal[i].keys():
                                    deal[i][entry['DEAL']] = {'MATCHED': 0, 'UNMATCHED': 0, 'FORCE_MATCHED': 0,
                                                              'TOTAL': 0}
                                if entry['MATCHING_STATUS'] == 'MATCHED':
                                    deal[i][entry['DEAL']]['MATCHED'] = entry['COUNT']
                                if entry['MATCHING_STATUS'] == 'UNMATCHED':
                                    deal[i][entry['DEAL']]['UNMATCHED'] = entry['COUNT']
                                if entry['MATCHING_STATUS'] == 'FORCE_MATCHED':
                                    deal[i][entry['DEAL']]['FORCE_MATCHED'] = entry['COUNT']
                                if entry['MATCHING_STATUS'] == 'TOTAL':
                                    deal[i][entry['DEAL']]['TOTAL'] = entry['COUNT']
                    logger.info(deal)
                    # df = df[df['SOURCE'] != 'TOTAL_COUNT']

                    df = pandas.pivot_table(df, columns=['DEAL', 'MATCHING_STATUS'], index=['SOURCE'], values=['COUNT'],
                                            aggfunc={'COUNT': np.sum}, margins=False, fill_value=0)

                    df = self.getMissedDealAndColumns(df)

                    if query['CYCLE'] == 'Cycle 5':
                        nightData = self.getDealReport("('Cycle 5')", recons, statementDate, connection)
                        if len(nightData):
                            sources = nightData['SOURCE'].unique()
                            dictDF = nightData.to_dict(orient='records')
                            nightDeal = {}

                            for i in sources:
                                for entry in dictDF:
                                    if entry['SOURCE'] == i:
                                        if not i in nightDeal.keys():
                                            nightDeal[i] = {}
                                        if not entry['DEAL'] in nightDeal[i].keys():
                                            nightDeal[i][entry['DEAL']] = {'MATCHED': 0, 'UNMATCHED': 0,
                                                                           'FORCE_MATCHED': 0,
                                                                           'TOTAL': 0}
                                        if entry['MATCHING_STATUS'] == 'MATCHED':
                                            nightDeal[i][entry['DEAL']]['MATCHED'] = entry['COUNT']
                                        if entry['MATCHING_STATUS'] == 'UNMATCHED':
                                            nightDeal[i][entry['DEAL']]['UNMATCHED'] = entry['COUNT']
                                        if entry['MATCHING_STATUS'] == 'FORCE_MATCHED':
                                            nightDeal[i][entry['DEAL']]['FORCE_MATCHED'] = entry['COUNT']
                                        if entry['MATCHING_STATUS'] == 'TOTAL':
                                            nightDeal[i][entry['DEAL']]['TOTAL'] = entry['COUNT']
                            #     # df = df[df['SOURCE'] != 'TOTAL_COUNT']

                            nightData = pandas.pivot_table(nightData, columns=['DEAL', 'MATCHING_STATUS'],
                                                           index=['SOURCE'],
                                                           values=['COUNT'],
                                                           aggfunc={'COUNT': np.sum}, margins=False, fill_value=0)

                            nightData = self.getMissedDealAndColumns(nightData)

                        dayData = self.getDealReport("('Cycle 1', 'Cycle 2', 'Cycle 3', 'Cycle 4')", recons,
                                                     statementDate, connection)
                        if len(dayData):
                            dayData = pandas.pivot_table(dayData, columns=['DEAL', 'MATCHING_STATUS'],
                                                         index=['SOURCE'],
                                                         values=['COUNT'],
                                                         aggfunc={'COUNT': np.sum}, margins=False, fill_value=0)
                            dayData = self.getMissedDealAndColumns(dayData)

                    object_id = str(flask.session["sessionData"]['_id'])
                    if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id)):
                        os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id))

                    file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/' + 'DEAL_DASHBOARD_REPORT' + '*.xlsx'
                    fileName = glob.glob(file_name)

                    for file in fileName:
                        os.remove(file)
                    to_date = datetime.datetime.now().strftime("%b-%d-%Y")
                    stmtDate = datetime.datetime.strptime(statementDate, '%d/%m/%y')
                    stmtDate = datetime.datetime.strftime(stmtDate, '%d-%b-%Y')

                    header = pandas.DataFrame(
                        [['REPORT:-', 'DEAL TALLY DASH BOARD REPORT'], ['STATEMENT_DATE', stmtDate],
                         ['CYCLE :-', query['CYCLE']]])

                    d = "select mdl_field_id,ui_display_name,width,mandatory,mdl_field_data_type from recon_dynamic_data_model where recon_id = '" + 'TRSC_CASH_APAC_61142' + "' and record_status = 'ACTIVE' order by order_seq_no"

                    columnlist = pandas.read_sql(d, connection)

                    renameCol = {}
                    for index, row in columnlist.iterrows():
                        renameCol[row['MDL_FIELD_ID']] = row['UI_DISPLAY_NAME']

                    Query = "select RECON_ID,TXT_2000_002,SOURCE_NAME,SOURCE_DEALING_SYSTEM,DEAL_NUMBER,TXT_180_003,TS_6_001,TXT_32_002,TXT_10_002,TXT_10_003,NUM_16_002,AMOUNT_1,AMOUNT_2,SPARE1,SPARE2,SPARE3,TS_6_004,TS_6_005,AUTHORIZATION_STATUS,MATCHING_STATUS from CASH_OUTPUT_TXN where recon_id in " + recons

                    Query += "and RECORD_STATUS='ACTIVE' and statement_date = to_date('" + statementDate + "','dd/mm/yy')"

                    logger.info(Query)
                    df1 = pandas.read_sql(Query, connection)
                    logger.info(df1)
                    writer = pandas.ExcelWriter(
                        config.contentDir + "/" + "recon_reports/" + object_id + '/' + 'DEAL_DASHBOARD_REPORT' + to_date + '.xlsx',
                        engine='xlsxwriter', datetime_format='YYYY-MM-DD', date_format='YYYY-MM-DD')

                    header.to_excel(writer, sheet_name='REPORT', startrow=1, startcol=0, index=None, header=None)

                    df.to_excel(writer, sheet_name='REPORT', startrow=5)

                    if dayData is not None and len(dayData) > 0:
                        dayDataCycleHeader = pandas.DataFrame([['REPORT:-', 'DEAL TALLY DASH BOARD REPORT'],
                                                               ['STATEMENT_DATE', stmtDate],
                                                               ['CYCLE :-', 'DAY CYCLE']])
                        dayDataCycleHeader.to_excel(writer, sheet_name='DAY CYCLE', startrow=1, startcol=0, index=None,
                                                    header=None)
                        dayData.to_excel(writer, sheet_name='DAY CYCLE', startrow=5)

                    if nightData is not None and len(nightData) > 0:
                        nightCycleHeader = pandas.DataFrame([['REPORT:-', 'DEAL TALLY DASH BOARD REPORT'],
                                                             ['STATEMENT_DATE', stmtDate],
                                                             ['CYCLE :-', 'NIGHT CYCLE']])
                        nightCycleHeader.to_excel(writer, sheet_name='NIGHT CYCLE', startrow=1, startcol=0, index=None,
                                                  header=None)
                        nightData.to_excel(writer, sheet_name='NIGHT CYCLE', startrow=5)

                    df1.rename(columns=renameCol, inplace=True)

                    dealReportColumns = {'Currency_1': 'BaseCurrency', 'Currency_2': 'TermCurrency',
                                         'Currency_1_Amount': 'BaseCurrencyAmount',
                                         'Currency_2_Amount': 'TermCurrencyAmount',
                                         'Swap_2nd__Leg_Value_date': 'MaturityDate2'}

                    df1.rename(columns=dealReportColumns, inplace=True)
                    df1 = df1.sort_values(by=['RECON_ID'], ascending=True)
                    df1.to_excel(writer, sheet_name='DATA', startrow=1, index=None)

                    writer.save()
                    writer.close()
                    data = {}
                    data['data'] = deal
                    if len(nightDeal) > 0:
                        data['nightDeal'] = nightDeal
                    data['filePath'] = object_id + '/' + 'DEAL_DASHBOARD_REPORT' + to_date + '.xlsx'
                    return True, data
                else:
                    return False, 'No Data Found For Selected date'
            else:
                return False, 'Data Not Found For ' + query['CYCLE']

        except Exception as e:
            logger.info(traceback.format_exc())

    def getMissedDealAndColumns(self, df):
        for dealName in ['SWAP', 'SPOT', 'FORWARD']:
            if dealName not in df.columns.levels[1]:
                df['COUNT', dealName, 'MATCHED'] = 0
                df['COUNT', dealName, 'UNMATCHED'] = 0
                df['COUNT', dealName, 'FORCE_MATCHED'] = 0
                df['COUNT', dealName, 'TOTAL'] = 0
            else:
                for status in ['MATCHED', 'UNMATCHED', 'FORCE_MATCHED']:
                    if status not in df['COUNT'][dealName]:
                        df['COUNT', dealName, status] = 0

        df = df.reindex_axis(['SPOT', 'SWAP', 'FORWARD'], axis=1, level=1)
        df = df.reindex_axis(['MATCHED', 'UNMATCHED', 'FORCE_MATCHED', 'TOTAL'], axis=1, level=2)
        return df

    def getBusinessContextPerUser(self, id):
        (status_b, data_d) = ReconAssignment().getAll(
            {'perColConditions': {'user': flask.session['sessionData']['userName']}})
        bus_id = []
        rec_id = []
        if status_b and len(data_d['data']) > 0:
            # logger.info(data_d['data'])
            for r in data_d['data']:
                bus_id.append(r['businessContext'])
                rec_id.append(r['recons'].split(','))
            logger.info('bus ------------' + str(bus_id))
            (status, recon_con) = sql.getConnection()
            if len(bus_id) > 0:
                joined_ids = "','".join(map(str, bus_id))
                joined_rec_ids = "','".join(np.concatenate(rec_id, axis=0))
                # get max execution id for recons
                cursor = recon_con.cursor()
                current_exec_query = "select max(recon_execution_id) as execution_id from recon_execution_details_log where record_status = 'ACTIVE' and processing_state_status = 'Matching Completed' and recon_id in ('" + joined_rec_ids + "') group by recon_id"
                # fetchmany caching issue migrated to pandas
                # #cursor.execute(current_exec_query)
                # rows = cursor.fetchmany()
                df = pandas.read_sql(current_exec_query, recon_con)
                rows = df['EXECUTION_ID'].tolist()
                # logger.info(rows)
                execution_ids = ",".join(str(val) for val in rows)
                # logger.info(execution_ids)
                if id == "join":
                    return True, " and wo.business_context_id in ('" + joined_ids + "') and rd.recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                elif id == "dummy2":
                    return True, " and business_context_id in ('" + joined_ids + "') and recon_id in ('" + joined_rec_ids + "')", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "')"
                else:
                    return True, " and business_context_id in ('" + joined_ids + "') and recon_id in ('" + joined_rec_ids + "') and recon_execution_id in (" + execution_ids + ")", " and txn.business_context_id in ('" + joined_ids + "') and txn.recon_id in ('" + joined_rec_ids + "') and txn.recon_execution_id in (" + execution_ids + ")"
            else:
                return True, ''
        else:
            return False, ''

    def queryData(self, levelkey, filters, datasetName, summary=None):
        filterstr = ""
        df = None
        if datasetName is not None:
            metadata = json.load(open("meta/" + datasetName + ".json"))

            (status, recon_con) = sql.getConnection()

            if status:
                # logger.info('comming here')
                (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                logger.info('queryyyyyyyyyyyyyyyyyyyyyyyyyyyy' + str(query))
                df = pandas.read_sql(query, recon_con)
                # logger.info("df" + str(df))
                if summary is not None:
                    if "TXN_MATCHING_STATUS" in df.columns:
                        del df['TXN_MATCHING_STATUS']
                df = pandas.merge(df, self.statesdf,
                                  on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                      "AUTHORIZATION_STATUS",
                                      "FORCEMATCH_AUTHORIZATION"], how="left")

        for key, val in filters.items():
            if len(filterstr) > 1:
                filterstr += ' & '
            if type(val) == str:
                val = "\"" + val + "\""
            else:
                val = str(val)
            filterstr = "(df[\"" + key + "\"]==" + val + ")"
        filteredData = df
        # logger.info(filteredData)
        if len(filterstr) > 1:
            # x="self.df["+filters+"]"
            filteredData = filteredData[eval(filterstr)]

        x = dict()
        x["df"] = filteredData
        x["levelkey"] = levelkey
        if summary is not None:
            # filterData = filteredData.to_json(orient="records")
            x['levelkey'] = "Summary"
            filteredData = eval(metadata["customMethod"] + "(x,metadata = metadata)")
            return True, filteredData
        else:
            filteredData = eval(metadata["customMethod"] + "(x,metadata = metadata)")
            return True, filteredData

    def makehighchartsjson(df, columnHierarchy):
        output = dict()
        columns = df.columns
        for index, row in df.iterrows():
            temp = output
            for col in columnHierarchy:
                # print row[col]
                if row[col] not in temp:
                    temp[row[col]] = dict()
                temp = temp[row[col]]
            index = 0
            for col in columns:
                if col in columnHierarchy:
                    continue
                else:
                    temp[col] = row[col]
        return output

    def getAgingReport(self, bucketsize, datasetName):
        logger.info(bucketsize)
        df = None
        # statesdf=pandas.read_csv("meta/states.csv")
        if datasetName is not None:
            logger.info(datasetName)
            metadata = json.load(open("meta/" + datasetName + ".json"))
            (status, recon_con) = sql.getConnection()
            (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
            if status:
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                logger.info(query)
                df = pandas.read_sql(query, recon_con)
                # logger.info("df" + str(df))
                df = pandas.merge(df, self.statesdf,
                                  on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                      "AUTHORIZATION_STATUS",
                                      "FORCEMATCH_AUTHORIZATION"], how="left")

        records = df[["RECON_ID", "BUSINESS_CONTEXT_ID", "STATEMENT_DATE", "AMOUNT", "EXCEPTION_STATUS"]].dropna(
            subset=['STATEMENT_DATE'])
        records = records[pandas.notnull(records['STATEMENT_DATE'])]
        records["TRANSACTIONAGE"] = records["STATEMENT_DATE"].apply(
            lambda x: (datetime.datetime.now() - x).days if x.year != 0001 else 0)
        records["BUCKET"] = records["TRANSACTIONAGE"] - (records["TRANSACTIONAGE"] % int(bucketsize))
        records[["RECON_ID", "BUSINESS_CONTEXT_ID", "BUCKET", "TRANSACTIONAGE", "STATEMENT_DATE", "AMOUNT",
                 "EXCEPTION_STATUS"]]
        records["COUNT"] = 1

        # Remove all matched transactions if any exists
        records.loc[records.loc[:, 'EXCEPTION_STATUS'] == 'CLOSED', 'EXCEPTION_STATUS'] = None
        # logger.info(records)
        x = records[["RECON_ID", "BUSINESS_CONTEXT_ID", "BUCKET", "COUNT", "AMOUNT", "EXCEPTION_STATUS"]].groupby(
            ["BUCKET", "RECON_ID", "EXCEPTION_STATUS", "BUSINESS_CONTEXT_ID"]).sum().reset_index()
        # logger.info(x.columns)
        df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'", recon_con)
        x = pandas.merge(x, df2, on=["RECON_ID"], how="left")
        df3 = pandas.read_sql("select workpool_name,business_context_id from workpool where record_status ='ACTIVE'",
                              recon_con)
        x = pandas.merge(x, df3, on=["BUSINESS_CONTEXT_ID"], how="left")
        # for column in x.columns:
        #    if column.find("AMOUNT") != -1:
        #        x[column]=x[column].apply(lambda x:'{:.2f}'.format(x))
        y = json.loads(x.to_json(orient="records"))
        for x in range(0, len(y)):
            if x + 1 < len(y):
                y[x]["RANGE"] = str(y[x]["BUCKET"]) + "-" + str(y[x]["BUCKET"] + int(bucketsize))
            else:
                y[x]["RANGE"] = ">" + str(y[x]["BUCKET"])
        logger.info(y)
        return True, y

    def customProcess(self, data, metadata=None):
        df = data["df"]
        levelkey = data["levelkey"]
        if levelkey == "TXN_MATCHING_STATUS" and "TXN_MATCHING_STATUS_x" in df.columns:
            df['TXN_MATCHING_STATUS'] = df['TXN_MATCHING_STATUS_y']
            del df['TXN_MATCHING_STATUS_x']
            del df['TXN_MATCHING_STATUS_y']

        columns = []
        indexcolums = []
        for key in metadata["tree"]:
            if key == levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        if levelkey == 'BUSINESS_CONTEXT_ID':
            columns.append("RECON_ID")

        dfOutstanding = df.copy()
        if 'TXN_MATCHING_STATUS_y' in dfOutstanding.columns:
            dfOutstanding = dfOutstanding[(dfOutstanding['TXN_MATCHING_STATUS_y'] != 'AUTOMATCHED') &
                                          (dfOutstanding['TXN_MATCHING_STATUS_y'] != 'FORCE_MATCHED') &
                                          (dfOutstanding['TXN_MATCHING_STATUS_y'] != 'ROLLBACK_AUTHORIZATION_PENDING')]

        dfOutstanding = dfOutstanding[columns]
        dfOutstanding["COUNT"] = 1
        if 'SOURCE_NAME' in indexcolums and levelkey != "SOURCE_NAME":
            indexcolums.remove('SOURCE_NAME')
        if 'ACCOUNT_NAME' in indexcolums and levelkey != "ACCOUNT_NAME":
            indexcolums.remove('ACCOUNT_NAME')
        if levelkey == 'BUSINESS_CONTEXT_ID':
            indexcolums.append("RECON_ID")
        logger.info(indexcolums)

        if 'EXCEPTION_STATUS' in dfOutstanding.columns:
            dfOutstanding.loc[df['EXCEPTION_STATUS'].isnull(), 'EXCEPTION_STATUS'] = 'CLOSED'

        if 'DEBIT_CREDIT_INDICATOR' in dfOutstanding.columns and dfOutstanding['DEBIT_CREDIT_INDICATOR'] is not None:
            dfOutstanding.loc[dfOutstanding['DEBIT_CREDIT_INDICATOR'].isnull(), 'DEBIT_CREDIT_INDICATOR'] = ''
            dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                               columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": len})

        if 'AMOUNT' in dfOutstanding.columns:
            dfOutstanding['AMOUNT'].fillna(0.0, inplace=True)

        if 'COUNT' in dfOutstanding.columns:
            dfOutstanding['COUNT'].fillna(0, inplace=True)

        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"] = 0.0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"] = 0.0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0
        # df["UNKNOWN"]=dfOutstanding["AMOUNT"][" "]
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["NETOUTSTANDINGCOUNT"] = dfOutstanding["CREDITCOUNT"] + dfOutstanding["DEBITCOUNT"]

        dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1)
        if dfOutstanding.empty:
            return dfOutstanding.to_json(orient="records")
        if levelkey != "Summary":
            dfOutstanding.columns = dfOutstanding.columns.droplevel(1)
            dfOutstanding.reset_index(inplace=True)
            dfOutstanding.set_index(indexcolums[-1])
        else:
            dfOutstanding = dfOutstanding.unstack(level=-1)
            dfOutstanding = dfOutstanding.reset_index()
        (status, recon_con) = sql.getConnection()

        if levelkey == "BUSINESS_CONTEXT_ID":
            df2 = pandas.read_sql(
                "select workpool_name,business_context_id from workpool where record_status ='ACTIVE'", recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["BUSINESS_CONTEXT_ID"], how="left")

            df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                  recon_con)

            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")

        if levelkey == "RECON_ID":
            df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                  recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")

        if levelkey == "Summary":
            df1 = df
            # compute carry forward count, if any exists
            df_carry_forward = df1.loc[:, ['CARRY_FORWARD_FLAG', 'RECON_ID']]
            df1.drop('CARRY_FORWARD_FLAG', axis=1, inplace=True)
            df_carry_forward['CARRY_FORWARD_COUNT'] = df_carry_forward['CARRY_FORWARD_FLAG'].apply(
                lambda x: True if x == 'Y' else False)
            df_carry_fwd_cnt = df_carry_forward.groupby('RECON_ID').agg({'CARRY_FORWARD_COUNT': sum}).reset_index()

            # Retrieve statement date from recon_execution_details_log instead of cash_output_txn
            # work around for multiple statement dates
            # df_statement = df1.groupby(['RECON_ID']).agg({'STATEMENT_DATE': max}).reset_index()

            df_grouped = df.groupby(['RECON_ID', 'BUSINESS_CONTEXT_ID', 'TXN_MATCHING_STATUS']).size().reset_index(
                name='count').to_dict(orient='records')
            _summary = [{'RECON_ID': _x['RECON_ID'], 'BUSINESS_CONTEXT_ID': _x['BUSINESS_CONTEXT_ID'],
                         _x['TXN_MATCHING_STATUS']: _x['count']} for _x in df_grouped]
            _summary = pandas.DataFrame(_summary).fillna(0)
            dfTOSend = _summary.groupby(['BUSINESS_CONTEXT_ID', 'RECON_ID']).sum().reset_index()

            (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('join')
            if concat_str:
                df2 = pandas.read_sql(
                    "SELECT wo.workpool_name, wo.business_context_id, rd.recon_name,rd.recon_id FROM workpool wo,recon_details rd where wo.business_context_id=rd.business_context_id and wo.record_status ='ACTIVE' and rd.record_status = 'ACTIVE'" + concat_data + "",
                    recon_con)
            else:
                df2 = pandas.read_sql(
                    "SELECT wo.workpool_name, wo.business_context_id, rd.recon_name,rd.recon_id FROM workpool wo,recon_details rd where wo.business_context_id=rd.business_context_id and wo.record_status ='ACTIVE' and rd.record_status = 'ACTIVE'",
                    recon_con)

            dfTOSend = pandas.merge(dfTOSend, df2, on=["BUSINESS_CONTEXT_ID", "RECON_ID"], how="left",
                                    suffixes=('', '_y'))

            for column in dfTOSend.columns:
                if '_y' in column:
                    del dfTOSend[column]

            (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy2')
            if concat_str:
                df3 = pandas.read_sql(
                    "select recon_name,recon_id from recon_details where record_status ='ACTIVE'" + concat_data + "",
                    recon_con)
                status, df_exec = sql.getLatestExecutionLog(recon_con, concat_data)
            else:
                df3 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                      recon_con)

            # update latest statement date from recon_execution_details_log
            if not df_exec.empty:
                dfTOSend = pandas.merge(dfTOSend, df_exec, on=["RECON_ID"], how="left")

            # update carry forward count
            if not df_carry_fwd_cnt.empty:
                dfTOSend = pandas.merge(dfTOSend, df_carry_fwd_cnt, on=['RECON_ID'], how="left")

            dfTOSend = pandas.merge(dfTOSend, df3, on=["RECON_ID"], how="left", suffixes=('', '_y'))
            for col in dfTOSend.columns:
                if '_y' in col:
                    del dfTOSend[col]

            # execution_date_time order
            execution_Date = dfTOSend.loc[:, 'EXECUTION_DATE'].dt.strftime('%d/%m/%Y %I:%M %p')
            dfTOSend.drop(labels=['EXECUTION_DATE'], axis=1, inplace=True)
            dfTOSend.insert(2, 'EXECUTION_DATE', execution_Date)
            # statement_date order
            statement_Date = dfTOSend.loc[:, 'STATEMENT_DATE'].dt.strftime('%d/%m/%Y')
            dfTOSend.drop(labels=['STATEMENT_DATE'], axis=1, inplace=True)
            dfTOSend.insert(3, 'STATEMENT_DATE', statement_Date)
            dfTOSend = dfTOSend[dfTOSend['BUSINESS_CONTEXT_ID'] != '1111131119']
            dfTOSend = dfTOSend.fillna(0)

            for column in dfTOSend.columns:
                if column.find("AMOUNT") != -1:
                    dfTOSend[column] = dfTOSend[column].apply(lambda x: '{:.2f}'.format(x))

            return dfTOSend.to_json(orient="records")
        if 'COUNT' in dfOutstanding.columns:
            del dfOutstanding['COUNT']
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1 or column in ['CREDIT', 'DEBIT']:
                # dfOutstanding[column]=dfOutstanding[column].apply(lambda x:'{:.2f}'.format(x))
                dfOutstanding[column] = dfOutstanding[column].round(2)

        return dfOutstanding.to_json(orient="records")

    def getNetOutstandingAmount(self):
        df = self.queryToDataframe(
            "select business_context_id, recon_id, recon_execution_id, statement_date, execution_date_time, source_type, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'")
        # print df
        # dfUnmatched=df.ix[(df["MATCHING_STATUS"]=="UNMATCHED") & (df["RECONCILIATION_STATUS"]=="EXCEPTION")]
        # dfUnmatched=dfUnmatched[["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        # dfUnmatched=dfUnmatched.groupby(["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR"]).sum()

        # print dfUnmatched.reset_index()

        # Business context level
        dfOutstanding = df[["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS", "DEBIT_CREDIT_INDICATOR", "AMOUNT"]]
        dfOutstanding["COUNT"] = 1
        # dfOutstanding=dfOutstanding.groupby(["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR"])
        dfOutstanding = pandas.pivot_table(dfOutstanding, index=['BUSINESS_CONTEXT_ID', 'RECONCILIATION_STATUS'],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len}).reset_index()
        # dfOutstanding=dfOutstanding.sum()
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["AMOUNT"]["D"] - dfOutstanding["AMOUNT"]["C"]
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        print dfOutstanding

    def getTransactionSummaryByDate(self, reconName=None):
        logger.info("Recon name passed is " + reconName)
        # reconName=None
        indexColumns = ["BUSINESS_CONTEXT_ID", "RECON_ID", "STATEMENT_DATE", "TXN_MATCHING_STATUS",
                        "DEBIT_CREDIT_INDICATOR", "AMOUNT"]
        # df=self.df
        (status, recon_con) = sql.getConnection()
        (concat_str, concat_data, concat_data_ex) = self.getBusinessContextPerUser('dummy')
        df = pandas.read_sql(
            "select business_context_id, recon_id, recon_execution_id,account_number,account_name, statement_date, execution_date_time, source_type, source_name, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T' " + concat_data,
            recon_con)
        # logger.info(df)
        # statesdf=pandas.read_csv("meta/states.csv")
        df = pandas.merge(df, self.statesdf, on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS",
                                                 "AUTHORIZATION_STATUS",
                                                 "FORCEMATCH_AUTHORIZATION"], how="left")
        logger.info(df.columns)
        if reconName and reconName != "dummy":
            df = df[df["RECON_ID"] == reconName]
            indexColumns.remove("RECON_ID")
            indexColumns.remove("BUSINESS_CONTEXT_ID")
        summarizer = df[indexColumns]
        # ["RECON_NAME", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "AMOUNT", "DEBIT_CREDIT_INDICATOR"]]
        # summarizer=self.df[["RECON_NAME","STATEMENT_DATE","TXN_MATCHING_STATUS","AMOUNT","DEBIT_CREDIT_INDICATOR"]]
        # summarizer=summarizer.groupby(indexColumns[0:-1]).sum().reset_index()
        # print summarizer
        # summarizer = self.df[indexColumns]

        summarizer["COUNT"] = 1
        summarizer = summarizer[pandas.notnull(summarizer['STATEMENT_DATE'])]
        summarizer = summarizer[summarizer['STATEMENT_DATE'].apply(lambda x: True if x.year != 0001 else False)]

        dfOutstanding = pandas.pivot_table(summarizer, index=indexColumns[0:-2],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len}).reset_index()

        dfOutstanding.fillna(0, inplace=True)

        # logger.info(dfOutstanding)
        presentColumns = []
        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"] = 0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"] = 0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["TOTALCOUNT"] = dfOutstanding["DEBITCOUNT"] + dfOutstanding["CREDITCOUNT"]
        dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1)
        dfOutstanding.columns = dfOutstanding.columns.droplevel(1)

        dfOutstanding = dfOutstanding[indexColumns[0:-2] + ["TOTALCOUNT", "NETOUTSTANDINGAMOUNT"]]
        dfOutstanding.reset_index(inplace=True)
        # logger.info(dfOutstanding.columns)
        if 'RECON_ID' in dfOutstanding.columns:
            df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",
                                  recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df2, on=["RECON_ID"], how="left")
            df3 = pandas.read_sql(
                "select workpool_name,business_context_id from workpool where record_status ='ACTIVE'", recon_con)
            dfOutstanding = pandas.merge(dfOutstanding, df3, on=["BUSINESS_CONTEXT_ID"], how="left")
        for column in dfOutstanding.columns:
            if column.find("AMOUNT") != -1:
                dfOutstanding[column] = dfOutstanding[column].apply(lambda x: '{:.2f}'.format(x))
        return True, dfOutstanding.to_json(orient="records")

    def getCreditOutstandingAmount(self):
        pass

    def getDebitOutstandingAmount(self):
        pass

    def getAutoMatching(self):
        pass

    def getRollbackTransactionsWaitingForAuthorizations(self):
        pass

    def getOutstandingEntriesInOpenStatus(self):
        pass

    def getWaitingForInvestigationAndResolution(self):
        pass

    def getPendingForSubmission(self):
        pass

    def getWaitingForAuthorization(self):
        pass

    def getRbiDadSummaryReport(self, id):
        statementDate = id
        feedPath = config.flaskPath + 'rbi_dad_report'
        frames = []
        # Read File For RBIDATA
        fpattern = "RBIDAD_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        rbi_feed_name = files[0]
        rbi_dad_df = pandas.read_excel(feedPath + os.sep + rbi_feed_name)
        frames.append(rbi_dad_df)

        # Read File For CBS
        fpattern = "ALGO-GL-OUTFILE_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        cbs_feed_name = files[0]
        cbs_df = pandas.read_csv(feedPath + os.sep + cbs_feed_name, sep='|', error_bad_lines=False, header=None)
        frames.append(cbs_df)

        # Read File For OGL
        fpattern = "TRIALBAL_" + statementDate
        files = [f for f in os.listdir(feedPath + os.sep) if re.match(fpattern, f)]
        if len(files) > 1: return False, "Found duplicate files"
        if len(files) == 0: return False, "No data found"
        ogl_feed_name = files[0]
        ogl_df = pandas.read_csv(feedPath + os.sep + ogl_feed_name, sep='|', error_bad_lines=False)

        frames.append(ogl_df)
        if any([len(frame) == 0 for frame in frames]) == True:
            return False, "No data found"

        # fix to handle new format of RBI DAD Summary Report files
        shift_rows = 0
        wb = open_workbook(feedPath + os.sep + rbi_feed_name)
        sheet = wb.sheet_by_index(0)
        if sheet.cell(0, 1).value == 'STATEMENT OF ACCOUNT':
            shift_rows = 1

        lookUpConditionDf = rbi_dad_df.loc[rbi_dad_df[rbi_dad_df.columns[3 - shift_rows]] == 'Closing Balance']
        rbi_records = json.loads(lookUpConditionDf.reset_index().fillna(0).to_json(orient='records'))

        displayTable = []
        for ind in range(0, len(rbi_records)):
            rbi_dad_indicator, ogl_cbs_indicator = '', ''
            rbi_closing_balance, cbs_closing_balance, ogl_closing_balance = 0, 0, 0
            rbi_record = rbi_records[ind]
            rbi_closing_balance = rbi_record[rbi_dad_df.columns[7 - shift_rows]]
            if isinstance(rbi_closing_balance, str) or isinstance(rbi_closing_balance, unicode):
                rbi_closing_balance = float("".join(re.split('\,', str(rbi_closing_balance))))
            else:
                rbi_closing_balance = float(rbi_closing_balance)

            rbi_dad_indicator = rbi_record[rbi_dad_df.columns[8 - shift_rows]]
            ogl_cbs_indicator = 'DR' if rbi_dad_indicator == 'CR' else 'CR'
            # CBS Match Scenario
            try:
                accountToMatch = 98026102018
                cbsConditionDf = cbs_df.loc[
                    (cbs_df[cbs_df.columns[0]] == 'H') & (cbs_df[cbs_df.columns[2]] == accountToMatch) & (
                            cbs_df[cbs_df.columns[5]] == ogl_cbs_indicator)]
                cbsConditionDf.columns = [str(i) for i in cbsConditionDf.columns]

                if len(cbsConditionDf):
                    cbs_record = json.loads(cbsConditionDf.reset_index().fillna(0).to_json(orient='records'))[0]
                    cbs_closing_balance = cbs_record[str(cbsConditionDf.columns[4])]

                    if type(cbs_closing_balance) == 'str':
                        float("".join(re.split('\,', cbs_closing_balance)))
                        # cbs_closing_balance = round(
                        #    float("".join(re.split('\,', str(cbs_record[str(cbsConditionDf.columns[4])])))), 2)
            except Exception, e:
                return False, str(e)

            # OGL Match Scenario
            try:
                ogl_source_col = ogl_df.columns[0]  # 'SOURCE'  # ogl_df.columns[0]
                ogl_acc_no__col = ogl_df.columns[1]  # 'GL_NO'  # ogl_df.columns[1]
                ogl_ind_col = ogl_df.columns[5]  # 'DEBIT_CREDIT_FLAG'  # ogl_df.columns[5]
                ogl_balance__col = ogl_df.columns[6]  # 'BALANCE_AMT'  # ogl_df.columns[6]

                ogl_dfConditionDf = ogl_df.loc[
                    (ogl_df[ogl_source_col] == 'CBS') & (ogl_df[ogl_acc_no__col].str.contains('41201'))]

                # ogl_dfConditionDf = ogl_dfConditionDf.loc[
                #    ogl_dfConditionDf[ogl_acc_no__col].apply(
                #        lambda x: True if len(str(x).split('41201', 3)[0]) else False)]
                ogl_dfConditionDf = ogl_dfConditionDf.loc[(ogl_df[ogl_acc_no__col].str[12:17] == '41201')]
                if len(ogl_dfConditionDf):
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf[ogl_balance__col].map(
                        lambda x: float("".join(re.split('\,', '%.2f' % x))))
                    ogl_dfConditionDf[ogl_balance__col] = ogl_dfConditionDf.apply(
                        lambda x: x[ogl_balance__col] * -1 if x[ogl_ind_col] == 'CR' else  x[ogl_balance__col], axis=1)
                    ogl_closing_balance = sum(ogl_dfConditionDf[ogl_balance__col])
                    # ogl_closing_balance = round(
                    #    sum(ogl_dfConditionDf[ogl_balance__col].map(lambda x: float("".join(re.split('\,', str(x)))))),
                    #    2)
            except Exception, e:
                return False, str(e)

            # format statemtn date
            frmtStmtDate = datetime.datetime.strptime(str(statementDate), '%d%m%Y').strftime("%d/%m/%Y")

            obj = dict()
            obj['status'] = True if (rbi_closing_balance) == (cbs_closing_balance) == (ogl_closing_balance) else False
            displaycontent = [{'Indicator': rbi_dad_indicator, 'Closing_Balance': (cbs_closing_balance),
                               'Source': 'RBI Mirror Account Closing balance as of ' + str(frmtStmtDate)}]
            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': (rbi_closing_balance),
                 'Source': 'Closing Balance of RBI DAD Account as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': cbs_closing_balance - rbi_closing_balance,
                 'Source': 'Net Difference between RBI Mirror and RBI DAD a/c as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': (ogl_closing_balance),
                 'Source': 'OGL Balance as of ' + str(frmtStmtDate)})

            displaycontent.append(
                {'Indicator': ogl_cbs_indicator, 'Closing_Balance': cbs_closing_balance - ogl_closing_balance,
                 'Source': 'Difference between RBI Mirror and OGL as of ' + str(frmtStmtDate)})

            obj['table'] = displaycontent
            displayTable.append(obj)
        return True, displayTable

    # Balance Reports
    def generateCustomReports(self, id, query):
        out_data = dict()

        functionName = query['functionName']
        statementDate = query['statementDate']
        out_data = eval(str(functionName))

        if len(out_data) > 0:
            return True, out_data
        else:
            return False, "No Data Found."

    # retrieve distinct currency from mongo for nostro reports
    def getDistinctCurrency(self, id, query):
        dis_currecy = NostroReport().distinct('CURRENCY')
        dis_currecy.sort()
        logger.info(dis_currecy)
        return True, dis_currecy

    # Nostro Reports
    def getNostroReportDetails(self, id, query):

        # if no query param passed, retrieve data for latest execution ID
        reportExtract = {'total': 0}
        reportData = []

        if query is not None and query == {}:
            (status, connection) = sql.getConnection()
            if status and connection is not None:
                recon_execution_id = sql.getLatestExecutionID(connection, 'TRSC_CASH_APAC_20181')
                logger.info(recon_execution_id)
                (status_b, qry_rst) = NostroReport().getAll(
                    {'perColConditions': {'execID': str(recon_execution_id)}})
                if qry_rst['total'] > 0:
                    reportData = qry_rst['data']
        else:
            # query based on the param passed
            q = {}
            query_result = []
            logger.info(query)
            q = json.loads(query, encoding='utf-8')
            logger.info("Nostro search query...")
            logger.info(q)
            reportData = list(NostroReport().find(q))

        if len(reportData) > 0:
            reportExtract['total'] = len(reportData)
            report_df = pandas.read_json(json.dumps(reportData))

            # order dataframe columns
            report_df = report_df.loc[:,
                        ['REPORT DATE', 'STATEMENT DATE', 'LAST TXN DATE', 'ACCOUNT NO', 'ACCOUNT NAME', 'CURRENCY',
                         'OP BAL DR / CR', 'OPENING BALANCE', 'CL BAL DR / CR', 'CLOSING BALANCE']]

            report_df.fillna(0, inplace=True)

            report_df['STATEMENT DATE'] = pandas.DatetimeIndex(report_df['STATEMENT DATE'])
            report_df['LAST TXN DATE'] = pandas.DatetimeIndex(report_df['LAST TXN DATE'])

            report_df = report_df.sort_values(by=['STATEMENT DATE', 'LAST TXN DATE', 'CURRENCY'],
                                              ascending=[False, False, True])
            report_df['STATEMENT DATE'] = report_df['STATEMENT DATE'].astype(str)
            report_df['STATEMENT DATE'] = report_df['STATEMENT DATE'].apply(
                lambda dateVal: datetime.datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                    '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')

            report_df['LAST TXN DATE'] = report_df['LAST TXN DATE'].astype(str)
            report_df['LAST TXN DATE'] = report_df['LAST TXN DATE'].apply(
                lambda dateVal: datetime.datetime.strptime(dateVal, '%Y-%m-%d').strftime(
                    '%Y/%m/%d') if dateVal != 'NaT' and dateVal != None else '')

            report_df.loc[:, 'REPORT DATE'] = report_df.loc[:, 'REPORT DATE']. \
                apply(lambda x: datetime.datetime.fromtimestamp(x).strftime('%Y/%m/%d %H:%M:%S'))

            # reportExtract['pdf_file_path'] = self.generateNostroPDFReport(report_df)
            reportExtract['csv_file_path'] = self.generateNostroCSVReport(report_df.copy())
            reportExtract['excel_file_path'] = self.generateNostroExcelReport(report_df.copy())

            report_df = report_df.rename(
                columns={'REPORT DATE': 'REP_DATE', 'STATEMENT DATE': 'STMT_DATE', 'LAST TXN DATE': 'LAST_TXN_DATE',
                         'ACCOUNT NO': 'ACCOUNT_NUMBER',
                         'ACCOUNT NAME': 'ACC_NAME', 'OPENING BALANCE': 'OP_BAL', 'CLOSING BALANCE': 'CL_BAL',
                         'OP BAL DR / CR': 'OP_DR_CR',
                         'CL BAL DR / CR': 'CL_DR_CR'})
            reportExtract['reportData'] = report_df.to_json(orient="records")

        return True, reportExtract

    def reportGenerator(self, data, fileName):
        object_id = self.checkSessionFolder(fileName)
        data[['PRODUCT_CODE', 'OGL', 'CDR', 'DIFFERENCE']].to_excel(
            config.contentDir + "/recon_reports/" + object_id + '/' + fileName, index=False)
        return object_id + '/' + fileName

    def adfReportStoreToMongo(self, df, reportName, statementDate):
        dict1 = {}
        dict1["stmtDate"] = str(statementDate)
        dict1["reportName"] = reportName
        dict1["reportDetais"] = df.to_dict(orient='records')
        dict1['partnerId'] = '54619c820b1c8b1ff0166dfc'
        ADF_OGL_Report().insert(dict1)
        return True

    def generateADFOGLReport(self, id, query):
        stmtDate = query['stmtDate'].replace('/', '-')
        fileName = query['report'] + '_' + stmtDate + '.xlsx'

        doc = ADF_OGL_Report().find_one({"stmtDate": stmtDate, "reportName": query['report']})
        if doc:
            df = pandas.DataFrame(doc)
            return self.reportGenerator(df, fileName)
        else:
            try:
                report = "AdfOglReport()." + query['report'] + "('" + str(stmtDate) + "')"

                status, df = eval(report)
                logger.info("Report Is Generated Successfully")
                if status:
                    status = self.adfReportStoreToMongo(df, stmtDate, query['report'])
                    logger.info("Report Is Stored Mongo Successfully")
                    fileName = self.reportGenerator(df, fileName)
                    logger.info("Report File Name " + fileName)
                    return True, fileName
                else:
                    return status, df
            except Exception as e:
                logger.info(e)
                logger.info(traceback.format_exc())

    def generateNostroExcelReport(self, nostro_report):
        object_id = str(flask.session["sessionData"]['_id'])
        nostro_file_name = 'Nostro_Closing_balance.xlsx'
        nostro_file_Path = ''

        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports')):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports'))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + '*.xlsx'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        nostro_file_Path = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + nostro_file_name

        nostro_report.loc[:, 'OPENING BALANCE'] = nostro_report.loc[:, 'OPENING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.loc[:, 'CLOSING BALANCE'] = nostro_report.loc[:, 'CLOSING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.to_excel(nostro_file_Path, sheet_name='Nostro Balance Report', index=False)
        # writer.save()
        return object_id + '/nostro_reports/' + nostro_file_name

    def generateNostroCSVReport(self, nostro_report):
        object_id = str(flask.session["sessionData"]['_id'])
        nostro_file_name = 'Nostro_Closing_balance.csv'
        nostro_file_Path = ''

        if not os.path.exists(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports')):
            os.makedirs(os.path.join(config.contentDir, "recon_reports/" + object_id + '/nostro_reports'))

        file_name = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + '*.csv'
        fileName = glob.glob(file_name)
        for file in fileName:
            os.remove(file)

        nostro_report.loc[:, 'OPENING BALANCE'] = nostro_report.loc[:, 'OPENING BALANCE'].astype(float).map(
            '{:,.2f}'.format)
        nostro_report.loc[:, 'CLOSING BALANCE'] = nostro_report.loc[:, 'CLOSING BALANCE'].astype(float).map(
            '{:,.2f}'.format)

        nostro_file_Path = config.contentDir + "/" + "recon_reports/" + object_id + '/nostro_reports/' + nostro_file_name
        nostro_report.to_csv(nostro_file_Path, index=False)

        return object_id + '/nostro_reports/' + nostro_file_name

    def generateGstReport(self, id, query):
        stmtDate = query['statementDate']  # TODO change all string date to date-time objects(ISO)
        _stmtDateTime = datetime.datetime.strptime(stmtDate, '%Y%m%d')

        report_status, _ = GstReports().get({"statementdate": str(stmtDate)})
        rollback_status, _ = GstMetaInfo().get({"statementdate": _stmtDateTime, 'rollback': True})

        g = GSTProcessor("", str(stmtDate))

        # Reload data when rollback status is set for the stmt date / if no data found.
        if rollback_status or not report_status:
            g.loadFiles()
            g.computeGST()
            g.mapState()
            g.saveResults()
            # drop rollback records for stmt date
            GstMetaInfo().delete({"statementdate": _stmtDateTime, 'rollback': True})
        else:
            pass
        status, gstReportData = GstReports().get({"statementdate": str(stmtDate)})

        userPool = list(UserPool().find({"users": {"$regex": ".*" + flask.session['sessionData']['userName'] + "*."}}))

        if userPool:
            gstSources = []
            for pool in userPool:
                gstSources.extend(pool.get('gst_sources', '').split(','))

            if gstSources:
                logger.info(query)
                if 'gstType' in query:
                    if query['gstType'] == 'Daily GST Report':
                        return True, g.filterData(gstSources, gstReportData['data'])
                    elif query and query['gstType'] == 'YTD GST Report':
                        logger.info(pandas.DataFrame(g.getYTD(gstSources)))
                        return True, g.getYTD(gstSources)

                    elif query['gstType'] == 'Source and State wise Daily Report':
                        filterData = g.filterData(gstSources, gstReportData['data'])
                        return True, g.gstReportByTotal(filterData)
                    elif query['gstType'] == 'YTD Total GST Report':
                        filterData = g.filterData(gstSources, g.queryYTDData())
                        return True, g.gstReportByTotal(filterData)

                    elif query['gstType'] == 'OGL GST Trail Balance':
                        filterData = g.filterData(gstSources, gstReportData['data'])
                        return True, g.getStateWiseGst(filterData)

                    elif query['gstType'] == 'State Wise Balance Daily Report':
                        filterData = g.filterData(gstSources, gstReportData['data'])
                        filterData.extend(g.getPreviousOGLBal(gstSources))
                        return True, g.groupGstByState(filterData)
                    elif query['gstType'] == 'State Wise Year Balances Report':
                        return True, g.groupGstByState(g.getYTD(gstSources))

                    elif query['gstType'] == 'System Wise Balance Daily Report':
                        filterData = g.filterData(gstSources, gstReportData['data'])
                        filterData.extend(g.getPreviousOGLBal(gstSources))
                        return True, g.groupGstBySystem(filterData)
                    elif query['gstType'] == 'System Wise Year Balances Report':
                        filterData = g.filterData(gstSources, g.queryYTDData())
                        return True, g.groupGstBySystem(filterData)
            else:
                return False, "No sources allocated."
        else:
            return False, []


class FeedLoaderInterface():
    def __init__(self):
        self.mongo_client = MongoClient('mongodb://' + config.engineIp + ':27017')
        self.mongo_collection = self.mongo_client.reconbuilder

    def getReconContextDetails(self, id):
        reconContextList = []
        cc_col = self.mongo_collection['datasource']
        for val in cc_col.find():
            reconContextDict = {}
            print (val)
            reconContextDict['RECON_NAME'] = val['reconName']
            reconContextDict['RECON_ID'] = val['reconId']
            reconContextDict["SOURCE"] = val['sources'].keys()
            reconContextList.append(reconContextDict)

        return True, reconContextList

    def getMatchingRules(self, id):
        cc_col = self.mongo_collection['matchingrules']
        print (cc_col.find({'recon_id': id}))


class CustomReports(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(CustomReports, self).__init__("customReports", hideFields)


class CustomValidation():
    # Validation funciton for Retail Assests RA_CASH_APAC_61204
    # Validate Based on Gl_Code
    def RA_61204(self, df):
        if len(df) > 0:
            glCode = []
            df_s1 = df[df['SOURCE_TYPE'] == 'S1']
            df_s2 = df[df['SOURCE_TYPE'] == 'S2']
            glCode += df_s1['TXT_60_001'].astype(int).astype(str).unique().tolist()

            df_s2['C40'] = df_s2['GL_ACCOUNT_NUMBER'].astype(str).str[12:17]
            glCode += df_s2['C40'].unique().tolist()

            # Mismatch in GL-Code
            if len(set(glCode)) > 1:
                return True, ', Mismatch in GL-Code'
            else:
                return False, ''

    # Validation funciton for Retail Assests TW_CASH_APAC_61204
    # Validate Based on Gl_Code
    def TW_61208(self, df):
        if len(df) > 0:
            glCode = []
            df_s1 = df[df['SOURCE_TYPE'] == 'S1']
            df_s2 = df[df['SOURCE_TYPE'] == 'S2']
            glCode += df_s1['TXT_32_001'].astype(int).astype(str).unique().tolist()

            df_s2['C40'] = df_s2['GL_ACCOUNT_NUMBER'].astype(str).str[12:17]
            glCode += df_s2['C40'].unique().tolist()

            # Mismatch in GL-Code
            if len(set(glCode)) > 1:
                return True, ', Mismatch in GL-Code'
            else:
                return False, ''

    def TD_61169(self, df):
        if len(df) > 0:
            DealId = []
            DealId = df['REF_NUMBER'].unique().tolist()
            if (len(DealId) > 1):
                return True, ',MisMatch in DealId'
            else:
                return False, ''


class CustomReportInterface():
    # RSystem v/s OGL Balance Report (RA_CASH_APAC_61204)
    def RA_61204(self, statementDate):
        (status, connection) = sql.getConnection()
        if status:

            logger.info(statementDate)
            qry = "select TXT_60_001,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount,GL_ACCOUNT_NUMBER" \
                  " from cash_output_txn where recon_id ='RA_CASH_APAC_61204' and record_status= 'ACTIVE' and" \
                  " statement_date = to_date('" + statementDate + "','dd/mm/yy')"

            df = pandas.read_sql(qry, connection)

            if len(df) > 0:

                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']
                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1'].copy()
                dfs1.rename(columns={'TXT_60_001': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2'].copy()
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]

                df = dfs1.append(dfs2, ignore_index=True)
                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = (df.loc[:, 'AMOUNTD'] - df.loc[:, 'AMOUNTC']).abs()
                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                if 'AMOUNTC' not in df.columns:
                    df['AMOUNTC'] = 0.0

                if 'AMOUNTD' not in df.columns:
                    df['AMOUNTD'] = 0.0
                logger.info(df)
                # exit(0)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()
                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                df_out = dict()
                status, data = self.getNaturalAcctNum()
                if status:
                    # check if column exists or not
                    if 'C40' in data.columns and 'ACCOUNT_NAME' in data.columns:
                        data['C40'] = data['C40'].astype(str)
                        df = pandas.merge(df, data, on='C40', how='left')
                        df_out['reportData'] = df.to_json(orient='records')
                        df_out['sourceDetails'] = sourceDict
                    else:
                        df_out['error'] = True
                        df_out['error_msg'] = 'Invalid Account Master file format'
                else:
                    df_out['error'] = True
                    df_out['error_msg'] = 'Account Master file is invalid / does not exists'
                return df_out
            else:
                return {}

    # AST report
    def AST_18485(self, statementDate):
        (status, connection) = sql.getConnection()
        if status:
            logger.info(statementDate)
            qry = "select ACCOUNT_NUMBER,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount,TXT_60_001" \
                  " from cash_output_txn where recon_id ='AST_CASH_APAC_18485' and record_status= 'ACTIVE' and" \
                  " statement_date = to_date('" + statementDate + "','dd/mm/yy')"

            df = pandas.read_sql(qry, connection)
            if len(df) > 0:

                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[
                        ['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']
                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1'].copy()
                # dfs1.loc[:, 'C40'] = dfs1.loc[:, 'ACCOUNT_NUMBER'].astype('str').str[12:17]
                dfs1.rename(columns={'TXT_60_001': 'C40'}, inplace=True)
                # dfs1.loc[:, 'C40'] = dfs1.loc[:, 'C40'].astype(int).astype(str)

                source1 = dfs1['SOURCE_NAME'].unique()
                dfs1['CLOSING_BALS1'] = dfs1.apply(
                    lambda x: x['AMOUNT'] if x['DEBIT_CREDIT_INDICATOR'] == 'D' else -x['AMOUNT'], axis=1)
                dfg1 = dfs1.groupby(
                    ['SOURCE_TYPE', 'SOURCE_NAME', 'DEBIT_CREDIT_INDICATOR', 'C40']).sum().reset_index()
                dfgroup1 = dfg1.groupby(['C40']).sum().reset_index()
                pandas.set_option('display.float_format', lambda x: '%.3f' % x)

                dfgroup1['SOURCE_NAME'] = source1[0]
                del dfgroup1['AMOUNT']

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2'].copy()

                source2 = dfs2['SOURCE_NAME'].unique()
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'ACCOUNT_NUMBER'].astype('str').str[12:17]

                dfs2['CLOSING_BALS2'] = dfs2.apply(
                    lambda x: x['AMOUNT'] if x['DEBIT_CREDIT_INDICATOR'] == 'D' else -x['AMOUNT'],
                    axis=1)
                dfg2 = dfs2.groupby(
                    ['SOURCE_TYPE', 'SOURCE_NAME', 'DEBIT_CREDIT_INDICATOR', 'C40']).sum().reset_index()
                dfgroup2 = dfg2.groupby(['C40']).sum().reset_index()

                dfgroup2['SOURCE_NAME'] = source2[0]
                del dfgroup2['AMOUNT']

                merge = pandas.merge(dfgroup1, dfgroup2, how='outer', on=['C40'])
                merge.fillna(0.0, inplace=True)
                merge = merge.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                merge['DIFF'] = merge['CLOSING_BALS1'] - merge['CLOSING_BALS2']
                pandas.set_option('display.float_format', lambda x: '%.3f' % x)

                # print merge['Difference']
                # sourceDict={"S1":"NOVOPAY","S2":"TRIAL"}

                merge['VALUE_GRE'] = merge.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                merge.drop(['SOURCE_NAME_x', 'SOURCE_NAME_y'], axis=1, inplace=True)

                # print merge
                df_out = dict()
                status, data = self.getNaturalAcctNum()

                if status:
                    # check if column exists or not
                    if 'C40' in data.columns and 'ACCOUNT_NAME' in data.columns:
                        data['C40'] = data['C40'].astype(str)
                        merge = pandas.merge(merge, data, on='C40', how='left')
                        df_out['reportData'] = merge.to_json(orient='records')
                        df_out['sourceDetails'] = sourceDict
                    else:
                        df_out['error'] = True
                        df_out['error_msg'] = 'Invalid Account Master file format'
                else:
                    df_out['error'] = True
                    df_out['error_msg'] = 'Account Master file is invalid / does not exists'
                return df_out
        else:
            return {}

    # RSystem v/s CBS Balance Report (RA_CASH_APAC_61207)
    def RA_61207(self, statementDate):
        (status, connection) = sql.getConnection()

        if status:
            qry = "select NATURAL_AC,Account_number,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount" \
                  " from cash_output_txn where recon_id ='RA_CASH_APAC_61207' and record_status= 'ACTIVE'" \
                  " and statement_date = to_date('" + statementDate + "','dd/mm/yy') "

            df = pandas.read_sql(qry, connection)

            if len(df) > 0:
                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']

                accountNumberMap = {'98160102080': '80104', '98159102083': '80117'}

                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1'].copy()
                dfs1.rename(columns={'NATURAL_AC': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2'].copy()
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'ACCOUNT_NUMBER'].apply(lambda x: accountNumberMap[str(x).lstrip('0')])

                df = dfs1.append(dfs2, ignore_index=True)

                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = df.loc[:, 'AMOUNTD'].abs() - df.loc[:, 'AMOUNTC'].abs()
                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()

                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                df_out = dict()
                status, data = self.getNaturalAcctNum()
                if status:
                    # check if column exists or not
                    if 'C40' in data.columns and 'ACCOUNT_NAME' in data.columns:
                        data['C40'] = data['C40'].astype(str)
                        df = pandas.merge(df, data, on='C40', how='left')
                        df_out['reportData'] = df.to_json(orient='records')
                        df_out['sourceDetails'] = sourceDict
                    else:
                        df_out['error'] = True
                        df_out['error_msg'] = 'Invalid Account Master file format'
                else:
                    df_out['error'] = True
                    df_out['error_msg'] = 'Account Master file is invalid / does not exists'
                return df_out
            else:
                return {}

    # Indus v/s OGL Balance Report (TW_CASH_APAC_61208)
    def TW_61208(self, statementDate):
        (status, connection) = sql.getConnection()
        if status:
            # exeID = sql.getLatestExecutionID(connection, 'TW_CASH_APAC_61208')
            logger.info(statementDate)

            qry = "select TXT_32_001,source_type,source_name,DEBIT_CREDIT_INDICATOR,amount,GL_ACCOUNT_NUMBER" \
                  " from cash_output_txn where recon_id ='TW_CASH_APAC_61208' and record_status= 'ACTIVE' and statement_date = to_date('" + statementDate + "','dd/mm/yy')"

            df = pandas.read_sql(qry, connection)
            df['AMOUNT'] = df['AMOUNT'].abs()

            if len(df) > 0:

                sourceDict = \
                    df.drop_duplicates(subset='SOURCE_TYPE', keep='last')[['SOURCE_TYPE', 'SOURCE_NAME']].set_index(
                        'SOURCE_TYPE').T.to_dict(orient="index")['SOURCE_NAME']
                # Source 1
                dfs1 = df[df['SOURCE_TYPE'] == 'S1']
                dfs1.rename(columns={'TXT_32_001': 'C40'}, inplace=True)

                # Source 2
                dfs2 = df[df['SOURCE_TYPE'] == 'S2']
                dfs2.loc[:, 'C40'] = dfs2.loc[:, 'GL_ACCOUNT_NUMBER'].astype('str').str[12:17]

                df = dfs1.append(dfs2, ignore_index=True)

                df = pandas.pivot_table(df, index=['C40', 'SOURCE_TYPE'],
                                        columns=['DEBIT_CREDIT_INDICATOR'], values=['AMOUNT'],
                                        aggfunc={"AMOUNT": np.sum})
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df.loc[:, 'CLOSING_BAL'] = df.loc[:, 'AMOUNTD'].abs() - df.loc[:, 'AMOUNTC'].abs()

                df = pandas.pivot_table(df, index=['C40'], columns=['SOURCE_TYPE'], values=['CLOSING_BAL'])
                df = df.reset_index()
                df.columns = [''.join(list(col)) for col in df.columns.values]
                df.fillna(0.0, inplace=True)

                df = df.round({'CLOSING_BALS1': 2, 'CLOSING_BALS2': 2})
                logger.info(df[['C40', 'CLOSING_BALS1', 'CLOSING_BALS2']])

                df['DIFF'] = df['CLOSING_BALS1'].abs() - df['CLOSING_BALS2'].abs()

                df['VALUE_GRE'] = df.apply(lambda x: self.checkVal(x, sourceDict), axis=1)

                df_out = dict()
                status, data = self.getNaturalAcctNum()
                if status:
                    # check if column exists or not
                    if 'C40' in data.columns and 'ACCOUNT_NAME' in data.columns:
                        data['C40'] = data['C40'].astype(str)
                        df = pandas.merge(df, data, on='C40', how='left')
                        df_out['reportData'] = df.to_json(orient='records')
                        df_out['sourceDetails'] = sourceDict
                    else:
                        df_out['error'] = True
                        df_out['error_msg'] = 'Invalid Account Master file format'
                else:
                    df_out['error'] = True
                    df_out['error_msg'] = 'Account Master file is invalid / does not exists'
                return df_out
            else:
                return {}

    def getNaturalAcctNum(self):
        if os.path.isfile('./Account_Master/ACCOUNT_MASTER.csv'):
            accnum_df = pandas.read_csv('./Account_Master/ACCOUNT_MASTER.csv')
            return True, accnum_df
        else:
            return False, ''

    def checkVal(self, row, sourceDict):
        return 0 if abs(row['CLOSING_BALS1']) - abs(row['CLOSING_BALS2']) == 0 \
            else sourceDict['S1'] if abs(row['CLOSING_BALS1']) > abs(row['CLOSING_BALS2']) else sourceDict['S2']


class NaturalAccMaster(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NaturalAccMaster, self).__init__("naturalAccMaster", hideFields)


class NotificationRule(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NotificationRule, self).__init__("notificationRule", hideFields)


class ReconGroup(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconGroup, self).__init__("recon_groups", hideFields)


class Audits(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Audits, self).__init__("auditTrails", hideFields)


class RecordsCount(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(RecordsCount, self).__init__("record_count", hideFields)


class NotificationMailGroup(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(NotificationMailGroup, self).__init__("mailGroup", hideFields)

    def create(self, doc):
        (status, mailGroup) = super(NotificationMailGroup, self).create(doc)
        return status, mailGroup


class ReconExecutionDetailsLog(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconExecutionDetailsLog, self).__init__("recon_execution_details_log", hideFields)

    def getLatestExeID(self, reconId):
        doc = ReconExecutionDetailsLog().find({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                               'PROCESSING_STATE_STATUS': 'Matching Completed',
                                               'RECORD_STATUS': 'ACTIVE'
                                               }).sort([('RECON_EXECUTION_ID', pymongo.DESCENDING)])
        doc = list(doc)
        return str(doc[0]['RECON_EXECUTION_ID']) if len(doc) > 0 else ''

    def getLatestExeDetails(self, reconId):
        doc = ReconExecutionDetailsLog().find_one({'RECON_ID': reconId, 'EXECUTION_STATUS': 'Completed',
                                                   'PROCESSING_STATE_STATUS': 'Matching Completed',
                                                   'RECORD_STATUS': 'ACTIVE'
                                                   }, sort=[('RECON_EXECUTION_ID', pymongo.DESCENDING)])
        return doc

    def getCurrentStatus(self, id, query):
        query['STATEMENT_DATE'] = datetime.datetime.strptime(query['STATEMENT_DATE'], '%d-%b-%Y')
        doc = list(
            ReconExecutionDetailsLog().find({'RECON_ID': query['RECON_ID']
                                             }, sort=[('RECON_EXECUTION_ID', DESCENDING)]))
        df = pandas.DataFrame(columns=['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID'])
        if len(doc):
            df = pandas.DataFrame(doc)
            # print df['RECON_EXECUTION_ID']
            df['RECON_EXECUTION_ID'] = df['RECON_EXECUTION_ID'].astype(np.int64)
            # print df['RECON_EXECUTION_ID'].unique()
            df = df[(df['STATEMENT_DATE'] >= query['STATEMENT_DATE']) & (
                    df['RECON_EXECUTION_ID'] == df['RECON_EXECUTION_ID'].max())]
            # print df[['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID']]
            df['RECORD_VERSION'] = df['RECORD_VERSION'].astype(np.int64)
            df.sort_values(by=['RECORD_VERSION'], inplace=True)
            df.index = range(1, len(df) + 1)
            df = df[['PROCESSING_STATE_STATUS', 'RECORD_VERSION', 'RECON_EXECUTION_ID']]
        return True, df.T.to_dict().values()

    def getReconWiseMaxExecution(self, reconId=[], stmtDate=None):
        from bson.son import SON
        if stmtDate is None:
            pipeline = [{'$match': {"PROCESSING_STATE_STATUS": "Matching Completed", "RECORD_STATUS": "ACTIVE",
                                    'RECON_ID': {'$in': reconId}}},
                        {'$group': {'_id': '$RECON_ID', 'max': {'$max': '$RECON_EXECUTION_ID'}}},
                        {"$sort": SON([("_id", -1)])}]
        if stmtDate is not None:
            pipeline = [{'$match': {"PROCESSING_STATE_STATUS": "Matching Completed", "RECORD_STATUS": "ACTIVE",
                                    'STATEMENT_DATE': {'$gte': stmtDate, '$lte': stmtDate},
                                    'RECON_ID': {'$in': reconId}}},
                        {'$group': {'_id': '$RECON_ID', 'max': {'$max': '$RECON_EXECUTION_ID'}}},
                        {"$sort": SON([("_id", -1)])}]
        agg_dict = ReconExecutionDetailsLog().aggregate(pipeline=pipeline)

        if 'result' in agg_dict and agg_dict['result'] is not None:
            agg_dict = agg_dict['result']
            return True, agg_dict
        else:
            return False, {}


class GstReports(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstReports, self).__init__("gstReports", hideFields)


class GstSources(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstSources, self).__init__("gstSources", hideFields)


class GSTSourceProductGroup(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GSTSourceProductGroup, self).__init__("gstSrcProductGroup", hideFields)

    def load_allocated_sources(self, id):
        status, userPool = UserPool().getAll({"condition": {"users": flask.session['sessionData']['userName']}})
        gst_sources = []
        for l in userPool['data']:
            gst_sources.extend(l.get('gst_sources', '').split(','))

        return GSTSourceProductGroup().getAll({"condition": {"sourceName": {'$in': gst_sources}}})


class GSTProcessor():
    def __init__(self, path, statementDate):
        self.basepath = config.flaskPath + config.gst_basepath
        self.filepath = self.basepath + path + "/" + statementDate + "/"
        self.outputPath = self.filepath + "output"
        self.statementDate = statementDate
        self.outputdir = self.outputPath + self.statementDate + '/'
        if not os.path.exists(self.outputdir):
            os.makedirs(self.outputdir)

        self.data = {}
        self.ogldata = None

        # load all the state codes
        self.stateCodeLookup = {}
        self.branchCodeLookup = {}
        status, refId = GstStateCodeMap().getDistinctStateCode('')
        for _x in refId:
            status, stateCodes = GstStateCodeMap().getAll({"condition": {"refIdentifier": _x}})
            if status and stateCodes['total'] > 0:
                stateCodes = pandas.DataFrame(stateCodes['data']).rename(
                    columns={"stateName": "STATE NAME", "stateCode": "STATE CODE", "branchCode": "BRANCH"})
                stateCodes['STATE CODE'] = stateCodes['STATE CODE'].astype(str).str.rjust(2, '0')
                stateCodes = stateCodes[stateCodes.columns].fillna('').astype(str)

                self.stateCodeLookup[_x] = stateCodes.drop_duplicates(subset=['STATE NAME', 'STATE CODE'])
                self.branchCodeLookup[_x] = stateCodes.drop_duplicates(subset=['BRANCH'])
            else:
                pass

        # load GST Sources Configured
        status, data = GSTSourceProductGroup().getAll({})
        self.sourceproductgroup = data['data']
        self.sourceproductgroup = pandas.DataFrame(self.sourceproductgroup).drop_duplicates(subset=['sourceName'])
        self.sourceprefixes = self.sourceproductgroup['sourceName'].unique().tolist()

        self.finaldata = {}
        self.totaldata = []
        self.frozedSrcList = []
        self.template = {"STATE CODE": 0, "TAX": 0.00, "STATE NAME": "", "SOURCE": "", "TRANSACTION COUNT": 0,
                         "STATEMENT DATE": statementDate}
        self.globalframe = pandas.DataFrame()

        if os.path.exists('/opt/gst_reports/%s' % self.statementDate):
            os.system('rm -R /opt/gst_reports/%s/' % self.statementDate)

        os.mkdir('/opt/gst_reports/%s' % self.statementDate)

    def recursive_glob(self, rootdir='.', pattern='*'):
        """Search recursively for files matching a specified pattern.

        Adapted from http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
        """
        matches = []
        for root, dirnames, filenames in os.walk(rootdir):
            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
        return matches

    def processFrame(self, df, source, reversal=1):
        lookup_on = ''

        for x in ['STATE CODE', 'STATE NAME', 'BRANCH']:
            if x in df.columns:
                lookup_on = x
                break

        if lookup_on:
            if lookup_on == 'STATE CODE':
                df[lookup_on] = df[lookup_on].astype(str).str.rjust(2, '0')

            df[lookup_on] = df[lookup_on].fillna('').astype(str)

            if lookup_on == 'BRANCH':
                lookup_frame = self.branchCodeLookup[source['stateCodeRef']]
            else:
                lookup_frame = self.stateCodeLookup[source['stateCodeRef']]

            df = df.merge(lookup_frame, how="left", on=[lookup_on])
            df.dropna(subset=[lookup_on], inplace=True)
        else:
            df['STATE NAME'] = 'Maharashtra'

        # in-case state code / name / branch is not defined, set state name to Maharashtra as default.
        df.loc[df['STATE NAME'] == '', 'STATE NAME'] = 'Maharashtra'

        df['SOURCE'] = df.get('SOURCE', source['sourceName'])
        df['OGLSOURCE'] = source.get('matchingSource', '')
        df['STATEMENT DATE'] = self.statementDate

        df['CHARGE VALUE'] = df.get('CHARGE VALUE', 0)
        df.rename(columns={'CHARGE VALUE': 'CHARGE VALUE_'}, inplace=True)
        df['CHARGE VALUE'] = df[['CHARGE VALUE_']].sum(axis=1, skipna=True).apply(
            lambda x: self.stringToFloat(x)) * reversal
        df.drop(labels='CHARGE VALUE_', inplace=True, errors='ignore', axis=1)

        df['TAX VALUE'] = df.get('TAX VALUE', 0)
        df.rename(columns={'TAX VALUE': 'TAX VALUE_'}, inplace=True)
        df['TAX VALUE'] = df[['TAX VALUE_']].sum(axis=1, skipna=True).apply(lambda x: self.stringToFloat(x)) * reversal
        df.drop(labels='TAX VALUE_', inplace=True, errors='ignore', axis=1)

        if 'REVERSAL' in df.columns:
            df['REVERSAL'] = df['REVERSAL'].fillna('N')
            logger.info("Reversal Flags ...")
            logger.info(df['REVERSAL'].unique().tolist())
            df.loc[df['REVERSAL'].fillna('') != 'N', ['TAX VALUE', 'CHARGE VALUE']] *= -1

        self.globalframe = self.globalframe.append(df, ignore_index=True)

    def loadFiles(self):
        status, rollback = GstMetaInfo().getAll(
            {"condition": {'statementdate': datetime.datetime.strptime(self.statementDate, '%Y%m%d'),
                           'rollback': True}})
        loadSources = self.sourceproductgroup.to_dict(orient='records')
        self.process_files = {}

        if status and rollback['total']:
            # OGL Default Base Source
            _filter = [x['sourceCode'] for x in rollback['data']] + config.delta_sources
            loadSources = [x for x in loadSources if x['sourceCode'] in _filter]
            logger.info('Loading rollback sources %s' % len(_filter))
        else:
            pass

        for source in loadSources:
            status, feedStructs = GstFeedDef().findFeedDetails('', {"sourceCode": source['sourceCode']})
            for feed_struct in feedStructs['data']:
                file_name = feed_struct['fileNamePattern']
                logger.info("loading file %s" % file_name)
                if 'fieldDetails' in feed_struct.keys():
                    if '#' in feed_struct['fileNamePattern']:
                        _f = feed_struct['fileNamePattern']
                        d_pattern = _f[_f.index('#') + 1: _f.rindex('#')]
                        file_name = _f.replace('#%s#' % d_pattern, datetime.datetime.strptime(self.statementDate,
                                                                                              '%Y%m%d').strftime(
                            d_pattern))

                    if feed_struct.get('mft') is None:
                        logger.info('MFT Path not defined for gst file %s' % file_name)
                        continue
                    else:
                        file_path = config.gst_basepath + '/' + feed_struct['mft']

                    if config.rsyncGSTFiles:
                        rsync_cmd = config.gst_rsync_cmd.format(
                            feed_struct['mft'], file_path)
                        files_list = config.gst_files_list.format(
                            feed_struct['mft'])
			rm_cmd = config.gst_rm_cmd.format(file_path)
                        os.system(files_list)
                        os.system(rsync_cmd)
			os.system(rm_cmd)

                    if not os.path.exists(file_path):
                        logger.info('File %s not found.' % file_path)
                        continue

                    files = [file_path + '/' + file for file in os.listdir(file_path) if
                             fnmatch.fnmatch(file, file_name)]
                    logger.info("loading data for source %s" % source['sourceCode'])
                    logger.info('Found %s files with pattern %s' % (len(files), file_name))

                    field_details = feed_struct.get('fieldDetails', [])
                    use_cols = [int(x['position']) - 1 for x in field_details if x.get('position')]
                    columns = [x['fieldName'] for x in field_details]
                    # if position specified load only those columns with position
                    if use_cols:
                        columns = [columns[idx] for idx, _ in enumerate(use_cols)]

                    mappedColumns = {x['fieldName']: x['mappedCol'] for x in field_details if x['mappedCol']}
                    skiprows, skipfooter = feed_struct.get('skipTopRows', 0), feed_struct.get('skipBottomRows', 0)

                    for file in files:
                        logger.info('loading file %s' % file)
                        df = pandas.DataFrame()

                        # Load OGL Source files (Base Source)
                        # if source['sourceName'] == 'OGL':
                        #     table_list = pandas.read_html(file, skiprows=1)
                        #     if True:
                        #         df = table_list[0]
                        #         df.columns = columns
                        #         df.rename(columns=mappedColumns, inplace=True)
                        #         accounts = pandas.read_csv(
                        #             config.flaskPath + "/GST_Reference/Natural Accounts OGL Filter.csv")
                        #         df = df[df["NATURAL_AC"].isin(accounts["NATURAL_AC"].unique())]

                        # Load OGL Source files (Base Source)
                        # if source['sourceName'] == 'OGL':
                        #     df = pandas.read_csv(file, sep='|',skiprows=1,names=columns,skipfooter=1)
                        #     if True:
                        #         df.rename(columns=mappedColumns, inplace=True)
                        #         accounts = pandas.read_csv(
                        #             config.flaskPath + "/GST_Reference/Natural Accounts OGL Filter.csv")
                        #         df = df[df["NATURAL_AC"].isin(accounts["NATURAL_AC"].unique())]

                        if feed_struct['format'] == 'Delimiter':
                            df = pandas.read_csv(file, names=columns, delimiter=str(feed_struct['delimiter']),
                                                 skiprows=int(skiprows if skiprows else 0),
                                                 usecols=use_cols if use_cols else None,
                                                 skipfooter=int(skipfooter if skipfooter else 0), header=None)
                        elif feed_struct['format'] == 'Excel':
                            work_book = pandas.ExcelFile(file)
                            df = pandas.read_excel(work_book, names=columns, sheetname=feed_struct.get('sheetName', 0),
                                                   skiprows=int(skiprows if skiprows else 0),
                                                   parse_cols=use_cols if use_cols else None,
                                                   skipfooter=int(skipfooter if skipfooter else 0), header=None)
                            df = df[range(0, len(columns))]
                            df.columns = columns
                        else:
                            logger.info("Un-known feed format type %s" % feed_struct['format'])

                        if source['sourceName'] == 'OGL':
                            df.rename(columns=mappedColumns, inplace=True)
                            accounts = pandas.read_csv(
                                config.flaskPath + "/GST_Reference/Natural Accounts OGL Filter.csv")
                            df = df[df["NATURAL_AC"].isin(accounts["NATURAL_AC"].unique())]

                        if feed_struct.get('filters', []):
                            for exp in feed_struct['filters']:
                                exec exp

                        # update static columns to feed files column
                        df.rename(columns=mappedColumns, inplace=True)
                        self.processFrame(df, source, -1 if feed_struct.get('isReversal') else 1)
                        if feed_struct.get('moveToProcess'):
                            self.process_files[file_path] = file
                else:
                    logger.info("Feed definition / Field Details not found for source %s" % source)
                    pass

        if self.globalframe.empty:
            self.ogldata = pandas.DataFrame()
            return

        self.globalframe.reset_index(drop=True, inplace=True)

        # load sources to be matched against OGL(incase of rollback, filter need to be applied)
        self.ogldata = self.globalframe[self.globalframe['SOURCE'] == 'OGL']
        self.ogldata = self.ogldata[self.ogldata['Source'].isin([x['sourceName'] for x in loadSources])]

        self.globalframe = self.globalframe[self.globalframe['SOURCE'] != 'OGL']

    def stringToFloat(self, x):
        if pandas.isnull(x):
            return np.nan
        elif isinstance(x, float) or isinstance(x, int):
            return x
        else:
            x = re.sub('[^0-9\.\-]', '', x)
            r = "".join(re.split('\,', x))
            val = str(r).strip()
            if len(val) > 0:
                return np.float64(val)
            else:
                return np.nan

    def computeGST(self):
        if self.globalframe.empty:
            return

        df = self.globalframe[
            ["STATE NAME", "STATEMENT DATE", "CHARGE VALUE", "TAX VALUE", "SOURCE", "OGLSOURCE"]].groupby(
            ["SOURCE", "OGLSOURCE", "STATEMENT DATE", "STATE NAME"]).agg(['sum', 'count']).reset_index()
        # df.rename(columns={"SOURCE NAME":"SOURCE"},inplace=True)

        df["TAX"] = df["TAX VALUE"]['sum']
        df["CHARGE"] = df["CHARGE VALUE"]['sum']
        df["TRANSACTION COUNT"] = df["TAX VALUE"]['count']
        # df["SOURCE"] = source
        df = df[["STATE NAME", "STATEMENT DATE", "TAX", "CHARGE", "TRANSACTION COUNT", "SOURCE", "OGLSOURCE"]]
        df['TAX'] = df['TAX'].apply(lambda x: round(x, 2))
        df['CHARGE'] = df['CHARGE'].apply(lambda x: round(x, 2))
        df.columns = df.columns.droplevel(level=1)
        data = json.loads(df.to_json(orient='records'))
        for x in data:
            if x['STATE NAME'] not in self.finaldata:
                self.finaldata[x['STATE NAME']] = []
            self.finaldata[x['STATE NAME']].append(x)

    def mapState(self):
        if self.ogldata.empty:
            return

        self.ogldata = self.ogldata[["Source", "STATE CODE", "STATE NAME", "TAX VALUE", "STATEMENT DATE"]]
        # self.data = self.sourceproductgroup.rename(columns={"sourceName": "SOURCE"})
        # self.ogldata = self.ogldata.merge(self.data, how="left", on="SOURCE")
        self.ogldata = self.ogldata.rename(columns={"Source": "MATCHSOURCE"})
        self.ogldata = self.ogldata.groupby(["MATCHSOURCE", "STATE CODE", "STATE NAME", "STATEMENT DATE"]).agg(
            ['sum', 'count']).reset_index()

        self.ogldata["TAX"] = self.ogldata["TAX VALUE"]['sum']
        self.ogldata["TRANSACTION COUNT"] = self.ogldata["TAX VALUE"]['count']
        self.ogldata = self.ogldata[["MATCHSOURCE", "STATE NAME", "TAX", "TRANSACTION COUNT", "STATEMENT DATE"]]

        self.ogldata["TAX"] = self.ogldata["TAX"].apply(lambda x: round(x, 2))
        self.ogldata.columns = self.ogldata.columns.droplevel(level=1)
        self.ogldata.to_csv(self.outputdir + "OGL.csv")
        self.ogldata["SOURCE"] = "OGL"

        data = json.loads(self.ogldata.to_json(orient='records'))
        for x in data:
            if x['STATE NAME'] not in self.finaldata:
                self.finaldata[x['STATE NAME']] = []
            self.finaldata[x['STATE NAME']].append(x)

        for x in self.finaldata.keys():
            autogen = deepcopy(self.sourceprefixes)
            for val in self.finaldata[x]:
                if val["SOURCE"] == 'OGL':
                    self.totaldata.append(val)
                    continue
                if val["SOURCE"] in autogen:
                    autogen.remove(val["SOURCE"])
                    self.totaldata.append(val)

            for src in autogen:
                if src == "OGL":
                    continue
                data = deepcopy(self.template)
                data["SOURCE"] = src
                data["STATE NAME"] = val["STATE NAME"]
                self.totaldata.append(data)

        return {}

    def saveResults(self):
        # if GST data already exists for statement date, assume rollback & check for frozen sources
        logger.info('Saving GST Results..')
        status, report_data = GstReports().get({"statementdate": self.statementDate})
        if status:
            report_data['data'].extend(self.totaldata)
            status, _ = GstReports().modify({"statementdate": self.statementDate}, report_data)
        else:
            status, _ = GstReports().create(
                {"statementdate": self.statementDate, "data": self.totaldata, "year": str(self.statementDate[0:4]),
                 'partnerId': "54619c820b1c8b1ff0166dfc"})

        # move files to process folder
        for source_path, file_path in self.process_files.iteritems():
            if not os.path.exists(source_path + os.sep + 'processed'):
                os.system('mkdir %s' % source_path + os.sep + 'processed')

            if os.path.exists(file_path):
                os.system('mv "%s" "%s/processed"' % (file_path, source_path))

    def filterData(self, allowedSources, data):
        filtereddata = []
        for x in data:
            if x["SOURCE"] == "OGL":
                if x["MATCHSOURCE"] in allowedSources:
                    filtereddata.append(x)
            elif x["SOURCE"] in allowedSources:
                filtereddata.append(x)
        return filtereddata

    def queryYTDData(self):
        records = []
        query = {"year": str(datetime.datetime.now().year)}
        if self.statementDate:
            query['statementdate'] = {"$lte": self.statementDate}

        condition = {"condition": query}
        status, records = GstReports().getAll(condition)

        allrecords = []
        if status and records['data']:
            for x in records['data']:
                allrecords.extend(x['data'])
        else:
            pass
        return allrecords

    def getYTD(self, allowedSources):
        allrecords = self.queryYTDData()
        if allrecords:
            allrecords = self.filterData(allowedSources, allrecords)
            df = pandas.read_json(json.dumps(allrecords), orient="records")
            df = df[["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT"]].groupby(
                ["STATE NAME", "SOURCE"]).sum().reset_index()

            return df.to_dict(orient='records')
        else:
            return []

    def getPreviousOGLBal(self, allowedSources={}):
        pipe_lines = [{'$match': {'statementdate': {'$lt': self.statementDate}}},
                      {'$group': {'_id': 'statementdate', 'max': {'$max': '$statementdate'}}},
                      {"$sort": SON([("_id", -1)])}]
        result = GstReports().aggregate(pipe_lines).get('result', [])

        if result:
            status, doc = GstReports().get({"statementdate": str(result[0]['max'])})
            prev_bal = doc['data']
            if status and prev_bal:
                if allowedSources:
                    # don't consider CBS_GL_01 extract for previous day computation
                    allowedSources = [s for s in allowedSources if not s.startswith('CBS_GL')]
                    prev_bal = self.filterData(allowedSources, prev_bal)

                prev_bal = pandas.DataFrame(prev_bal)
                # extract only OGL previous closing bal & sub with today's ogl bal.
                ogl_extract = prev_bal[prev_bal['SOURCE'] == 'OGL']
                ogl_extract[['TAX', 'CHARGE']] = ogl_extract[['TAX', 'CHARGE']].fillna(0.0)
                ogl_extract[['TAX', 'CHARGE']] *= -1
                return ogl_extract.to_dict(orient='records')
            else:
                return []
        else:
            return []

    def getStateWiseGst(self, gstReportData):
        df_all = pandas.DataFrame(gstReportData)
        df_all[['CHARGE', 'TAX']] = df_all[['CHARGE', 'TAX']].fillna(0.0)
        df_ogl_data = df_all[(df_all['SOURCE'] == 'OGL')]

        # diff of prev day OGL balance with current OGL balance.
        prev_bal = self.getPreviousOGLBal()
        if prev_bal:
            prev_bal = pandas.DataFrame(prev_bal)
            df_ogl_data = df_ogl_data.merge(prev_bal, on=['MATCHSOURCE', 'STATE NAME'], suffixes=('', '_'), how='left')
            df_ogl_data['TAX'] = df_ogl_data['TAX_'].abs() - df_ogl_data['TAX'].abs()
            for col in df_ogl_data.columns:
                if "_" in col: del df_ogl_data[col]

        matchSrcList = df_ogl_data['MATCHSOURCE'].unique().tolist()
        sourceBrkList = []
        for src in matchSrcList:
            obj = dict()
            obj['reportName'] = 'OGL v/s ' + src
            df_ogl = df_ogl_data[df_ogl_data['MATCHSOURCE'] == src]
            df_oth = df_all[df_all['SOURCE'] == src]
            df_merge = df_ogl.merge(df_oth, on='STATE NAME', suffixes=('', '_'))
            df_merge['DIFFERENCE'] = df_merge['TAX'] + df_merge['TAX_']
            df_merge = df_merge[['STATE NAME', 'TAX', 'TAX_', 'DIFFERENCE']]
            df_merge.rename(columns={"STATE NAME": "AGG_COLUMN"}, inplace=True)

            df_total = pandas.DataFrame(
                [{"AGG_COLUMN": "Total", 'TAX': df_merge['TAX'].sum(), 'TAX_': df_merge['TAX_'].sum(),
                  'DIFFERENCE': df_merge['DIFFERENCE'].sum()}])
            df_merge = df_merge.append(df_total, ignore_index=True)
            df_merge[['DIFFERENCE', 'TAX']] = df_merge[['DIFFERENCE', 'TAX']].fillna(0.0)
            df_merge = df_merge.round(2)

            obj['subcolumns'] = ['State Name', 'OGL Tax', src + ' Tax', 'Difference']
            obj['data'] = df_merge.to_dict(orient='records')
            sourceBrkList.append(obj)
        return sourceBrkList

    def groupGstByState(self, gstReportData):
        obj = dict()
        resp = []
        gstReportData = pandas.DataFrame(gstReportData)
        df_ogl = gstReportData[(gstReportData['SOURCE'] == 'OGL')]
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]
        df_other = df_other.groupby('STATE NAME').sum()['TAX'].reset_index()
        df_ogl = df_ogl.groupby('STATE NAME').sum()['TAX'].reset_index()
        df_merge = df_ogl.merge(df_other, on='STATE NAME', suffixes=('', '_'), how='outer')
        df_merge['DIFFERENCE'] = df_merge['TAX'] - df_merge['TAX_']
        df_merge.rename(columns={"STATE NAME": "AGG_COLUMN"}, inplace=True)
        df_merge[['TAX', 'TAX_']] = df_merge[['TAX', 'TAX_']].fillna(0.0)
        df_merge['DIFFERENCE'] = df_merge['TAX'] + df_merge['TAX_']

        df_total = pandas.DataFrame(
            [{"AGG_COLUMN": "Total", 'TAX': df_merge['TAX'].sum(), 'TAX_': df_merge['TAX_'].sum(),
              'DIFFERENCE': df_merge['DIFFERENCE'].sum()}])
        df_merge = df_merge.append(df_total, ignore_index=True)
        df_merge[['DIFFERENCE', 'TAX']] = df_merge[['DIFFERENCE', 'TAX']].fillna(0.0)
        df_merge = df_merge.round(2)

        obj['reportName'] = 'State wise GST Balances'
        obj['subcolumns'] = ['State Name', 'OGL Balance', 'Extract Balance', 'Difference']
        obj['data'] = df_merge.to_dict(orient='records')
        resp.append(obj)
        return resp

    def groupGstBySystem(self, gstReportData):
        obj = dict()
        resp = []
        gstReportData = pandas.DataFrame(gstReportData)
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]
        df_ogl = gstReportData[(gstReportData['SOURCE'] == 'OGL')]
        df_other = df_other.groupby('SOURCE').sum()['TAX'].reset_index()
        df_ogl = df_ogl.groupby('MATCHSOURCE').sum()['TAX'].reset_index()

        df_merge = df_ogl.merge(df_other, left_on='MATCHSOURCE', right_on='SOURCE', suffixes=('', '_'), how='outer')
        df_merge = df_merge[['SOURCE', 'TAX', 'TAX_']]
        df_merge = df_merge[df_merge['SOURCE'].notnull()]  # drop source's with NaN values (prev day OGL balance)
        df_merge.rename(columns={"SOURCE": "AGG_COLUMN"}, inplace=True)
        df_merge[['TAX', 'TAX_']] = df_merge[['TAX', 'TAX_']].fillna(0.0)
        df_merge['DIFFERENCE'] = df_merge['TAX'] + df_merge['TAX_']

        # display CBS_GL_01 extract data as indivitual value
        df_merge['CBS_GL_01'] = 0.0
        df_merge.loc[df_merge['AGG_COLUMN'].str.startswith('CBS_GL'), 'CBS_GL_01'] = df_merge['TAX_']
        df_merge.loc[df_merge['AGG_COLUMN'].str.startswith('CBS_GL'), 'TAX_'] = 0.0

        df_total = pandas.DataFrame(
            [{"AGG_COLUMN": "Total", 'TAX': df_merge['TAX'].sum(), 'TAX_': df_merge['TAX_'].sum(),
              'DIFFERENCE': df_merge['DIFFERENCE'].sum(), 'CBS_GL_01': df_merge['CBS_GL_01'].sum()}])
        df_merge = df_merge.append(df_total, ignore_index=True)
        df_merge[['DIFFERENCE', 'TAX']] = df_merge[['DIFFERENCE', 'TAX']].fillna(0.0)
        df_merge = df_merge.round(2)

        obj['reportName'] = 'System wise GST Balances'
        obj['subcolumns'] = ['System', 'OGL Balance', 'CBS_GL_01 Balance', 'Extract Balance', 'Difference']
        obj['data'] = df_merge.to_dict(orient='records')
        resp.append(obj)
        return resp

    def gstReportByTotal(self, gstReportData):
        gstReportData = pandas.DataFrame(gstReportData)
        gstReportData[['TAX', 'CHARGE']] = gstReportData[['TAX', 'CHARGE']].fillna(0.0)
        df_other = gstReportData[(gstReportData['SOURCE'] != 'OGL')]

        # IDFC Total Balance
        total_balance = df_other[["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT", "CHARGE"]].groupby(
            ["STATE NAME"]).sum().reset_index().round(2)
        total_balance['SOURCE'] = 'IDFC Bank Total'

        # Source Wise Total
        source_wise_total = df_other[["STATE NAME", "TAX", "SOURCE", "TRANSACTION COUNT", "CHARGE"]].groupby(
            ["STATE NAME", 'SOURCE']).sum().reset_index().round(2)

        df_total = total_balance.append(source_wise_total, ignore_index=True)
        df_total.fillna('', inplace=True)
        return df_total.to_dict(orient='records')


class GstMetaInfo(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(GstMetaInfo, self).__init__("gstMetaInfo", hideFields)

    def freeze_sources(self, id, input={}):
        logger.info("GST source freeze info: ")
        status, gst_data = GstReports().get({"statementdate": input['statementdate']})

        if status:
            stmt_date = datetime.datetime.strptime(input['statementdate'], '%Y%m%d')
            for src in input.get('sources', []):

                # stmt_date cannot be lesser than the max existing date
                cursor = GstMetaInfo().find(
                    {'sourceCode': src['sourceCode'], "statementdate": {'$gte': stmt_date}})
                if cursor.count():
                    logger.info('"%s" already frozen' % src['sourceCode'])
                    continue

                status, data = GstMetaInfo().get({'sourceCode': src['sourceCode']})

                if status:
                    data['statementdate'] = stmt_date
                    GstMetaInfo().modify({'sourceCode': src['sourceCode']}, data)
                else:
                    GstMetaInfo().create(
                        {'sourceCode': src['sourceCode'], 'sourceName': src["sourceName"],
                         'statementdate': datetime.datetime.strptime(input['statementdate'], '%Y%m%d'),
                         'freezed': True})
            return True, ''
        else:
            return False, 'No data found.'

    def rollback_sources(self, id, query={}):
        logger.info('Rollback GST Source for data ')
        status, gst_data = GstReports().get({"statementdate": query['statementdate']})
        if status:
            # check for frozed sources
            status, frozen_src = GstMetaInfo().getAll(
                {"condition": {'statementdate': {"$gte": datetime.datetime.strptime(query['statementdate'], '%Y%m%d')},
                               'freezed': True}})
            frozen_src = [x['sourceName'] for x in frozen_src['data']]
            rollback_src = [x for x in query['sources'] if x['sourceName'] not in frozen_src]
            src_name_list = [x['sourceName'] for x in rollback_src]
            filter_data = []

            if len(rollback_src) == 0:
                return False, 'Cannot rollback frozen sources.'

            for doc in gst_data['data']:
                # to avoid duplicates from OGL extract use MatchSource.
                if doc['SOURCE'] in src_name_list or doc.get('MATCHSOURCE', '') in src_name_list:
                    continue
                filter_data.append(doc)

            gst_data['data'] = filter_data
            for src in rollback_src:
                status, _doc = GstMetaInfo().get({'sourceCode': src['sourceCode'], 'rollback': True})

                if status:
                    _doc["statementdate"] = datetime.datetime.strptime(query['statementdate'], '%Y%m%d')
                    GstMetaInfo().modify({'sourceCode': src['sourceCode']}, _doc)
                else:
                    GstMetaInfo().create(
                        {"statementdate": datetime.datetime.strptime(query['statementdate'], '%Y%m%d'),
                         'sourceName': src['sourceName'], 'sourceCode': src['sourceCode'], 'rollback': True})

            status, _ = GstReports().modify({"statementdate": query['statementdate']}, gst_data)
            return True, 'Rollback Successful.'

        else:
            return False, 'No data found.'
