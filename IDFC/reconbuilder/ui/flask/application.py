import logger

logger = logger.Logger.getInstance("application").getLogger()
import dbinterface
from flask import request
import re
import pandas
import config
import os
# from datetime import timedelta, date
import json
import uuid
from pymongo import MongoClient


def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)


def getPartnerFilePath():
    return os.path.join(config.commonFilesPath, "theme_templates") + "/"


class ReconProcess(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconProcess, self).__init__("reconprocess", hideFields)

    def create(self, doc):
        logger.info(doc)
        if self.exists({'reconProcessName': {"$regex": "^" + doc['reconProcessName'] + "$", "$options": 'i'}}):
            return False, "Recon Process already exists."
        (status, businessDoc) = super(ReconProcess, self).create(doc)
        return status, businessDoc


class DataSource(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(DataSource, self).__init__("datasource", hideFields)

    def create(self, doc):
        if self.exists({'reconName': {"$regex": "^" + doc['reconName'] + "$", "$options": 'i'}}):
            return False, "reconName already exists."
        doc['reconId'] = str(uuid.uuid4().int & (1 << 64) - 1)
        (status, businessDoc) = super(DataSource, self).create(doc)
        return status, businessDoc


class MatchingRules(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(MatchingRules, self).__init__("matchingrules", hideFields)

    def create(self, doc):
        if self.exists({'reconId': {"$regex": "^" + doc['reconId'] + "$", "$options": 'i'}}):
            return False, "matching conditions are already configured."
        (status, businessDoc) = super(MatchingRules, self).create(doc)
        return status, businessDoc


class Accounts(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Accounts, self).__init__("accounts", hideFields)

    def create(self, doc):
        (status, businessDoc) = super(Accounts, self).create(doc)
        return status, businessDoc


class AccountMap(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(AccountMap, self).__init__("feedAccMapping", hideFields)

    def create(self, doc):
        # if self.exists({'recon': {"$regex": "^" + doc['recon'] + "$", "$options": 'i'},
        #                 'source': {"$regex": "^" + doc['source'] + "$", "$options": 'i'}}):
        #     return False, "Duplicate Account Mapping is not allowed for selecting source under selected recon"
        (status, businessDoc) = super(AccountMap, self).create(doc)
        return status, businessDoc


class AccountPools(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(AccountPools, self).__init__("account_pools", hideFields)

    def create(self, doc):
        (status, businessDoc) = super(AccountPools, self).create(doc)
        return status, businessDoc


class ReconContextDetails(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconContextDetails, self).__init__("recon_context_details", hideFields)

    def create(self, doc):
        logger.info(doc)
        (status, businessDoc) = super(ReconContextDetails, self).create(doc)
        return status, businessDoc


class Utilities(object):
    # def upload(self, id):
    #     data = dict(request.args)
    #     logger.info('daata' + str(data))
    #     # print 'data',data
    #     logger.info(request.files)
    #     logger.info("Received request:" + str(request.files))
    #     contentDir = os.path.join(config.contentDir)
    #     if not os.path.exists(contentDir):
    #         os.makedirs(contentDir)
    #     request.files['file'].save(os.path.join(contentDir, id))
    #     if data['type'][0].lower() == "csv":
    #         df = pandas.read_csv(contentDir + '/' + id)
    #         if data['formatType'][0] == "fixedLength":
    #             if len(pandas.unique(df.loc[:, "POSITION"])) == len(df.index):
    #                 pass
    #             else:
    #                 return False, "Duplicates in position values",
    #     os.remove(contentDir + '/' + id)
    #     return True, df.to_json(orient='records')

    def upload(self, id):
        data = dict(request.args)
        logger.info('daata' + str(data))
        # print 'data',data
        logger.info(request.files)
        logger.info("Received request:" + str(request.files))
        contentDir = os.path.join(config.contentDir)
        if not os.path.exists(contentDir):
            os.makedirs(contentDir)
        request.files['file'].save(os.path.join(contentDir, id))
        df = pandas.read_csv(contentDir + '/' + id)
        logger.info(df)

        # add field details
        # df.columns = [['fieldName','uiDisplayName']]
        # df.rename(columns={'$a': 'a', '$b': 'b'}, inplace=True)
        df.rename(columns={'Field Name': 'displayName', 'Data Type': 'dataType', 'Length': 'length',
                           'Sample Data': 'sampleData'}, inplace=True)
        # df['sampleData'] = df['Sample Data']
        df['displayType'] = 'M'
        os.remove(contentDir + '/' + id)
        return True, df.to_json(orient='records')

    def upload_account_details(self, file_name):
        try:
            logger.info("Received request:" + str(request.files))
            contentDir = os.path.join(config.contentDir, 'account_details')
            logger.info(contentDir)
            if not os.path.exists(contentDir):
                os.makedirs(contentDir)
            request.files['file'].save(os.path.join(contentDir, file_name))

            df_account_master = pandas.read_csv(os.path.join(contentDir, file_name))
            df_account_master.loc[:,'ACCOUNT_NUMBER'] = df_account_master.loc[:,'ACCOUNT_NUMBER'].astype('str')
            account_json = df_account_master.to_json(orient='records')
            account_list = json.loads(account_json)

            # delete existing account details
            Accounts().remove({})
            # insert account numbers
            Accounts().insert(account_list)
            return True, {}
        except Exception, e:
            return False, str(e)

    def upload_mdl_field(self):
        logger.info('Received request:' + str(request.files))


    def getPreLoadScriptData(self,id,query):
        fileLoc = config.enginePath+'custom_scripts/'+query['fileName']
        print fileLoc
        if os.path.exists(fileLoc):
            print open(fileLoc).readlines()
            scriptData = open(fileLoc).readlines()
            return True,scriptData
        return False,'File not exists'


class AccountSetupInterface():
    def __init__(self):
        self.mongo_client = MongoClient('mongodb://localhost:27017/')
        self.mongo_collection = self.mongo_client.accounts

    def getAccountDetails(self, id):
        reconContextList = []
        cc_col = self.mongo_collection['datasource']
        for val in cc_col.find():
            reconContextDict = {}
            reconContextDict['RECON_NAME'] = val['reconName']
            reconContextDict['RECON_ID'] = val['reconId']
            reconContextDict["SOURCE"] = val['sources'].keys()
            reconContextList.append(reconContextDict)

        return True, reconContextList

    def deleteAccountDetails(self):
        return self.mongo_collection.drop()


class BusinessContext(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(BusinessContext, self).__init__("business_context", hideFields)

    def create(self, doc):
        doc['BUSINESS_CONTEXT_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
        (status, businessDoc) = super(BusinessContext, self).create(doc)
        return status, businessDoc


class FeedDefination(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(FeedDefination, self).__init__("feedDefination", hideFields)

    def create(self, doc):
        logger.info(doc)
        (status, businessDoc) = super(FeedDefination, self).create(doc)
        return status, businessDoc


class MdlFieldStaticData(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(MdlFieldStaticData, self).__init__("model_field_staticdata", hideFields)


class SwiftTagsLibrary(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(SwiftTagsLibrary, self).__init__("swift_tags_library", hideFields)

    def create(self, doc):
        (status, swiftTagsLibDoc) = super(SwiftTagsLibrary, self).create(doc)
        return status, swiftTagsLibDoc


class SequenceGeneratorUtility(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        # sequenceGenerator
        super(SequenceGeneratorUtility, self).__init__("sequenceGenerator", hideFields)

    def create(self, doc):
        (status, seqGeneratorDoc) = super(SequenceGeneratorUtility, self).create(doc)
        return status, seqGeneratorDoc


class StaticDataMaster(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(StaticDataMaster, self).__init__("staticDataMaster", hideFields)

    def create(self, doc):
        (status, staticdataDoc) = super(StaticDataMaster, self).create(doc)
        return status, staticdataDoc

    def getStaticDataDetails(self, id, query=[]):
        self.getAll()
        logger.info(query)
        return True, ''


# interface to get the static data types
class StaticDataType(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(StaticDataType, self).__init__("staticDataTypes", hideFields)

    def create(self, doc):
        (status, staticDataTypeDoc) = super(StaticDataType, self).create(doc)
        return status, staticDataTypeDoc

