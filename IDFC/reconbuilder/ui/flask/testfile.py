from pymongo import MongoClient
import pandas as pd

client = MongoClient('mongodb://localhost:27017')
userpool = list(client['erecon']['userpool'].find())
reconassing = list(client['erecon']['reconassignment'].find())
userpool = pd.DataFrame(userpool)
reconassing = pd.DataFrame(reconassing)
reconassing = reconassing[['userpool','recons']]
reconassing = reconassing.groupby('userpool').agg(lambda g: dict([(k,g[k].tolist()) for k in g]))
reconassing = reconassing.reset_index()
details = userpool.merge(reconassing,left_on='name',right_on='userpool')
details = details[['userpool','recons','role']]
details['recons'] = details['recons'].apply(lambda x : ','.join(set(x)))
print details.to_csv('recon_assignment.csv',index=False)