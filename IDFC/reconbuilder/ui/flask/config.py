appUrl = "https://127.0.0.1/"
flaskBaseUrl = '/api'
flaskPath = '/usr/share/nginx/www/reconbuilder/flask/'
uiPath = '/usr/share/nginx/www/reconbuilder/ui/'
commonFilesPath = '/usr/share/nginx/www/reconbuilder/ui/app/'
secretKey = '58e57c3c-76cb-4c5c-8416-0f3226bc5e88' #Change per project
applicationModules = ["application", "usermanagement"]
sessionSalt = 'f80423b7-33f2-4b57-ae97-44ef3f8a0c32' #Change per project
cookieName = 'erecon' #Change per project
cookieDomain = '' #Change per project
sessionTimeoutSecs = 86400

logBaseDir = '/var/log/reconbuilder/' #Change per project
dataBackupDir = '/tmp/data/reconbuilder/' #Change per project

flaskLogFile="apiaccess"
flaskMaxBytes=50000000
flaskBackupCount=100
running = False
contentDir = '/usr/share/nginx/www/reconbuilder/ui/app/files'

usersMongoHost = 'localhost'
usersDatabaseName = 'reconbuilder' #Change per project
mongoHost = 'localhost'
databaseName = 'reconbuilder' #Change per project
isReplicaSet = False
replicaSetName = ''
# ticketServer = "smtp.gmail.com"
# ticketPort = 465
# ticketConnectionSecurity = "SSL/TLS"
# ticketUserName = "hash.izmosales@gmail.com"
# ticketPassword = "hash@1234"
companyDatabaseName = "crmframework"
isAllowTicketCreation = False
####For Calling Purpose
ACCOUNT_SID = "AC16772aca9e88c27b34b9d6f2cf0df11f"
AUTH_TOKEN = "28fe815d7064d6b83e8f07a159f63978"

ticketServer = "smtp.gmail.com"
ticketPort = 465
ticketConnectionSecurity = "SSL/TLS"
ticketUserName = "hash.appointment@gmail.com"
ticketPassword = "hash@1234"

ALLOWED_EXTENSIONS = set([ 'png', 'jpg', 'jpeg', 'gif'])

cash_columnsToConvert = ["LINK_ID","RAW_LINK","PARTICIPATING_RECORD_TXN_ID","OBJECT_OID","OBJECT_ID","RECON_EXECUTION_ID","PREV_LINK_ID"]

#Recon Ids that needs account number valication while forcematching the records
account_number_validation = ['SUSP_CASH_APAC_18646','TRSC_CASH_APAC_20181','TF_CASH_APAC_20063','EPYMTS_NEFT_INWARD_APAC_0009']
execution_summary_columns = ['RECON_ID','RECON_NAME','RECON_EXECUTION_ID','EXECUTION_DATE_TIME','STATEMENT_DATE','RECORD_STATUS','TOTAL_TRANSACTIONS','EXECUTION_COUNT','CARRYFORWARD_COUNT','REASON_CODE','PARTIAL_MATCHED_RECORDS_COUNT','PERFECT_MATCHED_RECORDS_COUNT','TOLERANCE_MATCHED_COUNT','MATCHED_TRANSACTIONS']


staticCols = ['SOURCE_TYPE', 'SOURCE_NAME', 'LINK_ID', 'RECON_EXECUTION_ID', 'EXECUTION_DATE_TIME', 'CREATED_BY', 'CREATED_DATE', 'MATCHING_EXECUTION_STATE', 'STATEMENT_DATE', 'MATCHING_EXECUTION_STATUS', 'MATCHING_STATUS', 'RECONCILIATION_STATUS', 'AUTHORIZATION_STATUS', 'FORCEMATCH_AUTHORIZATION','REASON_CODE','OBJECT_ID','OBJECT_OID','BUSINESS_CONTEXT_ID','GROUPED_FLAG','UPDATED_DATE','UPDATED_BY','TXN_MATCHING_STATE_CODE','TXN_MATCHING_STATUS','TXN_MATCHING_STATE','AUTHORIZATION_BY','RECON_ID','RECORD_STATUS','RECORD_VERSION','SESSION_ID','ENTRY_TYPE','IPADDRESS','RAW_LINK']
enginePath  = '/usr/share/nginx/www/v8'
