# # import json
# # import sys
# # import pprint
# # import cx_Oracle
# # import pandas as pd
# #
# #
# # class FeedDetails():
# #     def __init__(self, reconId):
# #         ip = 'localhost'
# #         port = 1521
# #         SID = 'xe'
# #         dsn_tns = cx_Oracle.makedsn(ip, port, SID)
# #         self.connection = cx_Oracle.connect('algoreconutil_new', 'algorecon', dsn_tns)
# #         self.getFieldDetails(reconId)
# #
# #     def getFieldDetails(self, reconId):
# #         dataTypeMap = {'Text':'str','Date':'np.datetime64','Timestamp':'np.datetime64','Decimal':'np.float64','Integer':'np.int64'}
# #         fieldsArray = []
# #         qry1 = "select * from feed_details where recon_id = '" + reconId + "' and record_status = 'ACTIVE'"
# #         qry2 = "select * from feedstructure_feed_mapping where feed_id = '$feedID'and record_status = 'ACTIVE'"
# #         qry3 = "select * from feed_structure where feedstructure_id = $feedStructID and record_status = 'ACTIVE'"
# #         qry4 = "select * from field_details where feedstructure_id = $feedStructID and record_status = 'ACTIVE' order by POSITION"
# #
# #         df1 = pd.read_sql(qry1, self.connection)
# #         feedDetails = json.loads(df1.to_json(orient='records'))
# #
# #         for val in feedDetails:
# #             tmp = dict()
# #             tmp['headerExists'] = 'true' if val['COLUMN_HEADER_EXISTS'] == 1 else 'false'
# #             tmp['skipTopRows'] = int(val['TOP_SKIP_LINES']) if val['TOP_SKIP_LINES'] else 0
# #             tmp['skipBottomRows'] = int(val['BOTTOM_SKIP_LINES']) if val['BOTTOM_SKIP_LINES'] else 0
# #             tmp['feedId'] = val['FEED_ID']
# #             tmp['format'] = val['FORMAT_TYPE']
# #             if tmp['format'] == 'DELIMITED':
# #                 tmp['delimiter'] = val['DELIMITER_TYPE']
# #             tmp['preLoadScriptIndicator'] = 'false'
# #             tmp['preLoadScript'] = {}
# #             tmp['filters'] = []
# #             tmp['limitFileCount'] = 0
# #             fieldsArray.append(tmp)
# #
# #         for val1 in fieldsArray:
# #             df2 = pd.read_sql(qry2.replace('$feedID',tmp['feedId']),self.connection)
# #             feedStructMap = json.loads(df2.to_json(orient='records'))
# #             val1['fieldDetails'] = []
# #
# #             for val2 in feedStructMap:
# #
# #                 df3 = pd.read_sql(qry3.replace('$feedStructID',str(val2['FEEDSTRUCTURE_ID'])),self.connection)
# #                 val1['feedName'] = json.loads(df3.to_json(orient='records'))[0]['FEEDSTRUCTURE_NAME']
# #                 df4 = pd.read_sql(qry4.replace('$feedStructID',str(val2['FEEDSTRUCTURE_ID'])),self.connection)
# #                 fieldDetails = json.loads(df4.to_json(orient='records'))
# #                 fieldArray = []
# #                 for fieldVal in fieldDetails:
# #                     field = dict()
# #                     field['displayName'] = fieldVal['FIELD_NAME']
# #                     field['mdlFieldName'] = fieldVal['MDL_FIELD_ID']
# #
# #                     field['endIndex'] = fieldVal['END_POSITION'] if fieldVal['END_POSITION'] else 0
# #                     field['startIndex'] = fieldVal['START_POSITION'] if fieldVal['START_POSITION'] else 0
# #                     field['tag'] = ""
# #                     field['position'] = fieldVal['POSITION']
# #                     field['type'] = 'M'
# #                     field['datePattern'] = self.formatDate(fieldVal['DATE_PATTERN']) if fieldVal['DATE_PATTERN'] else ""
# #                     field['dataType'] = dataTypeMap[fieldVal['DATATYPE']]
# #                     val1['fieldDetails'].append(field)
# #
# #         pprint.pprint(json.dumps(fieldsArray))
# #
# #
# #     def formatDate(self,date):
# #         date.replace('MM',"%m").replace('MMM','%b').replace('yyyy',"%Y").replace('yy',"%y").replace('dd','%d')
# #
# #
# #
# #
# # if __name__ == '__main__':
# #     reconId = sys.argv[1]
# #     reconJob = FeedDetails(reconId)
#
# from pymongo import MongoClient
#
# client = MongoClient('mongodb://localhost:27017')
# val_dict = {
#     "created": 1476263783,
#     "description": "",
#     "machineId": "16af9057-69af-4c69-bbcf-70d99334e027",
#     "partnerId": "54619c820b1c8b1ff0166dfc",
#     "staticCode": "",
#     "staticDataType": "CURRENCY",
#     "staticName": "",
#     "updated": 1486366303
# }
#
# arry = []
#
# currency = ['AUD', 'CAD', 'CHF', 'CNY', 'EUR', 'INR', 'KES', 'KWD', 'LIPA', 'POUND', 'SAD', 'USD', 'WSD']
# for val in currency:
#     val_dict['staticCode'] = val
#     val_dict['staticName'] = val
#     val_dict['description'] = val + ' type currency'
#     print id(val_dict)
#     arry.append(val_dict.copy())
# print arry

from pymongo import MongoClient
import pandas as pd

client = MongoClient('mongodb://localhost:27017')
userpool = list(client['erecon']['userpool'].find())
reconassing = list(client['erecon']['reconassignment'].find())
userpool = pd.DataFrame(userpool)
reconassing = pd.DataFrame(reconassing)
reconassing = reconassing[['userpool','recons']]
reconassing = reconassing.groupby('userpool').agg(lambda g: dict([(k,g[k].tolist()) for k in g]))
reconassing = reconassing.reset_index()
details = userpool.merge(reconassing,left_on='name',right_on='userpool')
details[['userpool','recons','role']]