import time
import json
import requests
#requests.packages.urllib3.disable_warnings()

class requestsInterface(object):
    def __init__(self, ip="10.4.8.11:389", userName="admin", password=None, apiKey=None):
        self.ip = ip
        self.userName = userName
        self.password = password
        self.apiKey = apiKey
        self.defaultHeaders = {'content-type': 'application/json'}
        if (self.password is None and self.apiKey is None) and not(ip == "127.0.0.1"):
            raise ValueError("Invalid credentials")

        self.baseurl = "https://%s/api" % self.ip
        self.loginResp = None

        self.rs = requests.Session()
        # TODO have to add proxy settings here
        # http://docs.python-requests.org/en/latest/user/advanced/#proxies
        # proxies = {}
        # proxies = {'http': 'http://user:pass@10.10.1.10:3128/'}
        # proxies = {'http://10.20.1.128': 'http://10.10.1.10:5323'}
        self.login()

    def __del__(self):
        self.logout()

    def login(self):
        self.loginResp = None
        logindata = {}
        logindata["userName"] = self.userName
        if self.password:
            logindata["password"] = self.password
        elif self.apiKey:
            logindata["apiKey"] = self.apiKey

        resp = self.rs.post(self.baseurl, data=json.dumps(logindata), headers=self.defaultHeaders, verify=False)
        if resp.status_code == 200:
            self.loginResp = resp.json()
        else:
            raise ValueError("Login Failed")

    def logout(self):
        self.rs.post(self.baseurl+"/logout", headers=self.defaultHeaders, verify=False)
        self.loginResp = None

    def doRequest(self, method, url, data, params=None, timeout=None):
        #Check if session is valid
        resp = self.rs.get(self.baseurl, verify=False)
        if not resp.status_code == 200:
            self.login()

        meth = getattr(self.rs, method, None)
        if meth:
            if data is None:
                if timeout is not None:
                    resp = meth(url, verify=False, params=params, timeout=timeout)
                else:
                    resp = meth(url, verify=False, params=params)
            elif type(data) is not str:
                resp = meth(url, data=json.dumps(data), params=params, headers=self.defaultHeaders, verify=False)
            else:
                resp = meth(url, data=data, params=params, headers=self.defaultHeaders, verify=False)

            if resp.status_code == 200:
                return resp.json()
            else:
                return "Error: %s" % (str(resp.status_code))
        else:
            print "Unknown method: %s" % (method)

    def get(self, collection, id=None, params=None):
        url = self.baseurl+"/"+collection
        if id is not None:
            url = url+"/"+id
        return self.doRequest("get", url, None, params=params)

    def post(self, collection, data=None):
        url = self.baseurl+"/"+collection
        return self.doRequest("post", url, data)

    def put(self, collection, id, data=None):
        url = self.baseurl+"/"+collection+"/"+id
        return self.doRequest("put", url, data)

    def delete(self, collection, id, data=None):
        url = self.baseurl+"/"+collection+"/"+id
        return self.doRequest("delete", url, data)

    def action(self, collection, id, actionName, data=None, timeout=None):
        url = self.baseurl+"/"+collection+"/"+id+"/"+actionName
        return self.doRequest("post", url, data, timeout=timeout)
