import datetime
import os
import sys
from collections import OrderedDict

import cx_Oracle
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient


class ReconMigration():
    def __init__(self, reconId, foldername):
        try:
            # change per sql database
            self.ip,self.port,self.SID = 'localhost', 1521, 'XE'
            self.user_name, self.pwd = '', ''
            self.app_ip = '10.1.57.53'
            self.engine_ip = '10.1.57.51'
            self.connection = self.getConnection()
            self.mongoClient = self.getMongoConnection()
            self.import_folder = '/home/ubuntu/Desktop/ReconMigration/recon_migration_bkp/'
            self.isFolderValid()
            self.reconId = reconId
            self.folder_name = foldername
            self.getReconDef()
            self.updateBCtxtDetails()
            self.updateWrkPoolDetails()
            self.updateReconDetails()
            self.updateReconDynamicSetup(import_folder=foldername)
            self.updateMongoDB()
            self.commitToSql()
        except:
            raise
            self.rollBack()

    def getConnection(self):
        dsn_tns = cx_Oracle.makedsn(self.ip, self.port, self.SID)
        connection = cx_Oracle.connect(self.user_name, self.pwd, dsn_tns)
        # begin new transaction
        return connection

    def isFolderValid(self, import_folder):
        if os.path.isdir(import_folder + os.sep + str(self.reconId) + os.sep + str(self.folder_name)):
            return ''
        else:
            print 'Invalid folder name {}!!'.format(import_folder)
            exit(0)

    def getMongoConnection(self, host_addr):
        mongo_client = MongoClient("mongodb://'{}':27017".format(self.engine_ip))
        return mongo_client

    def getReconDef(self):
        self.reconDef = self.mongoClient['reconbuilder']['recon_context_details'].find_one({"reconId": self.reconId})

    def getFieldDef(self):
        sourceDetails = self.reconDef['sources']
        self.fieldDetails = []
        duplicateTracker = []
        for val in sourceDetails:
            feedDef = {}
            feedDef = self.mongoClient['reconbuilder']['feedDefination'].find_one({"_id": ObjectId(val['feedId'])},
                                                                                  {"fieldDetails": 1})

            for value in feedDef['fieldDetails']:
                pos = [i for i, x in enumerate(duplicateTracker) if x == value['mdlFieldName']]
                if len(pos) == 0:
                    self.fieldDetails.append(value)
                    duplicateTracker.append(value['mdlFieldName'])

    def updateBCtxtDetails(self):

        bcntxt = self.reconDef['businessContext']
        qry = "select count(*) from business_context where business_context_id ='" + str(
                long(bcntxt['BUSINESS_CONTEXT_ID'])) + "'"
        if self.connection.cursor().execute(qry).fetchone()[0] > 0:
            print "**** Business context already exists, Ignoring ****"
        else:
            qry_1 = 'select max(BUSINESS_CONTEXT_OID)+1 from business_context'
            bcntxt['BUSINESS_CONTEXT_OID'] = self.connection.cursor().execute(qry_1).fetchone()[0]
            df = pd.DataFrame(bcntxt, index=[0])
            df = df[['BUSINESS_CONTEXT_ID', 'BUSINESS_CONTEXT_OID', 'reconProcessCode']]

            df.rename(columns={"reconProcessCode": "RECON_PROCESS_CODE"}, inplace=True)
            df['RECON_PROCESS_ID'] = 00
            df['PRODUCTLINE_ID'] = 1
            df['GEOGRAPHY_ID'] = 1
            df = self.appendAuditRecords(df)
            self.insertDFToDB(df, 'BUSINESS_CONTEXT', self.connection)
            print "**** Business Context Migrated ****"

    def updateWrkPoolDetails(self):

        qry = "select count(*) from workpool where business_context_id ='" + str(long(self.reconDef['businessContext'][
                                                                                          'BUSINESS_CONTEXT_ID'])) + "'"
        if self.connection.cursor().execute(qry).fetchone()[0] > 0:
            print "**** WorkPool already exists,  Ignoring ****"
        else:
            bcntxt = self.reconDef['businessContext']
            qry_1 = 'select max(WORKPOOL_OID)+1 from workpool'
            qry_2 = 'select max(WORKPOOL_ID)+1 from workpool'

            bcntxt['WORKPOOL_OID'] = self.connection.cursor().execute(qry_1).fetchone()[0]
            bcntxt['WORKPOOL_ID'] = self.connection.cursor().execute(qry_1).fetchone()[0]

            df = pd.DataFrame(bcntxt, index=[0])
            df = df[['WORKPOOL_ID', 'WORKPOOL_NAME', 'WORKPOOL_OID', 'BUSINESS_CONTEXT_ID']]

            df = self.appendAuditRecords(df)
            self.insertDFToDB(df, 'WORKPOOL', self.connection)
            print "**** WorkPool Details Migrated ****"

    def updateReconDetails(self):
        qry = "select count(*) from recon_details where recon_id ='" + self.reconDef['reconId'] + "'"
        if self.connection.cursor().execute(qry).fetchone()[0] > 0:
            print "**** Recon Details already exists,  Ignoring ****"
        else:
            reconDetails = {}
            qry_1 = 'select max(recon_oid)+1 from recon_details'

            reconDetails['RECON_ID'] = self.reconDef['reconId']
            reconDetails['BUSINESS_CONTEXT_ID'] = str(self.reconDef['businessContextId'])
            reconDetails['RECON_NAME'] = self.reconDef['reconName']
            reconDetails['BRANCH_ID'] = 0
            reconDetails['GEOGRAPHY_ID'] = 1
            reconDetails['RECON_PROCESS_ID'] = 0
            reconDetails['BUSINESS_PROCESS_ID'] = 0
            reconDetails['PRODUCTLINE_ID'] = 0
            reconDetails['COUNTRY_ID'] = 0
            reconDetails['RECON_OID'] = self.connection.cursor().execute(qry_1).fetchone()[0]
            df = pd.DataFrame(reconDetails, index=[0])
            df = self.appendAuditRecords(df)
            self.insertDFToDB(df, 'RECON_DETAILS', self.connection)

            print "**** Recon Details Migrated ****"

    def updateMongoDB(self):

        erecon_mongo = MongoClient("mongodb://'{}':27017".format(self.app_ip))
        businessDef = erecon_mongo['erecon']['business'].find_one({"RECON_ID": self.reconId})

        if businessDef is None:
            business = dict()
            business['BUSINESS_CONTEXT_ID'] = self.reconDef['businessContextId']
            business['BUSINESS_PROCESS_NAME'] = self.reconDef['businessContext']['BUSINESS_PROCESS_NAME']
            business['WORKPOOL_NAME'] = self.reconDef['businessContext']['WORKPOOL_NAME']
            business['partnerId'] = "54619c820b1c8b1ff0166dfc"
            business['machineId'] = "16af9057-69af-4c69-bbcf-70d99334e027"
            business['RECON_ID'] = self.reconDef['reconId']
            business['RECON_NAME'] = self.reconDef['reconName']
            business['TXN_PROCESSING_LEVEL'] = self.reconDef['processingLevel']
            business['TXN_PROCESSING_TYPE'] = self.reconDef['processingType']

            erecon_mongo['erecon']['business'].insert(business)
            print "**** Recon Details Migrate to Mongo DB ****"
        else:
            print "**** Recon Details Already exists Mongo DB ****"

    def appendAuditRecords(self, df):
        df['CREATED_BY'] = 'AlgoReconIT'
        df['CREATED_DATE'] = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        df['CREATED_DATE'] = pd.to_datetime(df['CREATED_DATE'], format='%Y-%m-%d %H:%M:%S')
        df['IPADDRESS'] = '0.0.0.0'
        df['RECORD_STATUS'] = 'ACTIVE'
        df['RECORD_VERSION'] = 1
        return df

    def insertDFToDB(self, df, table_name, connection):
        cols = [k.replace(' ', '_').strip() for k in df.dtypes.index]
        colnames = ','.join(cols)
        colpos = ', '.join([':' + str(i + 1) for i, f in enumerate(cols)])
        insert_sql = 'INSERT INTO %s (%s) VALUES (%s)' % (table_name, colnames, colpos)
        # logger.info("insert" + str(insert_sql))
        data = [self.convertSequenceToDict(rec) for rec in df.values]
        cur = connection.cursor()
        try:
            # print "data" + str(data)
            # for x in data:
            # print data
            cur.executemany(insert_sql, data)
            # connection.commit()
        except cx_Oracle.DatabaseError as db_exe:
            print db_exe.args
            error, = db_exe.args
            print error
            print "Table name " + table_name
            self.rollBack()
            print insert_sql
            sys.exit(0)
        except Exception as excep:
            print excep
            error, = excep.args
            print error
            print "Table name " + table_name
            print insert_sql
            self.rollBack()
            sys.exit(0)

    def convertSequenceToDict(self, list1):

        dict1 = OrderedDict()
        argList = range(1, len(list1) + 1)
        for k, v in zip(argList, list1):
            dict1[str(k)] = v
        for x in dict1.keys():
            if str(dict1[x]) == 'nan':
                dict1[x] = ''
        return dict1

    def updateReconDynamicSetup(self,import_folder):
        qry = "select count(*) from recon_dynamic_data_model where recon_id ='" + self.reconDef['reconId'] + "'"
        if self.connection.cursor().execute(qry).fetchone()[0] > 0:
            print "**** Recon Dynamic data setup already exists,  Ignoring ****"
        else:
            qry_1 = "select max(RECON_DYNAMIC_DATA_MODEL_OID)+1 from recon_dynamic_data_model"
            qry_2 = "select max(RECON_DYNAMIC_DATA_MODEL_ID)+1 from recon_dynamic_data_model"

            oid = self.connection.cursor().execute(qry_1).fetchone()[0]
            id = self.connection.cursor().execute(qry_2).fetchone()[0]

            df = pd.read_csv(import_folder + os.sep + str(self.reconId) + os.sep + str(
                self.folder_name) + os.sep + 'recon_dynamic_data_model.csv')
            df['RECON_DYNAMIC_DATA_MODEL_OID'] = df.index + oid
            df['RECON_DYNAMIC_DATA_MODEL_ID'] = df.index + id
            df = self.appendAuditRecords(df)

            self.insertDFToDB(df, 'recon_dynamic_data_model', self.connection)
            print "**** Recon Dynamic data setup updated ****"

    def commitToSql(self):
        self.connection.commit()

    def rollBack(self):
        self.connection.rollback()


if __name__ == '__main__':
    reconId, extract_date = sys.argv[1],sys.argv[2]
    reconJob = ReconMigration(reconId,extract_date)
