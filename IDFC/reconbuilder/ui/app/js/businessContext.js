/**
 * Created by hash on 22/8/16.
 */

function businessContextController($scope, $rootScope, $http, $upload, $sce, $timeout, $route, $window, $modal, $filter, ReconProcessService, BusinessContextService, StaticDataMasterService) {

    $scope.businessContextOptions = {};
    $scope.businessContextOptions.init = false;

    $scope.bcData = {}

    $scope.getBusinessContextDetails = function () {
        $scope.businessContextOptions.searchFields = {}
        $scope.businessContextOptions.service = BusinessContextService;
        $scope.businessContextOptions.headers = [
            [
                {
                    name: "Work Pool Name",
                    searchable: true,
                    sortable: true,
                    dataField: "WORKPOOL_NAME",
                    colIndex: 0
                },
                {
                    name: "Business Process",
                    searchable: true,
                    sortable: true,
                    dataField: "BUSINESS_PROCESS_NAME",
                    colIndex: 1
                },
                {
                    name: "Recon Process Code",
                    searchable: true,
                    sortable: true,
                    dataField: "reconProcessCode",
                    colIndex: 2
                },
                {name: "Asset Class", searchable: true, sortable: true, dataField: "assetClass", colIndex: 3},
                {name: "Functional Area", searchable: true, sortable: true, dataField: "functionalArea", colIndex: 4},
                {
                    name: "Actions",
                    colIndex: 5,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addBusinessContext",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg"
                        }]
                }
            ]
        ];
    }
    $scope.businessContextOptions.addBusinessContext = function (data) {
        $modal.open({
            templateUrl: 'addBusinessContext.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, BusinessContextService) {
                $scope.mode = "Add";


                $scope.populateBusinessDetails = function () {
                    if (angular.isDefined(data)) {
                        $scope.bcData = data
                    } else {
                        $scope.bcData = {}
                    }
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.businessContextOptions.reQuery = true;
                };

                $scope.saveBusinessContext = function (val) {
                    if (angular.isDefined(val['_id'])) {
                        BusinessContextService.put(val['_id'], val, function (data) {
                            //$scope.bcData = {}
                            $scope.businessContextOptions.reQuery = true;
                            $scope.cancel()
                            $rootScope.showSuccessMsg("Business Context Details Updated Successfully.")
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    } else {
                        BusinessContextService.post(val, function (data) {
                            //$scope.bcData = {}
                            $scope.businessContextOptions.reQuery = true;
                            $scope.cancel()
                            $rootScope.showSuccessMsg("Business Context Details Saved Successfully.")
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }
                }

                $scope.populateBusinessDetails()
            }
        });
    };

    $scope.confirmDelete = angular.noop();

    $scope.businessContextOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteReconProcess.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    BusinessContextService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.businessContextOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    // constants
    $scope.geography = []
    $scope.assetClass = []
    $scope.businessProcess = []
    $scope.functionalArea = []
    //$scope.businessProcess = ['BSMART', 'CBS Suspense', 'Clearing', 'CMS Bulk Payments', 'CMS Electronic Payments', 'Treasury', 'Retail Assets', 'Two Wheeler', 'Bharat Banking']
    //$scope.functionalArea = ['Wholesale', 'Retail', 'Miscellaneous', 'PDG']

    $scope.initializeConstants = function () {

        searchConstants = ['BUSINESS_PROCESS', 'CURRENCY', 'ASSET_CLASS', 'FUNCTIONAL_AREA', 'GEOGRAPHY']
        BusinessContextService.get(undefined, undefined, function (data) {

            if (data['total'] > 0) {
                data = data['data']
                angular.forEach(data, function (key, value) {
                    console.log(data)
                    console.log(key['functionalArea'])
                    $scope.geography.push(key['GEOGRAPHY_CODE'])
                    $scope.geography=Array.from(new Set($scope.geography))
                    $scope.businessProcess.push(key['WORKPOOL_NAME'])
                    $scope.assetClass.push(key['assetClass'])
                    $scope.assetClass=Array.from(new Set($scope.assetClass))
                    $scope.functionalArea.push(key['functionalArea'])
                    $scope.functionalArea=Array.from(new Set($scope.functionalArea))
                    if (key['staticDataType'] == 'GEOGRAPHY') {
                        $scope.geography.push({'label': key['staticName'], 'value': key['staticCode']})
                    }
                    else if (key['staticDataType'] == 'ASSET_CLASS') {
                        $scope.assetClass.push({'label': key['staticName'], 'value': key['staticCode']})
                    }
                    else if (key['staticDataType'] == 'BUSINESS_PROCESS') {
                        $scope.businessProcess.push({'label': key['staticName'], 'value': key['staticCode']})
                        console.log($scope.businessProce)
                    }
                    else if (key['staticDataType'] == 'FUNCTIONAL_AREA') {
                        $scope.functionalArea.push({'label': key['staticName'], 'value': key['staticCode']})
                    }
                })

            }

        }, function (err) {
            $rootScope.showErrormsg(err.msg)
        })
    }


    $scope.initializeConstants()

    $scope.getBusinessContextDetails();

}
