/**
 * Created by ubuntu on 3/5/17.
 */
function preloadScriptController($scope, $rootScope, $http, $upload, $sce, $timeout, $route, $window, $modal, $filter, FeedDefinationService,UtilitiesService) {
    $scope.preLoadScriptFiles = ["bgl_dump_txn_splitter.sh", "bgl_dump_bal_splitter.sh", "interbranch_splitter.sh", "rtgs_inward.sh", "neft_outward_return.sh",
        "neft_inward_return.sh", "rtgs_outward.sh", "rtgs_outward_return.sh", "rtgs_inward_return.sh", "vsplit.sh",
        "deal_tally_forward_kptp.py", "deal_tally_forward_db.py", "deal_tally_forward_360T.py", "deal_tally_forward_CITI.py",
        "deal_tally_spot_kptp.py", "deal_tally_spot_JP.py", "deal_tally_spot_db.py", "deal_tally_spot_CITI.py", "deal_tally_spot_Barx.py", "deal_tally_spot_360T.py",
        "deal_tally_swap_db.py", "deal_tally_swap_JP.py", "deal_tally_swap_360T.py", "deal_tally_swap_Barx.py", "deal_tally_swap_CCIL.py", "deal_tally_swap_CITI.py", "deal_tally_swap_kptp.py", "" +
        "deal_tally_spot_CCIL.py", "cbs_rsys_bal.py", "cbs_rsys_txn.py", "two_wheeler_bal.py", "neft_outward.sh", "epymts_20201.py", "ra_61204.py", "cbs_inward_return_account_filter.py", "sfms_neft_inward_return.py","pkgb_accquirer.py","pkgb_issuerer.py",
        "cbs_issuer_splitter.py"]

    $scope.getselectval = function () {
        $scope.selectedFile = $scope.feedDefination.preLoadScript.scriptFile
        console.log($scope.selectedFile)
        UtilitiesService.getPreLoadScriptData('dummy',{'fileName':$scope.selectedFile}, function (resp) {
            console.log(resp)
            $scope.scriptCode = resp
        }, function (err) {
            console.log(err)
        })

    }

    $scope.showContent = function ($fileContent) {
        $scope.content = $fileContent;
        console.log($scope.content)
    };
}