/**
 * Created by ubuntu on 15/9/16.
 */

function feedDefinationController($scope, $rootScope, $http, $upload, $sce, $timeout, $route, $window, $modal, $filter, FeedDefinationService, MdlFieldStaticDataService, SwiftTagsLibraryService) {

    $scope.feedDefination = {'filters': []}
    $scope.enableEditing = {}
    $scope.filterCondExpArray = []
    $scope.filterCondArray = []
    $scope.mdlFieldStaticData = []
    $scope.selectedMdlFieldArray = []
    $scope.fieldCheckArray = []
    // TODO need attention
    $scope.preLoadScriptFiles = ["bgl_dump_txn_splitter.sh", "bgl_dump_bal_splitter.sh", "interbranch_splitter.sh", "rtgs_inward.sh", "neft_outward_return.sh",
        "neft_inward_return.sh", "rtgs_outward.sh", "rtgs_outward_return.sh", "rtgs_inward_return.sh", "vsplit.sh",
        "deal_tally_forward_kptp.py", "deal_tally_forward_db.py", "deal_tally_forward_360T.py", "deal_tally_forward_CITI.py",
        "deal_tally_spot_kptp.py", "deal_tally_spot_JP.py", "deal_tally_spot_db.py", "deal_tally_spot_CITI.py", "deal_tally_spot_Barx.py", "deal_tally_spot_360T.py",
        "deal_tally_swap_db.py", "deal_tally_swap_JP.py", "deal_tally_swap_360T.py", "deal_tally_swap_Barx.py", "deal_tally_swap_CCIL.py", "deal_tally_swap_CITI.py", "deal_tally_swap_kptp.py", "" +
        "deal_tally_spot_CCIL.py", "cbs_rsys_bal.py", "cbs_rsys_txn.py", "two_wheeler_bal.py", "neft_outward.sh", "epymts_20201.py", "ra_61204.py", "cbs_inward_return_account_filter.py", "sfms_neft_inward_return.py",
        'deal_tally_spot_gs.py', 'deal_tally_spot_reuters.py', "deal_tally_swap_gs.py", 'deal_tally_swap_reuters.py', 'deal_tally_swap_boa.py', 'deal_tally_spot_boa.py', 'deal_tally_forward_boa.py',
        "deal_tally_spot_bnp.py",'deal_tally_swap_bnp.py','deal_tally_forward_bnp.py',"novopay_c40.py", "rbidad001.sh", "NGRTGS_preload.py","bgl_dump_txn_splitter_rbi.sh","novopay_BGL_report.py"]

    $scope.sortType = 'position'; // set the default sort type
    $scope.sortReverse = false;  // set the default sort order
    //$scope.searchFish = '';     // set the default search/filter term

    //$scope.dataTypes = {'String': 'str', 'Integer': 'np.int64', 'Float': 'np.float64', 'DateTimeStamp': 'np.datetime64'}
    $scope.filterConditionsMasterArray = {
        'str': ['notEqual', 'equal', 'startWith', 'notStartWith', 'endsWith', 'contains', 'notContains', 'notEndsWith', 'isIn', 'notIsIn'],
        'np.float64': ['>', '<', '>=', '<=', '!=', '=='],
        'np.int64': ['>', '<', '>=', '<=', '!=', '==']
    }

    // TODO verify strip function posiblities
    var stringExpressions = {
        'notEqual': "(data['$colName'].str.strip()!='$colValue')",
        'equal': "(data['$colName'].str.strip()=='$colValue')",
        'startWith': "(data['$colName'].str.strip().startswith('$colValue'))",
        'notStartWith': "(~data['$colName'].str.strip().str.startswith('$colValue'))",
        'endsWith': "(data['$colName'].str.strip().endswith('$colValue'))",
        'notEndsWith': "(~data['$colName'].str.strip().endswith('$colValue'))",
        'contains': "(data['$colName'].str.strip().contains('$colValue')",
        'notContains': "(~data['$colName'].strip().str.contains('$colValue'))",
        'isIn': "(data['$colName'].strip().isin('$colValue'))",
        'notIsIn': "(~data['$colName'].strip().isin('$colValue'))"
    }

    var numbericExpression = {
        'condition': "data[data['$colName'] $operator $colValue]"
    }


    // only to display on the ui
    /*$scope.dataTypeMapper = {
     'VARCHAR':'str',
     'np.int64': 'INT',
     'np.float64': 'FLOAT',
     'np.datetime64': 'DateTimeStamp'
     };*/

    $scope.updateSwiftTag = function (swiftType) {
        $scope.swiftTags = []
        perColConditions = {}
        perColConditions["swiftType"] = swiftType
        SwiftTagsLibraryService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            console.log(data)
            if (data['total'] > 0) {
                $scope.swiftTags = data['data'][0]['swiftTags']
            } else {
                $rootScope.showAlertMsg("Please update the swift tag library before proceeding")
            }

        })
    }

    $scope.addMdlFields = function () {
        $modal.open({
            templateUrl: 'addModelFields.html',
            windowClass: 'modal Add',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                $scope.getMdlFields = function () {
                    MdlFieldStaticDataService.get(undefined, undefined, function (data) {
                        if (data.total > 0) {
                            $scope.mdlFieldStaticData = data['data']
                        }
                    })
                    $scope.selectedMdlFieldArray = []
                    $scope.arrayCheck = []
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.selectedMdlField = function (value) {
                    if ($scope.arrayCheck.indexOf(value['_id']) == -1) {
                        $scope.selectedMdlFieldArray.push(value)
                        $scope.arrayCheck.push(value['_id'])
                    } else {
                        angular.forEach($scope.selectedMdlFieldArray, function (val, index) {
                            if (value['_id'] == val['_id']) {
                                $scope.selectedMdlFieldArray.splice(index, 1)
                                $scope.arrayCheck.splice($scope.arrayCheck.indexOf(value['_id']), 1)
                            }
                        });
                    }
                    console.log($scope.selectedMdlFieldArray)
                }

                $scope.addModelFieldList = function (value) {
                    if (!$scope.feedDefination.hasOwnProperty('fieldDetails')) {
                        $scope.feedDefination.fieldDetails = []
                    }
                    angular.forEach(value, function (val, index) {

                        if ($scope.fieldCheckArray.indexOf(val['MDL_FIELD_ID']) == -1) {

                            var tmp_var = {}
                            tmp_var['mdlFieldName'] = val['MDL_FIELD_ID']
                            tmp_var['dataType'] = val['MDL_FIELD_DATATYPE']
                            $scope.feedDefination.fieldDetails.push(tmp_var)

                            $scope.fieldCheckArray.push(val['MDL_FIELD_ID'])

                            $scope.enableEditing[index] = true;
                        }
                    });

                    $scope.cancel()
                }

                $scope.getMdlFields()
            }
        });
    }


    $scope.deleteMdlField = function (index, value) {
        console.log($scope.feedDefination.fieldDetails)
        $scope.feedDefination.fieldDetails.splice(index, 1)

        // update the position of the fields
        angular.forEach($scope.feedDefination.fieldDetails, function (val, index) {
            val['position'] = index + 1
        })
        //$scope.fieldCheckArray.splice($scope.fieldCheckArray.indexOf(value['mdlFieldName']), 1)
    }


    $scope.addFilterCond = function () {
        $scope.filterCondExpArray.push({condValue: ''})
    }

    $scope.onChangeOfMdlField = function (val, index) {
        $scope.filterCondExpArray[index]['filterCondArray'] = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
        //$scope.filterCondArray = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
    }

    $scope.onChangeOfFilterCond = function (val) {

    }

    $scope.buildExpression = function () {
        var str = "data = data[";

        for (i = 0; i < $scope.filterCondExpArray.length; i++) {

            if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'str') {
                str += buildStringCondition($scope.filterCondExpArray[i])
            }
            else if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.int64' || $scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.float64') {
                str += buildNumericCondition($scope.filterCondExpArray[i])
            }

            // append logical operator if any
            if (angular.isDefined($scope.filterCondExpArray[i]['logicalOperator'])) {

                if ($scope.filterCondExpArray[i]['logicalOperator'] != ''
                    && $scope.filterCondExpArray.length - 1 > i) {
                    str += ' ' + $scope.filterCondExpArray[i]['logicalOperator'] + ' '
                }
            }
        }

        str += "]"
        return str;
    }


    function buildStringCondition(value) {

        if (value['selectedCond'] == 'isIn' || value['selectedCond'] == 'notIsIn') {
            var condValue = ''
            var condValue = '[' + value['condValue'].replace("^", "'").replace('$', "'").replace(/,/g, "','") + ']'
            return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['mdlFieldName']).replace('$colValue', condValue)
        } else {
            return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['mdlFieldName']).replace('$colValue', value['condValue'] == undefined ? '' : value['condValue'])
        }
    }

    function buildNumericCondition(value) {
        return numbericExpression['condition'].replace('$colName', value['selectedField']['mdlFieldName']).replace('$operator', value['selectedCond']).replace('$colValue', value['condValue'])
    }

    $scope.saveFeedStructure = function (feedStructure) {
        //feedStructure.feedStructureID = generateRandomNumber()

        //feedStructure.filters = []
        // remove un-wanted elements from the json
        feedStructure = formatFeedStructure(feedStructure)

        if (Object.keys(feedStructure).indexOf('filters') == -1) {
            feedStructure['filters'] = []
        }
        // expression being constructed on click of add button
        /*if (angular.isDefined(feedStructure['filterCondExpArray']) && $scope.filterCondExpArray.length > 0) {
         feedStructure.filters.push($scope.buildExpression())
         delete feedStructure['filterCondExpArray']
         }*/


        // update if id is already defined
        if (angular.isDefined(feedStructure['_id'])) {
            FeedDefinationService.put(feedStructure['_id'], feedStructure, function (data) {
                console.log(data)
                resetFeedDefination()
                $scope.fieldCheckArray = []
                $scope.filterCondExpArray = []
                $rootScope.showSuccessMsg("Feed Details Updated Successfully")
                $scope.getFeedStructureDetails()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })
        } else {
            FeedDefinationService.post(feedStructure, function (data) {
                console.log(data)

                resetFeedDefination()
                $scope.fieldCheckArray = []
                $scope.filterCondExpArray = []
                $rootScope.showSuccessMsg("Feed Details Saved Successfully.")
                $scope.getFeedStructureDetails()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })
        }
        console.log(feedStructure)
    }

    function formatFeedStructure(feedStructure) {
        angular.forEach(feedStructure['fieldDetails'], function (value, index) {
            delete value['mdlFieldStaticData']
            delete value['mdlFieldDropDownVal']
            delete value['enableEdit']
        })
        return feedStructure
    }

    function resetFeedDefination() {
        $scope.feedDefination = {
            'skipTopRows': 0,
            'skipBottomRows': 0,
            'limitFileCount': 0,
            'positionsFeed': false,
            'headerExists': false,
            'preLoadScriptIndicator': false
        }
    }

    resetFeedDefination()

    $scope.clearFields = function () {
        $scope.feedDefination = {}
        $scope.selectedFeed = {}
        resetFeedDefination()
        $scope.getMdlFields()
    }

    $scope.removeFilterCond = function (index) {
        $scope.filterCondExpArray.splice(index, 1)
    }

    $scope.removeFilterExp = function (index) {
        $scope.feedDefination['filters'].splice(index, 1)
    }


    function generateRandomNumber() {
        return Date.now() + Math.floor((Math.random() * 100) + 1);
    }


    $scope.uploadFile = function () {
        $modal.open({
            templateUrl: 'uploadfile.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance) {
                $scope.upload = function (files) {
                    console.log(files[0]);
                    $scope.fileName = files[0].name
                    if (files[0].size > 20971520) {
                        $scope.msgNotify('warning', 'Uploaded File was more than 20 MB.Please choose less than 20MB file');
                        files = [];
                    }
                    else {
                        $scope.saveFile = function () {

                            $scope.name = files[0].name.split('.')
                            $scope.array = [$scope.name[0], $scope.name[1].toLowerCase()]
                            $scope.id = $scope.array.join('.')

                            $upload.upload({
                                url: '/api/utilities/' + $scope.id + '/upload',
                                method: 'POST',
                                file: files[0],
                                data: {'fileName': "test", 'description': "TESTING"}
                            }).progress(function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

                            }).success(function (data, status, headers, config) {
                                if (data.status == "error") {
                                    $rootScope.showAlertMsg(data.msg)
                                }
                                else {
                                    // update field details
                                    // check if undefined or defined
                                    if (angular.isDefined($scope.feedDefination.fieldDetails)) {
                                        angular.forEach(JSON.parse(data['msg']), function (value, index) {
                                            var len = $scope.feedDefination.fieldDetails.length
                                            value['enableEdit'] = true
                                            value['position'] = len + 1
                                            value['mdlFieldStaticData'] = $scope.mdlFieldStaticData
                                            $scope.feedDefination.fieldDetails.push(value)
                                        })

                                    } else {
                                        $scope.feedDefination.fieldDetails = []
                                        $scope.feedDefination.fieldDetails = JSON.parse(data['msg'])

                                        // enable editing on all the rows
                                        angular.forEach($scope.feedDefination.fieldDetails, function (value, index) {
                                            value['enableEdit'] = true
                                            value['position'] = index + 1
                                            value['mdlFieldStaticData'] = $scope.mdlFieldStaticData
                                        })
                                    }
                                    $modalInstance.dismiss('cancel');
                                }

                            })
                        };

                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    $scope.getFeedStructureDetails = function () {

        FeedDefinationService.get(undefined, undefined, function (data) {
            console.log(data)
            $scope.feedDetails = {}
            if (data.total > 0) {
                $scope.feedDetails = data['data']
            }
        })
    }


    $scope.onChangeFeedDetails = function (val) {
        //$rootScope.openModalPopupOpen()
        angular.forEach(val['fieldDetails'], function (value, index) {
            value['mdlFieldStaticData'] = $scope.mdlFieldStaticData
        })
        $scope.feedDefination = val
        /*if (angular.isDefined(val['filterCondExpArray'])) {
         $scope.filterCondExpArray = val['filterCondExpArray']
         }*/
        if (val['format'] == 'SWIFT') {
            $scope.updateSwiftTag(val['swiftType'])
        }
        //$rootScope.openModalPopupClose()
    }


    $scope.getMdlFields = function () {
        $scope.mdlFieldStaticData = []
        MdlFieldStaticDataService.get(undefined, undefined, function (data) {
            if (data.total > 0) {
                angular.forEach(data['data'], function (value, index) {
                    $scope.mdlFieldStaticData.push({
                        'mdlFieldID': value['MDL_FIELD_ID'],
                        'dataType': value['MDL_FIELD_DATATYPE']
                    })
                })
            }
        })
    }

    $scope.getFeedStructureDetails()
    $scope.getMdlFields()

    $scope.onMdlFieldChange = function (value, index) {
        if (angular.isDefined(value)) {
            $scope.feedDefination.fieldDetails[index]['dataType'] = value['dataType'] == 'Timestamp' ? 'np.datetime64' : value['dataType']
            $scope.feedDefination.fieldDetails[index]['mdlFieldName'] = value['mdlFieldID']
        }
    }

    $scope.addModelFields = function () {
        if (!angular.isDefined($scope.feedDefination.fieldDetails)) {
            $scope.feedDefination.fieldDetails = []
            $scope.feedDefination.fieldDetails.push({
                'enableEdit': true,
                'displayType': 'M',
                'position': 1,
                'mdlFieldStaticData': $scope.mdlFieldStaticData
            })
        } else {
            var position = $scope.feedDefination.fieldDetails.length + 1
            $scope.feedDefination.fieldDetails.push({
                'enableEdit': true,
                'displayType': 'M',
                'position': position,
                'mdlFieldStaticData': $scope.mdlFieldStaticData
            })
        }
    }

    $scope.delteFeedDefination = function (data) {
        $modal.open({
            templateUrl: 'deleteFeedAccMap.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, FeedDefinationService) {
                $scope.confirmDelete = function (data) {
                    $rootScope.openModalPopupOpen();
                    FeedDefinationService.deleteData(data["_id"], function (data) {
                        $scope.clear()
                        $rootScope.openModalPopupClose();
                        angular.element('#modal').modal('hide');
                        //$modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

    }


    $scope.createFeedDefination = function () {
        $scope.clear()
    }

    $scope.clear = function () {
        $scope.feedDefination = {}
        $scope.selectedFeed = {}
        $scope.getMdlFields()
        resetFeedDefination()

    }

    $scope.editMdlFieldDetails = function (index) {
        $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData'] = []
        $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData'] = $scope.mdlFieldStaticData
    }

    $scope.removeMdlFields = function (index) {
        delete $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData']
    }

    $scope.saveFilterCond = function () {
        if (!angular.isDefined($scope.feedDefination.filters)) {
            $scope.feedDefination.filters = []
        }

        if ($scope.filterCondExpArray.length > 0) {
            $scope.feedDefination.filters.push($scope.buildExpression())
            $scope.filterCondExpArray = [{}]
        }
    }

    $scope.sortChange = function (sortType) {
        $scope.sortType = sortType;
        $scope.sortReverse = !$scope.sortReverse
    }


    //TODO add additional button to add model fiedls
    // TODO field name cannot be duplicated eunsure

    /*TODO add additional params to data type [date, timestamp, datetimestamp] to format accordingly on the ui
     make data type as drop down instead of a static column */
    //Move pre-load script to mongo db

    /*$scope.onMdlFieldDropDwnShow = function (index) {

     if (!angular.isDefined($scope.feedDefination.fieldDetails[index]['mdlFieldStaticData'])) {
     $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData'] = []
     $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData'] = $scope.mdlFieldStaticData

     }
     }*/

    /*$scope.onMdlFieldDropDwnHide = function (index) {
     delete $scope.feedDefination.fieldDetails[index]['mdlFieldStaticData']
     }*/

    /*$scope.cloneFeedDefination = function () {
     if (!angular.isDefined($scope.selectedFeed)) {

     if (angular.isDefined($scope.selectedFeed['_id'])) {
     delete $scope.selectedFeed['_id']
     }

     seletedFeed['feedName'] = $scope.selectedFeed['feedName'] + '_cloned'
     FeedDefinationService.post($scope.selectedFeed, function (data) {
     console.log(data)
     resetFeedDefination()
     $scope.fieldCheckArray = []
     $scope.filterCondExpArray = []
     $rootScope.showSuccessMsg("Feed Details Cloned Successfully.")
     $scope.getFeedStructureDetails()
     }, function (err) {
     $rootScope.showAlertMsg(err.msg)
     })
     }
     }*/

    $scope.cloneFeedDefination = function (selectedFeed) {
        if (angular.isDefined(selectedFeed)) {

            if (angular.isDefined(selectedFeed['_id'])) {
                delete selectedFeed['_id']
            }

            selectedFeed['feedName'] = selectedFeed['feedName'] + '_cloned'
            FeedDefinationService.post(selectedFeed, function (data) {
                console.log(data)
                resetFeedDefination()
                $scope.fieldCheckArray = []
                $scope.filterCondExpArray = []
                $rootScope.showSuccessMsg("Feed Details Cloned Successfully.")
                $scope.getFeedStructureDetails()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })
        }
    }


}
