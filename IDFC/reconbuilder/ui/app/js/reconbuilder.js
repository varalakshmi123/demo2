/**
 * Created by swami on 13/4/15.
 */
function reconbuilderController($scope, $rootScope, $http, $upload, $sce, $timeout, $route, $window, $modal, $filter, ReconProcessService, DataSourceService, MatchingRulesService, UtilitiesService, AccountsService, ReconContextService, BusinessContextService, FeedDefinationService, SeqGeneratorService) {

    $scope.matchingProcessingLevel = ['Both', 'Position Level', 'Single Account', 'Transaction Level']
    $scope.reconciliationType = ['Auto', 'Manual']
    $scope.currency = ['AUD', 'CAD', 'CHF', 'CNY', 'EUR', 'INR', 'KES', 'KWD', 'LIPA', 'POUND', 'SAD', 'USD', 'WSD']


    $scope.srcDerivativeConditionsArray = {
        'str': ['equal', 'not equal', 'start with', 'not starts with', 'ends with', 'not ends with', 'contains', 'not contains', 'isin', 'not isin', 'not null', 'is null'],
        'np.int64': ['>', '<', '<=', '>=', '!=', '=='],
        'np.float64': ['>', '<', '<=', '>=', '!=', '=='],
        'np.datetime64': ['add days']
    }

    $scope.counterCondLookUP = {
        'equal': 'not equal',
        'not equal': 'equal',
        'start with': 'not starts with',
        'not starts with': 'start with',
        'ends with': 'not ends with',
        'not ends with': 'ends with',
        'contains': 'not contains',
        'not contains': 'contains',
        'isin': 'not isin',
        'not isin': 'isin',
        'not null': 'is null',
        'is null': 'not null',
        '>': '<',
        '<': '>',
        '>=': '<=',
        '<=': '>=',
        '!=': '==',
        '==': '!='
    }

    $scope.commonDerivedColFunc = ['assign']

    $scope.colDerivateCondArray = {
        'str': ['sub string', 'strip', 'lstrip', 'rstrip', 'replace', 'split'],
        'np.int64': ['add', 'sub', 'multiple', 'div', 'abs'],
        'np.float64': ['add', 'sub', 'multiple', 'div', 'abs'],
        'np.datetime64': ['add days', 'format']
    }

    numericOprMapper = {
        'add': 'x + $value',
        'sub': 'x - $value',
        'multiple': 'x * $value',
        'div': 'x / $value',
        'abs': 'abs($value)'
    }


    // String expressions for getting derived sources
    var derivedSrcConditions = {
        'not equal': "(df['$colName']!='$colValue')",
        'equal': "(df['$colName']=='$colValue')",
        'start with': "(df['$colName'].fillna('').str.startswith('$colValue'))",
        'not starts with': "(~df['$colName'].fillna('').str.startswith('$colValue'))",
        'ends with': "(df['$colName'].fillna('').str.endswith('$colValue'))",
        'not ends with': "(~df['$colName'].fillna('').str.endswith('$colValue'))",
        'contains': "(df['$colName'].fillna('').str.contains('$colValue', case = $status))",
        'not contains': "(~df['$colName'].fillna('').str.contains('$colValue', case = $status))",
        'isin': "(df['$colName'].isin(['$colValue']))",
        'not isin': "(~df['$colName'].isin(['$colValue']))",
        'not null': "(df['$colName'].notnull())",
        'is null': "(~df['$colName'].notnull())",
        'numericConditon': "df['$colName'].apply(lambda x : $condValue)" // numeric condition
    }

    /*
     Derived Column expression starts here
     */
    // string expression for derived columns
    var derivedColStrExp = {
        'sub string': "df['$colName'].fillna('').apply(lambda x : x[$colValue] if x and x!='' else '')",
        'strip': "df['$colName'].fillna('').str.strip()",
        "lstrip": "df['$colName'].fillna('').str.lstrip('$colValue')",
        "rstrip": "df['$colName'].fillna('').str.rstrip('$colValue')",
        "replace": "df['$colName'].apply(lambda x : x.replace('$colValue','$colValue1') if x and x != None else x)",
        "split": "df['$colName'].fillna('').str.split('$colValue')"
    }
    // date expressions
    var derivedColDateExp = {
        'add days': "pd.to_datetime(df['$colName'], format='%Y-%m-%d %H:%M:%S', coerce=True) - timedelta(days=$colValue)",
        "format": "pd.to_datetime(df['$colName'], format='%Y-%m-%d %H:%M:%S', coerce=True).apply(lambda x: x.strftime('$colValue'))"
    }
    // integer expressions
    var derivedColNumericExp = {
        'numericCondition': "df['$colName'] $condValue"
    }

    $scope.cols = []
    $scope.fuzzycolumns = []
    $scope.splitCond = ""
    $scope.dataCondExpression = ""

    //$scope.derivedColumnArray = []
    $scope.dataConditionArray = []
    $scope.displayCond = []

    $scope.disableconditionalVal1 = false
    $scope.disableconditionalVal2 = false

    $scope.condVal1PlaceHdrVal = ''
    $scope.condVal2PlaceHdrVal = ''
    $scope.renderBooleanCond = false

    $scope.derivedSrc = []
    $scope.enableEditing = {}

    $scope.derivedSrcCondArray = []

    $scope.groupCounter = 0
    $scope.derivedSrcCounter = 0

    $scope.virtualSourceArray = []

    $scope.selectedBusinessContext = {}

    $scope.customProcess = [{"processName": "CustomProcess1"}]

    $scope.getBusinessContext = function () {
        BusinessContextService.get(undefined, undefined, function (data) {
            if (data.total > 0) {
                $scope.businessContext = data['data']
                console.log($scope.businessContext)
            }
            else {
                $rootScope.showAlertMsg("Please Create business context to proceed")
            }
        })
    }

    /*$scope.getReconProcess = function () {
     ReconProcessService.get(undefined, undefined, function (data) {
     console.log(data)
     if (data.total > 0) {
     $scope.reconProcess = data['data']
     }
     else {
     $rootScope.showAlertMsg("Please Create recon process to proceed")
     }
     })
     };*/
    $scope.getBusinessContext();
    $scope.label = ''
    $scope.HTML = ' <label for="urole" class="col-sm-1 col-lg-1 control-label" >Data Source :</label><div class="col-sm-2 col-lg-2"><input type="file" name="file" data-ng-model="files" accept=".csv" onchange="angular.element(this).scope().upload(this.files)"/></div>'
    //$scope.files = [{'filed': $sce.trustAsHtml($scope.HTML)}];
    $scope.showCR1 = false
    $scope.cond = {}
    $scope.removeFile = function (index) {
        $scope.files.splice(index, 1);
    };
    $scope.attach = [];

    $scope.getDataCount = function (count) {
        $scope.sourceTypes = [];

        for (i = 1; i <= parseInt(count); i++) {
            $scope.recon['S' + i] = {}
            $scope.sourceTypes.push("S" + i)
        }
    }

    $scope.recon = {}
    $scope.recon['sources'] = []
    $scope.recon['matchingRules'] = []

    $scope.uploadRecon = function (recon) {
        console.log(recon)
        console.log($scope.recon)
        if (Object.keys($scope.recon['sources']).length > 0) {
            DataSourceService.post($scope.recon, function (data) {
                console.log(data)
                $scope.recon = {}
                $scope.files = []
                $rootScope.showSuccessMsg('File Uploaded Succesfully')
                $scope.getReconDetails()
            }, function (err) {
                $rootScope.showAlertMsg(err.msg)
            })
        }
        else {
            $rootScope.showAlertMsg('Please load source files with labels')
        }
    }
    $scope.upload = function (files) {
        $scope.fileName = files[0].name
        if (files[0].size > 20971520) {
            $scope.showAlertMsg('Uploaded File was more than 20 MB.Please choose less than 20MB file');
            files = [];
        }
        else {
            $scope.addFile = function (file) {
                $scope.name = files[0].name.split('.')
                $scope.array = [file['label'], $scope.name[1].toLowerCase()]
                $scope.id = $scope.array.join('.')
                $upload.upload({
                    url: '/api/utilities/' + $scope.id + '/upload?data=' + file.label + '&type=' + $scope.name[1] + '&formatType=' + file['formatType'],
                    file: files[0],
                    data: $scope.data
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function (data, status, headers, config) {
                    if (data.status == "error") {
                        $rootScope.showAlertMsg(data.msg)
                    }
                    else {
                        $rootScope.showSuccessMsg('File Uploaded Succesfully')
                        console.log(file)
                        $scope.recon['sources'][file['sourceType']] = {}
                        $scope.recon['sources'][file['sourceType']]['mdl_fields'] = JSON.parse(data.msg)
                        $scope.recon['sources'][file['sourceType']]['sourceName'] = file['label']
                        $scope.recon['sources'][file['sourceType']]['formatType'] = file['formatType']
                        $scope.recon['sources'][file['sourceType']]['delimiter'] = file['delimiter']
                        $scope.recon['sources'][file['sourceType']]['swift'] = file['swift']
                        $scope.recon['sources'][file['sourceType']]['sourceType'] = file['sourceType']
                        console.log($scope.recon)
                    }
                })
                console.log($scope.recon)
            };
        }
    };

    $scope.getSources = function (recon) {
        $scope.recon = recon

        angular.forEach($scope.businessContext, function (value, key) {
            if (value['BUSINESS_CONTEXT_ID'] === recon['businessContextId']) {
                $scope.recon['businessContext'] = value
            }
        })
    }


    $scope.deleteRecon = function () {
        if (angular.isDefined($scope.recon['_id'])) {
            $modal.open({
                templateUrl: 'deleteRecon.html',
                windowClass: 'modal alert alert-error popupLarge',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance) {
                    $scope.confirmDelete = function () {
                        $rootScope.openModalPopupOpen();
                        ReconContextService.deleteData($scope.recon['_id'], function (data) {
                            $rootScope.openModalPopupClose();
                            $scope.getReconDetails()
                            //$scope.getSources()
                            $modalInstance.dismiss('cancel');
                        }, function (errorData) {
                            $scope.showErrormsg(errorData.msg);
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }
    }
    $scope.r = {}
    $scope.r['columns'] = []
    $scope.r['fuzzyColumns'] = []
    $scope.listOfConditions = [{'name': "isin", 'value': "isin($)"}, {
        'name': "contains",
        'value': "contains($)"
    }, {'name': "equals", 'value': '=$'}]

    $scope.conds = [{'count': '', 'cols1': [], 'cols2': []}]
    $scope.splitconds = [{'count': '', 'cols1': [], 'cols2': []}]
    $scope.addConditionSplit = function () {
        $scope.splitconds.push({'count': '', 'cols1': [], 'cols2': []})
    }
    $scope.removeConditionSplit = function (index) {
        $scope.splitconds.splice(index, 1)
    }
    $scope.selectedSrc = function (data, index) {
        $scope.cols = []
        $scope.arraycheck = []
        $scope.fuzzyColumns = []

        /*angular.forEach($scope.reconData['sources'][data]['mdl_fields'], function (val, key) {
         if ($scope.arraycheck.indexOf(val['MDL_FIELD_NAME']) == -1){
         $scope.arraycheck.push(val['MDL_FIELD_NAME'])
         $scope.cols1.push({'id': key, 'name': val['MDL_FIELD_NAME'], 'datatype': val['MDL_FIELD_DATA_TYPE']})
         }
         })*/
        console.log(data, index)
        angular.forEach($scope.recon['sources'][data]['mdl_fields'], function (val, key) {
            if ($scope.arraycheck.indexOf(val['MDL_FIELD_NAME']) == -1) {
                $scope.arraycheck.push(val['MDL_FIELD_NAME'])

                $scope.cols.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})
                $scope.fuzzyColumns.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})

                //$scope.cols1.push({'id': key, 'name': val['MDL_FIELD_NAME'], 'datatype': val['MDL_FIELD_DATA_TYPE']})
                //$scope.fuzzycolumns.push({'id': key, 'name': val['MDL_FIELD_NAME'], 'datatype': val['MDL_FIELD_DATA_TYPE']})
            }
        })

        // add derived columns if any
        /*$scope.arraycheck = []
         angular.forEach($scope.derivedColumnArray, function (val, key) {
         if ($scope.arraycheck.indexOf(val) == -1) {
         $scope.arraycheck.push(val)

         $scope.cols.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})
         $scope.fuzzyColumns.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})
         //$scope.cols1.push({'id': $scope.cols1.length, 'name': val, 'datatype': 'DERIVED_TYPE'})
         //$scope.fuzzycolumns.push({'id': $scope.fuzzycolumns.length, 'name': val, 'datatype': 'DERIVED_TYPE'})

         }
         })*/
        //$scope.selectFirstSrc = data
    }

    $scope.maincond = []
    $scope.maincondSplit = []
    $scope.conditions = []
    $scope.columnsData = {}
    $scope.ruleCounter = 0
    $scope.ruleNameArray = []
    $scope.ruleNameRuleIDMap = {}

    $scope.addMatchingCondition = function (cond) {
        console.log(cond)
        $scope.fuzz = []
        $scope.MatchingColumns = []
        matchRuleId = ''

        if ($scope.ruleNameArray.indexOf(cond['ruleName']) == -1) {
            $scope.ruleNameArray.push(cond['ruleName'])
            matchRuleId = 'rule_' + $scope.ruleNameArray.length
            $scope.ruleNameRuleIDMap[cond['ruleName']] = matchRuleId
        } else {
            // if rule already exists in the scope
            matchRuleId = $scope.ruleNameRuleIDMap[cond['ruleName']]

        }

        angular.forEach(cond.fuzzyColumns, function (val, key) {
            console.log(val)
            $scope.fuzz.push(val['label'])

            // check for last iteration
            if (key == cond.fuzzyColumns.length - 1) {
                // to display on the data table
                $scope.maincond.push({
                    'columnsNames': $scope.fuzz,
                    'columnType': 'Fuzzy',
                    'ruleName': cond['ruleName'],
                    'sourceName': cond['selectedSource']['sourceName'],
                    'matchRuleID': matchRuleId,
                    'sourceType': cond['selectedSource']['sourceType']
                })
            }
        })
        angular.forEach(cond.columns, function (v, k) {
            $scope.MatchingColumns.push(v['label'])
            // check for last iteration
            if (k == cond.columns.length - 1) {
                // to display on the data table
                $scope.maincond.push({
                    'columnsNames': $scope.MatchingColumns,
                    'columnType': 'Normal',
                    'ruleName': cond['ruleName'],
                    'sourceName': cond['selectedSource']['sourceName'],
                    'matchRuleID': matchRuleId,
                    'sourceType': cond['selectedSource']['sourceType']
                })
            }
        })

        // check if main json has matchingConditions property
        if (!$scope.recon.hasOwnProperty('matchingConditions')) {
            $scope.recon['matchingConditions'] = {}
        }

        // check for rules names, should be unique, if not there declare
        if (matchRuleId != '' && !$scope.recon['matchingConditions'].hasOwnProperty(matchRuleId)) {
            $scope.recon['matchingConditions'][matchRuleId] = {}
        }

        if (!$scope.recon['matchingConditions'][matchRuleId].hasOwnProperty(cond['selectedSource']['sourceType'])) {
            $scope.recon['matchingConditions'][matchRuleId][cond['selectedSource'].sourceType] = {}
        }

        $scope.recon['matchingConditions'][matchRuleId][cond['selectedSource'].sourceType]['fuzzyColumns'] = $scope.fuzz
        $scope.recon['matchingConditions'][matchRuleId][cond['selectedSource'].sourceType]['mathingColumns'] = $scope.MatchingColumns
        $scope.recon['matchingConditions'][matchRuleId]['ruleName'] = cond['ruleName']
        $scope.recon['matchingConditions']['matchRuleCount'] = $scope.ruleNameArray.length

        console.log($scope.recon)
        console.log($scope.maincond)

    }
    $scope.getFullConditionSplit = function (cond) {
        console.log(cond)
        /*angular.forEach($scope.conds, function (val, key) {
         console.log(val)
         if (angular.isDefined(val['selectedDf2'])) {
         if (angular.isDefined(val.parentCondition)) {
         $scope.conditions.push(val['selectedDf1'] + '["' + val.selectedCol1 + '"].' + val.childCondition + '(' + val['selectedDf2'] + '["' + val.selectedCol1 + '"])' + ' ' + val.parentCondition + ' ')
         }
         else {
         $scope.conditions.push(val['selectedDf1'] + '["' + val.selectedCol1 + '"].' + val.childCondition + '(' + val['selectedDf2'] + '["' + val.selectedCol1 + '"])' + ' ')
         }
         }
         else {
         $scope.conditions.push(val['selectedDf1'] + '["' + val.selectedCol1 + '"].' + val.childCondition)
         }

         })*/
        $scope.conds = [{'count': '', 'cols1': [], 'cols2': []}]
        $scope.maincondSplit.push(cond)
        $scope.recon['sources'][$scope.selectedSource]['split_condition'] = cond
        $scope.splitCond = ""
        console.log($scope.recon)
    }
    $scope.selectedFirstDsSplit = function (data, index) {
        $scope.colsSplit1 = []
        $scope.arraycheck = []
        $scope.selectedSource = data
        $scope.selectedFirstDsSplit = ''

        angular.forEach($scope.recon['sources'][data]['mdl_fields'], function (val, key) {
            if ($scope.arraycheck.indexOf(val['MDL_FIELD_NAME']) == -1) {
                $scope.arraycheck.push(val['MDL_FIELD_NAME'])
                $scope.colsSplit1.push(val['MDL_FIELD_NAME'])
            }
        })
        console.log($scope.recon)
        /*angular.forEach($scope.reconData['sources'][data]['mdl_fields'], function (val, key) {
         $scope.colsSplit1.push(val)
         })*/
        $scope.selectFirstSrc = data
    }
    $scope.selectedSecondDsSplit = function (data, index) {
        $scope.colsSplit2 = []
        angular.forEach($scope.reconData['sources'][data]['mdl_fields'], function (val, key) {
            $scope.colsSplit2.push(val)
        })
        $scope.selectSecSrc = data
    }

    $scope.selectedFirstSrcMdlFiedlSplit = function (data, index) {
        $scope.splitCond += $scope.selectedSource + '["' + data + '"]';
    }

    $scope.selectedSecSrcMdlFiedlSplit = function (data, index) {
        $scope.splitCond += $scope.selectSecSrc + '["' + data + '"])';
    }

    $scope.selectedConditionSplit = function (condition, $index) {
        if (!angular.isDefined($scope.splitCond) || $scope.splitCond === '') {
            $rootScope.showAlertMsg("Please select fields before adding conditions")
        }
        else {
            $scope.splitCond += "." + condition + '('
        }
    }

    $scope.clearConditionSplit = function () {
        $scope.splitCond = ""
    }
    $scope.changeLogicConditionSplit = function (data, index) {
        $scope.splitCond += data + '('
    }
    $scope.createMatchingRules = function () {
        /*$scope.columnsData['fuzzy'] = $scope.fuzz
         var dataR = {}
         dataR['reconId'] = $scope.reconData['_id']
         dataR['matchingConditions'] = $scope.columnsData
         dataR['splittingConditions'] = $scope.maincondSplit
         console.log(dataR)
         MatchingRulesService.post(dataR, function (data) {
         $rootScope.showSuccessMsg('Matching Rules Created Succesfully')
         $scope.selectedRecon = ''
         $scope.maincond = ''
         $scope.maincondSplit = ''
         })*/
        ReconContextService.post($scope.recon, function (data) {
            $rootScope.showSuccessMsg('Recon context created successfully')
        })
    }

    $scope.deleteMatchingCondition = function (index, value) {
        delete $scope.maincond.splice(index, 1)
        //remove the matchingCondition from master json
        delete $scope.recon['matchingConditions'][value['matchRuleID']][value['sourceType']][value['columnType'] == 'Normal' ? 'mathingColumns' : 'fuzzyColumns']

        //check if source has any conditions or not
        if (!$scope.recon['matchingConditions'][value['matchRuleID']][value['sourceType']].hasOwnProperty('mathingColumns') && !$scope.recon['matchingConditions'][value['matchRuleID']][value['sourceType']].hasOwnProperty('fuzzyColumns')) {
            delete $scope.recon['matchingConditions'][value['matchRuleID']][value['sourceType']]
        }

        // check if rule has any source, if not delete
        //
        /* if(!$scope.recon['matchingConditions'][value['matchRuleID']].hasOwnProperty(value['sourceType'])) {
         delete $scope.recon['matchingConditions'][value['matchRuleID']]
         $scope.recon['matchingConditions']['matchRuleCount'] -= 1
         $scope.ruleNameArray.splice($scope.ruleNameArray.indexOf(value['ruleName']),1)
         }*/

        console.log($scope.recon)
    }


    $scope.getReconContextDetails = function () {
        ReconContextService.get(undefined, undefined, function (data) {
            console.log("recon context")
            console.log(data)
            $scope.disableReconContext = true;
            if (data.total > 0) {
                $scope.reconContextDetails = data['data']
                $scope.disableReconContext = false;
            }
        })
    }


    $scope.selected = function () {
        var i, selectedCount = 0;
        for (i = 0; i < $scope.categoryList.length; i++) {
            if ($scope.categoryList[i].Default) {
                selectedCount += 1;
            }
        }
        return selectedCount;
    };


    // Data Condition functions
    $scope.selectedDataCondition = function (condition, index) {
        // check if mdl_fields are selected or not
        if (!angular.isDefined($scope.dataCondExpression) || $scope.dataCondExpression == '') {
            $rootScope.showAlertMsg("Please select fields before adding conditions")
        } else {
            $scope.dataCondExpression += condition['value']
        }
    }


    $scope.updateExpression = function () {
        console.log($scope.dataCondConstant)
        $scope.dataCondExpression = $scope.dataCondExpression.replace('$', $scope.dataCondConstant)
        console.log($scope.dataCondExpression)
    }

    $scope.changeLogicConditionData = function (logicalCond, index) {
        $scope.dataCondExpression += " " + logicalCond + " "
    }


    $scope.selectedConditionSplit = function (condition, $index) {
        if (!angular.isDefined($scope.splitCond) || $scope.splitCond === '') {
            $rootScope.showAlertMsg("Please select fields before adding conditions")
        }
        else {
            $scope.splitCond += "." + condition + '('
        }
    }

    $scope.addDataCondition = function () {
        console.log($scope.dataCondExpression)
        $scope.dataConditionExpArray.push($scope.dataCondExpression)

        $scope.dataCondExpression = ''
    }


    $scope.getReconContextDetails()


    //Query Builder initializations
    var data = '';

    /*$scope.addDerSrcDetails = function(){
     $scope.derivedSrc = {}
     }*/

    /* function htmlEntities(str) {
     return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
     }

     function computed(group) {
     for (var str = "", i = 0; i < group.rules.length; i++) {
     i > 0 && (str += " <strong>" + group.operator + "</strong> ");
     str += group.rules[i].sourceType + "['" + group.rules[i].aliasName + "'] = " + group.rules[i].sourceType + "['" + group.rules[i].field.MDL_FIELD_NAME + "'] " + evaluateCondition(group.rules[i].condition, group.rules[i].data);
     }

     return str;
     }*/

    /*function evaluateCondition(conditionType, value) {
     evaluatedExp = ''
     if (conditionType == 'index_substr') {
     value = value.split(',')
     evaluatedExp = ".apply(lambda x: x[" + value[0] + ":" + value[1] + "])"
     }
     else if (conditionType == 'char_substr') {
     evaluatedExp = ".apply(lambda x : '' if '" + value + "' not in x else x[x.index('" + value + "'):])"
     }

     //NORMAL1_3440_CR['VALUE_DATE_3']=pd.to_datetime(NORMAL1_3440_CR['VALUE_DATE'], format='%Y-%m-%d %H:%M:%S', coerce=True) - timedelta(days=3)
     return evaluatedExp
     }*/

    /*$scope.json = null;


     $scope.$watch('filter', function (newValue) {
     console.log(newValue)
     $scope.json = JSON.stringify(newValue, null, 2);
     $scope.output = computed(newValue.group);
     }, true);*/

    $scope.addDerivedColumn = function () {
        if (!$scope.recon.hasOwnProperty('derivedColumns')) {
            $scope.recon['derivedColumns'] = []
        }

        if (!$scope.recon.hasOwnProperty('derivedExpression')) {
            $scope.recon['derivedExpression'] = []
        }

        //$scope.derivedColumnArray.push($scope.output)
        $scope.recon['derivedColumns'].push($scope.filter.group.rules[0].aliasName)
        $scope.recon['derivedExpression'].push({
            'derivedColName': $scope.filter.group.rules[0].aliasName,
            'expression': $scope.output
        })
        console.log($scope.recon['derivedColumns'])

        $scope.output = ''
    }

    $scope.removedDerivedValue = function (parentIdx, idx, value) {

        angular.forEach($scope.recon.matchingRules[parentIdx]['derivedColumnsList'][value['sourceType']], function (val, index) {
            if (value['columnLabel'] == val['mdlFieldName']) {
                $scope.recon.matchingRules[parentIdx]['derivedColumnsList'][value['sourceType']].splice(index, 1);
            }
        })

        $scope.recon.matchingRules[parentIdx]['derivedColumns'].splice(idx, 1);
    }

    $scope.addDerSrcDetaisRow = function () {
        console.log($scope.derivedSrc)
        $scope.derivedSrc.push({'masterDetails': $scope.recon})
        console.log($scope.derivedSrc)
    }

    $scope.selectedSourceDetails = function (data, index) {
        cols = []
        arraycheck = []

        //l̥TODO on change of source if not there reset the fields and conditions
        angular.forEach($scope.recon['sources'][data]['mdl_fields'], function (val, key) {
            if (arraycheck.indexOf(val['MDL_FIELD_NAME']) == -1) {
                arraycheck.push(val['MDL_FIELD_NAME'])
                cols.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})
            }
        })
        $scope.derivedSrc[index]['cols'] = cols
        console.log($scope.derivedSrc)
    }

    $scope.selectedColumn = function (value, index) {
        console.log($scope.derivedSrc)
        console.log(value)

        $scope.derivedSrc[index]['condOperators'] = $scope.conditionsArray[value['Default']]
    }

    /*$scope.onChangeCondition = function (val, index) {
     $scope.renderBooleanCond = true;

     if (val['value'] === '==' || val['value'] === '!=' || val['value'] === 'add_days' || val['value'] === 'sub_days') {
     //equality operators
     $scope.derivedSrc[index].disableconditionalVal1 = true
     $scope.derivedSrc[index].disableconditionalVal2 = false
     $scope.derivedSrc[index].condVal1PlaceHdrVal = 'value'

     } else if (val['value'] === 'index_substr') {
     $scope.derivedSrc[index].disableconditionalVal1 = true
     $scope.derivedSrc[index].disableconditionalVal2 = true
     $scope.derivedSrc[index].condVal1PlaceHdrVal = 'start index'
     $scope.derivedSrc[index].condVal2PlaceHdrVal = 'end index'
     } else if (val['value'] === 'char_substr') {
     $scope.derivedSrc[index].disableconditionalVal1 = true
     $scope.derivedSrc[index].disableconditionalVal2 = false
     $scope.derivedSrc[index].condVal1PlaceHdrVal = ''
     $scope.derivedSrc[index].condVal2PlaceHdrVal = ''
     }

     }*/

    $scope.addDerivedSourceCondition = function () {

        for (var str = "", i = 0; i < $scope.derivedSrc.length; i++) {
            str += $scope.derivedSrc["aliasName"] + "="
                + $scope.derivedSrc[i]["selectedSource"]
                + "[" + $scope.derivedSrc[i]["selectedSource"] + "['" + $scope.derivedSrc[i]["selectedCol"]["label"] + "']" + getOperatorValue($scope.derivedSrc[i]["selectedCol"]["Default"], $scope.derivedSrc[i]["selectedCond"]["label"]) + $scope.derivedSrc[i]["conditionalVal1"] + "]"
        }

        console.log(str)
    }

    /*function getOperatorValue(dataType,label){
     var conditionArray = $scope.conditionsArray[dataType]
     operator = ''
     angular.forEach(conditionArray, function (val, key) {
     if (val['label'] === label){
     operator = val['value']
     }
     })
     return operator
     }

     function buildDateTimeQuery(srcName, selCol, days) {
     queryTmpl = "pd.to_datetime($src_name['$src_col'], format='%Y-%m-%d %H:%M:%S', coerce=True) - timedelta(days=$days)"
     return queryTmpl.replace('$src_name', srcName).replace('$src_col', selCol).replace('$days', days)
     }

     function buildConditionalQuery(srcName, srcCol, trueVal, falseVal, condition, condVal) {
     queryTmpl = "$src_name['$src_col'].apply(lambda x: '$trueVal' if $condition $condVal else '$falseVal')"
     return queryTmpl.replace('$src_name', srcName).replace('$src_col', srcCol).replace('$trueVal', trueVal).replace('$falseVal', falseVal).replace('$condition', condition).replace('$condVal', condVal)
     }

     function buildStringQuery(srcName, srcCol, startIdx, endIdx, type) {
     queryTmpl = ''
     if (type == 'char_substr') {
     queryTmpl = "$src_name['$src_col'].apply(lambda x : '' if '$val' not in x else x[x.index('$startIdx'):])"
     return queryTmpl.replace('$src_name', srcName).replace('$src_col', srcCol).replace('$val', startIdx)
     }
     else if (type == 'index_substr') {
     queryTmpl = "$src_name['$src_col'].apply(lambda x: x[$startIdx:$endIdx])"
     return queryTmpl.replace('$src_name', srcName).replace('$src_col', srcCol).replace('$startIdx', startIdx).replace('$endIdx', endIdx)
     }
     }*/

    $scope.assignFeedStructure = function () {

        if (!angular.isDefined($scope.recon['sources'])) {
            $scope.recon.sources = []
        }
        $scope.recon.sources.push({'useInMatching': false, 'moveToProcessed': false, 'applyAccountFilter': false})
    }


    $scope.getFeedStructureDetails = function () {

        FeedDefinationService.get(undefined, undefined, function (data) {
            console.log("recon context")
            console.log(data)
            if (data.total > 0) {
                $scope.feedStructureDetails = data['data']

                // lookup of feedStructureID and feedName for UI display
                $scope.feedStrctIDLookup = {}
                angular.forEach($scope.feedStructureDetails, function (value, index) {
                    $scope.feedStrctIDLookup[value['_id']] = value['feedName']

                })
            }
        })
    }


    // Data-Table inline-editing for source details
    $scope.editSourceDetails = function (index) {
        $scope.enableEditing[index] = true;
    }

    $scope.cancelOperation = function (index) {
        $scope.enableEditing[index] = false;
    }

    $scope.deleteSourceDetails = function (index) {
        $scope.recon.sources.splice(index, 1)
    }

    $scope.saveSourceDetails = function (value, idx) {
        $scope.enableEditing[idx] = false;

        // add field details for the selected source
        angular.forEach($scope.feedStructureDetails, function (val, index) {
            if (val['_id'] == value['feedStructureID']) {
                $scope.recon.sources[idx]['fieldDetails'] = $scope.recon.sources[idx]['fieldDetails'] || {}
                $scope.recon.sources[idx]['fieldDetails'] = val['fieldDetails']
            }
        })
        // generate source id
        if (!angular.isDefined($scope.recon.sources[idx]['sourceId'])) {
            $scope.recon.sources[idx]['sourceId'] = generateUUID()
        }
    }


    // Data-Table inline-editing for matching rules
    /*$scope.editMatchRuleDetails = function (parentIdx, idx) {
     $scope.recon.matchingRules[parentIdx]['rules'][idx]['enableEditing'] = true;
     }*/

    $scope.cancelMatchRuleDetails = function (parentIdx, idx) {
        $scope.recon.matchingRules[parentIdx]['rules'][idx]['enableEditing'] = false;
    }

    $scope.deleteMatchRuleDetails = function (parentIdx, idx) {
        $scope.recon.matchingRules[parentIdx]['rules'].splice(idx, 1)
    }

    // Derived Condition tab related script
    $scope.changeOnSrc = function (value) {

        /*$scope.fieldDetailsArray = []
         feedIdArray = []

         // get all the feeds assigned to the selected source
         angular.forEach($scope.recon.sources, function (val, index) {
         if (val['SourceType'] == value['SourceType'] && val['useInMatching']) {
         feedIdArray.push(val['feedId'])
         }
         })

         angular.forEach($scope.feedStructureDetails, function (val, index) {
         if (feedIdArray.indexOf(val['feedId']) != -1) {
         $scope.fieldDetailsArray = $scope.fieldDetailsArray.concat(val['fieldDetails'])
         }
         })*/
    }

    $scope.changeOnMdlField = function (value, tabIndex, parentIdx, idx) {
        $scope.dataCondArray = []

        if (tabIndex == '1') {
            console.log($scope.srcDerivativeConditionsArray[value['dataType']])
            $scope.recon.matchingRules[parentIdx]['derivedSourcesQB'][idx]['dataCondArray'] = $scope.srcDerivativeConditionsArray[value['dataType']]
        } else {
            console.log($scope.colDerivateCondArray[value['dataType']])
            $scope.recon.matchingRules[parentIdx]['derivedColumnsQB'][idx]['dataCondArray'] = $scope.colDerivateCondArray[value['dataType']]
        }
    }

    $scope.addDerivedSrcCond = function (index, value) {

        var fieldDetailsArray = []
        var feedIdArray = []

        // get all the feeds assigned to the selected source
        angular.forEach($scope.recon.sources, function (val, index) {
            if (val['sourceType'] == value['sourceType'] && val['useInMatching']) {
                feedIdArray.push(val['feedId'])
            }
        })

        angular.forEach($scope.feedStructureDetails, function (val, index) {
            if (feedIdArray.indexOf(val['_id']) != -1) {
                fieldDetailsArray = fieldDetailsArray.concat(val['fieldDetails'])
            }
        })

        $scope.recon.matchingRules[index].derivedSourcesQB.push({'fields': fieldDetailsArray})

    }


    $scope.buildDerivedSrcExpression = function () {

    }

    // Data Conditioin tab related script
    $scope.addDataConditions = function (parentIdx, idx) {
        $scope.recon.matchingRules[parentIdx]['derivedColumnsQB'].push({})
    }

    $scope.removeDerivedSrcCond = function (parentIndex, index) {
        $scope.recon.matchingRules[parentIndex]['derivedSourcesQB'].splice(index, 1)

    }

    $scope.removeDerivedColCond = function (parentIdx, idx) {
        $scope.recon.matchingRules[parentIdx]['derivedColumnsQB'].splice(idx, 1)
    }

    $scope.addRuleGroup = function () {

        if (angular.isDefined($scope.recon['matchingRules']) && $scope.recon['matchingRules'].length > 0) {
            $scope.groupCounter = $scope.recon['matchingRules'].length + 1
        } else {
            $scope.groupCounter += 1;
        }

        var groupObj = {}

        //based on source processing count get derived sources
        /*for (var str = '', i = 1; i <= $scope.recon.processingCount; i++) {
         str = 's' + i + '_g' + $scope.groupCounter + ' = s' + i

         dataConditions.push(str)
         }*/

        groupObj = {
            'derivedSources': [],
            'derivedSourcesQB': [], // tmp variable used while building the expression
            'derivedColumns': [],
            'derivedColumnsQB': [], // tmp variable used while building the expression
            'rules': [],
            'groupIndex': $scope.groupCounter,
            'derivedColumnsList': {},
            'selDerivedSrcType': 'DERIVED_SRC',
            'virtualSourceArray': []
        }

        if (!angular.isDefined($scope.recon['matchingRules'])) {
            $scope.recon['matchingRules'] = []
        }
        $scope.recon.matchingRules.push(groupObj)
    }

    $scope.removeRuleGroup = function (index) {

        if ($scope.groupCounter > 0) {
            $scope.groupCounter = $scope.groupCounter - 1
        }
        $scope.recon.matchingRules.splice(index, 1)
    }

    $scope.addDerivedCol = function (sourceDetails, index) {
        // update field details for the selected source
        var fieldDetailsArray = []
        var feedIdArray = []

        // get all the feeds assigned to the selected source
        angular.forEach($scope.recon.sources, function (val, index) {
            if (val['sourceType'] == sourceDetails['sourceType'] && val['useInMatching']) {
                feedIdArray.push(val['feedId'])
            }
        })

        angular.forEach($scope.feedStructureDetails, function (val, index) {
            if (feedIdArray.indexOf(val['_id']) != -1) {
                fieldDetailsArray = fieldDetailsArray.concat(val['fieldDetails'])
            }
        })

        $scope.recon.matchingRules[index].derivedColumnsQB.push({
            'fieldDetailsArray': fieldDetailsArray,
            'excludeString': false
        })
    }

    $scope.saveDerivedSrcCond = function (index, srcDetails, srcAliasName) {

        var value = $scope.recon.matchingRules[index].derivedSourcesQB
        console.log(value)
        console.log(index)

        var str = "";
        var counterStr = "";
        var psuedoSrc = "";
        var condArray = []
        var virtualSrcIndex = 1
        var dfLabel = srcDetails['sourceType'].toLowerCase()


        // if it is a normal source
        if (angular.isDefined(srcDetails['virtualSrcIndicator']) && srcDetails['virtualSrcIndicator'] == true) {

            psuedoSrc = srcDetails['virtualSrcDFLabel']
            dfLabel = srcDetails['virtualSrcDFLabel']

        } else {
            psuedoSrc = srcDetails['sourceType'].toLowerCase() + '_g' + $scope.recon.matchingRules[index]['groupIndex']

            // incase of second group, s1_g1 df should be the input
            /* eg : group 1  s1_g1 = s1[s1['condition']]
             group 2  s1_g2 = s1_g1[s1_g1['condition']]
             */
            if ($scope.recon.matchingRules[index]['groupIndex'] > 1) {
                // current group index - 1
                dfLabel += ('_g' + ($scope.recon.matchingRules[index]['groupIndex'] - 1))
            }

            // for single source recon
            // since only one source available to diffrenciate use virtualSrcIndex
            if ($scope.recon.processingCount == 1) {
                var len = $scope.recon.matchingRules[index]['derivedSources'].length

                if (len > 0) {
                    virtualSrcIndex = $scope.recon.matchingRules[index]['derivedSources'][len - 1]['virtualSourceIndex'] + 1
                }
                psuedoSrc += '_' + virtualSrcIndex
            }
        }

        str += psuedoSrc + ' = ' + dfLabel + '['

        // incase of the derived source create counter condition
        // to be taken from the user
        if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {
            counterStr += psuedoSrc + '_fil = ' + dfLabel + '['
        }

        for (i = 0; i < value.length; i++) {

            //if ($scope.srcDerivativeConditionsArray['str'].indexOf(value[i]['selectedDataCond']) != -1) {
            if (value[i]['selectedMdlField']['dataType'] == 'str') {
                var condValue = ''
                // in-case of isin operator
                if (value[i]['selectedDataCond'] == 'isin' || value[i]['selectedDataCond'] == 'not isin') {
                    // add single quote (') leading and tailing
                    condValue = value[i]['dataCondVal'].replace("^", "'").replace('$', "'").replace(/,/g, "','")
                } else {
                    condValue = value[i]['dataCondVal']
                }

                /*// NOt null will not have any condition value
                 if (value[i]['selectedDataCond'] == 'is null' || value[i]['selectedDataCond'] == 'not null') {
                 // original expression
                 str += derivedSrcConditions[value[i]['selectedDataCond']].replace('$colName', value[i]['selectedMdlField']['mdlFieldName'])
                 //counter expression
                 counterStr += derivedSrcConditions[$scope.counterCondLookUP[value[i]['selectedDataCond']]].replace('$colName', value[i]['selectedMdlField']['mdlFieldName'])
                 } else {*/

                // original user input string
                str += derivedSrcConditions[value[i]['selectedDataCond']].replace(/df/g, dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', condValue)

                // incase of contains or not contains check for caseInsensitive flag
                if (value[i]['selectedDataCond'] == 'contains' || value[i]['selectedDataCond'] == 'not contains') {
                    if (angular.isDefined(value[i]['caseInsensitive']) && value[i]['caseInsensitive']) {
                        str = str.replace('$status', 'False')
                    }
                    else {
                        str = str.replace('$status', 'True')
                    }
                }


                if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {
                    // counter psuedo condition to get out-bound records and only for Derived_SRC
                    counterStr += derivedSrcConditions[$scope.counterCondLookUP[value[i]['selectedDataCond']]].replace(/df/g, dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', condValue)
                }

                // }
            } else if (value[i]['selectedMdlField']['dataType'] == 'np.int64' || value[i]['selectedMdlField']['dataType'] == 'np.float64') {
                //"(df['$colName'] $operator $colValue) selectedDataCond"

                // TODO IMP counter condition not created for numeric operations
                str += derivedSrcConditions['numericConditon'].replace('df', dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$operator', value[i]['selectedDataCond'])
                if (derivedSrcConditions['numericConditon'] != 'abs') {
                    str.replace('$colValue', value[i]['dataCondVal'])
                } else {
                    str.replace('$colValue', 'x')
                }

                if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {
                    // counter psuedo condition to get out-bound records and only for Derived_SRC
                    counterStr += derivedSrcConditions[$scope.counterCondLookUP[value[i]['selectedDataCond']]].replace(/df/g, dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', condValue)
                }

            } else if (value[i]['selectedMdlField']['dataType'] == 'np.datetime64') {
                $rootScope.showAlertMsg("Date columns cannot be part of derived source conditions")
                return;
                // break the loop and show validate msg
            }

            // append logical operator if any
            if (angular.isDefined(value[i]['logicalOperator']) &&
                value[i]['logicalOperator'] != '' && value.length - 1 > i) {
                str += ' ' + value[i]['logicalOperator'] + ' '

                if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {
                    // logical operator  OR (|) <--> AND (&)

                    counterStr += ' ' + (value[i]['logicalOperator'] == '|' ? '&' : '|') + ' '
                }
            }
        }
        str += ']'
        if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {
            counterStr += ']'
        }

        /*
         In-case of a split source following update
         --> splitSourceIndicator
         --> virtaulSourceIndex
         -->  sourceAliasName
         */
        var derivedsrcIdx = 0
        derivedsrcIdx = $scope.recon.matchingRules[index]['derivedSources'].push({
            'sourceType': srcDetails['sourceType'],
            'conditions': [str],
            'derivedSourceIndicator': false,
            'dfLabel': dfLabel // used by track engine to carry forward the unmatched records from one group to other
        })

        // in-case of derived source append counter condition
        if ($scope.recon.matchingRules[index]['selDerivedSrcType'] == 'DERIVED_SRC') {

            // updating the object with appropriate conditions
            $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['conditions'].push(counterStr)
            // used by back_end engine to diff between split source and derived source
            $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['derivedSourceIndicator'] = true

            if (angular.isDefined(srcDetails['virtualSrcIndicator'])) {
                // used by back_end engine to append virtual source index
                $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['virtualSourceIndex'] = srcDetails['virtualSourceIndex']
            }


            $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['dfLabel'] = dfLabel

        } else {
            $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['virtualSourceIndex'] = virtualSrcIndex
            /* $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['srcAliasName'] = srcAliasNam
             $scope.recon.matchingRules[index]['derivedSources'][derivedsrcIdx - 1]['splitSrcDFLabel'] = psuedoSrc*/

            // to track the virtual source details if any thing is added
            $scope.recon.matchingRules[index].virtualSourceArray.push({
                'sourceName': srcAliasName,
                'sourceType': srcDetails['sourceType'],
                'virtualSourceIndex': virtualSrcIndex,
                'useInMatching': true,
                'feedId': srcDetails['feedId'],
                'sourceId': generateUUID(),
                'virtualSrcIndicator': true,
                'virtualSrcDFLabel': psuedoSrc
            })
        }

        //$scope.recon.matchingRules[index]['derivedSources'].push(str)
        //$scope.recon.matchingRules[index]['derivedSources'].push(counterStr)
        $scope.recon.matchingRules[index]['derivedSourcesQB'] = []
    }

    $scope.saveDataCondition = function (index, srcDetails) {
        console.log(index)

        var str = ""
        var counterStr = ""
        var psuedoColLabel = $scope.recon.matchingRules[index]['derivedColName']
        var value = $scope.recon.matchingRules[index].derivedColumnsQB
        var dfLabel = ''
        var dType = ''
        //var colLoc = 0

        //psuedoSrc = srcDetails['SourceType'] + '_UM' + $scope.recon.matchingRules[index]['groupCounter']
        dfLabel = srcDetails['sourceType'].toLowerCase() + '_g' + $scope.recon.matchingRules[index]['groupIndex']

        // for single source recon append virtual source index
        if ($scope.recon.processingCount == 1) {
            if (angular.isDefined(srcDetails['virtualSourceIndex'])) {
                dfLabel += '_' + srcDetails['virtualSourceIndex']
            }
        }

        str += dfLabel + "['" + psuedoColLabel + "'] = "

        for (i = 0; i < value.length; i++) {

            dType = value[i]['selectedMdlField']['dataType']


            // assignment operator
            if (value[i]['selectedDataCond'] == 'assign') {
                str += dfLabel + "['" + value[i]['selectedMdlField']['mdlFieldName'] + "']"
            } else {
                if (value[i]['selectedMdlField']['dataType'] == 'str') {

                    // added new expression
                    if (value[i]['selectedDataCond'] == 'sub string') {
                        var strDataCondVal = ''

                        //in case of character based substring expected index from the user
                        /*
                         Type 1 : inclusive (default)
                         LHS : x.index('F') :
                         RHS : [: x.index('F') + 1]

                         Type 2 : exclusive
                         LHS : x.index('F') + 1 :
                         RHS : [: x.index('F')]
                         */
                        // check whether user data contains index or not
                        if (value[i]['dataCondVal'].includes('index')) {
                            var lhsPattern = /index.*?:/;
                            var rhsPattern = /:index/;

                            // check for exclusive flag
                            if (value[i]['excludeString']) {
                                // Type 2
                                if (lhsPattern.test(value[i]['dataCondVal'].replace(/ /g, ''))) {
                                    //replace " ') " with " ') + 1"
                                    strDataCondVal = value[i]['dataCondVal'].replace("')", "') + 1")
                                }
                                else if (rhsPattern.test(value[i]['dataCondVal'].replace(/ /g, ''))) {
                                    strDataCondVal = value[i]['dataCondVal']
                                }
                            } else {
                                // Type 1
                                if (lhsPattern.test(value[i]['dataCondVal'].replace(/ /g, ''))) {
                                    strDataCondVal = value[i]['dataCondVal']
                                }
                                else if (rhsPattern.test(value[i]['dataCondVal'].replace(/ /g, ''))) {
                                    strDataCondVal = value[i]['dataCondVal'] + ' + 1'
                                }
                            }

                            strDataCondVal = strDataCondVal.replace('index', 'x.index')

                        } else {
                            strDataCondVal = value[i]['dataCondVal']
                        }

                        //df['$colName'].apply(lambda x : x[$colValue])
                        str += derivedColStrExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colValue', strDataCondVal).replace('$colName', value[i]['selectedMdlField']['mdlFieldName'])

                    }
                    else if (value[i]['selectedDataCond'] == 'strip') {
                        //"df['$colName'].fillna('').str.strip()"
                        str += derivedColStrExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName'])
                    }
                    else if (value[i]['selectedDataCond'] == 'lstrip') {
                        //"df['$colName'].fillna('').str.strip($colValue)"
                        str += derivedColStrExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', value[i]['dataCondVal'])
                    }
                    else if (value[i]['selectedDataCond'] == 'replace') {
                        condValue = value[i]['dataCondVal1'] == undefined ? '' : value[i]['dataCondVal1']
                        condValue = condValue.replace("^", "'").replace('$', "'").replace(/,/g, "','")
                        //"df['$colName'].apply(lambda x : str(x).replace('$colValue','$colValue1')"
                        str += derivedColStrExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', value[i]['dataCondVal']).replace('$colValue1', condValue)
                    }
                    else {
                        //df['$colName'].fillna('').str.strip('$colValue')
                        str += derivedColStrExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$colValue', value[i]['dataCondVal'])
                    }

                }
                else if (value[i]['selectedMdlField']['dataType'] == 'np.datetime64') {

                    //"pd.to_datetime(df['$colName'], format='%Y-%m-%d %H:%M:%S', coerce=True) - timedelta(days=$colValue)"
                    //pd.to_datetime(df['$colName'], format='%Y-%m-%d %H:%M:%S', coerce=True).apply(lambda x: x.strftime('$colValue'))
                    str += derivedColDateExp[value[i]['selectedDataCond']].replace('df', dfLabel).replace('$colValue', value[i]['dataCondVal']).replace('$colName', value[i]['selectedMdlField']['mdlFieldName'])
                }
                else if (value[i]['selectedMdlField']['dataType'] == 'np.int64' || value[i]['selectedMdlField']['dataType'] == 'np.float64') {

                    //df['$colName'].apply(lambda x : x $cond $colValue)
                    var condValue = ''
                    condValue = numericOprMapper[value[i]['selectedDataCond']] // add, sub, multiply etc...
                    condValue = condValue.replace('$value', value[i]['dataCondVal']) // repalce + '$value' -- > + 100 etc..
                    // df['$colName'].apply(lambda x : $condValue)
                    str += derivedColNumericExp['numericCondition'].replace('df', dfLabel).replace('$colValue', value[i]['dataCondVal']).replace('$colName', value[i]['selectedMdlField']['mdlFieldName']).replace('$condValue', condValue)
                }

                // append logical operator if any
                if (angular.isDefined(value[i]['logicalOperator']) &&
                    value[i]['logicalOperator'] != '' && value.length - 1 > i) {
                    str += ' ' + value[i]['logicalOperator'] + ' '
                }
            }


        }

        // push expression onto master json
        $scope.recon.matchingRules[index]['derivedColumns'].push({
            'condition': str,
            'sourceType': srcDetails['sourceType'],
            'columnLabel': psuedoColLabel
        })

        // if source type is not there create then assign
        if (!angular.isDefined($scope.recon.matchingRules[index]['derivedColumnsList'][srcDetails['sourceType']])) {
            $scope.recon.matchingRules[index]['derivedColumnsList'][srcDetails['sourceType']] = []
        }

        // update source wise derived columns for further processing
        $scope.recon.matchingRules[index]['derivedColumnsList'][srcDetails['sourceType']].push({
            'mdlFieldName': psuedoColLabel,
            'dataType': dType
        })

        // clear the details entered
        $scope.recon.matchingRules[index]['derivedColumnsQB'] = []
        $scope.recon.matchingRules[index]['derivedColName'] = ''
        $scope.recon.matchingRules[index]['selectedSrcVal'] = ''
    }


    $scope.addMatchRules = function (index) {
        var fieldDetails = []
        var tmpObj = {}
        //$scope.cols.push({'label': val['MDL_FIELD_NAME'], 'Default': val['MDL_FIELD_DATA_TYPE']})

        angular.forEach($scope.recon.sources, function (outerValue, outerIdx) {
            if (outerValue['useInMatching']) {
                angular.forEach($scope.feedStructureDetails, function (innerValue, innerIdx) {
                    if (outerValue['feedId'] == innerValue['_id']) {
                        angular.forEach(innerValue['fieldDetails'], function (fieldValue, index) {
                            fieldDetails.push({'label': fieldValue['mdlFieldName'], 'Default': fieldValue})
                        })
                    }
                })
            }
        })

        /*angular.forEach($scope.recon.sources, function (outerValue, outerIdx) {
         angular.forEach(outerValue['fieldDetails'], function (innerValue, innerIdx) {

         console.log(innerValue['mdlFieldName']);
         fieldDetails.push({'label': innerValue['mdlFieldName'], 'Default': innerValue})
         })
         })*/

        $scope.recon.matchingRules[index]['rules'].push({'fieldDetails': fieldDetails, 'enableEditing': false})
    }

    $scope.saveReconDetails = function (recon) {

        console.log($scope.selectedBusinessContext)
        var businessContext = $scope.selectedBusinessContext
        recon['businessContext'] = businessContext
        recon['businessContextId'] = businessContext['BUSINESS_CONTEXT_ID']

        if (angular.isDefined(recon['_id'])) {
            ReconContextService.put(recon['_id'], recon, function (data) {
                console.log(data)
                $scope.recon = {}
                $rootScope.showSuccessMsg("Recon Details Updated Successfully.")
            }, function (err) {
                console.log(err)
                $rootScope.showAlertMsg(err.msg)
            })
        }
        else {
            var seqValue = 0
            SeqGeneratorService.get({"seqName": "reconIDSeq"}, undefined, function (data) {
                console.log(data)
                if (angular.isDefined(data)) {
                    seqValue = data['seq'] + 1
                    data['seq'] = seqValue
                    SeqGeneratorService.put(data['_id'], data, function (data) {
                        recon['reconId'] = businessContext['reconProcessCode'] + '_' + businessContext['assetClass'] + '_' + businessContext['GEOGRAPHY_CODE'] + '_' + seqValue

                        ReconContextService.post(recon, function (data) {
                            console.log(data)
                            $scope.recon = {}
                            $rootScope.showSuccessMsg("Recon Details Saved Successfully.")
                        }, function (err) {
                            console.log(err)
                            $rootScope.showAlertMsg(err.msg)
                        })

                    }, function (err) {
                        $rootScope.showAlertMsg(err.msg)
                    })
                }
                else {
                    $rootScope.showAlertMsg("Sequence not found defined")
                }
            })
        }

        $scope.groupCounter = 0

        // get latest recon details
        $scope.getReconDetails()
    }

    $scope.getFeedStructureDetails()


    $scope.openMatchRuleDetailsModal = function (_value, parentIdx, idx, mode) {
        $modal.open({
            templateUrl: 'addMatchingRules.html',
            windowClass: 'addGroupModalTemplate',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                $scope.matchRule = {'sourceMdlField': []}
                $scope.modelFieldArray = []

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.saveMatchRuleDetails = function (value) {


                    console.log(value, parentIdx, idx, mode)

                    // TODO check position (LHS / RHS is selected) if one selected and other not selected throw validation msg
                    /*if (angular.isDefined(value['sourceMdlField']) && value['sourceMdlField'].length > 0) {

                     var posCheckCounter = 0
                     angular.forEach(value['sourceMdlField'], function (val, index) {
                     if()
                     })
                     } else {

                     }*/

                    // check if lhs or rhs is available
                    // if there sort by sourcePosition
                    // then pass to the below for loop
                    var sortBySourcePosition = false
                    for (var i = 0; i < value['sourceMdlField'].length; i++) {
                        if (angular.isDefined(value['sourceMdlField'][i]['sourcePosition'])) {
                            sortBySourcePosition = true
                            break;
                        }
                    }


                    _saveObj = {}
                    _saveObj['ruleName'] = value['ruleName']
                    _saveObj['tag'] = value['tag']
                    _saveObj['oneToOneMatch'] = value['oneToOneMatch']
                    _saveObj['condition'] = $scope.prepareMatchingCond(value['sourceMdlField'], parentIdx, sortBySourcePosition, value['processType'], value['oneToOneMatch'])
                    _saveObj['selDropDownMdlField'] = getMdlFldDropDownSel(value['sourceMdlField'])
                    _saveObj['columnDisplayString'] = prepareSelColDisplay(value['sourceMdlField'])

                    if (mode == 'edit') {
                        $scope.recon.matchingRules[parentIdx]['rules'][idx] = _saveObj
                    }
                    else {
                        $scope.recon.matchingRules[parentIdx]['rules'].push(_saveObj)
                    }

                    //}
                    $scope.cancel()
                }

                // sort objects by source type
                function sortBySourceType() {
                    var bySourceType = [].concat($scope.recon.sources, $scope.recon.matchingRules[parentIdx]['virtualSourceArray']).slice(0);
                    bySourceType.sort(function (a, b) {
                        var x = a.sourceType.toLowerCase();
                        var y = b.sourceType.toLowerCase();
                        return x < y ? -1 : x > y ? 1 : 0;
                    });
                    return bySourceType
                }

                // pre-serve model fields selection to be used while editing
                function getMdlFldDropDownSel(value) {
                    tmpObj = {}

                    angular.forEach(value, function (val, index) {
                        //tmpObj[val['sourceType']] = {}

                        if (angular.isDefined(val['normalColumns']) || angular.isDefined(val['fuzzyColumns'])) {

                            tmpObj[val['sourceId']] = {}
                            console.log(val)

                            if (angular.isDefined(val['normalColumns'])) {
                                tmpObj[val['sourceId']]['normalColumns'] = val['normalColumns']
                            }
                            if (angular.isDefined(val['fuzzyColumns'])) {
                                tmpObj[val['sourceId']]['fuzzyColumns'] = val['fuzzyColumns']
                            }

                            // add position if selected LHS yields to 1 && RHS yiels to 2
                            console.log(val['sourcePosition'])
                            if (angular.isDefined(val['sourcePosition'])) {
                                tmpObj[val['sourceId']]['sourcePosition'] = val['sourcePosition']
                            }
                        }
                    })

                    return tmpObj
                }

                /* Select columns will be formated as below to display
                 SOURCENAME[COLVALUE1,COLVALUE2]
                 */
                function prepareSelColDisplay(value) {
                    var normalCol = ''
                    var fuzzyCol = ''
                    angular.forEach(value, function (val, index) {

                        normalCol += val['sourceName'] + ' ('
                        fuzzyCol += val['sourceName'] + ' ('

                        //check if normal column exists
                        if (angular.isDefined(val['normalColumns'])) {
                            angular.forEach(val['normalColumns'], function (nrmlCols, innerIdx) {
                                //normalCol += nrmlCols['label']
                                normalCol += nrmlCols['id']['mdlFieldName']

                                // just to format the string corently
                                if (innerIdx < val['normalColumns'].length - 1) {
                                    normalCol += ", "
                                }
                            })
                        }
                        //check if normal column exists
                        if (angular.isDefined(val['fuzzyColumns'])) {
                            angular.forEach(val['fuzzyColumns'], function (fuzzyCols, innerIdx) {
                                //fuzzyCol += fuzzyCols['label']
                                fuzzyCol += fuzzyCols['id']['mdlFieldName']

                                // just to format the string
                                if (innerIdx < val['fuzzyColumns'].length - 1) {
                                    fuzzyCol += ", "
                                }
                            })
                        }

                        normalCol += ')'
                        fuzzyCol += ')'

                        if (index < value.length - 1) {
                            normalCol += ', '
                            fuzzyCol += ', '
                        }
                    })
                    return {'normalDiscol': normalCol, 'fuzzyDisCol': fuzzyCol}
                }

                // populate source details
                $scope.populateSrcDetails = function () {
                    var sources = {}
                    var srcTypeChckArray = []

                    sources = sortBySourceType()

                    angular.forEach(sources, function (sourceDetail, outerIdx) {

                        /*if (srcTypeChckArray.indexOf(sourceDetail['sourceType']) == -1) {
                         srcTypeChckArray.push(sourceDetail['sourceType'])
                         }*/
                        // show only sources that will participate in matching

                        if (sourceDetail['useInMatching']) {
                            angular.forEach($scope.feedStructureDetails, function (feedDetails, innerIdx) {
                                // feedID v/s feedID
                                if (sourceDetail['feedId'] == feedDetails['_id']) {

                                    var fieldsArray = []
                                    var obj = {}

                                    /*if (!angular.isDefined($scope.matchRule.sourceMdlField[srcTypeChckArray.indexOf(sourceDetail['sourceType'])])) {
                                     // update the source details
                                     $scope.matchRule.sourceMdlField[srcTypeChckArray.indexOf(sourceDetail['sourceType'])] = {
                                     'sourceName': sourceDetail['sourceName'],
                                     'sourceType': sourceDetail['sourceType'],
                                     'mdlFields': []
                                     }
                                     }*/

                                    obj['sourceName'] = sourceDetail['sourceName']
                                    obj['sourceType'] = sourceDetail['sourceType']

                                    // check for virtualSrcIndicator
                                    if (angular.isDefined(sourceDetail['virtualSourceIndex'])) {
                                        obj['virtualSourceIndex'] = sourceDetail['virtualSourceIndex']
                                    }

                                    //obj = $scope.matchRule.sourceMdlField[srcTypeChckArray.indexOf(sourceDetail['sourceType'])] // retrieve data from location
                                    fieldsArray = prepareFieldsList(feedDetails['fieldDetails']) // update the model fields

                                    // append psuedo columns if any is configured
                                    if (angular.isDefined($scope.recon.matchingRules[parentIdx]['derivedColumnsList'][sourceDetail['sourceType']])) {
                                        fieldsArray = fieldsArray.concat(prepareFieldsList($scope.recon.matchingRules[parentIdx]['derivedColumnsList'][sourceDetail['sourceType']]))
                                    }

                                    obj['mdlFields'] = fieldsArray // add updted fiedls to the json

                                    // add id for every instance of the object
                                    obj['sourceId'] = sourceDetail['sourceId']

                                    // add an empty array to capture normal and fuzzy cols
                                    obj['normalColumns'] = []
                                    obj['fuzzyColumns'] = []

                                    $scope.matchRule.sourceMdlField.push(obj) // asign back to the array

                                    // set default values if any here
                                    $scope.matchRule.processType = 'DEFAULT'

                                    console.log(obj)
                                }
                            })
                        }
                    })
                    console.log($scope.matchRule.sourceMdlField)
                }

                // prepare model fields to display in select item
                function prepareFieldsList(fieldsList) {
                    tmp = []
                    angular.forEach(fieldsList, function (value, index) {
                        tmp.push({'label': value['mdlFieldName'], 'Default': value})
                    })
                    return tmp
                }

                $scope.range = function (count) {
                    $scope.sourceCountArray = []
                    for (var i = 0; i < count; i++) {
                        $scope.sourceCountArray.push({})
                    }
                }


                $scope.prepareMatchingCond = function (sourceVal, parentIdx, sortByPosition, processType, oneToOneMatch) {

                    //var disFuzzyColStr = ''
                    //var matchFuncFuzzyColStr = ''
                    //var disNorColStr = ''
                    //var matchFuncNorColStr = ''
                    var srcValues = ''
                    var condition = ''
                    var matchFunNorColArr = []
                    var matchFunFuzzColArr = []

                    var tmpSourceVar = sourceVal
                    console.log(sourceVal)

                    // TODO need to capture source positions as mandatory
                    // in-case of single source need to capture source postions LHS / RHS as mandatory
                    if (sortByPosition) {

                        var bySourceType = sourceVal.filter(function (val) {
                            if (angular.isDefined(val['sourcePosition']) && val['sourcePosition'] != null && val['sourcePosition'] != '') {
                                return val
                            }
                        });

                        console.log(bySourceType)

                        bySourceType.sort(function (a, b) {
                            var x = a.sourcePosition;
                            var y = b.sourcePosition;
                            return x < y ? -1 : x > y ? 1 : 0;

                        });
                        tmpSourceVar = bySourceType
                    }


                    // iterate and get all columns of sourceMdlfieldDetails
                    angular.forEach(tmpSourceVar, function (feedVal, index) {

                        console.log(feedVal['normalColumns'])
                        console.log(feedVal['fuzzyColumns'])
                        // check whether columns are selected or not
                        //if (angular.isDefined(feedVal['normalColumns']) || angular.isDefined(feedVal['fuzzyColumns']))
                        if (feedVal['normalColumns'].length > 0 || feedVal['fuzzyColumns'].length > 0) {
                            // normalColumns --> label:mdl_field_id / default : fieldDetailsObj
                            if (angular.isDefined(feedVal['normalColumns'])) {
                                matchFunNorColArr.push(makeMatchRuleString(feedVal['normalColumns']))
                            }
                            if (feedVal['fuzzyColumns'].length > 0) {
                                matchFunFuzzColArr.push(makeMatchRuleString(feedVal['fuzzyColumns']))
                            }

                            // check if selected normal / fuzzy columns has any aggregate column [AMOUNT]

                            srcValues += feedVal['sourceType'].toLowerCase() + '_g' + $scope.recon.matchingRules[parentIdx]['groupIndex']
                            if (angular.isDefined(feedVal['virtualSourceIndex']) && $scope.recon.processingCount == 1) {
                                srcValues += "_" + feedVal['virtualSourceIndex']
                            }
                            srcValues += ' ,'

                            /*if (feedVal['normalColumns'].length > 0 || feedVal['normalColumns'].length > 0) {

                             srcValues += feedVal['sourceType'].toLowerCase() + '_g' + $scope.recon.matchingRules[parentIdx]['groupIndex']
                             if (angular.isDefined(feedVal['virtualSourceIndex']) && $scope.recon.processingCount == 1) {
                             srcValues += "_" + feedVal['virtualSourceIndex']
                             }
                             srcValues += ' ,'
                             }*/
                        }
                    })

                    /*for (i = 1; i <= $scope.recon.processingCount; i++) {
                     srcValues += 's' + i + '_g' + $scope.recon.matchingRules[parentIdx]['groupIndex'] + " ,"
                     }*/

                    //self.mfns(g2s1,g2s2,['VALUE_DATE:np.datetime64'],['VALUE_DATE:np.datetime64'],['CHEQUE_NUMBER:str'],['NARRATION:str'])
                    if (angular.isDefined(processType) && processType == 'DEFAULT') {
                        condition = "self.mfns.performMatch(" + srcValues + matchFunNorColArr.join(',')
                    } else {
                        condition = "self.mfns." + processType + "(" + srcValues + matchFunNorColArr.join(',')
                    }


                    if (matchFunFuzzColArr.length > 0) {
                        condition += ", " + matchFunFuzzColArr.join(',')
                    }

                    // incase of one_to_one match set aggregate as None
                    if (oneToOneMatch) {
                        condition += ', aggr1 = None , aggr2 = None'
                    }

                    condition += ")"

                    return condition
                }

                function makeMatchRuleString(value) {
                    var colsArray = []
                    if (angular.isDefined(value) && value.length > 0) {
                        angular.forEach(value, function (val, index) {
                            colsArray.push("'" + val['id']['mdlFieldName'] + ":" + val['id']['dataType'] + "'")
                        })
                        colsArray = colsArray.join(' , ')
                        colsArray = '[' + colsArray + ']'
                    }
                    else {
                        colsArray.push('[]')
                    }
                    return colsArray
                }

                $scope.checkMode = function () {
                    // pre-populate exisitng data on to modal
                    if (mode == 'edit') {

                        console.log($scope.matchRule)

                        // _value input-param
                        $scope.matchRule['ruleName'] = _value['ruleName']
                        $scope.matchRule['tag'] = _value['tag']
                        $scope.matchRule['oneToOneMatch'] = _value['oneToOneMatch']

                        // pre-populate the select box selection
                        // sourceMdlField (contains both source details and model field details)
                        angular.forEach($scope.matchRule['sourceMdlField'], function (value, index) {

                            var tmpObj = {}

                            // if normal or fuzzy columns exists for the selection drop down, update
                            if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']])) {
                                if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']]['normalColumns'])) {

                                    /*tmpObj = $scope.matchRule['sourceMdlField'][index] // retrive object from the index
                                     tmpObj['normalColumns'] = _value['selDropDownMdlField'][value['sourceId']]['normalColumns'] // update the object
                                     $scope.matchRule['sourceMdlField'][index] = tmpObj // set the object into specific location*/

                                    value['normalColumns'] = _value['selDropDownMdlField'][value['sourceId']]['normalColumns']
                                }
                            }

                            if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']])) {
                                if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']]['fuzzyColumns'])) {

                                    /*tmpObj = $scope.matchRule['sourceMdlField'][index] // retrive object from the index
                                     tmpObj['fuzzyColumns'] = _value['selDropDownMdlField'][value['sourceId']]['fuzzyColumns'] // update the object
                                     $scope.matchRule['sourceMdlField'][index] = tmpObj // set the object into specific location*/

                                    value['fuzzyColumns'] = _value['selDropDownMdlField'][value['sourceId']]['fuzzyColumns']

                                    //tmpObj = _value['selDropDownMdlField'][value['sourceType']]['fuzzyColumns']
                                }
                            }

                            // update position LHS/RHS
                            if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']])) {
                                if (angular.isDefined(_value['selDropDownMdlField'][value['sourceId']]['sourcePosition'])) {
                                    value['sourcePosition'] = _value['selDropDownMdlField'][value['sourceId']]['sourcePosition']
                                }
                            }

                        })

                        console.log(_value, parentIdx, idx, mode)
                    }
                }

                $scope.populateSrcDetails()
                $scope.range($scope.recon.processingCount)
                $scope.checkMode()


                /* $scope.saveMatchRuleDetails = function () {


                 var disFuzzyColStr = ''
                 var matchFuncFuzzyColStr = '['
                 var disNorColStr = ''
                 var matchFuncNorColStr = '['
                 var srcValues = ''

                 // get fuzzy columns
                 angular.forEach(value['fuzzyColumns'], function (val, index) {
                 disFuzzyColStr += val['label'] // used only for display purpose

                 //'VALUE_DATE:np.datetime64' (used in the matching function)
                 matchFuncFuzzyColStr += "'" + val['Default']['mdlFieldName'] + ":" + val['Default']['dataType'] + "'"

                 if (index < value['fuzzyColumns'].length - 1) {
                 matchFuncFuzzyColStr += ", "
                 disFuzzyColStr += ", "
                 }

                 })

                 // get normal columns
                 angular.forEach(value['normalColumns'], function (val, index) {
                 disNorColStr += val['label']

                 matchFuncNorColStr += "'" + val['Default']['mdlFieldName'] + ":" + val['Default']['dataType'] + "'"

                 if (index < value['normalColumns'].length - 1) {
                 matchFuncNorColStr += ", "
                 disNorColStr += ", "
                 }
                 })

                 matchFuncFuzzyColStr += ']'
                 matchFuncNorColStr += ']'

                 for (i = 1; i <= $scope.recon.processingCount; i++) {
                 srcValues += 's' + i + '_g' + $scope.recon.matchingRules[parentIdx]['groupIndex'] + " ,"
                 }

                 // update the master json
                 //self.mfns(g2s1,g2s2,['VALUE_DATE:np.datetime64'],['VALUE_DATE:np.datetime64'],['CHEQUE_NUMBER:str'],['NARRATION:str'])
                 $scope.recon.matchingRules[parentIdx]['rules'][idx]['condition'] = "self.mfns(" + srcValues + matchFuncNorColStr + ", " + matchFuncFuzzyColStr + ")"
                 $scope.recon.matchingRules[parentIdx]['rules'][idx]['fuzzyColDisplay'] = disFuzzyColStr
                 $scope.recon.matchingRules[parentIdx]['rules'][idx]['normalColDisplay'] = disNorColStr

                 // enable editing
                 $scope.recon.matchingRules[parentIdx]['rules'][idx]['enableEditing'] = false
                 }*/
            }
        });
    }

    $scope.clearReconDetails = function () {
        $scope.groupCounter = 0
        $scope.recon = {}
        $scope.selectedBusinessContext = {}
        $scope.selectedRecon = {}
    }

    $scope.cancelDerSrcOperation = function (idx) {
        $scope.recon.matchingRules[idx]['derivedSourcesQB'] = []

    }

    $scope.removedDerivedSrcValue = function (parentIdx, idx, value) {

        // remove the virtual source details from master virtual source array
        if (angular.isDefined($scope.recon.matchingRules[parentIdx]['virtualSourceArray'])) {
            var val = $scope.recon.matchingRules[parentIdx]['virtualSourceArray']
            for (i = 0; i < val.length; i++) {
                if (val['sourceId'] == value['sourceId']) {
                    $scope.recon.matchingRules[parentIdx]['virtualSourceArray'].splice(i, 1)
                    break
                }
            }
        }

        $scope.recon.matchingRules[parentIdx]['derivedSources'].splice(idx, 1)
    }


    $scope.concatSources = function () {
        console.log($scope.virtualSourceArray)
        return [].concat($scope.recon.sources, $scope.virtualSourceArray)
    }

    function generateUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };

    $scope.dropDownSetting = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true,
        idProp: 'Default'
    };

    $scope.cloneReconContext = function (data) {

        $modal.open({
            templateUrl: 'reconCloneTemplate.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance) {

                $scope.confirmCloning = function () {

                    if (angular.isDefined(data['_id'])) {
                        delete data['_id']
                    }

                    data['reconName'] = data['reconName'] + "_cloned"

                    ReconContextService.post($scope.recon, function (data) {
                        console.log(data)
                        $scope.recon = {}
                        $scope.cancel()
                        $rootScope.showSuccessMsg("Recon Details Cloned Successfully.")
                    }, function (err) {
                        $scope.cancel()
                        $rootScope.showAlertMsg(err.msg)
                    })
                    // get latest recon details
                    $scope.getReconDetails()
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.getReconDetails = function () {

        console.log($scope.selectedBusinessContext)
        var perColConditions = {};
        $scope.recon = {}
        perColConditions["businessContextId"] = $scope.selectedBusinessContext['BUSINESS_CONTEXT_ID']
        $scope.listOfData = []
        ReconContextService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
            console.log(data)
            if (data['total'] > 0) {
                $scope.listOfData = data.data
            } else {
                $rootScope.showAlertMsg("No recons configured for the selected business context")
            }
        })
    }

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    $scope.isEmpty = function (value) {
        if (angular.equals({}, value)) {
            return true
        } else {
            return false
        }
    }
}
