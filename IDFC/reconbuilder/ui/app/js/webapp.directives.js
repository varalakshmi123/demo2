var app = angular.module("webApp.directives", []);

/**
 * Sample usage
 * NOTE: Define "user" in controller as: $scope.user = {}
 <wizard complete="wizComplete(user)">
 <step title="Step 1">
 <form>
 <input ng-model="user.name" placeholder="UserName">
 </form>
 </step>
 <step title="Step 2">
 <form>
 <input ng-model="user.password" placeholder="password">
 </form>
 </step>
 <step title="Step 3">
 <form>
 <input ng-model="user.addr" placeholder="Address">
 </form>
 </step>
 <step title="Step 4">
 <form>
 <input ng-model="user.email" placeholder="Email">
 </form>
 </step>
 </wizard>
 */
app.directive('wizard', function () {
    return {
        restrict: 'E',
        scope: {complete: '&'},
        transclude: true,
        controller: ['$scope', function ($scope) {
            $scope.wizSteps = [];
            var currentStepIndex = 0;
            $scope.wizisFirstStep = true;
            $scope.wizisLastStep = false;
            $scope.wizwizNextText = "Next";

            $scope.wizselect = function () {
                angular.forEach($scope.wizSteps, function (step, index) {
                    step.curStep = false;
                    step.completedStep = false;
                    if (index < currentStepIndex) {
                        step.completedStep = true;
                    }
                    else if (index === currentStepIndex) {
                        step.curStep = true;
                    }
                });

                $scope.wizisFirstStep = false;
                $scope.wizisLastStep = false;
                $scope.wizNextText = "Next";
                if (currentStepIndex == 0) {
                    $scope.wizisFirstStep = true;
                }
                if (currentStepIndex + 1 === $scope.wizSteps.length) {
                    $scope.wizisLastStep = true;
                    $scope.wizNextText = "Finish";
                }
            }

            $scope.wiznext = function () {
                if (currentStepIndex + 1 < $scope.wizSteps.length) {
                    ++currentStepIndex;
                    $scope.wizselect();
                }
                else {
                    //Wizard complete
                    $scope.complete();

                }
            }

            $scope.wizprev = function () {
                if (currentStepIndex > 0) {
                    --currentStepIndex;
                    $scope.wizselect();
                }
                else {
                    //Disable prev button
                    console.log("you dog???");
                }
            }

            $scope.goto = function (step) {
                if (step < currentStepIndex) {
                    console.log("Goto step: " + step);
                    currentStepIndex = step;
                    $scope.wizselect();
                }
            }

            this.addStep = function (step) {
                $scope.wizSteps.push(step);
                //Select the first step by default
                $scope.wizselect();
            }
        }],
        template: '<div class="fuelux">' +
        '  <div class="wizard">' +
        '    <ul class="steps">' +
        '      <li ng-repeat="step in wizSteps" ng-class="{active:step.curStep, complete: step.completedStep}" ng-click="goto($index)"><span class="badge" ng-class="{\'badge-info\':step.curStep, \'badge-success\':step.completedStep}">{{$index+1}}</span>{{step.title}}<span class="chevron"></span></li>' +
        '    </ul>' +
        '    <div class="actions">' +
        '      <button type="button" class="btn btn-mini btn-prev" ng-click="wizprev()" ng-disabled="wizisFirstStep"> <i class="icon-arrow-left"></i>Prev</button>' +
        '      <button type="button" class="btn btn-mini btn-next" ng-click="wiznext()">{{wizNextText}}<i class="icon-arrow-right"></i></button>' +
        '    </div>' +
        '  </div>' +
        '  <div class="step-content" ng-transclude></div>' +
        '</div>',

        replace: true
    };
}).directive('step', function () {
    return {
        require: '^wizard',
        restrict: 'E',
        transclude: true,
        scope: {title: '@'},
        link: function (scope, element, attrs, wizardCtrl) {
            wizardCtrl.addStep(scope);
        },
        template: '<div class="step-pane" ng-class="{active: curStep}" ng-transclude></div>',
        replace: true
    };
});
/*app.directive('bsActiveLink', ['$location', function ($location) {
 return {
 restrict: 'A', //use as attribute
 replace: false,
 link: function (scope, elem) {
 //after the route has changed
 scope.$on("$routeChangeSuccess", function () {
 var hrefs = ['/#' + $location.path(),
 '#' + $location.path(), //html5: false
 $location.path()]; //html5: true
 angular.forEach(elem.find('a'), function (a) {
 a = angular.element(a);
 if (-1 !== hrefs.indexOf(a.attr('href'))) {
 a.parent().addClass('active');
 } else {
 a.parent().removeClass('active');
 };
 });
 });
 }
 };
 }]);*/
/*
 app.directive('datagrid1', ['$compile', '$rootScope', '$timeout', function($compile, $rootScope, $timeout) {
 return {
 restrict: 'E',
 scope: {
 options: "="
 },
 transclude: true,
 replace: true,
 link: function(scope, element, attrs){
 scope.search = "";
 scope.start = 0;
 scope.limit = 10;
 scope.sort = "";
 scope.sortDirection = -1;
 scope.options.reQuery = false;
 scope.searchFields = []
 scope.searchOn = []
 scope.imgMaps = {}; //key -> data field, val => object containing the imgMap pass from controller

 scope.ptrefresh = function(){
 if(angular.isDefined(scope.options.autorefresh) && scope.options.autorefresh > 0){
 scope.tout = $timeout(function(){
 scope.doQuery();
 scope.ptrefresh();
 }, scope.options.autorefresh);
 }
 }

 scope.doQuery = function(){
 $rootScope.loading = true;
 var queryParams = {
 start: scope.start,
 limit: scope.limit
 };

 if(angular.isDefined(scope.options.predefinedSearch))
 {
 queryParams.query = scope.options.predefinedSearch;
 }
 else if(scope.search.length > 0)
 {
 if(scope.searchOn.length === 0)
 {
 scope.searchOn = jQuery.map(scope.searchFields, function(val, index){return val.value;});
 }
 queryParams.search = {fields: scope.searchOn, search: scope.search};
 }
 if(scope.sort.length>0)
 {
 queryParams.sort = {};
 queryParams.sort[scope.sort] = scope.sortDirection;
 }
 if(angular.isDefined(scope.options.query) && angular.isFunction(scope.options.query))
 {
 scope.options.query(queryParams);
 $rootScope.loading = false;
 }
 else if(angular.isDefined(scope.options.service))
 {
 scope.options.service.get(undefined, queryParams, function(data) {
 scope.options.datamodel = data;
 $rootScope.loading = false;
 });
 }
 else
 {
 alert("No query func");
 }
 $rootScope.loading = true;
 }

 scope.doSearch = function(){
 //if(scope.queryoptions.search.length>3 || scope.queryoptions.search.length === 0)
 {
 scope.start = 0;
 scope.doQuery();
 }
 }

 scope.pageSizeChange = function() {
 scope.start = 0;
 scope.doQuery();
 }

 scope.sortChange = function(name) {
 if(name === scope.sort)
 {
 scope.sortDirection *= -1;
 }
 else
 {
 scope.sortDirection = 1;
 }
 scope.sort = name;

 scope.doQuery();
 }

 scope.nextPage = function() {
 if(scope.start + scope.options.datamodel.current < scope.options.datamodel.total)
 {
 scope.start += scope.limit;
 scope.doQuery();
 }
 }

 scope.prevPage = function() {
 if(scope.start >= scope.limit)
 {
 scope.start -= scope.limit;
 scope.doQuery();
 }
 }

 scope.firstPage = function() {
 if(scope.start !== 0)
 {
 scope.start = 0;
 scope.doQuery();
 }
 }

 scope.lastPage = function() {
 var lastPageStart = 0
 if(scope.options.datamodel.total%scope.limit === 0)
 {
 lastPageStart = ((scope.options.datamodel.total/scope.limit)-1) * scope.limit;
 }
 else
 {
 lastPageStart = scope.limit * Math.floor(scope.options.datamodel.total/scope.limit);
 }

 if(scope.start !== lastPageStart)
 {
 scope.start = lastPageStart;
 scope.doQuery();
 }

 }

 scope.doQuery();

 scope.ptrefresh();

 scope.$on('$destroy', function() {
 $timeout.cancel(scope.tout);
 });

 scope.$watch('options.reQuery',function(newValue){
 if(newValue == true)
 {
 scope.doQuery();
 scope.options.reQuery = false;
 }
 });

 scope.$watch('limit', function(value){
 scope.pageSizeChange();
 });

 scope.displayCount = [
 { value: 10, key: "10" },
 { value: 25, key: "25"},
 { value: 50, key: "50"},
 { value: 100, key: "100"}
 ];
 //Build table header (row above table)
 var addbutton = 'New';
 var hasAddOption = false;
 var refreshbutton = "";
 if(angular.isDefined(scope.options.refreshbutton) && scope.options.refreshbutton){
 refreshbutton = '<button ng-click="doQuery()" type="button" class="btn btn-default" tooltip="Refresh" tooltip-trigger="mouseenter"><i class="icon-refresh"></i>&nbsp;Refresh</button>';
 }
 if(angular.isDefined(scope.options.addAction) && angular.isFunction(scope.options.addAction))
 {
 hasAddOption = true;
 addbutton = '<br/><button ng-click="options.addAction()" type="button" class="btn btn-default" tooltip="'+scope.options.addBtnName+'" tooltip-trigger="mouseenter"><i class="icon-plus-sign"></i>&nbsp;'+scope.options.addBtnName+'</button>';
 }
 var header = '';
 header += '<div class="col-md-2">';
 //header += '  Show ';
 header += '  <myselect options="displayCount" class="input-mini" model="limit"></myselect>';
 header += '</div>';

 //if(hasAddOption)
 //{
 //    header += '<div class="col-md-3 col-md-offset-6">';
 //}
 //else
 //{
 header += '<div class="col-md-3 col-md-offset-7">';
 //}
 header += '  <div class="input-group">';
 header += '    <input type="text" placeholder="Search" ng-change="doSearch()" ng-model="search" class="form-control"/>';
 header += '    <span class="input-group-btn">';
 header += '      <button class="btn btn-default" type="button"><i class="icon-search"></i></button>';
 header += '    </span>';
 header += '  </div>';
 //header += '      <myselect multiple model="searchOn" options="searchFields"></myselect>';
 header += '</div>';

 //Build the Header and the Body columns (th, td)
 var thead = '';
 var tbody = '';
 var tdArray = [];
 var th = '';

 //start
 //var actions = '';
 //            angular.forEach(scope.options.headers, function(value, index){
 //                if(angular.isDefined(value.actions))
 //                {
 //                    actions += '<div class=\'btn-group\'>';
 //                    angular.forEach(value.actions, function(action, actionIndex){
 //                        actions += '<button type=\'button\' class=\'btn btn-sm btn-default\' ng-click=\'options.'+action.action +'(data, $index)\'>';
 //                        if(angular.isDefined(action.preicon))
 //                        {
 //                            actions += '<i class=\''+action.preicon+'\'></i> '
 //                        }
 //
 //                        actions += action.name;
 //
 //                        if(angular.isDefined(action.posticon))
 //                        {
 //                            actions += ' <i class=\''+action.posticon+'\'></i>'
 //                        }
 //                        actions += '</button>';
 //                    });
 //                    actions +='</div>';
 //                }
 //            });
 //stop


 angular.forEach(scope.options.headers, function(rowDesc, rowIndex){
 thead += "<tr>";
 th = '';
 angular.forEach(rowDesc, function(value, index){
 if(angular.isDefined(value.searchable) && value.searchable)
 {
 scope.searchFields.push({'key': value.name, 'value': value.dataField})
 }

 if(angular.isDefined(value.visible) && value.visible == false)
 {
 //May be the field is defined only for search, not user visible!!!
 return;
 }

 //Handle header <th> creation
 th += '<th ';

 if(angular.isDefined(value.colspan))
 {
 th += 'colspan=\"'+value.colspan+'\" ';
 }
 if(angular.isDefined(value.rowspan))
 {
 th += 'rowspan=\"'+value.rowspan+'\" ';
 }
 if(angular.isDefined(value.align))
 {
 th += 'text-align=\"'+value.align+'\" ';
 }
 if(angular.isDefined(value.toolTip))
 {
 th += 'title=\"'+value.toolTip+'\" ';
 }
 if(angular.isDefined(value.dataField))
 {
 th += 'ng-class="{sortable: '+value.sortable+', sorted: \''+value.dataField +'\' === sort}" ng-click="sortChange(\''+value.dataField+'\')" ';
 th += '>'+ value.name +
 '          <i ng-show="\''+ value.dataField + '\'== sort && sortDirection == 1" class="icon-chevron-up"></i>'+
 '          <i ng-show="\''+ value.dataField + '\'== sort && sortDirection == -1" class="icon-chevron-down"></i>';
 }
 else
 {
 th += '>'+ value.name;
 }


 //Handle body <td> creation
 //Sortable header and body
 if(angular.isDefined(value.dataField))
 {
 var td = '';
 td += '<td class="text-center">';
 if(!angular.isDefined(value.imgMap))
 {
 td += '{{data.'+value.dataField;
 if(angular.isDefined(value.filter))
 {
 td += '|' + value.filter;
 }
 td += '}}';
 }
 else
 {
 scope[value.dataField] = {}
 //angular.copy(value.imgMap, scope.imgMaps[value.dataField]);
 angular.copy(value.imgMap, scope[value.dataField]);
 //td += '<img ng-src="{{ imgMaps["'+value.dataField+'"][data.'+value.dataField+'] }}">';
 td += '<img alt="{{ data.'+value.dataField+' }}" title="{{ data.'+value.dataField+' }}" ng-src="{{ '+value.dataField+'[data.'+value.dataField+'] }}">';
 }
 td +='</td>';
 tdArray[value.colIndex] = td;
 }

 if(angular.isDefined(value.selectoptions))
 {
 var td = '';
 td += '<td class="text-center">';
 td += '<div class="btn-group">';
 angular.forEach(value.selectoptions, function(action, actionIndex){
 console.log(action);
 td += '<myselect model="'+action.smodal+'" name="'+action.name+'" options="'+action.options+'"  key="'+action.key+'" value="'+action.value+'"></myselect>';
 });
 td +='</div>';
 td +='</td>';
 tdArray[value.colIndex] = td;
 }

 if(angular.isDefined(value.actions))
 {
 var td = '';
 td += '<td class="text-center">';
 td += '<div class="btn-group">';
 angular.forEach(value.actions, function(action, actionIndex){
 td += '<button type="button" class="btn btn-sm btn-default" ng-click="options.'+action.action +'(data, $index)" ';
 if(angular.isDefined(action.toolTip))
 {
 td += 'tooltip=' + action.toolTip;
 }
 td += '>';
 if(angular.isDefined(action.preicon))
 {
 td += '<i class="'+action.preicon+'"></i> '
 }

 if(angular.isDefined(action.name))
 {
 td += action.name;
 }

 if(angular.isDefined(action.posticon))
 {
 td += ' <i class="'+action.posticon+'"></i>'
 }
 td += '</button>';
 });
 td +='</div>';
 td +='</td>';
 tdArray[value.colIndex] = td;
 }

 th +='</th>';

 });
 thead += th;
 thead += "</tr>";
 });

 tbody = tdArray.join("");

 //Build  table footer (row below table)
 var footer = '';
 footer += '  <div class="col-md-3">';
 footer += '    <button type="button" class="btn btn-default btn-xs btn-prev" ng-click="firstPage()" tooltip="First"  tooltip-trigger="mouseenter"><i class="icon-step-backward"></i></button>';
 footer += '    <button type="button" class="btn btn-default btn-xs btn-prev" ng-click="prevPage()" tooltip="Prev"  tooltip-trigger="mouseenter"><i class="icon-chevron-left"></i></button>';
 footer += '    <button type="button" class="btn btn-default btn-xs btn-next" ng-click="nextPage()" tooltip="Next"  tooltip-trigger="mouseenter"><i class="icon-chevron-right"></i></button>';
 footer += '    <button type="button" class="btn btn-default btn-xs btn-next" ng-click="lastPage()" tooltip="Last"  tooltip-trigger="mouseenter"><i class=" icon-step-forward"></i></button>';
 footer += '  </div>';
 footer += '  <b class="col-md-2 col-md-offset-7">';
 footer += '    <ng-pluralize count="options.datamodel.total" when="{\'0\': \'No Records.\', ';
 footer += '                                                         \'1\': \'1 Record.\', ';
 footer += '                                                         \'other\': \'{{start+1}} - {{start+options.datamodel.current}} of {{options.datamodel.total}}\'}"></b>';
 footer += '  <br>';

 var html;
 html  = '<div>';
 html += '  <div class="row">';
 html +=      header;
 html += '  </div>';
 html += '  <div class="row">';
 html += '    <div class="col-md-12">';
 html += '      <table class="table table-bordered table-condensed table-hover datagrid datagrid-stretch-header">';
 html += '        <thead>';
 html +=              thead;
 html += '        </thead>';
 html += '        <tbody>';
 html += '          <tr ng-repeat="data in options.datamodel.data" ng-class="options.highlightIndex === $index ? \'warning\': \'\'">';
 html +=              tbody;
 html += '          </tr>';
 html += '        </tbody>';
 html += '      </table>';
 html += '    </div>';
 html += '  </div>';
 html += '  <div class="row">';
 html +=      footer;
 html += '  </div>'
 html += '</div>';
 if(hasAddOption)
 {
 //html += '<br/><button ng-click="options.addAction()" type="button" class="btn btn-default" tooltip="New User" tooltip-trigger="mouseenter"><i class="icon-plus-sign"></i>&nbsp;New User</button>';
 html += addbutton + refreshbutton;
 }else{
 html += '<br>'+refreshbutton;
 }
 element.html(html);
 $compile(element.contents())(scope);
 }
 };
 }]);*/
app.directive('datagrid', ['$compile', '$rootScope', '$filter', '$timeout',
    function ($compile, $rootScope, $filter, $timeout) {
        return {
            restrict: 'E',
            scope: {
                options: "="
            },
            //transclude: true,
            replace: true,
            link: function (scope, element, attrs) {
                scope.start = 0;
                scope.limit = 10;
                scope.paginationRange = 7;
                //scope.limit = (angular.isDefined(scope.options) && angular.isDefined(scope.options.limit)) ? scope.options.limit : scope.limit;
                scope.perColConditions = (angular.isDefined(scope.options) && angular.isDefined(scope.options.perColConditions)) ? scope.options.perColConditions : "";
                scope.sort = (angular.isDefined(scope.options) && angular.isDefined(scope.options.sortField)) ? scope.options.sortField : "";
                scope.sortName = "";
                scope.sortNameDirection = 1;
                scope.sortDirection = (angular.isDefined(scope.options) && angular.isDefined(scope.options.sortBy)) ? scope.options.sortBy : 1;
                scope.options.reQuery = false;
                scope.manulasort = true;
                scope.globalsearch = ""; //all columns search
                scope.searchFields = {}; //Per column search
                scope.imgMaps = {}; //key -> data field, val => object containing the imgMap pass from controller
                scope.options.selectedItems = {};
                scope.options.nextPage = function () {
                    scope.nextPage();
                };
                scope.options.prevPage = function () {
                    scope.prevPage();
                };
                scope.options.makeSearchEmpty = function () {
                    scope.searchFields = {};
                };
                scope.options.resetPaging = function () {
                    scope.start = 0;
                };
                scope.ptrefresh = function () {
                    if (angular.isDefined(scope.options.autorefresh) && scope.options.autorefresh > 0) {
                        scope.tout = $timeout(function () {
                            scope.doQuery();
                            scope.ptrefresh();
                        }, scope.options.autorefresh);
                    }
                }
                scope.drowUi = function () {
                    //Build the Header and the Body columns (th, td)
                    var thead = '';
                    var tbody = '';
                    var thSearch = '';
                    var tdArray = [];
                    var th = '';
                    var rowStyle = '';

                    angular.forEach(scope.options.headers, function (rowDesc, rowIndex) {
                        thead += '<tr role="row">';
                        thSearch += '<tr>';
                        if (angular.isDefined(scope.options.rowSelect) && scope.options.rowSelect) {
                            thead += '<th> </th>';
                            thSearch += '<th> </th>';
                            tdArray.push('<td><label class="input-control checkbox small-check no-margin"><input type="checkbox" ng-model="options.selectedItems[$index]" ng-true-value="{{data}}"><span class="check"></span></label></td>');
                        }
                        th = '';
                        angular.forEach(rowDesc, function (value, index) {
                            var visibleCol = '';
                            if (angular.isDefined(value.visibleCol)) {
                                visibleCol = value.visibleCol;
                            }
                            if (angular.isDefined(value.rowStyle)) {
                                rowStyle = 'background-color : {{data.' + value.rowStyle + ' | rowStyle}}';
                                /*	if(value.rowStyle==true)
                                 {
                                 //rowStyle = 'background-color : lightgreen';
                                 }*/
                            }

                            if (angular.isDefined(value.hide) && value.hide == true) {
                                return;
                            }
                            if (angular.isDefined(value.searchable) && value.searchable && angular.isDefined(value.autoFocus)) {

                                thSearch += '<th class="' + visibleCol + '"><input type="text" class="form-control"  focus-me=true  ng-model="searchFields.' + value.dataField + '" ng-keypress="doSearch($event)"/></th>';
                            }
                            else if (angular.isDefined(value.searchable) && value.searchable) {
                                thSearch += '<th class="' + visibleCol + '"><input type="text" class="form-control"  ng-model="searchFields.' + value.dataField + '" ng-keypress="doSearch($event)"/></th>';
                            } else if (angular.isDefined(value.selectAll) && value.selectAll) {
                                thSearch += '<th class="' + visibleCol + '"><label class="input-control checkbox small-check no-margin"><input type="checkbox" ng-model="options.selectedAll" ng-click="options.checkAll()"><span class="check"></span></label></th>';
                            }
                            //dropdown


                            else if (angular.isDefined(value.dropdown) && value.dropdown) {
                                thSearch += '<th class="' + visibleCol + '"> <select id="drop" style="padding-left: 3px;padding-right: 6px;" class="form-control" ng-model="searchFields.' + value.dataField + '" ng-change="doQuery()"><option value="">All</option><option value="Azure">Azure</option><option value="FileStor">File</option><option value="DiskStor">Image</option><option value="vmWare">VM</option></select>';
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                thSearch += '</th>'
                            }

                            else if (angular.isDefined(value.droplist) && value.droplist) {
                                thSearch += '<th class="' + visibleCol + '"> <select id="drop" style="padding-left: 3px;padding-right: 6px;" class="form-control" ng-model="searchFields.' + value.dataField + '" ng-change="doQuery()"><option value="">All</option><option value="true">Added to backup</option><option value="false">Not added to backup</option></select>';
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                thSearch += '</th>'
                            }

                            //dropdown
                            //date picker begin
                            else if (angular.isDefined(value.datepicker) && value.datepicker) {

                                //alert("picker")
                                thSearch += '<th class="' + visibleCol + '"><input type="date" class="form-control"  ng-model="searchFields.' + value.dataField + '" ng-change="doQuery()"/></th>';
//
                            }
                            //date picker end


                            else {
                                thSearch += '<th class="' + visibleCol + '"></th>';
                            }

                            if (angular.isDefined(value.visible) && value.visible == false) {
                                //May be the field is defined only for search, not user visible!!!
                                return;
                            }

                            //Handle header <th> creation

                            th += '<th ';
                            attr = (angular.isDefined(value.width) && value.width) ? 'style="width:' + value.width + '"' : '';
                            if (angular.isDefined(value.sortManual)) {
                                th += 'class="' + visibleCol + ' sorting" ng-click="sortManualChange(\'' + value.dataField + '\')"';
                            } else if (angular.isDefined(value.dataField) && value.sortable) {
                                if (scope.sortDirection == 1 && value.dataField == scope.sort) {
                                    th += 'class="' + visibleCol + ' sorting_asc" ng-click="sortChange(\'' + value.dataField + '\')"  ' + attr + ' ';
                                } else if (scope.sortDirection == -1 && value.dataField == scope.sort) {
                                    th += 'class="' + visibleCol + ' sorting_desc" ng-click="sortChange(\'' + value.dataField + '\')" ' + attr + ' ';
                                } else {
                                    th += 'class="' + visibleCol + ' sorting" ng-click="sortChange(\'' + value.dataField + '\')" ' + attr + ' ';
                                }
                            } else if (angular.isDefined(value.dataField) && value.sortSchedulerList) {
                                th += 'class="' + visibleCol + ' sorting" ng-click="sortSchedulerList(\'' + value.dataField + '\')" ' + attr + ' ';
                            } else if (angular.isDefined(value.checkbox) && value.checkbox) {
                                th += 'style="width:4%"';
                            } else {
                                th += 'class="' + visibleCol + '" ' + attr + ' ';
                            }
                            th += '>' + value.name;


                            //Handle body <td> creation
                            //Sortable header and body
                            if (!angular.isDefined(value.dataField)) {
                            } else {
                                var td = '';
                                var attr = '';
                                attr = (angular.isDefined(value.align) && value.align && angular.isDefined(value.width) && value.width) ? 'style="text-align:' + value.align + '; width:' + value.width + '"' : '';
                                td += '<td style="word-wrap: break-word;" class="' + visibleCol + '" ' + attr + ' ';
                                if (angular.isDefined(value.isHtml) && value.isHtml) {
                                    td += 'ng-bind-html="\'';
                                }
                                else {
                                    td += ">";
                                }
                                if (angular.isDefined(value.showHideFn) && value.dummy) {
                                    showHide = 'ng-show="options.' + value.showHideFn + '(data, $index)"';
                                    td += '<div ' + showHide + " " + ' >';
                                    td += '{{data.' + value.dataField;
                                    td += '}}';
                                    td += '</div></td>'
                                }

                                if (angular.isDefined(value.isLink) && value.isLink) {
                                    td += '<a class="tdlinks" ng-click="options.edit(data)">';
                                    td += '{{data.' + value.dataField;
                                    td += '}}';
                                    td += '</a></td>'
                                }
                                if (angular.isDefined(value.isPerLink) && value.isPerLink) {
                                    td += '<a class="tdlinks" ng-click="options.' + value.fnCall + '(data)">';
                                    if (angular.isDefined(value.imgMap)) {
                                        angular.copy(value.imgMap, scope[value.dataField]);
                                        var title = (angular.isDefined(value.tooltip) && value.tooltip) ? value.dataField : '';
                                        td += '<img title="{{ data.' + title + ' }}" ng-src="{{ ' + value.dataField + '[data.' + value.dataField + '] }}">';
                                    } else {
                                        if (angular.isDefined(value.filter)) {
                                            td += '{{data.' + value.dataField + ' | ' + value.filter;
                                        } else {
                                            td += '{{data.' + value.dataField;
                                        }
                                        td += '}}';
                                    }
                                    td += '</a></td>'
                                }
                                if (angular.isDefined(value.isSelect) && value.isSelect) {
                                    var tooltip = "";
                                    var disableFn = "";
                                    var showHide = "";
                                    var hvr = "";
                                    if (angular.isDefined(value.hoverFunction)) {
                                        hvr = 'ng-mouseover="options.' + value.hoverFunction + '(data, $index)"';
                                    }
                                    if (angular.isDefined(value.toolTip)) {
                                        tooltip = 'tooltip="{{ options.' + value.toolTip + ' }}" ';
                                    }
                                    if (angular.isDefined(value.disabledFn)) {
                                        disableFn = 'ng-disabled="options.' + value.disabledFn + '(data, $index)"';
                                    }
                                    if (angular.isDefined(value.showHideFn)) {
                                        showHide = 'ng-show="options.' + value.showHideFn + '(data, $index)"';
                                    }
                                    if (angular.isDefined(value.selectDataField) && value.selectDataField) {
                                        td += '{{value}}<select ' + hvr + ' ' + showHide + ' ' + disableFn + ' ' + tooltip + ' class="inputTextBox2" ng-change="options.' + value.changeFunction + '(data, $index)" ng-model="data.' + value.dataField + '" ng-options="' + value.selectOptions + ' data.' + value.selectDataField;
                                        if (angular.isDefined(value.filter)) {
                                            td += '| ' + value.filter + ": data";
                                        }
                                        td += '">';
                                        td += '<option value="">' + value.optionText + '</option>'
                                    } else if (angular.isDefined(value.useOptionField) && value.useOptionField) {
                                        var hvr = "";
                                        if (angular.isArray(scope.options[value.useOptionField])) {
                                            if (angular.isDefined(value.hoverFunction)) {
                                                hvr = 'ng-mouseover="options.' + value.hoverFunction + '(data, $index)"';
                                            }
                                            td += '{{value}}<select ' + showHide + ' ' + disableFn + ' ' + tooltip + ' class="inputTextBox2" ng-change="options.' + value.changeFunction + '(data, $index)" ' + hvr + '  ng-model="data.' + value.dataField + '" ng-options="' + value.selectOptions + ' options.' + value.useOptionField;
                                            if (angular.isDefined(value.filter)) {
                                                td += '| ' + value.filter + ": data";
                                            }
                                            td += '">';
                                            td += '<option value="">' + value.optionText + '</option>'
                                        }
                                    } else {
                                        scope.options[value.dataField] = value.selectData;
                                        td += '{{value}}<select ' + hvr + ' ' + showHide + ' ' + disableFn + ' ' + tooltip + ' class="inputTextBox2" ng-change="options.' + value.changeFunction + '(data, $index)" ng-model="data.' + value.dataField + '" ng-options="' + value.selectOptions + ' options.' + value.dataField;
                                        if (angular.isDefined(value.filter)) {
                                            td += '| ' + value.filter + ": data";
                                        }
                                        td += '">';
                                        td += '<option value="">' + value.optionText + '</option>'
                                    }
                                    td += '</select></td>'
                                }

                                /*/////////////////////////////
                                 if (angular.isDefined(value.isTruncate) && value.isTruncate) {
                                 td += '<a class="tdlinks" tooltip-placement="right" tooltip="{{data.' + value.dataField;
                                 td += '}}" tooltip-trigger="mouseenter" ng-click="options.edit(data)" >';
                                 td += '{{data.' + value.dataField;
                                 td += ' | truncate : 30}}';
                                 td += '</a></td>'
                                 }
                                 /////////////////////////////*/

                                if (angular.isDefined(value.isTruncate) && value.isTruncate) {
                                    td += '<a class="tdlinks" ng-click="options.edit(data)" >';
                                    td += '{{data.' + value.dataField;
                                    td += ' | truncate : 30}}';
                                    td += '</a></td>'
                                }
                                if (angular.isDefined(value.isSummaryTruncate) && value.isSummaryTruncate) {
                                    td += '<a class="tdlinks" ng-click="options.showSummary(data)" >';
                                    td += '{{data.' + value.dataField;
                                    td += ' | truncate : 150}}';
                                    td += '</a></td>'
                                }
                                if (angular.isDefined(value.isFormatTimeStamp) && value.isFormatTimeStamp) {
                                    td += '{{data.' + value.dataField;
                                    td += ' | formatTimeStamp }}';
                                    td += '</td>'
                                }
                                if (angular.isDefined(value.isImageText) && value.isImageText) {

                                    td += '<span ng-repeat="i in data.' + value.dataField + '" > <span class="name " ng-class="{online:\'online\', busy:\'busy\',offline:\'offline\',idle:\'away\'}[options.hasAcceptedTnC1(i)]" >{{i}} &nbsp;</span></span> ';
//                                    td += ' {{data.' + value.dataField;
//                                    td += '}}';
                                    td += '</td>'
                                }
                                if (angular.isDefined(value.isPopover)) {
                                    value.isPopover.placement = (value.isPopover.placement == undefined) ? "right" : value.isPopover.placement;
                                    td += '<a class="tdlinks" popover-template="' + value.isPopover.template + '.html" popover-toggle="data.close"  popover-trigger="manual" ng-click="options.popoveredit(data)" popover-placement="' + value.isPopover.placement + '"  popover-customclass="' + value.isPopover.popclass + '">';
                                    td += '{{data.' + value.dataField;
                                    if (angular.isDefined(value.filter)) {
                                        td += '| ' + value.filter;
                                    }
                                    td += '}}';
                                    td += '</a></td>'
                                }
                                if (angular.isDefined(value.iconMap)) {
                                    td += '<span  class="name ';
                                    td += '{{data.' + value.dataField;
                                    if (angular.isDefined(value.filter)) {
                                        td += '| ' + value.filter + ' | lowercase ';
                                    }
                                    td += '}}"';
                                    td += ' >';
                                    if (angular.isDefined(value.filter)) {
                                        td += '&nbsp;<span ng-show="false" ng-bind="data.presence=(data.' + value.dataField + '| ' + value.filter + ' | lowercase)"></span>';
                                    }
                                    td += ' </span>';
                                } else if (angular.isDefined(value.imgMap)) {
                                    scope[value.dataField] = {}
                                    //angular.copy(value.imgMap, scope.imgMaps[value.dataField]);
                                    angular.copy(value.imgMap, scope[value.dataField]);
                                    //td += '<img ng-src="{{ imgMaps["'+value.dataField+'"][data.'+value.dataField+'] }}">';
                                    if (angular.isDefined(value.filter)) {
                                        var title = (angular.isDefined(value.tooltip) && value.tooltip) ? value.dataField + '|' + value.filter : '';
                                    }
                                    else {
                                        var title = (angular.isDefined(value.tooltip) && value.tooltip) ? value.dataField : '';
                                    }
                                    td += '<img tooltip="{{ data.' + title + ' }}" ng-src="{{ ' + value.dataField + '[data.' + value.dataField + ']  }}">';

                                    if (angular.isDefined(value.moreImgField) && angular.isDefined(value.moreImg)) {
                                        scope[value.moreImgField] = {}
                                        angular.copy(value.moreImg, scope[value.moreImgField]);
                                        var title = (angular.isDefined(value.moreImgToolTip) && value.moreImgToolTip) ? value.moreImgToolTip : '';
//                                        td += ' &nbsp;&nbsp;<img ng-show="data.' + value.moreImgField + '" tooltip="{{ ' + value.moreImgField + '[data.' + value.dataField + '] ? ' + title + ' : \'\' }}" ng-src="{{ ' + value.moreImgField + '[data.' + value.dataField + ']  }}"';
                                        td += ' &nbsp;&nbsp;<img ng-show="data.' + value.moreImgField + '" tooltip="{{ (data.' + value.dataField + ' == \'Paused\' || data.' + value.dataField + ' == \'ManuallyPaused\') ? \'Resume\' : \'Pause\' }}" ng-src="{{ ' + value.moreImgField + '[data.' + value.dataField + ']  }}"';
                                        if (angular.isDefined(value.moreImgField)) {
                                            td += ' style="cursor: pointer;" ng-click = "options.' + value.moreImgAction + '(data, $index)" ';
                                        }
                                        td += ' >';
                                    }
//                                } else if (angular.isDefined(value.checkbox)) {
//                                    td += '<input type="checkbox"  ng-model="data.' + value.dataField + '">';
//                                
//                                }
                                } else if (angular.isDefined(value.checkbox) && value.checkbox && angular.isDefined(value.showHideFn)) {
                                    //td += '<input type="checkbox" ng-model="data.' + value.dataField + '"
                                    // ng-change="options.Change(data)" ng-show="options.' + value.showHideFn + '(data, $index)">';
                                    td += '<label class="input-control checkbox small-check no-margin"><input type="checkbox" ng-model="data.' + value.dataField + '" ng-change="options.Change(data)" ng-show="options.' + value.showHideFn + '(data, $index)"><span class="check"></span></label>';
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                    td += '</td>'
                                }
                                else if (angular.isDefined(value.checkbox) && value.checkbox) {
                                    td += '<input class="input-control checkbox" type="checkbox" ng-model="data.' + value.dataField + '" ng-change="options.Change(data)">';
                                    // td += '<label class="input-control checkbox small-check no-margin"><input type="checkbox" ng-model="data.' + value.dataField + '" ng-change="options.Change(data)"><span class="check"></span></label>'
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                    td += '</td>'
                                } else if (angular.isDefined(value.radioChk) && value.radioChk && angular.isDefined(value.showHideFn)) {
                                    td += '<input class="radio" type="radio" name="select" ng-model="data.' + value.dataField + '" ng-click="options.Change(data, $index)" ng-value="data" ng-hide="options.' + value.showHideFn + '(data, $index)">';
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                    td += '</td>'
                                }
                                else if (angular.isDefined(value.radioChk) && value.radioChk) {
                                    td += '<input class="radio" type="radio" name="select" ng-model="data.' + value.dataField + '" ng-change="options.Change(data, $index)" ng-value="data">';
//                                    td += '{{data.' + value.dataField;
//                                    td += '}}';
                                    td += '</td>'
                                }
                                else {
                                    td += '{{data.' + value.dataField;
                                    if (angular.isDefined(value.filter)) {
                                        td += '|' + value.filter;
                                    }
                                    td += '}}';
                                    if (angular.isDefined(value.isHtml) && value.isHtml) {
                                        td += '\'">';
                                    }
                                }
                                td += '</td>';
                                tdArray.push(td);
                            }
                            if (angular.isDefined(value.actions)) {
                                var td = '';
                                td += '<td class="' + visibleCol + ' actionsTd" >';
                                td += '<div class="btn-group">';
                                angular.forEach(value.actions, function (action, actionIndex) {
                                    var visibleBtn = '';
                                    if (angular.isDefined(action.visibleBtn)) {
                                        visibleBtn = action.visibleBtn;
                                    }
                                    var btnclass = "btn-default ";
                                    if (angular.isDefined(action.btnClass)) {
                                        btnclass = " btn-primary "
                                    }
                                    btnclass = btnclass + visibleBtn;
                                    if (angular.isDefined(action.preicon) || angular.isDefined(action.posticon)) {

                                        if (angular.isDefined(action.showHideFn) && angular.isDefined(action.disabledFn)) {
                                            td += '<button type="button" class="btn btn-sm ' + btnclass + '" ng-show="options.' + action.showHideFn + '(data, $index, \'' + action.action + '\')" ng-disabled="options.' + action.disabledFn + '(data, $index, \'' + action.action + '\')" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else if (angular.isDefined(action.showHideFn)) {
                                            td += '<button type="button" class="btn btn-sm ' + btnclass + '" ng-show="options.' + action.showHideFn + '(data, $index, \'' + action.action + '\')" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else if (angular.isDefined(action.disabledFn)) {
                                            td += '<button type="button" class="btn btn-sm ' + btnclass + '" ng-disabled="options.' + action.disabledFn + '(data, $index, \'' + action.action + '\')" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else {
                                            td += '<button type="button" class="btn btn-sm ' + btnclass + '" ng-click="options.' + action.action + '(data, $index)" ';
                                        }

                                    } else {
                                        if (angular.isDefined(action.showHideFn) && angular.isDefined(action.disabledFn)) {
                                            td += '<button type="button" class="btn btn-default" ng-show="options.' + action.showHideFn + '(data, $index)" ng-disabled="options.' + action.disabledFn + '(data, $index)" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else if (angular.isDefined(action.showHideFn)) {
                                            td += '<button type="button" class="btn btn-default" ng-show="options.' + action.showHideFn + '(data, $index)" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else if (angular.isDefined(action.disabledFn)) {
                                            td += '<button type="button" class="btn btn-default" ng-show="options.' + action.showHideFn + '(data, $index)" ng-disabled="options.' + action.disabledFn + '(data, $index)" ng-click="options.' + action.action + '(data, $index)" ';
                                        } else {
                                            td += '<button type="button" class="btn btn-default" ng-click="options.' + action.action + '(data, $index)" ';
                                        }
                                    }
                                    if (angular.isDefined(action.toolTip)) {
                                        td += 'tooltip="' + action.toolTip + '"';
                                    }
                                    td += '>';
                                    if (angular.isDefined(action.preicon)) {
                                        td += '<i class="' + action.preicon + '"></i> '
                                    }

                                    if (angular.isDefined(action.name)) {
                                        td += action.name;
                                    }

                                    if (angular.isDefined(action.posticon)) {
                                        td += ' <i class="' + action.posticon + '"></i>'
                                    }
                                    td += '</button>';
                                });
                                td += '</div>';
                                td += '</td>';
                                tdArray.push(td);
                            }

                            th += '</th>';

                        });
                        thead += th;
                        thead += "</tr>";
                        thSearch += "</tr>";

                        var err_html = '<tr ng-if="options.datamodel.total == 0" ><td colspan="' + tdArray.length + '">';
                        err_html += '<div role="alert"><span aria-hidden="true"></span> No records</div>';
                        err_html += '</td></tr>';
                        tdArray.push(err_html);
                    });

                    tbody = tdArray.join("");

                    var pagination = '';
                    var pageShow = '';
                    var refreshbutton = '';
                    var displayCount = '';
                    var dgSearch = '';
                    if (!angular.isDefined(scope.options.extpaging)) {
                        scope.Math = Math;
                        pageShow += '<div class="dataTables_info" role="status" aria-live="polite">Showing' +
                            ' {{options.datamodel.total>0 ? start+1 : start}} to {{start+options.datamodel.current}} of     {{0+options.datamodel.total}} entries</div>';

                        //pagination += '<div class="col-sm-5"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing {{options.datamodel.total>0 ? start+1 : start}} to {{start+options.datamodel.current}} of {{0+options.datamodel.total}} entries</div></div>';
                        pagination += ' <div class="dataTables_paginate paging_simple_numbers pull-left" id="example_paginate">';
                        pagination += '<a id="example_previous" class="paginate_button previous {{limit > options.datamodel.total || start == 0 ? \'disabled\':\'\'}}" ng-disabled="limit > options.datamodel.total || start == 0" tabindex="0" data-dt-idx="0" aria-controls="example" ng-click="firstPage()"><i class="fa fa-angle-double-left"></i> </a>';
                        pagination += '<a id="example_previous" class="paginate_button previous {{limit > options.datamodel.total || start == 0 ? \'disabled\':\'\'}}"  tabindex="0" data-dt-idx="0" aria-controls="example" ng-click="prevPage()"><!--<i class="fa fa-angle-left"></i>--> Previous</a>';
                        pagination += '<a class="paginate_button" ng-repeat="pageNumber in pages = getPageArray(options.datamodel.total, (options.datamodel.current+start)) track by $index" ng-class="{ active : (Math.ceil((start+options.datamodel.current)/limit)) == pageNumber, disabled : pageNumber == \'...\' }" href="" ng-click="currPage(pageNumber)">{{ pageNumber }} </a>';
                        pagination += '<a id="example_next" class="paginate_button next {{options.datamodel.current+start >= options.datamodel.total ? \'disabled\':\'\'}}" tabindex="0" data-dt-idx="7"  aria-controls="example" ng-click="nextPage()"> Next <!--<i class="fa fa-angle-right"></i>--></a>';
                        pagination += '<a id="example_next" class="paginate_button next {{options.datamodel.current+start >= options.datamodel.total ? \'disabled\':\'\'}}" tabindex="0" data-dt-idx="7" aria-controls="example" ng-click="lastPage()"><i class="fa fa-angle-double-right"></i></a>';
                        pagination += '</div>';
                        /*pagination += '<div class="dataTables_length pull-right" id="example_length">';
                         pagination += '<select class="form-control input-sm" ng-options="val as val for val in displayCount" ng-change="changePageLimit(limit)" ng-model="limit"></select> &nbsp;';
                         pagination += '</div></div>';*/
                        displayCount += '<div class="dataTables_length" id="example_length">';
                        displayCount += '<label>Show <select ng-options="val as val for val in displayCount" ng-change="changePageLimit(limit)" ng-model="limit"></select> entries</label>';
                        //displayCount += '&nbsp;<button ng-click="doQuery()" class="square-button mini-button" tooltip="Refresh" tooltip-trigger="mouseenter"><span class="mif-loop2"></span></button>';
                        displayCount += '</div>';
                        //dgSearch += ' <div id="example_length" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="example_length"></label></div>';
                        refreshbutton += '<div id="example_length" class="dataTables_filter">&nbsp;<button ng-click="doQuery()" class="square-button mini-button" tooltip="Refresh" tooltip-trigger="mouseenter"><span class="mif-loop2"></span></button></div>';

                    }
                    var html = '';
                    //html += '<div class="row"><div class="col-sm-12 col-md-12 col-lg-12"><div class="table-responsive">';
                    html += displayCount;
                    // html += dgSearch;
                    html += refreshbutton;
                    html += '<table class="dataTable border bordered no-footer" style="table-layout: fixed">';
                    html += '    <thead>';
                    html += thead;
                    html += thSearch;
                    html += '    </thead>';
                    html += '    <tbody>';
                    html += '      <tr ng-click="onRowSelect(data)" ng-repeat="data in options.datamodel.data" ng-class-even="\'cwRowAlt\'" style="' + rowStyle + '">';
                    html += tbody;
                    html += '      </tr>';
                    html += '    </tbody>';
                    html += '  </table>';
                    //html += '</div></div></div>';
                    html += pageShow;

                    html += pagination;
                    html += '<div class="row">';

                    html += '</div>';
                    element.html(html);
                    //console.log(html);
                    $compile(element.contents())(scope);
                }

                scope.getPageArray = function (totalRec, currentRec) {
                    var pages = [];
                    var totalPages = Math.ceil(totalRec / scope.limit);
                    var halfWay = Math.ceil(scope.paginationRange / 2);
                    var position;
                    var currentPage = (Math.ceil(currentRec / scope.limit))

                    if (currentPage <= halfWay) {
                        position = 'start';
                    } else if (totalPages - halfWay < currentPage) {
                        position = 'end';
                    } else {
                        position = 'middle';
                    }
                    var ellipsesNeeded = scope.paginationRange < totalPages;
                    var i = 1;
                    while (i <= totalPages && i <= scope.paginationRange) {
                        var pageNumber = scope.calculatePageNumber(i, currentPage, scope.paginationRange, totalPages);
                        var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
                        var closingEllipsesNeeded = (i === scope.paginationRange - 1 && (position === 'middle' || position === 'start'));
                        if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                            pages.push('...');
                        } else {
                            pages.push(pageNumber);
                        }
                        i++;
                    }
                    return pages;
                };

                scope.calculatePageNumber = function (i, currentPage, paginationRange, totalPages) {
                    var halfWay = Math.ceil(paginationRange / 2);
                    if (i === paginationRange) {
                        return totalPages;
                    } else if (i === 1) {
                        return i;
                    } else if (paginationRange < totalPages) {
                        if (totalPages - halfWay < currentPage) {
                            return totalPages - paginationRange + i;
                        } else if (halfWay < currentPage) {
                            return currentPage - halfWay + i;
                        } else {
                            return i;
                        }
                    } else {
                        return i;
                    }
                }

                //date picker begin
//                scope.convertintoEpoch = function(){
//                    doQuery();
//                }
                //date picker end

                scope.doQuery = function () {

                    var queryParams = {};
                    if (angular.isDefined(scope.options.isLoading)) {
                        $rootScope.openModalPopupOpen();
                        //$rootScope.loading = true;
                    }

                    if (angular.isDefined(scope.options.isTableSorting)) {
                        var c = {}
                        c[scope.options.isTableSorting] = 1;
                        if (angular.isDefined(scope.options.perColConditions)) {
                            queryParams = {
                                skip: scope.start,
                                limit: scope.limit,
                                perColConditions: scope.options.perColConditions,
                                orderBy: [c]
                            };
                        } else {
                            queryParams = {
                                skip: scope.start,
                                limit: scope.limit,
                                orderBy: [c]
                            };
                        }
                    } else if (angular.isDefined(scope.options.perColConditions)) {
                        queryParams = {
                            skip: scope.start,
                            limit: scope.limit,
                            perColConditions: scope.options.perColConditions
                        };
                    } else {
                        queryParams = {
                            skip: scope.start,
                            limit: scope.limit
                        };
                    }

                    //Figure out per column search or global search if any
                    // * High priority to per column search
                    // * Low for global search
                    var globalSearchQuery = {};
                    var perColSearchQuery = {};
                    var hasPerColSearch = false;
                    if (scope.globalsearch.length > 0) {
                        globalSearchQuery[scope.globalsearch] = [];
                    }

                    if (angular.isDefined(scope.options.searchFields)) {
                        if (angular.isDefined(scope.options.searchFields.snapshotTime) && isNaN(scope.options.searchFields.snapshotTime)) {
                            //scope.searchFields = $filter('dateToEpoch')(scope.options.searchFields);
//                            scope.searchFields={"snapshotTime":1440075600};
                            scope.options.searchFields.snapshotTime = $filter('dateToEpoch')(scope.options.searchFields);
                            scope.searchFields.snapshotTime = scope.options.searchFields.snapshotTime.snapshotTime;
                        }
                        else {
                            scope.searchFields = scope.options.searchFields;
                        }
                    }
                    angular.forEach(scope.searchFields, function (val, key) {
                        val = val.replace(/\*/g, '%');
                        if (val.length > 0) {
                            hasPerColSearch = true;
                            perColSearchQuery[key] = val;
                        } else {
                            if (angular.isDefined(queryParams.perColConditions)) {
                                delete queryParams.perColConditions[key];
                            }
                        }
//                        if(key=="snapshotTime")
//                        {
//                            hasPerColSearch = true;
//                            perColSearchQuery[key] = parseInt(val);
//                        }
                        if (scope.globalsearch.length > 0) {
                            globalSearchQuery[scope.globalsearch].push(key);
                        }
                    });

                    //High priority to per column search
                    //Low for global search
                    if (hasPerColSearch) {
                        if (angular.isDefined(scope.options.perColConditions)) {
                            for (var attrname in perColSearchQuery) {
                                queryParams.perColConditions[attrname] = perColSearchQuery[attrname];
                            }
                            //queryParams.perColConditions = scope.options.perColConditions.merge(perColSearchQuery);
                        } else {
                            queryParams.perColConditions = perColSearchQuery;
                        }
                    } else if (scope.globalsearch.length > 0) {
                        queryParams.globalConditions = globalSearchQuery;
                    }

                    if (scope.sort.length > 0) {
                        queryParams.orderBy = [];
                        k = {};
                        k[scope.sort] = scope.sortDirection;
                        queryParams.orderBy.push(k);
                    }
                    if (scope.sortName.length > 0) {
                        queryParams.order = '';
                        k = '';
                        k = scope.sortNameDirection;
                        if (k == 1) {
                            k = true;
                        } else {
                            k = false;
                        }
                        queryParams.order = k;
                        queryParams.queryField = scope.sortName;
                    }

                    scope.drowUi();
                    if (angular.isDefined(scope.options.query) && angular.isFunction(scope.options.query)) {
                        scope.options.query(queryParams);
                        $timeout(function () {
                            if (angular.isDefined(scope.options.isLoading)) {
                                $rootScope.openModalPopupClose();
                                //$rootScope.loading = false;
                            }
                        }, 500);
                    } else if (angular.isDefined(scope.options.service)) {
                        if (angular.isDefined(scope.options.dataClear) && angular.isDefined(scope.options.datamodel)) {
                            scope.options.dataClear(scope.options.datamodel);
                        }
                        //Start Loading image
                        $rootScope.openModalPopupOpen();
                        //$rootScope.loading = true;
                        if (angular.isDefined(scope.options.dynamicService) && scope.options.dynamicService) {
                            scope.options.service.getConfiguredMembers("dummy",
                                queryParams,
                                function (data) {
                                    if (angular.isDefined($rootScope.queues)) {
                                        scope.queues = $rootScope.queues;
                                    }
                                    if (angular.isDefined($rootScope.queuesTest)) {
                                        scope.queuesTest = $rootScope.queuesTest;
                                    }

                                    if (angular.isDefined($rootScope.getRoles)) {
                                        scope.getRoles = $rootScope.getRoles;
                                    }
                                    if (angular.isDefined($rootScope.chatcount)) {
                                        scope.chatcount = $rootScope.chatcount;
                                    }
                                    $rootScope.openModalPopupClose();
                                    //$rootScope.loading = false;
                                    scope.options.datamodel = data;
                                    if (angular.isDefined(scope.options.isLoading)) {
                                        $rootScope.openModalPopupClose();
                                        //$rootScope.loading = false;
                                    }
                                    if (angular.isDefined(scope.options.dataInit)) {
                                        scope.options.dataInit(data);
                                    }
                                    if (angular.isDefined(scope.options.datamodel) && angular.isDefined(scope.options.datamodel.total)) {
                                        scope.options.extPagingShow = true;
                                        if (scope.options.datamodel.total == 0) {
                                            scope.options.pagingText = (scope.start + 0) + " to " + (scope.start + scope.options.datamodel.current) + " of " + scope.options.datamodel.total;
                                            if ((scope.start + scope.options.datamodel.current) >= 10) {
                                                scope.options.isFwdArrowEnable = true;
                                            } else {
                                                scope.options.isFwdArrowEnable = false;
                                            }
                                            scope.options.OperpagingText = (scope.start + 0) + " to " + (scope.start + scope.options.datamodel.current);
                                        } else {
                                            scope.options.pagingText = (scope.start + 1) + " to " + (scope.start + scope.options.datamodel.current) + " of " + scope.options.datamodel.total;
                                            if (scope.start != 0) {
                                                scope.options.isBackArrowEnable = true;
                                            } else {
                                                scope.options.isBackArrowEnable = false;
                                            }
                                            if ((scope.start + scope.options.datamodel.current) >= 10) {
                                                scope.options.isFwdArrowEnable = true;
                                            } else {
                                                scope.options.isFwdArrowEnable = false;
                                            }
                                            scope.options.OperpagingText = (scope.start + 1) + " to " + (scope.start + scope.options.datamodel.current);
                                        }
                                    }
                                });

                        } else {
                            scope.options.service.get(undefined, {query: queryParams}, function (data) {
                                if (angular.isDefined($rootScope.queues)) {
                                    scope.queues = $rootScope.queues;
                                }
                                if (angular.isDefined($rootScope.queuesTest)) {
                                    scope.queuesTest = $rootScope.queuesTest;
                                }

                                if (angular.isDefined($rootScope.getRoles)) {
                                    scope.getRoles = $rootScope.getRoles;
                                }
                                if (angular.isDefined($rootScope.chatcount)) {
                                    scope.chatcount = $rootScope.chatcount;
                                }
                                scope.options.datamodel = data;
                                if (angular.isDefined(scope.options.dataInit)) {
                                    scope.options.dataInit(data);
                                }
                                if (angular.isDefined(scope.options.datamodel) && angular.isDefined(scope.options.datamodel.total)) {
                                    scope.options.extPagingShow = true;
                                    if (scope.options.datamodel.total == 0) {
                                        scope.options.pagingText = (scope.start + 0) + " to " + (scope.start + scope.options.datamodel.current) + " of " + scope.options.datamodel.total;
                                        if ((scope.start + scope.options.datamodel.current) >= 10) {
                                            scope.options.isFwdArrowEnable = true;
                                        } else {
                                            scope.options.isFwdArrowEnable = false;
                                        }
                                        scope.options.OperpagingText = (scope.start + 0) + " to " + (scope.start + scope.options.datamodel.current);
                                    } else {
                                        scope.options.pagingText = (scope.start + 1) + " to " + (scope.start + scope.options.datamodel.current) + " of " + scope.options.datamodel.total;
                                        if (scope.start != 0) {
                                            scope.options.isBackArrowEnable = true;
                                        } else {
                                            scope.options.isBackArrowEnable = false;
                                        }
                                        if ((scope.start + scope.options.datamodel.current) >= 10) {
                                            scope.options.isFwdArrowEnable = true;
                                        } else {
                                            scope.options.isFwdArrowEnable = false;
                                        }
                                        scope.options.OperpagingText = (scope.start + 1) + " to " + (scope.start + scope.options.datamodel.current);
                                    }
                                }
                                $rootScope.openModalPopupClose();
                                //$rootScope.loading = false;
                                if (angular.isDefined(scope.options.isLoading)) {
                                    $rootScope.openModalPopupClose();
                                    //$rootScope.loading = false;
                                }

                            });
                        }
                    } else {
                        // alert("No query func");
                    }
                }

                scope.doSearch = function (event) {
                    //if(scope.queryoptions.search.length>3 || scope.queryoptions.search.length === 0)
                    {
                        scope.start = 0;
                        if (event.keyCode === 13) {
                            scope.doQuery();
                        }
                    }
                }

                scope.pageSizeChange = function (value) {
                    scope.start = 0;
                    scope.doQuery();
                }

                scope.changePageLimit = function (value) {
                    scope.start = 0;
                    scope.limit = value;
                }

                scope.pageLimitChange = function (value) {
                    scope.start = 0;
                    scope.limit = value;
                    scope.doQuery();
                }

                scope.sortChange = function (name) {
                    if (name === scope.sort) {
                        scope.sortDirection *= -1;
                    } else {
                        scope.sortDirection = 1;
                    }
                    scope.sort = name;

                    scope.doQuery();
                }

                scope.sortSchedulerList = function (name) {
                    if (name === scope.sortName) {
                        scope.sortNameDirection *= -1;
                    } else {
                        scope.sortNameDirection = 1;
                    }
                    scope.sortName = name;
                    scope.doQuery();
                };

                scope.sortManualChange = function (name) {
                    scope.manulasort = !scope.manulasort;
                    var orderByField = name;
                    if (!angular.isDefined(name)) {
                        orderByField = '-presence';
                    }
                    scope.options.datamodel.data = $filter('orderBy')(scope.options.datamodel.data, orderByField, scope.manulasort);
                };

                scope.currPage = function (pageNumber) {
                    var numberRegex = /^\d+$/;
                    var currentRec = (scope.options.datamodel.current + scope.start)
                    var currentPage = (Math.ceil(currentRec / scope.limit))
                    if (numberRegex.test(pageNumber) && currentPage != pageNumber) {
                        scope.options.SelectedId = [];
                        if (angular.isDefined(scope.options.Change)) {
                            scope.options.Change({'data': {'SelectedOperator': false}});
                        }

                        pageNumber = parseInt(pageNumber) - 1;
                        scope.start = (pageNumber * scope.limit);
                        scope.doQuery();
                    }
                };

                scope.nextPage = function () {
                    if (scope.start + scope.options.datamodel.current < scope.options.datamodel.total) {
                        scope.start += scope.limit;
                        scope.doQuery();
                    }
                }

                scope.prevPage = function () {
                    if (scope.start >= scope.limit) {
                        scope.start -= scope.limit;
                        scope.doQuery();
                    }
                }

                scope.firstPage = function () {
                    if (scope.start !== 0) {
                        scope.start = 0;
                        scope.doQuery();
                    }
                }

                scope.lastPage = function () {
                    var lastPageStart = 0
                    if (scope.options.datamodel.total % scope.limit === 0) {
                        lastPageStart = ((scope.options.datamodel.total / scope.limit) - 1) * scope.limit;
                    } else {
                        lastPageStart = scope.limit * Math.floor(scope.options.datamodel.total / scope.limit);
                    }

                    if (scope.start !== lastPageStart) {
                        scope.start = lastPageStart;
                        scope.doQuery();
                    }

                }
                if (angular.isDefined(scope.options.init)) {
                    if (scope.options.init == true) {
                        scope.doQuery();
                    }
                } else {
//                    scope.doQuery();
                }

                scope.$watch('options.reQuery', function (newValue) {
                    if (newValue == true) {
                        scope.doQuery();
                        scope.options.reQuery = false;
                    }
                });

                scope.$watch('limit', function (value) {
                    scope.pageLimitChange(value);
                }, true);
                scope.onRowSelect = function (row) {
                    if (angular.isDefined(scope.options.onRowSelect) && angular.isFunction(scope.options.onRowSelect)) {
                        scope.options.onRowSelect(row);
                    }

                }
                if (angular.isDefined(scope.options.displayCount)) {
                    scope.displayCount = scope.options.displayCount
                }
                else {
                    scope.displayCount = [10, 25, 50];
                }
                //Build table header (row above table)
                var hasAddOption = false;
                if (angular.isDefined(scope.options.addAction) && angular.isFunction(scope.options.addAction)) {
                    hasAddOption = true;
                }
                scope.drowUi();
                //scope.doQuery();
                //scope.ptrefresh();
            }
        };
    }]);
app.directive('datagridOriginal', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            options: "="
        },
        transclude: true,
        replace: true,
        link: function (scope, element, attrs) {
            scope.search = "";
            scope.start = 0;
            scope.limit = 10;
            scope.sort = "";
            scope.sortDirection = -1;
            scope.options.reQuery = false;
            scope.searchFields = []
            scope.searchOn = []
            scope.imgMaps = {}; //key -> data field, val => object containing the imgMap pass from controller

            scope.doQuery = function () {
                $rootScope.loading = true;
                var queryParams = {
                    start: scope.start,
                    limit: scope.limit
                };

                if (angular.isDefined(scope.options.predefinedSearch)) {
                    queryParams.query = scope.options.predefinedSearch;
                }
                else if (scope.search.length > 0) {
                    if (scope.searchOn.length === 0) {
                        scope.searchOn = jQuery.map(scope.searchFields, function (val, index) {
                            return val.value;
                        });
                    }
                    queryParams.search = {fields: scope.searchOn, search: scope.search};
                }
                if (scope.sort.length > 0) {
                    queryParams.sort = {};
                    queryParams.sort[scope.sort] = scope.sortDirection;
                }
                if (angular.isDefined(scope.options.query) && angular.isFunction(scope.options.query)) {
                    scope.options.query(queryParams);
                    $rootScope.loading = false;
                }
                else if (angular.isDefined(scope.options.service)) {
                    scope.options.service.get(undefined, queryParams, function (data) {
                        scope.options.datamodel = data;
                        $rootScope.loading = false;
                    });
                }
                else {
                    $rootScope.loading = false;
                    alert("No query func");
                }
            }

            scope.doSearch = function () {
                //if(scope.queryoptions.search.length>3 || scope.queryoptions.search.length === 0)
                {
                    scope.start = 0;
                    scope.doQuery();
                }
            }

            scope.pageSizeChange = function () {
                scope.start = 0;
                scope.doQuery();
            }

            scope.sortChange = function (name) {
                if (name === scope.sort) {
                    scope.sortDirection *= -1;
                }
                else {
                    scope.sortDirection = 1;
                }
                scope.sort = name;

                scope.doQuery();
            }

            scope.nextPage = function () {
                if (scope.start + scope.options.datamodel.current < scope.options.datamodel.total) {
                    scope.start += scope.limit;
                    scope.doQuery();
                }
            }

            scope.prevPage = function () {
                if (scope.start >= scope.limit) {
                    scope.start -= scope.limit;
                    scope.doQuery();
                }
            }

            scope.firstPage = function () {
                if (scope.start !== 0) {
                    scope.start = 0;
                    scope.doQuery();
                }
            }

            scope.lastPage = function () {
                var lastPageStart = 0
                if (scope.options.datamodel.total % scope.limit === 0) {
                    lastPageStart = ((scope.options.datamodel.total / scope.limit) - 1) * scope.limit;
                }
                else {
                    lastPageStart = scope.limit * Math.floor(scope.options.datamodel.total / scope.limit);
                }

                if (scope.start !== lastPageStart) {
                    scope.start = lastPageStart;
                    scope.doQuery();
                }

            }

            scope.doQuery();

            scope.$watch('options.reQuery', function (newValue) {
                if (newValue == true) {
                    scope.doQuery();
                    scope.options.reQuery = false;
                }
            });

            scope.$watch('limit', function (value) {
                scope.pageSizeChange();
            });

            scope.displayCount = [
                {value: 10, key: "10"},
                {value: 25, key: "25"},
                {value: 50, key: "50"},
                {value: 100, key: "100"}
            ];
            //Build table header (row above table)
            var hasAddOption = false;
            if (angular.isDefined(scope.options.addAction) && angular.isFunction(scope.options.addAction)) {
                hasAddOption = true;
            }
            var header = '';
            header += '<div class="col-md-2">';
            //header += '  Show ';
            header += '  <myselect options="displayCount" class="input-mini" model="limit"></myselect>';
            header += '</div>';

            //if(hasAddOption)
            //{
            //    header += '<div class="col-md-3 col-md-offset-6">';
            //}
            //else
            //{
            header += '<div class="col-md-3 col-md-offset-7">';
            //}
            header += '  <div class="input-group">';
            header += '    <input type="text" placeholder="Search" ng-change="doSearch()" ng-model="search" class="form-control"/>';
            header += '    <span class="input-group-btn">';
            header += '      <button class="btn btn-default" type="button"><i class="icon-search"></i></button>';
            header += '    </span>';
            header += '  </div>';
            //header += '      <myselect multiple model="searchOn" options="searchFields"></myselect>';
            header += '</div>';

            //Build the Header and the Body columns (th, td)
            var th = '';
            var td = '';

            //start
            //var actions = '';
//            angular.forEach(scope.options.headers, function(value, index){
//                if(angular.isDefined(value.actions))
//                {
//                    actions += '<div class=\'btn-group\'>';
//                    angular.forEach(value.actions, function(action, actionIndex){
//                        actions += '<button type=\'button\' class=\'btn btn-sm btn-default\' ng-click=\'options.'+action.action +'(data, $index)\'>';
//                        if(angular.isDefined(action.preicon))
//                        {
//                            actions += '<i class=\''+action.preicon+'\'></i> '
//                        }
//
//                        actions += action.name;
//
//                        if(angular.isDefined(action.posticon))
//                        {
//                            actions += ' <i class=\''+action.posticon+'\'></i>'
//                        }
//                        actions += '</button>';
//                    });
//                    actions +='</div>';
//                }
//            });


            angular.forEach(scope.options.headers, function (value, index) {
                if (value.searchable) {
                    scope.searchFields.push({'key': value.name, 'value': value.dataField})
                }
                if (angular.isDefined(value.visible) && value.visible == false) {
                    //May be the field is defined only for search, not user visible!!!
                    return;
                }
                th += '<th ';
                td += '<td class="text-center">';
                //Sortable header and body
                if (angular.isDefined(value.dataField)) {
                    th += 'ng-class="{sortable: ' + value.sortable + ', sorted: \'' + value.dataField + '\' === sort}" ng-click="sortChange(\'' + value.dataField + '\')">' +
                        value.name +
                        '          <i ng-show="\'' + value.dataField + '\'== sort && sortDirection == 1" class="icon-chevron-up"></i>' +
                        '          <i ng-show="\'' + value.dataField + '\'== sort && sortDirection == -1" class="icon-chevron-down"></i>';

                    td += '{{data.' + value.dataField;
                    if (angular.isDefined(value.filter)) {
                        td += '|' + value.filter;
                    }
                    td += '}}';

                }
                if (angular.isDefined(value.img)) {
                    td += '<div class="btn-group">';
                    if (!angular.isDefined(value.img)) {
                        td += '{{data.' + value.dataField;
                        if (angular.isDefined(value.filter)) {
                            td += '|' + value.filter;
                        }
                        td += '}}';
                    } else {
                        scope[value.dataField] = {}
                        angular.copy(value.img, scope[value.dataField]);
                        td += '<img ng-src="{{ ' + value.dataField + '[data.' + value.dataField + '] }}">';
                    }
                    td += '</div>';

                }
                if (angular.isDefined(value.actions)) {
                    th += ">" + value.name;

                    td += '<div class="btn-group">';
                    angular.forEach(value.actions, function (action, actionIndex) {
                        td += '<button type="button" class="btn btn-sm btn-default" ng-click="options.' + action.action + '(data, $index)">';
                        if (angular.isDefined(action.preicon)) {
                            td += '<i class="' + action.preicon + '"></i> '
                        }

                        td += action.name;

                        if (angular.isDefined(action.posticon)) {
                            td += ' <i class="' + action.posticon + '"></i>'
                        }
                        td += '</button>';

                    });
                    td += '</div>';


                }
                if (angular.isDefined(value.selectoptions)) {
                    th += ">" + value.name;
                    td += '<div class="btn-group">';
                    angular.forEach(value.selectoptions, function (action, actionIndex) {
                        td += '<myselect model="data.values" name="values" options="values"></myselect>';

                    });
                    td += '</div>';
                }
                th += '</th>';
                td += '</td>';
            });

            //Build  table footer (row below table)
            var footer = '';
            footer += '  <div class="col-md-3">';
            footer += '    <button type="button" class="btn btn-default btn-xs btn-prev" ng-click="firstPage()" tooltip="First"  tooltip-trigger="mouseenter"><i class="icon-step-backward"></i></button>';
            footer += '    <button type="button" class="btn btn-default btn-xs btn-prev" ng-click="prevPage()" tooltip="Prev"  tooltip-trigger="mouseenter"><i class="icon-chevron-left"></i></button>';
            footer += '    <button type="button" class="btn btn-default btn-xs btn-next" ng-click="nextPage()" tooltip="Next"  tooltip-trigger="mouseenter"><i class="icon-chevron-right"></i></button>';
            footer += '    <button type="button" class="btn btn-default btn-xs btn-next" ng-click="lastPage()" tooltip="Last"  tooltip-trigger="mouseenter"><i class=" icon-step-forward"></i></button>';
            footer += '  </div>';
            footer += '  <b class="col-md-2 col-md-offset-7">';
            footer += '    <ng-pluralize count="options.datamodel.total" when="{\'0\': \'No Records.\', ';
            footer += '                                                         \'1\': \'1 Record.\', ';
            footer += '                                                         \'other\': \'{{start+1}} - {{start+options.datamodel.current}} of {{options.datamodel.total}}\'}"></b>';
            footer += '  <br>';

            var html;
            html = '<div>';
            html += '  <div class="row">';
            html += header;
            html += '  </div>';
            html += '  <div class="row">';
            html += '    <div class="col-md-12">';
            html += '      <table class="table table-bordered table-condensed table-hover datagrid datagrid-stretch-header">';
            html += '        <thead>';
            html += th;
            html += '        </thead>';
            html += '        <tbody>';
            html += '          <tr ng-repeat="data in options.datamodel.data" ng-class="options.highlightIndex === $index ? \'warning\': \'\'">';
            html += td;
            html += '          </tr>';
            html += '        </tbody>';
            html += '      </table>';
            html += '    </div>';
            html += '  </div>';
            html += '  <div class="row">';
            html += footer;
            html += '  </div>'
            html += '</div>';
            if (hasAddOption) {
                html += '<br/><button ng-click="options.addAction()" type="button" class="btn btn-default" tooltip="Add" tooltip-trigger="mouseenter"><i class="icon-plus-sign"></i>&nbsp;Add</button>';
            }
            element.html(html);
            $compile(element.contents())(scope);
        }
    };
}]);
app.directive('myselect', ['$document', '$location', function ($document, $location) {
    var openElement = null,
        closeMenu = angular.noop;
    return {
        restrict: 'E',
        scope: {
            options: "=", //Has the contents to be displayed in the dropdown, has to be array of objects
            model: "=", //The selected value is stored in this model
            key: "@", //The key field in the options used in the dropdown (shown to the user), defaults to "key"
            value: "@" //The value to be stored in the model, defaults to "value"
        },
        transclude: true,
        replace: true,
        link: function (scope, element, attrs) {
            scope.dataKey = "key";
            scope.dataValue = "value";
            scope.modelKey = "Select";
            scope.isMultiple = false;
            scope.isStringValues = false;

            if (angular.isDefined(scope.options) && scope.options.length > 0 && typeof scope.options[0] === 'string') {
                scope.isStringValues = true;
            }
            if ("multiple" in attrs) {
                scope.isMultiple = true;
            }
            if (angular.isDefined(scope.key)) {
                scope.dataKey = scope.key;
            }
            if (angular.isDefined(scope.value)) {
                scope.dataValue = scope.value;
            }

            if (scope.isMultiple) {
                if (!angular.isArray(scope.modelKey)) {
                    scope.modelKey = [];
                    scope.modelKey.push("Select");
                }
                if (!angular.isArray(scope.model)) {
                    scope.model = [];
                }
            }

            scope.$watch('$location.path', function () {
                closeMenu();
            });

            scope.setValue = function (key, value) {
                if (scope.isMultiple) {
                    //If the value already exist, remove it, else add it
                    var index = scope.model.indexOf(value);
                    if (index >= 0) {
                        scope.model.splice(index, 1);
                    }
                    else {
                        scope.model.push(value);
                    }
                }
                else {
                    scope.modelKey = key;
                    scope.model = value;
                }
            }

            scope.isActive = function (key, value) {
                if (scope.isMultiple) {
                    var found = false;
                    angular.forEach(scope.model, function (modelValue, index) {
                        if (angular.equals(value, modelValue)) {
                            found = true;
                        }
                    });
                    var index = scope.modelKey.indexOf(key);
                    if (found) {
                        if (index == -1) {
                            scope.modelKey.push(key);
                        }
                    }
                    else {
                        if (index >= 0) {
                            scope.modelKey.splice(index, 1);
                        }
                    }

                    if (scope.modelKey.length === 0) {
                        scope.modelKey.push("Select");
                    }
                    else {
                        var index = scope.modelKey.indexOf("Select");
                        if (index >= 0 && scope.modelKey.length > 1) {
                            scope.modelKey.splice(index, 1);
                        }
                    }
                    return found;
                }
                else {
                    if (angular.equals(value, scope.model)) {
                        scope.modelKey = key;
                        return true;
                    }
                }
                return false;
            }

            element.bind('click', function (event) {

                var elementWasOpen = (element === openElement);

                event.preventDefault();
                event.stopPropagation();

                if (!!openElement) {
                    if (scope.isMultiple) {
                        if (!angular.element(event.target).is("a") && !angular.element(event.target).is("i")) {
                            closeMenu();
                        }
                    }
                    else {
                        closeMenu();
                    }
                }

                if (!elementWasOpen) {
                    element.addClass('open');
                    openElement = element;
                    closeMenu = function (event) {
                        if (event) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        $document.unbind('click', closeMenu);
                        element.removeClass('open');
                        closeMenu = angular.noop;
                        openElement = null;
                    };
                    $document.bind('click', closeMenu);
                }
            });
        },
        template: '<div class="dropdown">' +
        '    <div class="btn-group">' +
        '        <button type="button" class="btn btn-default">' +
        '            <span ng-hide="isMultiple">{{ modelKey }}</span>' +
        '            <span ng-show="isMultiple">' +
        '                <ng-pluralize count="modelKey.length" when="{\'0\':\'{{ modelKey | arraytocsv:10 }}\', \'1\':\'{{ modelKey | arraytocsv:10 }}\', \'other\': \'{{ modelKey.length }}\' + \' selected\' }"></ng-pluralize>' +
        '            </span>' +
        '        </button>' +
        '        <button type="button" class="btn btn-default"><span class="caret"></span></button>' +
        '    </div>' +
        '    <ul class="dropdown-menu" style="overflow-y: auto;max-height: 300px">' + //TODO: Remove this harcode!!!!
        '        <li ng-hide="isStringValues" ng-repeat="data in options" ng-class="isActive(data[dataKey], data[dataValue]) ? \'active\': \'\'"><a href="" ng-click="setValue(data[dataKey], data[dataValue])"><i ng-show="isMultiple" ng-class="isActive(data[dataKey], data[dataValue]) ? \'icon-check\' : \'icon-check-empty\'"></i> {{data[dataKey]}}</a></li>' +
        '        <li ng-show="isStringValues" ng-repeat="data in options" ng-class="isActive(data, data) ? \'active\': \'\'"><a href="" ng-click="setValue(data, data)"><i ng-show="isMultiple" ng-class="isActive(data, data) ? \'icon-check\' : \'icon-check-empty\'"></i> {{data}}</a></li>' +
        '    </ul>' +
        '</div>'
    };
}]);
app.directive('confirmationDialog', function () {
    return {
        restrict: 'E',
        scope: {
            visible: "=",
            heading: "@",
            close: "&",
            confirm: "&",
            data: "=",
            autoclose: "@",
            successbutton: "@",
            rejectbutton: "@"
        },
        transclude: true,
        //replace: true,
        link: function (scope, element, attrs) {
            if (!angular.isDefined(scope.autoclose)) {
                scope.autoclose = -1;
            }
            scope.ec2InstanceList = [
                {insval: "t1.micro"},
                {insval: "m1.small"},
                {insval: "m1.medium"},
                {insval: "m1.large"}
            ];
        },
        template: '<modal visible="visible" close="close()" autoclose="autoclose">' +
        '<div class="modal-header" ng-show="heading">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">{{ heading }}</h4>' +
        '</div>' +
        '<div class="modal-body">' +
        '<div ng-transclude></div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal" ng-show="rejectbutton">{{ rejectbutton }}</button>' +
        '<button type="button" class="btn btn-danger" ng-click="confirm()" ng-show="successbutton">{{ successbutton }}</button>' +
        '</div>' +
        '</modal>'
    };
});
app.directive('notification', [function () {
    return {
        restrict: 'E',
        scope: {
            visible: "=",
            close: "&",
            confirm: "&"
        },
        transclude: true,
        //replace: true,
        link: function (scope, element, attrs) {

        },
        template: '<modal visible="visible" close="close()">' +
        '<div class="modal-body">' +
        '<div ng-transclude></div>' +
        '</div>' +
        '</modal>'
    };
}]);
app.directive('compile', function ($compile, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            $timeout(function () {
                $compile(elem.contents())(scope);
            });
        }
    };
});

/////////////////////////////////////////////////////////////////////
app.directive('customPopover', function () {
    return {
        restrict: 'A',
        template: '<span><i class="fa fa-info-circle fa-lg text-info form-control-static">{{label}}</i></span>',
        link: function (scope, el, attrs) {
            scope.label = attrs.popoverLabel;
            $(el).each(function () {
                var $elem = $(this);
                $elem.popover({
                    trigger: 'hover',
                    html: true,
                    content: attrs.popoverHtml,
                    placement: attrs.popoverPlacement,
                    title: attrs.popoverTitle,
                    animation: true,
                    container: $elem
                });
            });
        }
    }
});