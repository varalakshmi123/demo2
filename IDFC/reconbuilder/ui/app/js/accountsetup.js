/**
 * Created by hash on 22/8/16.
 */

function accountsetupController($scope, $rootScope, $http, $upload, $sce, $timeout, $route, $window, $modal, $filter, ReconContextService, FeedDefinationService, AccountMapService, AccountsService, AccountPoolService) {

    $scope.accountMapOptions = {};
    $scope.acountMasterOptions = {};
    $scope.accountPoolMasterOptions = {};
    $scope.accountMapOptions.init = false;
    $scope.acountMasterOptions.init = false;
    $scope.accountPoolMasterOptions.init = false;
    $scope.disableAcctMasterExport = false;
    //$scope.accountMapOptions.isLoading = true;
    //$scope.accountMapOptions.isTableSorting = "userName";
    //$scope.accountMapOptions.sortField = "userName";
    $scope.getAccountMapping = function () {
        $scope.accountMapOptions.searchFields = {}
        //$scope.accountMapOptions.service = AccountMapService;
        $scope.accountMapOptions.query = function (queryParam) {

            AccountMapService.get(undefined, {'query': queryParam}, function (data) {

                if (data['total'] > 0) {
                    angular.forEach(data['data'], function (val, outIndex) {
                        var accountName = []
                        angular.forEach(val['accounts'], function (accountDetails, innerIndex) {
                            accountName.push(accountDetails['accountName'])
                        })
                        val['account_name'] = accountName.join(",")
                    })
                }
                console.log(data)
                $scope.accountMapOptions.datamodel = data
            });
        }
        $scope.accountMapOptions.headers = [
            [
                {name: "Recon Name", searchable: true, sortable: true, dataField: 'reconContext.reconName', colIndex: 0},
                {name: "Feed Name", searchable: true, sortable: true, dataField: "feedDetails", colIndex: 1, filter: "formatFeedDetails"},
                {name: "Accounts", searchable: true, sortable: true, dataField: "account_name", colIndex: 2, isTruncate: true},
                {
                    name: "Actions",
                    colIndex: 4,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addAccountMapping",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteFunc",
                            posticon: "fa fa-trash-o fa-lg"
                        }]
                }
            ]
        ];
    }

    $scope.accountMapOptions.addAccountMapping = function (ac) {
        $scope.accounts = []

        $modal.open({
            templateUrl: 'feedAccountMap.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            controller: function ($scope, $modalInstance, AccountMapService, AccountsService, FeedDefinationService) {

                $scope.ac = {}
                $scope.feedDetailsArray = []
                $scope.accountMaster = []
                $scope.ac = {'accounts': [], 'feedDetails': []}
                var feedCheckArray = []
                var mode = ''

                $scope.getReconContextDetails = function () {
                    ReconContextService.get(undefined, undefined, function (data) {

                        $scope.reconContextDetails = []
                        console.log(data)
                        $scope.disableReconContext = true;
                        if (data.total > 0) {
                            $scope.reconContextDetails = data['data']

                            // in-case of edit mode update the details back
                            if (angular.isDefined(ac)) {
                                $scope.ac = ac

                                for (i = 0; i < $scope.reconContextDetails.length; i++) {
                                    if ($scope.reconContextDetails[i]['reconId'] == ac['reconContext']['reconId']) {
                                        $scope.ac['recon'] = $scope.reconContextDetails[i]
                                        $scope.getFeedStructureDetails($scope.ac['recon'])
                                        break
                                    }
                                }
                            }
                        } else {
                            $rootScope.showAlertMsg("Please configure recons before allocating accounts")
                        }
                    })
                }

                $scope.getFeedStructureDetails = function (reconDetails) {

                    var fieldIdCheckArray = []
                    $scope.feedDetailsArray = []

                    // get all feed Id's for the selected recon details
                    angular.forEach(reconDetails['sources'], function (val, index) {
                        fieldIdCheckArray.push(val['feedId'])
                    })

                    FeedDefinationService.get(undefined, undefined, function (data) {
                        console.log(data)

                        if (data['total'] > 0) {

                            // check for edit mode, in-case of get all the feed Ids pre-selected by the user
                            var selectedFeedDetails = []
                            if (angular.isDefined(ac)) {
                                angular.forEach(ac['feedDetails'], function (value, index) {
                                    selectedFeedDetails.push(value['feedId'])
                                })
                            }

                            angular.forEach(data.data, function (val, index) {

                                // preparing feed ID list
                                var feedDetails = {}
                                if (fieldIdCheckArray.indexOf(val['_id']) != -1) {
                                    feedDetails = {'feedName': val['feedName'], 'feedId': val['_id']}
                                    $scope.feedDetailsArray.push(feedDetails)
                                }

                                // incase of edit mode,pre-populate the drop down list
                                if (angular.isDefined(ac)) {
                                    if (selectedFeedDetails.indexOf(val['_id']) != -1) {
                                        $scope.ac['feedDetails'].push(feedDetails)
                                    }
                                }
                            })
                        }
                    })
                }

                $scope.getAccoutDetails = function () {
                    $scope.accountMaster = []

                    AccountsService.get(undefined, undefined, function (data) {
                        if (data['total'] > 0) {

                            var selectedAccount = []
                            console.log(ac)

                            // check for edit mode
                            if (angular.isDefined(ac)) {
                                angular.forEach(ac['accounts'], function (value, index) {
                                    selectedAccount.push(parseInt(value['accountNumber']))
                                })
                            }

                            $scope.ac['accounts'] = []
                            angular.forEach(data['data'], function (val, index) {
                                var tmp = {'label': val['ACCOUNT_NAME'], 'Default': val}
                                $scope.accountMaster.push(tmp)


                                if (angular.isDefined(selectedAccount) && selectedAccount.length > 0) {
                                    if (selectedAccount.indexOf(val['ACCOUNT_NUMBER']) != -1) {
                                        $scope.ac['accounts'].push(tmp)
                                    }

                                }
                            })
                            console.log($scope.ac['accounts'])
                        }
                    })
                }


                $scope.saveAccountMapping = function (ac) {

                    var dataR = {}
                    dataR['reconContext'] = {'reconId': ac['recon']['reconId'], 'reconName': ac['recon']['reconName']}
                    /*dataR['feedDetails'] = {
                     'feedId': ac['feedDetails']['feedId'],
                     'feedName': ac['feedDetails']['feedName']
                     }*/

                    dataR['feedDetails'] = ac['feedDetails']
                    dataR['accounts'] = []

                    angular.forEach(ac['accounts'], function (a, b) {
                        var tmp = {}
                        if (angular.isDefined(a['Default'])) {
                            tmp['accountNumber'] = a['Default']['ACCOUNT_NUMBER']
                            tmp['accountName'] = a['Default']['ACCOUNT_NAME']
                            dataR['accounts'].push(tmp)
                        }
                    })

                    console.log(dataR)

                    if (angular.isDefined(ac['_id'])) {
                        AccountMapService.put(ac['_id'], dataR, function (data) {
                            console.log(data)
                            $rootScope.showSuccessMsg("Feed Account Details updated successfully")
                            $scope.cancel()
                        })
                    }
                    else {
                        AccountMapService.post(dataR, function (data) {
                            console.log(data)
                            $rootScope.showSuccessMsg("Feed Account Details Saved successfully")
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.accountMapOptions.reQuery = true;
                };

                $scope.getReconContextDetails()
                $scope.getAccoutDetails()
            }
        });

        /* AccountsService.get(undefined, undefined, function (data) {
         angular.forEach(data['data'], function (a, b) {
         $scope.accounts.push({'id': b, 'name': a['ACCOUNT_NAME'], 'number': a['ACCOUNT_NUMBER']})
         })

         })*/

    };
    $scope.confirmDelete = angular.noop();

    $scope.accountMapOptions.deleteFunc = function (data) {
        $modal.open({
            templateUrl: 'deleteAccountMapping.html',
            windowClass: 'modal alert alert-error popupLarge',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, AccountMapService) {
                $scope.confirmDelete = function () {
                    $rootScope.openModalPopupOpen();
                    AccountMapService.deleteData(data["_id"], function (data) {
                        $rootScope.openModalPopupClose();
                        $scope.accountMapOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    }, function (errorData) {
                        $scope.cancel();
                        $rootScope.openModalPopupClose();
                        $scope.showErrormsg(errorData.msg);
                    });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }

    $scope.downloadAccMasterTemplate = function () {
        console.log('download template')
    }
    $scope.getAccountMapping()

    $scope.uploadFile = function () {
        $modal.open({
            templateUrl: 'uploadfile.html',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, DataSourceService) {
                $scope.upload = function (files) {
                    console.log(files[0]);
                    $scope.fileName = files[0].name
                    console.log($scope.fileName)
                    if (files[0].size > 20971520) {
                        $scope.msgNotify('warning', 'Uploaded File was more than 20 MB.Please choose less than 20MB file');
                        files = [];
                    }

                    $scope.saveFile = function () {

                        $rootScope.openModalPopupOpen()

                        $scope.name = files[0].name.split('.')
                        $scope.array = [$scope.name[0], $scope.name[1].toLowerCase()]
                        $scope.id = $scope.array.join('.')

                        $upload.upload({
                            url: '/api/utilities/' + $scope.id + '/upload_account_details',
                            method: 'POST',
                            file: files[0]
                        }).progress(function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        }).success(function (data, status, headers, config) {
                            if (data.status == "error") {
                                $rootScope.showAlertMsg(data.msg)
                            }
                            else {
                                $rootScope.showSuccessMsg('Account Details Added Succesfully')
                                $rootScope.openModalPopupClose()
                                $modalInstance.dismiss('cancel');
                                $scope.acountMasterOptions.reQuery = trueconf
                            }
                        })
                    };
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    // Account Master setup
    $scope.getAccountMaster = function () {
        $scope.acountMasterOptions.searchFields = {}
        $scope.acountMasterOptions.service = AccountsService;
        $scope.acountMasterOptions.headers = [
            [
                {name: "Account Number", searchable: true, sortable: true, dataField: "ACCOUNT_NUMBER", colIndex: 0},
                {name: "Account Name", searchable: true, sortable: true, dataField: "ACCOUNT_NAME", colIndex: 1}
            ]
        ];
        //if ($scope.acountMasterOptions.datamodel['total'] <= 0)
        //    $scope.disableAcctMasterExport = true
    }

    $scope.getAccountMaster()

    //export to csv
    $scope.separator = ','
    $scope.getHeader = function () {
        return ['ACCOUNT_NUMBER', 'ACCOUNT_NAME']
    };
    $scope.filename = "ACCOUNT_MASTER";
    $scope.getArray = function () {

        AccountsService.get('', '', function (data) {
            return data['data']
        })
    };


    $scope.exportAccountDetails = function () {
        $scope.getAccountMasterArray = [];

        AccountsService.get(undefined, undefined, function (data) {

            $scope.getAccountMasterArray.push({
                "ACCOUNT_NUMBER": "ACCOUNT_NUMBER",
                "ACCOUNT_NAME": "ACCOUNT_NAME"
            })
            for (i = 0; i < data.total; i++) {
                $scope.getAccountMasterArray.push({
                    "ACCOUNT_NUMBER": data.data[i]['ACCOUNT_NUMBER'],
                    "ACCOUNT_NAME": data.data[i]['ACCOUNT_NAME']
                })
            }
            $("#getAccountMaster").trigger('click');
            return false;
        })
    }

    // Account Pool Setup
    $scope.getAccountPoolDetails = function () {
        $scope.accountPoolMasterOptions.searchFields = {}
        $scope.accountPoolMasterOptions.service = AccountPoolService;
        $scope.accountPoolMasterOptions.headers = [
            [
                {
                    name: "Account Pool Name",
                    searchable: true,
                    sortable: true,
                    dataField: "accountPoolName",
                    colIndex: 0
                },
                {
                    name: "Accounts",
                    searchable: true,
                    sortable: true,
                    dataField: "accounts",
                    colIndex: 1,
                    filter: "formatAccNumber"
                },
                {
                    name: "Actions",
                    colIndex: 3,
                    width: '10%',
                    actions: [{
                        toolTip: "Edit",
                        action: "addAccountPools",
                        posticon: "fa fa-pencil fa-lg",
                        "disabledFn": "disableEdit"
                    },
                        {
                            toolTip: "Delete",
                            action: "deleteAccountPools",
                            posticon: "fa fa-trash-o fa-lg"
                        }]
                }
            ]
        ];
    }


    $scope.accountPoolMasterOptions.addAccountPools = function (ac) {
        $scope.accounts = []

        $modal.open({
            templateUrl: 'accountPoolMaster.html',
            windowClass: 'modal addUser',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, AccountsService) {

                $scope.getAccoutDetails = function () {

                    $scope.accountMaster = []
                    var selectedAcc = []

                    if (angular.isDefined(ac)) {
                        $scope.accPool = ac
                        $scope.accPool['selectedAccounts'] = []
                    } else {
                        $scope.accPool = {'selectedAccounts': []}
                    }

                    AccountsService.get(undefined, undefined, function (data) {
                        if (data['total'] > 0) {

                            //check for edit mode
                            if (angular.isDefined(ac)) {
                                $scope.accPool['accPoolName'] = ac['accountPoolName']

                                angular.forEach(ac['accounts'], function (value, index) {
                                    selectedAcc.push(value['accountNumber'])
                                })
                            }

                            angular.forEach(data['data'], function (val, index) {
                                var tmp = {'label': val['ACCOUNT_NAME'], 'Default': val}
                                $scope.accountMaster.push(tmp)

                                // check if any selected account number available
                                if (angular.isDefined(selectedAcc) && selectedAcc.length > 0) {
                                    if (selectedAcc.indexOf(val['ACCOUNT_NUMBER']) != -1) {
                                        $scope.accPool['selectedAccounts'].push(tmp)
                                    }
                                }
                            })
                        }
                    })
                }

                $scope.saveAccountPools = function (accountPool) {

                    dataR = {}
                    dataR['accountPoolName'] = accountPool['accPoolName']
                    dataR['accounts'] = []

                    angular.forEach(accountPool['selectedAccounts'], function (value, index) {
                        dataR['accounts'].push({
                            'accountNumber': value['Default']['ACCOUNT_NUMBER'],
                            'accountName': value['Default']['ACCOUNT_NAME']
                        })
                    })

                    if (angular.isDefined(accountPool['_id'])) {
                        AccountPoolService.put(accountPool['_id'], dataR, function (data) {
                            console.log(data)
                            $rootScope.showSuccessMsg("Account Pool Details updated successfully")
                            $scope.cancel()
                        })
                    }
                    else {
                        AccountPoolService.post(dataR, function (data) {
                            console.log(data)
                            $rootScope.showSuccessMsg("Account Pool Details Saved successfully")
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }

                    $scope.accountPoolMasterOptions.reQuery = true;

                }

                $scope.getAccoutDetails()


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    $scope.accountPoolMasterOptions.reQuery = true;
                };


            }
        });

        /* AccountsService.get(undefined, undefined, function (data) {
         angular.forEach(data['data'], function (a, b) {
         $scope.accounts.push({'id': b, 'name': a['ACCOUNT_NAME'], 'number': a['ACCOUNT_NUMBER']})
         })

         })*/

    };


    $scope.accountPoolMasterOptions.deleteAccountPools = function (value) {

        if (angular.isDefined(value['_id'])) {
            $modal.open({
                templateUrl: 'deleteAccountPools.html',
                windowClass: 'modal alert alert-error popupLarge',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance) {
                    $scope.confirmDelete = function () {
                        $rootScope.openModalPopupOpen();
                        AccountPoolService.deleteData(value['_id'], function (data) {
                            $scope.accountPoolMasterOptions.reQuery = true;
                            $rootScope.openModalPopupClose();
                            $modalInstance.dismiss('cancel');
                        }, function (errorData) {
                            $scope.showErrormsg(errorData.msg);
                        });
                    }
                    $scope.cancel = function () {
                        $scope.accountPoolMasterOptions.reQuery = true;
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }

    }

    $scope.getAccountMasterDetails = function () {

        $scope.accountMaster = []
        AccountsService.get(undefined, undefined, function (data) {
            console.log(data)
            angular.forEach(data['data'], function (a, b) {
                $scope.accountMaster.push({'id': b, 'name': a['ACCOUNT_NAME'], 'number': a['ACCOUNT_NUMBER']})
            })
        })
    }

    $scope.getAccountMasterDetails()

    $scope.getAccountPoolDetails()


}