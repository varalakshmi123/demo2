var app = angular.module("webApp");
app.config(["$routeProvider", function ($routeProvider) {
    //Configure application routes
    $routeProvider.when('/login', {
        templateUrl: '/app/partials/login.html'
    }).when('/reconbuilder', {
        templateUrl: '/app/partials/reconbuilder.html',
        controller: reconbuilderController
    }).when('/businesscontext', {
        templateUrl: '/app/partials/businessContext.html',
        controller: businessContextController
    }).when('/accountsetup', {
        templateUrl: '/app/partials/accountsetup.html',
        controller: accountsetupController
    }).when('/feedDefination', {
        templateUrl: '/app/partials/feedDefination.html',
        controller: feedDefinationController
    }).when('/staticDataSetup', {
        templateUrl: '/app/partials/staticDataSetup.html',
        controller: staticDataSetupController
    }).when('/preloadScript', {
        templateUrl: '/app/partials/preloadScript.html',
        controller: preloadScriptController
    })
}]);

