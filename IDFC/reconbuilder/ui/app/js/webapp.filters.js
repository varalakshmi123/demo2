var app = angular.module("webApp.filters", []);

app.filter("tolocal", function () {
    return function (input) {
        var offset = new Date().getTimezoneOffset() * 60;
        var d = new Date((input - offset) * 1000);
        return d.toString();
    }
});

app.filter("arraytocsv", function () {

    return function (input, maxlength) {
        if (!angular.isDefined(maxlength)) {
            maxlength = 10;
        }
        var resp = "";
        if (angular.isArray(input)) {
            resp = input.join(", ");
            if (angular.isDefined(maxlength) && resp.length > maxlength) {
                resp = resp.substr(0, maxlength - 4);
                resp += " ...";
            }
        }
        else {
            //console.log("arraytocsv: Input not array!!!");
            resp = input;
        }
        return resp;
    }
});

app.filter("showForRole", ['$rootScope', function ($rootScope) {
    return function (input) {
        if (input) {
            return ($rootScope.credentials && input.indexOf($rootScope.credentials["role"]) >= 0);
        }
        return true;
    }
}]);

// Below filters used in account setup page

app.filter("formatAccNumber", ['$rootScope', function ($rootScope) {
    return function (arr) {
        var accountNameArray = []
        angular.forEach(arr, function (value, index) {
            accountNameArray.push(value['accountName'])
        })
        return accountNameArray.join(' , ');
    }
}])

app.filter("formatFeedDetails", ['$rootScope', function ($rootScope) {
    return function (arr) {
        var feedDetails = []
        angular.forEach(arr, function (value, index) {
            feedDetails.push(value['feedName'])
        })
        return feedDetails.join(' , ');
    }
}])
