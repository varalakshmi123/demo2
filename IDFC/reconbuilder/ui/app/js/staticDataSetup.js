function staticDataSetupController($scope, $rootScope, StaticDataMasterService, StaticDataTypeService, $modal) {

    $scope.staticDataSetupOptions = {};
    $scope.staticDataSetupOptions.init = false;


    $scope.getStaticDataValues = function () {

        $scope.staticDataSetupOptions.searchFields = {}

        $scope.staticDataSetupOptions.query = function () {

            if (angular.isDefined($scope.staticDataType) && $scope.staticDataType != '') {

                var perColConditions = {};
                perColConditions["staticDataType"] = $scope.staticDataType
                $scope.staticDataValues = []

                StaticDataMasterService.get(undefined, {"query": {"perColConditions": perColConditions}}, function (data) {
                    $scope.staticDataSetupOptions.datamodel = data
                })
            }
        }
        $scope.staticDataSetupOptions.reQuery = true

    }

    $scope.staticDataSetupOptions.headers = [
        [
            {name: "Static Name", searchable: true, sortable: true, dataField: "staticName", colIndex: 0},
            {name: "Static Code", searchable: true, sortable: true, dataField: "staticCode", colIndex: 1},
            {name: "Description", searchable: true, sortable: true, dataField: "description", colIndex: 2},
            {
                name: "Actions",
                colIndex: 4,
                width: '10%',
                actions: [{
                    toolTip: "Edit",
                    action: "addStaticDataValue",
                    posticon: "fa fa-pencil fa-lg",
                    "disabledFn": "disableEdit"
                },
                    {
                        toolTip: "Delete",
                        action: "deleteStaticData",
                        posticon: "fa fa-trash-o fa-lg"
                    }]
            }
        ]
    ];


    function getStaticDataType() {
        $scope.staticDataTypes = []
        StaticDataTypeService.get(undefined, undefined, function (data) {
            if (data['total'] > 0) {
                $scope.staticDataTypes = data.data
            }
        }, function (err) {
            $rootScope.showAlertMsg(err.msg)
        })
    }

    $scope.staticDataSetupOptions.addStaticDataValue = function (_value) {
        $modal.open({
            templateUrl: 'addStaticDataDetails.html',
            windowClass: 'modal',
            backdrop: 'static',
            scope: $scope,
            controller: function ($scope, $modalInstance, StaticDataMasterService) {


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.saveStaticDataParam = function (value) {
                    if (!angular.isDefined(value['_id'])) {
                        value['staticDataType'] = $scope.staticDataType
                        StaticDataMasterService.post(value, function (data) {
                            console.log(data)
                            $scope.getStaticDataValues()
                            $rootScope.showSuccessMsg("Static details added successfully")
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    } else {
                        StaticDataMasterService.put(value['_id'], _value, function (data) {
                            console.log(data)
                            $scope.getStaticDataValues()
                            $rootScope.showSuccessMsg("Static details updated successfully")
                            $scope.cancel()
                        }, function (err) {
                            $rootScope.showAlertMsg(err.msg)
                        })
                    }
                }

                // populate static data if any
                function populateStaticData() {
                    $scope.staticData = _value
                }

                populateStaticData()
            }
        });
    }


    $scope.staticDataSetupOptions.deleteStaticData = function (value) {
        if (angular.isDefined(value['_id'])) {
            $modal.open({
                templateUrl: 'deleteStaticDataDetails.html',
                windowClass: 'modal',
                backdrop: 'static',
                scope: $scope,
                controller: function ($scope, $modalInstance, StaticDataMasterService) {
                    $scope.confirmDelete = function () {
                        $rootScope.openModalPopupOpen();
                        StaticDataMasterService.deleteData(value['_id'], function (data) {
                            $rootScope.openModalPopupClose();
                            $scope.staticDataSetupOptions.reQuery = true;
                            $modalInstance.dismiss('cancel');
                        }, function (errorData) {
                            $scope.showErrormsg(errorData.msg);
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }
    }

    getStaticDataType()

}