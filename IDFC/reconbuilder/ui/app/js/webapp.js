agGrid.initialiseAgGridWithAngular1(angular);
agGrid.initialiseAgGridWithWebComponents();
var app = angular.module("webApp", ['ngRoute', 'ngSanitize', 'webApp.services', 'webApp.directives', 'webApp.filters','ui.bootstrap', 'ui','agGrid', 'isteven-multi-select','ui.multiselect',"ngIdle","angularFileUpload","ngCsv",'ui.multiselect','angularjs-dropdown-multiselect','angular.chosen','scrollable-table']);

app.config(["$locationProvider", "$httpProvider", "$sceProvider", "IdleProvider", "KeepaliveProvider", function($locationProvider, $httpProvider, $sceProvider, IdleProvider, KeepaliveProvider) {
    $sceProvider.enabled(false);
    //Move to html5 mode, clean URL's
    $locationProvider.html5Mode(true);

    IdleProvider.idle(15 * 60); // in seconds
    IdleProvider.timeout(30); // in seconds

    //http interceptor to handle 401 and redirect
    var respInterceptor = ["$q", "$location", "$rootScope", function ($q, $location, $rootScope) {
        function success(response) {
			//console.log(JSON.stringify(response));
			if (response.data.status === "error") {
				$rootScope.notificationMsg.push({
					msg: response.data.msg,
					time: new Date().getTime() / 1000
				});
			}
            return response;
        }

        function error(response) {
            if (response.status === 401) {
                $rootScope.errorMsg = '';
                $rootScope.credentials = null;
                $rootScope.openModalPopupClose();
                $location.path("/login");
                return $q.reject(response);
            }
            // otherwise, default behaviour
            //return $q.reject(response);
            return response;
        }

        return function(promise) {
            return promise.then(success, error);
        }
    }];

    $httpProvider.responseInterceptors.push(respInterceptor);
}]);

app.run(["$rootScope", "$http", "$location", "$timeout", "$log", "$modal", '$window',"$templateCache", "Idle",
    function ($rootScope, $http, $location, $timeout, $log, $modal, $window, $templateCache, Idle) {
        toastr.options = {  "closeButton": true,  "debug": false,  "newestOnTop": true,  "progressBar": true,  "positionClass": "toast-top-right",  "preventDuplicates": true,  "onclick": null,  "showDuration": "300",  "hideDuration": "1000",  "timeOut": "5000",  "extendedTimeOut": "1000",  "showEasing": "swing",  "hideEasing": "linear",  "showMethod": "fadeIn",  "hideMethod": "fadeOut"};
        $rootScope.msgNotify = function(type, msg){
            if(type == 'error'){
                toastr.options = {  "closeButton": true,  "debug": false,  "newestOnTop": true,  "progressBar": true,  "positionClass": "toast-top-right",  "preventDuplicates": true,  "onclick": null,  "showDuration": "0",  "hideDuration": "0",  "timeOut": "0",  "extendedTimeOut": "0",  "showEasing": "swing",  "hideEasing": "linear",  "showMethod": "fadeIn",  "hideMethod": "fadeOut"};
            }else{
                toastr.options = {  "closeButton": true,  "debug": false,  "newestOnTop": true,  "progressBar": true,  "positionClass": "toast-top-right",  "preventDuplicates": true,  "onclick": null,  "showDuration": "300",  "hideDuration": "1000",  "timeOut": "5000",  "extendedTimeOut": "1000",  "showEasing": "swing",  "hideEasing": "linear",  "showMethod": "fadeIn",  "hideMethod": "fadeOut"};
            }
            toastr[type](msg);
        }

        Idle.watch();

        $rootScope.$on('IdleTimeout', function () {
            //$rootScope.doLogout();
        });
        $rootScope.reset = function() {
          Idle.watch();
        }
        $rootScope.class = 'rightContent'
        $rootScope.classFlag = false
        $rootScope.removeClass = function () {
            if($rootScope.classFlag && $rootScope.credentials != null){
                $rootScope.class = ''
            }
            else{
                if ($rootScope.credentials != null){
                    $rootScope.class = 'rightContent'
                }
            }
        }
	    var path = function () {
            return $location.path();
        };
        $rootScope.$watch(path, function (newVal, oldVal) {
            if (newVal.lastIndexOf("/")) {
                newVal = newVal.substring(0, newVal.lastIndexOf("/"));
            }
            $rootScope.activetab = newVal;
     //       $rootScope.redirectCheck();
        });
        $rootScope.notificationMsg = [];
        function init() {
            $rootScope.isauthenticated = false;
            $rootScope.credentials = null;
            $rootScope.notificationMsg = [];
            $rootScope.appData = {};
            $rootScope.user = {};
            $rootScope.user.role = "admin";
            $rootScope.curState = {};
            $rootScope.companies = false;
            $rootScope.administration = false;
            $rootScope.jobs = false;
            $rootScope.billingprofile = false;
//            resourceService.getResource('strings');
//            $rootScope.appData.title = "Fission Cloud";
            $rootScope.customeLoadingMsg = "";
	    $rootScope.config = {};
    	    $rootScope.config.title = "";
        }
        function getInternetExplorerVersion(){
            var rv = -1;
            if (navigator.appName == 'Microsoft Internet Explorer'){
                var ua = navigator.userAgent;
                var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{x`0,})");
                if (re.exec(ua) != null)
                  rv = parseFloat( RegExp.$1 );
            }
            else if (navigator.appName == 'Netscape') {
                var ua = navigator.userAgent;
                var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                  rv = parseFloat( RegExp.$1 );
            }
            return rv;
        }
        //$rootScope.navigator.sayswho= (function(){
        //    var ua= navigator.userAgent, tem,
        //    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        //    if(/trident/i.test(M[1])){
        //        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        //        return 'IE '+(tem[1] || '');
        //    }
        //    if(M[1]=== 'Chrome'){
        //        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        //        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        //    }
        //    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        //    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        //    return M.join(' ');
        //})();
        //console.log($rootScope.navigator.sayswho())
         $rootScope.$on('$stateChangeStart',
       function(event, toState, toParams, fromState, fromParams){
           var IEVersion = getInternetExplorerVersion();
           console.log(IEVersion)
           if(IEVersion == 11){
               $rootScope.handleErrorMessages({"msg":"ERECON does not support Internet Explorer 11 browser."});
           };
           if(toState.name.indexOf('dashboard') ==0)
           {
               angular.forEach($rootScope.curState, function(v, k){
                   $rootScope.curState[k] = false;
               });
               var states = toState.name.split('.');
                if(states.length >= 2){
                    $rootScope.curState[states[1]] = true;
                }
           }
       });
        //$rootScope.$broadcast('$stateChangeStart',{});
        $rootScope.notificationMsg = [];
        $rootScope.errorMsg = '';
        $rootScope.credentials = null;
        $rootScope.loading = false;
        //Register login/logout function at RootScope so that its available for the entire application
        $rootScope.doLogin = function (credentials) {
            //credentials['userName'] = credentials['userName'].toLowerCase()
            $rootScope.errorMsg = '';
            $http.post("/api", credentials, {headers: {"Content-Type": "application/json"}}).success(function (data, status) {
                if (data.status === "error")
                {
                    $rootScope.errorMsg = data.msg;
                    $rootScope.isauthenticated = false
                }
                else
                {
                  $rootScope.reset();
                  if (data.msg.role == 'admin' || data.msg.role == 'Administrator') {
                        $location.path("/businesscontext");
                    }
                    $rootScope.classFlag = false
                    $rootScope.isauthenticated = true
                    $rootScope.credentials = data.msg;
                    $rootScope.isCustomer = (data.msg.role == "customer") ? true : false;
                    $rootScope.isOracle = (data.msg.role == "oracleuser") ? true : false;
                    $rootScope.isOracleRO = (data.msg.role == "oraclereadonly") ? true : false;
                }
            })
        };
        $rootScope.doLogout = function () {
            $rootScope.classFlag = true
            //$rootScope.openModalPopupOpen()
            $rootScope.errorMsg = '';
            $rootScope.credentials = null;
            $http.post("/api/logout", {}, {headers: {"Content-Type": "application/json"}});
            //$rootScope.openModalPopupClose()
        };
        $rootScope.showsub = false
        $rootScope.checkSession = function () {
            $http.get("/api").error(function () {
                $location.path("/login");
                $rootScope.loading = false;
                $rootScope.classFlag = true
            }).success(function (data) {
            if (data.status === "error")
                {
                    $rootScope.errorMsg = data.msg;
                    $rootScope.isauthenticated = false
                    $rootScope.classFlag = true
                }
                else
                {
                    console.log($location.path())
                    //console.log($rootScope.reconID)
                    $rootScope.isauthenticated = true
                    $rootScope.classFlag = false
                    $rootScope.credentials = data.msg;
                    $rootScope.isCustomer = (data.msg.role == "customer") ? true : false;
                    $rootScope.isOracle = (data.msg.role == "oracleadmin") ? true : false;
                    $rootScope.isOracleRO = (data.msg.role == "oracleuser") ? true : false;
                      if (data.msg.role == 'admin' || data.msg.role == 'Administrator') {
                            $location.path("/businesscontext");
                        }

                }

            });
        }
  /*      $rootScope.redirectCheck = function () {
            $rootScope.fissionServer="";
            StorageService.isInitialized("dummy", function (data) {
                if (data == "Not Initialized") {
                    $location.path("/storage");
                }
            });
        }*/

	    function clearNotifications() {
                var currTime = new Date().getTime() / 1000;
                var count = $rootScope.notificationMsg.length;
                while (count--) {
                        if (currTime - $rootScope.notificationMsg[count].time >= 2) {
                                $rootScope.notificationMsg.splice(count, 1);
                        }
                }
                $timeout(clearNotifications, 10000);
        }
        $timeout(clearNotifications, 10000);

/*        function clearNotifications() {
            var currTime = new Date().getTime() / 1000;
            var count = $rootScope.notificationMsg.length;
            while (count--) {
                if (currTime - $rootScope.notificationMsg[count].time >= 2) {
                    $rootScope.notificationMsg.splice(count, 1);
                }
            }
            $timeout(clearNotifications, 2000);
        }
        $timeout(clearNotifications, 2000);*/
        angular.element(document.body).bind('click', function (e) {
            var popups = document.querySelectorAll('*[popover]');
            if (popups) {
                for (var i = 0; i < popups.length; i++) {
                    var popup = popups[i];
                    var popupElement = angular.element(popup);

                    var content;
                    var arrow;
                    if (popupElement.next()) {
                        content = popupElement.next()[0].querySelector('.popover-content');
                        arrow = popupElement.next()[0].querySelector('.arrow');
                    }
                    if (popup != e.target && e.target != content && e.target != arrow) {
                        if (popupElement.next().hasClass('popover')) {
                            popupElement.next().remove();
                            popupElement.scope().tt_isOpen = false;
                        }
                    }
                }
            }
        });
        init();
        $rootScope.checkSession();
        $rootScope.loadingModal = null;
        $rootScope.openModalPopupOpen = function (test) {
            if ($rootScope.loadingModal == null)
                $rootScope.loadingModal = $modal.open({
                    templateUrl: 'modalLoadingView.html',
                    windowClass: 'modal loaderModalPopup',
                    backdrop: 'static'
                });
            //$('#fxshell-tabspinner').show();
        };
        $rootScope.openModalPopupClose = function () {
            if ($rootScope.loadingModal != null) {
                $rootScope.customeLoadingMsg = '';
                $rootScope.loadingModal.close("");
                $rootScope.loadingModal = null;
            }
            //$('#fxshell-tabspinner').hide();
        };
        $rootScope.openModalPopupAddDiskOpen = function () {
            if ($rootScope.loadingModal == null)
                $rootScope.loadingModal = $modal.open({
                    templateUrl: 'modalAddingDiskView.html',
                    windowClass: 'modal loaderModalPopup',
                    backdrop: 'static'
                });
        };
        $rootScope.openModalPopupAddDiskClose = function () {
            if ($rootScope.loadingModal != null) {
                $rootScope.loadingModal.close("");
                $rootScope.loadingModal = null;
            }
        };
        $rootScope.openModalPopupCreateDiskOpen = function () {
            if ($rootScope.loadingModal == null)
                $rootScope.loadingModal = $modal.open({
                    templateUrl: 'modalCreatingDiskView.html',
                    windowClass: 'modal loaderModalPopup',
                    backdrop: 'static'
                });
        };
        $rootScope.openModalPopupCreateDiskClose = function () {
            if ($rootScope.loadingModal != null) {
                $rootScope.loadingModal.close("");
                $rootScope.loadingModal = null;
            }
        };

        $rootScope.formValidation = {"isNotValid": false, "message": "", "type": ""};
        $rootScope.showAlert = function (msg, show, type) {
            $rootScope.formValidation.isNotValid = show;
            $rootScope.formValidation.message = msg;
            $rootScope.formValidation.type = type;
        };

        $rootScope.handleErrorMessages = function (data) {
            //$rootScope.openModalPopupClose();
            $rootScope.isDisable = false;
            $rootScope.customeLoadingMsg = "";
            $rootScope.showAlert(data.msg, true, "alert-danger");
        };

        $rootScope.showErrormsg = function (msg) {
            $rootScope.customeLoadingMsg = "";
            $modal.open({
                templateUrl: 'showErrormsg.html',
                windowClass: 'modal alert alert-danger',
                backdrop: 'static',
                controller: function ($rootScope, $modalInstance) {
                    $rootScope.bindMsg = msg;
                    $rootScope.submit = function (data) {
                        $modalInstance.dismiss('cancel');
                    };
                    $rootScope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $rootScope.showSuccessMsg = function (msg) {
            $modal.open({
                templateUrl: 'showSuccessMsg.html',
                windowClass: 'modal alert alert-success',
                backdrop: 'static',
                controller: function ($rootScope, $modalInstance) {
                    $rootScope.bindMsg = msg;
                    $rootScope.submit = function (data) {
                        $modalInstance.dismiss('cancel');
                    };
                    $rootScope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    //$timeout(function () {
                    //    $modalInstance.dismiss('cancel');
                    //}, 5000)
                }
            });
        };

         $rootScope.showAlertMsg = function (msg) {
            $modal.open({
                templateUrl: 'showAlertMsg.html',
                windowClass: 'modal alert alert-success',
                backdrop: 'static',
                controller: function ($rootScope, $modalInstance) {
                    $rootScope.bindMsg = msg;
                    $rootScope.submit = function (data) {
                        $modalInstance.dismiss('cancel');
                    };
                    $rootScope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
//                    $timeout(function () {
//                        $modalInstance.dismiss('cancel');
//                    }, 5000)
                }
            });
        };
//        $http.get('app/js/account_pools.json').success(function (data) {
////            $scope.salesMarginData = data
//                console.log(data)
//                $rootScope.businessData = data
//        }).error(function (err) {
//
//        })

        //$(window).bind('beforeunload', function(){
        //    return 'Are you sure ?';
        //});
        //$( window ).unload(function() {
        //    var request = new XMLHttpRequest();
        //    request.open('GET', '/api/logout', false);  // `false` makes the request synchronous
        //    request.send(null);
        //
        //    if (request.status === 200) {
        //      console.log(request.responseText);
        //    }
        //});
	$rootScope.clock = "loading clock...";
	$rootScope.tickInterval = 1000

	var tick = function(){
	 $rootScope.clock = Date.now();
	 $timeout(tick, $rootScope.tickInterval);
	}
	 $timeout(tick, $rootScope.tickInterval);
    }]);

