import os
import sys
from datetime import datetime

import cx_Oracle
import pandas as pd
from bson import ObjectId
from pymongo import MongoClient

exportFolder = '/home/hduser/reconmigrations/'

def exportReconMigration(reconId):
    ip, port, SID, userName, pwd = '172.19.36.226', 1653, 'reconuat1', 'reconuat', 'IDfc$#321'

    col_name = 'recon_context_details'
    # UAT Mongo
    lmongo_client = MongoClient('mongodb://localhost:27017/')
    lmongo_db = lmongo_client.reconbuilder
    lcc_col_recon = lmongo_db[col_name]
    print 'Started Recon Migration for reconId: ', reconId
    backupDT = datetime.now().strftime("%d%b%Y")
    for record in lcc_col_recon.find():
        if 'reconId' in record and record['reconId'] == str(reconId):
            folderName = exportFolder + reconId + os.sep
            if not os.path.exists(folderName):
                os.mkdir(folderName)
            folderName = exportFolder + reconId + os.sep + backupDT + os.sep
            if not os.path.exists(folderName):
                os.mkdir(folderName)
            q = './mongoexport -d reconbuilder -c  recon_context_details -q "{ reconId: ' + repr(
                reconId) + ' }" --out ' + folderName + reconId + '.json'
            os.system(q)
            print '|Saved Recon Matching Rules: ', record['reconName']

            # Save Feed Defination
            for feed in record['sources']:
                for i in lmongo_db.feedDefination.find({'_id': ObjectId(feed['feedId'])}):
                    q = './mongoexport -d reconbuilder -c  feedDefination -q \'{ _id: ObjectId("' + (
                        feed['feedId']) + '") }\' --out ' + folderName + str(feed['feedId']) + '.json'
                    os.system(q)
                    print '|Saved Feed Details: ', i['feedName']

            for accMap in lmongo_db.feedAccMapping.find({'reconContext.reconId': reconId}):
                q = './mongoexport -d reconbuilder -c  feedAccMapping -q "{'+repr('reconContext.reconId')+': ' + repr(
                    reconId) + ' }" --out ' + folderName + str(reconId) + '_accMapping' + '.json'
                os.system(q)
                print '| Saved Account Mapping Details: ', i['feedName']

            for accMap in lmongo_db.feedAccMapping.find({'reconId': reconId}):
                print '| Saved PreMatching Details: ', i['feedName']
                q = './mongoexport -d reconbuilder -c  feedAccMapping -q "{ reconId: ' + repr(
                    reconId) + ' }" --out ' + folderName + str(reconId) + '_prematch.json'
                os.system(q)

            for accMap in lmongo_db.postmatchscripts.find({'reconId': reconId}):
                q = './mongoexport -d reconbuilder -c  postmatchscripts -q "{ reconId: ' + repr(
                    reconId) + ' }" --out ' + folderName + str(reconId) + '_postmatch.json'
                os.system(q)
                print '| Saved PostMatching Details: ', i['feedName']
            print 'Please Find the files in folder :',folderName

            # export recon_dynamic_data_model
            dsn_tns = cx_Oracle.makedsn(ip, port, SID)
            connection = cx_Oracle.connect(userName, pwd, dsn_tns)
            qry = "select * from recon_dynamic_data_model where recon_id = '{}' " \
                  "and record_status = 'ACTIVE' order by order_seq_no".format(reconId)
            df = pd.read_sql(qry, connection)
            df.to_csv(folderName+'recon_dynamic_data_model.csv',index=False)

            break
    exit(0)


if __name__ == '__main__':
    reconName = 'EPYMTS_CASH_APAC_20225'
    exportReconMigration(sys.argv[1])
