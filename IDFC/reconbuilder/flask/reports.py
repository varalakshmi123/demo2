import cx_Oracle
import pandas
import numpy as np

class ReportInterface():
    def __init__(self,datasetName):
        self.ip="192.168.2.129"
        self.port=1521
        self.sid='xe'
        self.oradsn=cx_Oracle.makedsn(self.ip, self.port, self.sid)
        self.oraconnection=cx_Oracle.connect('algoreconutil_dev', 'algorecon', self.oradsn)
        self.metadata={"tree":["BUSINESS_CONTEXT_ID","RECON_ID","SOURCE_TYPE","RECONCILIATION_STATUS"],"aggregation":["COUNT","CREDIT","DEBIT","NETOUTSTANDINGAMOUNT"]}
        self.df=None

    def getHierarchy(self):
        return True,self.metadata

    def loadDataSet(self,datasetName):
        self.df = self.queryToDataframe(
            "select business_context_id, recon_id, recon_execution_id, statement_date, execution_date_time, source_type, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'")


    def queryToDataframe(self,query):
        df=pandas.read_sql(query,self.oraconnection)
        return df

    def queryData(self,levelkey,filters):
        filterstr=""
        for key,val in filters.items():
            if len(filterstr)>1:
                filterstr+= ' & '
            if type(val)==str:
                val="\""+val+"\""
            else:
                val=str(val)
            filterstr="(self.df[\""+key+"\"]=="+val+")"
        filteredData=self.df
        if len(filterstr)>1:
            #x="self.df["+filters+"]"
            filteredData=filteredData[eval(filterstr)]
        filteredData=self.customProcess(filteredData,levelkey)
        return True,filteredData

    def makehighchartsjson(df, columnHierarchy):
        output = dict()
        columns = df.columns
        for index, row in df.iterrows():
            temp = output
            for col in columnHierarchy:
                # print row[col]
                if row[col] not in temp:
                    temp[row[col]] = dict()
                temp = temp[row[col]]
            index = 0
            for col in columns:
                if col in columnHierarchy:
                    continue
                else:
                    temp[col] = row[col]
        return True,output

    def customProcess(self,df,levelkey):
        columns=[]
        indexcolums=[]
        for key in self.metadata["tree"]:
            if key==levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        dfOutstanding = df[columns]
        dfOutstanding["COUNT"] = 1
        dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len})

        dfOutstanding["CREDIT"]=dfOutstanding["AMOUNT"]["C"]
        dfOutstanding["DEBIT"]=dfOutstanding["AMOUNT"]["D"]
        dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        #df["UNKNOWN"]=dfOutstanding["AMOUNT"][" "]
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["AMOUNT"]["D"] - dfOutstanding["AMOUNT"]["C"]

        dfOutstanding=dfOutstanding.drop("AMOUNT",axis=1,level=1)

        dfOutstanding.columns= dfOutstanding.columns.droplevel(1)
        print dfOutstanding.columns
        dfOutstanding.reset_index(inplace=True)
        dfOutstanding=dfOutstanding[[indexcolums[-1]]+["CREDIT","DEBIT","CREDITCOUNT","DEBITCOUNT","NETOUTSTANDINGAMOUNT"]]
        #del dfOutstanding[""]["DEBIT_CREDIT_INDICATOR"]
        #dfOutstanding.reset_index(inplace=True)
        #dfOutstanding.dropna(how='all',inplace=True)
        dfOutstanding.set_index(indexcolums[-1])
        return True,dfOutstanding

    def getNetOutstandingAmount(self):
        df=self.queryToDataframe("select business_context_id, recon_id, recon_execution_id, statement_date, execution_date_time, source_type, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'")
        #print df
        #dfUnmatched=df.ix[(df["MATCHING_STATUS"]=="UNMATCHED") & (df["RECONCILIATION_STATUS"]=="EXCEPTION")]
        #dfUnmatched=dfUnmatched[["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        #dfUnmatched=dfUnmatched.groupby(["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR"]).sum()

        #print dfUnmatched.reset_index()

        #Business context level
        dfOutstanding=df[["BUSINESS_CONTEXT_ID","RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        dfOutstanding["COUNT"]=1
        #dfOutstanding=dfOutstanding.groupby(["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR"])
        dfOutstanding=pandas.pivot_table(dfOutstanding,index=['BUSINESS_CONTEXT_ID','RECONCILIATION_STATUS'], columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT',"COUNT"],aggfunc={"AMOUNT":np.sum,"COUNT":len}).reset_index()
        #dfOutstanding=dfOutstanding.sum()
        dfOutstanding["NETOUTSTANDINGAMOUNT"]=dfOutstanding["AMOUNT"]["D"]-dfOutstanding["AMOUNT"]["C"]
        print dfOutstanding
        return True,dfOutstanding

    def getCreditOutstandingAmount(self):
        pass

    def getDebitOutstandingAmount(self):
        pass

    def getAutoMatching(self):
        pass

    def getRollbackTransactionsWaitingForAuthorizations(self):
        pass

    def getOutstandingEntriesInOpenStatus(self):
        pass

    def getWaitingForInvestigationAndResolution(self):
        pass

    def getPendingForSubmission(self):
        pass

    def getWaitingForAuthorization(self):
        pass

if __name__=='__main__':
    r= ReportInterface("RECON")
    r.loadDataSet("RECON")
    status,df= r.queryData("RECON_ID",{"BUSINESS_CONTEXT_ID":"1111148147112"})
    #del df["DEBIT_CREDIT_INDICATOR"]
    #df=df.reset_index(level=0,drop=True)
    print df
    df.to_json("onerecon.json",orient="records")