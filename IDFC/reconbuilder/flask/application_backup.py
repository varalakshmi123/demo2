import logger
logger = logger.Logger.getInstance("application").getLogger()
import dbinterface
import re
import flask
import pandas
import cx_Oracle
import numpy as np
import pymongo
import config
import os
from bson import json_util
import time
import datetime
# from datetime import timedelta, date
import json
import sql
import uuid
import datatypes


def getMongoCaseInsensitive(data):
    return re.compile("^" + data + "$", re.IGNORECASE)

def getPartnerFilePath():
    return os.path.join(config.commonFilesPath,"theme_templates")+"/"


class Business(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(Business, self).__init__("business", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(Business, self).create(doc)
        return status, businessDoc


class UsersGraph(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UsersGraph, self).__init__("usersgraph", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(UsersGraph, self).create(doc)
        return status, businessDoc

class AccountPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(AccountPool, self).__init__("accountpool", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "Business already exists."
        (status, businessDoc) = super(AccountPool, self).create(doc)
        return status, businessDoc

    def getAccountPoolDetails(self,id):
        (status, recon_con) = sql.getConnection()
        if status:
            (status_ac,ac_data) = sql.getAcPools({},connection=recon_con)
            return ac_data.to_json()


class UserPool(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(UserPool, self).__init__("userpool", hideFields)

    def create(self, doc):
        print "1111111111"
        print doc
        if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
            return False, "UserPool already exists."
        (status, userpoolDoc) = super(UserPool, self).create(doc)
        return status, userpoolDoc



class ReconAssignment(dbinterface.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def create(self, doc):
        # if self.exists({'name': {"$regex": "^" + doc['name'] + "$", "$options": 'i'}}):
        #     return False, "UserPool already exists."
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def getData(self, id, query):
        if query is not None:
            (status,recon_con) = sql.getConnection()
            if status:
                (status_rec,reconData) = sql.read_query(recon_con,query)
                json_out = reconData.to_json(orient = 'records')
                return status_rec,json_out
            else:
                return False,"No Data Found for this reconid"

    def getReconUnmatchedAll(self, id, query = {}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_unmatched_all(recon_con, query)
                # print "recondataaaaaaa" + str(reconData)
                # print type(reconData)
                # pandas.to_datetime(reconData.index, unit='ms')
                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatchedAll(self, id, query = {}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconRollBackAll(self, id, query = {}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_rollback_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconPendingForSubmissionAll(self, id, query = {}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_pending_all(recon_con, query)
                print "reconDAtaaaaaaaa" + str(reconData)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                print type(json_out)
                # json_out = json.dumps(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconWaitingForAuthorizationAll(self, id, query= {}):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_waitingForAuthorization_all(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconUnmatched(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_unmatched(recon_con, query)
                # print "recondataaaaaaa" + str(reconData)
                # print type(reconData)
                # pandas.to_datetime(reconData.index, unit='ms')
                json_out = reconData.to_json(orient='records')
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatched(self, id, query):
        logger.info("getting fired")
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched(recon_con, query)

                # To avoid [Python int too large to convert to C long] error
                reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")

                json_out = reconData_merge.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconRollBack(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_rollback(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconPendingForSubmission(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_pending(recon_con, query)
                print "reconDAtaaaaaaaa" + str(reconData)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                print type(json_out)
                # json_out = json.dumps(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconWaitingForAuthorization(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_waitingForAuthorization(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconColData(self, id, query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query(recon_con, query)
                json_out = reconData.to_json(orient='records')
                # print 'json of colss' + str(json_out)

                tosession = []
                makekey = ''
                makevalue = ''
                for r in json.loads(json_out):
                    a = dict()
                    for (k,v) in r.items():
                        if k == "MDL_FIELD_ID":
                            makekey = v
                        if k == "MDL_FIELD_DATA_TYPE":
                            makevalue = v
                        if makekey is not None and makevalue is not None:
                            a[makekey] = makevalue
                    tosession.append(a)
                flask.session['sessionData']['format'] = tosession
                # print flask.session
                # print "to push to session" + str(tosession)
                return status_rec, json_out
            else:
                return False,"No Recon Column Data"

    def validateTransaction(self, selectedTxn):
        logger.info("Validation initiated ..")

        df_cash = pandas.read_json(json.dumps(selectedTxn, default=json_util.default), orient='columns')

        df_validate = df_cash.loc[:,
                      ['ACCOUNT_NUMBER', 'CURRENCY', 'AMOUNT', 'DEBIT_CREDIT_INDICATOR', 'SOURCE_TYPE', 'RECON_ID']]
        reconID = df_validate['RECON_ID'].iloc[0]

        validation_msg = "Unable to forcematch records "
        validation_status = True

        #Currency Validation
        if len(pandas.unique(df_validate.loc[:, "CURRENCY"])) > 1:
            logger.info("mismatch in currency")
            validation_status = False
            validation_msg += ",mismatch in currency"
            #return False,"Could not forcematch the records due to mismatch in currency"

        # Amount Validation (Need to be upgraded)
        pivoted_frame = pandas.pivot_table(df_validate, columns=["SOURCE_TYPE", "DEBIT_CREDIT_INDICATOR"],
                                           values=['AMOUNT'], aggfunc={"AMOUNT": np.sum})
        pivoted_frame = pivoted_frame.unstack(level=2)
        pivoted_frame.columns = [''.join(list(col)) for col in pivoted_frame.columns.values]
        pivoted_frame.reset_index().drop(axis=1, labels="level_0")
        pivoted_frame['OutStanding_Amount'] = 0
        pivoted_frame = pivoted_frame.fillna(0)
        if 'D' in pivoted_frame.columns and 'C' in pivoted_frame.columns:
            pivoted_frame["OutStanding_Amount"] = (-pivoted_frame.loc[:, 'D']) + pivoted_frame.loc[:, 'C']
        elif 'D' in pivoted_frame.columns:
            pivoted_frame["OutStanding_Amount"] = (-pivoted_frame.loc[:, 'D'])
        elif 'C' in pivoted_frame.columns:
            pivoted_frame["OutStanding_Amount"] = (pivoted_frame.loc[:, 'C'])
        pivoted_frame = pivoted_frame.reset_index()

        diff_amount = pivoted_frame.loc[:, "OutStanding_Amount"].sum()

        if diff_amount != 0 or diff_amount != 0.0:
            logger.info("could not forcematch records due to mismatch in amount")
            validation_status = False
            validation_msg += ",outstanding amount is not zero"
            #return False, "could not forcematch records, due to mismatch in amount."

        # Account number validation
        if reconID in config.account_number_validation:
            #logger.info("Before ")
            #logger.info(df_validate["ACCOUNT_NUMBER"])
            df_validate["ACCOUNT_NUMBER"] = df_validate["ACCOUNT_NUMBER"].apply(lambda x : str(x).lstrip("0"))
            #logger.info("After")
            #logger.info(df_validate["ACCOUNT_NUMBER"])
            if len(pandas.unique((df_validate.loc[:, "ACCOUNT_NUMBER"]))) > 1:
                logger.info("mismatch in account number")
                validation_status = False
                validation_msg += ",found different Account Numbers in selected transactions"
                #return False, "could not forcematch records, due to mismatch in account numbers."
            # if len(pandas.unique(str(df_validate.loc[:, "ACCOUNT_NUMBER"]).lstrip("0"))) > 1:
            #     logger.info("mismatch in account number")
            #     return False, "could not forcematch records, due to mismatch in account numbers."

        return validation_status,"Success" if validation_status else validation_msg


    def groupColumns(self,id,query):
        # print query
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        status_commit = False
        validataion_status = True
        if not (query['singleSideMatch']):
            # logger.info("validate entering")
            (validataion_status,valdation_msg) = self.validateTransaction(cloned_val)
            # logger.info(validataion_status)
            # logger.info(valdation_msg)

        if query is not None and query['selected_rec'] is not None and validataion_status:
            (status, recon_con) = sql.getConnection()
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status,updata) = sql.updateSqlData(query=newData,connection=recon_con)
                linkid = str(uuid.uuid4().int & (1<<64)-1)
                # logger.info("linkiddddddddddddd" + str(linkid))
                logger.info('before loop time' + str(time.time()))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k,v in format_type.items():
                                for key,val in rec_old.items():
                                    if k == key and (v == "DATE" or v =="TIMESTAMP"):
                                        if val is not None:
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp((rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "GROUPED"
                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 3
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_GROUPED"

                    rec_old['LINK_ID'] = linkid
                    rec_old['GROUPED_FLAG'] = 'Y'
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = 1
                    rec_old["MATCHING_EXECUTION_STATUS"] = "OUTSTANDING"
                    rec_old['OBJECT_OID']=  str(uuid.uuid4().int & (1<<64)-1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    if 'PREV_LINK_ID' in rec_old:
                        del rec_old['PREV_LINK_ID']
                    # logger.info("Object OID ****************" + rec_old["OBJECT_OID"])
                    oldData.append(rec_old)
                logger.info('after loop and before converting to df time' + str(time.time()))
                df_cash = pandas.read_json(json.dumps(oldData,default=json_util.default),orient='columns',dtype={'OBJECT_OID':'np.int64','LINK_ID':'np.int64','RAW_LINK':'np.int64','PARTICIPATING_RECORD_TXN_ID':'np.int64','OBJECT_ID':'np.int64','PREV_LINK_ID':'np.int64'})
                logger.info('after converting to df time' + str(time.time()))
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                    # logger.info("Data frame of cash_output_txn object Oid" + str(df_cash['OBJECT_OID']))
                    # logger.info("Data frame of cash_output_txn link id" + str(df_cash['LINK_ID']))
                logger.info('before inserting time' + str(time.time()))
                 # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",connection=recon_con)
                logger.info('after inserting time' + str(time.time()))

                exceptionData = dict()
                exceptionData["EXCEPTION_ID"] = str(uuid.uuid4().int & (1<<64)-1)
                exceptionData["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exceptionData['CLEARING_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["COMMENTS"] = query['comments']
                exceptionData["CREATED_BY"] = flask.session["sessionData"]['userName']
                exceptionData["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_GROUP_INDICATOR"] = "MERGED"
                exceptionData["EXCEPTION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exceptionData["EXCEPTION_STATUS"] = "OPEN"
                exceptionData["IPADDRESS"] = "192.168.2.248"
                exceptionData["LINK_ID"] = linkid
                exceptionData["REASON_CODE"] = query['reason_code']
                if "RECON_EXECUTION_ID" in oldData[0]:
                    # print oldData[0]["RECON_EXECUTION_ID"]
                    exceptionData["RECON_EXECUTION_ID"] = oldData[0]["RECON_EXECUTION_ID"]
                exceptionData["RECON_ID"] = oldData[0]["RECON_ID"]
                exceptionData["RECORD_STATUS"] = "ACTIVE"
                exceptionData["RECORD_VERSION"] = 1
                exceptionData["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_ex.append(exceptionData)
                parseDates = ["CREATED_DATE","EXCEPTION_CREATED_DATE","CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default),orient='columns',dtype={'EXCEPTION_OID':'int64','EXCEPTION_ID':'int64','LINK_ID':'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",connection=recon_con)
                exception_alloc_data = dict()
                exception_alloc_data["ACTION_BY"] = "SYSTEM"
                exception_alloc_data["ALLOCATED_BY"] = "SYSTEM"
                exception_alloc_data["ALLOCATION_REMARKS"] = query["comments"]
                exception_alloc_data["ALLOCATION_STATUS"] = "UNALLOCATED"
                exception_alloc_data["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exception_alloc_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exception_alloc_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exception_alloc_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1<<64)-1)
                exception_alloc_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exception_alloc_data["EXCEPTION_ID"] = exceptionData["EXCEPTION_ID"]
                exception_alloc_data["EXCEPTION_RESOLUTION_STATUS"] = "OPEN"
                exception_alloc_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exception_alloc_data["IPADDRESS"] = "192.168.2.248"
                exception_alloc_data["RECON_ID"] = oldData[0]["RECON_ID"]
                exception_alloc_data["RECORD_STATUS"] = "ACTIVE"
                exception_alloc_data["RECORD_VERSION"] = 1
                exception_alloc_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                # exception_alloc_data["USER_POOL_ID"] = ""
                exception_alloc_data["WORK_POOL_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                oldData_allocation.append(exception_alloc_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default),orient='columns',dtype={'EXCEPTION_ALLOCATION_ID':'int64','EXCEPTION_ID':'int64','EXCEPTION_ALLOCATION_OID':'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                if query["groupAndForce"]:
                    logger.info("grouping done entered to force matching")
                    query_link_id = dict()
                    query_link_id["LINK_ID"] = linkid
                    columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID","RECON_EXECUTION_ID"]
                    columnsToConvertAllocation = [ "EXCEPTION_ID","EXCEPTION_ALLOCATION_ID","EXCEPTION_ALLOCATION_OID"]
                    columnsToConvertCash=["LINK_ID","RAW_LINK","PARTICIPATING_RECORD_TXN_ID","OBJECT_OID","OBJECT_ID","RECON_EXECUTION_ID"]
                    (status_exception,exmaster_data) = sql.getExceptionMasterDetails(query=query_link_id,connection=recon_con)
                    if status_exception:
                        for x in columnsToConvertMaster:
                            if x in exmaster_data.columns:
                                exmaster_data[x] = exmaster_data[x].apply(str, 1)
                        exmaster_data = exmaster_data.to_json(orient='records')
                    (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query_link_id, connection=recon_con)
                    if status_allocation:
                        for x in columnsToConvertAllocation:
                            if x in exallocation_data.columns:
                                exallocation_data[x] = exallocation_data[x].apply(str, 1)
                        exallocation_data = exallocation_data.to_json(orient='records')
                    (status_cash, cash_data) = sql.getTxnDetails(query=query_link_id,connection=recon_con)
                    if status_cash:
                        for x in columnsToConvertCash:
                            if x in cash_data.columns:
                                cash_data[x] = cash_data[x].apply(str, 1)
                        cash_data = cash_data.to_json(orient='records')
                if query['groupAndForce']:
                    queryForce = dict()
                    queryForce['selected_rec'] = cloned_val
                    queryForce['comments'] = query['comments']
                    queryForce["reason_code"] = query["reason_code"]
                    logger.info("for force matching")
                    (status_force,forcematchData) = self.forceMatchingColumns('dummy',query=queryForce,exmaster_data=exmaster_data,exallocation_data=exallocation_data)
                    if status_force:
                        logger.info("for authorization-------")
                        queryAuth = dict()
                        queryAuth['comments'] = query['comments']
                        queryAuth['reason_code'] = query['reason_code']
                        queryAuth['authorizer'] = query['authorizer']
                        # logger.info(type(forcematchData['oldData']))
                        (status_auth,auth_data) = self.submitForAuthorization('dummy',query=queryAuth,oldData=cloned_val,exmaster_data=forcematchData['oldData_ex'],exallocation_data=forcematchData['oldData_allocation'])
                        if status_auth:
                            logger.info("commiting ---------------------------------------------------------------")
                            (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                        if not status_commit:
                            logger.info("Not Committed ............................................")

                return True,'Success'
            else:
                return False,''
        else:
            if not validataion_status:
                return True,valdation_msg
            else:
                return True,'No records selected'

    def submitForAuthorization(self,id,query,oldData = None,exallocation_data = None,exmaster_data = None):
        # logger.info("submit for authorization- -----------------------------------------------")
        logger.info(query)
        oldData_auth = []
        newData = []
        oldData_ex_auth = []
        oldData_allocation_auth = []
        (status, recon_con) = sql.getConnection()
        if status:
            for rec in oldData:
                rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                #rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                rec['RECORD_STATUS'] = "INACTIVE"
                newData.append(rec)
            (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
            for rec_old in oldData:
                if 'format' in flask.session['sessionData']:
                    for format_type in flask.session['sessionData']['format']:
                        for k, v in format_type.items():
                            for key, val in rec_old.items():
                                if k == key and (v == "DATE" or v == "TIMESTAMP"):
                                    if val is not None:
                                        print "key" + str(key)
                                        # print "val" + str(val)
                                        if type(rec_old[key]) != str:
                                            rec_old[key] = datetime.datetime.fromtimestamp(
                                                (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                        else:
                                            rec_old[key] = rec_old[key]
                                        print rec_old[key]
                                        # rec_old[key] = pandas.to_datetime(rec_old[key])
                rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                rec_old['MATCHING_EXECUTION_STATE'] = "WAITING_FOR_AUTHORIZATION"
                rec_old['LINK_ID'] = rec_old['LINK_ID']
                rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                rec_old["MATCHING_STATUS"] = "MATCHED"
                rec_old["RECORD_STATUS"] = "ACTIVE"
                rec_old['AUTHORIZATION_BY'] = query['authorizer']
                rec_old["REASON_CODE"] = query["reason_code"]

                ## new added columns
                rec_old['TXN_MATCHING_STATE_CODE'] = 16
                rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_PENDING"
                rec_old['TXN_MATCHING_STATE'] = "WAITING_FOR_AUTHORIZATION"

                rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                rec_old["MATCHING_EXECUTION_STATUS"] = "WAITING FOR AUTHORIZATION"
                rec_old["MANUAL_MATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"
                rec_old["FORCEMATCH_AUTHORIZATION"] = "WAITING_FOR_AUTHORIZATION"
                oldData_auth.append(rec_old)
            df_cash_auth = pandas.read_json(json.dumps(oldData_auth, default=json_util.default), orient='columns',
                                       dtype={'OBJECT_OID': 'int64', 'LINK_ID': 'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
            parseDates = []
            for k, v in datatypes.datatypes.items():
                if v == "np.datetime64":
                    parseDates.append(k)
            for i in parseDates:
                if i in df_cash_auth.columns:
                    df_cash_auth[i] = pandas.to_datetime(df_cash_auth[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                    df_cash_auth[i] = df_cash_auth[i].fillna('')
            # logger.info('waitingforauth')
             # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
            df_cash_auth["POSITIONS_LINK_ID"] = 0
            df_cash_auth["PREV_LINK_ID"] = 0
            (status_cash_auth, ins_data_cash) = sql.insertDFToDB(df=df_cash_auth,table_name="cash_output_txn", connection=recon_con)
            exmaster_data = exmaster_data[0]
            # logger.info( "*" * 100)
            # logger.info(type(exmaster_data))
            # logger.info(exmaster_data)
            # logger.info(query)
            # logger.info(type(query))
            exmaster_data["COMMENTS"] = query['comments']
            exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
            exmaster_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
            exmaster_data["EXCEPTION_STATUS"] = "INPROGRESS"
            exmaster_data["IPADDRESS"] = "192.168.2.248"
            exmaster_data["REASON_CODE"] = query['reason_code']
            exmaster_data["RECORD_STATUS"] = "ACTIVE"
            exmaster_data["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
            exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
            oldData_ex_auth.append(exmaster_data)
            parseDates = [ "EXCEPTION_CREATED_DATE", "CLEARING_DATE","CREATED_DATE","TRADE_DATE","UPDATED_DATE","RECORD_END_DATE","EXCEPTION_COMPLETION_DATE","EXCEPTION_PROCESSING_DATE"]
            df_ex_auth = pandas.read_json(json.dumps(oldData_ex_auth, default=json_util.default), orient='columns',
                                     dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
            for i in parseDates:
                if i in df_ex_auth.columns:
                    df_ex_auth[i] = pandas.to_datetime(df_ex_auth[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                    df_ex_auth[i] = df_ex_auth[i].fillna('')
            (status_master_auth, ins_data_mas) = sql.insertDFToDB(df=df_ex_auth,
                                                      table_name="exception_master", connection=recon_con)

            exallocation_data = exallocation_data[0]
            exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
            exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
            exallocation_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            exallocation_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
            exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
            exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
            exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
            # exallocation_data['COMPLETION_DATE_TIME'] = np.NaN
            exallocation_data["IPADDRESS"] = "192.168.2.248"
            exallocation_data["RECORD_STATUS"] = "ACTIVE"
            exallocation_data["RECORD_VERSION"] = exallocation_data["RECORD_VERSION"] + 1
            exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
            oldData_allocation_auth.append(exallocation_data)
            parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
            df_al_auth = pandas.read_json(json.dumps(oldData_allocation_auth, default=json_util.default), orient='columns',
                                     dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                            'EXCEPTION_ALLOCATION_OID': 'int64'})
            for i in parseDates:
                if i in df_al_auth.columns:
                    df_al_auth[i] = pandas.to_datetime(df_al_auth[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                    df_al_auth[i] = df_al_auth[i].fillna('')
            (status_allocation_auth, ins_data_alloc) = sql.insertDFToDB(df=df_al_auth,
                                                        table_name="exception_allocation", connection=recon_con)

            if status_cash_auth and status_master_auth and status_allocation_auth:
                (status_commit,toCommit) = sql.commitToSql(connection=recon_con)

            return True,'Success'
        else:
            return False,"Oracle Connection Failed"

    def forceMatchingColumns(self, id, query,exmaster_data = None,exallocation_data = None):
        # logger.info('query ----------------------------------' + str(query['selected_rec']))
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID","RECON_EXECUTION_ID"]
            columnsToConvertAllocation = [ "EXCEPTION_ID","EXCEPTION_ALLOCATION_ID","EXCEPTION_ALLOCATION_OID"]
            if exmaster_data is None:
                (status_exception,exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],connection=recon_con)
                if status_exception:
                    for x in columnsToConvertMaster:
                        if x in exmaster_data.columns:
                            exmaster_data[x] = exmaster_data[x].apply(str, 1)
                    exmaster_data = exmaster_data.to_json(orient='records')
            if exallocation_data is None:
                (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query['selected_rec'][0], connection=recon_con)
                if status_allocation:
                    for x in columnsToConvertAllocation:
                        if x in exallocation_data.columns:
                            exallocation_data[x] = exallocation_data[x].apply(str, 1)
                    exallocation_data = exallocation_data.to_json(orient='records')
            # logger.info("exception_master" + exmaster_data)
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    #rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                # linkid = uuid.uuid4().int & (1 << 64) - 1
                # logger.info("linkid  fif" + str(linkid))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k, v in format_type.items():
                                for key, val in rec_old.items():
                                    if k == key and (v == "DATE" or v == "TIMESTAMP"):
                                        if val is not None:
                                            print "key" + str(key)
                                            # print "val" + str(val)
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp(
                                                    (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                                            print rec_old[key]
                                            # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "FORCEMATCH_MATCHED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"

                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 14
                    rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "SUBMISSION_PENDING"

                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["MATCHING_EXECUTION_STATUS"] = "AUTHORIZATION SUBMISSION PEND"
                    rec_old["MANUAL_MATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = "AUTHORIZATION_SUBMISSION_PENDING"
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype={'OBJECT_OID':'int64','LINK_ID':'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                 # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,table_name="cash_output_txn", connection=recon_con)
                exmaster_data = json.loads(exmaster_data)[0]
                print "*" * 100
                print exmaster_data
                print query['comments']
                exmaster_data["COMMENTS"] = query['comments']
                exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exmaster_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exmaster_data["EXCEPTION_STATUS"] = "INPROGRESS"
                exmaster_data["IPADDRESS"] = "192.168.2.248"
                exmaster_data["REASON_CODE"] = query['reason_code']
                exmaster_data["RECORD_STATUS"] = "ACTIVE"
                exmaster_data["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
                exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_ex.append(exmaster_data)
                parseDates = [ "EXCEPTION_CREATED_DATE", "CLEARING_DATE","CREATED_DATE","TRADE_DATE","UPDATED_DATE","RECORD_END_DATE","EXCEPTION_COMPLETION_DATE","EXCEPTION_PROCESSING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID':'int64','EXCEPTION_ID':'int64','LINK_ID':'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                          table_name="exception_master", connection=recon_con)
                exallocation_data = json.loads(exallocation_data)[0]
                exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exallocation_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exallocation_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exallocation_data["IPADDRESS"] = "192.168.2.248"
                exallocation_data["RECORD_STATUS"] = "ACTIVE"
                exallocation_data["RECORD_VERSION"] = exallocation_data["RECORD_VERSION"] + 1
                exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_allocation.append(exallocation_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID':'int64','EXCEPTION_ID':'int64','EXCEPTION_ALLOCATION_OID':'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                            table_name="exception_allocation", connection=recon_con)

                if status_cash and status_master and status_allocation:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                dataToReturn['oldData_ex'] = oldData_ex
                dataToReturn['oldData_allocation'] = oldData_allocation
                return True, dataToReturn
            else:
                return False, ''
        else:
            return False,'No records to Force Match'

    def unGroupingColumns(self, id, query):
        # print query
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()

            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID","RECON_EXECUTION_ID"]
            columnsToConvertAllocation = [ "EXCEPTION_ID","EXCEPTION_ALLOCATION_ID","EXCEPTION_ALLOCATION_OID"]
            (status_exception,exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = exmaster_data.to_json(orient='records')
            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query['selected_rec'][0], connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = exallocation_data.to_json(orient='records')
            logger.info("exception_master" + exmaster_data)
            print "exception_master" + exmaster_data
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                linkid = uuid.uuid4().int & (1 << 64) - 1
                print linkid
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k, v in format_type.items():
                                for key, val in rec_old.items():
                                    if k == key and (v == "DATE" or v == "TIMESTAMP"):
                                        if val is not None:
                                            print "key" + str(key)
                                            # print "val" + str(val)
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp(
                                                    (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                                            print rec_old[key]
                                            # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "UNGROUPED"
                    rec_old['LINK_ID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # rec_old["COMMENTS"] = query['comments']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    # rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_STATUS"] = "ACTIVE"
                    rec_old['GROUPED_FLAG'] = 'N'
                    rec_old["REASON_CODE"] = query["reason_code"]

                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 4
                    rec_old['TXN_MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['TXN_MATCHING_STATE'] = "SYSTEM_UNMATCHED_UNGROUPED"

                    # rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    # rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype={'OBJECT_OID':'int64','LINK_ID':'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                # print json.dumps(oldData)
                # print pandas.read_json(json.dumps(oldData),orient='records')
                 # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash,
                                                               table_name="cash_output_txn", connection=recon_con)
                for r in oldData:
                    logger.info("*" * 100)
                    logger.info(exmaster_data)
                    logger.info(type(exmaster_data))
                    if type(exmaster_data) == dict:
                        exmaster_data = exmaster_data
                    else:
                        exmaster_data = json.loads(exmaster_data)[0]
                    # exmaster_data = json.loads(exmaster_data)[0]
                    exmaster_data["COMMENTS"] = query['comments']
                    exmaster_data["LINK_ID"] = r["LINK_ID"]
                    exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    exmaster_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    exmaster_data["EXCEPTION_CREATED_DATE"] =datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    exmaster_data["EXCEPTION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    exmaster_data["EXCEPTION_STATUS"] = "OPEN"
                    exmaster_data["IPADDRESS"] = "192.168.2.248"
                    exmaster_data["REASON_CODE"] = query['reason_code']
                    exmaster_data["RECORD_STATUS"] = "ACTIVE"
                    exmaster_data["RECORD_VERSION"] = 1
                    exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    oldData_ex.append(exmaster_data)
                    parseDates = [ "EXCEPTION_CREATED_DATE", "CLEARING_DATE","CREATED_DATE","TRADE_DATE","UPDATED_DATE","RECORD_END_DATE","EXCEPTION_COMPLETION_DATE","EXCEPTION_PROCESSING_DATE"]
                    df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                             dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                    for i in parseDates:
                        if i in df_ex.columns:
                            df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                            df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                              table_name="exception_master", connection=recon_con)
                for i in oldData_ex:
                    if type(exallocation_data) == dict:
                        exallocation_data = exallocation_data
                    else:
                        exallocation_data = json.loads(exallocation_data)[0]
                    # exallocation_data = json.loads(exallocation_data)[0]
                    exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                    exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                    exallocation_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    exallocation_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                    exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "OPEN"
                    exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                    exallocation_data["IPADDRESS"] = "192.168.2.248"
                    exallocation_data["RECORD_STATUS"] = "ACTIVE"
                    exallocation_data["RECORD_VERSION"] =  1
                    exallocation_data["EXCEPTION_ID"] = i["EXCEPTION_ID"]
                    exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                    oldData_allocation.append(exallocation_data)
                    parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                    df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                             dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                    'EXCEPTION_ALLOCATION_OID': 'int64'})
                    for i in parseDates:
                        if i in df_al.columns:
                            df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                            df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                                table_name="exception_allocation", connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                return True, 'Success'
            else:
                return False, ''
        else:
            return False,"No Records to Ungroup"

    def authorizeRecords(self,id,query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status,recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID","RECON_EXECUTION_ID"]
            columnsToConvertAllocation = [ "EXCEPTION_ID","EXCEPTION_ALLOCATION_ID","EXCEPTION_ALLOCATION_OID"]
            (status_exception,exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = exmaster_data.to_json(orient='records')
                exmaster_data = json.loads(exmaster_data)[0]
            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query['selected_rec'][0], connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                logger.info(exallocation_data)
                exallocation_data = exallocation_data.to_json(orient='records')
                exallocation_data = json.loads(exallocation_data)[0]
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    # rec['MATCHING_EXECUTION_STATE'] = "AUTHORIZED"
                    # rec['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    # rec['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    newData.append(rec)
                (status_up,upData) = sql.updateSqlData(query=newData,connection=recon_con)
                # linkid = str(uuid.uuid4().int & (1<<64)-1)
                # logger.info("linkiddddddddddddd" + str(linkid))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k,v in format_type.items():
                                for key,val in rec_old.items():
                                    if k == key and (v == "DATE" or v =="TIMESTAMP"):
                                        if val is not None:
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp((rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "AUTHORIZED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "AUTHORIZED"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID']=  str(uuid.uuid4().int & (1<<64)-1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    logger.info("Object OID ****************" + rec_old["OBJECT_OID"])
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData,default=json_util.default),orient='columns',dtype={'OBJECT_OID':'int64','LINK_ID':'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                logger.info("Data frame of cash_output_txn object Oid" + str(df_cash['OBJECT_OID']))
                logger.info("Data frame of cash_output_txn link id" + str(df_cash['LINK_ID']))
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",connection=recon_con)

                exceptionData = dict()
                exceptionData["EXCEPTION_ID"] = exmaster_data['EXCEPTION_ID']
                exceptionData["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exceptionData['CLEARING_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["COMMENTS"] = query['comments']
                exceptionData["EXCEPTION_STATUS"] = "CLOSED"
                exceptionData["IPADDRESS"] = "192.168.2.248"
                exceptionData["CREATED_BY"] = flask.session["sessionData"]['userName']
                exceptionData["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exceptionData["LINK_ID"] = oldData[0]['LINK_ID']
                exceptionData["REASON_CODE"] = query['reason_code']
                if "RECON_EXECUTION_ID" in oldData[0]:
                    # print oldData[0]["RECON_EXECUTION_ID"]
                    exceptionData["RECON_EXECUTION_ID"] = oldData[0]["RECON_EXECUTION_ID"]
                exceptionData["RECON_ID"] = oldData[0]["RECON_ID"]
                exceptionData["RECORD_STATUS"] = "ACTIVE"
                exceptionData["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
                exceptionData["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_ex.append(exceptionData)
                parseDates = ["CREATED_DATE","EXCEPTION_CREATED_DATE","CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default),orient='columns',dtype={'EXCEPTION_OID':'int64','EXCEPTION_ID':'int64','LINK_ID':'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",connection=recon_con)
                exception_alloc_data = dict()
                exception_alloc_data["ACTION_BY"] = flask.session['sessionData']['userName']
                exception_alloc_data["ALLOCATED_BY"] = flask.session['sessionData']['userName']
                exception_alloc_data["ALLOCATION_REMARKS"] = query["comments"]
                exception_alloc_data["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exception_alloc_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exception_alloc_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exception_alloc_data["EXCEPTION_ALLOCATION_ID"] = exallocation_data['EXCEPTION_ALLOCATION_ID']
                exception_alloc_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exception_alloc_data["EXCEPTION_ID"] = exmaster_data["EXCEPTION_ID"]
                exception_alloc_data['COMPLETION_DATE_TIME'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exception_alloc_data["EXCEPTION_RESOLUTION_STATUS"] = "CLOSED"
                exception_alloc_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exception_alloc_data["RECON_ID"] = oldData[0]["RECON_ID"]
                exception_alloc_data["RECORD_STATUS"] = "ACTIVE"
                exception_alloc_data["IPADDRESS"] = "192.168.2.248"
                exception_alloc_data["RECORD_VERSION"] = exallocation_data['RECORD_VERSION'] + 1
                exception_alloc_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                # exception_alloc_data["USER_POOL_ID"] = ""
                exception_alloc_data["WORK_POOL_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                oldData_allocation.append(exception_alloc_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default),orient='columns',dtype={'EXCEPTION_ALLOCATION_ID':'int64','EXCEPTION_ID':'int64','EXCEPTION_ALLOCATION_OID':'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                # if not status_commit:
                #     logger.info("Not Committed ............................................")
                return True, 'Success'
            else:
                return False, ''
        else:
            return False,"No Records to authorize"

    def rejectRecords(self,id,query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status,recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID","RECON_EXECUTION_ID"]
            columnsToConvertAllocation = [ "EXCEPTION_ID","EXCEPTION_ALLOCATION_ID","EXCEPTION_ALLOCATION_OID"]
            (status_exception,exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = exmaster_data.to_json(orient='records')
                exmaster_data = json.loads(exmaster_data)[0]
            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(query=query['selected_rec'][0], connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                logger.info(exallocation_data)
                exallocation_data = exallocation_data.to_json(orient='records')
                exallocation_data = json.loads(exallocation_data)[0]
            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    # rec['MATCHING_EXECUTION_STATE'] = "AUTHORIZED"
                    # rec['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZED"
                    # rec['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZED"
                    newData.append(rec)
                (status_up,upData) = sql.updateSqlData(query=newData,connection=recon_con)
                # linkid = str(uuid.uuid4().int & (1<<64)-1)
                # logger.info("linkiddddddddddddd" + str(linkid))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k,v in format_type.items():
                                for key,val in rec_old.items():
                                    if k == key and (v == "DATE" or v =="TIMESTAMP"):
                                        if val is not None:
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp((rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "MANUAL_REJECTED"
                    rec_old['MATCHING_STATUS'] = "UNMATCHED"
                    rec_old['RECONCILIATION_STATUS'] = "EXCEPTION"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "AUTHORIZATION_REJECTION"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID']=  str(uuid.uuid4().int & (1<<64)-1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    logger.info("Object OID ****************" + rec_old["OBJECT_OID"])
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData,default=json_util.default),orient='columns',dtype={'OBJECT_OID':'int64','LINK_ID':'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                logger.info("Data frame of cash_output_txn object Oid" + str(df_cash['OBJECT_OID']))
                logger.info("Data frame of cash_output_txn link id" + str(df_cash['LINK_ID']))
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",connection=recon_con)

                exceptionData = dict()
                exceptionData["EXCEPTION_ID"] = exmaster_data['EXCEPTION_ID']
                exceptionData["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exceptionData['CLEARING_DATE'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["COMMENTS"] = query['comments']
                exceptionData["EXCEPTION_STATUS"] = "INPROGRESS"
                exceptionData["IPADDRESS"] = "192.168.2.248"
                exceptionData["CREATED_BY"] = flask.session["sessionData"]['userName']
                exceptionData["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exceptionData["EXCEPTION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exceptionData["LINK_ID"] = oldData[0]['LINK_ID']
                exceptionData["REASON_CODE"] = query['reason_code']
                if "RECON_EXECUTION_ID" in oldData[0]:
                    # print oldData[0]["RECON_EXECUTION_ID"]
                    exceptionData["RECON_EXECUTION_ID"] = oldData[0]["RECON_EXECUTION_ID"]
                exceptionData["RECON_ID"] = oldData[0]["RECON_ID"]
                exceptionData["RECORD_STATUS"] = "ACTIVE"
                exceptionData["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
                exceptionData["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_ex.append(exceptionData)
                parseDates = ["CREATED_DATE","EXCEPTION_CREATED_DATE","CLEARING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default),orient='columns',dtype={'EXCEPTION_OID':'int64','EXCEPTION_ID':'int64','LINK_ID':'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex, table_name="exception_master",connection=recon_con)
                exception_alloc_data = dict()
                exception_alloc_data["ACTION_BY"] = flask.session['sessionData']['userName']
                exception_alloc_data["ALLOCATED_BY"] = flask.session['sessionData']['userName']
                exception_alloc_data["ALLOCATION_REMARKS"] = query["comments"]
                exception_alloc_data["BUSINESS_CONTEXT_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                exception_alloc_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exception_alloc_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exception_alloc_data["EXCEPTION_ALLOCATION_ID"] = exallocation_data['EXCEPTION_ALLOCATION_ID']
                exception_alloc_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1<<64)-1)
                exception_alloc_data["EXCEPTION_ID"] = exmaster_data["EXCEPTION_ID"]
                exception_alloc_data["EXCEPTION_RESOLUTION_STATUS"] = "INPROGRESS"
                exception_alloc_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exception_alloc_data["RECON_ID"] = oldData[0]["RECON_ID"]
                exception_alloc_data["RECORD_STATUS"] = "ACTIVE"
                exception_alloc_data["IPADDRESS"] = "192.168.2.248"
                exception_alloc_data["RECORD_VERSION"] = exallocation_data['RECORD_VERSION'] + 1
                exception_alloc_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                # exception_alloc_data["USER_POOL_ID"] = ""
                exception_alloc_data["WORK_POOL_ID"] = oldData[0]["BUSINESS_CONTEXT_ID"]
                oldData_allocation.append(exception_alloc_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default),orient='columns',dtype={'EXCEPTION_ALLOCATION_ID':'int64','EXCEPTION_ID':'int64','EXCEPTION_ALLOCATION_OID':'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al, table_name="exception_allocation",connection=recon_con)
                if status_cash and status_master and status_allocation:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                # if not status_commit:
                #     logger.info("Not Committed ............................................")
                return True, 'Success'
            else:
                return False, ''
        else:
            return False,"No Records to authorize"

    def rollBack(self,id,query):
        newData = []
        cloned_val = query['selected_rec']
        oldData = []
        oldData_ex = []
        oldData_allocation = []

        if query is not None and query['selected_rec'] is not None:
            (status,recon_con) = sql.getConnection()

            #Validation for part match
            # To avoid [Python int too large to convert to C long] error
            part_match_frame = pandas.read_json(json.dumps(cloned_val, default=json_util.default), orient='columns',dtype={'LINK_ID': 'int64'})
            part_match_frame["LINK_ID"] = part_match_frame["LINK_ID"].astype("str")
            df_cash_aggr = pandas.DataFrame({'validation_count': part_match_frame.groupby("LINK_ID").size()}).reset_index()

            reconData_merge = pandas.merge(part_match_frame, df_cash_aggr, on="LINK_ID")
            reconData_merge["count_res"] = reconData_merge["count"] - reconData_merge["validation_count"]
            logger.info("*" * 10)
            part_match_count = np.count_nonzero(reconData_merge["count_res"])

            if status and part_match_count == 0:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session['sessionData']['userName']
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status_up,upData) = sql.updateSqlData(query=newData,connection=recon_con,rollBackRecordIndicator = True)
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k,v in format_type.items():
                                for key,val in rec_old.items():
                                    if k == key and (v == "DATE" or v =="TIMESTAMP"):
                                        if val is not None:
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp((rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "ROLLBACK_AUTH_WAITING"
                    rec_old['MATCHING_EXECUTION_STATUS'] = "WAITING FOR ROLBACK AUTH"
                    rec_old['MANUAL_MATCH_AUTHORIZATION'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old['AUTHORIZATION_BY'] = query['authorizer']
                    rec_old['FORCEMATCH_AUTHORIZATION'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old["REASON_CODE"] = query["reason_code"]

                    ## new added columns
                    rec_old['TXN_MATCHING_STATE_CODE'] = 18
                    rec_old['TXN_MATCHING_STATUS'] = "ROLLBACK_AUTHORIZATION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "ROLLBACK_AUTH_WAITING"

                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old["UPDATED_BY"] = None
                    rec_old["RECORD_VERSION"] = rec_old['RECORD_VERSION'] + 1
                    rec_old['OBJECT_OID']=  str(uuid.uuid4().int & (1<<64)-1)
                    rec_old['RECORD_STATUS'] = "ACTIVE"
                    logger.info("Object OID ****************" + rec_old["OBJECT_OID"])
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData,default=json_util.default),orient='columns',dtype={'OBJECT_OID':'int64','LINK_ID':'int64','RAW_LINK':'int64','PARTICIPATING_RECORD_TXN_ID':'int64','OBJECT_ID':'int64','PREV_LINK_ID':'int64'})
                # df_cash.to_csv('rollback.csv')
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                logger.info("Data frame of cash_output_txn object Oid" + str(df_cash['OBJECT_OID']))
                logger.info("Data frame of cash_output_txn link id" + str(df_cash['LINK_ID']))
                # forcing positions link id and prev_link_id to zero to avoid oralce numeric error
                df_cash["POSITIONS_LINK_ID"] = 0
                df_cash["PREV_LINK_ID"] = 0
                # dummy column used only during part match process
                del df_cash["count"]
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",connection=recon_con)
                if status_cash:
                    (status_commit,toCommit) = sql.commitToSql(connection=recon_con)
                # if not status_commit:
                #     logger.info("Not Committed ............................................")
                return True, 'Success'
            elif part_match_count > 0:
                logger.info("Partial roll back of records initiated!!!!")
                return True,"Partial Rollback is not allowed"
            else:
                return False, ''
        else:
            return False,"No Records to authorize"

    def authorizeRollBackRecords(self,id,query):
        # logger.info('query ----------------------------------' + str(query['selected_rec']))
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID"]

            # Retrive exception master details
            (status_exception, exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],
                                                                                  connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = exmaster_data.to_json(orient='records')

            # Retrieve exception allocatoin details
            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(
                query=query['selected_rec'][0], connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = exallocation_data.to_json(orient='records')
            # logger.info("exception_master" + exmaster_data)

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con)
                # linkid = uuid.uuid4().int & (1 << 64) - 1
                # logger.info("linkid  fif" + str(linkid))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k, v in format_type.items():
                                for key, val in rec_old.items():
                                    if k == key and (v == "DATE" or v == "TIMESTAMP"):
                                        if val is not None:
                                            print "key" + str(key)
                                            # print "val" + str(val)
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp(
                                                    (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                                            print rec_old[key]
                                            # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "FORCEMATCH_ROLLEDBACK"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "UNMATCHED "
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"

                    ## new added columns ( need to verify)
                    rec_old['TXN_MATCHING_STATE_CODE'] = 14
                    rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "SUBMISSION_PENDING"

                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RECONCILIATION_STATUS"] = "EXCEPTION"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["MATCHING_EXECUTION_STATUS"] = "OUTSTANDING"
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype={'OBJECT_OID': 'int64', 'LINK_ID': 'int64', 'RAW_LINK': 'int64',
                                                  'PARTICIPATING_RECORD_TXN_ID': 'int64', 'OBJECT_ID': 'int64',
                                                  'PREV_LINK_ID': 'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                             connection=recon_con)
                logger.info(10 * "*")
                logger.info(exmaster_data)
                exmaster_data = json.loads(exmaster_data)[0]
                print "*" * 100
                print exmaster_data
                print query['comments']
                exmaster_data["COMMENTS"] = query['comments']
                exmaster_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exmaster_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exmaster_data["EXCEPTION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exmaster_data["EXCEPTION_STATUS"] = "OPEN"
                exmaster_data["IPADDRESS"] = "192.168.2.248"
                exmaster_data["REASON_CODE"] = query['reason_code']
                exmaster_data["RECORD_STATUS"] = "ACTIVE"
                exmaster_data["RECORD_VERSION"] = exmaster_data["RECORD_VERSION"] + 1
                exmaster_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_ex.append(exmaster_data)
                parseDates = ["EXCEPTION_CREATED_DATE", "CLEARING_DATE", "CREATED_DATE", "TRADE_DATE", "UPDATED_DATE",
                              "RECORD_END_DATE", "EXCEPTION_COMPLETION_DATE", "EXCEPTION_PROCESSING_DATE"]
                df_ex = pandas.read_json(json.dumps(oldData_ex, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_OID': 'int64', 'EXCEPTION_ID': 'int64', 'LINK_ID': 'int64'})
                for i in parseDates:
                    if i in df_ex.columns:
                        df_ex[i] = pandas.to_datetime(df_ex[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_ex[i] = df_ex[i].fillna('')
                (status_master, ins_data_mas) = sql.insertDFToDB(df=df_ex,
                                                                 table_name="exception_master", connection=recon_con)
                exallocation_data = json.loads(exallocation_data)[0]
                exallocation_data["ALLOCATION_REMARKS"] = query["comments"]
                exallocation_data["CREATED_BY"] = flask.session["sessionData"]['userName']
                exallocation_data["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                exallocation_data["EXCEPTION_ALLOCATION_ID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exallocation_data["EXCEPTION_ALLOCATION_OID"] = str(uuid.uuid4().int & (1 << 64) - 1)
                exallocation_data["EXCEPTION_RESOLUTION_STATUS"] = "OPEN"
                exallocation_data["EXCP_RESL_REASON_CODE"] = query['reason_code']
                exallocation_data["IPADDRESS"] = "192.168.2.248"
                exallocation_data["RECORD_STATUS"] = "ACTIVE"
                exallocation_data["RECORD_VERSION"] = exallocation_data["RECORD_VERSION"] + 1
                exallocation_data["SESSION_ID"] = flask.session["sessionData"]["_id"]
                oldData_allocation.append(exallocation_data)
                parseDates = ["ALLOCATION_TIME", "CREATED_DATE"]
                df_al = pandas.read_json(json.dumps(oldData_allocation, default=json_util.default), orient='columns',
                                         dtype={'EXCEPTION_ALLOCATION_ID': 'int64', 'EXCEPTION_ID': 'int64',
                                                'EXCEPTION_ALLOCATION_OID': 'int64'})
                for i in parseDates:
                    if i in df_al.columns:
                        df_al[i] = pandas.to_datetime(df_al[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_al[i] = df_al[i].fillna('')
                (status_allocation, ins_data_alloc) = sql.insertDFToDB(df=df_al,
                                                                       table_name="exception_allocation",
                                                                       connection=recon_con)

                if status_cash and status_master and status_allocation:
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                dataToReturn['oldData_ex'] = oldData_ex
                dataToReturn['oldData_allocation'] = oldData_allocation
                return True, dataToReturn
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def rejectRollBackRecords(self,id, query):
        # logger.info('query ----------------------------------' + str(query['selected_rec']))
        # print query['comments']
        cloned_val = query['selected_rec']
        newData = []
        oldData = []
        oldData_ex = []
        oldData_allocation = []
        if query is not None and query['selected_rec'] is not None:
            (status, recon_con) = sql.getConnection()
            columnsToConvertMaster = ["LINK_ID", "EXCEPTION_ID", "EXCEPTION_OID", "RECON_EXECUTION_ID"]
            columnsToConvertAllocation = ["EXCEPTION_ID", "EXCEPTION_ALLOCATION_ID", "EXCEPTION_ALLOCATION_OID"]

            # Retrive exception master details
            (status_exception, exmaster_data) = sql.getExceptionMasterDetails(query=query['selected_rec'][0],
                                                                              connection=recon_con)
            if status_exception:
                for x in columnsToConvertMaster:
                    if x in exmaster_data.columns:
                        exmaster_data[x] = exmaster_data[x].apply(str, 1)
                exmaster_data = exmaster_data.to_json(orient='records')

            # Retrieve exception allocatoin details
            (status_allocation, exallocation_data) = sql.getExceptionAllocationDetails(
                query=query['selected_rec'][0], connection=recon_con)
            if status_allocation:
                for x in columnsToConvertAllocation:
                    if x in exallocation_data.columns:
                        exallocation_data[x] = exallocation_data[x].apply(str, 1)
                exallocation_data = exallocation_data.to_json(orient='records')
            # logger.info("exception_master" + exmaster_data)

            if status:
                for rec in query['selected_rec']:
                    rec['UPDATED_BY'] = flask.session["sessionData"]['userName']
                    # rec['UPDATED_DATE'] = datetime.datetime.fromtimestamp(utilities.getUtcTime()).strftime("%d-%b-%Y")
                    rec['RECORD_STATUS'] = "INACTIVE"
                    newData.append(rec)
                (status, updata) = sql.updateSqlData(query=newData, connection=recon_con, rollBackRecordIndicator = True)
                # linkid = uuid.uuid4().int & (1 << 64) - 1
                # logger.info("linkid  fif" + str(linkid))
                for rec_old in cloned_val:
                    if 'format' in flask.session['sessionData']:
                        for format_type in flask.session['sessionData']['format']:
                            for k, v in format_type.items():
                                for key, val in rec_old.items():
                                    if k == key and (v == "DATE" or v == "TIMESTAMP"):
                                        if val is not None:
                                            print "key" + str(key)
                                            # print "val" + str(val)
                                            if type(rec_old[key]) != str:
                                                rec_old[key] = datetime.datetime.fromtimestamp(
                                                    (rec_old[key]) / 1000).strftime("%d-%b-%Y")
                                            else:
                                                rec_old[key] = rec_old[key]
                                            print rec_old[key]
                                            # rec_old[key] = pandas.to_datetime(rec_old[key])
                    rec_old["CREATED_BY"] = flask.session["sessionData"]['userName']
                    rec_old["CREATED_DATE"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    rec_old['MATCHING_EXECUTION_STATE'] = "MANUAL_REJECTED_MATCHED_ROLLBACK"
                    rec_old['LINK_ID'] = rec_old['LINK_ID']
                    rec_old['OBJECT_OID'] = str(uuid.uuid4().int & (1 << 64) - 1)
                    rec_old["MATCHING_STATUS"] = "MATCHED"
                    rec_old["RECORD_VERSION"] = rec_old["RECORD_VERSION"] + 1
                    rec_old["RECORD_STATUS"] = "ACTIVE"

                    ## new added columns ( need to verify)
                    rec_old['TXN_MATCHING_STATE_CODE'] = 14
                    rec_old['TXN_MATCHING_STATUS'] = "AUTHORIZATION_SUBMISSION_PENDING"
                    rec_old['TXN_MATCHING_STATE'] = "SUBMISSION_PENDING"

                    rec_old["REASON_CODE"] = query["reason_code"]
                    rec_old["RECONCILIATION_STATUS"] = "RECONCILED"
                    rec_old["AUTHORIZATION_STATUS"] = "AUTHORIZED"
                    rec_old["MATCHING_EXECUTION_MODE"] = "MANUAL"
                    rec_old["FORCEMATCH_AUTHORIZATION"] = "AUTHORIZED"
                    oldData.append(rec_old)
                df_cash = pandas.read_json(json.dumps(oldData, default=json_util.default), orient='columns',
                                           dtype={'OBJECT_OID': 'int64', 'LINK_ID': 'int64', 'RAW_LINK': 'int64',
                                                  'PARTICIPATING_RECORD_TXN_ID': 'int64', 'OBJECT_ID': 'int64',
                                                  'PREV_LINK_ID': 'int64'})
                parseDates = []
                for k, v in datatypes.datatypes.items():
                    if v == "np.datetime64":
                        parseDates.append(k)
                for i in parseDates:
                    if i in df_cash.columns:
                        df_cash[i] = pandas.to_datetime(df_cash[i], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                        df_cash[i] = df_cash[i].fillna('')
                (status_cash, ins_data_cash) = sql.insertDFToDB(df=df_cash, table_name="cash_output_txn",
                                                                connection=recon_con)

                if status_cash :
                    (status_commit, toCommit) = sql.commitToSql(connection=recon_con)
                if not status_commit:
                    logger.info("Not Committed ............................................")
                dataToReturn = dict()
                dataToReturn['oldData'] = oldData
                dataToReturn['oldData_ex'] = oldData_ex
                dataToReturn['oldData_allocation'] = oldData_allocation
                return True, dataToReturn
            else:
                return False, ''
        else:
            return False, 'No records to Force Match'

    def getReconMatchedExecutionDate(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_execution_date(recon_con, query)

                # To avoid [Python int too large to convert to C long] error
                reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")

                json_out = reconData_merge.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatchedStatementDate(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_statement_date(recon_con, query)

                # To avoid [Python int too large to convert to C long] error
                reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")

                json_out = reconData.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getReconMatched_St_Ex_Date(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.read_col_query_matched_ex_st_date(recon_con, query)

                # To avoid [Python int too large to convert to C long] error
                reconData["LINK_ID"] = reconData["LINK_ID"].astype("str")
                df_cash_aggr = pandas.DataFrame({'count': reconData.groupby("LINK_ID").size()}).reset_index()
                reconData_merge = pandas.merge(reconData, df_cash_aggr, on="LINK_ID")

                json_out = reconData_merge.to_json(orient='records')
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getExceptionExecutionDate(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_execution_date(recon_con, query)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    json_out = reconData.to_json(orient='records')
                else:
                    json_out = str([])
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getExceptionStatementDate(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_statement_date(recon_con, query)
                # logger.info(len(reconData.index))
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    json_out = reconData.to_json(orient='records')
                else:
                    json_out = str([])
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getException_St_Ex_Date(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary_ex_st_date(recon_con, query)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    json_out = reconData.to_json(orient='records')
                else:
                    json_out = str([])                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getExecutionSummary(self,id,query):
        if query is not None:
            (status, recon_con) = sql.getConnection()
            if status:
                (status_rec, reconData) = sql.getExecutionSummary( query,recon_con)
                # logger.info(reconData)
                if len(reconData.index) > 0:
                    reconData = reconData.T.groupby(level=0).first().T
                    # logger.info(reconData)
                    json_out = reconData.to_json(orient='records')
                else:
                    json_out = str([])
                # print "json_out" + str(json_out)
                return status_rec, json_out
            else:
                return False, "No Data Found for this reconid"

    def getSummary(self,id,query):
        (status,totalData) = ReportInterface().queryData(levelkey='BUSINESS_CONTEXT_ID',filters={},datasetName='BusinessRecon',summary='get')
        # logger.info(status)
        # logger.info(totalData)
        # logger.info(totalData)
        return True,totalData

    def getSuspence(self,id,query):
        (status_sql,recon_con) = sql.getConnection()
        (status,suspData) = sql.read_col_query_unmatched_suspence(connection=recon_con,query={})
        if not suspData.empty:
            # logger.info(type(suspData['ACCOUNT_NUMBER']))
            # logger.info(suspData)
            # suspData['ACCOUNT_NUMBER_NEW'] = suspData['ACCOUNT_NUMBER'].apply(lambda x:str(x).lstrip('0'))
            # logger.info(suspData)
            conToDf = pandas.DataFrame()
            if status:
                (status_re,acData) = ReconAssignment().getAll({"perColConditions":{"user":flask.session['sessionData']['userName']}})
                # logger.info(acData)
                totalAc = []
                if len(acData['data']) > 0:
                    for r in acData['data']:
                        ac_codes = []
                        if 'accountPools' in r and 'accountPools' is not '':
                            if len(ac_codes) > 0:
                                ac_codes = ac_codes + r['accountPools'].split(',')
                            else:
                                if type(r['accountPools']) == str:
                                    ac_codes = r['accountPools'].split(',')
                    totalAc = ac_codes
                    # logger.info(totalAc)
                    totalDatatoDf = []
                    if len(totalAc) > 0:
                        for code in totalAc:
                            (status_an,data_an) = AccountPool().getAll({"perColConditions":{"ACCOUNT_POOL_CONTEXT_CODE":code}})
                            totalDatatoDf.append(data_an['data'])
                    # logger.info(len(totalDatatoDf[0]))
                    if len(totalDatatoDf) > 0:
                        conToDf = pandas.read_json(json.dumps(totalDatatoDf[0],default=json_util.default),orient='columns')
                    # logger.info(conToDf)
            # suspData['AC'] = 1
            # logger.info(conToDf)
            # suspData = pandas.merge(conToDf,suspData,on=["ACCOUNT_NUMBER"],how="inner")
            # suspData = suspData.join(conToDf,on=["ACCOUNT_NUMBER"],how="inner",lsuffix="_x")
            # conToDf.to_csv('condf.csv')
            # suspData.to_csv('susp.csv')
            suspData['COUNT'] = 1
            indexcolums = ['ACCOUNT_POOL_NAME','ACCOUNT_NUMBER','ACCOUNT_NAME','ACCOUNT_POOL_CONTEXT_CODE']
            suspData = pandas.pivot_table(suspData, index=indexcolums,columns='DEBIT_CREDIT_INDICATOR',
                                                values=['AMOUNT', "COUNT"],
                                               aggfunc={"AMOUNT": np.sum, "COUNT": len},fill_value=0)
            # suspData = suspData.groupby(['ACCOUNT_POOL_NAME','ACCOUNT_NUMBER'],as_index=False).agg({"AC":len})
            # logger.info(suspData)
            # logger.info(suspData.columns)
            suspData.columns = [''.join(list(col)) for col in suspData.columns.values]
            # suspData =suspData.unstack(level=-1)
            suspData = suspData.reset_index()
            if not conToDf.empty:
                suspData = pandas.merge(conToDf,suspData,on=["ACCOUNT_NAME"],how="left")
            # logger.info(suspData)
            dfToSend = pandas.DataFrame()
            if 'ACCOUNT_POOL_NAME_x' in suspData.columns:
                dfToSend['ACCOUNT_POOL_NAME'] = suspData['ACCOUNT_POOL_NAME_x']
            else:
                dfToSend['ACCOUNT_POOL_NAME'] = suspData['ACCOUNT_POOL_NAME']
            if 'ACCOUNT_POOL_CONTEXT_CODE_x' in suspData.columns:
                dfToSend['ACCOUNT_POOL_CONTEXT_CODE'] = suspData['ACCOUNT_POOL_CONTEXT_CODE_x']
            else:
                dfToSend['ACCOUNT_POOL_CONTEXT_CODE'] = suspData['ACCOUNT_POOL_CONTEXT_CODE']
            if 'ACCOUNT_NUMBER_x' in suspData.columns:
                dfToSend['ACCOUNT_NUMBER'] = suspData['ACCOUNT_NUMBER_x']
            else:
                dfToSend['ACCOUNT_NUMBER'] = suspData['ACCOUNT_NUMBER']
            dfToSend['ACCOUNT_NAME'] = suspData['ACCOUNT_NAME']
            dfToSend['CREDIT_COUNT'] = suspData['COUNTC']
            dfToSend['DEBIT_COUNT'] = suspData['COUNTD']
            dfToSend['CREDIT_AMOUNT'] = suspData['AMOUNTC']
            dfToSend['DEBIT_AMOUNT'] = suspData['AMOUNTD']
            dfToSend.drop_duplicates(['ACCOUNT_NUMBER'], keep='last')
            return True,dfToSend.to_json(orient="records")
        else:
            return True,'[]'


class ReportInterface():
    def __init__(self):
        # self.ip="192.168.2.129"
        # self.port=1521
        # self.sid='xe'
        # self.oradsn=cx_Oracle.makedsn(self.ip, self.port, self.sid)
        # self.oraconnection=cx_Oracle.connect('algoreconutil_dev', 'algorecon', self.oradsn)
        # self.metadata={"tree":["BUSINESS_CONTEXT_ID","RECON_ID","SOURCE_TYPE","RECONCILIATION_STATUS"],"aggregation":["COUNT","CREDIT","DEBIT","NETOUTSTANDINGAMOUNT"]}
        # self.df=None
        self.statesdf = pandas.read_csv("meta/states.csv")

    def getBusinessContextPerUser(self,id):
        (status_b,data_d) = ReconAssignment().getAll({'perColConditions':{'user':flask.session['sessionData']['userName']}})
        bus_id = []
        rec_id = []
        if status_b and len(data_d['data']) > 0:
            logger.info(data_d['data'])
            for r in data_d['data']:
                bus_id.append(r['businessContext'])
                rec_id.append(r['recons'].split(','))
            logger.info('bus ------------' + str(bus_id))
            # logger.info('recons --------' + str(rec_id))
            logger.info('recons --------' + str(np.array(np.concatenate( rec_id, axis=0 ))))
            if len(bus_id) > 0:
                joined_ids = "','".join(bus_id)
                joined_rec_ids = "','".join(np.concatenate( rec_id, axis=0 ))
                logger.info("and business_context_id in ('"+joined_ids+"')")
                logger.info("and recc in ('"+joined_rec_ids+"')")
                if id == "join":
                    return True," and wo.business_context_id in ('"+joined_ids+"') and rd.recon_id in ('"+joined_rec_ids+"')"," and txn.business_context_id in ('"+joined_ids+"') and txn.recon_id in ('"+joined_rec_ids+"')"
                else:
                    return True," and business_context_id in ('"+joined_ids+"') and recon_id in ('"+joined_rec_ids+"')"," and txn.business_context_id in ('"+joined_ids+"') and txn.recon_id in ('"+joined_rec_ids+"')"
            else:
                return True,''
        else:
            return False,''

    def queryData(self,levelkey,filters,datasetName,summary = None):
        filterstr=""
        df = None
        if datasetName is not None:
            metadata=json.load(open("meta/"+datasetName+".json"))
            (status, recon_con) = sql.getConnection()
            if status:
                (concat_str,concat_data,concat_data_ex) = self.getBusinessContextPerUser('dummy')
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                logger.info('queryyyyyyyyyyyyyyyyyyyyyyyyyyyy' + str(query))
                df = pandas.read_sql(query,recon_con)
                # logger.info("df" + str(df))
                if summary is not None:
                    if "TXN_MATCHING_STATUS" in df.columns:
                        del df['TXN_MATCHING_STATUS']
                df = pandas.merge(df,self.statesdf,on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS", "AUTHORIZATION_STATUS",
                         "FORCEMATCH_AUTHORIZATION"],how="left")

        for key,val in filters.items():
            if len(filterstr)>1:
                filterstr+= ' & '
            if type(val)==str:
                val="\""+val+"\""
            else:
                val=str(val)
            filterstr="(df[\""+key+"\"]=="+val+")"
        filteredData= df
        # logger.info(filteredData)
        if len(filterstr)>1:
            #x="self.df["+filters+"]"
            filteredData=filteredData[eval(filterstr)]

        x=dict()
        x["df"]=filteredData
        x["levelkey"]=levelkey
        if summary is not None:
            # filterData = filteredData.to_json(orient="records")
            x['levelkey'] = "Summary"
            filteredData=eval(metadata["customMethod"]+"(x,metadata = metadata)")
            return True,filteredData
        else:
            filteredData=eval(metadata["customMethod"]+"(x,metadata = metadata)")
            return True,filteredData


    def makehighchartsjson(df, columnHierarchy):
        output = dict()
        columns = df.columns
        for index, row in df.iterrows():
            temp = output
            for col in columnHierarchy:
                # print row[col]
                if row[col] not in temp:
                    temp[row[col]] = dict()
                temp = temp[row[col]]
            index = 0
            for col in columns:
                if col in columnHierarchy:
                    continue
                else:
                    temp[col] = row[col]
        return output

    def getAgingReport(self,bucketsize,datasetName):
        df = None
        # statesdf=pandas.read_csv("meta/states.csv")
        if datasetName is not None:
            metadata=json.load(open("meta/"+datasetName+".json"))
            (status, recon_con) = sql.getConnection()
            (concat_str,concat_data,concat_data_ex) = self.getBusinessContextPerUser('dummy')
            if status:
                if concat_str and datasetName == "BusinessRecon":
                    query = metadata['query'] + concat_data
                elif concat_str and datasetName == 'ExceptionReport':
                    query = metadata['query'] + concat_data_ex
                else:
                    query = metadata['query']
                df = pandas.read_sql(query,recon_con)
                # logger.info("df" + str(df))
                df = pandas.merge(df,self.statesdf,on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS", "AUTHORIZATION_STATUS",
                         "FORCEMATCH_AUTHORIZATION"],how="left")
        records=df[["STATEMENT_DATE","AMOUNT"]].dropna(subset=["STATEMENT_DATE"])
        records["TRANSACTIONAGE"]=records["STATEMENT_DATE"].apply(lambda x:(datetime.datetime.now()-x).days)
        records["BUCKET"]=records["TRANSACTIONAGE"]-(records["TRANSACTIONAGE"]%int(bucketsize))
        records[["BUCKET","TRANSACTIONAGE","STATEMENT_DATE","AMOUNT"]]
        records["COUNT"]=1
        x= records[["BUCKET","COUNT","AMOUNT"]].groupby("BUCKET").sum().reset_index()
        y=json.loads(x.to_json(orient="records"))
        for x in range(0,len(y)):
            if x+1 < len(y):
                y[x]["RANGE"]=str(y[x]["BUCKET"])+"-"+str(y[x]["BUCKET"]+int(bucketsize))
            else:
                y[x]["RANGE"]=">"+str(y[x]["BUCKET"])
        print y
        return True,y


    def customProcess(self,data,metadata = None):
        df=data["df"]
        # logger.info(df)

        levelkey=data["levelkey"]

        columns=[]
        indexcolums=[]
        for key in metadata["tree"]:
            if key==levelkey:
                columns.append(key)
                indexcolums.append(key)
                break
            else:
                indexcolums.append(key)
                columns.append(key)
        # print df.columns
        # if levelkey == "Summary":
            # indexcolums.append("BUSINESS_CONTEXT_ID")
            # indexcolums.append("RECON_ID")
            # indexcolums.append("TXN_MATCHING_STATUS")
        columns.append("DEBIT_CREDIT_INDICATOR")
        columns.append("AMOUNT")
        # logger.info(df)

        # df1.to_csv("dataii.csv")
        # if levelkey == "Summary":
        #     columns.append('TXN_MATCHING_STATUS_y')
        #     df['TXN_MATCHING_STATUS'] = ''
        dfOutstanding = df[columns]
        # logger.info(dfOutstanding)
        dfOutstanding["COUNT"] = 1
        # logger.info( dfOutstanding)
        # logger.info(indexcolums)
        if 'SOURCE_NAME' in indexcolums:
            indexcolums.remove('SOURCE_NAME')
        if 'ACCOUNT_NAME' in indexcolums:
            indexcolums.remove('ACCOUNT_NAME')
        dfOutstanding = pandas.pivot_table(dfOutstanding, index=indexcolums,
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len},fill_value=0)

        # logger.info(dfOutstanding)
        # dfOutstanding.to_csv("dataamount.csv")
        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"]=dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"]=0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"]=dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"]=0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0
        #df["UNKNOWN"]=dfOutstanding["AMOUNT"][" "]
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]

        dfOutstanding=dfOutstanding.drop("AMOUNT",axis=1)
        # logger.info( dfOutstanding.columns)
        # logger.info(dfOutstanding)
        if levelkey != "Summary":
            dfOutstanding.columns= dfOutstanding.columns.droplevel(1)
            dfOutstanding.reset_index(inplace=True)
            dfOutstanding=dfOutstanding[[indexcolums[-1]]+["CREDIT","DEBIT","CREDITCOUNT","DEBITCOUNT","NETOUTSTANDINGAMOUNT"]]
            #del dfOutstanding[""]["DEBIT_CREDIT_INDICATOR"]
            #dfOutstanding.reset_index(inplace=True)
            #dfOutstanding.dropna(how='all',inplace=True)dfOutstanding.columns = dfOutstanding.columns.droplevel(1)
            dfOutstanding.set_index(indexcolums[-1])
        else:
            dfOutstanding = dfOutstanding.unstack(level = -1)
            dfOutstanding = dfOutstanding.reset_index()
        # logger.info(dfOutstanding)
        (status, recon_con) = sql.getConnection()
        # logger.info(levelkey)
        # logger.info(dfOutstanding)
        if levelkey == "BUSINESS_CONTEXT_ID":
            df2 = pandas.read_sql("select workpool_name,business_context_id from workpool where record_status ='ACTIVE'",recon_con)
            dfOutstanding = pandas.merge(dfOutstanding,df2,on=["BUSINESS_CONTEXT_ID"],how="left")
            # logger.info(dfOutstanding)
        if levelkey == "RECON_ID":
            df2 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",recon_con)
            dfOutstanding = pandas.merge(dfOutstanding,df2,on=["RECON_ID"],how="left")
        if levelkey == "Summary":
            df1 =df
            df1["AC"] = 1
            df1 = df1.groupby(['RECON_ID','BUSINESS_CONTEXT_ID','TXN_MATCHING_STATUS'],as_index=False).agg({"AC":len})
            df1 = pandas.merge(df1,df,on=["RECON_ID","BUSINESS_CONTEXT_ID"],how="left")
            # logger.info(df1)
            df_unique = pandas.DataFrame()
            df_unique['RECON_ID'] = df1['RECON_ID']
            df_unique['BUSINESS_CONTEXT_ID'] = df1['BUSINESS_CONTEXT_ID']
            df_unique['TXN_MATCHING_STATUS'] = df1['TXN_MATCHING_STATUS_x']
            df_unique['AC'] = df1['AC_x']
            df_unique['RECON_EXECUTION_ID'] = df1['RECON_EXECUTION_ID']
            df_unique['STATEMENT_DATE'] = df1['STATEMENT_DATE']
            df_unique['EXECUTION_DATE_TIME'] = df1['EXECUTION_DATE_TIME']
            df_unique = df_unique.drop_duplicates(subset=['RECON_ID','TXN_MATCHING_STATUS'])


            # logger.info(df1.columns)
            # logger.info(df1)
            # df_unique = df_unique.set_index(['RECON_ID','BUSINESS_CONTEXT_ID','AC','TXN_MATCHING_STATUS','RECON_EXECUTION_ID','EXECUTION_DATE_TIME','STATEMENT_DATE'])
            # df_unique['TXN_MATCHING_STATUS_x_duplicate'] = ''
            # logger.info(df_unique)
            # df_unique = df_unique.pivot(index='TXN_MATCHING_STATUS_x_duplicate', columns='TXN_MATCHING_STATUS', values='AC')
            # df_unique.to_csv('summary.csv',index=False)

            df1 = pandas.pivot_table(df_unique, index=['RECON_ID','BUSINESS_CONTEXT_ID','TXN_MATCHING_STATUS'],
                                               values='AC',
                                               fill_value=0)
            df1 = df1.unstack(level=2)

            dfOutstanding.columns = [''.join(list(col)) for col in dfOutstanding.columns.values]
            # logger.info(dfOutstanding.columns.values)
            df1 = df1.reset_index()
            # logger.info(df1['RECON_ID'])
            # df_exe = pandas.DataFrame()
            # df_exe['RECON_ID'] = df_unique['RECON_ID']
            # df_exe['RECON_EXECUTION_ID'] = df_unique['RECON_EXECUTION_ID']
            # df_exe = df_exe.drop_duplicates(subset=['RECON_ID'])
            # logger.info(df_exe)

            # df1 = pandas.merge
            # logger.info(df1.columns.values)

            # dfOutstanding.to_csv('dat21.csv',index=False)
            # df1.to_csv('data1.csv',index=False)
            # df1 = pandas.merge(df,df1,on=["RECON_ID"],how="left")
            # logger.info(df1)
            # logger.info(dfOutstanding)

            dfOutstanding = pandas.merge(dfOutstanding,df1,on=['RECON_ID'],how="left")
            # logger.info(dfOutstanding.columns.values)
            # if ""
            # dfOutstanding.to_csv("data.csv")
            # logger.info(dfOutstanding)
            dfTOSend = pandas.DataFrame()
            dfTOSend['BUSINESS_CONTEXT_ID'] = dfOutstanding['BUSINESS_CONTEXT_ID_y']
            dfTOSend['RECON_ID'] = dfOutstanding['RECON_ID']
            if 'COUNTCAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_AUTHORIZATION_PENDING'] = dfOutstanding['COUNTCAUTHORIZATION_PENDING']
            if 'COUNTCAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_AUTOMATCHED'] = dfOutstanding['COUNTCAUTOMATCHED']
            if 'COUNTCUNMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_COUNT_UNMATCHED'] = dfOutstanding['COUNTCUNMATCHED']
            if 'COUNTDAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_AUTHORIZATION_PENDING'] = dfOutstanding['COUNTDAUTHORIZATION_PENDING']
            if 'COUNTDAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_AUTOMATCHED'] = dfOutstanding['COUNTDAUTOMATCHED']
            if 'COUNTDUNMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_COUNT_UNMATCHED'] = dfOutstanding['COUNTDUNMATCHED']
            if 'CREDITAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['CREDIT_AUTHORIZATION_PENDING'] = dfOutstanding['CREDITAUTHORIZATION_PENDING']
            if 'CREDITAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_AUTOMATCHED'] = dfOutstanding['CREDITAUTOMATCHED']
            if 'DEBITUNMATCHED' in dfOutstanding.columns:
                dfTOSend['CREDIT_UNMATCHED'] = dfOutstanding['DEBITUNMATCHED']
            if 'DEBITAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['DEBIT_AUTHORIZATION_PENDING'] = dfOutstanding['DEBITAUTHORIZATION_PENDING']
            if 'DEBITAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_AUTOMATCHED'] = dfOutstanding['DEBITAUTOMATCHED']
            if 'DEBITUNMATCHED' in dfOutstanding.columns:
                dfTOSend['DEBIT_UNMATCHED'] = dfOutstanding['DEBITUNMATCHED']
            if 'NETOUTSTANDINGAMOUNTAUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['NETOUTSTANDINGAMOUNT_AUTHORIZATION_PENDING'] = dfOutstanding['NETOUTSTANDINGAMOUNTAUTHORIZATION_PENDING']
            if 'NETOUTSTANDINGAMOUNTAUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['NETOUTSTANDINGAMOUNT_AUTOMATCHED'] = dfOutstanding['NETOUTSTANDINGAMOUNTAUTOMATCHED']
            if 'NETOUTSTANDINGAMOUNTUNMATCHED' in dfOutstanding.columns:
                dfTOSend['NETOUTSTANDINGAMOUNT_UNMATCHED'] = dfOutstanding['NETOUTSTANDINGAMOUNTUNMATCHED']
            # dfTOSend['STATEMENT_DATE'] = dfOutstanding['STATEMENT_DATE']
            # dfTOSend['EXECUTION_DATE_TIME'] = dfOutstanding['EXECUTION_DATE_TIME']
            if 'AUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['AUTHORIZATION_PENDING'] = dfOutstanding['AUTHORIZATION_PENDING']
            if 'AUTHORIZATION_SUBMISSION_PENDING' in dfOutstanding.columns:
                dfTOSend['AUTHORIZATION_SUBMISSION_PENDING'] = dfOutstanding['AUTHORIZATION_SUBMISSION_PENDING']
            if 'AUTOMATCHED' in dfOutstanding.columns:
                dfTOSend['AUTOMATCHED'] = dfOutstanding['AUTOMATCHED']
            if 'FORCE_MATCHED' in dfOutstanding.columns:
                dfTOSend['FORCE_MATCHED'] = dfOutstanding['FORCE_MATCHED']
            if 'ROLLBACK_AUTHORIZATION_PENDING' in dfOutstanding.columns:
                dfTOSend['ROLLBACK_AUTHORIZATION_PENDING'] = dfOutstanding['ROLLBACK_AUTHORIZATION_PENDING']
            if 'UNMATCHED' in dfOutstanding.columns:
                dfTOSend['UNMATCHED'] = dfOutstanding['UNMATCHED']
            # logger.info(dfTOSend)
            (concat_str,concat_data,concat_data_ex) = self.getBusinessContextPerUser('join')
            if concat_str:
                df2 = pandas.read_sql("SELECT wo.workpool_name, wo.business_context_id, rd.recon_name,rd.recon_id FROM workpool wo,recon_details rd where wo.business_context_id=rd.business_context_id and wo.record_status ='ACTIVE' and rd.record_status = 'ACTIVE'"+concat_data+"",recon_con)
            else:
                df2 = pandas.read_sql("SELECT wo.workpool_name, wo.business_context_id, rd.recon_name,rd.recon_id FROM workpool wo,recon_details rd where wo.business_context_id=rd.business_context_id and wo.record_status ='ACTIVE' and rd.record_status = 'ACTIVE'",recon_con)
            dfTOSend = pandas.merge(dfTOSend,df2,on=["BUSINESS_CONTEXT_ID","RECON_ID"],how="right")
            # logger.info(dfTOSend)
            if 'RECON_ID_y' in dfTOSend.columns:
                dfTOSend['RECON_ID'] = dfTOSend['RECON_ID_y']
            (concat_str,concat_data,concat_data_ex) = self.getBusinessContextPerUser('dummy')
            if concat_str:
                df3 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'"+concat_data+"",recon_con)
            else:
                df3 = pandas.read_sql("select recon_name,recon_id from recon_details where record_status ='ACTIVE'",recon_con)
            # logger.info(df3.columns)
            dfTOSend = pandas.merge(dfTOSend,df3,on=["RECON_ID"],how="left")
            logger.info(dfTOSend.columns)
            # logger.info(dfTOSend)
            recon_name = dfTOSend['RECON_NAME_x']
            dfTOSend.drop(labels=['RECON_NAME_x','RECON_NAME_y'], axis=1,inplace = True)
            # dfTOSend.drop(labels=['RECON_ID'], axis=1,inplace = True)
            dfTOSend.insert(0, 'RECON_NAME', recon_name)
            # dfTOSend = dfTOSend.drop_duplicates(subset=['RECON_NAME'], keep=False)
            # logger.info(dfTOSend)
            # dfTOSend = dfTOSend.drop_duplicates(['RECON_ID'], keep='last')
            return dfTOSend.to_json(orient="records")
            # dfOutstanding.columns= dfOutstanding.columns.droplevel(1)
            # dfOutstanding.reset_index(inplace=True)
        return dfOutstanding.to_json(orient="records")


    def getNetOutstandingAmount(self):
        df=self.queryToDataframe("select business_context_id, recon_id, recon_execution_id, statement_date, execution_date_time, source_type, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'")
        #print df
        #dfUnmatched=df.ix[(df["MATCHING_STATUS"]=="UNMATCHED") & (df["RECONCILIATION_STATUS"]=="EXCEPTION")]
        #dfUnmatched=dfUnmatched[["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        #dfUnmatched=dfUnmatched.groupby(["BUSINESS_CONTEXT_ID", "RECON_ID","SOURCE_TYPE","DEBIT_CREDIT_INDICATOR"]).sum()

        #print dfUnmatched.reset_index()

        #Business context level
        dfOutstanding=df[["BUSINESS_CONTEXT_ID","RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR","AMOUNT"]]
        dfOutstanding["COUNT"]=1
        #dfOutstanding=dfOutstanding.groupby(["BUSINESS_CONTEXT_ID", "RECONCILIATION_STATUS","DEBIT_CREDIT_INDICATOR"])
        dfOutstanding=pandas.pivot_table(dfOutstanding,index=['BUSINESS_CONTEXT_ID','RECONCILIATION_STATUS'], columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT',"COUNT"],aggfunc={"AMOUNT":np.sum,"COUNT":len}).reset_index()
        #dfOutstanding=dfOutstanding.sum()
        dfOutstanding["NETOUTSTANDINGAMOUNT"]=dfOutstanding["AMOUNT"]["D"]-dfOutstanding["AMOUNT"]["C"]
        print dfOutstanding

    # def getTransactionSummaryByDate(self,reconName=None):
    #     indexColumns=["RECON_NAME", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "DEBIT_CREDIT_INDICATOR","AMOUNT" ]
    #     (status, recon_con) = sql.getConnection()
    #     df = pandas.read_sql("select business_context_id, recon_id, recon_execution_id,account_number,account_name, statement_date, execution_date_time, source_type, source_name,txn_matching_status, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T'",recon_con)
    #     # df=self.df
    #     if reconName:
    #         df=df["RECON_NAME"==reconName]
    #         indexColumns.remove("RECON_NAME")
    #     summarizer = df[indexColumns]
    #        # ["RECON_NAME", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "AMOUNT", "DEBIT_CREDIT_INDICATOR"]]
    #     #summarizer=self.df[["RECON_NAME","STATEMENT_DATE","TXN_MATCHING_STATUS","AMOUNT","DEBIT_CREDIT_INDICATOR"]]
    #     summarizer=summarizer.groupBy(indexColumns[0:-1]).sum().reset_index()
    #     dfOutstanding = pandas.pivot_table(summarizer, index=[indexColumns[0:-2]],
    #                                        columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
    #                                        aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0)
    #
    #     if "C" in dfOutstanding["AMOUNT"]:
    #         dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
    #     else:
    #         dfOutstanding["CREDIT"] = 0
    #     if "D" in dfOutstanding["AMOUNT"]:
    #         dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
    #     else:
    #         dfOutstanding["DEBIT"] = 0
    #     if "C" in dfOutstanding["COUNT"]:
    #         dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
    #     else:
    #         dfOutstanding["CREDITCOUNT"] = 0
    #     if "D" in dfOutstanding["COUNT"]:
    #         dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
    #     else:
    #         dfOutstanding["DEBITCOUNT"] = 0
    #     # df["UNKNOWN"]=dfOutstanding["AMOUNT"][" "]
    #     dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
    #     dfOutstanding = dfOutstanding.drop("AMOUNT", axis=1, level=1)
    #     dfOutstanding.columns = dfOutstanding.columns.droplevel(1)
    #     print dfOutstanding.columns
    #     dfOutstanding=dfOutstanding[[indexColumns[0:-2]]+["CREDIT","DEBIT","CREDITCOUNT","DEBITCOUNT","NETOUTSTANDINGAMOUNT"]]
    #     dfOutstanding.reset_index(inplace=True)
    #     return True,dfOutstanding.to_json(orient="record")

    def getTransactionSummaryByDate(self,reconName=None):
        logger.info("Recon name passed is "+reconName)
        # reconName=None
        indexColumns=["RECON_ID", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "DEBIT_CREDIT_INDICATOR","AMOUNT" ]
        # df=self.df
        (status, recon_con) = sql.getConnection()
        (concat_str,concat_data,concat_data_ex) = self.getBusinessContextPerUser('dummy')
        df = pandas.read_sql("select business_context_id, recon_id, recon_execution_id,account_number,account_name, statement_date, execution_date_time, source_type, source_name, matching_status, reconciliation_status, authorization_status, forcematch_authorization, matching_execution_state, amount, debit_credit_indicator,raw_link from cash_output_txn where record_status='ACTIVE' and entry_type='T' "+concat_data,recon_con)
        #logger.info(df)
        # statesdf=pandas.read_csv("meta/states.csv")
        df=pandas.merge(df,self.statesdf,on=["MATCHING_EXECUTION_STATE", "MATCHING_STATUS", "RECONCILIATION_STATUS", "AUTHORIZATION_STATUS",
                         "FORCEMATCH_AUTHORIZATION"],how="left")
        #logger.info(df)
        if reconName and reconName!="dummy":
            df=df[df["RECON_ID"]==reconName]
            indexColumns.remove("RECON_ID")
        summarizer = df[indexColumns]
           # ["RECON_NAME", "STATEMENT_DATE", "TXN_MATCHING_STATUS", "AMOUNT", "DEBIT_CREDIT_INDICATOR"]]
        #summarizer=self.df[["RECON_NAME","STATEMENT_DATE","TXN_MATCHING_STATUS","AMOUNT","DEBIT_CREDIT_INDICATOR"]]
        #summarizer=summarizer.groupby(indexColumns[0:-1]).sum().reset_index()
        #print summarizer
        #summarizer = self.df[indexColumns]

        summarizer["COUNT"]=1
        logger.info(summarizer)
        dfOutstanding = pandas.pivot_table(summarizer, index=indexColumns[0:-2],
                                           columns='DEBIT_CREDIT_INDICATOR', values=['AMOUNT', "COUNT"],
                                           aggfunc={"AMOUNT": np.sum, "COUNT": len}, fill_value=0).reset_index()
        logger.info(dfOutstanding)
        presentColumns=[]
        if "C" in dfOutstanding["AMOUNT"]:
            dfOutstanding["CREDIT"] = dfOutstanding["AMOUNT"]["C"]
        else:
            dfOutstanding["CREDIT"] = 0
        if "D" in dfOutstanding["AMOUNT"]:
            dfOutstanding["DEBIT"] = dfOutstanding["AMOUNT"]["D"]
        else:
            dfOutstanding["DEBIT"] = 0
        if "C" in dfOutstanding["COUNT"]:
            dfOutstanding["CREDITCOUNT"] = dfOutstanding["COUNT"]["C"]
        else:
            dfOutstanding["CREDITCOUNT"] = 0
        if "D" in dfOutstanding["COUNT"]:
            dfOutstanding["DEBITCOUNT"] = dfOutstanding["COUNT"]["D"]
        else:
            dfOutstanding["DEBITCOUNT"] = 0
        dfOutstanding["NETOUTSTANDINGAMOUNT"] = dfOutstanding["DEBIT"] - dfOutstanding["CREDIT"]
        dfOutstanding["TOTALCOUNT"]=dfOutstanding["DEBITCOUNT"]+dfOutstanding["CREDITCOUNT"]
        dfOutstanding=dfOutstanding.drop("AMOUNT",axis=1)
        dfOutstanding.columns = dfOutstanding.columns.droplevel(1)

        dfOutstanding=dfOutstanding[indexColumns[0:-2]+["TOTALCOUNT","NETOUTSTANDINGAMOUNT"]]
        dfOutstanding.reset_index(inplace=True)
        logger.info(dfOutstanding.columns)
        return True,dfOutstanding.to_json(orient="records")

    def getCreditOutstandingAmount(self):
        pass

    def getDebitOutstandingAmount(self):
        pass

    def getAutoMatching(self):
        pass

    def getRollbackTransactionsWaitingForAuthorizations(self):
        pass

    def getOutstandingEntriesInOpenStatus(self):
        pass

    def getWaitingForInvestigationAndResolution(self):
        pass

    def getPendingForSubmission(self):
        pass

    def getWaitingForAuthorization(self):
        pass

# if __name__=='__main__':
#     r= ReportInterface("RECON")
#     r.loadDataSet("RECON")
#     status,df= r.queryData("RECON_ID",{"BUSINESS_CONTEXT_ID":"1111148147112"})
#     #del df["DEBIT_CREDIT_INDICATOR"]
#     #df=df.reset_index(level=0,drop=True)
#     print df
#     df.to_json("onerecon.json",orient="records")
