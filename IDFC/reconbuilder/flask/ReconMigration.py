import csv
from os import sys

import xlrd


def csv_from_excel():
    workbook = xlrd.open_workbook('KPTP_FWD_21062016.xls')
    all_worksheets = workbook.sheet_names()
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        your_csv_file = open(''.join([worksheet_name, '.csv']), 'wb')
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

        for rownum in xrange(worksheet.nrows):
            wr.writerow([unicode(entry).encode("utf-8") for entry in worksheet.row_values(rownum)])
        your_csv_file.close()


if __name__ == "__main__":
    csv_from_excel()

# class ReconMigration():
#     def __init__(self):
#         tmp = []
#         client = MongoClient('localhost:27017')
#         db = client.reconbuilder
#         recon_details = db.feedAccMapping.find()
#         for val in recon_details:
#             #print val['reconContext']['reconId']
#             for reconVal in db.recon_context_details.find({'reconId':val['reconContext']['reconId']}):
#                 str = ''
#                 str += '['
#                 fieldArray = []
#                 for sourceVal in reconVal['sources']:
#                     if sourceVal['applyAccountFilter']:
#                         fieldArray.append('{"feedName" : "' + db.feedDefination.find_one({'_id':bson.objectid.ObjectId(sourceVal['feedId'])})['feedName'] + '", "feedId" : "' + sourceVal['feedId'] + '"}')
#                 fieldArray = ', '.join(fieldArray)
#                 str += fieldArray + ']'
#
#                 print val['reconContext']['reconId']
#                 print str
#                 print ""
#                 data = data[(data['TXT_5_008'].str.startswith('N')) & ((data['TXT_5_006'] == 'OUT') | (data['TXT_5_006'] == 'O') | (data['TXT_5_006'] == 'Out')) & (data['TXT_32_005'] == 'RETU')]
#                 data = data[(data['TXT_5_008'].str.startswith('N')) & ((data['TXT_5_006'] == 'IN') | (data['TXT_5_006'] == 'I') | (data['TXT_5_006'] == 'In')) & (data['TXT_32_005'] == 'RVRS')]
#                 data = data[(data['TXT_5_008'].str.startswith('R')) & (data['TXT_5_006'] == 'O') & ((data['TXT_32_005'] == 'AUTH') | (data['TXT_32_005'] == 'WAIT') | (data['TXT_32_005'] == 'SPIR'))]
#                 data = data[(data['TXT_5_008'].str.startswith('R')) & ((data['TXT_5_006'] == 'OUT') | (data['TXT_5_006'] == 'O') | (data['TXT_5_006'] == 'Out')) & (data['TXT_32_005'] == 'RETU')]
#                 data = data[(data['TXT_5_008'].str.startswith('R')) & ((data['TXT_5_006'] == 'IN') | (data['TXT_5_006'] == 'I') | (data['TXT_5_006'] == 'In')) & (data['TXT_32_005'] == 'RVRS')]
#
#                 data = data[(data['TXT_180_001'].str.strip()=='Interbank') & (data['TXT_60_001'].str.strip()=='ForwardDeals')]
#
#     def strip(self,df):
#         columsn = df.columns
#         for val in columsn:
#             df[val] = df[val].str.strip()
#             df[df.dtype == object] = df[(df.dtype == object)].applymap(lambda x : str(x).strip())
#             df[df.dtype == object] = df[df.dtype == object].applymap(lambda x : x.replace('nan',np.NaN))
#
# if __name__ == '__main__':
#     reconJob = ReconMigration()
