import pandas
from pandas import DataFrame
import sys
from pymongo import MongoClient
import cx_Oracle

class Rollback():
    def __init__(self, recon_id):
        self.ip, self.port, self.SID, self.userName, self.pwd = '172.19.36.226', 1653, 'reconuat1', 'reconuat', 'IDfc$#321'
        self.engine_ip = ''
        self.erecon_ip = ''
        self.connection =self.get_connection()
        self.reconbuilder = self.getMongoConnection(self.engine_ip)['reconbuilder']
        self.erecon = self.getMongoConnection(self.erecon_ip)['erecon']
        self.recon_id = recon_id

    def get_connection(self):
        dsn_tns = cx_Oracle.makedsn(self.ip, self.port, self.SID)
        connection = cx_Oracle.connect(self.userName, self.pwd, dsn_tns)
        return connection

    def getMongoConnection(self, host_addr):
        mongo_client = MongoClient("mongodb://'{}':27017".format(host_addr))
        return mongo_client

    def rollbackData(self):
        self.reconbuilder['recon_context_details'].remove({'reconId':self.recon_id})
        print "Recon context details removed successfully .."
        self.reconbuilder['feedAccMapping'].remove({'reconContext.reconId':self.recon_id})
        print "Feed Account details removed successfully .."
        self.erecon['business'].remove({'RECON_ID':self.reconid})
        print "Business details removed successfully .."

        cur= self.connection.cursor()
        cur.execute("delete from recon_details where recon_id='"+ self.reconid + "' ")
        cur.execute("delete from recon_dynamic_data_model where recon_id='"+ self.reconid +"'")
        self.commitToSql()
        self.connection.close()
        print "Recon dynamic details removed successfully .."

    def commitToSql(self):
        self.connection.commit()

if __name__=='__main__':
    rb=Rollback()
    rb.rollbackData()
    print "Rollback done Successfully !!"
