import os
import pandas
import numpy
import re
import io
import zipfile
# from .. import config
import json
import shutil
import datetime


class ParseEj:
    def __init__(self,dayFilePath):
        self.tmpList = []
        self.dayFilePath = dayFilePath
        # with open('.RESP_CODE_DESCR.json', 'r') as f:
        #     self.responseCodeDescr = json.load(f)

    def parseEj(self, ticketDoc, cases):
        ejFiles = []
        filePath = '/tmp/' + datetime.datetime.now().strftime('%d%m%Y') + os.sep + ticketDoc['ticketId'] + os.sep
        for file in ticketDoc['attachments']:
            if file.split('.')[-1] == 'zip':
                newExtractFiles = self.unZipTheFolder(file, ticketDoc['ticketId'])
                ticketDoc['attachments'].extend(newExtractFiles)
                continue
            elif file.split('.')[-1] == 'txt':
                if 'FNKD' in file:
                    df = self.getFNKDTransaction(filePath + file)
                else:
                    df = self.getOtherTypes(filePath + file)
                # if len(df) > 0:
                #     if 'ATM ID' in df.columns:
                #         atmId = df['ATM ID'].unique()[0]

                    # for case in cases:
                    #     if str(atmId).index(case['reqiredKeys']['ATM ID']) != -1:
                    #         ejfolder = case['caseId'].split('.')[1]
                    #         ejFiles.append(filePath + os.sep + ejfolder + os.sep + file.split('.')[0] + '.csv')
                    #         df.to_csv(filePath + os.sep + ejfolder + os.sep + file.split('.')[0] + '.csv')
        # return ejFiles

    def unZipTheFolder(self, file, ticketId):
        with zipfile.ZipFile(self.dayFilePath + ticketId + os.sep + file, mode='r') as fileZip:
            fileZip.setpassword('axis')
            fileZip.extractall(self.dayFilePath + ticketId)
        newExtractFiles = []
        files = os.listdir(self.dayFilePath + ticketId)
        for i in files:
            if os.path.isdir(self.dayFilePath + ticketId + os.sep + i):
                for j in os.listdir(self.dayFilePath + ticketId + os.sep + i):
                    newExtractFiles.append(j)
                    shutil.move(self.dayFilePath + ticketId + os.sep + i + os.sep + j, self.dayFilePath + ticketId)
                    # if i.split('.')[1] != 'html' and i.split('.')[1] != 'csv':
            #     newExtractFiles.append(i)
        return newExtractFiles

    def getOtherTypes(self, f):
        self.complete = []
        self.tmpList = []
        fp = io.open(f, 'r', encoding='utf-8')
        lines = fp.readlines()
        start = False
        for index, line in enumerate(lines):
            try:
                if 'ATM ID' in line:
                    start = True
                    for i in range(1, 4):
                        if lines[index - i].strip().replace('\n', '') != '':
                            self.tmpList.append(lines[index - i])

                if 'BANK OF BARODA' in line:
                    self.tmpList.append(line)
                    if len(self.tmpList) > 1:
                        res = self.extractData(f)
                        self.complete.append(res)
                    self.tmpList = []
                    start = False

                if start and line.strip().replace('\n', '') != '':
                    self.tmpList.append(line)

                if 'TRANSACTION START' in line:
                    transactionStart = True
                    break;

            except Exception as e:
                print(e)
        # if transactionStart:
        #     df = self.getFNKDTransaction(f)
        #     return df
        df = pandas.DataFrame(self.complete)
        return df

    def getFNKDTransaction(self, f):
        self.complete = []
        self.tmpList = []
        fp = io.open(f, 'r', encoding='utf-8')
        lines = fp.readlines()
        start = False
        for index, line in enumerate(lines):
            try:

                if 'TRANSACTION START' in line:
                    start = True
                    if lines[index - 1].strip().replace('\n', '') != '':
                        self.tmpList.append(lines[index - 1])

                if 'TRANSACTION END' in line:
                    self.tmpList.append(line)
                    if len(self.tmpList) > 1:
                        res = self.extractData(f)
                        self.complete.append(res)
                    self.tmpList = []
                    start = False

                if start and line.strip().replace('\n', '') != '':
                    self.tmpList.append(line)
            except Exception as e:
                print(e)
        df = pandas.DataFrame(self.complete)
        return df

    def extractData(self, f):
        doc = {}
        for i, line in enumerate(self.tmpList):

            for type in ['ATM ID', 'REF NO', 'RRN NO', 'CARD NUMBER', 'ACCOUNT NO', 'RESP CODE', 'SEQ NO', 'RESP CDE',
                         'TERM.ID', 'CARD NO', 'RECORD NO','RESPONSE CODE']:
                if type in line:
                    doc[type] = re.search('(:[a-zA-z 0-9]*)', line).group().strip().replace(":", "") if re.search(
                        '(:[a-zA-z 0-9]*)', line) else ''
                    break

            for tran in ['BALANCE INQUIRY', 'CASH WITHDRAWAL']:
                if tran in line:
                    doc['TRAN_TYPE'] = tran
                    break

            for amount in ['AVAIL. BALANCE', 'TRANS AMOUNT']:
                if amount in line:
                    doc[amount] = re.search('(RS.[0-9. ]*)', line).group().replace("RS.", "").strip() if re.search(
                        '(RS.[0-9.]*)', line) else ''
                    break

            if 'RESP CODE' not in doc.keys() and 'TRANSACTION DECLINED' in line:
                doc['RESP CODE'] = self.tmpList[i + 1].strip()
                doc['REASON'] = self.tmpList[i + 2].strip()
                continue

            # if 'RESP CDE' in doc.keys():
            #     doc['ERROR CODE'] = self.responseCodeDescr.get(doc['RESP CDE'], '')

            if 'NOTES PRESENTED' in line:
                groups = re.findall('([0-9,]+)', line)
                if groups:
                    doc['NOTES PRESENTED'] = groups[-1]

            if 'CASH TOTAL' in line:
                self.getDispensedRecords(i, doc)

            if 'FNKD' in f:
                df = self.getFNKDDatef(line, doc)
                continue

            else:
                df = self.getOtherTypesDate(i, line, doc)

        self.tmpList = []
        return doc

    def getDispensedRecords(self, index, doc):
        for type in ['CASH TOTAL', 'DISPENSED', 'REJECTED', 'REMAINING']:
            for i in range(index, index + 5):
                if type in self.tmpList[i]:
                    doc[type] = ','.join(self.tmpList[i].split(' ')[-4:])

    def getOtherTypesDate(self, index, line, doc):
        if 'Date' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line):
            doc['Date'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line).group()
            if 'Location' not in doc.keys():
                doc['Location'] = self.tmpList[index + 1].strip().replace('BANK OF BARODA', '').strip()

    def getFNKDDatef(self, line, doc):
        if 'Date' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{4}[ *]*[0-9]{2}:[0-9]{2})', line):
            doc['Date'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{4}[ *]*[0-9]{2}:[0-9]{2})', line).group()

        if 'Location' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line):
            doc['Location'] = line.strip().replace('BANK OF BARODA', '').strip()

    def getCassetData(self, f):
        self.complete = []
        self.cdf = pandas.DataFrame()
        self.tmpList = []
        fp = io.open(f, 'r', encoding='utf-8')
        print(f)
        lines = fp.readlines()
        start = False
        for index, line in enumerate(lines):
            try:

                if 'SUPERVISOR MODE ENTRY' in line:
                    start = True
                    if line.strip().replace('\n', '') != '':
                        self.tmpList.append(line)

                if 'SUPERVISOR MODE EXIT' in line:
                    self.tmpList.append(line)
                    if len(self.tmpList) > 1:
                        res = self.extractCasseteData()
                        df = self.getDfFromDoc(res)
                        if len(df) > 0:
                            self.cdf = pandas.concat([self.cdf, df], join_axes=[df.columns])
                        self.complete.append(res)
                    self.tmpList = []
                    start = False

                if start and line.strip().replace('\n', '') != '':
                    self.tmpList.append(line)
            except Exception as e:
                print(e)
        return self.cdf

    def getDfFromDoc(self, doc):
        columns = ['CASH ADDED',
                   'CASH DISPENSED',
                   'DISPENSED',
                   'CASSETTE',
                   'REJECTED',
                   'CASH REMAINING', 'TOTAL',
                   'REMAINING', 'TYPE', 'LAST CLEARED', 'DATE-TIME']

        alldf = pandas.DataFrame(columns=columns)
        for data in doc:
            for type in ['TYPE1', 'TYPE2', 'TYPE3', 'TYPE4']:
                df = pandas.DataFrame([data[type]])
                df['TYPE'] = type
                df['LAST CLEARED'] = data['LAST CLEARED']
                df['DATE-TIME'] = data['DATE-TIME']
                alldf = pandas.concat([alldf, df], join_axes=[alldf.columns])
        return alldf

    def extractCasseteData(self):
        doc = []
        data = {}
        data['TYPE1'] = {}
        data['TYPE2'] = {}
        data['TYPE3'] = {}
        data['TYPE4'] = {}
        count = 0
        for i, line in enumerate(self.tmpList):
            if 'DATE-TIME' in line:
                if count == 1:
                    doc.append(data.copy())
                    data['TYPE1'] = {}
                    data['TYPE2'] = {}
                    data['TYPE3'] = {}
                    data['TYPE4'] = {}
                    count = 0
                data['DATE-TIME'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                              line).group() if re.search(
                    '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''
                count = count + 1

            if 'TYPE 1   TYPE 2' in line:

                for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                    for ldx in range(i + 1, i + 5):
                        if type in self.tmpList[ldx]:
                            groups = re.findall('([0-9]+)', self.tmpList[ldx])
                            data['TYPE1'][type] = groups[0]
                            data['TYPE2'][type] = groups[1]

            if 'TYPE 3   TYPE 4' in line:
                for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                    for ldx in range(i + 1, i + 6):
                        if type in self.tmpList[ldx]:
                            groups = re.findall('([0-9]+)', self.tmpList[ldx])
                            data['TYPE3'][type] = groups[0]
                            data['TYPE4'][type] = groups[1]

            for cashType in ['CASH DISPENSED', 'CASH REMAINING', 'CASH ADDED']:
                if cashType in line:
                    groups = re.findall('(=[ 0-9]*)', self.tmpList[i + 1])
                    if groups:
                        for ctype in ['TYPE1', 'TYPE2']:
                            if not cashType in data[ctype]:
                                data[ctype][cashType] = int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                    groups[1].replace('=', '').strip())
                            else:
                                data[ctype][cashType] = data[ctype][cashType] + int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                    groups[1].replace('=', '').strip())

                if cashType in line:
                    groups = re.findall('(=[ 0-9]*)', self.tmpList[i + 2])
                    if groups:
                        for ctype in ['TYPE3', 'TYPE4']:
                            if not cashType in data[ctype]:
                                data[ctype][cashType] = int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                    groups[1].replace('=', '').strip())
                            else:
                                data[ctype][cashType] = data[ctype][cashType] + int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                    groups[1].replace('=', '').strip())

            if 'LAST CLEARED' in line:
                data['LAST CLEARED'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                                 line).group() if re.search(
                    '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''

        return doc

# if __name__ == '__main__':
#     # files = os.listdir('/home/gumma/Downloads/RediffMail.111111563279554/ejfile')
#     for file1 in files:
#         df = ParseEj().parseEj('/home/gumma/Downloads/RediffMail.111111563279554/ejfile/' + file1)
#         df.to_csv('/tmp/' + file1 + '.csv',sep='|')
