# import pandas
# from bson import ObjectId
# import os
# import io
# import json
# import datetime
# import re
#
# with open('../JSON/ERROR_CODE.json', 'r') as js:
#     error = json.load(js)
#
#
# def parseEjFile(rawejfile, case):
#     investigationStatus = {}
#     a = io.open('/tmp/' + case['ticketId'] + os.sep + rawEjFile, 'r', encoding='utf-8')
#     lines = a.readlines()
#
#     tmpList = []
#     complete = []
#     start = False
#     for index, line in enumerate(lines):
#         line = lines[index - 1].strip().replace('\n', '').replace('\r', '').replace('\t', '')
#         if 'TRANSACTION START' in line:
#             if tmpList != []:
#                 complete.append(','.join(tmpList))
#                 # start = False
#                 tmpList = []
#
#             start = True
#             if lines[index - 1].strip().replace('\n', '') != '':
#                 reqLine = lines[index - 1].strip().replace('\n', '').replace('\r', '').replace('\t', '')
#                 tmpList.append(reqLine)
#                 continue
#
#         if 'TRANSACTION END' in line:
#             tmpList.append(line)
#             if len(tmpList) > 1:
#                 complete.append(','.join(tmpList))
#             tmpList = []
#             start = False
#
#         if line.strip().replace('\n', '') != '':
#             tmpList.append(line)
#
#     tranNumberFound = False
#     neighbourTranNo = {}
#     for index, tran in enumerate(complete):
#         # print(case['requiredKeys']['Transaction Number'])
#
#         if re.search("recordno:"+case['requiredKeys']['Transaction Number']+"|"+"recordno."+case['requiredKeys']['Transaction Number'],tran.lower().replace(' ','')):
#             tranNumberFound = True
#             tranFound = complete[index]
#             break
#         for i in range(1, 15):
#             pattern = 'recordno:' + str(int(case['requiredKeys']['Transaction Number']) - i)+ "|" +'recordno.' + str(int(case['requiredKeys']['Transaction Number']) - i)
#             pattern2 = 'recordno:' + str(int(case['requiredKeys']['Transaction Number']) + i)+ "|" +'recordno.' + str(int(case['requiredKeys']['Transaction Number']) + i)
#
#             if re.search(pattern, tran.replace(' ', '').lower()):
#                 neighbourTranNo[str(int(case['requiredKeys']['Transaction Number']) - i)] = str(-i) + '_' + str(index)
#
#             elif re.search(pattern2, tran.replace(' ', '').lower()):
#                 # print(str(int(case['requiredKeys']['Transaction Number']) - i))
#                 neighbourTranNo[str(int(case['requiredKeys']['Transaction Number']) + i)] = str(i) + '_' + str(index)
#
#     if not tranNumberFound:
#         investigationStatus['Missing Tran'] = True
#         investigationStatus['MachineStatus'] = ''
#         differences = neighbourTranNo.values()
#         differences = sorted(differences, reverse=False)
#
#         if len(differences) > 1:
#             # print(invsetigation)
#             try:
#                 for i in range(0, len(differences) - 1):
#                     if int(differences[i + 1].split('_')[1]) - int(differences[i].split('_')[1]) > 1 and \
#                             investigationStatus['MachineStatus'] == '':
#                         idxrange = int(differences[i + 1].split('_')[1]) - int(differences[i].split('_')[1])
#                         for idx in range(0, idxrange + 1):
#                             tran = complete[int(differences[i].split('_')[1]) + idx]
#
#                             if 'POWER-UP/RESET' in tran:
#                                 investigationStatus['EJ ERROR'] = 'POWER FAILURE'
#                                 break
#
#                             elif 'HOST TX TIMEOUT' in tran:
#                                 investigationStatus['EJ ERROR'] = 'HOST TX TIMEOUT'
#                                 break
#
#             except Exception as e:
#                 print("-" * 100)
#
#         elif len(differences) == 1:
#             for idx in range(0, int(differences[0].split('_')[0]) + 1):
#                 if (int(differences[0].split('_')[1]) + idx) < len(complete):
#                     try:
#                         tran = complete[int(differences[0].split('_')[1]) + idx]
#
#                         if 'POWER-UP/RESET' in tran:
#                             investigationStatus['EJ ERROR'] = 'POWER FAILURE'
#                             break
#
#                         elif 'HOST TX TIMEOUT' in tran:
#                             investigationStatus['EJ ERROR'] = 'HOST TX TIMEOUT'
#                             break
#                     except:
#                         print('')
#
#     elif tranNumberFound:
#         investigationStatus['tranFound'] = True
#         if "*e" in tranFound.lower() and "1*3" in tranFound.lower() and "m-13" in tranFound.lower():
#             investigationStatus['EJ ERROR'] = "*E/1*3"
#     return investigationStatus
#
#
# def verifyCBR(investigaion, case):
#     for i in os.listdir('/tmp/CBR/'):
#         if case['requiredKeys']['ATM ID'] in i:
#             if i.split('.')[1].lower() == 'csv':
#                 cbrData = pandas.read_csv('/tmp/CBR/' + i)
#             elif i.split('.')[1].lower() == 'xlsx':
#                 cbrData = pandas.read_excel('/tmp/CBR/' + i)
#
#             cbrData['Loading_Date'] = pandas.to_datetime(cbrData['Loading_Date'])
#             cbrData['caseDate'] = case['requiredKeys']['Transaction Date']
#             cbrData['caseDate'] = pandas.to_datetime(cbrData['caseDate'])
#             caseAtm = case['requiredKeys']['ATM ID']
#
#             cbrData['DIFF'] = cbrData['Loading_Date'] - cbrData['caseDate']
#             cbrData['DIFF'] = cbrData['DIFF'].dt.days
#             dates = cbrData.loc[cbrData['DIFF'] > 0, 'Loading_Date'].tolist()
#             twoEods = False
#             count = 0
#
#             for i in dates:
#                 specificData = cbrData.loc[
#                     (cbrData['ATM ID'].str.lower() == caseAtm.lower()) & ((cbrData['Loading_Date'] == i))]
#                 if len(specificData) > 0:
#                     if specificData['Cash Dispense Total'].values[0] > 0:
#                         count += 1
#                     specificData = specificData.to_dict(orient='records')[0]
#                     investigation['overageStatus'] = ''
#                     investigation['switchStatus'] = ''
#
#                     if specificData['Mid Cash Increase Total'] > specificData['Replenishment Total']:
#                         investigation['overageStatus'] = 'no'
#                         investigation['switchStatus'] = 'yes'
#                         investigation['midCashIncrease'] = str(i)
#                         if specificData['Overage Total'] != 0:
#                             investigation['overageStatus'] = 'yes'
#                         break;
#
#                     elif specificData['Mid Cash Increase Total'] == specificData['Replenishment Total']:
#                         if count == 2:
#                             investigation['overageStatus'] = 'no'
#                             investigation['switchStatus'] = 'no'
#                             if specificData['Overage Total'] != 0:
#                                 investigation['overageStatus'] = 'yes'
#                             break;
#                         continue
#
#                     elif specificData['Mid Cash Increase Total'] < specificData['Replenishment Total']:
#                         investigation['overageStatus'] = 'no'
#                         investigation['switchStatus'] = 'yes'
#                         if specificData['Overage Total'] != 0:
#                             investigation['overageStatus'] = 'yes'
#                         break;
#                     # elif len(specificData) > 0 and (specificData['Mid Cash Increase Total'] == specificData['Replenishment Total']):
#
#     return investigation
#
#
# cases = [{
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d45299f0e4e695258c4b355'),
#     'Rcvd-amt': '0',
#     'RRN': 919811180043,
#     'Time': '11:37o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.0',
#     '0': 1,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****3782',
#     'requiredKeys': {
#         'ATM ID': 'DLON1719',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '17-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '5000',
#         'Disputed Amount': '5000',
#         'Transaction Number': '180043',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b356'),
#     'Rcvd-amt': '0',
#     'RRN': 919914307801,
#     'Time': '14:58o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.1',
#     '0': 2,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****7063',
#     'requiredKeys': {
#         'ATM ID': 'BLOW9458',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '18-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '5000',
#         'Disputed Amount': '5000',
#         'Transaction Number': '307801',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b357'),
#     'Rcvd-amt': '0',
#     'RRN': 920119186488,
#     'Time': '19:30o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.2',
#     '0': 3,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6069****6043',
#     'requiredKeys': {
#         'ATM ID': 'MUOW1210',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1700',
#         'Disputed Amount': '1700',
#         'Transaction Number': '186488',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b358'),
#     'Rcvd-amt': '0',
#     'RRN': 920016177307,
#     'Time': '16:09o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.3',
#     '0': 4,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****7316',
#     'requiredKeys': {
#         'ATM ID': 'DLON1514',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '177307',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b359'),
#     'Rcvd-amt': '0',
#     'RRN': 919920243818,
#     'Time': '20:20o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.4',
#     '0': 5,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4598****3461',
#     'requiredKeys': {
#         'ATM ID': 'DLON9358',
#         'EJ Error': 'E*3/M-13',
#         'Transaction Date': '18-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1000',
#         'Disputed Amount': '1000',
#         'Transaction Number': '243818',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35a'),
#     'Rcvd-amt': '0',
#     'RRN': 920115271976,
#     'Time': '15:08o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.5',
#     '0': 6,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6072****4063',
#     'requiredKeys': {
#         'ATM ID': 'MUON9427',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '271976',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35b'),
#     'Rcvd-amt': '0',
#     'RRN': 920114769740,
#     'Time': '14:59o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.6',
#     '0': 7,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6072****4063',
#     'requiredKeys': {
#         'ATM ID': 'MUOW1045',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '769740',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35c'),
#     'Rcvd-amt': '0',
#     'RRN': 919313310887,
#     'Time': '13:45o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.7',
#     '0': 8,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6521****4233',
#     'requiredKeys': {
#         'ATM ID': 'UPOD9061',
#         'EJ Error': 'Incomplete ej',
#         'Transaction Date': '12-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '5000',
#         'Disputed Amount': '5000',
#         'Transaction Number': '310887',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35d'),
#     'Rcvd-amt': '0',
#     'RRN': 919811189025,
#     'Time': '11:51o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.8',
#     '0': 9,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4016****7396',
#     'requiredKeys': {
#         'ATM ID': 'CHOW1729',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '17-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '189025',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35e'),
#     'Rcvd-amt': '0',
#     'RRN': 919021411141,
#     'Time': '21:56o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.9',
#     '0': 10,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6075****6595',
#     'requiredKeys': {
#         'ATM ID': 'UPBN9299',
#         'EJ Error': 'E*3/M-18',
#         'Transaction Date': '09-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '8500',
#         'Disputed Amount': '8500',
#         'Transaction Number': '411141',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b35f'),
#     'Rcvd-amt': '0',
#     'RRN': 919705428294,
#     'Time': '5:09',
#     'CIT': 'CMS',
#     'caseId': '201908033188.10',
#     '0': 11,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****7118',
#     'requiredKeys': {
#         'ATM ID': 'HYON1630',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '16-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '400',
#         'Disputed Amount': '400',
#         'Transaction Number': '428294',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b360'),
#     'Rcvd-amt': '0',
#     'RRN': 919812227500,
#     'Time': '12:50o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.11',
#     '0': 12,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****1878',
#     'requiredKeys': {
#         'ATM ID': 'APOW9063',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '17-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '2000',
#         'Disputed Amount': '2000',
#         'Transaction Number': '227500',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b361'),
#     'Rcvd-amt': '0',
#     'RRN': 919914196197,
#     'Time': '14:54o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.12',
#     '0': 13,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6074****9070',
#     'requiredKeys': {
#         'ATM ID': 'DLON1731',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '18-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '200',
#         'Disputed Amount': '200',
#         'Transaction Number': '196197',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b362'),
#     'Rcvd-amt': '0',
#     'RRN': 920112370769,
#     'Time': '12:38o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.13',
#     '0': 14,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****6327',
#     'requiredKeys': {
#         'ATM ID': 'KNBW9115',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1000',
#         'Disputed Amount': '1000',
#         'Transaction Number': '370769',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b363'),
#     'Rcvd-amt': '0',
#     'RRN': 920117033861,
#     'Time': '17:57o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.14',
#     '0': 15,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6074****4069',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW774',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '33861o:p',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b364'),
#     'Rcvd-amt': '0',
#     'RRN': 920117033863,
#     'Time': '17:57o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.15',
#     '0': 16,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6074****4069',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW774',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '33863o:p',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b365'),
#     'Rcvd-amt': '0',
#     'RRN': 920114315514,
#     'Time': '14:34o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.16',
#     '0': 17,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****6620',
#     'requiredKeys': {
#         'ATM ID': 'UPOD9061',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '315514',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b366'),
#     'Rcvd-amt': '0',
#     'RRN': 920109423644,
#     'Time': '9:50',
#     'CIT': 'CMS',
#     'caseId': '201908033188.17',
#     '0': 18,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4173****0930',
#     'requiredKeys': {
#         'ATM ID': 'BLOD9455',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '423644',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b367'),
#     'Rcvd-amt': '8500',
#     'RRN': 920114814217,
#     'Time': '14:24o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.18',
#     '0': 19,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5192****9258',
#     'requiredKeys': {
#         'ATM ID': 'DLON1020',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '1500',
#         'Transaction Number': '814217',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b368'),
#     'Rcvd-amt': '0',
#     'RRN': 919920243817,
#     'Time': '20:12o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.19',
#     '0': 20,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6521****5666',
#     'requiredKeys': {
#         'ATM ID': 'DLON9358',
#         'EJ Error': 'E*3/M-13',
#         'Transaction Date': '18-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '9000',
#         'Disputed Amount': '9000',
#         'Transaction Number': '243817',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b369'),
#     'Rcvd-amt': '0',
#     'RRN': 920016392449,
#     'Time': '16:48o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.20',
#     '0': 21,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5177****9317',
#     'requiredKeys': {
#         'ATM ID': 'PUOD9378',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '300',
#         'Disputed Amount': '300',
#         'Transaction Number': '392449',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36a'),
#     'Rcvd-amt': '0',
#     'RRN': 920107140010,
#     'Time': '7:48',
#     'CIT': 'CMS',
#     'caseId': '201908033188.21',
#     '0': 22,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6069****1732',
#     'requiredKeys': {
#         'ATM ID': 'DLOW1181',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '140010',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36b'),
#     'Rcvd-amt': '0',
#     'RRN': 916411844339,
#     'Time': '11:24o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.22',
#     '0': 23,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****5155',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW734',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '13-Jun-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '844339',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36c'),
#     'Rcvd-amt': '0',
#     'RRN': 916411844341,
#     'Time': '11:25o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.23',
#     '0': 24,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****5155',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW734',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '13-Jun-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '844341',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36d'),
#     'Rcvd-amt': '0',
#     'RRN': 920215719423,
#     'Time': '15:57o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.24',
#     '0': 25,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****0253',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW889',
#         'EJ Error': 'successful txn',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '3500',
#         'Disputed Amount': '3500',
#         'Transaction Number': '719423',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36e'),
#     'Rcvd-amt': '0',
#     'RRN': 920020157342,
#     'Time': '20:07o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.25',
#     '0': 26,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6521****6230',
#     'requiredKeys': {
#         'ATM ID': 'HYOW1249',
#         'EJ Error': 'successful txn',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '157342',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b36f'),
#     'Rcvd-amt': '0',
#     'RRN': 920020157350,
#     'Time': '20:16o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.26',
#     '0': 27,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6521****6230',
#     'requiredKeys': {
#         'ATM ID': 'HYOW1249',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '157350',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b370'),
#     'Rcvd-amt': '0',
#     'RRN': 920217422562,
#     'Time': '17:32o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.27',
#     '0': 28,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5559****5346',
#     'requiredKeys': {
#         'ATM ID': 'BLOD9399',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1900',
#         'Disputed Amount': '1900',
#         'Transaction Number': '422562',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b371'),
#     'Rcvd-amt': '0',
#     'RRN': 910909107557,
#     'Time': '9:59',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.28',
#     '0': 29,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****7481',
#     'requiredKeys': {
#         'ATM ID': 'MUOW1210',
#         'EJ Error': 'successful txn',
#         'Transaction Date': '19-Apr-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '107557',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b372'),
#     'Rcvd-amt': '0',
#     'RRN': 919110142431,
#     'Time': '10:42o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.29',
#     '0': 30,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****2919',
#     'requiredKeys': {
#         'ATM ID': 'DLON1155',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '10-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '4000',
#         'Disputed Amount': '4000',
#         'Transaction Number': '142431',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b373'),
#     'Rcvd-amt': '0',
#     'RRN': 919918243800,
#     'Time': '18:35o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.30',
#     '0': 31,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6079****5493',
#     'requiredKeys': {
#         'ATM ID': 'DLON9358',
#         'EJ Error': 'E*3/M-13',
#         'Transaction Date': '18-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '8000',
#         'Disputed Amount': '8000',
#         'Transaction Number': '243800',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b374'),
#     'Rcvd-amt': '0',
#     'RRN': 918713432122,
#     'Time': '13:24o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.31',
#     '0': 32,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6080****8518',
#     'requiredKeys': {
#         'ATM ID': 'CHOD1855',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '06-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '432122',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b375'),
#     'Rcvd-amt': '0',
#     'RRN': 920217412311,
#     'Time': '17:01o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.32',
#     '0': 33,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5166****5280',
#     'requiredKeys': {
#         'ATM ID': 'CHOD9545',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '3000',
#         'Disputed Amount': '3000',
#         'Transaction Number': '412311',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b376'),
#     'Rcvd-amt': '0',
#     'RRN': 920206164365,
#     'Time': '6:45',
#     'CIT': 'CMS',
#     'caseId': '201908033188.33',
#     '0': 34,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4722****1677',
#     'requiredKeys': {
#         'ATM ID': 'DLON1526',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1000',
#         'Disputed Amount': '1000',
#         'Transaction Number': '164365',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b377'),
#     'Rcvd-amt': '0',
#     'RRN': 919320217494,
#     'Time': '20:32o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.34',
#     '0': 35,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6081****4782',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW783',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '12-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '217494',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b378'),
#     'Rcvd-amt': '0',
#     'RRN': 920011378486,
#     'Time': '11:55o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.35',
#     '0': 36,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6522****2752',
#     'requiredKeys': {
#         'ATM ID': 'PUBW9155',
#         'EJ Error': 'E*3/M-13',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '378486',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b379'),
#     'Rcvd-amt': '0',
#     'RRN': 920016401556,
#     'Time': '16:49o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.36',
#     '0': 37,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4214****9415',
#     'requiredKeys': {
#         'ATM ID': 'TNOD9449',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '300',
#         'Disputed Amount': '300',
#         'Transaction Number': '401556',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37a'),
#     'Rcvd-amt': '0',
#     'RRN': 918221433008,
#     'Time': '21:02o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.37',
#     '0': 38,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4591****8960',
#     'requiredKeys': {
#         'ATM ID': 'CHON2667',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '01-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '300',
#         'Disputed Amount': '300',
#         'Transaction Number': '433008',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37b'),
#     'Rcvd-amt': '0',
#     'RRN': 919620456346,
#     'Time': '20:46o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.38',
#     '0': 39,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '6220****6537',
#     'requiredKeys': {
#         'ATM ID': 'CHON2680',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '15-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '2000',
#         'Disputed Amount': '2000',
#         'Transaction Number': '456346',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37c'),
#     'Rcvd-amt': '0',
#     'RRN': 919712314114,
#     'Time': '17:45o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.39',
#     '0': 40,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5242****3706',
#     'requiredKeys': {
#         'ATM ID': 'DLOD2448',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '16-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1500',
#         'Disputed Amount': '1500',
#         'Transaction Number': '314114',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37d'),
#     'Rcvd-amt': '0',
#     'RRN': 919712314111,
#     'Time': '17:42o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.40',
#     '0': 41,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5242****3706',
#     'requiredKeys': {
#         'ATM ID': 'DLOD2448',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '16-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '1500',
#         'Disputed Amount': '1500',
#         'Transaction Number': '314111',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37e'),
#     'Rcvd-amt': '0',
#     'RRN': 920015791459,
#     'Time': '20:37o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.41',
#     '0': 42,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5129****8806',
#     'requiredKeys': {
#         'ATM ID': 'HYON1115',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '3500',
#         'Disputed Amount': '3500',
#         'Transaction Number': '791459',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b37f'),
#     'Rcvd-amt': '0',
#     'RRN': 920111342221,
#     'Time': '17:19o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.42',
#     '0': 43,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4059****0144',
#     'requiredKeys': {
#         'ATM ID': 'HYON1171',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '342221',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b380'),
#     'Rcvd-amt': '0',
#     'RRN': 920102248573,
#     'Time': '7:46',
#     'CIT': 'CMS',
#     'caseId': '201908033188.43',
#     '0': 44,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5129****0698',
#     'requiredKeys': {
#         'ATM ID': 'PUON9335',
#         'EJ Error': 'Successful txn',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '4000',
#         'Disputed Amount': '4000',
#         'Transaction Number': '248573',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b381'),
#     'Rcvd-amt': '0',
#     'RRN': 920210157877,
#     'Time': '16:24o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.44',
#     '0': 45,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4505****2972',
#     'requiredKeys': {
#         'ATM ID': 'DLON1388',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '157877',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b382'),
#     'Rcvd-amt': '0',
#     'RRN': 919411131677,
#     'Time': '16:35o:p',
#     'CIT': 'CMS',
#     'caseId': '201908033188.45',
#     '0': 46,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4585****6218',
#     'requiredKeys': {
#         'ATM ID': 'BLON1132',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '13-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '131677',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Offuso:p',
#     '_id': ObjectId('5d4529a00e4e695258c4b383'),
#     'Rcvd-amt': '0',
#     'RRN': 918813029741,
#     'Time': '19:20o:p',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.46',
#     '0': 47,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '22-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4160****9588',
#     'requiredKeys': {
#         'ATM ID': 'MUMOW774',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '07-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '29741o:p',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b384'),
#     'Rcvd-amt': '0',
#     'RRN': 902908267152,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.47',
#     '0': 48,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4594********6219',
#     'requiredKeys': {
#         'ATM ID': 'BLON9271',
#         'EJ Error': 'Txn skip/power-up reset',
#         'Transaction Date': '29-Jan-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '2200',
#         'Disputed Amount': '2200',
#         'Transaction Number': '267152',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b385'),
#     'Rcvd-amt': '0',
#     'RRN': 918503423765,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.48',
#     '0': 49,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********7879',
#     'requiredKeys': {
#         'ATM ID': 'PUOD9538',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '04-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '5000',
#         'Disputed Amount': '5000',
#         'Transaction Number': '423765',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b386'),
#     'Rcvd-amt': '0',
#     'RRN': 919414426586,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.49',
#     '0': 50,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********5225',
#     'requiredKeys': {
#         'ATM ID': 'HYON1630',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '13-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '900',
#         'Disputed Amount': '900',
#         'Transaction Number': '426586',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b387'),
#     'Rcvd-amt': '0',
#     'RRN': 920002374481,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.50',
#     '0': 51,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********4577',
#     'requiredKeys': {
#         'ATM ID': 'BLON1040',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '3000',
#         'Disputed Amount': '3000',
#         'Transaction Number': '374481',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b388'),
#     'Rcvd-amt': '0',
#     'RRN': 920007432904,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.51',
#     '0': 52,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********0591',
#     'requiredKeys': {
#         'ATM ID': 'CHON2676',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '19-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '500',
#         'Disputed Amount': '500',
#         'Transaction Number': '432904',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b389'),
#     'Rcvd-amt': '0',
#     'RRN': 920106350771,
#     'Time': '-',
#     'CIT': 'SIPL',
#     'caseId': '201908033188.52',
#     '0': 53,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********5586',
#     'requiredKeys': {
#         'ATM ID': 'HYON1465',
#         'EJ Error': 'E*2/-19',
#         'Transaction Date': '20-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '20000o:p',
#         'Disputed Amount': '20000o:p',
#         'Transaction Number': '350771',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b38a'),
#     'Rcvd-amt': '0',
#     'RRN': 920208250191,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.53',
#     '0': 54,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5496********7733',
#     'requiredKeys': {
#         'ATM ID': 'BLBN9013',
#         'EJ Error': 'EJ NAo:p',
#         'Transaction Date': '21-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '10000o:p',
#         'Disputed Amount': '10000o:p',
#         'Transaction Number': '250191',
#         'Reference No': '-',
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b38b'),
#     'Rcvd-amt': '0',
#     'RRN': 920307399423,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.54',
#     '0': 55,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4594********5159',
#     'requiredKeys': {
#         'ATM ID': 'MPBN9039',
#         'EJ Error': 'CASH PRESENT TIMER EXPIRED',
#         'Transaction Date': '22-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '3200',
#         'Disputed Amount': '3200',
#         'Transaction Number': '399423',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b38c'),
#     'Rcvd-amt': '0',
#     'RRN': 920309296582,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.55',
#     '0': 56,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '5219********5004',
#     'requiredKeys': {
#         'ATM ID': 'APBD9068',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '22-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '9000',
#         'Disputed Amount': '9000',
#         'Transaction Number': '296582',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b38d'),
#     'Rcvd-amt': '0',
#     'RRN': 920309342729,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.56',
#     '0': 57,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********9914',
#     'requiredKeys': {
#         'ATM ID': 'DLON9200',
#         'EJ Error': 'HOST TX TIMEOUT',
#         'Transaction Date': '22-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '24000o:p',
#         'Disputed Amount': '24000o:p',
#         'Transaction Number': '342729',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }, {
#     'status': 'OPEN',
#     'Tie-up': 'Onus',
#     '_id': ObjectId('5d4529a00e4e695258c4b38e'),
#     'Rcvd-amt': '19500o:p',
#     'RRN': 920420413538,
#     'Time': '-',
#     'CIT': 'CMS',
#     'caseId': '201908033188.57',
#     '0': 58,
#     'userInputKeys': {
#         'Internal Action': '',
#         'Shortage To Date': '',
#         'Shortage From Date': '',
#         'Customer Status': '',
#         'Deposited Amount': '',
#         'Dispute Comments': '',
#         'Deposited Date': '',
#         'HO Remarks': '',
#         'Justification (if Rejected)': ''
#     },
#     'Date': '24-Jul-19',
#     'ticketId': '201908033188',
#     'Card-no': '4280********0912',
#     'requiredKeys': {
#         'ATM ID': 'BLON9586',
#         'EJ Error': 'EJ NAo:p',
#         'Transaction Date': '23-Jul-19',
#         'Card Number': '-',
#         'Dispute Date': '-',
#         'Transaction Amount': '20000o:p',
#         'Disputed Amount': '500',
#         'Transaction Number': '413538',
#         'Reference No': '-',
#
#         'Transaction Time': '-'
#     }
# }]
# status = {}
# for case in cases:
#     # if "e*13" in case['requiredKeys']['EJ Error'] or "M-13" in case['requiredKeys']['EJ Error']:
#     # if case['requiredKeys']['ATM ID'] == 'CHOD1855':
#         ejFile = case['requiredKeys']['ATM ID']
#         tranNo = ''
#         for chars in case['requiredKeys']['Transaction Number']:
#             if chars.isdigit():
#                 tranNo += chars
#         case['requiredKeys']['Transaction Number'] = tranNo
#         if 'Transaction Date' in case['requiredKeys']:
#             ejFile += '-' + str(
#                 datetime.datetime.strftime(datetime.datetime.strptime(case['requiredKeys']['Transaction Date'], '%d-%b-%y'),
#                                            '%d-%m-%Y'))
#
#         files = os.listdir('/tmp/' + case['ticketId'])
#         for file in files:
#             if ejFile in file:
#                 rawEjFile = file
#                 break
#
#         investigation = parseEjFile(rawEjFile, case)
#         investigation = verifyCBR(investigation, case)
#         status[case['requiredKeys']['ATM ID'] + str(case['requiredKeys']['Transaction Number'])] = investigation
#
# for key, inves in status.items():
#     errorStatus = error.get(inves.get('EJ ERROR', ''), {})
#     if errorStatus != '':
#         # remarks = errorStatus.get(inves.get('EJ ERROR'), {})
#         remarks = errorStatus.get(inves.get('overageStatus', ''), {}).get(inves.get('switchStatus', ''), {})
#         inves['rmsMspStatus'] = remarks.get('rmpStatus')
#         inves['rejectionJust'] = remarks.get('rejectJust')
#         inves['region'] = remarks.get('region')
#         inves['errorType'] = remarks.get('errorType')
#         inves['MachineStatus'] = remarks.get('machineStatus')
#         status[key] = inves
# print(status)
