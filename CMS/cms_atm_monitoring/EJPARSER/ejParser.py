import pandas
import os
from pymongo import MongoClient
import re

config = {'startKey': 'TRANSACTION START',
          'endKey': 'TRANSACTION END',
          'mftPath': '/home/gumma/Downloads/Algofusion POC/Required data ATM And Network Recon/EJ Reports/Mar 2019/01032019/',
          'tranTypes': ['WITHDRAWAL', 'BALANCE INQUIRY']
          }


class EjParser():
    def __init__(self):
        mongo = MongoClient('localhost')
        self.db = mongo['ngerecon_uk']

    def parseRequiredDetails(self, reqLines, file):
        data = {}
        cashTaken = False
        if len(reqLines) == 2:
            return data
        for index, line in enumerate(reqLines):
            if 'EMV AID' in line:
                data['CARD NUMBER'] = line.split('/')[1].strip(' ').split(' ')[0]

            if 'TERMINAL' in line:
                nextLine = reqLines[index+1]
                prevLine = reqLines[index-1]
                prevLine = prevLine.split(' ')
                location = [detail for detail in prevLine if detail != '']
                data['LOCATION'] = ' '.join(location)
                details = nextLine.split(' ')
                date, time, id = [detail for detail in details if detail!='']
                data['DATE'] = date
                data['TIME'] = time
                data['TERMINAL ID'] = id


            # if 'DATE' in line:
            #     data['DATE'] = line.split(':', 1)[1].replace('\\', '-').strip(' ')

            # if 'CASH PRESENTED' in line:
            #     print(reqLines[index - 1].split(';'))

            if 'SET PIN TRANSACTION' in line:
                data['Tran Type'] = 'SET PIN TRANSACTION'

            if 'RESPCODE#' in line:
                line = line.replace(' ','')
                data['RSP CODE'] = line.split('RESPCODE#')[1].strip(' ')
                data['TXN NO'] = line.split('RESPCODE#')[0].strip(' ').split('#')[1]
                # data['RSP CODE'] = respDetails[0][:2]
                # if 'Tran Type' in data and data['Tran Type'] == 'WITHDRAWAL':
                #     if len(respDetails) > 1:
                #         data['Amount'] = respDetails[1].strip('RS.')
            #
            if 'FROM ACCT NO' in line:
                data['FROM ACCT NO'] = line.split(':')[1].strip('')

            if 'AUTH CODE' in line:
                data['AUTH CODE'] = reqLines[index+1].split(' ')[-1].strip('')

            if 'AMOUNT RS' in line:
                data['AMOUNT'] = line.split('AMOUNT RS')[1].strip('')
            try:
                if 'AVAILABLE' in line.replace(' ', ''):
                    data['AVAILABLE BALANCE'] = re.search(r'\d.+',line).group().strip()
            except Exception as e:
                print(e)
            if 'LEDGER' in line:
                data['LEDGER BALANCE'] = line.split(':')[1].strip('RS. # ')
                # print(data)

            if 'TRACK 2 DATA:' in line and 'CARD NUMBER' not in data:
                if line.split('TRACK 2 DATA:')[1].strip(' ') != '':
                    data['CARD NUMBER'] = line.split('TRACK 2 DATA:')[1].strip(' ')

            if 'CARD NUMBER' in line and 'CARD NUMBER' not in data:
                data['CARD NUMBER'] = line.split(':')[1].strip(' ')

            for tran in ['BALANCE ENQUIRY', 'CASH WITHDRAWAL','CASH WITHDRAWL','BALANCE-INQUIRY','PIN-CHANGE','PIN CHANGE']:
                if tran in line:
                    data['TRAN_TYPE'] = tran
                    break

            if 'MINISTATEMENT' in line:
                data['Tran Type'] = 'MINISTATEMENT'

            if 'PIN CHANGE' in line:
                data['Tran Type'] = 'PIN CHANGE'

            if 'CASH TAKEN' in line:
                cashTaken = True

            # if 'TRANSACTION END' in line:
            #     if data.get('RSP CODE','') == '00' and data.get('Tran Type','') == 'WITHDRAWAL':
            #         if not cashTaken:
            #             data['TRANSACTION_STATUS'] = 'ERROR FOUND'


            # if 'DISPENSER V4:' in line:
            #     print(line)
            # if 'ERROR' in line:
            #     print('*'*100)
            #     print(reqLines[index+1])
            #     print(reqLines[index-1])
            #     print(line)
            #     print('*' * 100)
        return data

    def findFiles(self):
        # path = config['mftPath']
        # files = os.listdir(path)F
        allAtmData = []
        files = ['/home/gumma/Documents/NgEreconDemo/Co-op Bank/fwdglstatements_/JP 31-3-2019 to 01-04-2019-1/Bopal/20190401.jrn']
        for file in files:
            status, atmData = self.extractData(file)
            if status:
                allAtmData.extend(atmData)
                # print("%s File Parsed SuccessFully" % file)
        # allAtmData.loc[(allAtmData['RSP CODE']=='92')|(allAtmData['RSP CODE']=='91'),'TRANSACTION_STATUS'] = 'CURRENTLY SERVICES ARE UNAVAILABLE'
        # allAtmData.loc[(allAtmData['RSP CODE']=='E3')|(allAtmData['RSP CODE']=='30')|(allAtmData['RSP CODE']=='63'),'TRANSACTION_STATUS'] = 'THE TRANSACTION COULD NOT BE COMPLETED'
        # allAtmData.loc[(allAtmData['RSP CODE']=='34')|(allAtmData['RSP CODE']=='36') ,'TRANSACTION_STATUS'] = 'DECLINED FROM FINANCIAL INSTITUTION'
        # allAtmData.loc[(allAtmData['RSP CODE']=='00') & (allAtmData['Tran Type']=='BALANCE INQUIRY'),'TRANSACTION_STATUS'] = 'SUCCESS'
        # allAtmData.loc[(allAtmData['RSP CODE']=='00') & (allAtmData['Tran Type']=='MINISTATEMENT'),'TRANSACTION_STATUS'] = 'SUCCESS'
        # allAtmData.loc[(allAtmData['RSP CODE']=='00') & (allAtmData['Tran Type']=='SET PIN TRANSACTION'),'TRANSACTION_STATUS'] = 'SUCCESS'

        allAtmData = pandas.DataFrame(allAtmData)

        allAtmData.loc[allAtmData['RSP CODE']=='80','TRANSACTION_STATUS'] = 'UNABLE TO DISPENSE CASH'
        allAtmData.loc[allAtmData['RSP CODE'].isin(['92','91']),'TRANSACTION_STATUS'] = 'CURRENTLY SERVICES ARE UNAVAILABLE'
        allAtmData.loc[allAtmData['RSP CODE'].isin(['E3','30','63','P1']),'TRANSACTION_STATUS'] = 'THE TRANSACTION COULD NOT BE COMPLETED'
        allAtmData.loc[allAtmData['RSP CODE'].isin(['34','36']),'TRANSACTION_STATUS'] = 'DECLINED FROM FINANCIAL INSTITUTION'
        allAtmData.loc[(allAtmData['RSP CODE']=='00')&(allAtmData['TRANSACTION_STATUS']!='ERROR FOUND'),'TRANSACTION_STATUS'] = 'SUCCESS'
        allAtmData.loc[(allAtmData['RSP CODE']=='000')&(allAtmData['TRANSACTION_STATUS']!='ERROR FOUND'),'TRANSACTION_STATUS'] = 'SUCCESS'

        # allAtmData.loc[(allAtmData['RSP CODE']=='00') & (allAtmData['Tran Type']=='WITHDRAWAL'),'TRANSACTION_STATUS'] = 'CASH TAKEN'

        allAtmData.loc[allAtmData['RSP CODE']=='20' ,'TRANSACTION_STATUS'] = 'FAILED'
        allAtmData.loc[allAtmData['RSP CODE']=='52' ,'TRANSACTION_STATUS'] = 'AN INVALID ACCOUNT WAS SELECTED'
        allAtmData.loc[(allAtmData['RSP CODE']=='59') ,'TRANSACTION_STATUS'] = 'DECLINED CONTACT YOUR NEAREST UTKARSH BRANCH'
        allAtmData.loc[(allAtmData['RSP CODE']=='68') ,'TRANSACTION_STATUS'] = 'YOUR FINANCIAL INSTITUTION IS UNAVAILABLE'
        allAtmData.loc[(allAtmData['RSP CODE']=='75') ,'TRANSACTION_STATUS'] = 'THE MAXIMUM PIN TRIES WAS EXCEEDED'
        allAtmData.loc[(allAtmData['RSP CODE']=='61') ,'TRANSACTION_STATUS'] = 'TXN LMIT EXCEEDED'
        allAtmData.loc[(allAtmData['RSP CODE']=='51') ,'TRANSACTION_STATUS'] = 'INSUFFICIENT BALANCE IN YOUR ACCOUNT'
        allAtmData.loc[(allAtmData['RSP CODE']=='55') ,'TRANSACTION_STATUS'] = 'YOU HAVE ENTERED INVALID PIN'
        allAtmData.loc[(allAtmData['RSP CODE']=='42') ,'TRANSACTION_STATUS'] = 'INVALID ACCOUNT'


        # allAtmData['UNIQUE TRAN NO'] = allAtmData['UNIQUE TRAN NO'].str.lstrip('0')
        # allAtmData['UNIQUE RRN NO'] = allAtmData['UNIQUE RRN NO'].str.lstrip('0')
        allAtmData['AMOUNT'].fillna(0, inplace=True)
        allAtmData.to_csv('/tmp/'+file.split('/')[-1]+'.csv', index=False)

    def extractData(self, file):
        reqLines = []
        transactionStart = False
        transactionEnd = False

        try:
            with open(file, 'r') as f:
                allData = []
                lines = f.read().splitlines()
                for line in lines:
                    if config['startKey'] in line:
                        transactionStart = True
                    if config['endKey'] in line:
                        transactionEnd = True

                    if transactionStart and not transactionEnd:
                        reqLines.append(line)

                    if transactionEnd:
                        reqLines.append(line)
                        parsedData = self.parseRequiredDetails(reqLines, file)
                        if len(parsedData) > 1:
                            allData.append(parsedData)
                        reqLines = []
                        transactionStart = False
                        transactionEnd = False

                if len(allData):
                    allData = pandas.DataFrame(allData)
                    # GlAtmDetails = self.db['GlWiseAtmDetails'].find_one({'ATM Id': file.split('_')[0]}, {'_id': 0})
                    # if GlAtmDetails:
                    #     for key, value in GlAtmDetails.items():
                    #         allData[key] = value
                    # else:
                        # allData['ATM Id'] = file.split('_')[0]

                    for iin in ['508524', '608014']:
                        allData.loc[allData['CARD NUMBER'].str.startswith(iin, na=False), 'CardType'] = 'Onus'
                        allData.loc[~allData['CARD NUMBER'].str.startswith(iin, na=False), 'CardType'] = 'Offus'

                    return True, allData.to_dict(orient='records')
                else:
                    return False, []
        except Exception as e:
            import traceback
            print(traceback.format_exc())
            print(e)


if __name__ == '__main__':
    EjParser().findFiles()