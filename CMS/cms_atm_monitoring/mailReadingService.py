import imaplib
import pandas as pd
import config
import re
import os
import csv
import email
from pymongo import MongoClient
import Helper
from bs4 import BeautifulSoup
import json
# import times
from random import randint
from EJPARSER.MainEJParser import ParseEj
from caseStatusVerify import CaseStatus
from datetime import datetime
from time import gmtime, strftime


# import nltk
# from textblob import TextBlob
# import shutil


# Mail Monitor
class MailParser():
    def __init__(self):
        self.filePath = []

        self.mongo = MongoClient(config.MONGO_HOST)
        self.db = self.mongo[config.DATABASE]
        self.ticketCollection = self.db[config.TICKET_COLL]
        self.ticketCaseCollection = self.db[config.TICKET_CASE_COLL]
        date = datetime.now().date()
        date = datetime.strftime(date, '%d%m%Y')
        self.dayFilePath = config.caseFolder + date + os.sep
        if not os.path.exists(self.dayFilePath):
            os.mkdir(self.dayFilePath)
        with open('JSON/mailTableKeys.json', 'r') as jsonFile:
            self.standardKeys = json.load(jsonFile)

    def read_mail(self):
        mail = imaplib.IMAP4_SSL(config.MAIL_SERVER)
        mail.login(config.MAIL_USER, config.MAIL_PASSWORD)
        mail.list()
        latest_email_uid = ''
        while True:
            mail.select("Inbox", readonly=True)
            result, data = mail.uid('search', None, "ALL")  # search and return uids instead
            ids = data[0]  # data is a list.
            id_list = ids.split()
            if len(data[0].split())==0:# ids is a space separated string
                continue
            present_mail_id = data[0].split()[-1]

            # for id in data[0].split():
            if latest_email_uid != present_mail_id:
                latest_email_uid = present_mail_id

                # call mail parser
                ticketDoc = self.multipart_attachment(mail, latest_email_uid)

                if ticketDoc != None:
                    # get the cases for the each mail
                    ticketDoc, cases = self.get_case_details(ticketDoc)

                    # Formating the cases keys to standard keys
                    cases = self.formatTheCases(cases)

                    # Parsing the EJ files found in the attachements
                    ParseEj(self.dayFilePath).parseEj(ticketDoc, cases)

                    # verifying the cases
                    data = self.verifyTheCases(cases,ticketDoc)

                    ticketDoc['resolved'] = data['resolved']
                    ticketDoc['openCase'] = data['openCase']
                    cases = data['cases']

                    # save the details to database
                    self.saveDataToDataBase(ticketDoc, cases)

    # Parsing the recent mail data.
    def multipart_attachment(self, mail, latest_email_uid):
        ticketDoc = {}
        attachments = []
        csvFile = ''
        print("mail Latest found")
        result, data = mail.uid('fetch', latest_email_uid.decode('utf-8'),
                                '(RFC822)')  # fetch the email headers and body (RFC822) for the given ID
        raw_email = data[0][1]
        raw_email_string = raw_email.decode('utf-8')  # converts byte literal to string removing b''
        email_message = email.message_from_string(raw_email_string)

        ticketDoc['subject'] = email_message.get_all('Subject')[0]
        ticketDoc['from'] = email_message.get_all('From')[0].split('<')[1].rstrip('>')
        ticketDoc['to'] = email_message.get_all('To')
        for bank in config.bankNames:
            if re.search(bank, ticketDoc['subject'], re.IGNORECASE):
                ticketDoc['bank'] = bank
                break

        # if ticketDoc['subject'].replace(' ', '') != "Fwd:FW:CustomerClaim_AXISIAD_Onus_CMS_SIPL_26-JUL-2019" and \
        #         ticketDoc['subject'].replace(' ', '') != "FW:KOTAKBankchargebackDisputesCMS-SIPL-24-Jul-19":
        #     print(ticketDoc['subject'])
        #     return
        ticketDoc['ticketId'] = Helper.generateTicketId(4)

        path = Helper.create_folder(self.dayFilePath, str(ticketDoc['ticketId']))

        ticketDoc['status'] = "open"
        ticketDoc['creationTime'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        for part in email_message.walk():
            if part.get_content_maintype() == 'multipart':
                for nextPart in part.get_payload():
                    if nextPart.get_content_maintype() == 'multipart':
                        for p in nextPart.get_payload():
                            if p.get_content_subtype() == 'html':
                                csv_file = self.parseTableFromHtml(nextPart, ticketDoc['ticketId'])
                            elif p.get_content_subtype() == 'plain':
                                bodyPart = self.txt_format(nextPart)
                                # self.parsingEmailBody(bodyPart)
                                # doc['caseState'] = self.case_map(body_content=bodyPart)
                    else:
                        if nextPart.get_content_subtype() == 'plain':
                            bodyPart = self.txt_format(nextPart)
                            # self.parsingEmailBody(bodyPart)
                            # doc['caseState'] = self.case_map(body_content=bodyPart)

                        if nextPart.get_content_subtype() == 'html':
                            csv_file = self.parseTableFromHtml(nextPart, ticketDoc['ticketId'])

            if part.get('Content-Disposition') is not None:
                fileName = part.get_filename()
                if bool(fileName):
                    attachments.append(fileName)
                    if not os.path.isfile(os.path.join(self.dayFilePath + ticketDoc['ticketId'], fileName)):
                        fp = open(os.path.join(self.dayFilePath + ticketDoc['ticketId'], fileName), 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()
                        subject = str(email_message).split("Subject: ", 1)[1].split("\nTo:", 1)[0]
                        print('Downloaded "{file}" from email titled "{subject}" with UID {uid}.'.format(
                            file=fileName,
                            subject=subject,
                            uid=latest_email_uid.decode(
                                'utf-8')))
        ticketDoc['csvFile'] = csv_file
        ticketDoc['attachments'] = attachments
        return ticketDoc

    def get_case_details(self, ticketDoc):
        if ticketDoc['csvFile'] != '':
            df = pd.read_csv(ticketDoc['csvFile'], skiprows=1, skipfooter=1)
            df.fillna('-', inplace=True)
            df['ticketId'] = ticketDoc['ticketId']
            df['status'] = "OPEN"
            if len(df) == 1:
                ticketDoc['caseType'] = "Single"
            elif len(df) > 1:
                ticketDoc['caseType'] = "Bulk"
            ticketDoc['caseCount'] = len(df)
            for i in range(0, len(df)):
                df.loc[i:i, 'caseId'] = ticketDoc['ticketId'] + '.' + str(i)

            # df['']
                # path = Helper.create_folder(self.dayFilePath + str(ticketDoc['ticketId']) + os.sep, str(i))
            return ticketDoc, df.to_dict(orient='records')

    # save Details
    def saveDataToDataBase(self, ticketDoc, cases):
        # Ticket insert/Update
        ticket = self.ticketCollection.find_one({'ticketId': ticketDoc['ticketId']})
        if ticket:
            self.ticketCollection.update({'_id': ticket['_id']}, {"$set": ticketDoc})
        else:
            if 'attachments' in ticketDoc.keys():
                del ticketDoc['attachments']
            ticketDoc['partnerId'] = '54619c820b1c8b1ff0166dfc'
            self.ticketCollection.insert(ticketDoc)

        # Case insert/Update
        for case in cases:
            item = self.ticketCaseCollection.find_one({'caseId': case['caseId']})
            if 'attachments' in case.keys():
                del case['attachments']
            if item:
                self.ticketCaseCollection.update({'_id': item['_id']}, {"$set": case})
            else:
                case['partnerId'] = '54619c820b1c8b1ff0166dfc'
                for key in case.keys():
                    try:
                        a = float(key)
                        del case[key]
                    except:
                        if '.' in key:
                            case[key.replace('.','')] = case[key]
                            del case[key]
                        print('a')
                self.ticketCaseCollection.insert(case)

        return True, ''

    # email parsing for multipart/plain/whatever
    def txt_format(self, nextPart):
        bodyPart = str(nextPart).replace('\r', '').replace('\n', '').replace('\t', '')

        for replaceSign in config.replaceSigns:
            bodyPart = bodyPart.strip(' ')
            bodyPart = bodyPart.replace("=" + replaceSign, replaceSign)
            bodyPart = bodyPart.replace(replaceSign + "=", replaceSign)

        b = re.finditer('[\,]+', bodyPart)
        for i in b:
            bodyPart = bodyPart.replace(i.group(), ',')
        return bodyPart

    def parseTableFromHtml(self, nextPart, fileName):
        filePath = self.dayFilePath + fileName + os.sep + fileName + '.html'
        csvFile = ''
        if '</html>' in str(nextPart) and '<html' in str(nextPart):
            bodyPart = str(nextPart)[str(nextPart).index('<html'):]
            bodyPart = str(bodyPart)[:str(bodyPart).index('</html>')]
        elif '<meta' in str(nextPart):
            bodyPart = str(nextPart)[str(nextPart).index('<meta'):]
            # bodyPart = bodyPart[:bodyPart.index('</html>') + 7]
        bodyPart = self.txt_format(bodyPart)
        with open(filePath, 'w') as f:
            f.writelines(bodyPart)
        if '</table>' in bodyPart:
            csvFile = self.extractTableDataFromHtml(fileName)
        return csvFile

    #
    def extractTableDataFromHtml(self, fileName):
        filePath = self.dayFilePath + fileName + os.sep + fileName
        html = open(filePath + '.html').read()
        df = pd.DataFrame()
        soup = BeautifulSoup(html)
        tables = soup.find_all("table")
        output_rows = []
        for table in tables:
            for table_row in table.findAll('tr'):
                columns = table_row.findAll('td')
                output_row = []
                for column in columns:
                    output_row.append(column.text)
                output_rows.append(output_row)
            for index, row in enumerate(output_rows):
                for inx, item in enumerate(row):
                    for sign in config.replacehtmlSigns:
                        item = item.replace(sign, '')
                    row[inx] = item
                output_rows[index] = row
            df1 = pd.DataFrame(output_rows)
        df = pd.concat([df,df1])
        df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
        df.to_csv(filePath + '.csv')
        return filePath + '.csv'

    def formatTheCases(self, cases):
        excludeKeys = ["_id", "partnerId", "recommdation", "errorCode", 'Justification (if Rejected)',
                       'Customer Status', 'Dispute Comments', 'HO Remarks', 'Internal Action', 'Deposited Date',
                       'Deposited Amount', 'Error Code', 'Error Code', 'Description', 'Error Category',
                       'Shortage From Date', 'Shortage To Date']

        for index, case in enumerate(cases):
            dict1 = {}
            dict2 = {}
            for key, value in self.standardKeys.items():
                for k, v in case.items():
                    caseKey = k.replace(' ', '').lower()
                    caseKey = caseKey.replace('_', '').lower()

                    if caseKey in value:
                        if key not in excludeKeys:
                            del case[k]
                            dict1[key] = v
                if key not in dict1.keys() and key not in excludeKeys:
                    dict1[key] = '-'


                elif key in excludeKeys and key not in case.keys():
                    dict2[key] = ''
                else:
                    try:
                        a = float(k)
                        del case[k]
                    except Exception as e:
                        print(e)


            cases[index]['requiredKeys'] = dict1
            cases[index]['userInputKeys'] = dict2

        return cases

    def verifyTheCases(self, cases, ticketDoc):
        resolved = 0
        openCase = 0

        for idx, case in enumerate(cases):
            case = CaseStatus(self.dayFilePath).analyseCase(case,ticketDoc)
            cases[idx] = case
            if case['status'] == 'RESOLVED':
                resolved += 1
            else:
                openCase += 1
        data = {}
        data['resolved'] = resolved
        data['openCase'] = openCase
        data['cases'] = cases
        return data


if __name__ == "__main__":
    MailParser().read_mail()
