import pandas
from bson import ObjectId
import os
import io
import json
import datetime
import re
import config
from EJPARSER import MainEJParser
import traceback


class CaseStatus():
    def __init__(self, dailyPath):
        self.status = {}
        self.dayFilePath = dailyPath
        with open('JSON/ERROR_CODE.json', 'r') as js:
            self.error = json.load(js)

    def analyseCase(self, case, ticketDoc):
        try:
            ejFile1 = case['requiredKeys']['ATM ID']
            ejFile2 = case['requiredKeys']['ATM ID']
            case['flowStatus'] = {}
            case['flowStatus'] = {'autoMailParsing': True,
                                  'caseTicketCreation': True,
                                  'emailContentExtraction': True,
                                  'attachmentDownload': True,
                                  'ejTxnDetailsExtracted': False,
                                  'ejErrorCode': False,
                                  'cbrDetailsFound': False,
                                  'autoRuleValidation': False,
                                  'caseResolved': False
                    }
            tranNo = ''
            for chars in str(case['requiredKeys']['Transaction Number']):
                if chars.isdigit():
                    tranNo += chars
            case['requiredKeys']['Transaction Number'] = tranNo
            if 'Transaction Date' in case['requiredKeys'] and case['requiredKeys']['Transaction Date']!='-':
                ejFile1 += '-' + str(
                    datetime.datetime.strftime(
                        datetime.datetime.strptime(case['requiredKeys']['Transaction Date'], '%d-%b-%y'),
                        '%d-%m-%Y'))
                # pass


            if 'Transaction Number' in case['requiredKeys']:
                ejFile2 += "_" + str(case['requiredKeys']['Transaction Number'])

            files = os.listdir(self.dayFilePath + case['ticketId'])
            rawEjFile = None
            for file in files:
                if ejFile1 in file:
                    rawEjFile = file
                    break
            if rawEjFile==None:
             for file in files:
                if ejFile2 in file:
                    rawEjFile = file
                    break

            if rawEjFile != None:
                case['ejFile'] = rawEjFile
                # Finding the error in EJ
                case = self.parseEjFile(rawEjFile, case)
                # superViseEntry = MainEJParser.ParseEj(self.dayFilePath).getCassetData(self.dayFilePath + case['ticketId'] + os.sep + rawejfile)
                case['flowStatus']['ejTxnDetailsExtracted'] = True
                # Verifying the TRAN BASED ON CBR REPORT

                case = self.verifyCBR(case)

                errorStatus = self.error.get(case.get('EJ ERROR', ''), {})
                case['flowStatus']['ejErrorCode'] = True
                case['errorCodeDescription'] = case.get('EJ ERROR', '')

                if errorStatus != '' and 'rmsMspStatus' in case.keys() and case['rmsMspStatus'] != 'CBR NOT FOUND':
                    case['flowStatus']['cbrDetailsFound'] = True
                    # remarks = errorStatus.get(case.get('EJ ERROR'), {})

                    remarks = errorStatus.get(case.get('overageStatus', ''), {}).get(case.get('switchStatus', ''), {})
                    case['flowStatus']['autoRuleValidation'] = True
                    case['errorCode'] = errorStatus.get('errorCode', '')
                    case['errorCategory'] = errorStatus.get('errorCategory', '')

                    case['rmsMspStatus'] = remarks.get('rmpStatus')
                    case['rejectionJust'] = remarks.get('rejectJust')

                    case['region'] = remarks.get('region')
                    case['errorType'] = remarks.get('errorType')
                    case['MachineStatus'] = remarks.get('machineStatus')
                    case['status'] = 'RESOLVED'
                    case['flowStatus']['caseResolved'] = True
                    if case.get('overageStatus') == 'yes':
                        case['rejectionJust'] = 'Overage Reported'
                        case['rmsMspStatus'] = 'Close with Claim Acceptance / Partial Acceptance'
                        case['MachineStatus'] = 'Switch Difference/Switch Decreased'


            else:
                case['rmsMspStatus'] = 'EJ IS NOT AVAILABLE'
            return case
        except Exception as e:
            print(traceback.format_exc())
            print(e)

    def parseEjFile(self, rawejfile, case):
        a = io.open(self.dayFilePath + case['ticketId'] + os.sep + rawejfile, 'r', encoding='utf-8')
        lines = a.readlines()

        tmpList = []
        complete = []
        start = False
        for index, line in enumerate(lines):
            line = lines[index - 1].strip().replace('\n', '').replace('\r', '').replace('\t', '')
            if 'TRANSACTION START' in line:
                if tmpList != []:
                    complete.append(','.join(tmpList))
                    # start = False
                    tmpList = []

                start = True
                if lines[index - 1].strip().replace('\n', '') != '':
                    reqLine = lines[index - 1].strip().replace('\n', '').replace('\r', '').replace('\t', '')
                    tmpList.append(reqLine)
                    continue

            if 'TRANSACTION END' in line:
                tmpList.append(line)
                if len(tmpList) > 1:
                    complete.append(','.join(tmpList))
                tmpList = []
                start = False

            if line.strip().replace('\n', '') != '':
                tmpList.append(line)

        tranNumberFound = False
        neighbourTranNo = {}
        for index, tran in enumerate(complete):
            # print(case['requiredKeys']['Transaction Number'])

            if re.search(
                    "recordno:" + case['requiredKeys']['Transaction Number'] + "|" + "recordno." + case['requiredKeys'][
                        'Transaction Number'], tran.lower().replace(' ', '')):
                tranNumberFound = True
                tranFound = complete[index]
                tranFoundIdx = index
                break
            if case['requiredKeys']['Transaction Number']!='':
                for i in range(1, 15):
                    pattern = 'recordno:' + str(
                        int(case['requiredKeys']['Transaction Number']) - i) + "|" + 'recordno.' + str(
                        int(case['requiredKeys']['Transaction Number']) - i)
                    pattern2 = 'recordno:' + str(
                        int(case['requiredKeys']['Transaction Number']) + i) + "|" + 'recordno.' + str(
                        int(case['requiredKeys']['Transaction Number']) + i)

                    if re.search(pattern, tran.replace(' ', '').lower()):
                        neighbourTranNo[str(int(case['requiredKeys']['Transaction Number']) - i)] = str(-i) + '_' + str(
                            index)

                    elif re.search(pattern2, tran.replace(' ', '').lower()):
                        # print(str(int(case['requiredKeys']['Transaction Number']) - i))
                        neighbourTranNo[str(int(case['requiredKeys']['Transaction Number']) + i)] = str(i) + '_' + str(
                            index)

        if not tranNumberFound:
            case['Missing Tran'] = True
            case['MachineStatus'] = ''
            case['rmsMspStatus'] = ''
            differences = neighbourTranNo.values()
            differences = sorted(differences, reverse=False)
            seqNo = []
            seqencesNo = sorted(neighbourTranNo.keys())
            for idx in range(0, len(seqencesNo) - 1):
                if seqencesNo[idx] != '' and seqencesNo[idx + 1] != '':
                    if int(seqencesNo[idx + 1]) - int(seqencesNo[idx]) > 2:
                        for j in range(0, int(seqencesNo[idx + 1]) - int(seqencesNo[idx]) - 1):
                            seqNo.append(str(int(seqencesNo[idx]) + j + 1))
                    elif int(seqencesNo[idx + 1]) - int(seqencesNo[idx]) > 1:
                        seqNo.append(str(int(seqencesNo[idx]) + 1))
            case['missingSeqNo'] = ','.join(seqNo)
            if len(differences) > 1:
                try:
                    for i in range(0, len(differences) - 1):
                        if int(differences[i + 1].split('_')[1]) - int(differences[i].split('_')[1]) > 1 and \
                                case['MachineStatus'] == '':
                            idxrange = int(differences[i + 1].split('_')[1]) - int(differences[i].split('_')[1])
                            for idx in range(0, idxrange + 1):
                                tran = complete[int(differences[i].split('_')[1]) + idx]

                                if 'POWER-UP/RESET' in tran:
                                    case['EJ ERROR'] = 'POWER FAILURE'
                                    break

                                elif 'HOST TX TIMEOUT' in tran:
                                    case['EJ ERROR'] = 'HOST TX TIMEOUT'
                                    break

                except Exception as e:
                    print("-" * 100)

            elif len(differences) == 1:
                for idx in range(0, int(differences[0].split('_')[0]) + 1):
                    if (int(differences[0].split('_')[1]) + idx) < len(complete):
                        try:
                            tran = complete[int(differences[0].split('_')[1]) + idx]

                            if 'POWER-UP/RESET' in tran:
                                case['EJ ERROR'] = 'POWER FAILURE'
                                break

                            elif 'HOST TX TIMEOUT' in tran:
                                case['EJ ERROR'] = 'HOST TX TIMEOUT'
                                break
                        except:
                            print('')

        elif tranNumberFound:
            case['tranFound'] = True
            if "e*3" in tranFound.lower() and "m-13" in tranFound.lower():
                case['EJ ERROR'] = "E/1*3"
                # case['EJ ERROR'] = "*E/1*3"
                rangeReq = len(complete)-tranFoundIdx
                if 'RESPONSECODE112' in complete[tranFoundIdx].replace(' ',''):
                    case['MachineStatus'] = 'Response code 112 and 103'
                else:
                    found = 0
                    for idx in range(0,rangeReq-1):
                        tran = complete[idx+tranFoundIdx]
                        if "e*3" in tranFound.lower() and "m-13" in tranFound.lower():
                            found += 1
                    if rangeReq-found <=8:
                        case['MachineStatus'] = 'Machine stopped Dispensing / Decline till EOD'
                    else:
                        case['MachineStatus'] = 'ATM WORKING FINE / SST OFFLINE - SST ONLINE / Machine stopped Dispensing / Decline till EOD'

        for tran in complete:
            if 'HOST TOTALS' in tran:
                if 'hostTotals' not in case.keys():
                    case['hostTotals'] = []
                case['hostTotals'].extend(self.extractCasseteData(tran.split(',')))

            if 'SUPERVISOR MODE ENTRY' in tran:
                if 'machieneDetails' not in case.keys():
                    case['machieneDetails'] = []
                case['machieneDetails'].extend(self.extractCasseteData(tran.split(',')))
        return case

    def extractCasseteData(self,tran):
        doc = []
        data = {}
        data['TYPE1'] = {}
        data['TYPE2'] = {}
        data['TYPE3'] = {}
        data['TYPE4'] = {}
        count = 0
        for i, line in enumerate(tran):
            if 'DATE' in line:
                if count == 1:

                    dict1 = {}
                    for key,val in data.items():
                        if val!={} and val!='':
                            dict1[key] = val
                        if dict1!={}:
                            if isinstance(dict1[key], dict):
                                dict1[key]['type'] = key
                                doc.append(dict1[key])
                    if len(doc)>1:
                        break;

                    data['TYPE1'] = {}
                    data['TYPE2'] = {}
                    data['TYPE3'] = {}
                    data['TYPE4'] = {}
                    count = 0
                data['DATE'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                              line).group() if re.search(
                    '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''
                count = count + 1

            if 'TYPE1' in line.replace(' ',''):

                for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                    for ldx in range(i + 1, i + 5):
                        if type in tran[ldx]:
                            groups = re.findall('([0-9]+)', tran[ldx])
                            if len(groups)==2:
                                data['TYPE1'][type] = groups[0]
                                data['TYPE2'][type] = groups[1]

            if 'TYPE3' in line.replace(' ',''):
                for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                    for ldx in range(i + 1, i + 6):
                        if type in tran[ldx]:
                            groups = re.findall('([0-9]+)', tran[ldx])
                            if len(groups)==2:
                                data['TYPE3'][type] = groups[0]
                                data['TYPE4'][type] = groups[1]

            for cashType in ['CASH DISPENSED', 'CASH REMAINING', 'CASH ADDED']:
                if cashType in line:
                    print(tran[i + 1])
                    groups = re.findall('(=[ 0-9]*)', tran[i + 1])
                    print(groups)
                    if groups:
                        for ctype in ['TYPE1', 'TYPE2']:
                            if not cashType in data[ctype]:
                                data[ctype][cashType] = int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                    groups[1].replace('=', '').strip())
                            else:
                                data[ctype][cashType] = data[ctype][cashType] + int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                    groups[1].replace('=', '').strip())

                if cashType in line:
                    groups = re.findall('(=[ 0-9]*)', tran[i + 2])
                    print(groups)
                    if groups:
                        for ctype in ['TYPE3', 'TYPE4']:
                            if not cashType in data[ctype]:
                                data[ctype][cashType] = int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                    groups[1].replace('=', '').strip())
                            else:
                                data[ctype][cashType] = data[ctype][cashType] + int(
                                    groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                    groups[1].replace('=', '').strip())

            if 'LAST CLEARED' in line:
                data['LAST CLEARED'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                                 line).group() if re.search(
                    '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''
        if data!=[] and doc==[]:
            dict1 = {}
            for key, val in data.items():
                if val != {} and val != '':
                    dict1[key] = val
                if dict1 != {}:
                    if isinstance(dict1[key],dict):
                        dict1[key]['type'] = str(key)
                        doc.append(dict1[key])
                        # if len(doc) > 1:
                # d oc = pandas.DataFrame(doc)
                # doc = pandas.
                # break;
            # doc.append(data)
        return doc

    def verifyCBR(self, case):
        cbrFound = False
        cbrlist = []
        for i in os.listdir('/home/suneel/CMS/CBR'):
            if case['requiredKeys']['ATM ID'] in i:
                cbrFound = True
                if i.split('.')[1].lower() == 'csv':
                    cbrData = pandas.read_csv('/home/suneel/CMS/CBR/' + i)
                elif i.split('.')[1].lower() == 'xlsx':
                    cbrData = pandas.read_excel('/home/suneel/CMS/CBR/' + i)

                cbrData['Loading_Date'] = pandas.to_datetime(cbrData['Loading_Date'])
                cbrData['caseDate'] = case['requiredKeys']['Transaction Date']
                cbrData['caseDate'] = pandas.to_datetime(cbrData['caseDate'])
                caseAtm = case['requiredKeys']['ATM ID']

                cbrData['DIFF'] = cbrData['Loading_Date'] - cbrData['caseDate']
                cbrData['DIFF'] = cbrData['DIFF'].dt.days
                dates = cbrData.loc[cbrData['DIFF'] >= 0, 'Loading_Date'].tolist()
                twoEods = False
                count = 0
                specificData = pandas.DataFrame()
                for i in dates:
                    specificData = cbrData.loc[
                        (cbrData['ATM ID'].str.lower() == caseAtm.lower()) & ((cbrData['Loading_Date'] == i))]
                    if len(specificData) > 0:
                        if specificData['Cash Dispense Total'].values[0] > 0:
                            count += 1
                        specificData = specificData.to_dict(orient='records')[0]
                        case['overageStatus'] = ''
                        case['switchStatus'] = ''

                        if specificData['Mid Cash Increase Total'] > specificData['Replenishment Total']:
                            case['overageStatus'] = 'no'
                            case['switchStatus'] = 'yes'
                            case['midCashIncrease'] = str(i)
                            if specificData['Overage Total'] != 0:
                                case['overageStatus'] = 'yes'
                            break;

                        elif specificData['Mid Cash Increase Total'] == specificData['Replenishment Total']:
                            if count == 2:
                                case['overageStatus'] = 'no'
                                case['switchStatus'] = 'no'
                                if specificData['Overage Total'] != 0:
                                    case['overageStatus'] = 'yes'
                                break;
                            continue

                        elif specificData['Mid Cash Increase Total'] < specificData['Replenishment Total']:
                            case['overageStatus'] = 'no'
                            case['switchStatus'] = 'yes'
                            if specificData['Overage Total'] != 0:
                                case['overageStatus'] = 'yes'
                            break;
                if len(specificData) > 0:
                    if specificData['Mid Cash Increase Total'] == specificData['Replenishment Total']:
                        specificData['midCashStatus'] = 'notReported'
                    else:
                        specificData['midCashStatus'] = 'Reported'

                    for key, val in specificData.items():
                        dict2 = {}
                        if key in ['midCashStatus']:
                            continue

                        if key == 'Overage Total':
                            dict2['key'] = key
                            dict2['value'] = val
                            if val > 0:
                                dict2['status'] = 'Overage Reported'
                            else:
                                dict2['status'] = 'Overage Not Reported'
                        elif key == 'Shortage Total':
                            dict2['key'] = key
                            dict2['value'] = val
                            if val > 0:
                                dict2['status'] = 'Shortage Reported'
                            else:
                                dict2['status'] = 'Shortage Not Reported'

                        elif key == 'Replenishment Total':
                            dict2['key'] = key
                            dict2['value'] = val

                            if val > 0:
                                dict2['status'] = 'Replenishment Reported'
                            else:
                                dict2['status'] = 'Replenishment Not Reported'
                        elif key == 'Mid Cash Increase Total':
                            dict2['key'] = key
                            dict2['value'] = val
                            if val > 0 and specificData['midCashStatus'] != 'notReported':
                                dict2['status'] = 'Mid Cash Increase Reported'
                            else:
                                dict2['status'] = 'Mid Cash Increase Not Reported'
                        elif key == 'Mid Cash Decrease Total':
                            dict2['key'] = key
                            dict2['value'] = val
                            if val > 0:
                                dict2['status'] = 'Mid Cash Decrease Reported'
                            else:
                                dict2['status'] = 'Mid Cash Decrease Not Reported'
                        if dict2!={}:
                            cbrlist.append(dict2)

        case['cbrData'] = cbrlist

                        # elif len(specificData) > 0 and (specificData['Mid Cash Increase Total'] == specificData['Replenishment Total']):
        if not cbrFound:
            case['rmsMspStatus'] = 'CBR NOT FOUND'
            case['cbrData'] = []
        return case
