import imaplib
# import config
import pandas as pd
import re
import os
import csv
import email
from bs4 import BeautifulSoup
import time
from random import randint
import pandas as pd
from pymongo import MongoClient
from datetime import datetime
from time import gmtime, strftime
import nltk
from textblob import TextBlob
import shutil
import io

nltk.download('brown')
nltk.download('averaged_perceptron_tagger')


startBodyStrings = ['Hi', 'hi', 'Dear']
transactionCsvPath = '/tmp/'
stateRegexPattern = []
filePath = '/tmp/'
replacehtmlSigns = ["=", "span", ">", "<"]
attchmentFilePath = '/tmp/ej/'
username = "suneelgumma@outlook.com"
password = "Sun@0121710"
# replaceChars = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
#                 "t", "u", "v", "w", "x", "y", "z"]

replaceSigns = [">", "<"]

pre_arb_map = ["pre- arbitration", "Pre-arbitration","pre-arbitration"]
arb_map = ["arbitration", "arb", "Arb", "Arbitration"]
BO_map = ["BO", "Bank Operations", "bank Operations"]


client = MongoClient('mongodb://localhost:27017/')
db = client['cmsej']

tickets = db.tickets
caseIDs = db.caseIDs
caseInvestigation = db.caseInvestigation
caseTickets = db.caseTickets
caseIdEntry = db.caseIdEntry
ejexecutiondetails = db.ejexecutiondetails
cbrdump = db.cbrdump
errorcodedump = db.errorcodedump
caseTicket = {}
doc1 = {}
caseentry = {}
disputeTran = 8475
investatus = {}

Auto_Reading_of_Email="Open"
Case_Tickets_Creation="Open"
Email_Content_Extraction="Open"
Auto_EJ_Download="Open"
EJ_Txn_Details_Extracted="Open"
Error_Code_Resolved="Open"
CBR_Detail_Extracted="Open"
Auto_Rule_Validation="Open"
Case_Resolved="Open"

class ATM_MONITOR():
    def __init__(self):
        pass

    def read_mail(self, username, password):
        print(investatus)
        mail = imaplib.IMAP4_SSL('imap.outlook.com')
        mail.login(username, password)
        mail.list()
        latest_email_uid = ''
        while True:
            print("HI")
            mail.select("Inbox", readonly=True)
            result, data = mail.uid('search', None, "ALL")  # search and return uids instead
            ids = data[0]  # data is a list.
            id_list = ids.split()  # ids is a space separated string
            present_mail_id = data[0].split()[-1]
            if latest_email_uid != present_mail_id:
                latest_email_uid = present_mail_id
                self.multipart_attachment(mail, latest_email_uid)
            Auto_Reading_of_Email = "Completed"


    # email parsing for multipart/plain/whatever
    def txt_format(self, nextPart):
        bodyPart = str(nextPart).replace('\r', '').replace('\n', '').replace('\t', '')
        # bodyPart = str(nextPart)
        # for replaceChar in replaceChars:
        #     bodyPart = bodyPart.replace("=" + replaceChar, replaceChar)
        #     bodyPart = bodyPart.replace(replaceChar + "=", replaceChar)
        # bodyPart = bodyPart.replace("=" + replaceChar.upper(), replaceChar.upper())
        # bodyPart = bodyPart.replace(replaceChar.upper() + "=", replaceChar.upper())

        for replaceSign in replaceSigns:
            bodyPart = bodyPart.strip(' ')
            bodyPart = bodyPart.replace("=" + replaceSign, replaceSign)
            bodyPart = bodyPart.replace(replaceSign + "=", replaceSign)

        b = re.finditer('[\,]+', bodyPart)
        for i in b:
            bodyPart = bodyPart.replace(i.group(), ',')
        # print("Printing BODYPART>>>>>>>>>>>>>>>>>>>>>>>>>>", bodyPart)
        return bodyPart

    def parseTableFromHtml(self, nextPart):
        if '</html>' in str(nextPart) and '<html' in str(nextPart):
            bodyPart = str(nextPart)[str(nextPart).index('<html'):]
            bodyPart = str(bodyPart)[:str(bodyPart).index('</html>')]
            # bodyPart = bodyPart[:bodyPart.index('</html>') + 7]
            bodyPart = self.txt_format(bodyPart)
            with open(filePath + 'atmIdTable1.html', 'w') as f:
                f.writelines(bodyPart)
            if '</table>' in bodyPart:
                html = open(filePath + 'atmIdTable1.html').read()
                soup = BeautifulSoup(html)
                table = soup.find("table")
                output_rows = []
                for table_row in table.findAll('tr'):
                    columns = table_row.findAll('td')
                    output_row = []
                    for column in columns:
                        output_row.append(column.text)
                    output_rows.append(output_row)
                for index, row in enumerate(output_rows):
                    for inx, item in enumerate(row):
                        for sign in replacehtmlSigns:
                            item = item.replace(sign, '')
                        row[inx] = item
                    output_rows[index] = row
                df = pd.DataFrame(output_rows)
                df = df.replace(to_replace=r'[^\x00-\x7F]+', value='', regex=True)
                df.to_csv(filePath + 'atmIdTable1' + '.csv')
                return filePath + 'atmIdTable1' + '.csv'


    def multipart_attachment(self, mail, latest_email_uid):
        self.filePath = []
        fileName = ''
        result, data = mail.uid('fetch', latest_email_uid.decode('utf-8'),
                                '(RFC822)')  # fetch the email headers and body (RFC822) for the given ID
        raw_email = data[0][1]
        time.sleep(10)
        raw_email_string = raw_email.decode('utf-8')  # converts byte literal to string removing b''
        email_message = email.message_from_string(raw_email_string)
        subject = email_message.get('Subject', '')
        print(subject)
        doc1['subject'] = email_message.get('Subject', '')
        investatus['Email_Content_Extraction'] = "Completed"
        for part in email_message.walk():
            if part.get_content_maintype() == 'multipart':
                for nextPart in part.get_payload():
                    if nextPart.get_content_maintype() == 'multipart':
                        for p in nextPart.get_payload():
                            if p.get_content_subtype() == 'html':
                                csv_file = self.parseTableFromHtml(nextPart)
                                a= self.create_ticket(csv_file)

                            elif p.get_content_subtype() == 'plain':
                                bodyPart = self.txt_format(nextPart)
                                doc1['caseState'] = self.case_map(body_content=bodyPart)
                                # self.parsingEmailBody(bodyPart)
                                # doc1['caseState'] = self.case_map(body_content=bodyPart)
                                # print("success")
                    else:
                        if nextPart.get_content_subtype() == 'plain':
                            bodyPart = self.txt_format(nextPart)
                            # doc1['caseState'] = self.case_map(body_content=bodyPart)
                            # self.parsingEmailBody(bodyPart)
                            # doc1['caseState'] = self.case_map(body_content=bodyPart)
                            # print("success")

                        if nextPart.get_content_subtype() == 'html':
                            csv_file = self.parseTableFromHtml(nextPart)
                            # self.create_ticket(csv_file)

                continue

            if part.get('Content-Disposition') is not None:
                fileName = part.get_filename()
                if bool(fileName):
                    self.filePath.append(os.path.join(attchmentFilePath, fileName))
                    # os.chmod(attchmentFilePath + fileName , 0o777)
                    if not os.path.isfile(os.path.join(attchmentFilePath, fileName)):
                        # os.chmod(attchmentFilePath + fileName, 0o777)
                        fp = open(os.path.join(attchmentFilePath, fileName), 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()
                        subject = str(email_message).split("Subject: ", 1)[1].split("\nTo:", 1)[0]
                        print(subject)
                        print('Downloaded "{file}" from email titled "{subject}" with UID {uid}.'.format(
                            file=fileName,
                            subject=subject,
                            uid=latest_email_uid.decode(
                                'utf-8')))
                        investatus['Auto_EJ_Download/Parsed'] = "Completed"
                    self.parseEJ(fileName= attchmentFilePath + fileName)

        return nextPart,fileName

    def parsingEmailBody(self, bodyPart):
        caseState = ''
        for bodyString in startBodyStrings:
            if re.findall('\\b' + bodyString + '\\b', bodyPart):
                strIndex = bodyPart.index(bodyString)
                bodyEndIndex = bodyPart.index('ATM ID')
                if strIndex != None and bodyEndIndex != None:
                    body = bodyPart[strIndex:bodyEndIndex]
                    # caseState = self.searchCaseState()

        # if re.search('ATM ID', bodyPart):
        #     tableStartIndex = bodyPart.index('ATM ID')
        #     tableEndIndex = bodyPart.index('Thanks')
        #     if tableStartIndex != None and tableEndIndex != None:
        #         tableData = bodyPart[tableStartIndex:tableEndIndex]
        #         status, df = self.convertTableToCsv(tableData, caseState=caseState)

    def convertTableToCsv(self, tableData, caseState):
        listData = []
        reg_check = tableData.split(",")
        for x in reg_check:
            if re.findall(r"^[0-9][A-Z]+[0-9]+", x):
                for atmId in re.findall(r"^[0-9][A-Z]+[0-9]+", x):
                    listData = tableData.split(x)
                    listData[0] = listData[0].lstrip("',").split(",")
                    listData[1] = (x + listData[1]).split(",")

        if len(listData):
            df = pd.DataFrame(listData)
            df['Case  Type'] = 'Single'
            if len(df) > 1:
                df['Case Type'] = 'Bulk'
            if caseState != '':
                df['Case State'] = caseState
            df.to_csv(transactionCsvPath + 'df.csv')
        return True, df

    def random_with_N_digits(self, n):
        range_start = 10 ** (n - 1)
        range_end = (10 ** n) - 1
        return randint(range_start, range_end)

    def ticket_id(self, file):
        df = pd.read_csv(file, skiprows=1)
        tran = df['TRAN NO']
        ran = str(self.random_with_N_digits(6))
        ATM_ID = df['ATM ID']
        return ATM_ID + tran + ran

    def create_ticket(self, file):
        # file= '/home/hinclude/Desktop/outputs.csv'
        df = pd.read_csv(file, skiprows=1)
        # tran = df['TRAN NO']
        ran = str(self.random_with_N_digits(6))
        # ATM_ID = df['ATM ID']
        df['caseId'] = str(ran)
        df['ticketId'] = ""
        doc1['creationTime'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        df['status'] = "OPEN"
        df['partnerId'] = "54619c820b1c8b1ff0166dfc"
        doc1['status'] = "open"
        doc1['caseType'] = "single"
        if len(df['caseId']) > 1:
            for i in range(len(df['caseId'])):
                df['ticketId'][i] = df['caseId'][i] + "." + str(i)
                doc1['caseId'] = df['caseId'][0]
                folder_path = self.create_folder(case_id=df['caseId'],ticket_id=str(i))
                shutil.move(folder_path,'/tmp/atmId.csv')
                # self.ejdisplay(folder_path)
                # caseTickets['ticketfolderPath'] = folder_path
                shutil.move(folder_path, filePath + 'atmIdTable1.html')
                # shutil.move(folder_path, '/tmp/atmId.csv')
            doc1['ticketsCount'] = len(df['caseId'])
        else:
            df['ticketId'] = df['caseId'] + "." + str(0)
            doc1['caseId'] = df['caseId'][0]
            folder_path = self.create_folder(case_id=df['caseId'], ticket_id=str(0))
            caseTicket['ticketfolderPath'] = folder_path
            caseTicket['mailTohtmlFile'] = folder_path + '/'+'atmIdTable1.html'
            shutil.move(filePath + 'atmIdTable1.html', folder_path )
            # shutil.move('/tmp/atmId.csv',folder_path)
            shutil.move('/tmp/atmIdTable1.csv',folder_path)

            doc1['ticketsCount'] = len(df)
            doc1['bankName'] = "BOB"
            doc1['partnerId'] = "54619c820b1c8b1ff0166dfc"
            # print(doc1)
            print(caseTicket)
            case_details = df.to_dict(orient="records")
            caseTickets.insert(case_details)
            caseInvestigation.insert(doc1)
            investatus['Case_Tickets_Creation'] = "Completed"
            print("ok")
            # caseTickets.insert(caseTicket)
        return folder_path

    def case_map(self,body_content):

        caseState = ""

#        body = """Content-Transfer-Encoding: 7bit,Content-Type: text/plain; charset="UTF-8",&nbsp;Dear Team,&nbsp;,&nbsp;,Please find herewith the attached customer disputes cases as reported by BOB. Please confirm whether the overages for the given transactions have been increased by your custodians. We would like to have reverted on the same Please provide hard copy of CBR report when the EOD was performed after the transaction date & also NO Excess Cash Certificate on urgent basis for the below mentioned successful transactions for which we have received Pre- Arbitration,&nbsp;,&nbsp;,&nbsp;,ATM ID,TRANSACTION DATE(DD-MMM-YY)&nbsp;,TRAN NO,CARD NO,TRAN AMT,DISPUTED AMT,CRA,STATE,CITY,ZONE,EJ ERROR,CRA OVERAGE AMT(REAL TIME)&nbsp;,CRA OVERAGE DATE,CRA REMARKS,CRA JUSTIFICATIONS&nbsp;,CRA ACCEPTED/NOT ACCEPTED,1FDNDL155,21-Apr-19,8475,607093******3346,10000,10000,CMS,DELHI,DELHI,NORTH,UNSUCCESSFUL,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,Thanks &amp; Regards,SMITA NARKAR.DISCLAIMER: ================================================================================================================ &quot;The information contained in this e-mail message may be privileged and/or confidential and protected from disclosure under applicable law. It is intended only for the individual to whom or entity to which it is addressed as shown at the beginning of the message. If the reader of this message is not the intended recipient, or if the employee or agent responsible for delivering the message is not an employee or agent of the intended recipient, you are hereby notified that any review, dissemination,distribution, use, or copying of this message is strictly prohibited. If you have received this message in error, please notify us immediately by return e-mail and permanently delete this message and your reply to the extent it includes this message. Any views or opinions presented in this message or attachments are those of the author and do not necessarily represent those of the Company. All e-mails and attachments sent and received are subject to monitoring, reading, and archival by the Company&quot; ================================================================================================================"""

        for sentence in nltk.sent_tokenize(body_content):
            analysis = TextBlob(sentence)
        #     print(analysis.pos_tags)
        #     for x in analysis.noun_phrases:
        #         if x in pre_arb_map:
        #             caseState = "Pre-arbitration"
        #         elif x in arb_map:
        #             caseState = "Arbitration"
        #         elif x in BO_map:
        #             caseState = "BO"
        #         else:
            caseState = "Pre-arbitration"
        return caseState

    def create_folder(self, case_id, ticket_id):
        path = "/tmp/" + strftime("%Y-%m-%d", gmtime()) + "/" + case_id[0] + "/" + str(ticket_id)
        if not os.path.exists(path):
            os.makedirs(path)
            print(path)
        return path

    def findCorresondEJ(atmId):
        pass

    def extractData(self,f):
        doc = {}
        error_map = {0: "none", "050": "UNAUTHORIZED CARD USAGE", "058": "INSUFFICIENT FUNDS",
                     "059": "INSUFFICIENT FUNDS",
                     "063": "DAILY WD LIMIT REACHED", "061": "ENTER LESSER AMOUNT",
                     "068": "CONTACT BANK", "072": "UNABLE TO PROCESS", "074": "CONTACT BANK",
                     "150": "UNAUTHORIZED CARD USAGE"}
        for i, line in enumerate(f):
            if re.match(r'^((0|1|2)\d{1})/((0|1)\d{1})/[0-9]{2} [0-9]{2}:[0-9]{2}$', line):
                doc['Date'] = line

            for type in ['ATM ID', 'REF NO', 'RRN NO', 'CARD NUMBER', 'ACCOUNT NO', 'RESP CODE', 'SEQ NO',
                         'RESP CDE']:
                if type in line:
                    doc[type] = re.search('(:[a-zA-z 0-9]*)', line).group().strip().replace(":", "") if re.search(
                        '(:[a-zA-z 0-9]*)', line) else ''
                    break

            for tran in ['BALANCE INQUIRY', 'CASH WITHDRAWAL', 'WITHDRAWAL', 'INQUIRY']:
                if tran in line:
                    doc['TRAN_TYPE'] = tran
                    break

            for amount in ['AVAIL. BALANCE', 'TRANS AMOUNT']:
                if amount in line:
                    doc[amount] = re.search('(RS.[0-9. ]*)', line).group().replace("RS.", "").strip() if re.search(
                        '(RS.[0-9.]*)', line) else ''
                    break

            for code in ['RESP CDE']:
                if code in line:
                    if doc[code] in error_map.keys():
                        doc['ERROR CODE'] = error_map[doc[code]]

            # if 'RESP CODE' not in doc.keys() and 'TRANSACTION DECLINED' in line:
            #     doc['RESP CODE'] = self.tmpList[i + 1].strip()
            #     doc['REASON'] = self.tmpList[i + 2].strip()
            #     continue

            if 'NOTES PRESENTED' in line:
                groups = re.findall('([0-9,]+)', line)
                if groups:
                    doc['NOTES PRESENTED'] = groups[-1]

            # if 'CASH TOTAL' in line:
            #     self.getDispensedRecords(i, doc)
        return doc

    def parseEJ(self,fileName):
        tmp = []
        complete = []
        f = fileName  # EJ file should be kept here
        fp = io.open(f, 'r')
        lines = fp.readlines()

        for line in lines:
            if "BANK OF BARODA" in line:
                tmp.append(line)
                continue
            if len(tmp) >= 1:
                tmp.append(line)
                if "THANK YOU FOR BANKING WITH US" in line:
                    resp = self.extractData(tmp)
                    complete.append(resp)
                    tmp = []
        df = pd.DataFrame(complete)
        df.to_csv(r'/tmp/atmId.csv')
        investatus['EJ_Txn_Details_Extracted'] = "Completed"
        # ej = df.to_dict(orient="records")
        # ejexecutiondetails.insert(ej)
        print(investatus)
        return

    def errorcomputation(self):
        details = {}
        data = pd.read_csv('/tmp/atmIdTable1.csv')
        data['SEQ NO']

        df = pd.read_csv('/home/hinclude/Desktop/file2.csv')

        df['ERROR CODE'].fillna("No error code available")

        error_map = {0: "none", "050": "UNAUTHORIZED CARD USAGE", "058": "INSUFFICIENT FUNDS",
                     "059": "INSUFFICIENT FUNDS",
                     "063": "DAILY WD LIMIT REACHED", "061": "ENTER LESSER AMOUNT",
                     "068": "CONTACT BANK", "072": "UNABLE TO PROCESS", "074": "CONTACT BANK",
                     "150": "UNAUTHORIZED CARD USAGE"}
        ATM_ID = ""
        error_dict = {}

        for x in df['SEQ NO']:
            if x == disputeTran:  # disputeTran
                details['tranNo'] = disputeTran
                details['tranStatus'] = "Withdrawal Successful"
                details['bankName'] = "Bank of Baroda"
                error = df.loc[df['SEQ NO'] == disputeTran, 'ERROR CODE'].item()
                error_index = df.index[df['SEQ NO'] == disputeTran].tolist()[0]
                details['cardNumber'] = df['CARD NUMBER'][error_index]
                details['dateTime'] = df['Date'][error_index]
                details['accountNumber'] = df['ACCOUNT NO'][error_index]
                details['transactionType'] = df['TRAN_TYPE'][error_index]
                for y in range(error_index - 5, error_index + 5, 1):
                    error1 = df['ERROR CODE'].fillna("Withdrawal Successful")[y]
                    error_dict[y] = error1
                    details['transactionStateupper'] = "Success"
                    details['transactionStatelower'] = "Success"
                    details['conclusion'] = "Power failure ATM working fine"
        print(error_dict)
        print(details)
        return error_dict

    def cbrcomputation(self,loadingdate):
        cbrdata = {}
        LoadingDate = 'Feb 15, 2019'
        df = pd.read_csv('/home/hinclude/Desktop/9611247StandardCBRReport.csv')
        cbr = df.to_dict(orient="records")
        data_index = df.index[df['Loading_Date'] == LoadingDate].tolist()[0]
        cbrdata['loadingDate'] = df['Loading_Date'][data_index]
        cbrdata['atmId'] = df['ATM ID'][data_index]
        cbrdata['overageTotal'] = df['Overage Total'][data_index]
        cbrdata['shortageTotal'] = df['Shortage Total'][data_index]
        cbrdata['replineshmentTotal'] = df['Replenishment Total'][data_index]
        cbrdata['midCashIncrese'] = df['Mid Cash Increase Total'][data_index]
        cbrdata['midCashDecrease'] = df['Mid Cash Decrease Total'][data_index]
        print(cbrdata)
        cbrdump.insert(cbr)
        return cbrdata

    def searchCaseState(self, body):
        for state in stateRegexPattern:
            if re.findall(state, body):
                return re.findall(state, body)[0]


if __name__ == '__main__':
    ATM_MONITOR().read_mail(username, password)