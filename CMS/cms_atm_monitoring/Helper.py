import config
import pandas as pd
from random import randint
import datetime
import os


# Generating TicketId
def generateTicketId(n):
    dateNow = datetime.datetime.now().date()
    dateNow = str(dateNow).replace('-', '')
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    ticketId = str(dateNow) + str(randint(range_start, range_end))
    return ticketId


def create_folder(path, folderName):
    folderName = path + folderName
    if os.path.exists(path) and not os.path.exists(folderName):
        os.makedirs(folderName)
    return path + folderName
