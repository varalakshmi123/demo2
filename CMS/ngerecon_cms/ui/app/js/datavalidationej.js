'use strict';
var app = angular.module('nextGen');

app.controller('datavalidationejController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', '$http', "CaseTicketsService",
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, $http, CaseTicketsService) {

        $scope.configurationSteps =
            ['ticketInfo', 'ticketStatus'];

        $scope.statusAvailable =
            ['OPEN', 'IN PROGRESS', 'CLOSED', 'REJECTED']

        $scope.errorCodes = ['ATM Is Fine, Not Working Properly',
            'New Error Code Found',
            'Fine this thing'
        ]

        $scope.errorTypes = ['Network Error', 'Hardware Error', 'Custodian Error']
        $scope.overageStatusOptions = ['yes', 'no','NA']
        $scope.switchStatusOptions = ['yes', 'no','NA']
        $scope.regionTypes = ['OPEN REGION', 'CLOSED']
        $scope.investigationTypes = ['Close with Claim Acceptance / Partial Acceptance', 'Close with Claim Rejection']
        $scope.rejectionTypes = ['No Overage Reported', 'Switch Increased', 'Cash Deposited', 'Overage Reported', 'Switch Reversal', 'Overage Wrongly Reported', 'Overage Not Applicable']
        // $scope.headers = Object.keys($rootScope.analysingTicket['hostTotals'][0])
        //
        // CaseTicketsService.getErrorCode('dummy',function (data) {
        //     $scope.errorCodes = data['errorCodes']
        //     $scope.errorCodeDescription = data['errorCodeDescription']
        //     $scope.errorCategory = data['errorCategory']
        // })

        // $rootScope.ejSummaryDetails = {'CASH WITHDRAWL': 3, 'BALANCE ENQUIRY': 0, 'UNABLE DISPENSE': 0}

        $scope.recommdations = ['Not Valid Ticket', 'Valid Ticket']

        $scope.present = 'ticketInfo';
        $scope.next = 'ticketStatus';

        $scope.nextStep = function (present) {
            $scope.prev = present;
            $scope.present = $scope.next;
            $scope.next = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) + 1];
            $scope.active[present] = true;
        };

        $scope.previousStep = function (present) {
            $scope.next = present;
            $scope.present = $scope.prev;
            $scope.active[present] = false;
            if ($scope.configurationSteps.indexOf($scope.prev) != 0) {
                $scope.prev = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) - 2];
            }
        };

        $scope.showTicketslist = function () {
            $location.path("/createInvestigation")
        }

        $scope.saveTicketStatus = function () {
            // if ($rootScope.analysingTicket.recommdations != undefined || $rootScope.analysingTicket.recommdations != '' || $rootScope.analysingTicket.errorCode != undefined || $rootScope.analysingTicket.errorCode != '') {
            //     $rootScope.analysingTicket.status = 'InProgress'
            // }
            // else if(){
            //     $rootScope.analysingTicket.recommdations = 'Closed'
            // }
            CaseTicketsService.put($rootScope.analysingTicket._id, $rootScope.analysingTicket, function (data) {
                $rootScope.showMessage('Ticket', "Status Is Updated", "Success")
            })
        }

        $scope.downloadEJTrans = function (key) {
            console.log(key)
        }


        $scope.updateStatus = function () {
            CaseTicketsService.put($rootScope.analysingTicket['_id'], $rootScope.analysingTicket, function (data) {
                console.log(data)
                $location.path("/createInvestigation")
            })
        }
    }
]);



