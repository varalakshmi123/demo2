'use strict';
var app = angular.module('nextGen');
app.controller('createInvestigationController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'CasesInvestigationService', 'CaseTicketsService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, CasesInvestigationService, CaseTicketsService) {

        $scope.casesOpen = []
        $scope.casesClosed = []
        $scope.caseTickets = []
        $scope.addComments = false
        $scope.editStatus = false
        $scope.editErrorStatus = false


        $scope.stepsCovered = {
            'Auto Parsing Mail': 'autoMailParsing', 'Case-Ticket Creation': 'caseTicketCreation',
            'EMAIL Content Extraction': 'emailContentExtraction',
            'Attachment Download': 'attachmentDownload',
            'EJ Txn Details Extracted': 'ejTxnDetailsExtracted',
            'Error Code Resolved': 'ejErrorCode',
            'CBR Details Verifed': 'cbrDetailsFound',
            'Auto Rule Validation': 'autoRuleValidation',
            'Case Resolved': 'caseResolved'
        }

        // $scope.completedSteps = {
        //     'Auto Parsing Mail': true,
        //     'Case-Ticket Creation': true,
        //     'EMAIL Content Extraction': true,
        //     'Attachment Download': true,
        //     'EJ Txn Details Extracted': true,
        //     'Error Code Resolved': 'inprogress',
        //     'CBR Details': false,
        //     'Auto Rule Validation': false,
        //     'Case Resolved': false
        // }

        CasesInvestigationService.get(undefined, undefined, function (data) {
            if (data.total > 0) {
                angular.forEach(data['data'], function (v, k) {
                    if (v.status == 'open') {
                        $scope.casesOpen.push(v)
                    }
                    else {
                        $scope.casesClosed.push(v)
                    }
                })
                if (angular.isDefined($rootScope.analysingTicket) & (angular.isDefined($rootScope.caseIndex))) {
                    // $scope.viewTickets = true;
                    $scope.viewCaseTickets($rootScope.caseIndex)
                }
            }
            else {
                $rootScope.showMessage('Cases', "No Case Found", 'warning')
            }

        })

        $scope.showCaseStateFlow = function (index) {
            $scope.stateFlow = true
            $scope.completedSteps = $scope.caseTickets[index]['flowStatus']
        }

        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };

        $scope.selectedValues = [{'filed': ' ', 'value': ' '}]
        $scope.availableFields = ['ACCOUNT NO', 'CARD NUMBER', 'ATM ID', 'TRANS AMOUNT']
        $scope.customerClaims = false
        $scope.supervisorRecords = false
        $scope.timeListTab = false
        $scope.displayDetails = false
        $scope.investDeta = {}
        $scope.atmIdSupervisor = ''
        $rootScope.ejSummaryDetails = {}
        $scope.supervisorCheck = false
        $scope.view = true
        var mailPath = 'file://usr/share/nginx/www/ngerecon_cms/ui/app/partials/atmIdTable.html'
        $scope.finalData = false

        $scope.analyseTicket = function (index) {
            $rootScope.analysingTicket = $scope.caseTickets[index]
            if ($scope.caseTickets[index]['status'] != 'RESOLVED') {
                $scope.caseTickets[index]['status'] = 'IN PROGRESS'
                CaseTicketsService.put($rootScope.analysingTicket['_id'], $rootScope.analysingTicket, function (data) {
                    console.log($rootScope.analysingTicket)
                })
            }

            $rootScope.analysingTicket['creationTime'] = $scope.casesOpen[$rootScope.caseIndex]['creationTime']


            // CaseTicketsService.formatTheTicket('dummy', $rootScope.analysingTicket, function (data) {
            //     $rootScope.renamedAnalysingTicket = data['dict1']
            //     $rootScope.analysingTicket = data['query']
            //     if (Object.keys(data).indexOf('enqTxn') !== -1) {
            //         $rootScope.ejSummaryDetails['enqTxn'] = data['enqTxn']
            //         $rootScope.ejSummaryDetails['summary'] = data['ejSummary']
            //         $rootScope.ejSummaryDetails['investSummary'] = data['investSummary']
            //         $rootScope.ejSummaryDetails['missingSeq'] = data['missingSeq']
            //         $rootScope.ejSummaryDetails['cbrList'] = data['cbr']
            // $rootScope.analysingTicket['investigationStatus'] = data['investigationStatus']
            // }
            // })
            $location.path('/datavalidationej')
        }

        $scope.viewCaseTickets = function (index) {
            $scope.viewTickets = true
            $rootScope.caseIndex = index;
            var query = {}
            query['ticketId'] = $scope.casesOpen[index]['ticketId']

            CaseTicketsService.get(undefined, {'query': {'perColConditions': query}, "limit": 0}, function (data) {
                if (data.total > 0) {
                    $rootScope.selectedCase = $scope.casesOpen[index]
                    $scope.caseTickets = data.data
                }
                else {
                    $rootScope.showMessage('Tickets', "No Tickets Present", 'warning')
                }
            });
        }

        $scope.viewMail = function (val, index) {
            var mailPath = '/app/files/'
            if (index != undefined) {
                var query = {}
                query['ticketId'] = $scope.caseTickets[index]['ticketId']
                query['ticketId'] = $scope.caseTickets[index]['ticketId']
                query['creationTime'] = $scope.casesOpen[$rootScope.caseIndex]['creationTime']
                query['atmId'] = $scope.caseTickets[index]['atmId']
                query['fileRequired'] = 'mail'
            } else {
                var query = {}
                query['ticketId'] = val['ticketId']
                query['creationTime'] = val['creationTime']
                query['fileRequired'] = 'mail'
            }
            CaseTicketsService.getFilePath('dummy', query, function (data) {
                if (data) {
                    $window.open(mailPath + data)
                }
                else {
                    $rootScope.showMessage('Mail', 'No Found', 'warning')
                }
            }, function (err) {
                $rootScope.showMessage('Mail', 'No Found', 'warning')
            });
        }

        $scope.downloadEJFile = function (index) {
            var mailPath = '/app/files/'
            var query = {}
            query['ticketId'] = $scope.caseTickets[index]['ticketId']
            // query['ticketId'] = $scope.caseTickets[index]['ticketId']
            query['ejFile'] = $scope.caseTickets[index]['ejFile']
            query['creationTime'] = $scope.casesOpen[$rootScope.caseIndex]['creationTime']
            query['atmId'] = $scope.caseTickets[index]['atmId']
            query['fileRequired'] = 'ej'
            CaseTicketsService.getFilePath('dummy', query, function (data) {
                if (data) {
                    var filePath = 'app/files/' + data
                    $window.open(filePath)
                }
                else {
                    $rootScope.showMessage('EJ', 'No Found', 'warning')
                }
            }, function (err) {
                $rootScope.showMessage('EJ', 'No Found', 'warning')
            });
        }

        $scope.removeCase = function (index) {
            var id = $scope.casesClosed[index]['_id']
            var ticketId = $scope.casesClosed[index]['ticketId']
            $scope.casesClosed.splice(index, 1)
            CasesInvestigationService.deleteData(id, function (data) {
                angular.forEach($scope.caseTickets, function (v, k) {
                    if (v['id'] == ticketId) {
                        CaseTicketsService.deleteData(v['_id'], function (data) {
                            console.log(data)
                        })
                    }
                })
                $rootScope.showMessage('Case', 'Case Removed SuccessFully', 'success')
                console.log(data)
            })
        }

        $scope.viewSummary = function () {
            $scope.showSummary = true
            CasesInvestigationService.getSummary('dummy', function (data) {
                $scope.summaryDetails = data
            })
        }

        // $scope.caseStateCount = [{"state": "OPEN", "count": 50}, {"state": "RESOLVED", "count": 3}]

        $scope.getTicketDetails = function (index) {
            $uibModal.open({
                templateUrl: 'ticketDetails.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "lg",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService, ReconProcessService) {
                    $scope.ticket = $scope.casesOpen[index]
                    $scope.caseStateCount = [{
                        'state': 'RESOLVED',
                        'count': $scope.ticket['resolved']
                    }, {
                        'state': 'OPEN',
                        'count': $scope.ticket['openCase']
                    }]
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                }
            })
        }

    }
]);



