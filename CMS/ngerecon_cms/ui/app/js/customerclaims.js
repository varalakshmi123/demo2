'use strict';
var app = angular.module('nextGen');
app.controller('customerclaimsController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'ReconDetailsService',
    '$http', 'ReconExecDetailsLogService', 'ReconsService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, ReconDetailsService, $http,
              ReconExecDetailsLogService, ReconsService) {

        $scope.addAdvFilter = function () {
            $scope.advFilters.push({'selectedColumn': '', 'operation': '', 'value': '', 'jsonDtype': ''});
        }

        $scope.deleteAdvFilter = function (index) {
            $scope.advFilters.splice(index, 1);
        }


        $http.get('/app/json/advanceSearch.json')
            .success(function (data) {
                console.log(data)
                $scope.jsonData = data
                $scope.searchVal = Object.keys(data);
            })
            .error(function (data) {
                console.log('there was an error');
            });


        $scope.getOperation = function (params) {
            $scope.operationList = []
            $scope.filter = true
            $scope.pageIndex = 1
            $scope.columnDtypeMapper = $scope.transactionData['columnGrouper']
            angular.forEach($scope.columnDtypeMapper, function (val, key) {
                angular.forEach(val, function (v, index) {
                    if (v == params['selectedColumn']) {
                        params['j   sonDtype'] = key
                        $scope.operationList = Object.keys($scope.jsonData[key])
                    }
                })
            })

        }

                $scope.getpageIndex = function (index) {
            $scope.pageIndex = index;
        };

        $scope.pageIndex = 1

        $scope.getRecords = function (source) {
            $scope.advFilters = []
            var data = {}
            data['index'] = $scope.pageIndex
            data['pageSize'] = 50000
            data['source']=source
            $rootScope.openModalPopupOpen()
            ReconDetailsService.getEjData('dummy', data, function (data) {
                $scope.summary = false
                $scope.transactionData = data

                $scope.columnDefs = []
                $scope.columnDefs = [
                    {
                        headerName: '',
                        'width': 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }
                ]

                angular.forEach($scope.transactionData['columns'], function (k, v) {
                    $scope.columnDefs.push({'field': k, 'filter': 'agTextColumnFilter'})
                })
                $scope.maxPages = $scope.transactionData['maxPages']
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData['data'];

                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs)
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)
                // $scope.toolPanel = true
                $scope.sourcePanel = true
                // $rootScope.rightPanel = true
                $rootScope.openModalPopupClose()
            }, function (err) {
                $scope.initGrid()
                $rootScope.showMessage($rootScope.resources.records, $rootScope.resources.defaultMsg, 'warning')
            })
        }


        // $scope.getRecords()


        $scope.addExpression = function (searchParams) {
            var data = {}
            if (angular.isDefined(searchParams['selectedColumn'])) {
                data['expression'] = $scope.jsonData[searchParams['jsonDtype']][searchParams['operation']]
                data['expression'] = data['expression'].replace('column', searchParams['selectedColumn'])
                data['expression'] = data['expression'].replace('value', searchParams['value'])
                return data['expression']
            }
        }



        $scope.getFilteredRecord = function (searchParams,source) {
            var exp = []
            angular.forEach(searchParams, function (k, v) {
                exp.push($scope.addExpression(k))
            })
            var data = {}
            data['index'] = $scope.pageIndex
            data['pageSize'] = 5000
            data['filters'] = exp
            data['source']=source

            // if ($scope.search['stmtDate'] != null && angular.isDefined($scope.search['stmtDate'])) {
            //     data['stmtDate'] = $filter('date')(new Date($scope.toEpoch($scope.search.stmtDate) * 1000), 'dd/MM/yy')
            //     searchParams['stmtDate'] = $scope.search['stmtDate']
            // }

            $rootScope.openModalPopupOpen()
            ReconDetailsService.getFilteredData('dummy', data, function (data) {

                $scope.transactionData = data
                $scope.columnDefs = []

                angular.forEach($scope.transactionData['columns'], function (k, v) {
                    $scope.columnDefs.push({'field': k, 'filter': 'agTextColumnFilter'})
                })
                $scope.maxPages = $scope.transactionData['maxPages']
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData['data'];

                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs)
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)
                $rootScope.openModalPopupClose()

            }, function (err) {
                // TODO, use pop up notification
                console.log(err)
            })

        }



        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };

                $scope.initGrid = function () {
            $scope.gridOptions = {
                defaultColDef: {
                    // allow every column to be aggregated
                    enableValue: true,
                    // allow every column to be grouped
                    enableRowGroup: true,
                    // allow every column to be pivoted
                    enablePivot: false
                },
                columnDefs: [],
                enableSorting: true,
                showToolPanel: true,
                rowMultiSelectWithClick: true,
                enableFilter: true,
                enableColResize: true,
                groupSelectsChildren: true,
                groupSelectsFiltered: true,
                rowSelection: 'multiple',
                checkboxSelection: true,
                rowGroupPanelShow: 'always',
                floatingFilter: true,
                // suppressMenu:true,
                pagination: false,
                onSelectionChanged: selectionChangedFunc
            };
        };

        $scope.initGrid()

         function selectionChangedFunc() {
            $scope.selectedRecords = $scope.matchGridOptions.api.getSelectedRows();
            $scope.disableFlag = false;
            $scope.$apply();
        }



    }

]);



