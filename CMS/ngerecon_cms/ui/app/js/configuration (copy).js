var app = angular.module('nextGen');
app.controller('configurationController', ['$scope', '$route', '$location', '$timeout', '$uibModal',
    '$rootScope', '$http', 'ReconsService', 'fileUploadService', 'ReconProcessService', 'SourceReferenceService', 'MatchRuleReferenceService',
    function ($scope, $route, $location, $timeout, $uibModal, $rootScope, $http, ReconsService, fileUploadService, ReconProcessService, SourceReferenceService, MatchRuleReferenceService) {
        $scope.create = false;
        $scope.active = {};
        $scope.defineSource = false;
        $scope.feedDef = {};
        $scope.filters = {}
        $scope.sourceDetails = []
        $scope.fileUploaded = false;
        $scope.ruleDefined = []
        $scope.dataTypes = ['np.int64', 'np.float64', 'datetime64', 'str']
        $scope.create = false
        $scope.configurationSteps =
            ['defineSource', 'defineFilters', 'selectMatchColumns', 'resultsOfProcess', 'saveProcess'];

        $http.get('/app/json/summaryColumn.json').success(function (data) {
            $scope.summaryCols = data.columnTypes;
            $scope.loadTypes = data.loadTypes;
            $scope.delimeters = data.delimiters;
            angular.forEach(data.columnTypes, function (k, v) {
                $scope.selectedReportColumns[v.keyCol] = [];
            });
        });


        $scope.refreshReconView = function () {
            $scope.distinctReconProcess = []
            $scope.recon = {}
            $scope.reconDetails = []
            $scope.sources = []
            $scope.add = false
            $scope.listSource = false

            ReconsService.get(undefined, {'limit': 100}, function (data) {
                $scope.reconDetails = data.data
                angular.forEach(data.data, function (k, v) {
                    if ($scope.distinctReconProcess.indexOf(k.reconProcess) == -1) {
                        $scope.distinctReconProcess.push(k.reconProcess);
                        // $scope.image[k.reconProcess] = false;
                    }
                });
            });
            // ReconProcessService.get(undefined, undefined, function (data) {
            //     angular.forEach(data.data, function (v, k) {
            //         if (v.imagePath != undefined) {
            //             $scope.image[v.reconProcess] = true;
            //         }
            //     });
            // });
        }

        $scope.refreshReconView()


        $scope.getProcesRecons = function (index) {
            $scope.processRecons = []
            $scope.process = $scope.distinctReconProcess[index]
            $scope.expanded = true
            angular.forEach($scope.reconDetails, function (v, k) {
                if (v.reconProcess == $scope.distinctReconProcess[index]) {
                    $scope.processRecons.push(v);
                }
                ;
            });
        }

        $scope.createRecon = function (data) {
            $scope.create = true;
            $scope.present = 'defineSource';
            $scope.next = 'defineFilters';
        };

        $scope.editRecon = function (recon, index) {
            angular.forEach($scope.reconDetails, function (k, v) {
                if (k.reconName == recon.reconName) {
                    $scope.sources = k.sources;
                }
            });
            $scope.recon = recon;
            $scope.createRecon();
        }

        $scope.deleteRecon = function (val, index) {
            var index = index
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconsService) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteReconMsg']
                    $scope.values = $scope.processRecons[index]

                    $scope.submit = function () {
                        ReconsService.deleteData('dummy', $scope.values, function (data) {
                            $uibModalInstance.dismiss('cancel');
                            $scope.refreshReconView()
                            $scope.processRecons.splice(index, 1)
                            $rootScope.showMessage($rootScope.resources.delete, $rootScope.resources.deleteReconSucc, 'success')
                        }, function (err) {
                            console.log(err)
                        })
                    }

                    $scope.cancel = function () {
                        $scope.refreshReconView()
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            console.log($scope.reconDetails[index])
        }


        $scope.saveRecon = function () {
            var perColConditions = {
                'perColConditions': {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess
                }
            }

            ReconsService.get(undefined, {'query': perColConditions}, function (data) {
                if (data['total'] < 1) {
                    ReconsService.post($scope.recon, function (data) {
                        console.log(data);
                    })
                }


            })
        }


        $scope.nextStep = function (present) {
            $scope.prev = present;
            $scope.present = $scope.next;
            $scope.next = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) + 1];
            $scope.active[present] = true;
            if ($scope.prev == 'defineSource') {
                $scope.recon.sourceCount = $scope.sourceDetails.length;
                $scope.recon.sources = $scope.sources
                $scope.saveRecon()

                var perColConditions = {
                    'perColConditions': {
                        'reconName': $scope.recon.reconName,
                        'reconProcess': $scope.recon.reconProcess
                    }
                }
                SourceReferenceService.get(undefined, {'query': perColConditions}, function (data) {
                    $scope.sourceCols = {};
                    angular.forEach(data.data, function (v, k) {
                        $scope.sourceCols[v.source] = v.feedDetails;
                        $scope.filters[v['source']] = v['filters']
                    });
                });
            }

            if ($scope.prev == 'defineFilters') {
                var query = {
                    'reconName': $scope.recon.reconName,
                    'reconProcess': $scope.recon.reconProcess,
                    'filters': $scope.filters
                }
                SourceReferenceService.saveFilters('dummy', query, function (data) {
                    console.log(data);
                });
                var query = {
                    'perColConditions': {
                        'reconName': $scope.recon.reconName,
                        'reconProcess': $scope.recon.reconProcess
                    }
                }
                MatchRuleReferenceService.get(undefined, {'query': query}, function (data) {
                    $scope.matchId = 'dummy '
                    if (data != undefined) {
                        $scope.matchId = data.data[0]['_id'];
                        $scope.ruleDefined = data.data[0]['matchRules'];
                    }
                });
            }

            if ($scope.prev == 'selectMatchColumns') {
                var data = {}
                data.reconName = $scope.recon.reconName
                data.reconProcess = $scope.recon.reconProcess
                data.matchRules = $scope.ruleDefined

                MatchRuleReferenceService.saveMatchRule($scope.matchId, data, function (data) {
                    console.log(data);
                });

            }
        };

        $scope.previousStep = function (present) {
            $scope.next = present;
            $scope.present = $scope.prev;
            $scope.active[present] = false;
            if ($scope.configurationSteps.indexOf($scope.prev) != 0) {
                $scope.prev = $scope.configurationSteps[$scope.configurationSteps.indexOf($scope.next) - 2];
            }
        };

        $scope.uploadSourceFile = function () {
            var file = $scope.uploadFile;
            $scope.feedDef.fileName = file.name
            var uploadUrl = '/api/no_auth/source/upload';
            $rootScope.openModalPopupOpen()
            fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                $scope.post = data.msg;
                $scope.fileUploaded = true;
                $rootScope.openModalPopupClose()
                $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
            }).catch(function () {
                $scope.error = 'unable to get posts';
            });
        };

        $scope.uploadFeedFiles = function () {
            var file = $scope.uploadFeedFile;
            $scope.feedDef.referenceFile = file.name
            var uploadUrl = '/api/no_auth/file/upload';
            fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                $scope.post = data.msg;
                $scope.fileUploaded = true;
                $rootScope.openModalPopupClose()
                $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
            }).catch(function () {
                $scope.error = 'unable to get posts';
            });
        }
        $scope.saveSources = function (data) {
            $scope.sourcePresent = false;
            $scope.feedDef.reconName = $scope.recon.reconName
            $scope.feedDef.reconProcess = $scope.recon.reconProcess
            SourceReferenceService.saveSourceDetails('dummy', data, function (data) {
                $scope.defineSource = false;
                if ($scope.sources.indexOf(data.source) == -1) {
                    $scope.sources.push(data.source);
                }
                $scope.feedDef = {};
            });
        };

        //
        $scope.editSource = function (val) {
            var perColConditions = {
                'reconName': $scope.recon.reconName,
                'source': val,
                'reconProcess': $scope.recon.reconProcess
            }
            SourceReferenceService.get(undefined, {'query': {'perColConditions': perColConditions}}, function (data) {
                console.log(data);
                if ($scope.present == 'defineSource') {
                    $scope.defineSource = true;
                }
                $scope.feedDef = data.data[0];
            });
        };

        $scope.editDtypes = function (val) {
            $scope.editDataTypes = true;
            var perColConditions = {'reconName': $scope.recon.reconName, 'source': val};
            SourceReferenceService.get(undefined, {'query': {'perColConditions': perColConditions}}, function (data) {
                $scope.details = data.data[0];
                $scope.feedDetails = $scope.details.feedDetails;
            });
        };

        $scope.saveColumn = function (index, val) {
            $scope.feedDetails[val.position - 1] = val;
        };

        $scope.cancelSource = function () {
            $scope.defineSource = false;
            $scope.feedDef = {};
        };

        $scope.saveDataTypes = function () {
            $scope.editDataTypes = false;
            $scope.details.feedDetails = $scope.feedDetails
            SourceReferenceService.put($scope.details._id, $scope.details, function (data) {
                console.log(data);
            });
        };


        $scope.deleteSource = function (key) {
            $uibModal.open({
                templateUrl: '/app/components/notifications/confirmation.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance) {
                    $scope.deleteTitle = $rootScope.resources.confirmation
                    $scope.deleteMsg = $rootScope.resources['deleteMsg']
                    $scope.submit = function () {
                        var indx = $scope.sources.indexOf(key)
                        $scope.sources.splice(indx, 1)
                        angular.forEach($scope.reconDetails, function (v, k) {
                            if (v.reconName == $scope.recon.reconName) {
                                v.sources = $scope.sources;
                                ReconsService.put(v._id, v, function (data) {
                                    console.log(data)
                                    $uibModalInstance.dismiss('cancel');
                                })
                            }
                        })

                        SourceReferenceService.deleteSource('dummy', {
                            'reconName': $scope.recon.reconName,
                            'source': key
                        }, function (data) {
                            console.log(data)
                        })

                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        $scope.uploadFilters = function () {
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: 'md',
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"
                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/filter/upload";

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            if (angular.isDefined(file)) {
                                SourceReferenceService.loadSourceRef('dummy', {
                                    'filter': true,
                                    'fileName': file.name
                                }, function (data) {
                                    $scope.filters[$scope.filterSource] = data
                                    $scope.updateDerivedCols(data);
                                }, function (err) {
                                    console.log(err.msg);
                                });
                            }
                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.cancelDataTypes = function () {
            $scope.editDataTypes = false;
            $scope.feedDetails = [];
        }


        // filters sections starts from here
        $scope.enableEditing = {}
        $scope.filterCondExpArray = []
        $scope.filterCondArray = []
        $scope.mdlFieldStaticData = []
        $scope.selectedMdlFieldArray = []
        $scope.fieldCheckArray = []
        // $scope.filters = []
        $scope.filterConditionsMasterArray = {
            'str': ['notEqual', 'equal', 'startWith', 'notStartWith', 'endsWith', 'contains', 'notContains', 'notEndsWith', 'isIn', 'notIsIn'],
            'np.float64': ['>', '<', '>=', '<=', '!=', '=='],
            'np.int64': ['>', '<', '>=', '<=', '!=', '==']
        }
        var stringExpressions = {
            'notEqual': "(data['$colName'].str.strip()!='$colValue')",
            'equal': "(data['$colName'].str.strip()=='$colValue')",
            'startWith': "(data['$colName'].str.strip().startswith('$colValue'))",
            'notStartWith': "(~data['$colName'].str.strip().str.startswith('$colValue'))",
            'endsWith': "(data['$colName'].str.strip().endswith('$colValue'))",
            'notEndsWith': "(~data['$colName'].str.strip().endswith('$colValue'))",
            'contains': "(data['$colName'].str.strip().contains('$colValue')",
            'notContains': "(~data['$colName'].strip().str.contains('$colValue'))",
            'isIn': "(data['$colName'].strip().isin('$colValue'))",
            'notIsIn': "(~data['$colName'].strip().isin('$colValue'))"
        }

        var numbericExpression = {
            'condition': "data[data['$colName'] $operator $colValue]"
        }

        // $scope.getSourceColumns = function (selectedSource) {
        //     $scope.columnList = []
        //     angular.forEach($scope.sourceDetails, function (val, index) {
        //         if (val['source'] == selectedSource) {
        //             $scope.columnList = val['columns']
        //         }
        //     })
        // }
        $scope.addFilterCond = function () {
            $scope.filterCondExpArray.push({condValue: ''})
        }

        $scope.removeFilterCond = function (index) {
            $scope.filterCondExpArray.splice(index, 1)
        }

        $scope.removeFilterExp = function (index) {
            $scope.filters.splice(index, 1)
        }

        $scope.onChangeOfMdlField = function (val, index) {
            $scope.filterCondExpArray[index]['filterCondArray'] = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
            //$scope.filterCondArray = $scope.filterConditionsMasterArray[val['selectedField']['dataType']]
        }

        $scope.onChangeOfFilterCond = function (val) {

        }

        $scope.buildExpression = function () {
            var str = "data = data[";

            for (i = 0; i < $scope.filterCondExpArray.length; i++) {

                if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'str') {
                    str += buildStringCondition($scope.filterCondExpArray[i])
                }
                else if ($scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.int64' || $scope.filterCondExpArray[i]['selectedField']['dataType'] == 'np.float64') {
                    str += buildNumericCondition($scope.filterCondExpArray[i])
                }

                // append logical operator if any
                if (angular.isDefined($scope.filterCondExpArray[i]['logicalOperator'])) {

                    if ($scope.filterCondExpArray[i]['logicalOperator'] != ''
                        && $scope.filterCondExpArray.length - 1 > i) {
                        str += ' ' + $scope.filterCondExpArray[i]['logicalOperator'] + ' '
                    }
                }
            }

            str += "]"
            return str;
        }


        function buildStringCondition(value) {

            if (value['selectedCond'] == 'isIn' || value['selectedCond'] == 'notIsIn') {
                var condValue = ''
                var condValue = '[' + value['condValue'].replace("^", "'").replace('$', "'").replace(/,/g, "','") + ']'
                return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['fileColumn']).replace('$colValue', condValue)
            } else {
                return stringExpressions[value['selectedCond']].replace('$colName', value['selectedField']['fileColumn']).replace('$colValue', value['condValue'] == undefined ? '' : value['condValue'])
            }
        }

        function buildNumericCondition(value) {
            return numbericExpression['condition'].replace('$colName', value['selectedField']['fileColumn']).replace('$operator', value['selectedCond']).replace('$colValue', value['condValue'])
        }


        $scope.clearFields = function () {
            $scope.selectedFeed = {}
            resetFeedDefination()
            $scope.getMdlFields()
        }

        // $scope.removeFilterCond = function (index) {
        //     $scope.filterCondExpArray.splice(index, 1)
        // }

        // $scope.removeFilterExp = function (index) {
        //     $scope.filters.splice(index, 1)
        // }


        function generateRandomNumber() {
            return Date.now() + Math.floor((Math.random() * 100) + 1);
        }


        $scope.saveFilterCond = function () {
            if (!angular.isDefined($scope.filters[$scope.selectedSource])) {
                $scope.filters[$scope.selectedSource] = []
            }

            if ($scope.filterCondExpArray.length > 0) {
                $scope.filters[$scope.selectedSource].push($scope.buildExpression())
                $scope.filterCondExpArray = [{}]
            }
        }

        // Matching Rule Define section starts
        $scope.addNewMatchRule = function (mode, index) {
            $scope.mode = mode
            $scope.matchRules = {}
            if ($scope.mode == 'Add') {
                angular.forEach($scope.sources, function (k, v) {
                    $scope.matchRules[k] = {'matchColumns': '', 'sumColumns': ''};
                });
            }
            if ($scope.mode == 'Edit') {
                $scope.ruleName = $scope.ruleDefined[index]['ruleName']
                $scope.matchRules = $scope.ruleDefined[index]['columns']
            }
            $uibModal.open({
                templateUrl: 'ruleDefine.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: 'md',
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }

                    $scope.saveRule = function (ruleName) {
                        if ($scope.mode == 'Edit') {
                            $scope.ruleDefined[index]['ruleName'] = ruleName;
                            $scope.ruleDefined[index]['columns'] = $scope.matchRules
                        }
                        else {
                            $scope.ruleDefined.push({'ruleName': ruleName, 'columns': $scope.matchRules})

                        }
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            })
        }

        $scope.uploadFilters = function () {
            $scope.deleteModel = $uibModal.open({
                templateUrl: 'app/components/notifications/fileupload.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, fileUploadService) {
                    $scope.title = "File Upload"

                    $scope.upload = function () {
                        var file = $scope.uploadFile;
                        var uploadUrl = "/api/no_auth/filter/upload"; //Url of webservice/api/server

                        fileUploadService.uploadFileToUrl(file, uploadUrl).then(function (data) {
                            $scope.post = data.msg;
                            $rootScope.openModalPopupClose()
                            $rootScope.displayMessage('File Uploaded Successfully', 'md', 'success');
                            $uibModalInstance.dismiss('cancel');
                            $scope.plate = true
                            if (angular.isDefined(file)) {
                                SourceReferenceService.loadSourceRef('dummy', {
                                    'filter': true,
                                    'fileName': file.name
                                }, function (data) {
                                    $scope.feedDef.filters = data
                                    $scope.updateDerivedCols(data)
                                }, function (err) {
                                    console.log(err.msg)
                                })
                            }

                        }).catch(function () {
                            $scope.error = 'unable to get posts';
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

    }]);