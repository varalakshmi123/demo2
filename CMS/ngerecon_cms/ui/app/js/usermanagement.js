'use strict';
var app = angular.module("nextGen");
app.controller('userManagementController', ["$rootScope", "$scope", '$uibModal', "UsersService","AuditsService", function ($rootScope, $scope, $uibModal, UsersService,AuditsService) {
    $scope.getUserDetails = function () {
        UsersService.get(undefined, {'skip': 0, 'limit': $rootScope.rowLimit}, function (data) {
            $scope.userDetails = data['data']
            $scope.totalItems = data['total']
        }, function (err) {
            console.log(err.msg)
        });
    }

    $scope.userUnlock = false
    $scope.approveList = ['Not Approved', 'Approved']
    $scope.reportApprove = ['Allow', 'Not Allow']
    $scope.Details = {}
    $scope.rowData = [];

    $scope.setEdit = function (rowData) {
        $scope.edit = true
        console.log(rowData)
        $scope.selectedRow = rowData
    }

    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields,
                                                  orderBy, orderByReverse) {
        if (Object.keys($rootScope).indexOf('genDataTableQuery') != -1) {
            UsersService.get(undefined, $rootScope.genDataTableQuery(currentPage, pageItems,
                filterBy, filterByFields, orderBy, orderByReverse), function (data) {
                $scope.userDetails = data['data']
                $scope.totalItems = data['total']
            }, function (err) {
                console.log(err.msg)
            })
        }
    }

    $scope.setUnlock = function (details) {
        $scope.userUnlock = true
        $uibModal.open({
            templateUrl: '/app/components/notifications/confirmation.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            size: "md",
            controller: function ($uibModalInstance, $scope, $rootScope, UsersService) {
                $scope.deleteTitle = $rootScope.resources['unlockUser']
                $scope.deleteMsg = $rootScope.resources['unlockUserMsg']

                $scope.submit = function () {
                    var msg = 'Successfully Updated'
                    if ($scope.userUnlock && details['accountLocked']) {
                        details['accountLocked'] = false
                        details['loginCount'] = 0
                        $uibModalInstance.dismiss('cancel')
                        UsersService.put(details['_id'], details, function (data) {
                             $scope.eventDoc = {
                            'type': 'User Unlocked',
                            'actionOn': details['userName'],
                             'logTime': Date.now(),
                            'createdBy': $rootScope.userData['userName'],
                        }
                          AuditsService.post($scope.eventDoc)
                            $rootScope.showMessage('Users', $rootScope.resources['unlockSuccess'], 'success')
                            $scope.userUnlock = false;
                        }, function (err) {
                            console.log(err)
                        })
                    }
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }

    $scope.saveUserDeatils = function (details) {
        UsersService.put(details['_id'], details, function (data) {
            $rootScope.showMessage('Users', $rootScope.resources['userUpdateSuccess'], 'success')
            $scope.userUnlock = false

        }, function (err) {
            console.log(err)
        })
    }

    $scope.removeUser = function (gridItem) {
        $uibModal.open({
            templateUrl: '/app/components/notifications/confirmation.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            size: "md",
            controller: function ($uibModalInstance, $scope, $rootScope, ReconAssignmentService) {
                $scope.deleteTitle = $rootScope.resources['removeUser']
                $scope.deleteMsg = $rootScope.resources['removeUserMsg']
                $scope.submit = function () {

                    UsersService.deleteUser(gridItem['_id'], gridItem, function (data) {
                            $scope.eventDoc = {
                                'type': 'User deleted',
                                'actionOn': gridItem['userName'],
                                'logTime': Date.now(),
                                'createdBy': $rootScope.userData['userName'],
                            }
                            AuditsService.post($scope.eventDoc, function (data) {
                                console.log(data)
                            })
                            $uibModalInstance.dismiss('cancel')
                            $rootScope.showMessage('Users', $rootScope.resources['removeSuccess'], 'success')
                            $scope.getUserDetails()
                        },
                        function (err) {
                            console.log(err)
                        })
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }
    $scope.getUserDetails()

    $scope.editUserDetails = function (index) {
        $scope.editUser = $scope.userDetails[index]
        $uibModal.open({
            templateUrl: 'editUser.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            size: "md",
            controller: function ($uibModalInstance, $scope, $rootScope, UsersService) {



                $scope.submit = function (details) {

                    $scope.eventDoc = {
                        'type': 'User Privileges updated',
                        'actionOn': details.userName,
                        'logTime': Date.now(),
                        'createdBy': $rootScope.userData['userName']
                    }
                    AuditsService.post($scope.eventDoc)

                    UsersService.put(details['_id'], details, function (data) {
                        $rootScope.showMessage('Users', $rootScope.resources['updateSuccess'], 'success')
                        $scope.userUnlock = false
                        $uibModalInstance.dismiss('cancel')
                    }, function (err) {
                        console.log(err)
                        $rootScope.showMessage('Error', err.msg, 'error')
                        $uibModalInstance.dismiss('cancel')
                    })
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }
}]);






