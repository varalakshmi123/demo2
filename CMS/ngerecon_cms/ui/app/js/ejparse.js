'use strict';
var app = angular.module('nextGen');
app.controller('ejparseController', ['$scope', '$filter', '$route', '$location',
    '$window', '$timeout', '$rootScope', '$uibModal', 'ReconDetailsService',
    '$http', 'ReconExecDetailsLogService', 'ReconsService',
    function ($scope, $filter, $route, $location, $window, $timeout, $rootScope,
              $uibModal, ReconDetailsService, $http,
              ReconExecDetailsLogService, ReconsService) {
        $scope.selReconDetail={}


        $scope.toEpoch = function (date) {
            return Math.round(new Date(date) / 1000.0);
        };

                $scope.initGrid = function () {
            $scope.gridOptions = {
                defaultColDef: {
                    // allow every column to be aggregated
                    enableValue: true,
                    // allow every column to be grouped
                    enableRowGroup: true,
                    // allow every column to be pivoted
                    enablePivot: false
                },
                columnDefs: [],
                enableSorting: true,
                showToolPanel: true,
                rowMultiSelectWithClick: true,
                enableFilter: true,
                enableColResize: true,
                groupSelectsChildren: true,
                groupSelectsFiltered: true,
                rowSelection: 'multiple',
                checkboxSelection: true,
                rowGroupPanelShow: 'always',
                floatingFilter: true,
                // suppressMenu:true,
                pagination: false,
                onSelectionChanged: selectionChangedFunc
            };
        };

        $scope.initGrid()

         function selectionChangedFunc() {
            $scope.selectedRecords = $scope.matchGridOptions.api.getSelectedRows();
            $scope.disableFlag = false;
            $scope.$apply();
        }




        $scope.uploadFeedFile = function () {
            $uibModal.open({
                templateUrl: 'job.html',
                windowClass: 'modal show modalDialogCenter',
                backdrop: 'static',
                backdropClass: 'show',
                scope: $scope,
                keyboard: false,
                size: "md",
                controller: function ($scope, $rootScope, $uibModalInstance, ReconDetailsService, $upload) {
                    $scope.upload = function (files) {
                        $scope.fileName = files[0].name
                        $scope.runReconJob = function () {
                            $rootScope.openModalPopupOpen()
                            // $scope.selReconDetail['statementDate'] = $filter('date')($scope.toEpoch($scope.selReconDetail['statementDate']) * 1000, 'dd/MM/yyyy')
                            $scope.name = files[0].name.split('.')
                            $scope.array = [$scope.name[0], $scope.name[1].toLowerCase()]
                            $scope.id = $scope.array.join('.')


                            $upload.upload({
                                url: '/api/utilities/' + $scope.id + '/upload_ej_files',
                                method: 'POST',
                                file: files[0],
                                data: $scope.selReconDetail
                            }).progress(function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            }).success(function (data, status, headers, config) {
                                if (data['status'] == "error") {
                                    $rootScope.showMessage('JobStatus', data['msg'], 'warning')
                                }
                                else {
                                    $rootScope.showMessage('JobStatus', data['msg'], 'success')
                                }
                                $rootScope.openModalPopupClose()
                                $uibModalInstance.dismiss('cancel');
                            });
                        };
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel')
                    }
                }
            });
        };


         $scope.getpageIndex = function (index) {
            $scope.pageIndex = index;
        };



        $scope.pageIndex = 1

        $scope.getRecords = function (source) {
            var data = {}
            data['index'] = $scope.pageIndex
            data['pageSize'] = 50000
            data['source']=source
            $rootScope.openModalPopupOpen()
            ReconDetailsService.getEjData('dummy', data, function (data) {
                $scope.summary = false
                $scope.transactionData = data
                $scope.columnDefs = []
                $scope.columnDefs = [
                    {
                        headerName: '',
                        'width': 30,
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        suppressAggregation: true,
                        suppressSorting: true,
                        suppressMenu: true,//headerCellTemplate: headerCellRendererFunc,
                        pinned: true
                    }
                ]


                angular.forEach($scope.transactionData['columns'], function (k, v) {
                    $scope.columnDefs.push({'field': k, 'filter': 'agTextColumnFilter'})
                })
                $scope.maxPages = $scope.transactionData['maxPages']
                $scope.gridOptions.columnDefs = $scope.columnDefs;
                $scope.gridOptions.rowData = $scope.transactionData['data'];

                $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs)
                $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)


                // $scope.toolPanel = true
                $scope.sourcePanel = true
                // $rootScope.rightPanel = true
                $rootScope.openModalPopupClose()
            }, function (err) {
                $scope.initGrid()
                $rootScope.showMessage($rootScope.resources.records, $rootScope.resources.defaultMsg, 'warning')
            })
        }



    }

]);



