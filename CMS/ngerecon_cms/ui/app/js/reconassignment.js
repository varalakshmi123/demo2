/**
 * Created by kedarnath on 8/7/18.
 */

var app = angular.module('nextGen')

app.controller('reconAssignmentController', ['$scope', '$rootScope', 'ReconsService', '$uibModal', 'UsersService', 'ReconAssignmentService', 'AuditsService', function ($scope, $rootScope, ReconsService, $uibModal, UsersService, ReconAssignmentService, AuditsService) {

    $scope.filteredProcess = []
    $scope.filteredRecons = []
    $scope.reconAssign = {}
    $scope.mode = "Add";
    $scope.display = true
    $scope.userNames = []

    $scope.completeList = []
    $scope.dictVal = {}

    $scope.getRecons = function () {
        $scope.process = []
        ReconsService.get(undefined, {'limit': 100}, function (data) {
            $scope.recons = data.data
            angular.forEach($scope.recons, function (val, index) {
                if (!angular.isDefined($scope.dictVal[[val.reconProcess]])) {
                    $scope.dictVal[[val.reconProcess]] = false;
                }

                $scope.currentProcess = val.reconProcess
                $scope.completeList.push({
                    'name': '<strong>' + $scope.currentProcess +
                    '<strong>', 'msGroup': true
                });
                angular.forEach($scope.recons, function (v, i) {
                    if (v.reconProcess == $scope.currentProcess &&
                        $scope.dictVal[[val.reconProcess]] == false) {
                        if ($scope.tempRecons.indexOf(v.reconName) == -1) {
                            $scope.completeList.push
                            ({'name': v.reconName, 'ticked': false});
                        }
                        else {
                            $scope.completeList.push
                            ({'name': v.reconName, 'ticked': true});
                        }
                    }
                })
                $scope.completeList.push({'msGroup': false})
                $scope.dictVal[[val.reconProcess]] = true;
            })
            console.log($scope.completeList);
        });
    }

    $scope.onServerSideItemsRequested = function (currentPage, pageItems,
                                                  filterBy, filterByFields, orderBy, orderByReverse) {
        $scope.crntPage = currentPage
        $scope.pageCount = pageItems
        $scope.filterFrom = filterBy
        $scope.filterWithFields = filterByFields
        $scope.orderWith = orderBy
        $scope.orderReverse = orderByReverse

        ReconAssignmentService.get(undefined, $rootScope.genDataTableQuery(currentPage, pageItems,
            filterBy, filterByFields, orderBy, orderByReverse), function (data) {
            $scope.assignedData = data.data;
            $scope.totalItems = data.total;
        });
    }


    $scope.getUserList = function () {
        $scope.userDetails = []
        UsersService.get(undefined, undefined, function (data) {
            angular.forEach(data.data, function (v, k) {
                if (v.role != 'admin' && v.ApprovalStatus == 'Approved') {
                    $scope.userDetails.push(v);
                    $scope.userNames.push({'name': v.userName, 'ticked': false});
                }
            });
        });
    }

    $scope.setCreate = function () {
        $scope.dictVal = {}
        $scope.selectedProcesses = []
        $scope.reconAssign = {}
        $scope.filteredProcess = []
        $scope.filteredRecons = []
        $scope.tempRecons = []
        $scope.tempUsers = []
        $scope.userNames=[]
        $scope.tempProcess = []
        $scope.completeList = []
        $scope.mode = 'Add'
        $scope.getRecons()
        $scope.openAssignment();
    }

    $scope.openAssignment = function (data) {
        $uibModal.open({
            templateUrl: 'reconassignment.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            controller: function ($uibModalInstance, $scope, $rootScope) {
                $scope.assignRecon = function (data) {
                    $scope.reconAssign.reconNames = []
                    $scope.reconAssign.reconProcesses = []
                    $scope.reconAssign.users = []

                    angular.forEach($scope.selectedRecons, function (v, k) {
                        $scope.reconAssign.reconNames.push(v.name);
                    })

                    angular.forEach($scope.selectedUsers, function (v, k) {
                        $scope.reconAssign.users.push(v.name);
                    })

                    angular.forEach($scope.recons, function (v, k) {
                        if ($scope.reconAssign.reconNames.indexOf(v.reconName) != -1) {
                            if (!angular.isDefined($scope.reconAssign[v.reconProcess])) {
                                $scope.reconAssign[v.reconProcess] = [];
                            }
                            if ($scope.reconAssign.reconProcesses.indexOf(v.reconProcess) == -1) {
                                $scope.reconAssign.reconProcesses.push(v.reconProcess);
                            }
                            if ($scope.reconAssign[v.reconProcess].indexOf(v.reconName) == -1) {
                                $scope.reconAssign[v.reconProcess].push(v.reconName);
                            }
                        }
                    })

                    if ($scope.mode == 'Add') {
                        $scope.display = true
                        ReconAssignmentService.post(data, function (data) {
                            $scope.eventDoc =
                                {
                                    'type': 'Recon Assignment added',
                                    'actionOn': data['processGroup'],
                                    'logTime': Date.now(),
                                    'createdBy': $rootScope.userData['userName'],
                                }
                            AuditsService.post($scope.eventDoc)
                            $scope.recons._id = data._id
                            $rootScope.showMessage($rootScope.resources.reconAssignment, $rootScope.resources.reconAssignedSucc, 'success')
                            $scope.onServerSideItemsRequested($scope.crntPage, $scope.pageCount, $scope.filterFrom, $scope.filterWithFields, $scope.orderWith, $scope.orderReverse)
                        }, function (err) {
                            $rootScope.showMessage($rootScope.resources.reconAssignment, err.msg, 'error')
                        });
                        $scope.mode = 'Edit'
                    }

                    else if ($scope.mode == 'Edit') {
                        $scope.display = true
                        $scope.onServerSideItemsRequested()
                        ReconAssignmentService.put(data["_id"], data, function (data) {
                            $scope.eventDoc = {
                                'type': 'Recon Assignment edited',
                                'actionOn': data['processGroup'],
                                'logTime': Date.now(),
                                'createdBy': $rootScope.userData['userName'],
                            }

                            AuditsService.post($scope.eventDoc)
                            $scope.onServerSideItemsRequested($scope.crntPage, $scope.pageCount, $scope.filterFrom, $scope.filterWithFields, $scope.orderWith, $scope.orderReverse)
                            $scope.getAssignedData()
                        });
                    }
                    $uibModalInstance.dismiss('cancel')
                }

                $scope.cancel = function () {
                    $scope.onServerSideItemsRequested($scope.crntPage, $scope.pageCount, $scope.filterFrom, $scope.filterWithFields, $scope.orderWith, $scope.orderReverse)
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }
    $scope.editReconAssign = function (data) {
        // $scope.triggerDirective=false
        $scope.reconAssign = {}
        $scope.filteredProcess = []
        $scope.filteredRecons = []
        $scope.selectedProcesses = []
        $scope.completeList = []
        $scope.display = false
        $scope.mode = 'Edit'
        $scope.dictVal = {}
        $scope.reconAssign = data
        $scope.tempProcess = data['reconProcesses']
        $scope.tempRecons = data['reconNames']
        $scope.tempUsers = data['users']
        $scope.getRecons()
        $scope.getUsers($scope.tempRecons)
        $scope.openAssignment(data)

    }

    $scope.getUsers = function (selectedRecons) {
        var data = {}
        if (selectedRecons != undefined && selectedRecons.length > [] && $scope.reconAssign.groupRole != undefined) {
            var recons = []
            angular.forEach(selectedRecons, function (v, k) {
                recons.push(v['name'])
            })
            data['role'] = $scope.reconAssign.groupRole
            data['recons'] = recons
            ReconAssignmentService.getUsers('dummy', data, function (data) {
                $scope.userNames = []
                angular.forEach(data, function (v, k) {
                    if ($scope.tempUsers.indexOf(v) == -1) {
                        $scope.userNames.push({'name': v, 'ticked': false});
                    }
                    else {
                        $scope.userNames.push({'name': v, 'ticked': true});
                    }
                });
            })
        }
        else if ($scope.tempUsers.length > 0 && $scope.tempRecons.length > 0) {
            data['role'] = $scope.reconAssign.groupRole
            data['recons'] = $scope.tempRecons
            ReconAssignmentService.getUsers('dummy', data, function (data) {
                $scope.userNames = []
                angular.forEach(data, function (v, k) {
                    if ($scope.tempUsers.indexOf(v) == -1) {
                        $scope.userNames.push({'name': v, 'ticked': false});
                    }
                    else {
                        $scope.userNames.push({'name': v, 'ticked': true});
                    }
                });
            })

        }
    }


    $scope.getAssignedData = function () {
        ReconAssignmentService.get(undefined, undefined, function (data) {
            $scope.myItemsTotalCount = data['total']
            $scope.assignedData = data['data']
        })
    }

    $scope.removeUser = function (data) {
        console.log(data)
        $uibModal.open({
            templateUrl: '/app/components/notifications/confirmation.html',
            windowClass: 'modal show modalDialogCenter',
            backdrop: 'static',
            backdropClass: 'show',
            scope: $scope,
            keyboard: false,
            size: "md",
            controller: function ($uibModalInstance, $scope, $rootScope, ReconAssignmentService) {

                $scope.deleteTitle = $rootScope.resources.confirmation
                $scope.deleteMsg = $rootScope.resources['deleteMsg']
                $scope.submit = function () {
                    var actionOn = data['processGroup']
                    ReconAssignmentService.deleteData(data['_id'], function (data) {
                        $scope.eventDoc = {
                            'type': 'Recon Assignment deletion',
                            'actionOn': actionOn,
                            'logTime': Date.now(),
                            'createdBy': $rootScope.userData['userName'],
                        }
                        AuditsService.post($scope.eventDoc)
                        $scope.getAssignedData()
                        $uibModalInstance.dismiss('cancel')
                        $rootScope.showMessage($rootScope.resources['reconAssignment'], $rootScope.resources['removeSuccess'], 'success')
                    }, function (err) {
                        console.log(err)
                    })
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel')
                }
            }
        })
    }

    $scope.getAssignedData()

    $scope.getUserList()
}]);