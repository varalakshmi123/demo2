from numpy.distutils.command.config import config
from pandas.computation import expressions

import flask
import hashframe
import pandas
import numpy as np
import hashlib
from pandas.api.types import *
import AesEncrypt
import shutil
import json
import zipfile
from bson.json_util import dumps
from bson import json_util
from copy import copy
import traceback

# import ParseEj
logr = hashframe.Logger.getInstance("application").getLogger()
import datetime
import subprocess
import os
import csv
import datetime
import re
import io
from flask import request
from shutil import copyfile
import smtplib
from bson import ObjectId
import rstr
from email.mime.multipart import MIMEMultipart
from email.MIMEText import MIMEText
import uuid
import requests


# Login
class Login(hashframe.noAuthHandler):
    def create(self, doc):
        print("Entering Login Page...")
        status, data = Users().get({'userName': doc['userName']})
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, doc['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        password = hashlib.md5(decrypt).hexdigest()
        resp = "User not found"

        # check is password is correct
        if status:
            # Checking Approved status
            if data['ApprovalStatus'] == "Not Approved":
                return False, 'Account to be approved'

            # Passsword check
            if password == data['password']:
                if data["role"] not in ["admin", "Configurer"]:
                    if (datetime.datetime.now() - datetime.datetime.fromtimestamp(
                            data['lastPwdUpdt'] / 1000)).days > 45:
                        return False, 'Password Expired'

                    if data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                flask.session['sessionData'] = {}
                flask.session['sessionData']['userName'] = data['userName']
                flask.session['sessionData']['email'] = data['email']
                flask.session['sessionData']['partnerId'] = data['partnerId']
                flask.session["sessionData"]["userId"] = data['_id']
                # flask.session['sessionData']['preveliges'] = data['preveliges']
                data['accountLocked'] = False
                data['loginCount'] = 0
                Users().modify(data['_id'], data)
                if data.get('role'):
                    flask.session['sessionData']['role'] = data['role']
            else:
                # if Password is Wrong
                if data['role'] != 'admin':
                    if 'accountLocked' in data and data['accountLocked']:
                        return False, 'accountLocked Please Contact Admin'

                    if 'loginCount' in data and data['loginCount'] != hashframe.config.loginCount:
                        data['loginCount'] = data['loginCount'] + 1
                        data['accountLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'Password is Incorrect You have ' + str(
                            hashframe.config.loginCount - data['loginCount']) + ' attempts left'

                    elif 'loginCount' in data and data['loginCount'] == hashframe.config.loginCount:
                        data['accountLocked'] = True
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                        return False, 'accountLocked Please Contact Admin'

                    else:
                        data['loginCount'] = 1
                        data['accoungLocked'] = False
                        del data['password']
                        _, found_up = Users(hideFields=False).modify(data['_id'], data)
                    return False, 'Password Incorrect'
                else:
                    return False, 'Password Incorrect'

            # get all the assigned recons to session
            # _, assigned_recons = self.get_assigned_recons(data['userName'])
            # flask.session['sessionData'].update(assigned_recons)

            return True, flask.session['sessionData']
        return False, resp

    def getAll(self, query={}):
        if 'sessionData' in flask.session:
            return True, flask.session['sessionData']
        return False, "Logged out."

    def get_assigned_recons(self, user_name):
        users = []
        users.append(user_name)
        data = list(ReconAssignment().find({'users': {"$in": users}}))
        # status,data = ReconAssignment().getAll({'condition': {'userName': user_name}})
        assigned_recons = []
        recon_process = {}
        actions = {}
        if data:
            for r in data:
                assigned_recons.extend(r.get('reconNames', []))
                actions = {recon: r['groupRole'] for recon in assigned_recons}
                recon_details = list(Recons().find({"reconName": {"$in": assigned_recons}}))
                # status,recon_details = list(Recons().getAll({'condition': {"reconName": {"$in": assigned_recons}}}))
            for r in recon_details:
                if recon_process.get(r['reconProcess']) is None:
                    recon_process[r['reconProcess']] = []
                recon_process[r['reconProcess']].append({'name': r['reconName'], 'actions': actions[r['reconName']]})
                # recon_process[r['reconProcess']].append(r['reconName'])
        else:
            pass
        return True, {"reconProcess": recon_process, 'assignedRecons': assigned_recons}


#  signup User/Creating User


class ReconLicense(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconLicense, self).__init__("reconlicense", hideFields)

    def getReconLicense(self, reconName):
        doc = ReconLicense().find_one({'reconName': reconName})
        return True, doc


class NoAuth(hashframe.noAuthHandler):
    def createUser(self, id, query):
        if not re.match(r'[\w.?\d]*@suryodaybank.com', query['email']):
            return False, 'Mail Id is Not Valid'

        if Users().find_one({'userName': query['userName']}):
            return False, 'UserName Already Exist'

        if Users().find_one({'email': query['email']}):
            return False, "Mail Id Is Already Exist"

        query['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['password'])
        decrypt = decrypt.replace('/<=%+,?->/', '')
        query['password'] = hashlib.md5(decrypt).hexdigest()
        query['ApprovalStatus'] = 'Not Approved'
        query['loginCount'] = 0
        query['accountLocked'] = False
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        Users().insert_one(query)
        return True, 'Success'

    def upload(self, id):
        mftPath = os.path.join(hashframe.config.uploadDir.get(id, hashframe.config.uploadDir['file']))
        fname = flask.request.files['file'].filename

        if os.path.exists(mftPath + '/' + fname):
            os.remove(mftPath + '/' + fname)

        if not os.path.exists(mftPath):
            os.makedirs(mftPath)
        flask.request.files['file'].save(os.path.join(mftPath, fname))
        status = {'status': 'File uploaded.', 'fileName': fname}
        return True, status

    def sendPasswordToMail(self, id, query):
        userDetails = Users().find_one({'userName': query['userName']})
        if userDetails:
            if userDetails['email'] == query['associatedMail']:
                password = rstr.xeger(r'[A-Z][a-z]{2}[@#$][0-9]{4}')
                email = userDetails['email']

                try:
                    smtp = smtplib.SMTP(hashframe.config.smtpServer, hashframe.config.smtpPort)
                    sender = hashframe.config.smtpMail

                    smtp.ehlo()
                    smtp.starttls()

                    smtp.login(hashframe.config.smtpMail, hashframe.config.smtpPassword)

                    recipients = userDetails['email']

                    msg = MIMEMultipart()
                    msg["From"] = sender
                    msg['To'] = recipients
                    msg["Subject"] = 'PASSWORD CHANGE'

                    body = "UserName : " + userDetails[
                        'userName'] + "Kindly Use the following PASSWORD to login" + " : " + str(password)

                    msg.attach(MIMEText(body, 'Plain'))

                    smtp.sendmail(sender, recipients, msg.as_string())
                    smtp.close()

                    logr.info('Password Change Email sent')
                except Exception as e:
                    logr.info(e)
                    logr.info('Error while sending Email')
                    return False, 'Error while sending Mail'

                del userDetails['password']
                userDetails['password'] = hashlib.md5(str(password)).hexdigest()
                Users().modify(userDetails['_id'], userDetails)
                return True, 'Password Sent To your Mail Id'
            else:
                return False, 'Please Enter Associated mail Id'
        else:
            return False, 'User Not Found'

    def updateExpirePassword(self, id, query):
        status, doc = Users().get({'userName': query['userName']})

        if 'newPassword' in query and 'oldPassword' in query:
            decryptnewPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['newPassword'])
            decryptnewPassword = decryptnewPassword.replace('/<=%+,?->/', '')

            decryptoldPassword = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, query['oldPassword'])
            decryptoldPassword = decryptoldPassword.replace('/<=%+,?->/', '')

            newPassowrd = hashlib.md5(decryptnewPassword).hexdigest()
            oldPassword = hashlib.md5(decryptoldPassword).hexdigest()

            if (doc['password'] != oldPassword):
                return False, 'Old Password is incorrect'
            doc['password'] = newPassowrd
            doc['lastPwdUpdt'] = int(datetime.datetime.now().strftime("%s")) * 1000

        Users().modify({'userName': query['userName']}, doc)
        return True, 'User Details Updated SuccessFully'


#  Logout
class Logout(hashframe.noAuthHandler):
    def create(self, doc):
        flask.session.clear()
        flask.abort(401)

    def getAll(self, query={}):
        flask.session.clear()
        flask.abort(401)


class ReconExecDetailsLog(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconExecDetailsLog, self).__init__("recon_execution_details_log", hideFields)

    def getLatestExeDetails(self, reconId):
        doc = ReconExecDetailsLog().find_one({'reconName': reconId, 'jobStatus': 'Success'},
                                             sort=[('reconExecutionId', -1)])
        return True, doc

    def getMaxReconExecutionDoc(self, recon_names=None):
        from bson.son import SON
        pipeline = [{'$match': {'reconName': {'$in': recon_names or []}}},
                    {'$group': {'_id': '$reconName', 'executionId': {'$max': '$reconExecutionId'}}},
                    {"$sort": SON([("_id", -1)])}]

        agg_results = ReconExecDetailsLog().aggregate(pipeline=pipeline)
        filters = []
        if agg_results is not None:
            filters = [x['executionId'] for x in agg_results]

        return ReconExecDetailsLog().find({'reconExecutionId': {'$in': filters}})

    def rollbackRecon(self, id, query):
        date = datetime.datetime.strptime(query['statementDate'], '%d-%m-%Y')

        doc = ReconExecDetailsLog().find(
            {'reconName': query['reconName'], 'jobStatus': 'Success', 'statementDate': {'$gte': date}})

        if doc.count() == 0:
            return False, "No Previous data found."

        li = [d['statementDate'].strftime('%d%m%Y') for d in doc]
        mft_path = hashframe.config.mftPath + '/%s/' % query['reconName']
        for i in li:
            path = mft_path + i
            shutil.rmtree(path, ignore_errors=True)

        logr.info("Rollback Records for recon %s " % query['reconName'])
        ReconExecDetailsLog().remove(
            {'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        ReconSummary().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        CustomReport().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        MasterDumps().remove({'reconName': query['reconName'], 'statementDate': {'$gte': date}}, multi=True)
        return True, 'Rollback successful.'

    def get_latest_execution_details(self, id):
        assigned_recons = flask.session['sessionData'].get('assignedRecons', [])
        _, recons = Recons().getAll({"condition": {'reconName': {'$in': assigned_recons}}}, limit=100)

        if recons['total'] == 0:
            return False, 'No Recon Allocated.'

        recon_exec = list(self.getMaxReconExecutionDoc(assigned_recons))
        if recon_exec:
            for _exec in recon_exec:
                _exec['statementDate'] = _exec['statementDate'].strftime('%d-%m-%Y')

            recon_exec = {x['reconName']: x for x in recon_exec}
            for x in recons['data']:
                x.update(recon_exec.get(x['reconName'], {}))

        return True, {'data': recons['data'], 'total': len(recons['data'])}


# Store Audit logs
class ReconAudit(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAudit, self).__init__("reconaudit", hideFields)


# Store EJRecon execution details
class EjExecutionDetails(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(EjExecutionDetails, self).__init__("ejexecutiondetails", hideFields)

    def create(self, doc):
        (status, docExe) = super(EjExecutionDetails, self).create(doc)
        return status, docExe

    #
    def deleteColData(self, id, query):
        print("this is delete data func")
        print(query)
        params = query['reqData']
        print(params)
        status = EjExecutionDetails().remove({'Id': params['Id']})
        if status:
            return True, 'Investigation removed'
        else:
            return True, 'Investigation not removed'

    def getATMIdList(self, id, query):
        print('===========this is time query====================')
        print(query)
        path = '/opt/reconDB/mft/EJRecon/' + str(query['Id']) + '/OUTPUT/cassetedf.csv'
        timeDict = {}
        timeDf = pandas.read_csv(path)
        timeDf['atmId'] = timeDf['FILE'].apply(lambda x: x.split('_')[0].strip())
        timeDict['atm'] = list(timeDf['atmId'].unique())
        if len(timeDict['atm']) > 0:
            return True, timeDict
        else:
            return False, 'no timelist found'

    def getSupervisorRecords(self, id, query):
        print("=================this is the supervisor blog============================")
        print(query)
        path = '/opt/reconDB/mft/EJRecon/' + str(query['Id']) + '/OUTPUT/cassetedf.csv'
        print(path)
        supervisorDict = {}
        superDf = pandas.read_csv(path)
        superDf['atmId'] = superDf['FILE'].apply(lambda x: x.split('_')[0].strip())
        superDf = superDf[superDf['atmId'] == query['atmId']]
        superDf['CASH ADDED'] = superDf['CASH ADDED'].fillna('NA')
        cashDf = superDf[superDf['CASH ADDED'] != 'NA']

        # superDf['DATE-TIME'] = pandas.to_datetime(superDf['DATE-TIME'], format='%m/%d/%y %H:%M')
        # cashDf['DATE-TIME'] = pandas.to_datetime(cashDf['DATE-TIME'], format='%m/%d/%y %H:%M')
        uniqueTimeList = cashDf['DATE-TIME'].unique()
        supervisorDict['timelist'] = list(cashDf['DATE-TIME'].unique())
        if len(supervisorDict['timelist']) > 0:
            return True, supervisorDict
        else:
            return False, 'no timelist found'

    def getSupervisorData(self, id, query):
        print('====================this is supervisor data blog====================================')
        print(query)
        path = '/opt/reconDB/mft/EJRecon/' + str(query['Id']) + '/OUTPUT/cassetedf.csv'
        postDict = {}
        preDict = {}
        cashDict = {}
        superDataDict = {}
        superDataDf = pandas.read_csv(path)
        superDataDf['ATM_ID'] = superDataDf['FILE'].apply(lambda x: x.split('_')[0].strip())
        superDataDf['CASH ADDED'] = superDataDf['CASH ADDED'].fillna('NA')

        query['time'] = datetime.datetime.strptime(query['time'], '%m/%d/%y %H:%M')

        superDataDf['DATE-TIME'] = pandas.to_datetime(superDataDf['DATE-TIME'], format='%m/%d/%y %H:%M')

        # subDf = superDataDf[(superDataDf['ATM_ID'] == query['atmId'])&(superDataDf['DATE-TIME'] == query['time'])]

        # subDf.to_csv('/tmp/sample.csv')
        lastIndex = superDataDf[(superDataDf['TYPE'] == 'TYPE4') & (superDataDf['CASH ADDED'] != 'NA') & (
                superDataDf['ATM_ID'] == query['atmId']) & (
                                        superDataDf['DATE-TIME'] == query['time'])].index.values[-1]
        fstIndex = superDataDf[(superDataDf['TYPE'] == 'TYPE1') & (superDataDf['CASH ADDED'] != 'NA') & (
                superDataDf['ATM_ID'] == query['atmId']) & (
                                       superDataDf['DATE-TIME'] == query['time'])].index.values[-1]

        postDf = superDataDf.iloc[lastIndex + 1: lastIndex + 5, :]

        preDf = superDataDf.iloc[fstIndex - 4:fstIndex, :]
        cashDf = superDataDf.iloc[fstIndex:lastIndex + 1, :]

        postDf['CASH ADDED'] = postDf['CASH ADDED'].replace('NA', '0')
        preDf['CASH ADDED'] = preDf['CASH ADDED'].replace('NA', '0')
        cashDf['CASH ADDED'] = cashDf['CASH ADDED'].replace('NA', '0')

        postDf = postDf.fillna('0')
        preDf = preDf.fillna('0')
        cashDf = cashDf.fillna('0')

        postDf['TOTAL'] = postDf['TOTAL'].astype(str).str.replace('\.(0)*', '')
        preDf['TOTAL'] = preDf['TOTAL'].astype(str).str.replace('\.(0)*', '')
        cashDf['TOTAL'] = cashDf['TOTAL'].astype(str).str.replace('\.(0)*', '')

        preDf.to_csv('/tmp/pre.csv')
        postDf.to_csv('/tmp/post.csv')
        cashDf.to_csv('/tmp/cash.csv')

        for every in postDf['TYPE']:
            ind = 0
            if every not in postDict:
                postDict[every] = {}
            postDict[every]['CASH ADDED'] = postDf[postDf['TYPE'] == every]['CASH ADDED'].iloc[ind]
            postDict[every]['DISPENSED'] = postDf[postDf['TYPE'] == every]['DISPENSED'].iloc[ind]
            postDict[every]['CASH DISPENSED'] = postDf[postDf['TYPE'] == every]['CASH DISPENSED'].iloc[ind]
            postDict[every]['CASSETTE'] = postDf[postDf['TYPE'] == every]['CASSETTE'].iloc[ind]
            postDict[every]['REJECTED'] = postDf[postDf['TYPE'] == every]['REJECTED'].iloc[ind]
            postDict[every]['CASH REMAINING'] = postDf[postDf['TYPE'] == every]['CASH REMAINING'].iloc[ind]
            postDict[every]['TOTAL'] = postDf[postDf['TYPE'] == every]['TOTAL'].iloc[ind]
            postDict[every]['REMAINING'] = postDf[postDf['TYPE'] == every]['REMAINING'].iloc[ind]
            ind += 1
        print(postDict)
        for every in preDf['TYPE']:
            preInd = 0
            if every not in preDict:
                preDict[every] = {}
            preDict[every]['CASH ADDED'] = preDf[preDf['TYPE'] == every]['CASH ADDED'].iloc[preInd]

            preDict[every]['DISPENSED'] = preDf[preDf['TYPE'] == every]['DISPENSED'].iloc[preInd]
            preDict[every]['CASH DISPENSED'] = preDf[preDf['TYPE'] == every]['CASH DISPENSED'].iloc[preInd]
            preDict[every]['CASSETTE'] = preDf[preDf['TYPE'] == every]['CASSETTE'].iloc[preInd]
            preDict[every]['REJECTED'] = preDf[preDf['TYPE'] == every]['REJECTED'].iloc[preInd]
            preDict[every]['CASH REMAINING'] = preDf[preDf['TYPE'] == every]['CASH REMAINING'].iloc[preInd]
            preDict[every]['TOTAL'] = preDf[preDf['TYPE'] == every]['TOTAL'].iloc[preInd]
            preDict[every]['REMAINING'] = preDf[preDf['TYPE'] == every]['REMAINING'].iloc[preInd]
            preInd += 1
        print(preDict)
        for every in cashDf['TYPE']:
            cashInd = 0
            if every not in cashDict:
                cashDict[every] = {}
            cashDict[every]['CASH ADDED'] = cashDf[cashDf['TYPE'] == every]['CASH ADDED'].iloc[cashInd]

            cashDict[every]['DISPENSED'] = cashDf[cashDf['TYPE'] == every]['DISPENSED'].iloc[cashInd]
            cashDict[every]['CASH DISPENSED'] = cashDf[cashDf['TYPE'] == every]['CASH DISPENSED'].iloc[cashInd]
            cashDict[every]['CASSETTE'] = cashDf[cashDf['TYPE'] == every]['CASSETTE'].iloc[cashInd]
            cashDict[every]['REJECTED'] = cashDf[cashDf['TYPE'] == every]['REJECTED'].iloc[cashInd]
            cashDict[every]['CASH REMAINING'] = cashDf[cashDf['TYPE'] == every]['CASH REMAINING'].iloc[cashInd]
            cashDict[every]['TOTAL'] = cashDf[cashDf['TYPE'] == every]['TOTAL'].iloc[cashInd]
            cashDict[every]['REMAINING'] = cashDf[cashDf['TYPE'] == every]['REMAINING'].iloc[cashInd]
            cashInd += 1
        print(cashDict)

        superDataDict['preDict'] = preDict
        superDataDict['postDict'] = postDict
        superDataDict['cashDict'] = cashDict
        print(superDataDict)

        if len(preDict) > 0 and len(postDict) > 0:
            return 'True', superDataDict
        else:
            return False, 'No Data'

    def getColData(self, id, query):
        print(
            "=======================this is column      query===========================================================")
        print(query)
        path = '/opt/reconDB/mft/EJRecon/' + str(query) + '/OUTPUT/alldf.csv'
        print(path)
        reqDict = {}
        if not os.path.exists(path):
            return False, "File not found."

        reqDf = pandas.read_csv(path)

        reqDf['ACCOUNT NO'] = reqDf['ACCOUNT NO'].fillna('').str.strip().astype(str).replace(' ', '').str.lstrip('0')
        reqDf['ATM ID'] = reqDf['ATM ID'].fillna('').str.strip().astype(str).replace(' ', '').str.lstrip('0')
        reqDf['CARD NUMBER'] = reqDf['CARD NUMBER'].fillna('').str.strip().astype(str).replace(' ', '').str.lstrip('0')
        reqDict['AccNo'] = list(set(reqDf['ACCOUNT NO'].tolist()))
        reqDict['ATMId'] = list(set(reqDf['ATM ID'].tolist()))
        reqDict['CARD NUMBER'] = list(set(reqDf['CARD NUMBER'].tolist()))
        print(reqDict)

        if not reqDf.empty:

            return True, reqDict
        else:
            return False, 'Fail'


#  store users details
class Users(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(Users, self).__init__("users", hideFields)

    def create(self, doc):
        (status, userDoc) = super(Users, self).create(doc)
        return status, userDoc

    def deleteUser(self, id, query):
        status, data = Users().delete(id)
        status, data = ReconAssignment().delete({"userName": query['userName']})
        if status:
            return True, 'DELETED'
        else:
            return False, 'error'


# Recon Processes information collection
class ReconProcess(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconProcess, self).__init__("reconProcessDetails", hideFields)

    def create(self, doc):
        (status, reconProcessDetails) = super(ReconProcess, self).create(doc)
        return status, reconProcessDetails

    def saveProcessDetail(self, id, query):
        doc = ReconProcess().find_one({'reconProcess': query['reconProcess']})
        if doc:
            doc['reconProcess'] = query['reconProcess']
            if 'imagePath' in query.keys():
                doc['imagePath'] = query['imagePath']
            if 'isLicensed' in query.keys():
                doc['isLicensed'] = query['isLicensed']
            status, data = ReconProcess().modify(doc['_id'], doc)
            return True, 'Process updated successfully'
        query['partnerId'] = "54619c820b1c8b1ff0166dfc"
        query['isLicensed'] = False
        doc = ReconProcess().save(query)
        return True, 'Uploaded'

    def getJobProcess(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            doc['statementDate'] = doc['statementDate'].strftime("%d%m%Y")
        return True, doc


# Cases raised from mail information collection
class CasesInvestigation(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(CasesInvestigation, self).__init__("caseInvestigation", hideFields)

    def create(self, doc):
        (status, caseInvestigation) = super(CasesInvestigation, self).create(doc)
        return status, caseInvestigation


# Tickets created for each case information collection
class ErrorCodes(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ErrorCodes, self).__init__("errorCodes", hideFields)

    def create(self, doc):
        (status, errorCodes) = super(ErrorCodes, self).create(doc)
        return status, errorCodes


# Tickets created for each case information collection
class CaseTickets(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(CaseTickets, self).__init__("caseTickets", hideFields)

    def create(self, doc):
        (status, caseTickets) = super(CaseTickets, self).create(doc)
        return status, caseTickets

    def getFilePath(self, id, query):
        date = query['creationTime'].split(' ')[0].split('-')
        date1 = ['', '', '']
        date1[0] = date[-1]
        date1[1] = date[-2]
        date1[2] = date[-3]
        date1 = ''.join(date1)
        caseIdPath = '/tmp/' + date1 + os.sep + query['ticketId']

        if query['fileRequired'] == 'ej':
            fileName = query['ejFile']
        else:
            fileName = query['ticketId'] + '.html'
        if os.path.exists(caseIdPath + os.sep + fileName):
            copyfile(caseIdPath + os.sep + fileName, hashframe.config.contentDir + fileName)
            return True, fileName
        else:
            return False, ''

    def getErrorCode(self):
        status, errorCodes = ErrorCodes().getAll({'limit': 0})
        return

    def formatTheTicket(self, id, query):
        excludeKeys = ["_id", "partnerId", "recommdation", "errorCode", 'Justification (if Rejected)',
                       'Customer Status', 'Dispute Comments', 'HO Remarks', 'Internal Action', 'Deposited Date',
                       'Deposited Amount', 'Error Code', 'Error Code', 'Description', 'Error Category',
                       'Shortage From Date', 'Shortage To Date']
        requiredKeys = {
            'ATM ID': ['atm_id', 'atmid'],
            'Transaction Date': ['transactiondate', 'transaction_date'],
            'Dispute Date': [],
            'Disputed Amount': ['disputedamt', 'disputedamount','disputeamt'],
            'Reference No': ['referenceno'],
            'Shortage From Date': [],
            'Shortage To Date': [],
            'Card Number': ['cardno', 'cardnumber'],
            'Transaction Amount': ['tranamt', 'transactionamount'],
            'Transaction Number': ['tranno', 'seqno'],
            'Dispute Comments': [],
            'Error Code': [],
            'Customer Status': [],
            'Justification (if Rejected)': [],
            'HO Remarks': [],
            'Internal Action': [],
            'Deposited Date': [],
            'Deposited Amount': [],
            'caseId':['caseid'],
            'ticketId':['ticketid']
        }

        dict1 = {}
        for key, value in requiredKeys.items():
            for k, v in query.items():
                if k.replace(' ', '').lower() in value:
                    if key not in excludeKeys:
                        dict1[key] = v
            if key not in dict1.keys() and key not in excludeKeys:
                dict1[key] = ''

            if key in excludeKeys and key not in query.keys():
                query[key] = ''
        data = {}
        creationTime = query['creationTime']
        status,res = self.parseEJSummary(dict1,creationTime)
        status1,cbr = self.cbrcomputation(dict1,creationTime)
        if status:
            data['enqTxn'] = res['enqTxn']
            data['ejSummary'] = res['summary']
            data['missingSeq'] = res['missingSeq']
            data['investSummary'] = res['investSummary']
            query['investigationStatus'] = res['investigationStatus']
        else:
            query['investigationStatus'] = res

        cbrlist = []
        dict2 = {}
        print(cbr)
        if status1 and len(cbr)>0:
            for key,val in cbr[0].items():
                dict2  = {}
                if key in ['midCashStatus']:
                    continue
                dict2['key'] = key
                dict2['value'] = val
                if key == 'Overage Total':
                    if val>0:
                        dict2['status'] = 'Overage Reported'
                    else:
                        dict2['status'] = 'Overage Not Reported'
                elif key == 'Shortage Total':
                    if val>0:
                        dict2['status'] = 'Shortage Reported'
                    else:
                        dict2['status'] = 'Shortage Not Reported'

                elif key == 'Replenishment Total':
                    if val>0:
                        dict2['status'] = 'Replenishment Reported'
                    else:
                        dict2['status'] = 'Replenishment Not Reported'
                elif key == 'Mid Cash Increase Total' :
                    if val>0 and cbr[0]['midCashStatus'] !='notReported':
                        dict2['status'] = 'Mid Cash Increase Reported'
                    else:
                        dict2['status'] = 'Mid Cash Increase Not Reported'
                elif key == 'Mid Cash Decrease Total':
                    if val>0:
                        dict2['status'] = 'Mid Cash Decrease Reported'
                    else:
                        dict2['status'] = 'Mid Cash Decrease Not Reported'
                cbrlist.append(dict2)

        data['cbr'] = cbrlist
        data['dict1'] = dict1
        data['query'] = query

        return True, data

    def cbrcomputation(self, dict1, loadingdate):
        cbrdata = {}
        LoadingDate = dict1['Transaction Date']
        LoadingDate = LoadingDate.split('-')
        LoadingDate = LoadingDate[1] + ' ' + LoadingDate[0] + ', 20' + LoadingDate[2]
        print(LoadingDate)
        print(dict1['ATM ID'])
        df = pandas.read_csv('/tmp/9611247StandardCBRReport.csv')
        print(df.columns)
        df = df.loc[
            (df['ATM ID'].str.strip(' ') == dict1['ATM ID']) & (df['Loading_Date'].str.strip(' ') == LoadingDate)]
        df.fillna('', inplace=True)
        # data_index = df.index[df['Loading_Date'] == LoadingDate].tolist()[0]
        # cbrdata['loadingDate'] = df['Loading_Date'][data_index]
        # cbrdata['atmId'] = df['ATM ID'][data_index]
        # cbrdata['overageTotal'] = df['Overage Total'][data_index]
        # cbrdata['shortageTotal'] = df['Shortage Total'][data_index]
        # cbrdata['replineshmentTotal'] = df['Replenishment Total'][data_index]
        # cbrdata['midCashIncrese'] = df['Mid Cash Increase Total'][data_index]
        # cbrdata['midCashDecrease'] = df['Mid Cash Decrease Total'][data_index]
        df = df[['Overage Total', 'Shortage Total', 'Replenishment Total',
                 'Mid Cash Increase Total', 'Mid Cash Decrease Total']]
        df.loc[df['Mid Cash Increase Total'] == df['Replenishment Total'], 'midCashStatus'] = 'notReported'
        return True, df.to_dict(orient='records')

    def parseEJSummary(self, dict1, creationTime):
        query = {}
        query['fileRequired'] = 'ej'
        query['atmId'] = dict1['ATM ID']
        query['ticketId'] = dict1['ticketId']
        query['ticketId'] = dict1['ticketId']
        query['creationTime'] = creationTime

        status, filePath = self.getFilePath('dummy', query)

        if filePath:
            date = query['creationTime'].split(' ')[0].split('-')
            date1 = ['', '', '']
            date1[0] = date[-1]
            date1[1] = date[-2]
            date1[2] = date[-3]
            date1 = ''.join(date1)
            caseIdPath = '/tmp/' + date1 + os.sep + query['ticketId']
            caseIdPath += os.sep + query['ticketId'].split('.')[1] + os.sep
            fileName = caseIdPath + filePath
            df = pandas.read_csv(fileName, dtype='str', sep='|')
            tranNo = dict1['Transaction Number']
            print(df.columns)
            df.fillna('', inplace=True)
            df.loc[df['ERROR CODE'] != '', 'TRAN_TYPE'] = df['ERROR CODE']
            df['COUNT'] = 1
            df['SEQ NO'] = df['SEQ NO'].astype(str)
            ejSummary = df.groupby(['TRAN_TYPE'])['COUNT'].sum().reset_index()
            enqTran = df.loc[df['SEQ NO'].str.strip(' ') == str(tranNo).strip(' ')]

            idx = df.index[df['SEQ NO'].str.strip(' ') == str(tranNo).strip(' ')].tolist()[0]
            onlyreq = df.loc[idx - 5:idx + 5]
            onlyreq['COUNT'] = 1
            onlyreq['SEQ NO'] = onlyreq['SEQ NO'].str.strip(' ')
            missingSeqences = onlyreq['SEQ NO'].unique().tolist()
            seqNo = []
            for idx in range(0, len(missingSeqences) - 1):
                if missingSeqences[idx] != '' and missingSeqences[idx + 1] != '':
                    if int(missingSeqences[idx + 1]) - int(missingSeqences[idx]) > 2:
                        for j in range(0, int(missingSeqences[idx + 1]) - int(missingSeqences[idx]) - 1):
                            seqNo.append(int(missingSeqences[idx]) + j + 1)
                    elif int(missingSeqences[idx + 1]) - int(missingSeqences[idx]) > 1:
                        seqNo.append(int(missingSeqences[idx]) + 1)
            investigationSummary = onlyreq.groupby(['TRAN_TYPE'])['COUNT'].sum().reset_index()

            data1 = {}
            data1['enqTxn'] = enqTran.to_dict(orient='records')
            data1['summary'] = ejSummary.to_dict(orient='records')
            data1['investSummary'] = investigationSummary.to_dict(orient='records')
            data1['missingSeq'] = seqNo
            if len(seqNo) > 2:
                data1['investigationStatus'] = 'ATM WAS NOT WORKING FINE'

            if len(data1):
                return True, data1
            else:
                return False, 'TXN NOT FOUND'
        return False, 'No Files Found'


#  Recon Assigned Collection
class ReconAssignment(hashframe.MongoCollection):
    def __init__(self, hideFields=True):
        super(ReconAssignment, self).__init__("reconassignment", hideFields)

    def create(self, doc):
        (status, reconassignmentDoc) = super(ReconAssignment, self).create(doc)
        return status, reconassignmentDoc

    def deleteData(self, condition):
        (status, found) = ReconAssignment(hideFields=False).get({'_id': condition})
        return super(ReconAssignment, self).delete(condition)

    # Get Assigned Recons For logged In User
    def assignedRecons(self, id, query):
        status, assignData = ReconAssignment().getAll({'condition': {'userName': query['userName']}})
        if status and assignData['total'] == 0:
            return False, 'No Recon Assignment For this User'
        status, reconsData = Recons().getAll({})
        reconsList = {}
        assignedRecons = []
        for doc in assignData['data']:
            for recon in reconsData['data']:
                if recon['reconProcess'] in doc['reconProcesses']:
                    reconsList[recon['reconProcess']] = [] if recon['reconProcess'] not in reconsList.keys() else \
                        reconsList[recon['reconProcess']]
                    if recon['reconName'] in doc['reconNames']:
                        reconsList[recon['reconProcess']].append(recon['reconName'])
                        assignedRecons.append(recon['reconName'])
        detailsLog = []
        for i in assignedRecons:
            stat, logDetails = ReconExecDetailsLog().getLatestExeDetails(i)
            if logDetails:
                for process in reconsList:
                    if i in reconsList[process]:
                        logDetails['reconProcess'] = process
                        logDetails['statementDate'] = datetime.datetime.strftime(logDetails['statementDate'],
                                                                                 '%Y-%m-%d')
                detailsLog.append(logDetails)
            else:
                doc = {'reconName': i}
                for process in reconsList:
                    if i in reconsList[process]:
                        doc['reconProcess'] = process
                detailsLog.append(doc)
        data = {'total': len(detailsLog), 'reconsLog': detailsLog, 'data': reconsList}
        return True, data

    def getReconDetails(self):
        recondata = list(Recons().find({}))
        recondetails = []
        if len(recondata):
            reconAssign = ReconAssignment()
            users = []
            users.append(flask.session["sessionData"]["userName"])
            assignData = list(reconAssign.find({'users': {"$in": users}}))

            if assignData:
                for data in recondata:
                    for recon in assignData:
                        if data['reconName'] in recon['reconNames']:
                            recondetails.append(data)
        return True, recondetails

    def getUsers(self, id, query):
        usernames = []
        roles = hashframe.config.roles
        reconAssignments = list(ReconAssignment().find(
            {'groupRole': roles[query['role']], 'reconNames': {'$in': query['recons']}}))

        for i in reconAssignments:
            usernames.extend(i['users'])
        users = list(Users().find({"role": {'$ne': 'admin'}, 'ApprovalStatus': 'Approved'}))
        allUsers = [user['userName'] for user in users]
        availableUsers = list(set(allUsers) - set(usernames))
        return True, availableUsers


# reorder Column Collection
class ColumnConfiguration(hashframe.MongoCollection):
    def __init__(self):
        super(ColumnConfiguration, self).__init__('columnConfiguration')

    def create(self, doc):
        (status, columnConfiguration) = super(ColumnConfiguration, self).create(doc)
        return status, columnConfiguration

    def removeSourceColumns(self, id, query):
        status, doc = ColumnConfiguration().get(query)
        if status:
            ColumnConfiguration().remove(query)
            return True, 'Columns Reordering Removed Successfully'
        return False, 'Cannot delete the reordering columns'


# recon Summary Collection
class ReconSummary(hashframe.MongoCollection):
    def __init__(self):
        super(ReconSummary, self).__init__('recon_summary')

    def create(self, doc):
        (status, reconSummary) = super(ReconSummary, self).create(doc)
        return status, reconSummary

    def getSummary(self, query):
        doc = ReconSummary().find_one(query)
        return True, doc


# Recon Details Collections
class ReconDetails(hashframe.MongoCollection):
    def __init__(self):
        super(ReconDetails, self).__init__("recon_details")

    def create(self, doc):
        (status, recon_details) = super(ReconDetails, self).create(doc)
        return status, recon_details

    def getDashboardSummary(self, id):
        status, doc = ReconAssignment().getReconDetails()
        if status:
            total = len(doc)
            ExecLog = []
            data = {'reconsExceuted': 0, 'successCount': 0, 'failureCount': 0}
            for recon in doc:
                if recon['reconProcess'] not in data.keys():
                    # data[recon['reconProcess']] = []
                    data['executedReconsList'] = []
                    data[recon['reconProcess']] = []
                data[recon['reconProcess']].append(recon['reconName'])
                execlog = ReconExecDetailsLog().find_one(
                    {"reconName": recon['reconName']},
                    sort=[('RECON_EXECUTION_ID', -1)])
                if execlog:
                    data['reconsExceuted'] += 1
                    data['executedReconsList'].append(recon['reconName'])
                    if execlog['jobStatus'] == 'Success':
                        data['successCount'] += 1
                    else:
                        data['failureCount'] += 1
                det = (
                    ReconExecDetailsLog().find_one({"reconName": recon['reconName']},
                                                   sort=[('reconExecutionId', -1)]))
                if det is not None:
                    det['reconProcess'] = recon['reconProcess']
                    ExecLog.append(det)
            if len(ExecLog):
                data['msg'] = 'reconExecution details'
                data['total'] = len(ExecLog)
                for i in ExecLog:
                    i['statementDate'] = datetime.datetime.strftime(i['statementDate'], '%Y-%m-%d')
            # else:
            #     return True, "There is no execution details"
            data['totalAssignedRecons'] = total
            data['reconDeatails'] = doc
            data['reconExecutionDetailsLog'] = ExecLog
            return True, data
        else:
            return False, 'No Recons assigned for user'

    def exportAllCsv(self, id, query):
        data = {}
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            fileName = filePath + '/' + query['reportPath']
            reportPath = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/' + query['reportPath']
            copyfile(reportPath, fileName)
            fileName = flask.session["sessionData"]["userId"] + '/' + query['reportPath']
            return True, fileName
        return False, 'No Execution Details Available'

    def getOlfFormatReport(self, id, query):
        status, log = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        log['statementDate'] = log['statementDate'].strftime("%d%m%Y")
        if log:
            filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filePath):
                os.mkdir(filePath)
            path = hashframe.config.mftPath + query['reconName'] + '/' + log[
                'statementDate'] + '/' + 'OUTPUT' + '/'
            outfiles = os.listdir(path)
            columnList = []
            alldf = pandas.DataFrame()
            doc = list(ColumnConfiguration().find({"reconName": query['reconName']}))
            if status:
                for source in doc:
                    for file in [source['source'] + '.csv', source['source'] + '_UnMatched.csv',
                                 source['source'] + '_Self_Matched']:
                        if file in os.listdir(path):
                            status, converters = ReconDetails().getConverters(query['reconName'], source['source'])
                            # converters['System_Idx'] = str
                            df = pandas.read_csv(path + file)
                            for key, val in converters.iteritems():
                                df[key] = df[key].astype(val)
                            if df.empty:
                                continue
                            status, doc = ColumnConfiguration().get(
                                {"reconName": query['reconName'], "source": source['source']})
                            columnList.extend(doc['columns'])
                            seen = set()
                            columnList = [x for x in columnList if not (x in seen or seen.add(x))]
                            matchfil = ['(df["%s"].fillna("") == "MATCHED")' % col for col in df.columns if
                                        "Match" in col and 'Self Matched' not in col]
                            matchfil = "&".join(matchfil)
                            if 'Self Matched' in df.columns:
                                matchfil = matchfil + '|(df["Self Matched"].fillna("")=="MATCHED")'
                            matchfil = eval(matchfil)
                            if len(df) > 0:
                                df.loc[matchfil, 'MATCHING_STATUS'] = 'MATCHED'
                                df.loc[df['MATCHING_STATUS'].fillna('') == '', 'MATCHING_STATUS'] = 'UNMATCHED'
                                # df=df[keyColumns+['MATCHED']]
                                # if alldf.empty:
                                #     alldf = pandas.concat([df, alldf], join_axes=[df.columns])
                                # else:
                                #     alldf = pandas.concat([df, alldf], join_axes=[alldf.columns])
                                alldf = alldf.append(df)
                                alldf = alldf[columnList + ['MATCHING_STATUS']]
                                alldf = alldf.reset_index(drop=True)

                            if query['type'] == 'Matched':
                                alldf = alldf[alldf['MATCHING_STATUS'] == 'MATCHED']
                            else:
                                alldf = alldf[alldf['MATCHING_STATUS'] == 'UNMATCHED']

                for col in alldf.columns.tolist():
                    alldf[col] = alldf[col].fillna(ReconDetails().infer_dtypes(alldf[col]))

                alldf = alldf.replace('nan', '')
                alldf.rename(columns={'SOURCE': "SOURCE_NAME"}, inplace=True)

                if query['reconName'] in hashframe.config.reconMapper.keys():
                    infoJson = hashframe.config.reconMapper[query['reconName']]
                    if 'expressions' in infoJson:
                        for exp in infoJson['expressions']:
                            exec (exp)

                    if 'sortOn' in infoJson:
                        alldf = alldf.sort_values(by=infoJson['sortOn'])

                    if 'apostrophe' in infoJson:
                        for col in infoJson['apostrophe']:
                            alldf[col] = "'" + alldf[col].astype(str)

                fileName = query['reconName'] + '_' + query['type'] + '_' + str(
                    datetime.datetime.now()) + '.csv'
                alldf.to_csv(filePath + os.sep + fileName, index=False)
                return True, flask.session["sessionData"]["userId"] + '/' + fileName
            else:
                return False, 'No Execution Details Available'

    def getTransactionDetails(self, id, query):
        countDict = {}
        query['reconName'] = 'EJRecon'
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        print('======================here printing query================================')
        print(query)
        # if doc:
        # stmtDate = doc['statementDate'].strftime("%d%m%Y")
        # for v in query['values']:
        #     if v['field'] =='AMOUNT':
        #         query[v['field']] = int(v['value'])
        #     else:
        #         query[v['field']] = v['value']

        fileName = query['fileName'].split('.')[0]
        datet = query['date'].replace('/', '')

        path = hashframe.config.mftPath + query[
            'reconName'] + '/' + fileName + '_' + datet + '/' + 'OUTPUT/' + 'alldf.csv'

        print(path)
        inputDf = pandas.read_csv(path)

        print("===============================this is the input df==================================")
        print(inputDf)
        for everyKey in query['values']:
            if inputDf[everyKey['field']].dtype == 'object':
                inputDf[everyKey['field']] = inputDf[everyKey['field']].fillna('').str.strip().astype(str).replace(' ',
                                                                                                                   '').str.lstrip(
                    '0')
            elif inputDf[everyKey['field']].dtype == 'float64':
                inputDf[everyKey['field']] = inputDf[everyKey['field']].astype(str).str.replace('\.(0)*', '').astype(
                    np.int64)

            if everyKey['field'] == 'TRANS AMOUNT':
                everyKey['value'] = int(everyKey['value'])
                copyDf = inputDf[inputDf[everyKey['field']] == everyKey['value']]
            else:
                copyDf = inputDf[inputDf[everyKey['field']] == everyKey['value'].strip()]
            if copyDf.empty:
                copyDf = inputDf
            else:
                inputDf = copyDf

        # inputDf['TRANS AMOUNT'] = inputDf['TRANS AMOUNT'].fillna(0)
        # inputDf['ATM ID'] = inputDf['ATM ID'].fillna('')
        # inputDf['ACCOUNT NO'] =  inputDf['ACCOUNT NO'].fillna('')
        # print(query)
        # # print(inputDf[(inputDf['ATM ID'].str.strip() == query['ATM ID'])&(inputDf['ACCOUNT NO'].str.strip() == query['AccountNumber'])])
        # inputDf.to_csv('/tmp/sample.csv')
        # print(inputDf[inputDf['TRANS AMOUNT'] == query['amount']])
        # print('-----------------------------------------------')
        # inputDf['ATM ID'] = inputDf['ATM ID'].str.strip(' ')
        # inputDf['ACCOUNT NO'] = inputDf['ACCOUNT NO'].fillna('')
        # inputDf['ACCOUNT NO'] = inputDf['ACCOUNT NO'].astype(str)
        # inputDf['ACCOUNT NO'] = inputDf['ACCOUNT NO'].str.replace(' ', '')

        # inputDf['ACCOUNT NO'] = inputDf['ACCOUNT NO'].str.lstrip('0')

        # inputDf = inputDf[(inputDf['ATM ID'].str.strip() == query['ATM ID'])&(inputDf['ACCOUNT NO'].str.strip() == query['ACCOUNT NO'])&(inputDf['TRANS AMOUNT'] == query['AMOUNT'])]
        # inputDf = inputDf.fillna('')
        # print('-----------------------------------------------')

        # inputDf = inputDf.fillna('')

        countDict['data'] = inputDf.fillna("").to_dict(orient='records')
        countDict['columns'] = [col for col in inputDf.columns]
        # print(countDict['data'])
        # print(countDict['columns'])
        # countDict = {'count':len(inputDf),'msg':'success','result':inputDf.to_dict(orient = 'records')}
        print("======================this is countdict=============================")
        print(countDict)
        if not inputDf.empty:

            return True, countDict
        else:
            return False, 'Fail'

    def getReqTransactionDetails(self, id, query):

        print('===============this is filter query===============================')
        print(query)
        query['reconName'] = 'EJRecon'
        filterDict = {}
        path = hashframe.config.mftPath + query['reconName'] + '/' + query['ID'] + '/' + 'OUTPUT/' + 'alldf.csv'
        print(path)
        filterDf = pandas.read_csv(path)
        print("===============this is filterdf===========================")
        print(len(filterDf))
        filterDf['ATM ID'] = filterDf['ATM ID'].fillna('').str.strip().astype(str).replace(' ', '').str.lstrip('0')
        filterDf['CARD NUMBER'] = filterDf['CARD NUMBER'].fillna('').str.strip().astype(str).replace(' ',
                                                                                                     '').str.lstrip('0')
        filterDf['ACCOUNT NO'] = filterDf['ACCOUNT NO'].fillna('').str.strip().astype(str).replace(' ', '').str.lstrip(
            '0')
        filterDf['TRANS AMOUNT'] = filterDf['TRANS AMOUNT'].astype(str).str.replace('\.(0)*', '').astype(np.int64)

        # print(filterDf[(filterDf['ATM ID'].str.strip() == query['ATMID'])&(filterDf['ACCOUNT NO'].str.strip() == query['AccountNumber'])])
        # print(type(query['Amount']))
        # print(filterDf['TRANS AMOUNT'].dtype)
        filterDf = filterDf[(filterDf['ATM ID'].str.strip() == query['ATMID']) & (
                filterDf['ACCOUNT NO'].str.strip() == query['AccountNumber']) & (
                                    filterDf['TRANS AMOUNT'] == query['Amount']) & (
                                    filterDf['CARD NUMBER'] == query['CardNumber'])]
        print(
            '==============================this is filterdf after result=========================================================')
        print(filterDf)

        filterDict['data'] = filterDf.fillna("").to_dict(orient='records')
        filterDict['columns'] = [col for col in filterDf.columns]
        if not filterDf.empty:

            return True, filterDict
        else:
            return False, 'Fail'

    def getReportDetails(self, id, query):

        details = ReconDetails().find_one({'reconName': query['reconName']})
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if doc:
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
            summaryQuery = {'reconName': query['reconName'], 'statementDate': doc['statementDate'],
                            'reconExecutionId': doc['reconExecutionId']}
            status, summary = ReconSummary().getSummary(summaryQuery)
            if summary:
                summary['sources'] = []
                summary['matchedCount'] = []
                summary['unMatchedCount'] = []
                sourceGroup = {}
                sourceGroup['defaultReport'] = {}
                for report in details['reportDetails']:
                    # report['source'] = str(report['source']).replace(' ', '_')
                    # if report['source'] not in summary['sources']:
                    #     summary['sources'].append(report['source'])
                    #     summary['matchedCount'].append()
                    for i in summary['details']:
                        if i['Source'] == report['source']:
                            if report['source'] not in summary['sources']:
                                summary['sources'].append(report['source'])
                                summary['matchedCount'].append(i['Matched Count'])
                                summary['unMatchedCount'].append(i['UnMatched Count'])
                            # i['Execution Date Time'] = datetime.datetime.strptime(i['Execution Date Time'],"%d-%m-%Y")
                            #           if type(i['statementDate']) == datetime.datetime:
                            #               i['statementDate'] = datetime.datetime.strftime(i['Statement Date'], "%d-%m-%Y")
                            #               i['Execution Date Time'] = i['Execution Date Time'].strftime("%d-%m-%Y")
                            if report['reportName'] == 'Unmatched':
                                i['unMatchedReport'] = report['reportPath']
                            elif report['reportName'] == 'Matched':
                                i['matchedReport'] = report['reportPath']
                            elif report['reportName'] == report['source'] + ' ' + 'Self Match':
                                i['reversalReport'] = report['reportPath']

                    if report['source'] == 'Reports':
                        if 'customReports' not in summary:
                            summary['customReports'] = []
                        summary['customReports'].append(
                            {"name": report['reportName'], 'reportPath': report['reportPath']})

                summary['statementDate'] = summary['statementDate'].strftime("%d-%m-%Y")
            else:
                return False, 'No summary Found'

        else:
            return False, 'No execution found for this recon'
        return True, summary

    #  Filtered Data with advanced Search
    def getFilteredData(self, id, query):
        records = {}
        query['reconName'] = 'EJRecon'
        dicty = {'alldf': "EJ", 'cassetedf': "Supervisor"}

        print(query['source'])
        df = pandas.DataFrame()
        if 'stmtDate' in query.keys() and query['stmtDate']:
            stmtDate = datetime.datetime.strptime(str(query['stmtDate']), '%d/%m/%y').strftime("%d%m%Y")

        else:
            status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
            stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters('EJRecon', dicty[query['source']])

        path = hashframe.config.mftPath + str(query['reconName']) + '/' + stmtDate + '/' + 'OUTPUT/' + str(
            query['source'] + '.csv')
        print()
        if not os.path.exists(path):
            return False, "File Path Doesn't Exist"
        df = pandas.read_csv(path)
        print(query['filters'])
        print(len(df))
        if 'filters' in query.keys() and query['filters']:
            for expression in query['filters']:
                exec expression
        print(len(df))
        index = query['index']
        pageSize = query['pageSize']
        length = len(df)

        maxPages = ((length / pageSize) + 1)
        skip = (index * pageSize) - pageSize

        limit = (index * pageSize)
        if limit < 0:
            limit = len(df) + 1

        df = df[skip:limit]

        for col in df.columns:
            df[col] = df[col].fillna(self.infer_dtypes(df[col]))
        columnGrouper = self.columnDTypeGrouper(converters)

        records['data'] = df.to_dict(orient='records')
        records['maxPages'] = maxPages
        records['columns'] = [col for col in df.columns]
        records['reconName'] = query['reconName']
        records['columnGrouper'] = columnGrouper
        return True, records

    # get Authorize Recoords
    def getAuthourizedRecords(self, id, query):
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/'
        details = Recons().find_one({'reconName': query['reconName']})
        sources = details['sources']
        totalDf = pandas.DataFrame()
        for source in sources:
            sourcePath = path + source + '_Authorization.csv'
            if (os.path.exists(sourcePath)):
                df = pandas.read_csv(sourcePath)
                totalDf.append(df)
        if len(totalDf) > 0:
            return True, totalDf
        else:
            return False, 'No Data Found'

    #  get Reports Data With Pagination
    def getReportData(self, id, query):
        records = {}
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], query['source'])
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query['reportPath']
        print(path)
        print(os.path.exists(path))
        if (os.path.exists(path)):
            print("INSIDE")
            index = query['index']
            pageSize = query['pageSize']
            length = int(subprocess.check_output("wc -l " + path.replace(' ', '\ '), shell=True).split()[0])
            maxPages = ((length / pageSize) + 1)

            skip = (index * pageSize) - pageSize
            limit = length - (index * pageSize)
            if maxPages:
                records['maxPages'] = maxPages
            # initialize headers
            with open(hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                'reportPath'], 'r') as file:
                reader = csv.reader(file)
                header = reader.next()
                df = pandas.read_csv(
                    hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                        'reportPath'],
                    skiprows=skip + 1, skipfooter=0 if limit < 0 else limit, names=header,
                    header=None,
                    engine='python')

            for col in df.columns:
                df[col] = df[col].fillna(self.infer_dtypes(df[col]))
            columnGrouper = self.columnDTypeGrouper(converters)

            if query['carryForward']:
                df = df[df['CARRY_FORWARD'] == 'Y']
            status, columnsDoc = ColumnConfiguration().get({'reconName': query['reconName'], 'source': query['source']})
            if status:
                df = df[columnsDoc['columns']]
            records['data'] = df.to_dict(orient='records')
            records['columns'] = [col for col in df.columns]
            records['reconName'] = query['reconName']
            records['report'] = query['reportPath']
            records['columnGrouper'] = columnGrouper
        else:
            return False, "No Files are there"
        return True, records

    def getEjData(self, id, query):
        records = {}
        query['reconName'] = 'EJRecon'
        query['reportPath'] = query['source'] + '.csv'
        print
        query
        dicty = {'alldf': "EJ", 'cassetedf': "Supervisor"}
        print(dicty['cassetedf'])
        status, doc = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        stmtDate = doc['statementDate'].strftime("%d%m%Y")
        status, converters = self.getConverters(query['reconName'], dicty[query['source']])

        # status, converters = self.getConverters(query['reconName'], query['source'])
        path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query['reportPath']
        print(path)
        print(os.path.exists(path))
        if (os.path.exists(path)):
            index = query['index']
            pageSize = query['pageSize']
            length = int(subprocess.check_output("wc -l " + path.replace(' ', '\ '), shell=True).split()[0])
            maxPages = ((length / pageSize) + 1)
            skip = (index * pageSize) - pageSize
            limit = length - (index * pageSize)
            if maxPages:
                records['maxPages'] = maxPages
            # initialize headers
            with open(hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                'reportPath'], 'r') as file:
                reader = csv.reader(file)
                header = reader.next()
                df = pandas.read_csv(
                    hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query[
                        'reportPath'],
                    skiprows=skip + 1, skipfooter=0 if limit < 0 else limit, names=header,
                    header=None,
                    engine='python')
                print(df.dtypes)
            for col in df.columns:
                df[col] = df[col].fillna(self.infer_dtypes(df[col]))
            columnGrouper = self.columnDTypeGrouper(converters)

            # df['TRANS AMOUNT']=df['TRANS AMOUNT'].fillna('0')
            records['data'] = df.to_dict(orient='records')
            records['columns'] = [col for col in df.columns]
            records['reconName'] = query['reconName']
            records['report'] = query['reportPath']
            records['columnGrouper'] = columnGrouper

            # print(records)
        else:
            return False, "No Files are there"
        return True, records

    def getConverters(self, reconName, source):
        # print (reconName,source)
        doc = ColumnDataTypes().getColumnDataTypes(reconName, source)

        converters = {}
        if doc:
            for val in doc['dtypes']:
                if str(val['dtype']) != 'np.datetime64':
                    converters[val['column']] = self.typeMapper(val['dtype'])
                else:
                    converters[val['column']] = str
        else:
            logr.info('-------------------------')
            logr.info('No Column Dtypes Found')
            return False, 'No Column Dtypes Found'
        return True, converters

    # Download CustomReports
    def downloadCustomReport(self, id, query):
        status, execLog = ReconExecDetailsLog().getLatestExeDetails(query['reconName'])
        if status:
            stmtDate = execLog['statementDate'].strftime("%d%m%Y")
            filepath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
            if not os.path.exists(filepath):
                os.mkdir(filepath)
            path = hashframe.config.mftPath + query['reconName'] + '/' + stmtDate + '/' + 'OUTPUT/' + query['report'][
                'reportPath']
            if not os.path.exists(path):
                return True, 'Report file not found'
            copyfile(path, filepath + '/' + query['report']['reportPath'])
            return True, flask.session["sessionData"]["userId"] + '/' + query['report']['reportPath']

    def columnDTypeGrouper(self, converters):
        columnGrouper = {"string": [], "numeric": []}
        for key, val in converters.iteritems():
            print(val)
            if val == str:
                columnGrouper["string"].append(key)

            elif val == np.float64:
                columnGrouper["numeric"].append(key)

            elif val == np.int64:
                columnGrouper["numeric"].append(key)

        print(columnGrouper)
        return columnGrouper

    def typeMapper(self, dtype):
        if dtype == 'str':
            return str
        elif dtype == 'np.float64':
            return np.float64

        elif dtype == 'np.int64':
            return np.int64
        else:
            return np.datetime64

    def infer_dtypes(self, series):
        if is_float_dtype(series):
            return 0.0
        elif is_integer_dtype(series):
            return 0
        elif is_string_dtype(series):
            return ''
        elif is_datetime64_dtype(series):
            return pandas.NaT
        else:
            np.nan

    def updateReportName(self, id, query):
        report = query['report']
        data = ReconDetails().update(
            {'reconName': query['selectedRecon'], 'reportDetails.reportPath': report['reportPath']},
            {'$set': {'reportDetails.$.reportName': report['reportName']}})
        return True, data

    def getReports(self, id, query):
        reportData = ReconDetails().find_one({'reconName': query['reconName']})
        return True, reportData

    def getHistoricalData(self, id, query):
        user = flask.session['sessionData']['userName']
        password = flask.session['sessionData']['password']
        decrypt = AesEncrypt.decrypt(hashframe.config.key, hashframe.config.iv, password)
        decrypt = decrypt.replace('/<=%+,?->/', '')
        s = requests.Session()
        a = s.post('https://erecon.com/api/login',
                   data=json.dumps({"userName": user,
                                    "password": decrypt}),
                   headers={"Content-Type": "application/json"}, verify=False)
        reconMapper = hashframe.config.reconMapper
        if a.status_code == 200:
            doc = {"accountNumber": "",
                   "executionEndDate": 0,
                   "executionStartDate": 0,
                   "exportData": True,
                   "recon_id": reconMapper[query['recon_name']]['reconId'],
                   "recon_name": reconMapper[query['recon_name']]['reconName'],
                   "statementEndDate": query['statementEndDate'],
                   "statementStartDate": query['statementStartDate'],
                   }
            headers = {'Content-Type': 'application/json'}
            data = {'query': doc}
            result = s.post('https://erecon.com/api/recon_assignment/dummy/getReconMatchedStatementDate',
                            headers=headers, data=json.dumps(data))
            result = json.loads(str(result.text))
            fileName = ''
            print
            result
            if result['status'] == 'ok' and result['msg']['txn_count'] > 0:
                fileName = result['msg']['fileName']
                oid = fileName.split('/')[0]
                downloadedfileName = '\ '.join(fileName.split('/')[1].split(' '))
                completeFileName = '/usr/share/nginx/www/erecon/ui/app/files/recon_reports/' + oid + '/' + downloadedfileName
                filePath = hashframe.config.contentDir + flask.session["sessionData"]["userId"]
                if not os.path.exists(filePath):
                    os.mkdir(filePath)
                cmd = 'mv ' + completeFileName + ' ' + filePath
                os.system(cmd)
                fileName = flask.session["sessionData"]["userId"] + '/' + fileName.split('/')[-1]

        return True, fileName


#  Recon Job Operations
class Utilities(object):
    def __init__(self):
        self.sourceData = {}

    def upload(self, id):
        data = dict()
        mftPath = os.path.join(hashframe.config.mftPath)
        if not os.path.exists(mftPath):
            os.makedirs(mftPath)

        filesDir = mftPath + "/" + data['date'][0]
        if not os.path.exists(filesDir):
            os.mkdir(filesDir)

        if data['fileName'][0]:
            # request.files['file'].save(os.path.join(filesDir, data['fileName'][0]))
            if not os.path.exists(os.path.join(hashframe.config.mftPath, data['date'][0])):
                os.mkdir(hashframe.config.mftPath + data['date'][0])
            else:
                return False, 'Files already uploaded with this date'

            os.system('mv %s %s' % (
                os.path.join(filesDir, data['fileName'][0]), hashframe.config.mftPath + data['date'][0] + os.sep))
            # request.files['sendfile'].save('/tmp/'+dummy)
            return True, id
        else:
            return False, data

    def upload_ej_files(self, id):
        print(id)
        print("==================this is ejparsing==================================")
        params = json.loads(request.form['data'])
        hashvar = hashlib.md5(params['fileUploaded'].split('.')[0]).hexdigest()
        invId = params['fileUploaded'].split('.')[0] + '_' + str(datetime.datetime.now().date()) + '_' + hashvar
        print("=====================this is random id========================")
        print(invId)

        params['Id'] = invId
        print(params)
        params['partnerId'] = '54619c820b1c8b1ff0166dfc'
        print(dir(EjExecutionDetails))
        status = EjExecutionDetails().save(params)

        # statementDate = params['statementDate'].replace('/','')

        contentDir = os.path.join(hashframe.config.contentDir)

        if not os.path.exists(contentDir):
            os.makedirs(contentDir)

        request.files['file'].save(os.path.join(contentDir, id))
        logr.info("File Uploaded : %s" % os.path.join(contentDir, id))
        logr.info(hashframe.config.mftPath + '/' + 'EJRecon')

        shutil.copy(os.path.join(contentDir, id), hashframe.config.mftPath + '/' + 'EJRecon')

        pathId = params['Id']
        data = ParseEj().initializeEjParser(id, pathId)
        print("===============this is data==============================")
        print(data)
        return True, data

    def upload_recon_files(self, id, query):
        try:
            params = query
            stmt_date = params['statementDate']

            contentDir = os.path.join(hashframe.config.contentDir)
            if not os.path.exists(contentDir):
                os.makedirs(contentDir)

            # request.files['file'].save(os.path.join(contentDir, id))
            # logr.info("File Uploaded : %s" % os.path.join(contentDir, id))
            # logr.info(hashframe.config.mftPath + '/' + params['reconName'])

            # shutil.copy(os.path.join(contentDir, id), hashframe.config.mftPath + '/' + params['reconName'])

            statementDate = datetime.datetime.strptime(stmt_date, '%d/%m/%Y')

            # create upstart script & init
            #
            # init_service = '_'.join(params['reconName'].split())
            # fname = "/etc/init/%s" % init_service + '_RJob.conf'
            #
            # if os.path.exists(fname):
            #     return False, "Previous Job in progress, please wait until it completes."
            #
            # with open(fname, "w") as f:
            #     lines = list("script\n")
            #     lines.append("truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
            #                                                                               logFile=params['reconName']))
            #
            #     cmd = "{envPath} {flowRunner} {reconName} {stmtDate} {reportFile} > {dir}/error_logs/'{logFile}_Log.log' \n".format(
            #         envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
            #         stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
            #         reportFile=request.files['file'].filename , dir=hashframe.config.contentDir,
            #         logFile=params['reconName'])
            #     lines.append(cmd)
            #     lines.append("rm -f %s\n" % fname)
            #     lines.append("end script\n")
            #     f.writelines(lines)
            #
            # logr.info('=================================== UP-Start Job ===================================')
            # logr.info('Execution Command')
            # logr.info(cmd)
            # (status, out, err) = self.execution('initctl start "%s_RJob"' % init_service)
            # logr.info("====================================================================================")
            # os.remove(contentDir + '/' + id)
            #
            # return True, "Recon Job Initiated, please wait."

            # create upstart script & init
            init_service = '_'.join(params['reconName'].split())
            fname = "/etc/systemd/system/%s" % init_service + '_RJob.service'
            if os.path.exists(fname):
                return False, "Previous Job in progress, please wait until it completes."

            with open(fname, "w") as f:
                lines = list()
                lines.append('[Unit]\n')
                lines.append("Description = Recon Job\n")
                lines.append("After = network.target \n")
                lines.append("[Service] \n")
                lines.append("User = root \n")
                pre = "truncate -s 0 {dir}/error_logs/{logFile}_Log.log \n".format(dir=hashframe.config.contentDir,
                                                                                   logFile=params['reconName'])

                cmd = "/bin/sh -c 'exec {envPath} {flowRunner} {reconName} {stmtDate} > {dir}/error_logs/{logFile}_Log.log' \n".format(
                    envPath=hashframe.config.envPath, flowRunner=hashframe.config.flowRunner,
                    stmtDate=statementDate.strftime('%d-%b-%Y'), reconName=params['reconName'],
                    dir=hashframe.config.contentDir,
                    logFile=params['reconName'])
                lines.append("ExecStartPre=%s" % pre)
                lines.append("ExecStart=%s" % cmd)
                print(cmd)
                lines.append("ExecStop=/bin/rm -f %s" % fname)
                f.writelines(lines)

            logr.info('=================================== UP-Start Job ===================================')
            logr.info('Execution Command')
            logr.info(cmd)

            os.system('systemctl daemon-reload')
            os.system('systemctl start "%s_RJob.service"' % init_service)
            logr.info("====================================================================================")
            # os.remove(contentDir + '/' + id)
            return True, "Recon Job Initiated, please wait."
        except Exception as e:
            print
            traceback.format_exc()
            return False, 'Internal Error, Contact Admin.'

    def execution(self, command):
        childProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        close_fds=True)
        out, err = childProcess.communicate()
        status = childProcess.returncode
        return status

    class ParseEj:
        def __init__(self):
            self.tmpList = []

        def parseEj(self, f):
            if 'FNKD' in f:
                df = self.getFNKDTransaction(f)

            else:
                df = self.getOtherTypes(f)
            return df

        def getOtherTypes(self, f):
            self.complete = []
            self.tmpList = []
            fp = io.open(f, 'r', encoding='utf-8')
            lines = fp.readlines()
            start = False
            for index, line in enumerate(lines):
                try:

                    if 'ATM ID' in line:
                        start = True
                        for i in range(1, 4):
                            if lines[index - i].strip().replace('\n', '') != '':
                                self.tmpList.append(lines[index - i])

                    if 'BANK OF BARODA' in line:
                        self.tmpList.append(line)
                        if len(self.tmpList) > 1:
                            res = self.extractData(f)
                            self.complete.append(res)
                        self.tmpList = []
                        start = False

                    if start and line.strip().replace('\n', '') != '':
                        self.tmpList.append(line)
                except Exception as e:
                    print(e)
            df = pandas.DataFrame(self.complete)
            return df

        def getFNKDTransaction(self, f):
            self.complete = []
            self.tmpList = []
            fp = io.open(f, 'r', encoding='utf-8')
            lines = fp.readlines()
            start = False
            for index, line in enumerate(lines):
                try:

                    if 'TRANSACTION START' in line:
                        start = True
                        if lines[index - 1].strip().replace('\n', '') != '':
                            self.tmpList.append(lines[index - 1])

                    if 'TRANSACTION END' in line:
                        self.tmpList.append(line)
                        if len(self.tmpList) > 1:
                            res = self.extractData(f)
                            self.complete.append(res)
                        self.tmpList = []
                        start = False

                    if start and line.strip().replace('\n', '') != '':
                        self.tmpList.append(line)
                except Exception as e:
                    print(e)
            df = pandas.DataFrame(self.complete)
            return df

        def extractData(self, f):
            doc = {}
            for i, line in enumerate(self.tmpList):

                for type in ['ATM ID', 'REF NO', 'RRN NO', 'CARD NUMBER', 'ACCOUNT NO', 'RESP CODE', 'SEQ NO',
                             'RESP CDE']:
                    if type in line:
                        doc[type] = re.search('(:[a-zA-z 0-9]*)', line).group().strip().replace(":", "") if re.search(
                            '(:[a-zA-z 0-9]*)', line) else ''
                        break

                for tran in ['BALANCE INQUIRY', 'CASH WITHDRAWAL', 'WITHDRAWAL', 'INQUIRY']:
                    if tran in line:
                        doc['TRAN_TYPE'] = tran
                        break

                for amount in ['AVAIL. BALANCE', 'TRANS AMOUNT']:
                    if amount in line:
                        doc[amount] = re.search('(RS.[0-9. ]*)', line).group().replace("RS.", "").strip() if re.search(
                            '(RS.[0-9.]*)', line) else ''
                        break

                if 'RESP CODE' not in doc.keys() and 'TRANSACTION DECLINED' in line:
                    doc['RESP CODE'] = self.tmpList[i + 1].strip()
                    doc['REASON'] = self.tmpList[i + 2].strip()
                    continue

                if 'NOTES PRESENTED' in line:
                    groups = re.findall('([0-9,]+)', line)
                    if groups:
                        doc['NOTES PRESENTED'] = groups[-1]

                if 'CASH TOTAL' in line:
                    self.getDispensedRecords(i, doc)

                if 'FNKD' in f:
                    df = self.getFNKDDatef(line, doc)
                    continue

                else:
                    df = self.getOtherTypesDate(i, line, doc)

            # doc['FILE']=file.split('/')[-1]
            self.tmpList = []
            return doc

        def getDispensedRecords(self, index, doc):
            for type in ['CASH TOTAL', 'DISPENSED', 'REJECTED', 'REMAINING']:
                for i in range(index, index + 5):
                    if type in self.tmpList[i]:
                        doc[type] = ','.join(self.tmpList[i].split(' ')[-4:])

        def getOtherTypesDate(self, index, line, doc):
            if 'Date' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line):
                doc['Date'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line).group()
                if 'Location' not in doc.keys():
                    doc['Location'] = self.tmpList[index + 1].strip().replace('BANK OF BARODA', '').strip()

        def getFNKDDatef(self, line, doc):
            if 'Date' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{4}[ *]*[0-9]{2}:[0-9]{2})', line):
                doc['Date'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{4}[ *]*[0-9]{2}:[0-9]{2})', line).group()

            if 'Location' not in doc.keys() and re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ ]*[0-9]{2}:[0-9]{2})', line):
                doc['Location'] = line.strip().replace('BANK OF BARODA', '').strip()

        def getCassetData(self, f):
            self.complete = []
            self.cdf = pandas.DataFrame()
            self.tmpList = []
            fp = io.open(f, 'r', encoding='utf-8')
            print(f)
            lines = fp.readlines()
            start = False
            for index, line in enumerate(lines):
                try:

                    if 'SUPERVISOR MODE ENTRY' in line:
                        start = True
                        if line.strip().replace('\n', '') != '':
                            self.tmpList.append(line)

                    if 'SUPERVISOR MODE EXIT' in line:
                        self.tmpList.append(line)
                        if len(self.tmpList) > 1:
                            res = self.extractCasseteData()
                            df = self.getDfFromDoc(res)
                            if len(df) > 0:
                                self.cdf = pandas.concat([self.cdf, df], join_axes=[df.columns])
                            self.complete.append(res)
                        self.tmpList = []
                        start = False

                    if start and line.strip().replace('\n', '') != '':
                        self.tmpList.append(line)
                except Exception as e:
                    print(e)
            return self.cdf

        def getDfFromDoc(self, doc):
            columns = ['CASH ADDED',
                       'CASH DISPENSED',
                       'DISPENSED',
                       'CASSETTE',
                       'REJECTED',
                       'CASH REMAINING', 'TOTAL',
                       'REMAINING', 'TYPE', 'LAST CLEARED', 'DATE-TIME']

            alldf = pandas.DataFrame(columns=columns)
            for data in doc:
                for type in ['TYPE1', 'TYPE2', 'TYPE3', 'TYPE4']:
                    df = pandas.DataFrame([data[type]])
                    df['TYPE'] = type
                    df['LAST CLEARED'] = data['LAST CLEARED']
                    df['DATE-TIME'] = data['DATE-TIME']
                    alldf = pandas.concat([alldf, df], join_axes=[alldf.columns])
            return alldf

        def extractCasseteData(self):
            doc = []
            data = {}
            data['TYPE1'] = {}
            data['TYPE2'] = {}
            data['TYPE3'] = {}
            data['TYPE4'] = {}
            count = 0
            for i, line in enumerate(self.tmpList):
                if 'DATE-TIME' in line:
                    if count == 1:
                        doc.append(data.copy())
                        data['TYPE1'] = {}
                        data['TYPE2'] = {}
                        data['TYPE3'] = {}
                        data['TYPE4'] = {}
                        count = 0
                    data['DATE-TIME'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                                  line).group() if re.search(
                        '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''
                    count = count + 1

                if 'TYPE 1   TYPE 2' in line:

                    for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                        for ldx in range(i + 1, i + 5):
                            if type in self.tmpList[ldx]:
                                groups = re.findall('([0-9]+)', self.tmpList[ldx])
                                data['TYPE1'][type] = groups[0]
                                data['TYPE2'][type] = groups[1]

                if 'TYPE 3   TYPE 4' in line:
                    for type in ['CASSETTE', 'REJECTED', 'REMAINING', 'DISPENSED', 'TOTAL']:
                        for ldx in range(i + 1, i + 6):
                            if type in self.tmpList[ldx]:
                                groups = re.findall('([0-9]+)', self.tmpList[ldx])
                                data['TYPE3'][type] = groups[0]
                                data['TYPE4'][type] = groups[1]

                for cashType in ['CASH DISPENSED', 'CASH REMAINING', 'CASH ADDED']:
                    if cashType in line:
                        print(self.tmpList[i + 1])
                        groups = re.findall('(=[ 0-9]*)', self.tmpList[i + 1])
                        print(groups)
                        if groups:
                            for ctype in ['TYPE1', 'TYPE2']:
                                if not cashType in data[ctype]:
                                    data[ctype][cashType] = int(
                                        groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                        groups[1].replace('=', '').strip())
                                else:
                                    data[ctype][cashType] = data[ctype][cashType] + int(
                                        groups[0].replace('=', '').strip()) if ctype == 'TYPE1' else int(
                                        groups[1].replace('=', '').strip())

                    if cashType in line:
                        groups = re.findall('(=[ 0-9]*)', self.tmpList[i + 2])
                        print(groups)
                        if groups:
                            for ctype in ['TYPE3', 'TYPE4']:
                                if not cashType in data[ctype]:
                                    data[ctype][cashType] = int(
                                        groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                        groups[1].replace('=', '').strip())
                                else:
                                    data[ctype][cashType] = data[ctype][cashType] + int(
                                        groups[0].replace('=', '').strip()) if ctype == 'TYPE3' else int(
                                        groups[1].replace('=', '').strip())

                if 'LAST CLEARED' in line:
                    data['LAST CLEARED'] = re.search('([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})',
                                                     line).group() if re.search(
                        '([0-9]{2}\/[0-9]{2}\/[0-9]{2}[ *]*[0-9]{2}:[0-9]{2})', line) else ''

            return doc

    def typeMapper(self, dtype):
        if dtype == 'object':
            return 'str'
        elif dtype == 'float64':
            return 'np.float64'

        elif dtype == 'int64':
            return 'np.int64'
        else:
            return 'np.datetime64'

    def initializeEjParser(self, uploadedZipFile, pathId):
        print(id)
        print(pathId)
        path = '/opt/reconDB/mft/EJRecon/'
        source_path = os.path.join(path, uploadedZipFile)
        print(source_path)
        extract_path = path + pathId
        print(extract_path)
        statementDate = datetime.datetime.now()

        if os.path.isfile(source_path):
            extractFile = zipfile.ZipFile(source_path, mode='a')
            if not os.path.exists(os.path.join(extract_path)):
                os.mkdir(extract_path)

            if os.listdir(extract_path) != []:
                shutil.rmtree(extract_path)

            extractFile.extractall(extract_path)
        files = os.listdir(extract_path)
        output_path = os.path.join(extract_path, 'OUTPUT')
        if not os.path.exists(output_path):
            os.mkdir(output_path)

        alldf = pandas.DataFrame(
            columns=['ACCOUNT NO', 'ATM ID', 'AVAIL. BALANCE', 'CARD NUMBER', 'Date', 'FILE', 'Location', 'REF NO',
                     'RESP CODE',
                     'RRN NO', 'TRANS AMOUNT', 'TRAN_TYPE', 'REASON', 'SEQ NO', 'RESP CDE', 'CASH TOTAL',
                     'DISPENSED', 'REJECTED', 'REMAINING', 'NOTES PRESENTED'])

        p = ParseEj()

        for f in files:
            df = pandas.DataFrame()
            df = p.parseEj(extract_path + os.sep + f)

            if not df.empty:
                df = df.fillna('')
                df['FILE'] = f
                alldf = alldf.fillna('')
                alldf = pandas.concat([df, alldf], join_axes=[alldf.columns])
        for type in ['*UNABLE TO DISPENSE', 'UNABLE TO DISPENSE', '*UNABLE TO PROCESS', 'UNABLE TO PROCESS']:
            alldf.loc[alldf['RESP CODE'] == type, ['REASON',
                                                   'RESP CODE']] = type, ''

        for col in ['TRANS AMOUNT', 'AVAIL. BALANCE']:
            alldf[col] = alldf[col].astype(str).replace('', '0').astype(np.float64)
        alldf.to_csv(output_path + '/alldf.csv', index=False)

        columns = ['CASH ADDED',
                   'CASH DISPENSED',
                   'DISPENSED',
                   'CASSETTE',
                   'REJECTED',
                   'CASH REMAINING', 'TOTAL',
                   'REMAINING', 'TYPE', 'LAST CLEARED', 'DATE-TIME']
        cassetedf = pandas.DataFrame(columns=columns)
        for f in files:
            df = pandas.DataFrame()
            df = p.getCassetData(extract_path + os.sep + f)
            if not df.empty:
                df = df.fillna('')
                df['FILE'] = f
                cassetedf = cassetedf.fillna('')
                cassetedf = pandas.concat([cassetedf, df], join_axes=[df.columns])
        cassetedf = cassetedf.drop_duplicates(subset=cassetedf.columns.tolist())
        cassetedf.to_csv(output_path + '/cassetedf.csv', index=False)

        for column in ['CASH ADDED',
                       'CASH DISPENSED',
                       'DISPENSED',
                       'CASSETTE',
                       'REJECTED',
                       'CASH REMAINING',
                       'REMAINING']:
            cassetedf[column] = cassetedf[column].astype(str).replace('', '0').astype(np.int64)

        status, doc = ReconExecDetailsLog().getLatestExeDetails('EJRecon')

        columnDtype = []

        if not ColumnDataTypes().find({"reconName": "EJRecon", "source": 'EJ'}).count() > 0:
            for index, dtype in enumerate(alldf.dtypes):
                columnDtype.append({'column': alldf.columns[index], 'dtype': self.typeMapper(str(dtype))})
            ColumnDataTypes().insert_one(
                {"reconName": "EJRecon", "source": 'EJ', "partnerId": "54619c820b1c8b1ff0166dfc",
                 "dtypes": columnDtype})
        columnDtype = []
        if not ColumnDataTypes().find({"reconName": "EJRecon", "source": 'Supervisor'}).count() > 0:
            for index, dtype in enumerate(cassetedf.dtypes):
                columnDtype.append({'column': cassetedf.columns[index], 'dtype': self.typeMapper(str(dtype))})
            ColumnDataTypes().insert_one(
                {"reconName": "EJRecon", "source": 'Supervisor', "partnerId": "54619c820b1c8b1ff0166dfc",
                 "dtypes": columnDtype})

        if len(alldf) > 0 and os.path.exists(output_path + '/alldf.csv'):
            if doc is None:
                ReconExecDetailsLog().insert({
                    "executionDateTime": datetime.datetime.now().strftime('%s'),
                    "partnerId": "54619c820b1c8b1ff0166dfc",
                    "reconName": "EJRecon",
                    "statementDate": statementDate,
                    "errorMsg": "",
                    "jobStatus": "Success",
                    "reconExecutionId": "1000",
                    "uploadedFile": extract_path.split('/')[-1]
                })
            else:
                reconExecutionId = str(int(doc['reconExecutionId']) + 1)
                ReconExecDetailsLog().insert({
                    "executionDateTime": datetime.datetime.now().strftime('%s'),
                    "partnerId": "54619c820b1c8b1ff0166dfc",
                    "reconName": "EJRecon",
                    "statementDate": statementDate,
                    "errorMsg": "",
                    "jobStatus": "Success",
                    "reconExecutionId": reconExecutionId,
                    "uploadedFile": extract_path.split('/')[-1]
                })
            return 'Success'

        else:
            reconExecutionId = str(int(doc['reconExecutionId']) + 1)
            ReconExecDetailsLog().insert({
                "executionDateTime": datetime.datetime.now().strftime('%s'),
                "partnerId": "54619c820b1c8b1ff0166dfc",
                "reconName": "EJRecon",
                "statementDate": statementDate,
                "errorMsg": "",
                "jobStatus": "Failed",
                "reconExecutionId": reconExecutionId,
                "uploadedFile": extract_path.split('/')[-1]

            })

            return 'Failed'
